import 'dart:convert';

Models modelsFromJson(String str) => Models.fromJson(json.decode(str));

String modelsToJson(Models data) => json.encode(data.toJson());

class Models {
    Models({
        this.model3,
        this.page,
    });

    List<Model3> model3;
    Page page;

    factory Models.fromJson(Map<String, dynamic> json) => Models(
        model3: List<Model3>.from(json["model3"].map((x) => Model3.fromJson(x))),
        page: Page.fromJson(json["page"]),
    );

    Map<String, dynamic> toJson() => {
        "model3": List<dynamic>.from(model3.map((x) => x.toJson())),
        "page": page.toJson(),
    };
}

class Model3 {
    Model3({
        this.id,
        this.modelUrl,
        this.fileName,
        this.createdDate,
        this.showInMainCamera,
        this.active,
    });

    int id;
    dynamic modelUrl;
    String fileName;
    DateTime createdDate;
    bool showInMainCamera;
    bool active;

    factory Model3.fromJson(Map<String, dynamic> json) => Model3(
        id: json["id"],
        modelUrl: json["modelUrl"],
        fileName: json["fileName"],
        createdDate: DateTime.parse(json["createdDate"]),
        showInMainCamera: json["showInMainCamera"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "modelUrl": modelUrl,
        "fileName": fileName,
        "createdDate": createdDate.toIso8601String(),
        "showInMainCamera": showInMainCamera,
        "active": active,
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}
