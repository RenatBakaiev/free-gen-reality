// import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';

// class ProfileFaq extends StatefulWidget {
//   @override
//   _ProfileFaqState createState() => _ProfileFaqState();
// }

// class _ProfileFaqState extends State<ProfileFaq> {
//   bool isExpanded = false;
//   bool isExpanded2 = false;
//   bool isExpanded3 = false;
//   bool isExpanded4 = false;
//   bool isExpanded5 = false;
//   bool isExpanded6 = false;

//   @override
//   Widget build(BuildContext context) {
//     final theme = Theme.of(context).copyWith(
//       dividerColor: Colors.transparent,
//     );

//     return Scaffold(
//         body: Stack(
//       overflow: Overflow.visible,
//       children: [
//         Container(
//           decoration: BoxDecoration(
//             color: Hexcolor('#F2F2F2'),
//           ),
//           child: Column(
//             children: <Widget>[
//               Container(
//                 decoration: BoxDecoration(
//                   color: Hexcolor('#7D5AC2'),
//                   borderRadius: BorderRadius.only(
//                       bottomLeft: Radius.circular(10.0),
//                       bottomRight: Radius.circular(10.0)),
//                 ),
//                 child: Container(
//                   decoration: BoxDecoration(
//                     color: Hexcolor('#7D5AC2'),
//                   ),
//                   alignment: Alignment.bottomLeft,
//                   margin: EdgeInsets.only(
//                     top: 50,
//                     // left: 15,
//                     // right: 15,
//                     bottom: 22,
//                   ),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: <Widget>[
//                       Container(
//                         margin: EdgeInsets.only(left: 10, right: 10),
//                         child: SizedBox(
//                           // height: 55,
//                           // width: 40,
//                           child: IconButton(
//                             icon: Image.asset(
//                               'assets/fg_images/6_home_search_back.png',
//                               width: 10,
//                               height: 19,
//                             ),
//                             onPressed: () {
//                               Navigator.of(context).pop();
//                             },
//                           ),
//                         ),
//                       ),
//                       Text(
//                         'ЧаВо',
//                         style: TextStyle(
//                             fontSize: 33,
//                             color: Colors.white,
//                             fontFamily: 'Arial',
//                             fontWeight: FontWeight.w900),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               Container(
//                 child: Expanded(
//                     child: SingleChildScrollView(
//                         physics: BouncingScrollPhysics(),
//                         child: Container(
//                             margin: EdgeInsets.only(left: 5),
//                             child: Column(
//                               children: [
//                                 Theme(
//                                   data: theme,
//                                   child: ExpansionTile(
//                                     title: Row(
//                                       children: [
//                                         Text(
//                                           "Как выполнять квест?",
//                                           style: TextStyle(
//                                             color: isExpanded
//                                                 ? Hexcolor('#FE6802')
//                                                 : Hexcolor('#1E2E45'),
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w600,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     trailing: isExpanded
//                                         ? Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_up.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#FE6802'),
//                                           )
//                                         : Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_down.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#B7B7B7'),
//                                           ),
//                                     onExpansionChanged: (bool expanding) =>
//                                         setState(
//                                             () => this.isExpanded = expanding),
//                                     children: <Widget>[
//                                       Container(
//                                         margin: EdgeInsets.only(bottom: 13),
//                                         child: Divider(
//                                           color: Hexcolor('#ECECEC'),
//                                           thickness: 1,
//                                         ),
//                                       ),
//                                       Container(
//                                         margin: EdgeInsets.only(
//                                             left: 17, right: 37, bottom: 13),
//                                         child: Text(
//                                           "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                                           style: TextStyle(
//                                             color: Hexcolor('#6F6F6F'),
//                                             fontSize: 15,
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w500,
//                                           ),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(
//                                   color: Hexcolor('#ECECEC'),
//                                   thickness: 1,
//                                 ),
//                                 Theme(
//                                   data: theme,
//                                   child: ExpansionTile(
//                                     title: Row(
//                                       children: [
//                                         Text(
//                                           "Как улучшить свой рейтинг?",
//                                           style: TextStyle(
//                                             color: isExpanded2
//                                                 ? Hexcolor('#FE6802')
//                                                 : Hexcolor('#1E2E45'),
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w600,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     trailing: isExpanded2
//                                         ? Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_up.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#FE6802'),
//                                           )
//                                         : Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_down.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#B7B7B7'),
//                                           ),
//                                     onExpansionChanged: (bool expanding) =>
//                                         setState(
//                                             () => this.isExpanded2 = expanding),
//                                     children: <Widget>[
//                                       Container(
//                                         margin: EdgeInsets.only(bottom: 13),
//                                         child: Divider(
//                                           color: Hexcolor('#ECECEC'),
//                                           thickness: 1,
//                                         ),
//                                       ),
//                                       Container(
//                                         margin: EdgeInsets.only(
//                                             left: 17, right: 37, bottom: 13),
//                                         child: Text(
//                                           "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                                           style: TextStyle(
//                                             color: Hexcolor('#6F6F6F'),
//                                             fontSize: 15,
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w500,
//                                           ),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(
//                                   color: Hexcolor('#ECECEC'),
//                                   thickness: 1,
//                                 ),
//                                 Theme(
//                                   data: theme,
//                                   child: ExpansionTile(
//                                     title: Row(
//                                       children: [
//                                         Text(
//                                           "Как стать партнером?",
//                                           style: TextStyle(
//                                             color: isExpanded3
//                                                 ? Hexcolor('#FE6802')
//                                                 : Hexcolor('#1E2E45'),
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w600,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     trailing: isExpanded3
//                                         ? Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_up.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#FE6802'),
//                                           )
//                                         : Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_down.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#B7B7B7'),
//                                           ),
//                                     onExpansionChanged: (bool expanding) =>
//                                         setState(
//                                             () => this.isExpanded3 = expanding),
//                                     children: <Widget>[
//                                       Container(
//                                         margin: EdgeInsets.only(bottom: 13),
//                                         child: Divider(
//                                           color: Hexcolor('#ECECEC'),
//                                           thickness: 1,
//                                         ),
//                                       ),
//                                       Container(
//                                         margin: EdgeInsets.only(
//                                             left: 17, right: 37, bottom: 13),
//                                         child: Text(
//                                           "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                                           style: TextStyle(
//                                             color: Hexcolor('#6F6F6F'),
//                                             fontSize: 15,
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w500,
//                                           ),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(
//                                   color: Hexcolor('#ECECEC'),
//                                   thickness: 1,
//                                 ),
//                                 Theme(
//                                   data: theme,
//                                   child: ExpansionTile(
//                                     title: Row(
//                                       children: [
//                                         Text(
//                                           "Вопрос?",
//                                           style: TextStyle(
//                                             color: isExpanded4
//                                                 ? Hexcolor('#FE6802')
//                                                 : Hexcolor('#1E2E45'),
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w600,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     trailing: isExpanded4
//                                         ? Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_up.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#FE6802'),
//                                           )
//                                         : Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_down.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#B7B7B7'),
//                                           ),
//                                     onExpansionChanged: (bool expanding) =>
//                                         setState(
//                                             () => this.isExpanded4 = expanding),
//                                     children: <Widget>[
//                                       Container(
//                                         margin: EdgeInsets.only(bottom: 13),
//                                         child: Divider(
//                                           color: Hexcolor('#ECECEC'),
//                                           thickness: 1,
//                                         ),
//                                       ),
//                                       Container(
//                                         margin: EdgeInsets.only(
//                                             left: 17, right: 37, bottom: 13),
//                                         child: Text(
//                                           "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                                           style: TextStyle(
//                                             color: Hexcolor('#6F6F6F'),
//                                             fontSize: 15,
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w500,
//                                           ),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(
//                                   color: Hexcolor('#ECECEC'),
//                                   thickness: 1,
//                                 ),
//                                 Theme(
//                                   data: theme,
//                                   child: ExpansionTile(
//                                     title: Row(
//                                       children: [
//                                         Text(
//                                           "Стать партнером?",
//                                           style: TextStyle(
//                                             color: isExpanded5
//                                                 ? Hexcolor('#FE6802')
//                                                 : Hexcolor('#1E2E45'),
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w600,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     trailing: isExpanded5
//                                         ? Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_up.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#FE6802'),
//                                           )
//                                         : Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_down.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#B7B7B7'),
//                                           ),
//                                     onExpansionChanged: (bool expanding) =>
//                                         setState(
//                                             () => this.isExpanded5 = expanding),
//                                     children: <Widget>[
//                                       Container(
//                                         margin: EdgeInsets.only(bottom: 13),
//                                         child: Divider(
//                                           color: Hexcolor('#ECECEC'),
//                                           thickness: 1,
//                                         ),
//                                       ),
//                                       Container(
//                                         margin: EdgeInsets.only(
//                                             left: 17, right: 37, bottom: 13),
//                                         child: Text(
//                                           "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                                           style: TextStyle(
//                                             color: Hexcolor('#6F6F6F'),
//                                             fontSize: 15,
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w500,
//                                           ),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(
//                                   color: Hexcolor('#ECECEC'),
//                                   thickness: 1,
//                                 ),
//                                 Theme(
//                                   data: theme,
//                                   child: ExpansionTile(
//                                     title: Row(
//                                       children: [
//                                         Text(
//                                           "Какие статусы игрока бывают?",
//                                           style: TextStyle(
//                                             color: isExpanded6
//                                                 ? Hexcolor('#FE6802')
//                                                 : Hexcolor('#1E2E45'),
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w600,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     trailing: isExpanded6
//                                         ? Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_up.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#FE6802'),
//                                           )
//                                         : Image.asset(
//                                             'assets/fg_images/10_profile_faq_arrow_down.png',
//                                             width: 19,
//                                             height: 10,
//                                             color: Hexcolor('#B7B7B7'),
//                                           ),
//                                     onExpansionChanged: (bool expanding) =>
//                                         setState(
//                                             () => this.isExpanded6 = expanding),
//                                     children: <Widget>[
//                                       Container(
//                                         margin: EdgeInsets.only(bottom: 13),
//                                         child: Divider(
//                                           color: Hexcolor('#ECECEC'),
//                                           thickness: 1,
//                                         ),
//                                       ),
//                                       Container(
//                                         margin: EdgeInsets.only(
//                                             left: 17, right: 37, bottom: 13),
//                                         child: Text(
//                                           "challenger - ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nadvantor - ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nresident - ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nsenator - ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nco-founder - ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                                           style: TextStyle(
//                                             color: Hexcolor('#6F6F6F'),
//                                             fontSize: 15,
//                                             fontFamily: 'Arial',
//                                             fontWeight: FontWeight.w500,
//                                           ),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(
//                                   color: Hexcolor('#ECECEC'),
//                                   thickness: 1,
//                                 ),
//                               ],
//                             )))),
//               ),
//               // Container(
//               //   margin: EdgeInsets.only(bottom: 25),
//               //   height: 60,
//               //   width: MediaQuery.of(context).size.width - 50,
//               //   child: RaisedButton(
//               //     color: Hexcolor('#FE6802'),
//               //     shape: new RoundedRectangleBorder(
//               //       borderRadius: new BorderRadius.circular(14.0),
//               //     ),
//               //     child: Text("Другой вопрос",
//               //         style: TextStyle(
//               //             fontWeight: FontWeight.w600,
//               //             fontFamily: 'AvenirNextLTPro-Bold',
//               //             color: Colors.white,
//               //             letterSpacing: 1.09,
//               //             fontSize: 17)),
//               //     onPressed: () {
//               //       // Navigator.pushNamed(context, '/5_myBottomBar.dart');
//               //     },
//               //   ),
//               // ),
//             ],
//           ),
//         ),
//         Positioned(
//           top: 25,
//           right: 0,
//           child: Image.asset(
//             'assets/fg_images/10_profile_faq_penguin.png',
//             height: 230,
//           ),
//         ),
//       ],
//     ));
//   }
// }

//-----------------------------------------new FAQ 4.12.2020-------------------------------------------------------------
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';
import '10_profile_faq_json.dart';
// import '10_profile_news_list_array.dart';

class ProfileFaq extends StatefulWidget {
  @override
  _ProfileFaqState createState() => _ProfileFaqState();
}

class _ProfileFaqState extends State<ProfileFaq> {
  List<Publication> posts;
  bool loading;
 
  void _launchUrl(String i) async {
    if (await canLaunch(i)) {
      await launch(i);
    } else {
      throw 'Could not open Url';
    }
  }

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesFaq.getFaq().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(
      dividerColor: Colors.transparent,
    );

    return Scaffold(
        body: loading
            ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Hexcolor('#7D5AC2'),
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Hexcolor('#FE6802'),
                  ),
                ),
              )
            : Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#F2F2F2'),
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0)),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Hexcolor('#7D5AC2'),
                            ),
                            alignment: Alignment.bottomLeft,
                            margin: EdgeInsets.only(
                              top: 50,
                              // left: 15,
                              // right: 15,
                              bottom: 22,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(left: 10, right: 10),
                                  child: SizedBox(
                                    // height: 55,
                                    // width: 40,
                                    child: IconButton(
                                      icon: Image.asset(
                                        'assets/fg_images/6_home_search_back.png',
                                        width: 10,
                                        height: 19,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ),
                                ),
                                Text(
                                  'ЧаПи',
                                  style: TextStyle(
                                      fontSize: 33,
                                      color: Colors.white,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w900),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            physics: BouncingScrollPhysics(),
                            itemCount: posts.length,
                            itemBuilder: (context, index) {
                              // Publication publication = posts[index];
                              // var title = utf8.decode(publication.title.runes.toList());
                              // var content =
                              //     utf8.decode(publication.content.runes.toList());
                              Publication publication = posts[index];

                              var title =
                                  utf8.decode(publication.titleUa.runes.toList());
                              var content = utf8
                                  .decode(publication.contentUa.runes.toList());

                              bool isExpanded = false;
                              
                              return GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                                                  
                                },
                                child: Column(
                                children: [
                                  Theme(
                                    data: theme,
                                    child: ExpansionTile(
                                      title: Row(
                                        children: [
                                          Flexible(
                                            child: Text(
                                              title == null ? '' : title,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: isExpanded
                                                    ? Hexcolor('#FE6802')
                                                    : Hexcolor('#1E2E45'),
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      trailing: isExpanded
                                          ? Image.asset(
                                              'assets/fg_images/10_profile_faq_arrow_up.png',
                                              width: 19,
                                              height: 10,
                                              color: Hexcolor('#FE6802'),
                                            )
                                          : Image.asset(
                                              'assets/fg_images/10_profile_faq_arrow_down.png',
                                              width: 19,
                                              height: 10,
                                              color: Hexcolor('#B7B7B7'),
                                            ),
                                      onExpansionChanged: (bool expanding) =>
                                          setState(() {
                                        isExpanded = expanding;
                                      }),
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(bottom: 13),
                                          child: Divider(
                                            color: Hexcolor('#ECECEC'),
                                            thickness: 1,
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  left: 17,
                                                  right: 37,
                                                  bottom: 13),
                                              child: Html(
                                                data: content == null
                                                    ? ''
                                                    : content,
                                                onLinkTap: (i) {
                                                  _launchUrl(i);
                                                },
                                              )
                                              // Text(
                                              //   content == null ? '' : content,
                                              //   // content == null ? '' : content,
                                              //   style: TextStyle(
                                              //     color: Hexcolor('#6F6F6F'),
                                              //     fontSize: 15,
                                              //     fontFamily: 'Arial',
                                              //     fontWeight: FontWeight.w500,
                                              //   ),
                                              // ),
                                              ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Hexcolor('#ECECEC'),
                                    thickness: 1,
                                  ),
                                ],
                              )
                              );
                              
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 25,
                    right: 0,
                    child: Image.network(
                      'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-30_10_profile_faq_penguin.png',
                      height: 230,
                    ),
                  ),
                ],
              ));
  }
}
