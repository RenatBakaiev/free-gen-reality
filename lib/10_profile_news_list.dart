// import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '10_profile_news_list_array.dart';
// import 'package:http/http.dart' as http;
import 'package:flutter_free_gen_reality/services/services.dart';
import 'dart:convert' show utf8;

class ProfileNewsList extends StatefulWidget {
  @override
  _ProfileNewsListState createState() => _ProfileNewsListState();
}

class _ProfileNewsListState extends State<ProfileNewsList> {
  List<Publication>
      posts; // https://www.youtube.com/watch?v=WOILEQTZtgY&t=326s - tutorial
  bool loading;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  // List<String> listNews = new List();

  // fetch() async {
  //   final response = await http.get('https://jsonplaceholder.typicode.com/users');
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       listNews.add(jsonDecode(response.body)['name']);
  //     });
  //   } else {
  //     throw Exception('Failed to load data...');
  //   }
  // }

  @override
  void initState() {
    super.initState();
    loading = true;
    Services.getNews().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Text(
                            'Новини',
                            style: TextStyle(
                                fontSize: 37,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: posts.length,
                      itemBuilder: (context, index) {
                        // print(posts.length);
                        Publication publication = posts[index];

                        DateTime time = DateTime.parse(
                            publication.publicationDate.toString());
                        String publicationDate =
                            // "${time.year.toString()}-${time.month.toString().padLeft(2, '0')}-${time.day.toString().padLeft(2, '0')} ${time.hour.toString()}:${time.minute.toString()}:${time.second.toString()}";
                            "${time.day.toString().padLeft(2, '0')}-${time.month.toString().padLeft(2, '0')}-${time.year.toString()}";

                        var title =
                            utf8.decode(publication.titleUa.runes.toList());
                        var contentShort = utf8
                            .decode(publication.contentShortUa.runes.toList());
                        var content =
                            utf8.decode(publication.contentUa.runes.toList());

                        return GestureDetector(
                          onTap: () {
                            // Navigator.pushNamed(context, listOfNews[index].link);
                            Navigator.pushNamed(
                                context, '/10_profile_news.dart',
                                arguments: ({
                                  // 'image': post.thumbnailUrl,
                                  'image': publication.img == "" ||
                                          publication.img == null
                                      ? '$link' + 'default.png'
                                      : '$link' + '${publication.img}',
                                  'title': title,
                                  'time': publicationDate,
                                  'text': content,
                                }));
                          },
                          child: Container(
                              // color: Colors.blue,
                              height: 326,
                              width: MediaQuery.of(context).size.width - 30,
                              margin: EdgeInsets.only(
                                  bottom: 20, left: 15, right: 15),
                              // decoration: BoxDecoration(
                              //   borderRadius: BorderRadius.all(Radius.circular(10)),
                              // ),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        // top: 20,
                                        // left: 15,
                                        // right: 15,
                                        bottom: 10,
                                      ),
                                      height: 193,
                                      width: MediaQuery.of(context).size.width,
                                      child: Image.network(
                                          publication.img == "" ||
                                                  publication.img == null
                                              ? '$link' + 'default.png'
                                              : '$link' + '${publication.img}',
                                          fit: BoxFit.cover),
                                      // child: Image.network('https://picsum.photos/250?image=9', fit: BoxFit.cover),
                                      // child: Image.network(
                                      //     publication.img == null
                                      //         ? 'https://picsum.photos/250?image=9'
                                      //         : publication.img,
                                      //     fit: BoxFit.cover),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 20,
                                        right: 20,
                                      ),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(
                                                  bottom: 10,
                                                ),
                                                child: Text(
                                                  title == null ? '' : title,
                                                  style: TextStyle(
                                                      color:
                                                          Hexcolor('#484848'),
                                                      fontSize: 18,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                  bottom: 10,
                                                ),
                                                child: Text(
                                                  publicationDate == null
                                                      ? ''
                                                      : publicationDate,
                                                  // publication.publicationDate.toString(),
                                                  style: TextStyle(
                                                    color: Hexcolor('#B9BCC4'),
                                                    fontSize: 14,
                                                    fontFamily: 'Arial',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                contentShort == null
                                                    ? ''
                                                    : contentShort,
                                                // publication.contentShort.toString() == null
                                                //     ? ''
                                                //     : publication.contentShort.toString(),
                                                // overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  color: Hexcolor('#484848'),
                                                  fontSize: 16,
                                                  fontFamily: 'Arial',
                                                ),
                                              ),
                                            ]),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        );
                      },
                    ),
                  ),

                  // Container(
                  //   margin: EdgeInsets.only(
                  //     top: 20,
                  //     left: 15,
                  //     right: 15,
                  //     bottom: 17,
                  //   ),
                  //   height: 193,
                  //   decoration: BoxDecoration(
                  //       borderRadius: BorderRadius.all(Radius.circular(10)),
                  //       image: DecorationImage(
                  //         image: AssetImage(
                  //             'assets/fg_images/10_profile_news_bunner.jpg'),
                  //         fit: BoxFit.cover,
                  //       )),
                  // ),
                  // Container(
                  //   child: Expanded(
                  //       child: SingleChildScrollView(
                  //           physics: BouncingScrollPhysics(),
                  //           child: Container(
                  //             margin: EdgeInsets.only(
                  //               left: 30,
                  //               right: 30,
                  //             ),
                  //             child: Text(
                  //               'Описание\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                  //               style: TextStyle(
                  //                 color: Hexcolor('#484848'),
                  //                 fontSize: 16,
                  //                 fontFamily: 'AvenirNextLTPro-Regular',
                  //               ),
                  //             ),
                  //           )
                  //           )
                  //           ),
                  // ),
                ],
              ),
            ),
    );
  }
}
