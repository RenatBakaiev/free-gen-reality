import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/6_home_quests.dart';
import 'package:flutter_free_gen_reality/8_camera_preview.dart';
import 'package:flutter_free_gen_reality/run_map_quest_desc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:model_viewer/model_viewer.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/rendering.dart';

class MissionScanQR extends StatefulWidget {
  @override
  _MissionScanQRState createState() => _MissionScanQRState();
}

class _MissionScanQRState extends State {
  CameraController controller;
  List cameras;
  int selectedCameraIndex;
  String imgPath;
  bool isListHidden = false;
  bool isImageHidden = false;

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      cameras = availableCameras;

      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        _initCameraController(cameras[selectedCameraIndex]).then((void v) {});
      } else {
        print('No camera available');
      }
    }).catchError((err) {
      print('Error :${err.code}Error message : ${err.message}');
    });
    // _requestPermission();
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.high);

    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (controller.value.hasError) {
        print('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: _cameraPreviewWidget(),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              height: 90,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(20),
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  // _openGallery(context),
                  // SizedBox(
                  //   width: 20,
                  // ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isListHidden = !isListHidden;
                      });
                    },
                    child: SizedBox(
                      width: 34,
                      child: Image.asset(
                        'assets/fg_images/8_camera_icon_ar.png',
                        height: 34,
                        width: 34,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  // Spacer(),
                  _cameraControlWidget(context),
                  // SizedBox(
                  //   width: 33,
                  // ),
                  // Spacer(),
                  _openQrCode(context),
                  _cameraToggleRowWidget(),
                ],
              ),
            ),
          ),

          // for emulator

          Positioned(
            bottom: 70,
            child: isListHidden
                ? Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: picsForCamera.length,
                        itemBuilder: (context, index) {
                          showImage() {
                            showDialog(
                                // barrierDismissible: false,
                                barrierColor: Colors.white.withOpacity(0.0),
                                context: context,
                                builder: (context) {
                                  // var width = MediaQuery.of(context).size.width;
                                  return Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      Positioned(
                                        top:
                                            MediaQuery.of(context).size.height /
                                                    2 -
                                                150,
                                        left:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                100,
                                        child: InteractiveViewer(
                                          minScale: 0.5,
                                          boundaryMargin: EdgeInsets.all(double
                                              .infinity), // no limits on screen
                                          child: Image.asset(
                                            // 'assets/fg_images/4_enter_logo.png',
                                            picsForCamera[index].image,
                                            height: 300,
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }

                          isImageHidden
                              ? Stack(
                                  overflow: Overflow.visible,
                                  children: [
                                    Positioned(
                                      top: MediaQuery.of(context).size.height /
                                              2 -
                                          150,
                                      left: MediaQuery.of(context).size.width /
                                              2 -
                                          100,
                                      child: InteractiveViewer(
                                        minScale: 0.5,
                                        boundaryMargin: EdgeInsets.all(double
                                            .infinity), // no limits on screen
                                        child: Image.asset(
                                          // 'assets/fg_images/4_enter_logo.png',
                                          picsForCamera[index].image,
                                          height: 300,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              : Container();
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                showImage();
                                // isImageHidden = true;
                                isListHidden = false;
                              });
                            },
                            child: Image.asset(
                              picsForCamera[index].image,
                            ),
                          );
                        }),
                  )
                : Container(),
          ),

          isImageHidden
              ? Positioned(
                  top: MediaQuery.of(context).size.height / 2 - 150,
                  left: MediaQuery.of(context).size.width / 2 - 100,
                  child: InteractiveViewer(
                    minScale: 0.5,
                    boundaryMargin:
                        EdgeInsets.all(double.infinity), // no limits on screen
                    child: Image.asset(
                      // 'assets/fg_images/4_enter_logo.png',
                      'assets/fg_images/0_splash_screen_lion_and_penguin.gif',
                      height: 300,
                    ),
                  ),
                )
              : Container(),

          // for device

          // Positioned(
          //   bottom: 70,
          //   child: isListHidden
          //       ? Container(
          //           height: 300,
          //           width: MediaQuery.of(context).size.width,
          //           child: ListView.builder(
          //               physics: BouncingScrollPhysics(),
          //               scrollDirection: Axis.horizontal,
          //               itemCount: arForCamera.length,
          //               itemBuilder: (context, index) {
          //                 return Container(
          //                   margin: EdgeInsets.only(
          //                     right: 20,
          //                   ),
          //                   height: 300,
          //                   width: 100,
          //                   child: ModelViewer(
          //                     backgroundColor: Colors.white.withOpacity(0),
          //                     src: arForCamera[index].image,
          //                     ar: true,
          //                     autoRotate: false,
          //                     cameraControls: true,
          //                   ),
          //                 );
          //               }),
          //         )
          //       : Container(),
          // ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                // Navigator.of(context).pop();
                Navigator.pushNamed(context, '/5_myBottomBar.dart');
              },
            ),
          ),
        ],
      ),
    );
  }

  /// Display Camera preview.
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Loading',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }

    return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: CameraPreview(controller),
    );
  }

  /// Display the control bar with buttons to take pictures
  Widget _cameraControlWidget(context) {
    return FloatingActionButton(
      child: Image.asset(
        'assets/fg_images/8_camera_icon_make_pic.png',
        width: 90,
        height: 90,
      ),
      backgroundColor: Colors.transparent,
      onPressed: () {
        _onCapturePressed(context);
        // _saveScreen();
        // loadPrefs();
      },
    );
  }

  Widget _openQrCode(context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ScanQrCode()),
        );
      },
      child: Image.asset(
        'assets/fg_images/8_camera_icon_qr_code.png',
        width: 34,
        height: 34,
        color: Colors.white,
      ),
    );
  }

  File imageFile;
  final picker = ImagePicker();

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraToggleRowWidget() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }
    // CameraDescription selectedCamera = cameras[selectedCameraIndex];
    // CameraLensDirection lensDirection = selectedCamera.lensDirection;

    return GestureDetector(
      onTap: _onSwitchCamera,
      child: Container(
        // margin: EdgeInsets.only(left: 20),
        child: Image.asset(
          'assets/fg_images/8_camera_icon_toggle.png',
          width: 40,
          height: 35,
        ),
      ),
    );
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error:${e.code}\nError message : ${e.description}';
    print(errorText);
  }

  void _onCapturePressed(context) async {
    try {
      final path =
          join((await getTemporaryDirectory()).path, '${DateTime.now()}.png');
      await controller.takePicture(path);

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PreviewScreen(
                  imgPath: path,
                )),
      );
    } catch (e) {
      _showCameraException(e);
    }
  }

  void _onSwitchCamera() {
    selectedCameraIndex =
        selectedCameraIndex < cameras.length - 1 ? selectedCameraIndex + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    _initCameraController(selectedCamera);
  }
}

//https://www.youtube.com/watch?v=kgqiMzjAyZo&t=306s - tutorial
//https://www.the-qrcode-generator.com/ - qr generator
class ScanQrCode extends StatefulWidget {
  @override
  _ScanQrCodeState createState() => _ScanQrCodeState();
}

class _ScanQrCodeState extends State<ScanQrCode> {
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  QRViewController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Column(
            children: [
              Expanded(
                flex: 9,
                child: QRView(
                    key: qrKey,
                    overlay: QrScannerOverlayShape(
                      borderRadius: 10,
                      borderColor: Colors.red,
                      borderLength: 30,
                      borderWidth: 10,
                      cutOutSize: 300,
                    ),
                    onQRViewCreated: _onQRViewCreate),
              ),
            ],
          ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          Positioned(
            bottom: 60,
            left: 20,
            child: Container(
              padding: EdgeInsets.only(
                left: 10,
                right: 10,
              ),
              decoration: BoxDecoration(
                color: Hexcolor('#FE6802'),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
              ),
              // color: Hexcolor('#FE6802'),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  qrText == ''
                      ? 'Отсканируйте qr код'
                      : 'Нажмите для отображения',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  if (qrText != '') {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            ScanQRcodeResult(text: '$qrText')));
                  }
                },
              ),
            ),
          ),
          Positioned(
            bottom: 21.5,
            left: MediaQuery.of(context).size.width / 2 - 50,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.pushNamed(context, '/run_map_quest.dart');
              },
              child: Container(
                width: 100,
                child: Center(
                    child: Text(
                  'Пропустить',
                  style: TextStyle(
                    color: Hexcolor('#FFFFFF'),
                    fontFamily: 'Arial',
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                )),
              ),
            ),
          )
        ],
      ),
    );
  }

  //global variables:

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }
}

class ScanQRcodeResult extends StatefulWidget {
  final String text;
  ScanQRcodeResult({Key key, @required this.text}) : super(key: key);

  @override
  _ScanQRcodeResultState createState() => _ScanQRcodeResultState();
}

class _ScanQRcodeResultState extends State<ScanQRcodeResult> {
  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: this.context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        // start progress line
                        margin: EdgeInsets.only(top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                Image.asset(
                                    'assets/fg_images/next_step_icon_start.png',
                                    width: 12,
                                    height: 12),
                                Positioned(
                                  bottom: 9,
                                  left: 4.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_start_flag.png',
                                          width: 18,
                                          height: 21),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 8),
                                        child: Text(
                                          'ул. Владимирская, 24а',
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 12,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#FE6802')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_done_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#FE6802')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_done_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#FE6802')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_done_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#1E2E45')),
                            ),
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                Image.asset(
                                    'assets/fg_images/next_step_icon_notdone_step.png',
                                    width: 12,
                                    height: 12),
                                Positioned(
                                  top: 9,
                                  right: 4.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(right: 10, top: 8),
                                        child: Text(
                                          'ул. Богдана Хмельницкого, 13/2',
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 12,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_finish_flag.png',
                                          width: 18,
                                          height: 21),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            ),
                        child: Text(
                          'Вы молодец, прошли шаг миссии.\nНажмите кнопку для продолжения',
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на cледующий шаг",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        RunMapQuestDesc()));
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
                Positioned(
                  right: 40,
                  top: 50,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '3',
                          style: TextStyle(
                            fontSize: 36,
                            color: Hexcolor('#1E2E45'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 2),
                          child: Text(
                            ' ' + '/ ' + '3',
                            style: TextStyle(
                              fontSize: 24,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#FFFFFF'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                  left: 5,
                  right: 5,
                  bottom: 17,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          // margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Сканирование',
                          style: TextStyle(
                              fontSize: 32,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/run_map_quest_pic_gostart.png',
                            width: 22,
                            height: 24,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              '/5_myBottomBar.dart',
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 195,
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(children: [
                    Container(
                      margin: EdgeInsets.only(top: 120, bottom: 80),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/fg_images/main_icon.png',
                            width: 40,
                            height: 34,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Image.asset(
                            'assets/fg_images/6_home_sidebar_title_image.png',
                            width: 160,
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Информация об артефакте',
                            style: TextStyle(
                              fontSize: 18,
                              color: Hexcolor('#545454'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {},
                            child: Text(
                              widget.text,
                              style: TextStyle(
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ])),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Продолжить",
                    style: TextStyle(
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  Navigator.pushNamed(context, '/quest_done.dart');
                  // showInfoWindow();

                  // Navigator.pushReplacement(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (BuildContext context) =>
                  //             RunMapQuestDesc()));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
