import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MissionMuromecMap extends StatefulWidget {
  @override
  _MissionMuromecMapState createState() => _MissionMuromecMapState();
}

class _MissionMuromecMapState extends State<MissionMuromecMap> {
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  Stopwatch watch = Stopwatch();
  Timer timer;
  String elapsedTime = '';
  int elapsedtimeFromDarabase = 0;

  getUserData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    print(email);
    setState(() {
      userEmail = email;
    });
    print(token);
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';
    print(url);

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
        print(response.body);

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        print(responseJson);

        email = responseJson["email"];
        print(email);

        setState(() {
          if (responseJson["timer"] != null) {
            elapsedtimeFromDarabase = responseJson["timer"];
          }
          isLoading = false;
        });
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  Future<User> updateTimer() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    String url = 'http://generation-admin.ehub.com.ua/api/user/update';
    print(id);
    print(url);
    print(token);

    final http.Response response = await http.put(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "id": id,
        "timer": watch.elapsedMilliseconds + elapsedtimeFromDarabase,
      }),
    );
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      print('Timer updated');
      setState(() {
        isLoading = false;
      });
      // print(response.body);
    } else {
      setState(() {
        isLoading = false;
      });
      throw Exception('Failed to update timer.');
    }
  }

  updateTime(Timer timer) {
    print(elapsedtimeFromDarabase);
    if (watch.isRunning) {
      setState(() {
        elapsedTime = transformMilliSeconds(
            watch.elapsedMilliseconds + elapsedtimeFromDarabase);
      });
    }
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    // String millisecondsStr = (milliseconds % 60).toString().padLeft(2, '0');
    // return "$minutesStr:$secondsStr:$millisecondsStr";
    return "$hoursStr:$minutesStr:$secondsStr";
  }

  startWatch() {
    setState(() {
      watch.start();
      timer = Timer.periodic(Duration(milliseconds: 100), updateTime);
    });
  }

  stopWatch() {
    setState(() {
      watch.stop();
      print(watch.elapsedMilliseconds); // its give to database
      // watch.reset();
    });
  }

  int distanceInMeters1;
  int distanceInMeters2;
  int distanceInMeters3;
  int distanceInMeters4;
  int distanceInMeters5;
  int distanceInMeters6;
  int distanceInMeters7;
  int distanceInMeters8;
  int distanceInMeters9;
  int distanceInMeters10;
  int distanceInMeters11;
  int distanceInMeters12;
  int distanceInMeters13;
  int distanceInMeters14;

  var currentLocation;
  bool mapToggle = false;
  double zoomVal = 17.0;

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _getMyLocation(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  BitmapDescriptor _checkPoint1;
  BitmapDescriptor _checkPoint2;
  BitmapDescriptor _checkPoint3;
  BitmapDescriptor _checkPoint4;
  BitmapDescriptor _checkPoint5;
  BitmapDescriptor _checkPoint6;
  BitmapDescriptor _checkPoint7;
  BitmapDescriptor _checkPoint8;
  BitmapDescriptor _checkPoint9;
  BitmapDescriptor _checkPoint10;
  BitmapDescriptor _checkPoint11;
  BitmapDescriptor _checkPoint12;
  BitmapDescriptor _checkPoint13;
  BitmapDescriptor _checkPoint14;
  Set<Marker> _markers = HashSet<Marker>();

  // double _destLatitude = 50.450631, _destLongitude = 30.510639; // test
  double _destLatitude = 50.49676, _destLongitude = 30.54246; // muromec
  double _destLatitude2 = 50.50081, _destLongitude2 = 30.54259;
  double _destLatitude3 = 50.50303, _destLongitude3 = 30.53895;
  double _destLatitude4 = 50.50447, _destLongitude4 = 30.53463;
  double _destLatitude5 = 50.51068, _destLongitude5 = 30.54137;
  double _destLatitude6 = 50.51506, _destLongitude6 = 30.53985;
  double _destLatitude7 = 50.52254, _destLongitude7 = 30.54019;
  double _destLatitude8 = 50.5204, _destLongitude8 = 30.55877;
  double _destLatitude9 = 50.51417, _destLongitude9 = 30.56225;
  double _destLatitude10 = 50.51214, _destLongitude10 = 30.55427;
  double _destLatitude11 = 50.50963, _destLongitude11 = 30.54964;
  double _destLatitude12 = 50.51098, _destLongitude12 = 30.54363;
  double _destLatitude13 = 50.49773, _destLongitude13 = 30.54957;
  double _destLatitude14 = 50.49653, _destLongitude14 = 30.5473;

  Map<PolylineId, Polyline> polylines = {};

  List<LatLng> polylineCoordinates = [];
  List<LatLng> polylineCoordinates2 = [];
  List<LatLng> polylineCoordinates3 = [];
  List<LatLng> polylineCoordinates4 = [];
  List<LatLng> polylineCoordinates5 = [];
  List<LatLng> polylineCoordinates6 = [];
  List<LatLng> polylineCoordinates7 = [];
  List<LatLng> polylineCoordinates8 = [];
  List<LatLng> polylineCoordinates9 = [];
  List<LatLng> polylineCoordinates10 = [];
  List<LatLng> polylineCoordinates11 = [];
  List<LatLng> polylineCoordinates12 = [];
  List<LatLng> polylineCoordinates13 = [];
  List<LatLng> polylineCoordinates14 = [];

  PolylinePoints polylinePoints = PolylinePoints();
  PolylinePoints polylinePoints2 = PolylinePoints();
  PolylinePoints polylinePoints3 = PolylinePoints();
  PolylinePoints polylinePoints4 = PolylinePoints();
  PolylinePoints polylinePoints5 = PolylinePoints();
  PolylinePoints polylinePoints6 = PolylinePoints();
  PolylinePoints polylinePoints7 = PolylinePoints();
  PolylinePoints polylinePoints8 = PolylinePoints();
  PolylinePoints polylinePoints9 = PolylinePoints();
  PolylinePoints polylinePoints10 = PolylinePoints();
  PolylinePoints polylinePoints11 = PolylinePoints();
  PolylinePoints polylinePoints12 = PolylinePoints();
  PolylinePoints polylinePoints13 = PolylinePoints();
  PolylinePoints polylinePoints14 = PolylinePoints();

  Geolocator geolocator = Geolocator();

  bool isLoading = false;
  bool deleteFirstPolyline = false;
  var userEmail;

  bool isStep1Completed = false;
  bool isStep2Completed = false;
  bool isStep3Completed = false;
  bool isStep4Completed = false;
  bool isStep5Completed = false;
  bool isStep6Completed = false;
  bool isStep7Completed = false;
  bool isStep8Completed = false;
  bool isStep9Completed = false;
  bool isStep10Completed = false;
  bool isStep11Completed = false;
  bool isStep12Completed = false;
  bool isStep13Completed = false;
  bool isStep14Completed = false;

  checkPoints() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res1 = sharedPreferences.get('muromecStep1Done');
    var res2 = sharedPreferences.get('muromecStep2Done');
    var res3 = sharedPreferences.get('muromecStep3Done');
    var res4 = sharedPreferences.get('muromecStep4Done');
    var res5 = sharedPreferences.get('muromecStep5Done');
    var res6 = sharedPreferences.get('muromecStep6Done');
    var res7 = sharedPreferences.get('muromecStep7Done');
    var res8 = sharedPreferences.get('muromecStep8Done');
    var res9 = sharedPreferences.get('muromecStep9Done');
    var res10 = sharedPreferences.get('muromecStep10Done');
    var res11 = sharedPreferences.get('muromecStep11Done');
    var res12 = sharedPreferences.get('muromecStep12Done');
    var res13 = sharedPreferences.get('muromecStep13Done');
    var res14 = sharedPreferences.get('muromecStep14Done');
    if (res1 == true) {
      startWatch();
      setState(() {
        checkPoints14[0].isCompleted = true;
        deleteFirstPolyline = true;
        isStep1Completed = true;
        isLoading = false;
      });
    }
    if (res2 == true) {
      setState(() {
        checkPoints14[1].isCompleted = true;
        isStep2Completed = true;
        isLoading = false;
      });
    }
    if (res3 == true) {
      setState(() {
        checkPoints14[2].isCompleted = true;
        isStep3Completed = true;
        isLoading = false;
      });
    }
    if (res4 == true) {
      setState(() {
        checkPoints14[3].isCompleted = true;
        isStep4Completed = true;
        isLoading = false;
      });
    }
    if (res5 == true) {
      setState(() {
        checkPoints14[4].isCompleted = true;
        isStep5Completed = true;
        isLoading = false;
      });
    }
    if (res6 == true) {
      setState(() {
        checkPoints14[5].isCompleted = true;
        isStep6Completed = true;
        isLoading = false;
      });
    }
    if (res7 == true) {
      setState(() {
        checkPoints14[6].isCompleted = true;
        isStep7Completed = true;
        isLoading = false;
      });
    }
    if (res8 == true) {
      setState(() {
        checkPoints14[7].isCompleted = true;
        isStep8Completed = true;
        isLoading = false;
      });
    }
    if (res9 == true) {
      setState(() {
        checkPoints14[8].isCompleted = true;
        isStep9Completed = true;
        isLoading = false;
      });
    }
    if (res10 == true) {
      setState(() {
        checkPoints14[9].isCompleted = true;
        isStep10Completed = true;
        isLoading = false;
      });
    }
    if (res11 == true) {
      setState(() {
        checkPoints14[10].isCompleted = true;
        isStep11Completed = true;
        isLoading = false;
      });
    }
    if (res12 == true) {
      setState(() {
        checkPoints14[11].isCompleted = true;
        isStep12Completed = true;
        isLoading = false;
      });
    }
    if (res13 == true) {
      setState(() {
        checkPoints14[12].isCompleted = true;
        isStep13Completed = true;
        isLoading = false;
      });
    }
    if (res14 == true) {
      setState(() {
        checkPoints14[13].isCompleted = true;
        isStep14Completed = true;
        isLoading = false;
      });
    }
    setState(() {
      isLoading = false;
    });
  }

  showWindowQuizDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдене',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви не можете повторно пройти це завдання',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  checkDone() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var res1 = sharedPreferences.get('muromecStep1Done');
    var res2 = sharedPreferences.get('muromecStep2Done');
    var res3 = sharedPreferences.get('muromecStep3Done');
    var res4 = sharedPreferences.get('muromecStep4Done');
    var res5 = sharedPreferences.get('muromecStep5Done');
    var res6 = sharedPreferences.get('muromecStep6Done');
    var res7 = sharedPreferences.get('muromecStep7Done');
    var res8 = sharedPreferences.get('muromecStep8Done');
    var res9 = sharedPreferences.get('muromecStep9Done');
    var res10 = sharedPreferences.get('muromecStep10Done');
    var res11 = sharedPreferences.get('muromecStep11Done');
    var res12 = sharedPreferences.get('muromecStep12Done');
    var res13 = sharedPreferences.get('muromecStep13Done');
    var res14 = sharedPreferences.get('muromecStep14Done');
    if (res1 == true &&
        res2 == true &&
        res3 == true &&
        res4 == true &&
        res5 == true &&
        res6 == true &&
        res7 == true &&
        res8 == true &&
        res9 == true &&
        res10 == true &&
        res11 == true &&
        res12 == true &&
        res13 == true &&
        res14 == true) {
      _getPositionSubscription.cancel();
      showWindowQuizDone();
      stopWatch();
    }
    circles = Set.from([
      Circle(
        center: LatLng(_destLatitude, _destLongitude),
        circleId: CircleId("1"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true
            ? Hexcolor('#FFFFFF').withOpacity(0)
            : Hexcolor('#028AFE'),
        fillColor: res1 == true
            ? Hexcolor('#FFFFFF').withOpacity(0)
            : Hexcolor('#028AFE').withOpacity(0.25),
      ),
      Circle(
        center: LatLng(_destLatitude2, _destLongitude2),
        circleId: CircleId("2"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true
            // &&
            // res2 == false &&
            // res3 == false &&
            // res4 == false &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true
            // &&
            // res2 == false &&
            // res3 == false &&
            // res4 == false &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude3, _destLongitude3),
        circleId: CircleId("3"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true && res2 == true
            // &&
            // res3 == false &&
            // res4 == false &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true && res2 == true
            // &&
            // res3 == false &&
            // res4 == false &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude4, _destLongitude4),
        circleId: CircleId("4"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true && res2 == true && res3 == true
            // &&
            // res4 == false &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true && res2 == true && res3 == true
            // &&
            // res4 == false &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude5, _destLongitude5),
        circleId: CircleId("5"),
        radius: 15,
        strokeWidth: 1,
        strokeColor:
            res1 == true && res2 == true && res3 == true && res4 == true
                // &&
                // res5 == false &&
                // res6 == false &&
                // res7 == false &&
                // res8 == false &&
                // res9 == false &&
                // res10 == false &&
                // res11 == false &&
                // res12 == false &&
                // res13 == false &&
                // res14 == false
                ? Hexcolor('#028AFE')
                : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true && res2 == true && res3 == true && res4 == true
            // &&
            // res5 == false &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude6, _destLongitude6),
        circleId: CircleId("6"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true
            // &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true
            // &&
            // res6 == false &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude7, _destLongitude7),
        circleId: CircleId("7"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true
            // &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true
            // &&
            // res7 == false &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude8, _destLongitude8),
        circleId: CircleId("8"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true
            // &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true
            // &&
            // res8 == false &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude9, _destLongitude9),
        circleId: CircleId("9"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true
            // &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true
            // &&
            // res9 == false &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude10, _destLongitude10),
        circleId: CircleId("10"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true
            // &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true
            // &&
            // res10 == false &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude11, _destLongitude11),
        circleId: CircleId("11"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true
            // &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true
            // &&
            // res11 == false &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude12, _destLongitude12),
        circleId: CircleId("12"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true &&
                res11 == true
            // &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true &&
                res11 == true
            // &&
            // res12 == false &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude13, _destLongitude13),
        circleId: CircleId("13"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true &&
                res11 == true &&
                res12 == true
            // &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true &&
                res11 == true &&
                res12 == true
            // &&
            // res13 == false &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
      Circle(
        center: LatLng(_destLatitude14, _destLongitude14),
        circleId: CircleId("14"),
        radius: 15,
        strokeWidth: 1,
        strokeColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true &&
                res11 == true &&
                res12 == true &&
                res13 == true
            // &&
            // res14 == false
            ? Hexcolor('#028AFE')
            : Hexcolor('#FFFFFF').withOpacity(0),
        fillColor: res1 == true &&
                res2 == true &&
                res3 == true &&
                res4 == true &&
                res5 == true &&
                res6 == true &&
                res7 == true &&
                res8 == true &&
                res9 == true &&
                res10 == true &&
                res11 == true &&
                res12 == true &&
                res13 == true
            // &&
            // res14 == false
            ? Hexcolor('#028AFE').withOpacity(0.25)
            : Hexcolor('#FFFFFF').withOpacity(0),
      ),
    ]);
  }

  StreamSubscription _getPositionSubscription;

  _getLocation() {
    _getPositionSubscription =
        Geolocator.getPositionStream().listen((currentLocation) {
      distanceInMeters1 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude, _destLongitude))
          .toInt();
      distanceInMeters2 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude2, _destLongitude2))
          .toInt();
      distanceInMeters3 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude3, _destLongitude3))
          .toInt();
      distanceInMeters4 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude4, _destLongitude4))
          .toInt();
      distanceInMeters5 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude5, _destLongitude5))
          .toInt();
      distanceInMeters6 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude6, _destLongitude6))
          .toInt();
      distanceInMeters7 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude7, _destLongitude7))
          .toInt();
      distanceInMeters8 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude8, _destLongitude8))
          .toInt();
      distanceInMeters9 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude9, _destLongitude9))
          .toInt();
      distanceInMeters10 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude10, _destLongitude10))
          .toInt();
      distanceInMeters11 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude11, _destLongitude11))
          .toInt();
      distanceInMeters12 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude12, _destLongitude12))
          .toInt();
      distanceInMeters13 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude13, _destLongitude13))
          .toInt();
      distanceInMeters14 = (Geolocator.distanceBetween(currentLocation.latitude,
              currentLocation.longitude, _destLatitude14, _destLongitude14))
          .toInt();

      print(distanceInMeters1);
      print(distanceInMeters2);
      print(distanceInMeters3);
      print(distanceInMeters4);
      print(distanceInMeters5);
      print(distanceInMeters6);
      print(distanceInMeters7);
      print(distanceInMeters8);
      print(distanceInMeters9);
      print(distanceInMeters10);
      print(distanceInMeters11);
      print(distanceInMeters12);
      print(distanceInMeters13);
      print(distanceInMeters14);

      if (distanceInMeters1 < 15) {
        _getPositionSubscription.cancel();
        if (isStep1Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step1.dart');
        }
      }
      if (distanceInMeters2 < 15) {
        _getPositionSubscription.cancel();
        if (isStep2Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step2.dart');
        }
      }
      if (distanceInMeters3 < 15) {
        _getPositionSubscription.cancel();
        if (isStep3Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step3.dart');
        }
      }
      if (distanceInMeters4 < 15) {
        _getPositionSubscription.cancel();
        if (isStep4Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step4.dart');
        }
      }
      if (distanceInMeters5 < 15) {
        _getPositionSubscription.cancel();
        if (isStep5Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step5.dart');
        }
      }
      if (distanceInMeters6 < 15) {
        _getPositionSubscription.cancel();
        if (isStep6Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step6.dart');
        }
      }
      if (distanceInMeters7 < 15) {
        _getPositionSubscription.cancel();
        if (isStep7Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step7.dart');
        }
      }
      if (distanceInMeters8 < 15) {
        _getPositionSubscription.cancel();
        if (isStep8Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step8.dart');
        }
      }
      if (distanceInMeters9 < 15) {
        _getPositionSubscription.cancel();
        if (isStep9Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step9.dart');
        }
      }
      if (distanceInMeters10 < 15) {
        _getPositionSubscription.cancel();
        if (isStep10Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step10.dart');
        }
      }
      if (distanceInMeters11 < 15) {
        _getPositionSubscription.cancel();
        if (isStep11Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step11.dart');
        }
      }
      if (distanceInMeters12 < 15) {
        _getPositionSubscription.cancel();
        if (isStep12Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step12.dart');
        }
      }
      if (distanceInMeters13 < 15) {
        _getPositionSubscription.cancel();
        if (isStep13Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step13.dart');
        }
      }
      if (distanceInMeters14 < 15) {
        _getPositionSubscription.cancel();
        if (isStep14Completed == true) {
          showWindowPointDone();
        } else {
          stopWatch();
          updateTimer();
          Navigator.pushNamed(context, '/mission_muromec_step14.dart');
        }
      }
    });
  }

  @override
  void initState() {
    getUserData();
    // checkDone();
    checkPoints();
    checkDone();

    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        mapToggle = true;
      });
    });

    _getLocation();

    // Geolocator.getPositionStream().listen((currentLocation) {
    //   distanceInMeters1 = (Geolocator.distanceBetween(currentLocation.latitude,
    //           currentLocation.longitude, _destLatitude, _destLongitude))
    //       .toInt();
    //   print(distanceInMeters1);
    //   if (distanceInMeters1 < 5650) {
    //     // Geolocator.getPositionStream().listen((currentLocation) {}).cancel();
    //     if (isStep13Completed == true) {
    //       showWindowPointDone();
    //     } else {
    //       Navigator.pushNamed(context, '/mission_muromec_step13.dart');

    //     }
    //   } else {
    //     print("point 1 not found");
    //   }
    // });

    super.initState();
    _setCheckPoint1();
    _setCheckPoint2();
    _setCheckPoint3();
    _setCheckPoint4();
    _setCheckPoint5();
    _setCheckPoint6();
    _setCheckPoint7();
    _setCheckPoint8();
    _setCheckPoint9();
    _setCheckPoint10();
    _setCheckPoint11();
    _setCheckPoint12();
    _setCheckPoint13();
    _setCheckPoint14();

    // _getPolyline2();
    // _getPolyline3();
    // _getPolyline4();
    // _getPolyline5();
    // _getPolyline6();
    // _getPolyline7();
    // _getPolyline8();
    // _getPolyline9();
    // _getPolyline10();
    // _getPolyline11();
    // _getPolyline12();
    // _getPolyline13();
    // _getPolyline14();
  }

  @override
  void dispose() {
    _getPositionSubscription.cancel();
    super.dispose();
  }
  // void _setCheckPoint1() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res1 = sharedPreferences.get('muromecStep1Done');
  //   if (res1 == true) {
  //     _checkPoint1 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step1_done.png');
  //   } else {
  //     _checkPoint1 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step1_start.png');
  //   }
  // }

  // void _setCheckPoint2() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res2 = sharedPreferences.get('muromecStep2Done');
  //   if (res2 == true) {
  //     _checkPoint2 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step2_done.png');
  //   } else {
  //     _checkPoint2 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step2_start.png');
  //   }
  // }

  // void _setCheckPoint3() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res3 = sharedPreferences.get('muromecStep3Done');
  //   if (res3 == true) {
  //     _checkPoint3 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step3_done.png');
  //   } else {
  //     _checkPoint3 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step3_start.png');
  //   }
  // }

  // void _setCheckPoint4() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res4 = sharedPreferences.get('muromecStep4Done');
  //   if (res4 == true) {
  //     _checkPoint4 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step4_done.png');
  //   } else {
  //     _checkPoint4 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step4_start.png');
  //   }
  // }

  // void _setCheckPoint5() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res5 = sharedPreferences.get('muromecStep5Done');
  //   if (res5 == true) {
  //     _checkPoint5 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step5_done.png');
  //   } else {
  //     _checkPoint5 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step5_start.png');
  //   }
  // }

  // void _setCheckPoint6() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res6 = sharedPreferences.get('muromecStep6Done');
  //   if (res6 == true) {
  //     _checkPoint6 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step6_done.pngg');
  //   } else {
  //     _checkPoint6 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step6_start.png');
  //   }
  // }

  // void _setCheckPoint7() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res7 = sharedPreferences.get('muromecStep7Done');
  //   if (res7 == true) {
  //     _checkPoint7 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step7_done.png');
  //   } else {
  //     _checkPoint7 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step7_start.png');
  //   }
  // }

  // void _setCheckPoint8() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res8 = sharedPreferences.get('muromecStep8Done');
  //   if (res8 == true) {
  //     _checkPoint8 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step8_done.png');
  //   } else {
  //     _checkPoint8 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step8_start.png');
  //   }
  // }

  // void _setCheckPoint9() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res9 = sharedPreferences.get('muromecStep9Done');
  //   if (res9 == true) {
  //     _checkPoint9 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step9_done.png');
  //   } else {
  //     _checkPoint9 = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step9_start.png');
  //   }
  // }

  // void _setCheckPoint10() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res10 = sharedPreferences.get('muromecStep10Done');
  //   if (res10 == true) {
  //     _checkPoint10 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step10_done.png');
  //   } else {
  //     _checkPoint10 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step10_start.png');
  //   }
  // }

  // void _setCheckPoint11() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res11 = sharedPreferences.get('muromecStep11Done');
  //   if (res11 == true) {
  //     _checkPoint11 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step11_done.png');
  //   } else {
  //     _checkPoint11 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step11_start.png');
  //   }
  // }

  // void _setCheckPoint12() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res12 = sharedPreferences.get('muromecStep12Done');
  //   if (res12 == true) {
  //     _checkPoint12 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step12_done.png');
  //   } else {
  //     _checkPoint12 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step12_start.png');
  //   }
  // }

  // void _setCheckPoint13() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res13 = sharedPreferences.get('muromecStep13Done');
  //   if (res13 == true) {
  //     _checkPoint13 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step13_done.png');
  //   } else {
  //     _checkPoint13 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step13_start.png');
  //   }
  // }

  // void _setCheckPoint14() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var res14 = sharedPreferences.get('muromecStep14Done');
  //   if (res14 == true) {
  //     _checkPoint14 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step14_done.png');
  //   } else {
  //     _checkPoint14 = await BitmapDescriptor.fromAssetImage(
  //         ImageConfiguration(),
  //         'assets/fg_images/mission_muromec_step14_start.png');
  //   }
  // }

  void _setCheckPoint1() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res1 = sharedPreferences.get('muromecStep1Done');
    if (res1 == true) {
      _checkPoint1 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint1 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint2() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res2 = sharedPreferences.get('muromecStep2Done');
    if (res2 == true) {
      _checkPoint2 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint2 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint3() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res3 = sharedPreferences.get('muromecStep3Done');
    if (res3 == true) {
      _checkPoint3 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint3 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint4() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res4 = sharedPreferences.get('muromecStep4Done');
    if (res4 == true) {
      _checkPoint4 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint4 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint5() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res5 = sharedPreferences.get('muromecStep5Done');
    if (res5 == true) {
      _checkPoint5 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint5 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint6() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res6 = sharedPreferences.get('muromecStep6Done');
    if (res6 == true) {
      _checkPoint6 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint6 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint7() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res7 = sharedPreferences.get('muromecStep7Done');
    if (res7 == true) {
      _checkPoint7 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint7 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint8() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res8 = sharedPreferences.get('muromecStep8Done');
    if (res8 == true) {
      _checkPoint8 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint8 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint9() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res9 = sharedPreferences.get('muromecStep9Done');
    if (res9 == true) {
      _checkPoint9 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint9 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint10() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res10 = sharedPreferences.get('muromecStep10Done');
    if (res10 == true) {
      _checkPoint10 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint10 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint11() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res11 = sharedPreferences.get('muromecStep11Done');
    if (res11 == true) {
      _checkPoint11 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint11 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint12() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res12 = sharedPreferences.get('muromecStep12Done');
    if (res12 == true) {
      _checkPoint12 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint12 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint13() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res13 = sharedPreferences.get('muromecStep13Done');
    if (res13 == true) {
      _checkPoint13 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint13 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  void _setCheckPoint14() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var res14 = sharedPreferences.get('muromecStep14Done');
    if (res14 == true) {
      _checkPoint14 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    } else {
      _checkPoint14 = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    }
  }

  showInfoWindowCheckTrue() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я на місці',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showInfoWindowCheckFalse() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Ти не на місці',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Спробуй підійти до ялинки ближче\nі ще раз зачекінитись',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "withdraw 5 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      print(response.body);
      print('-5TUS done');
    } else {
      throw Exception('Failed to update User.');
    }
  }

  showWindowPointDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Точка вже пройдена',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви вже проходили цю точку раніше',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, '/mission_muromec_map.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  // Set<Circle> circles = Set.from([
  //   Circle(
  //     circleId: CircleId("1"),
  //     center: LatLng(50.49676, 30.54246),
  //     radius: 15,
  //     strokeWidth: 2,
  //     strokeColor: Hexcolor('#FE6802'),
  //   ),
  // ]);

  Set<Circle> circles;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              child: Stack(overflow: Overflow.visible, children: [
                Container(
                  margin: EdgeInsets.only(top: 109),
                  child: mapToggle
                      ? GoogleMap(
                          circles: circles,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(currentLocation.latitude,
                                  currentLocation.longitude),
                              zoom: 17.0),
                          mapType: MapType.normal,
                          tiltGesturesEnabled: true,
                          compassEnabled: true,
                          scrollGesturesEnabled: true,
                          zoomGesturesEnabled: true,
                          zoomControlsEnabled: false, // off default buttons
                          myLocationEnabled: true,
                          myLocationButtonEnabled:
                              false, // off default my location
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);

                            _addPolyLine() {
                              PolylineId id = PolylineId("meToStart");
                              Polyline polyline = Polyline(
                                  patterns: <PatternItem>[
                                    PatternItem.dot,
                                    PatternItem.gap(50)
                                  ],
                                  polylineId: id,
                                  color: Hexcolor('#FE6802'),
                                  points: polylineCoordinates,
                                  width: 10);
                              polylines[id] = polyline;
                              setState(() {});
                            }

                            _getPolyline() async {
                              PolylineResult result = await polylinePoints
                                  .getRouteBetweenCoordinates(
                                "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
                                PointLatLng(currentLocation.latitude,
                                    currentLocation.longitude),
                                PointLatLng(_destLatitude, _destLongitude),
                                travelMode: TravelMode.walking,
                                // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
                              );
                              if (result.points.isNotEmpty) {
                                result.points.forEach((PointLatLng point0) {
                                  polylineCoordinates.add(LatLng(
                                      point0.latitude, point0.longitude));
                                });
                              }
                              _addPolyLine();
                            }

                            if (deleteFirstPolyline == false) {
                              _getPolyline();
                            }

                            setState(() {
                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("1"),
                                position: LatLng(_destLatitude, _destLongitude),
                                icon: _checkPoint1,
                                onTap: () {
                                  // if (isStep1Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   // startWatch();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step1.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("2"),
                                position:
                                    LatLng(_destLatitude2, _destLongitude2),
                                icon: _checkPoint2,
                                onTap: () {
                                  // if (isStep2Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step2.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("3"),
                                position:
                                    LatLng(_destLatitude3, _destLongitude3),
                                icon: _checkPoint3,
                                onTap: () {
                                  // if (isStep3Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step3.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("4"),
                                position:
                                    LatLng(_destLatitude4, _destLongitude4),
                                icon: _checkPoint4,
                                onTap: () {
                                  // if (isStep4Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step4.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("5"),
                                position:
                                    LatLng(_destLatitude5, _destLongitude5),
                                icon: _checkPoint5,
                                onTap: () {
                                  // if (isStep5Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step5.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("6"),
                                position:
                                    LatLng(_destLatitude6, _destLongitude6),
                                icon: _checkPoint6,
                                onTap: () {
                                  // if (isStep6Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step6.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("7"),
                                position:
                                    LatLng(_destLatitude7, _destLongitude7),
                                icon: _checkPoint7,
                                onTap: () {
                                  // if (isStep7Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step7.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("8"),
                                position:
                                    LatLng(_destLatitude8, _destLongitude8),
                                icon: _checkPoint8,
                                onTap: () {
                                  // if (isStep8Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step8.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("9"),
                                position:
                                    LatLng(_destLatitude9, _destLongitude9),
                                icon: _checkPoint9,
                                onTap: () {
                                  // if (isStep9Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(
                                  //       context, '/mission_muromec_step9.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("10"),
                                position:
                                    LatLng(_destLatitude10, _destLongitude10),
                                icon: _checkPoint10,
                                onTap: () {
                                  // if (isStep10Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(context,
                                  //       '/mission_muromec_step10.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("11"),
                                position:
                                    LatLng(_destLatitude11, _destLongitude11),
                                icon: _checkPoint11,
                                onTap: () {
                                  // if (isStep11Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(context,
                                  //       '/mission_muromec_step11.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("12"),
                                position:
                                    LatLng(_destLatitude12, _destLongitude12),
                                icon: _checkPoint12,
                                onTap: () {
                                  // if (isStep12Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(context,
                                  //       '/mission_muromec_step12.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("13"),
                                position:
                                    LatLng(_destLatitude13, _destLongitude13),
                                icon: _checkPoint13,
                                onTap: () {
                                  // if (isStep13Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(context,
                                  //       '/mission_muromec_step13.dart');
                                  // }
                                },
                              ));

                              _markers.add(Marker(
                                anchor: const Offset(0.5, 0.5),
                                markerId: MarkerId("14"),
                                position:
                                    LatLng(_destLatitude14, _destLongitude14),
                                icon: _checkPoint14,
                                onTap: () {
                                  // if (isStep14Completed == true) {
                                  //   showWindowPointDone();
                                  // } else {
                                  //   stopWatch();
                                  //   updateTimer();
                                  //   Navigator.pushNamed(context,
                                  //       '/mission_muromec_step14.dart');
                                  // }
                                },
                              ));
                            });
                          },
                          polylines: Set<Polyline>.of(polylines.values),
                          markers: _markers,
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Hexcolor('#7D5AC2'),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              Hexcolor('#FE6802'),
                            ),
                          ),
                        ),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 5, right: 5),
                                child: SizedBox(
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/6_home_search_back.png',
                                      width: 10,
                                      height: 19,
                                    ),
                                    onPressed: () {
                                      _getPositionSubscription.cancel();
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  // startWatch();
                                },
                                child: Text(
                                  'Муромець race',
                                  style: TextStyle(
                                      fontSize: 24,
                                      color: Colors.white,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w900),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 5),
                            child: SizedBox(
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/run_map_quest_pic_gostart.png',
                                  width: 22,
                                  height: 24,
                                ),
                                onPressed: () {
                                  // stopWatch();
                                  _getPositionSubscription.cancel();
                                  Navigator.pushNamed(
                                      context, '/mission_muromec_desc.dart');
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 140,
                  right: 20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // GestureDetector(
                      //     onTap: () {
                      //       Navigator.pushNamed(
                      //           context, '/missions_12_12_trees.dart');
                      //     },
                      //     child: Image.asset(
                      //       'assets/fg_images/run_map_quest_pic_update.png',
                      //       width: 50,
                      //       height: 50,
                      //     )),
                      // SizedBox(
                      //   height: 15,
                      // ),
                      GestureDetector(
                          onTap: () {
                            zoomVal++;
                            _plus(zoomVal);
                          },
                          child: Image.asset(
                            'assets/fg_images/7_map_icon_zoom_plus.png',
                            width: 50,
                            height: 50,
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          zoomVal--;
                          _minus(zoomVal);
                        },
                        child: Image.asset(
                          'assets/fg_images/7_map_icon_zoom_minus.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          _getMyLocation(zoomVal);
                        },
                        child: Image.asset(
                          'assets/fg_images/7_map_icon_location.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 10,
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 117,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: checkPoints14.length,
                      itemBuilder: (context, index) {
                        return Stack(
                          overflow: Overflow.visible,
                          children: [
                            GestureDetector(
                              onTap: () {
                                _goToCheckPoint(checkPoints14[index].lat,
                                    checkPoints14[index].long);
                              },
                              child: Container(
                                margin: EdgeInsets.only(left: 5, right: 5),
                                height: 117,
                                width: 174,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          checkPoints14[index].image),
                                      fit: BoxFit.cover,
                                    )),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: checkPoints14[index]
                                            .color
                                            .withOpacity(0.5),
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(10.0),
                                            bottomLeft: Radius.circular(10.0)),
                                      ),
                                      height: 33,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: 5,
                                          bottom: 5,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Text(
                                                checkPoints14[index].name,
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1.09,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            checkPoints14[index].isCompleted == true
                                ? Positioned(
                                    top: 12.5,
                                    left: 15,
                                    child: Image.asset(
                                      'assets/fg_images/6_home_logo_mission_completed2.png',
                                      height: 30,
                                      width: 100,
                                    ))
                                : Container()
                          ],
                        );
                      },
                    ),
                  ),
                ),
                Positioned(
                  top: 140,
                  left: 20,
                  child: Text(elapsedTime,
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Hexcolor('#FE6802'),
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                      )),
                )
              ]),
            ),
    );
  }

  // _addPolyLine2() {
  //   PolylineId id = PolylineId("poly2");
  //   Polyline polyline2 = Polyline(
  //       patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
  //       polylineId: id,
  //       color: Colors.green,
  //       points: polylineCoordinates2,
  //       width: 10);
  //   polylines[id] = polyline2;
  //   setState(() {});
  // }

  // _getPolyline2() async {
  //   PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
  //     "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
  //     PointLatLng(_destLatitude, _destLongitude),
  //     PointLatLng(_destLatitude2, _destLongitude2),
  //     travelMode: TravelMode.walking,
  //     // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
  //   );
  //   if (result2.points.isNotEmpty) {
  //     result2.points.forEach((PointLatLng point) {
  //       polylineCoordinates2.add(LatLng(point.latitude, point.longitude));
  //     });
  //   }
  //   _addPolyLine2();
  // }

  // _addPolyLine3() {
  //   PolylineId id = PolylineId("poly3");
  //   Polyline polyline3 = Polyline(
  //       patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
  //       polylineId: id,
  //       color: Colors.green,
  //       points: polylineCoordinates3,
  //       width: 10);
  //   polylines[id] = polyline3;
  //   setState(() {});
  // }

  // _getPolyline3() async {
  //   PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
  //     "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
  //     PointLatLng(_destLatitude2, _destLongitude2),
  //     PointLatLng(_destLatitude3, _destLongitude3),
  //     travelMode: TravelMode.walking,
  //     // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
  //   );
  //   if (result3.points.isNotEmpty) {
  //     result3.points.forEach((PointLatLng point) {
  //       polylineCoordinates3.add(LatLng(point.latitude, point.longitude));
  //     });
  //   }
  //   _addPolyLine3();
  // }

  // _addPolyLine4() {
  //   PolylineId id = PolylineId("poly4");
  //   Polyline polyline4 = Polyline(
  //       patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
  //       polylineId: id,
  //       color: Colors.green,
  //       points: polylineCoordinates4,
  //       width: 10);
  //   polylines[id] = polyline4;
  //   setState(() {});
  // }

  // _getPolyline4() async {
  //   PolylineResult result4 = await polylinePoints4.getRouteBetweenCoordinates(
  //     "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
  //     PointLatLng(_destLatitude3, _destLongitude3),
  //     PointLatLng(_destLatitude4, _destLongitude4),
  //     // travelMode: TravelMode.walking,

  //     // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
  //   );
  //   if (result4.points.isNotEmpty) {
  //     result4.points.forEach((PointLatLng point) {
  //       polylineCoordinates4.add(LatLng(point.latitude, point.longitude));
  //     });
  //   }
  //   _addPolyLine4();
  // }

  Future<void> _goToCheckPoint(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 17)));
  }

  Future<void> centerScreen(Position currentLocation) async {
    setState(() {});
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 17.0),
    ));
  }
}
