import 'package:flutter_free_gen_reality/10_profile_rating_json.dart';
import 'package:flutter_free_gen_reality/6_home_filter_json.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_free_gen_reality/10_profile_partners_json.dart';
import 'package:flutter_free_gen_reality/10_profile_cooperation_static_json.dart';
import 'package:flutter_free_gen_reality/10_profile_news_list_array.dart';
import 'package:flutter_free_gen_reality/10_profile_faq_json.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../10_profile_store_products_json.dart';
import '../10_profile_about_us_static_json.dart';
import '../10_profile_contacts_static_json.dart';
import '../6_home_missions_json.dart';
import '../6_home_missions_favorite_json.dart';
import '../6_home_submissions_json.dart';
import '../6_home_submissions_progress_json.dart';
import '../10_profile_currencies_json.dart';
import '../10_profile_statuses_json.dart';
import '../8_camera_models_json.dart';
// import '../0_links.dart';

class Services {
  static const String url =
      // LinkMainDomen.urlDomen + '/api/publication/admin/all/news/?active=true&sort=publicationDate,DESC&size=10000';
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/news/?active=true&sort=publicationDate,DESC&size=10000';
      // 'http://generation-admin.ehub.com.ua/api/publication/admin/all/news/?active=true&sort=publicationDate,DESC&size=10000';

  static Future<Post> getNews() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Post listPosts = postFromJson(response.body);
        return listPosts;
      } else {
        return Post();
      }
    } catch (e) {
      print(e);
      return Post();
    }
  }
}

class ServicesFaq {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/faq/?active=true&?size=10000';

  static Future<Faq> getFaq() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Faq listPosts = faqFromJson(response.body);
        return listPosts;
      } else {
        return Faq();
      }
    } catch (e) {
      print(e);
      return Faq();
    }
  }
}

class ServicesPartners {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/partners/?active=true&?size=10000';

  static Future<Partners> getPartners() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Partners listPosts = partnersFromJson(response.body);
        return listPosts;
      } else {
        return Partners();
      }
    } catch (e) {
      print(e);
      return Partners();
    }
  }
}

class ServicesStaticCooperation {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/static_page/cooperation';

  static Future<StaticCooperation> getCooperation() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final StaticCooperation listPosts =
            staticCooperationFromJson(response.body);
        return listPosts;
      } else {
        return StaticCooperation();
      }
    } catch (e) {
      print(e);
      return StaticCooperation();
    }
  }
}

class ServicesStaticAboutUs {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/static_page/about-us';

  static Future<StaticAboutUs> getAboutUs() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final StaticAboutUs listPosts = staticAboutUsFromJson(response.body);
        return listPosts;
      } else {
        return StaticAboutUs();
      }
    } catch (e) {
      print(e);
      return StaticAboutUs();
    }
  }
}

class ServicesStaticContacts {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/static_page/contacts';

  static Future<StaticContacts> getContacts() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final StaticContacts listPosts = staticContactsFromJson(response.body);
        return listPosts;
      } else {
        return StaticContacts();
      }
    } catch (e) {
      print(e);
      return StaticContacts();
    }
  }
}

class ServicesProducts {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/products?active=true&?size=10000';

  static Future<Products> getProducts() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Products listPosts = productsFromJson(response.body);
        return listPosts;
      } else {
        return Products();
      }
    } catch (e) {
      print(e);
      return Products();
    }
  }
}

class ServicesFilters {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/filters?locale=uk_UA&?active=true';

  static Future<Filters> getFilters() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Filters listPosts = filtersFromJson(response.body);
        return listPosts;
      } else {
        return Filters();
      }
    } catch (e) {
      print(e);
      return Filters();
    }
  }
}

class ServicesUsers {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/user/list/USER?sortByTus=true&size=10000';

  static Future<Users> getUsers() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      if (response.statusCode == 200) {
        final Users listPosts = usersFromJson(response.body);
        return listPosts;
      } else {
        return Users();
      }
    } catch (e) {
      print(e);
      return Users();
    }
  }
}

class ServicesUsersMuromec {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/user/list/USER?sort=timer&size=10000';

  static Future<Users> getUsers() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      if (response.statusCode == 200) {
        final Users listPosts = usersFromJson(response.body);
        return listPosts;
      } else {
        return Users();
      }
    } catch (e) {
      print(e);
      return Users();
    }
  }
}

class ServicesSubMissions {
  static Future<SubMissions> getSubMissionData() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var missionId = sharedPreferences.get('missionId');
    var userId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");
    String url = 'http://generation-admin.ehub.com.ua/api/mission/' +
        '$missionId' +
        '/submission/list?active=true&user_id=' +
        '$userId&sort=position';
    print(url);
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {
        print('GET SUBMISSIONS DATA');
        final SubMissions listSubMissions = subMissionsFromJson(response.body);
        return listSubMissions;
      } else {
        return SubMissions();
      }
    } catch (e) {
      print(e);
      return SubMissions();
    }
  }
}

class ServicesMissions {
  static Future<Missions> getMissions() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    String id = sharedPreferences.get("id").toString();
    String url =
        'http://generation-admin.ehub.com.ua/api/mission/list?sort=position,ASC&active=true&user_id=$id'; // id
    String urlProgress =
        'http://generation-admin.ehub.com.ua/api/mission/progress/list/$id';
        //print(url); // id user
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      final progress = await http.get(urlProgress, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      //print(response.statusCode);
      if (response.statusCode == 200) {
        final Missions listMissions = missionsFromJson(response.body);
        
        final List<MissionProgress> missionProgress =
            missionProgressFromJson(progress.body);
        for (final mission in listMissions.missions) {
          for (var misProgress in missionProgress) {
            if (misProgress.missionId == mission.id) {
              mission.progress = misProgress.trackStatus;
            }
          }
        }
        // print(response.body);
        return listMissions;
      } else {
        return Missions();
      }
    } catch (e) {
      print(e);
      return Missions();
    }
  }
}

class ServicesSubmissionProgresses {
  static Future<List<SubMissionProgresses>> getProressSubmissions() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    var usedId = sharedPreferences.get('id');
    String url =
        'http://generation-admin.ehub.com.ua/api/submission/progress/list/$usedId';
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {
        print('GET ALL SUBMISSIONS PROGRESSES');
        final List<SubMissionProgresses> subMissionsProgresses =
            subMissionProgressesFromJson(response.body);
        return subMissionsProgresses;
      } else {
        return List<SubMissionProgresses>();
      }
    } catch (e) {
      return List<SubMissionProgresses>();
    }
  }
}

class ServicesCurrencies {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies/?sort=position';

  static Future<Currencies> getCurrencies() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Currencies currencies = currenciesFromJson(response.body);
        return currencies;
      } else {
        return Currencies();
      }
    } catch (e) {
      print(e);
      return Currencies();
    }
  }
}

class ServicesModels {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/model3d/list?size=50&active=true';

  static Future<Models> getModelsForCamera() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {
        final Models models = modelsFromJson(response.body);
        return models;
      } else {
        return Models();
      }
    } catch (e) {
      print(e);
      return Models();
    }
  }
}

class ServicesStatuses {
  static const String url =
      'http://generation-admin.ehub.com.ua/api/publication/admin/all/statuses';

  static Future<Statuses> getStatuses() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final Statuses statuses = statusesFromJson(response.body);
        return statuses;
      } else {
        return Statuses();
      }
    } catch (e) {
      print(e);
      return Statuses();
    }
  }
}

class ServicesFavoriteMissions {
  static Future<List<FavoriteMissions>> getFavoriteMissions() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    var id = sharedPreferences.get('id');
    String url =
        'http://generation-admin.ehub.com.ua/api/favorites/$id';
    String urlProgress =
        'http://generation-admin.ehub.com.ua/api/mission/progress/list/$id';
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      final progress = await http.get(urlProgress, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {        
        final List<FavoriteMissions> favoriteMissions =
            favoriteMissionsFromJson(response.body);

        final List<MissionProgress> missionProgress =
            missionProgressFromJson(progress.body);
        for (final mission in favoriteMissions) {
          for (var misProgress in missionProgress) {
            if (misProgress.missionId == mission.id) {
              mission.progress = misProgress.trackStatus;
            }
          }
        }

        return favoriteMissions;
      } else {
        return List<FavoriteMissions>();
      }
    } catch (e) {
      return List<FavoriteMissions>();
    }
  }
}
