import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:flutter/material.dart';

class AuthService {
  Dio dio = new Dio();

  login(name, password) async {
    try {
      return await dio.post(
          'https://flutter-auth-freegen.herokuapp.com/authenticate',
          data: {"name": name, "password": password},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e.response.data['msg']);
      toast(
        "Пользователь не найден или неверный пароль!",
        Toast.LENGTH_LONG,
        ToastGravity.BOTTOM,
        Colors.red,
      );
    }
  }

  toast(String msg, Toast toast, ToastGravity toastGravity, Color colors) {
    Fluttertoast.showToast(
        timeInSecForIosWeb: 2,
        msg: msg,
        toastLength: toast,
        gravity: toastGravity,
        backgroundColor: colors,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  addUser(name, password) async {
    try {
      return await dio.post(
          'https://flutter-auth-freegen.herokuapp.com/adduser',
          data: {"name": name, "password": password},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e.response.data['msg']);
      toast(
        "Ошибка!",
        Toast.LENGTH_LONG,
        ToastGravity.BOTTOM,
        Colors.red,
      );
    }
  }

  getInfo(token) async {
    dio.options.headers['Authorization'] = 'Bearer $token';
    return await dio.get('https://flutter-auth-freegen.herokuapp.com/getinfo');
  }
}
