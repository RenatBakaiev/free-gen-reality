import 'package:firebase_auth/firebase_auth.dart';

// import 'package:google_sign_in/google_sign_in.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:rxdart/rxdart.dart';

import '../user.dart';

class AuthService{
  final FirebaseAuth _fAuth = FirebaseAuth.instance;

  Future<User> signInWithEmailAndPassword(String email, String password) async {
    try{
      AuthResult result = await _fAuth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return User.fromFirebase(user);
    }catch(e) {
      print(e);
      return null;
    }
  }

  Future<User> registerWithEmailAndPassword(String email, String password) async {
    try{
      AuthResult result = await _fAuth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return User.fromFirebase(user);
    }catch(e) {
      print(e);
      return null;
    }
  }

  Future logOut() async {
    await _fAuth.signOut();
  }

  Stream<User> get currentUser{
    return _fAuth.onAuthStateChanged.map((FirebaseUser user) => user != null ? User.fromFirebase(user) : null);
  }

}

// class AuthGoogleService {
//   final GoogleSignIn _googleSignIn = GoogleSignIn();
//   final FirebaseAuth _auth = FirebaseAuth.instance;
//   final Firestore _db = Firestore.instance;

//   bool isSignIn = false;

//   Stream<FirebaseUser> user;
//   Stream<Map<String, dynamic>> profile;
//   PublishSubject loading = PublishSubject();

//   AuthGoogleService() {
//     user = _auth.onAuthStateChanged;

//     profile = user.switchMap((FirebaseUser u) {
//       if (u != null) {
//         return _db
//           .collection('usersAll')
//           .document(u.uid)
//           .snapshots()
//           .map((snap) => snap.data);
//       } else {
//         return Stream.value({}); 
//       }
//     });
//   }

//   Future<FirebaseUser> googleSignIn() async {
//     loading.add(true);
//     GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//     GoogleSignInAuthentication googleAuth = await googleUser.authentication;
//     final AuthCredential credential = GoogleAuthProvider.getCredential(
//       accessToken: googleAuth.accessToken,
//       idToken: googleAuth.idToken, 
//       );

//     final FirebaseUser user =
//       (await _auth.signInWithCredential(credential)).user;

//     updateUserData(user);

//     loading.add(false);
//     print("signed in " + user.displayName);
//     return user;

    

//   }
  
//   void updateUserData(FirebaseUser user) async {
//     DocumentReference ref = _db.collection('usersAll').document(user.uid);

//       return ref.setData({
//         'uid': user.uid,
//         'email': user.email,
//         'photoURL': user.photoUrl,
//         'displayName': user.displayName,
//         'lastSeen': DateTime.now()
//       }, merge: true);


//   }

//   void signOut() {
//     _auth.signOut();
//   }

// }

// final AuthGoogleService authGoogleService = AuthGoogleService();