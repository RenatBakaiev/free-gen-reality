import 'dart:ui';

class Partner {
  final String name;
  final String image;
  final Color backcolor;
  final Color backColorImage;
  final String link;

  Partner ({
    this.name,
    this.image, 
    this.backcolor,
    this.backColorImage,
    this.link,
  });
}