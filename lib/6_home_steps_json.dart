// To parse this JSON data, do
//
//     final steps = stepsFromJson(jsonString);

import 'dart:convert';

Steps stepsFromJson(String str) => Steps.fromJson(json.decode(str));

String stepsToJson(Steps data) => json.encode(data.toJson());

class Steps {
    Steps({
        this.page,
        this.steps,
    });

    Page page;
    List<Stup> steps;

    factory Steps.fromJson(Map<String, dynamic> json) => Steps(
        page: Page.fromJson(json["page"]),
        steps: List<Stup>.from(json["steps"].map((x) => Stup.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "page": page.toJson(),
        "steps": List<dynamic>.from(steps.map((x) => x.toJson())),
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class Stup {
    Stup({
        this.id,
        this.stepType,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.createdDate,
        this.lastModifiedDate,
        this.location,
        this.descriptionUa,
        this.descriptionRu,
        this.descriptionEn,
        this.imageUrl,
        this.audioUrl,
        this.videoUrl,
        this.position,
        this.currencyType,
        this.points,
        this.activationRadius,
        this.correctAnswer,
        this.currencyTypeWrongAnswer,
        this.pointsWrongAnswer,
        this.imageScan,
        this.scanType,
        this.canBeSkipped,
        this.active,
        this.modelList,
        this.geolocationList,
    });

    int id;
    String stepType;
    String titleUa;
    String titleRu;
    String titleEn;
    DateTime createdDate;
    DateTime lastModifiedDate;
    dynamic location;
    String descriptionUa;
    String descriptionRu;
    String descriptionEn;
    dynamic imageUrl;
    dynamic audioUrl;
    dynamic videoUrl;
    int position;
    String currencyType;
    double points;
    dynamic activationRadius;
    dynamic correctAnswer;
    String currencyTypeWrongAnswer;
    double pointsWrongAnswer;
    String imageScan;
    String scanType;
    bool canBeSkipped;
    bool active;
    List<dynamic> modelList;
    List<dynamic> geolocationList;

    factory Stup.fromJson(Map<String, dynamic> json) => Stup(
        id: json["id"],
        stepType: json["stepType"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        location: json["location"],
        descriptionUa: json["descriptionUa"],
        descriptionRu: json["descriptionRu"],
        descriptionEn: json["descriptionEn"],
        imageUrl: json["imageUrl"],
        audioUrl: json["audioUrl"],
        videoUrl: json["videoUrl"],
        position: json["position"],
        currencyType: json["currencyType"],
        points: json["points"],
        activationRadius: json["activationRadius"],
        correctAnswer: json["correctAnswer"],
        currencyTypeWrongAnswer: json["currencyTypeWrongAnswer"],
        pointsWrongAnswer: json["pointsWrongAnswer"],
        imageScan: json["imageScan"],
        scanType: json["scanType"],
        canBeSkipped: json["canBeSkipped"],
        active: json["active"],
        modelList: List<dynamic>.from(json["modelList"].map((x) => x)),
        geolocationList: List<dynamic>.from(json["geolocationList"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "stepType": stepType,
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "location": location,
        "descriptionUa": descriptionUa,
        "descriptionRu": descriptionRu,
        "descriptionEn": descriptionEn,
        "imageUrl": imageUrl,
        "audioUrl": audioUrl,
        "videoUrl": videoUrl,
        "position": position,
        "currencyType": currencyType,
        "points": points,
        "activationRadius": activationRadius,
        "correctAnswer": correctAnswer,
        "currencyTypeWrongAnswer": currencyTypeWrongAnswer,
        "pointsWrongAnswer": pointsWrongAnswer,
        "imageScan": imageScan,
        "scanType": scanType,
        "canBeSkipped": canBeSkipped,
        "active": active,
        "modelList": List<dynamic>.from(modelList.map((x) => x)),
        "geolocationList": List<dynamic>.from(geolocationList.map((x) => x)),
    };
}
