// import 'dart:convert';

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/10_profile_rewards.dart';
// import 'package:flutter_free_gen_reality/services/auth_node.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '10_profile_json.dart';
import 'ProgressHUD.dart';
import 'api.dart';
import 'package:http/http.dart' as http;

// import '10_profile_rewards.dart';
// import 'package:http/http.dart' as http;

class EnterRegister extends StatefulWidget {
  @override
  _EnterRegisterState createState() => _EnterRegisterState();
}

class _EnterRegisterState extends State<EnterRegister> {
  var _blankFocusNode = new FocusNode();
  final formKey = GlobalKey<FormState>();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController nicknameTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController2 =
      new TextEditingController();

  bool isVisible = false;
  bool isVisible2 = false;

  var email;
  var password;
  var nickName;
  var photo;

  showYouReceiveRewardWindow() {
    setState(() {
      allRewards[0].quantity = allRewards[0].quantity + 20;
    });
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              // width: MediaQuery.of(context).size.width - 40,
              height: 425,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 10,
                              ),
                              child: Text(
                                'Вітаємо!',
                                style: TextStyle(
                                  color: Hexcolor('#1E2E45'),
                                  fontSize: 24,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 5,
                              ),
                              child: Text(
                                'Ви успішно зареєструвались',
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'і отримали бонус  ',
                                  style: TextStyle(
                                    color: Hexcolor('#747474'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  '20' + ' TUS',
                                  style: TextStyle(
                                    color: Hexcolor('#519950'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ])),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                    ),
                    child: Image.asset(
                      'assets/fg_images/4_enter_register_rewards.jpg',
                      height: 200,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Продовжити",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              var userId = sharedPreferences.get("id");
                              var tusRewardReceived = sharedPreferences
                                  .get("20TUSRewardReceived$userId");

                              if (tusRewardReceived == '' ||
                                  tusRewardReceived == null) {
                                changeTus(20.0, "DEPOSIT");
                                sharedPreferences.setBool(
                                    '20TUSRewardReceived$userId',
                                    true); // check20TUS
                              } else {
                                print('you cant to reseive more');
                              }
                              Navigator.pushNamed(
                                  context, '/5_myBottomBar.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  logIn() {
    if (formKey.currentState.validate()) {
      print("logIned");
      emailTextEditingController.clear();
      passwordTextEditingController.clear();
      Navigator.pushNamed(context, '/5_myBottomBar.dart');
    }
  }

  final scaffoldKey = GlobalKey<ScaffoldState>();

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "deposit 20 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('+20TUS done');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  bool isApiCallProcess = false;
  String message = '';
  register() async {
    if (formKey.currentState.validate()) {
      var email = emailTextEditingController.text;
      var password = passwordTextEditingController.text;
      var nickName = nicknameTextEditingController.text;
      setState(() {
        isApiCallProcess = true;
      });
      var registrationType = "EMAIL";

      var response =
          await registerUser(email, nickName, password, registrationType);
      print(response);
      if (response.statusCode == 200) {
        setState(() {
          isApiCallProcess = false;
        });
        // message = "Користувач зареєстрований!";
        Future.delayed(const Duration(seconds: 1), () {
          showYouReceiveRewardWindow();
        });
        Map<String, dynamic> responseJson = jsonDecode(response.body);

        print(responseJson["access"]);
        print(responseJson["id"]);

        final SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setString('token', responseJson["access"]);
        sharedPreferences.setInt('id', responseJson["id"]);

        sharedPreferences.setBool('isStep1Completed', false);
        sharedPreferences.setBool('isStep2Completed', false);
        sharedPreferences.setBool('isStep3Completed', false);
        sharedPreferences.setBool('isStep4Completed', false);
        sharedPreferences.setBool('isStep5Completed', false);
        sharedPreferences.setBool('isStep6Completed', false);
        sharedPreferences.setBool('isStep7Completed', false);
        sharedPreferences.setBool('isStep8Completed', false);
        sharedPreferences.setBool('isStep9Completed', false);
        sharedPreferences.setBool('isStep10Completed', false);
        sharedPreferences.setBool('isStep11Completed', false);
        sharedPreferences.setBool('isStep12Completed', false);

        sharedPreferences.setBool('isQuiz1Done', false);
        sharedPreferences.setBool('isQuiz2Done', false);
        sharedPreferences.setBool('isQuiz3Done', false);
        sharedPreferences.setBool('isQuiz4Done', false);
        sharedPreferences.setBool('isQuiz5Done', false);
        sharedPreferences.setBool('isQuiz6Done', false);
        sharedPreferences.setBool('isQuiz7Done', false);
        sharedPreferences.setBool('isQuiz8Done', false);
        sharedPreferences.setBool('isQuiz9Done', false);
        sharedPreferences.setBool('isQuiz10Done', false);
        sharedPreferences.setBool('isQuiz11Done', false);
        sharedPreferences.setBool('isQuiz12Done', false);

        sharedPreferences.setBool('muromecStep1Done', false);
        sharedPreferences.setBool('muromecStep2Done', false);
        sharedPreferences.setBool('muromecStep3Done', false);
        sharedPreferences.setBool('muromecStep4Done', false);
        sharedPreferences.setBool('muromecStep5Done', false);
        sharedPreferences.setBool('muromecStep6Done', false);
        sharedPreferences.setBool('muromecStep7Done', false);
        sharedPreferences.setBool('muromecStep8Done', false);
        sharedPreferences.setBool('muromecStep9Done', false);
        sharedPreferences.setBool('muromecStep10Done', false);
        sharedPreferences.setBool('muromecStep11Done', false);
        sharedPreferences.setBool('muromecStep12Done', false);
        sharedPreferences.setBool('muromecStep13Done', false);
        sharedPreferences.setBool('muromecStep14Done', false);

        sharedPreferences.setInt('tus', 20);

        sharedPreferences.setBool('isQuestionaireDone', false);

        sharedPreferences.setBool('Mission12NewYearTreesCompleted', false);
      } else {
        setState(() {
          isApiCallProcess = false;
        });
        message = "Сервіс недоступний!";
      }
    }
  }

  // register() {
  //   if (formKey.currentState.validate()) {
  //     print("Registered");
  //     emailTextEditingController.clear();
  //     passwordTextEditingController.clear();
  //     passwordTextEditingController2.clear();
  //     AuthService().addUser(name, password).then((val) {
  //       print(val.data['msg']);
  //     });
  //     toast(
  //       "Пользователь зарегистрирован!",
  //       Toast.LENGTH_LONG,
  //       ToastGravity.BOTTOM,
  //       Colors.green,
  //     ); // /4_enter_register_close
  //     Navigator.pushNamed(context, '/4_enter_register_close');
  //     // showYouReceiveRewardWindow();
  //   }
  // }

  toast(String msg, Toast toast, ToastGravity toastGravity, Color colors) {
    Fluttertoast.showToast(
        timeInSecForIosWeb: 5,
        msg: msg,
        toastLength: toast,
        gravity: toastGravity,
        backgroundColor: colors,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  // showYouReceiveRewardWindow() {
  //   setState(() {
  //     allRewards[0].quantity = allRewards[0].quantity + 100;
  //   });
  //   showDialog(
  //       barrierDismissible: false,
  //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
  //       context: context,
  //       builder: (context) {
  //         return Dialog(
  //           insetPadding: EdgeInsets.only(
  //             left: 20,
  //             right: 20,
  //           ),
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           child: Container(
  //             // width: MediaQuery.of(context).size.width - 40,
  //             height: 415,
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 Container(
  //                     alignment: Alignment.center,
  //                     margin: EdgeInsets.only(
  //                       top: 20,
  //                       left: 10,
  //                       right: 10,
  //                     ),
  //                     child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         crossAxisAlignment: CrossAxisAlignment.center,
  //                         children: [
  //                           Container(
  //                             margin: EdgeInsets.only(
  //                               bottom: 10,
  //                             ),
  //                             child: Text(
  //                               'Поздравляем!',
  //                               style: TextStyle(
  //                                 color: Hexcolor('#1E2E45'),
  //                                 fontSize: 24,
  //                                 fontFamily: 'Arial',
  //                                 fontWeight: FontWeight.w700,
  //                               ),
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           ),
  //                           Container(
  //                             margin: EdgeInsets.only(
  //                               bottom: 5,
  //                             ),
  //                             child: Text(
  //                               'Вы успешно зарегистрировались',
  //                               style: TextStyle(
  //                                 color: Hexcolor('#747474'),
  //                                 fontSize: 20,
  //                                 fontFamily: 'Arial',
  //                                 fontWeight: FontWeight.w400,
  //                               ),
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           ),
  //                           Row(
  //                             mainAxisAlignment: MainAxisAlignment.center,
  //                             crossAxisAlignment: CrossAxisAlignment.end,
  //                             children: [
  //                               Text(
  //                                 'и получили бонус ',
  //                                 style: TextStyle(
  //                                   color: Hexcolor('#747474'),
  //                                   fontSize: 20,
  //                                   fontFamily: 'Arial',
  //                                   fontWeight: FontWeight.w400,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                               Text(
  //                                 ' 100 TUS',
  //                                 style: TextStyle(
  //                                   color: Hexcolor('#519950'),
  //                                   fontSize: 20,
  //                                   fontFamily: 'Arial',
  //                                   fontWeight: FontWeight.w700,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                             ],
  //                           ),
  //                         ])),
  //                 Container(
  //                   margin: EdgeInsets.only(
  //                     top: 10,
  //                   ),
  //                   child: Image.asset(
  //                     'assets/fg_images/4_enter_register_rewards.jpg',
  //                     height: 200,
  //                   ),
  //                 ),
  //                 Container(
  //                   margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
  //                   child: Column(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Container(
  //                         height: 60,
  //                         width: MediaQuery.of(context).size.width,
  //                         child: RaisedButton(
  //                           child: Text(
  //                             "Продолжить",
  //                             style: TextStyle(
  //                               color: Colors.white,
  //                               fontSize: 17,
  //                               fontFamily: 'Arial',
  //                               fontWeight: FontWeight.w600,
  //                             ),
  //                           ),
  //                           color: Hexcolor('#EB5C18'),
  //                           shape: new RoundedRectangleBorder(
  //                               borderRadius: new BorderRadius.circular(14.0)),
  //                           onPressed: () {
  //                             Navigator.pushNamed(
  //                                 context, '/5_myBottomBar.dart');
  //                           },
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }

  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

  // register(email, password) async {
  //   if (formKey.currentState.validate()) {
  //     print("Registered");
  //     emailTextEditingController.clear();
  //     passwordTextEditingController.clear();
  //     var url = "http://127.0.0.1:5000";
  //     final http.Response response = await http.post(
  //   url,
  //   headers: <String, String>{
  //     'Content-Type': 'application/json; charset=UTF-8',
  //   },
  //   body: jsonEncode(<String, String>{
  //     'email': email,
  //     'password': password,
  //   }),
  // );

  // if (response.statusCode == 201) {
  // } else {
  //   throw Exception('Failed to create album.');
  // }
  //     // Navigator.pushNamed(context, '/5_myBottomBar.dart');
  //   }
  // }

  Future<User> loadUserImage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    String url = 'http://generation-admin.ehub.com.ua/api/user/update';

    String photoLink = sharedPreferences.get("photo");

    final http.Response response = await http.put(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "id": id,
        "profileImageUrl": photoLink,
      }),
    );
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      print(photoLink);
      print('Image loaded');
      print(response.body);
    } else {
      throw Exception('Failed to load image.');
    }
  }

  FacebookLogin fbLogin = FacebookLogin();

  signUpWithFacebook() async {
    setState(() {
      isApiCallProcess = true;
    });
    // try {
    var facebookLogin = new FacebookLogin();
    var result = await facebookLogin.logIn(['email']);
    if (result.status == FacebookLoginStatus.loggedIn) {
      final AuthCredential credential = FacebookAuthProvider.getCredential(
          accessToken: result.accessToken.token);
      final FirebaseUser user =
          (await FirebaseAuth.instance.signInWithCredential(credential)).user;

      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString('facebookName', user.displayName);
      sharedPreferences.setString('uid', result.accessToken.userId);
      sharedPreferences.setString('token', result.accessToken.token);
      sharedPreferences.setString('email', user.email);
      sharedPreferences.setString('photo', user.photoUrl);

      // print('FACEBOOK ID: ' + result.accessToken.userId);
      // print('FACEBOOK TOKEN: ' + result.accessToken.token);
      // print('FACEBOOK NAME: ' + user.displayName);
      // print('FACEBOOK EMAIL: ' + user.email);
      // print('FACEBOOK PHOTO: ' + user.photoUrl);

      email = sharedPreferences.get("email");
      nickName = sharedPreferences.get("facebookName");
      password = sharedPreferences.get("uid");
      photo = sharedPreferences.get("photo");

      var registrationType = "FACEBOOK";
      print('FACEBOOK UID: ' + password);
      // print('FACEBOOK TOKEN: ' + result.accessToken.token);
      print('FACEBOOK NAME: ' + nickName);
      print('FACEBOOK EMAIL: ' + email);
      print('FACEBOOK PHOTO: ' + photo);

      var response =
          await registerUser(email, nickName, password, registrationType);
      print(response);
      if (response.statusCode == 200) {
        setState(() {
          isApiCallProcess = false;
        });
        // message = "Користувач зареєстрований!";
        Future.delayed(const Duration(seconds: 1), () {
          showYouReceiveRewardWindow();
        });
        Map<String, dynamic> responseJson = jsonDecode(response.body);

        print(responseJson["access"]);
        print(responseJson["id"]);

        final SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setString('token', responseJson["access"]);
        sharedPreferences.setInt('id', responseJson["id"]);

        sharedPreferences.setBool('isStep1Completed', false);
        sharedPreferences.setBool('isStep2Completed', false);
        sharedPreferences.setBool('isStep3Completed', false);
        sharedPreferences.setBool('isStep4Completed', false);
        sharedPreferences.setBool('isStep5Completed', false);
        sharedPreferences.setBool('isStep6Completed', false);
        sharedPreferences.setBool('isStep7Completed', false);
        sharedPreferences.setBool('isStep8Completed', false);
        sharedPreferences.setBool('isStep9Completed', false);
        sharedPreferences.setBool('isStep10Completed', false);
        sharedPreferences.setBool('isStep11Completed', false);
        sharedPreferences.setBool('isStep12Completed', false);

        sharedPreferences.setBool('isQuiz1Done', false);
        sharedPreferences.setBool('isQuiz2Done', false);
        sharedPreferences.setBool('isQuiz3Done', false);
        sharedPreferences.setBool('isQuiz4Done', false);
        sharedPreferences.setBool('isQuiz5Done', false);
        sharedPreferences.setBool('isQuiz6Done', false);
        sharedPreferences.setBool('isQuiz7Done', false);
        sharedPreferences.setBool('isQuiz8Done', false);
        sharedPreferences.setBool('isQuiz9Done', false);
        sharedPreferences.setBool('isQuiz10Done', false);
        sharedPreferences.setBool('isQuiz11Done', false);
        sharedPreferences.setBool('isQuiz12Done', false);

        sharedPreferences.setBool('muromecStep1Done', false);
        sharedPreferences.setBool('muromecStep2Done', false);
        sharedPreferences.setBool('muromecStep3Done', false);
        sharedPreferences.setBool('muromecStep4Done', false);
        sharedPreferences.setBool('muromecStep5Done', false);
        sharedPreferences.setBool('muromecStep6Done', false);
        sharedPreferences.setBool('muromecStep7Done', false);
        sharedPreferences.setBool('muromecStep8Done', false);
        sharedPreferences.setBool('muromecStep9Done', false);
        sharedPreferences.setBool('muromecStep10Done', false);
        sharedPreferences.setBool('muromecStep11Done', false);
        sharedPreferences.setBool('muromecStep12Done', false);
        sharedPreferences.setBool('muromecStep13Done', false);
        sharedPreferences.setBool('muromecStep14Done', false);

        sharedPreferences.setInt('tus', 20);

        sharedPreferences.setBool('isQuestionaireDone', false);

        sharedPreferences.setBool('Mission12NewYearTreesCompleted', false);
        loadUserImage();
      } else {
        setState(() {
          isApiCallProcess = false;
        });
        message = "Сервіс недоступний!";
      }

      // print('signed in ' + user.displayName);
      // Navigator.of(context).pushReplacementNamed('/5_myBottomBar.dart');
      // setState(() {
      //   isSignIn = true;
      //   isApiCallProcess = false;
      // });
      // return user;
    }
    // } catch (e) {
    //   print(e.message);
    // }
  }

  bool isSignIn = false;

  GoogleSignIn _googleSignIn = new GoogleSignIn();
  FirebaseUser _user;
  FirebaseAuth _auth = FirebaseAuth.instance;

  signUpWithGoogle() async {
    setState(() {
      isApiCallProcess = true;
    });
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

    AuthResult result = (await _auth.signInWithCredential(credential));

    _user = result.user;
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    sharedPreferences.setString('email', _user.email);
    sharedPreferences.setString('uid', _user.uid);
    sharedPreferences.setString('googleName', _user.displayName);
    sharedPreferences.setString('photo', _user.photoUrl);

    email = sharedPreferences.get("email");
    nickName = sharedPreferences.get("googleName");
    password = sharedPreferences.get("uid");
    photo = sharedPreferences.get("photo");
    var registrationType = "GOOGLE";

    print('GOOGLE EMAIL: ' + email);
    print('GOOGLE NAME: ' + nickName);
    print('GOOGLE UID: ' + password);
    print('GOOGLE PHOTO: ' + photo);

    var response2 =
        await registerUser(email, nickName, password, registrationType);
    print(response2);
    if (response2.statusCode == 200) {
      setState(() {
        isApiCallProcess = false;
      });
      // message = "Користувач зареєстрований!";
      Future.delayed(const Duration(seconds: 1), () {
        showYouReceiveRewardWindow();
      });
      Map<String, dynamic> responseJson = jsonDecode(response2.body);

      print(responseJson["access"]);
      print(responseJson["id"]);

      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString('token', responseJson["access"]);
      sharedPreferences.setInt('id', responseJson["id"]);

      sharedPreferences.setBool('isStep1Completed', false);
      sharedPreferences.setBool('isStep2Completed', false);
      sharedPreferences.setBool('isStep3Completed', false);
      sharedPreferences.setBool('isStep4Completed', false);
      sharedPreferences.setBool('isStep5Completed', false);
      sharedPreferences.setBool('isStep6Completed', false);
      sharedPreferences.setBool('isStep7Completed', false);
      sharedPreferences.setBool('isStep8Completed', false);
      sharedPreferences.setBool('isStep9Completed', false);
      sharedPreferences.setBool('isStep10Completed', false);
      sharedPreferences.setBool('isStep11Completed', false);
      sharedPreferences.setBool('isStep12Completed', false);

      sharedPreferences.setBool('isQuiz1Done', false);
      sharedPreferences.setBool('isQuiz2Done', false);
      sharedPreferences.setBool('isQuiz3Done', false);
      sharedPreferences.setBool('isQuiz4Done', false);
      sharedPreferences.setBool('isQuiz5Done', false);
      sharedPreferences.setBool('isQuiz6Done', false);
      sharedPreferences.setBool('isQuiz7Done', false);
      sharedPreferences.setBool('isQuiz8Done', false);
      sharedPreferences.setBool('isQuiz9Done', false);
      sharedPreferences.setBool('isQuiz10Done', false);
      sharedPreferences.setBool('isQuiz11Done', false);
      sharedPreferences.setBool('isQuiz12Done', false);

      sharedPreferences.setBool('muromecStep1Done', false);
      sharedPreferences.setBool('muromecStep2Done', false);
      sharedPreferences.setBool('muromecStep3Done', false);
      sharedPreferences.setBool('muromecStep4Done', false);
      sharedPreferences.setBool('muromecStep5Done', false);
      sharedPreferences.setBool('muromecStep6Done', false);
      sharedPreferences.setBool('muromecStep7Done', false);
      sharedPreferences.setBool('muromecStep8Done', false);
      sharedPreferences.setBool('muromecStep9Done', false);
      sharedPreferences.setBool('muromecStep10Done', false);
      sharedPreferences.setBool('muromecStep11Done', false);
      sharedPreferences.setBool('muromecStep12Done', false);
      sharedPreferences.setBool('muromecStep13Done', false);
      sharedPreferences.setBool('muromecStep14Done', false);

      sharedPreferences.setInt('tus', 20);

      sharedPreferences.setBool('isQuestionaireDone', false);

      sharedPreferences.setBool('Mission12NewYearTreesCompleted', false);

      loadUserImage();
    } else {
      setState(() {
        isApiCallProcess = false;
      });
      message = "Сервіс недоступний!";
    }

    // print('signed in ' + _user.displayName);
    // setState(() {
    //   isApiCallProcess = true;
    // });
  }

  var name, password2, token;

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Hexcolor('#7D5AC2'),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blankFocusNode);
        },
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 60),
                    child: Image.asset(
                      'assets/fg_images/4_enter_logo.png',
                      width: 140,
                      height: 126,
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 15, top: 30),
                          child: GestureDetector(
                            onTap: () {
                              // signUpWithFacebook();
                            },
                            child: Text(
                              'Реєстрація',
                              style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 25,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Arial',
                                letterSpacing: 1.28,
                              ),
                            ),
                          ),
                        ),
                        Form(
                          key: formKey,
                          child: Column(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 7,
                                    ),
                                    child: Text(
                                      "Email",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                  TextFormField(
                                    onChanged: (val) {
                                      name = val;
                                    },
                                    validator: (value) {
                                      return RegExp(
                                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                              .hasMatch(value)
                                          ? null
                                          : "Будь-ласка введіть коректний email";
                                    },
                                    controller: emailTextEditingController,
                                    textAlign: TextAlign.left,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: InputDecoration(
                                      hintText: "Введіть email",
                                      contentPadding: new EdgeInsets.only(
                                          left: 20, top: 21.5, bottom: 21.5),
                                      hintStyle: TextStyle(
                                          color: Hexcolor('#D3D3D3'),
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'Arial'),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Hexcolor('#FE6802'),
                                          width: 1,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Colors.white54,
                                          width: 1,
                                        ),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Hexcolor('#FF1E1E'),
                                          width: 1,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Hexcolor('#FF1E1E'),
                                          width: 1,
                                        ),
                                      ),
                                      fillColor: Colors.white,
                                      filled: true,
                                    ),
                                    style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial'),
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 15,
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 7,
                                    ),
                                    child: Text(
                                      "Нікнейм",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      TextFormField(
                                        onChanged: (val) {
                                          password = val;
                                        },
                                        // obscuringCharacter: '*',
                                        obscureText: false,
                                        validator: (value) {
                                          return value.length > 2
                                              ? null
                                              : "Будь-ласка введіть нікнейм не менше 3х символів";
                                        },
                                        controller:
                                            nicknameTextEditingController,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                          hintText: "Введіть нікнейм",
                                          contentPadding: new EdgeInsets.only(
                                              left: 20,
                                              top: 21.5,
                                              bottom: 21.5),
                                          hintStyle: TextStyle(
                                              color: Hexcolor('#D3D3D3'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial'),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FE6802'),
                                              width: 1,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Colors.white54,
                                              width: 1,
                                            ),
                                          ),
                                          focusedErrorBorder:
                                              OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          fillColor: Colors.white,
                                          filled: true,
                                        ),
                                        style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Arial'),
                                      ),
                                      // Positioned(
                                      //   right: 22.4,
                                      //   top: 18,
                                      //   child: GestureDetector(
                                      //     onTap: () {
                                      //       setState(() {
                                      //         isVisible = !isVisible;
                                      //       });
                                      //     },
                                      //     child: !isVisible
                                      //         ? Image.asset(
                                      //             'assets/fg_images/4_enter_visible.png',
                                      //             color: Hexcolor('#9B9B9B'),
                                      //             width: 24,
                                      //             height: 24,
                                      //           )
                                      //         : Image.asset(
                                      //             'assets/fg_images/4_enter_hidden.png',
                                      //             color: Hexcolor('#9B9B9B'),
                                      //             width: 24,
                                      //             height: 24,
                                      //           ),
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 15,
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 7,
                                    ),
                                    child: Text(
                                      "Пароль",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      TextFormField(
                                        onChanged: (val) {
                                          password = val;
                                        },
                                        obscuringCharacter: '*',
                                        obscureText: isVisible ? false : true,
                                        validator: (value) {
                                          if (value.length < 6) {
                                            return "Будь-ласка введіть пароль не менше 6 символів";
                                          }
                                          if (passwordTextEditingController
                                                  .text !=
                                              passwordTextEditingController2
                                                  .text) {
                                            return "Паролі не співпадають";
                                          }
                                          if (passwordTextEditingController
                                                      .text !=
                                                  passwordTextEditingController2
                                                      .text &&
                                              value.length < 6) {
                                            return "Паролі не співпадають.\nБудь-ласка введіть пароль не менше 6 символів";
                                          }
                                          return null;
                                        },
                                        controller:
                                            passwordTextEditingController,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                          hintText: "Введіть пароль",
                                          contentPadding: new EdgeInsets.only(
                                              left: 20,
                                              top: 21.5,
                                              bottom: 21.5),
                                          hintStyle: TextStyle(
                                              color: Hexcolor('#D3D3D3'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial'),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FE6802'),
                                              width: 1,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Colors.white54,
                                              width: 1,
                                            ),
                                          ),
                                          focusedErrorBorder:
                                              OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          fillColor: Colors.white,
                                          filled: true,
                                        ),
                                        style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Arial'),
                                      ),
                                      Positioned(
                                        right: 22.4,
                                        top: 18,
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isVisible = !isVisible;
                                            });
                                          },
                                          child: !isVisible
                                              ? Image.asset(
                                                  'assets/fg_images/4_enter_visible.png',
                                                  color: Hexcolor('#9B9B9B'),
                                                  width: 24,
                                                  height: 24,
                                                )
                                              : Image.asset(
                                                  'assets/fg_images/4_enter_hidden.png',
                                                  color: Hexcolor('#9B9B9B'),
                                                  width: 24,
                                                  height: 24,
                                                ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 15,
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 7,
                                    ),
                                    child: Text(
                                      "Підтвердіть пароль",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      TextFormField(
                                        onChanged: (val) {
                                          password2 = val;
                                        },
                                        obscuringCharacter: '*',
                                        obscureText: isVisible2 ? false : true,
                                        validator: (value) {
                                          if (value.length < 6) {
                                            return "Будь-ласка введіть пароль не менше 6 символів";
                                          }
                                          if (passwordTextEditingController
                                                  .text !=
                                              passwordTextEditingController2
                                                  .text) {
                                            return "Паролі не співпадають";
                                          }
                                          if (passwordTextEditingController
                                                      .text !=
                                                  passwordTextEditingController2
                                                      .text &&
                                              value.length < 6) {
                                            return "Паролі не співпадають.\nБудь-ласка введіть пароль не менше 6 символів";
                                          }
                                          return null;
                                        },
                                        controller:
                                            passwordTextEditingController2,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                          hintText: "Введіть пароль",
                                          contentPadding: new EdgeInsets.only(
                                              left: 20,
                                              top: 21.5,
                                              bottom: 21.5),
                                          hintStyle: TextStyle(
                                              color: Hexcolor('#D3D3D3'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial'),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FE6802'),
                                              width: 1,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Colors.white54,
                                              width: 1,
                                            ),
                                          ),
                                          focusedErrorBorder:
                                              OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          fillColor: Colors.white,
                                          filled: true,
                                        ),
                                        style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Arial'),
                                      ),
                                      Positioned(
                                        right: 22.4,
                                        top: 18,
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isVisible2 = !isVisible2;
                                            });
                                          },
                                          child: !isVisible2
                                              ? Image.asset(
                                                  'assets/fg_images/4_enter_visible.png',
                                                  color: Hexcolor('#9B9B9B'),
                                                  width: 24,
                                                  height: 24,
                                                )
                                              : Image.asset(
                                                  'assets/fg_images/4_enter_hidden.png',
                                                  color: Hexcolor('#9B9B9B'),
                                                  width: 24,
                                                  height: 24,
                                                ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),

                              // ),

                              Container(
                                margin: EdgeInsets.only(bottom: 15, top: 45),
                                height: 60,
                                width: MediaQuery.of(context).size.width - 40,
                                child: RaisedButton(
                                  color: Hexcolor('#FE6802'),
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0)),
                                  child: Text("Зареєструватися",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontFamily: 'Arial',
                                          color: Colors.white,
                                          letterSpacing: 1.09,
                                          fontSize: 17)),
                                  onPressed: () async {
                                    final SharedPreferences sharedPreferences =
                                        await SharedPreferences.getInstance();
                                    sharedPreferences.setString('email',
                                        emailTextEditingController.text);
                                    register();
                                    // getInfo();
                                  },
                                ),
                              ),

                              Text(
                                message,
                                style: TextStyle(color: Colors.white),
                              ),

                              Container(
                                margin: EdgeInsets.only(bottom: 15),
                                child: Text(
                                  'АБО',
                                  style: TextStyle(
                                      color: Hexcolor('#FFFFFF'),
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1.09,
                                      fontFamily: 'Arial'),
                                ),
                              ),

                              Row(
                                children: [
                                  Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            25,
                                    child: RaisedButton(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/fg_images/4_enter_facebook.png',
                                            width: 14,
                                            height: 25,
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            'Facebook',
                                            style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              color: Hexcolor('#FFFFFF'),
                                            ),
                                          )
                                        ],
                                      ),
                                      color: Hexcolor('#3B5998'),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(14.0)),
                                      onPressed: () {
                                        signUpWithFacebook();
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            25,
                                    child: RaisedButton(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/fg_images/4_enter_google.png',
                                            width: 30,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            'Google',
                                            style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              color: Hexcolor('#1E2E45'),
                                            ),
                                          )
                                        ],
                                      ),
                                      color: Hexcolor('#FFFFFF'),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(14.0)),
                                      onPressed: () {
                                        signUpWithGoogle();
                                        // signUpWithGoogle().then((value) =>
                                        //     Navigator.pushReplacementNamed(
                                        //         context,
                                        //         '/5_myBottomBar.dart'));
                                      },
                                    ),
                                  ),
                                ],
                              ),

                              // Container(
                              //   margin: EdgeInsets.only(top: 15),
                              //   height: 60,
                              //   width: MediaQuery.of(context).size.width - 40,
                              //   child: RaisedButton(
                              //     color: Hexcolor('#FFFFFF'),
                              //     shape: new RoundedRectangleBorder(
                              //         borderRadius:
                              //             new BorderRadius.circular(14.0)),
                              //     child: Row(
                              //       mainAxisAlignment: MainAxisAlignment.center,
                              //       crossAxisAlignment:
                              //           CrossAxisAlignment.center,
                              //       children: [
                              //         Image.asset(
                              //           'assets/fg_images/4_enter_apple.png',
                              //           width: 21.34,
                              //           height: 25.34,
                              //         ),
                              //         SizedBox(
                              //           width: 20,
                              //         ),
                              //         Text(
                              //           'Увійти з Apple',
                              //           style: TextStyle(
                              //             fontFamily: 'Arial',
                              //             fontSize: 17,
                              //             fontWeight: FontWeight.w700,
                              //             color: Hexcolor('#1E2E45'),
                              //           ),
                              //         )
                              //       ],
                              //     ),
                              //     onPressed: () {
                              //       print('Apple');
                              //     },
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                        top: 20,
                        bottom: 20,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Вже є аккаунт? ',
                            style: TextStyle(
                              fontFamily: 'Arial',
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                              color: Hexcolor('#FFFFFF'),
                            ),
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              Navigator.pushNamed(context, '/4_enter');
                            },
                            child: Text(
                              'Увійти',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontFamily: 'Arial',
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                color: Hexcolor('#FFFFFF'),
                              ),
                            ),
                          )
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 20,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Використовуючи цей додаток, ви погоджуєтесь з нашими',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Arial',
                            color: Colors.white,
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  _launchUrl('https://freegen.net/');
                                },
                                child: Text(
                                  'Умовами',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Arial',
                                    color: Hexcolor('#FE6802'),
                                    fontSize: 12,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                              Text(
                                ' і ',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'Arial',
                                  color: Colors.white,
                                  fontSize: 12,
                                ),
                              ),
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  _launchUrl('https://freegen.net/');
                                },
                                child: Text(
                                  'Політикою конфіденційності',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Arial',
                                    color: Hexcolor('#FE6802'),
                                    fontSize: 12,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                            ])
                      ],
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
