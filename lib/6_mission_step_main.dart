import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
// import 'package:vibration/vibration.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'dart:async';
import '6_home_steps_json.dart';
import '6_home_submissions_progress_json.dart';
import '6_home_submissions_json.dart';

import 'package:flutter_free_gen_reality/services/services.dart';

class MissionStepMain extends StatefulWidget {
  @override
  _MissionStepMainState createState() => _MissionStepMainState();
}

class _MissionStepMainState extends State<MissionStepMain> {
  bool isApiCallProcess = false;
  bool isLoading = false;
  String stepType;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  Future<Stup> getStepData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    // var stepId = sharedPreferences.get('stepId');
    String token = sharedPreferences.get("token");
    int subMissionId = sharedPreferences.get("subMissionId");
    var userId = sharedPreferences.get('id');

    String url = 'http://generation-admin.ehub.com.ua/api/submission/' +
        '$subMissionId' +
        '/step/list?active=true&user_id=' +
        '$userId';
    try {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(url);
      print('STEPS _____________________________________________STATUS CODE');
      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        final Steps steps = stepsFromJson(response.body);
        for (final stup in steps.steps) {
          String url =
              'http://generation-admin.ehub.com.ua/api/step/progress?user_id=' +
                  '$userId' +
                  '&step_id=' +
                  stup.id.toString();
          print(url);
          final response = await http.get(url, headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          });
          Map<String, dynamic> responseJson = jsonDecode(response.body);
          print(responseJson);

          // if (!responseJson.containsValue("INITIALIZED")) {
          //       print('all steps done');}

          if (response.statusCode == 200) {
            if (responseJson["trackStatus"] == "INITIALIZED") {
              if (stup.stepType == 'INFORMATIVE') {
                sharedPreferences.setInt('stepId', stup.id);
                Navigator.pushNamed(context, '/6_mission_step_info.dart');
              } else {
                if (stup.stepType == 'SCANNING') {
                  sharedPreferences.setInt('stepId', stup.id);
                  Navigator.pushNamed(
                      context, '/6_mission_step_scan_info.dart');
                } else {
                  if (stup.stepType == 'QUIZ') {
                    sharedPreferences.setInt('stepId', stup.id);
                    Navigator.pushNamed(context, '/6_mission_step_quiz.dart');
                  } else {
                    if (stup.stepType == 'SELFIE') {
                      sharedPreferences.setInt('stepId', stup.id);
                      Navigator.pushNamed(
                          context, '/6_mission_step_selfie.dart');
                      print('SELFIE');
                    }
                  }
                }
              }
              setState(() {
                isLoading = false;
              });
            } else {
              continue;
            }
            return Stup();
          }
        }
      } else {
        return Stup();
      }
    } catch (e) {
      print(e);
      return Stup();
    }
  }

  Future<Stup> finishStep() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    var usedId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");

    // String url = 'http://generation-admin.ehub.com.ua/api/step/' + '$stepId';
    String url =
        'http://generation-admin.ehub.com.ua/api/step/checkAnswer?user_id=' +
            '$usedId' +
            '&step_id=' +
            '$stepId';
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      print(responseJson);
      if (response.statusCode == 200) {
        print('Page done');
        setState(() {
          isLoading = false;
        });
        // Navigator.pushNamed(context, '/6_mission_map.dart');
      } else {
        print('error');
        setState(() {
          isLoading = false;
        });
      }
    }

    getMission();
    return Stup();
  }

  // checkAllSubmissions() {
  //   List<String> progressResult = new List<String>();

  //   for (final submission in subMissions) {
  //     for (final subMissionsProgress in subMissionsProgresses) {
  //       if (subMissionsProgress.submissionId == submission.id) {
  //         if (subMissionsProgress.trackStatus == 'FINISHED') {
  //           setState(() {
  //             progressResult.add('FINISHED');
  //           });
  //         } else {
  //           setState(() {
  //             progressResult.add('INITIALIZED');
  //           });
  //         }
  //       }
  //     }
  //   }
  //   print(progressResult);
  //   if (progressResult.contains('INITIALIZED')) {
  //     print('go map');
  //     Navigator.pushNamed(context, '/6_mission_map.dart');
  //   } else {
  //     print('go main');
  //     Navigator.pushNamed(context, '/5_myBottomBar.dart');
  //   }
  // }

  setSubmissionDone() async {
    setState(() {
      isApiCallProcess = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var usedId = sharedPreferences.get('id');
    var subMissionId = sharedPreferences.get('subMissionId');
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/submission/progress/isFinished?submission_id=$subMissionId&user_id=$usedId';
    print(url);
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      // checkAllSubmissions();
      setState(() {
        isApiCallProcess = false;
      });

      Navigator.pushNamed(context, '/6_mission_map.dart');
    } else {
      print('error');
      setState(() {
        isApiCallProcess = false;
      });
    }
  }

  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  width: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      subMissionPoints != null && subMissionPoints != 0
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Вам нараховано ',
                                  style: TextStyle(
                                    color: Hexcolor('#747474'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Flexible(
                                  child: Text(
                                    subMissionPoints.toInt().toString() +
                                        ' ' +
                                        subMissionCurrency.toString(),
                                    style: TextStyle(
                                      color: Hexcolor('#58B12D'),
                                      fontSize: 20,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w900,
                                      letterSpacing: 1.089,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            )
                          : Container(),
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            ),
                        child: Text(
                          'Вы молодець, пройшли крок підміссії.\nНатисніть кнопку для продовження',
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на наступну підмісію",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            setSubmissionDone();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<User> changeTus(
      double value, String operation, String typeCurrency) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": typeCurrency,
        "operation": operation,
        "amount": value,
        "description": "mission reward"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');

    if (response.statusCode == 200) {
      print(response.body);
    } else {
      throw Exception('Failed to update User.');
    }
  }

  double subMissionPoints;
  var subMissionCurrencyType;
  var subMissionCurrency;
  getSubmissionData() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();

    subMissionPoints = sharedPreferences.get("subMissionPoints");
    subMissionCurrencyType = sharedPreferences.get("subMissionCurrencyType");

    var userId = sharedPreferences.get("id");
    var subMissionId = sharedPreferences.get('subMissionId');

    var subMissionDone =
        sharedPreferences.get('SubMission$subMissionId' + 'Done' + '$userId');

    String token = sharedPreferences.get("token");
    String urlCurrencies =
        'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies';
    final responseCurrencies = await http.get(urlCurrencies, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    Map<String, dynamic> responseJsonCurrencies =
        jsonDecode(responseCurrencies.body);
    if (responseCurrencies.statusCode == 200) {
      for (int i = 0; i < responseJsonCurrencies["publication"].length; i++) {
        if (subMissionCurrencyType ==
            responseJsonCurrencies["publication"][i]["id"].toString()) {
          setState(() {
            subMissionCurrency =
                responseJsonCurrencies["publication"][i]["titleUa"];
          });

          print(subMissionCurrency);
        }
      }
      if (subMissionPoints != null) {
        if (subMissionDone == '' || subMissionDone == null) {
          changeTus(subMissionPoints, "DEPOSIT", subMissionCurrency);
          sharedPreferences.setBool(
              'SubMission$subMissionId' + 'Done' + '$userId', true);
          print('deposit done');
        } else {
          print('you cant to reseive many times');
        }
      }

      showInfoWindow();
    }
  }

  bool subMissionCompleted = true;
  checkSubmissionDone() {
    if (subMissionCompleted == true) {
      print(
          'SUBMISSION DONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2st step');
      getSubmissionData();
      // перед этим окном getSubmissionData и начислять или не начислять баллы!
    } else {
      print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2st step');
      print(
          'SUBMISSION NOT DONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2st step');
    }
  }

  Future<Stup> checkAllStepsDone() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var usedId = sharedPreferences.get('id');
    var subMissionProgressId = sharedPreferences.get('subMissionProgressId');
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/step/progress/list?user_id=$usedId&subMissionProgress_id=$subMissionProgressId';
    print('----------------------------- ALL STEPS  ------------------------------');
    print(url);
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    List<dynamic> responseJson = jsonDecode(response.body);

    print(responseJson);
    print(
        '!!!!!!!!!!!!!!!!!!!!! CHECK SUBMISSION PROGRESS !!!!!!!!!! SUBMISSION PROGRESS ID = $subMissionProgressId!!!!!!!!!!!!');
    print(response.statusCode);    
    if (response.statusCode == 200) {
      for (int i = 0; i < responseJson.length; i++) {
        if (responseJson[i]["trackStatus"] == "INITIALIZED") {
          print(responseJson[i]["trackStatus"]);
          setState(() {
            subMissionCompleted = false;
            print(
                '!!!!!!!!!!!!!!!!!! SUBMISSION NOT DONE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1st step');
          });
        }
        // checkSubmissionDone();
      }
      // print('Info progress speps done');

      setState(() {
        isLoading = false;
      });
      // checkSubmissionDone();
    } else {
      print('error to get info');
      setState(() {
        isLoading = false;
      });
    }
    return Stup();
  }

  getSubmissionProgressId() async {
    for (int i = 0; i < subMissionsProgresses.length; i++) {
      SubMissionProgresses subMission = subMissionsProgresses[i];
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var subMissionId = sharedPreferences.get('subMissionId');
      if (subMissionId == subMission.submissionId) {
        final SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setInt('subMissionProgressId', subMission.id);
        print(
            "@@@@@@@@@@@@@@@@@@@@@@@ subMissionId @@@@@@@@@@@@@ subMissionProgressId @@@@@@@@@@@@@@");
        print(subMissionId);
        print(subMission.id);
      }
    }
  }

  List<SubMissionProgresses> subMissionsProgresses;
  List<Submission> subMissions;

  bool loadingL = true;
  @override
  void initState() {
    ServicesSubMissions.getSubMissionData().then((list) {
      setState(() {
        subMissions = list.submissions;
        // ServicesSubmissionProgresses.getProressSubmissions().then((list) {
        //   setState(() {
        //     subMissionsProgresses = list;
        //     loadingL = false;
        //   });
        //   getSubmissionProgressId();
        // });
      });
    });

    ServicesSubmissionProgresses.getProressSubmissions().then((list) {
      setState(() {
        subMissionsProgresses = list;
        loadingL = false;
      });
      getSubmissionProgressId();
      getStepData().then(
        (value) => checkAllStepsDone().then((value) => checkSubmissionDone()));
    });

    // getStepData().then(
    //     (value) => checkAllStepsDone().then((value) => checkSubmissionDone()));

    super.initState();
  }

  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Hexcolor('#7D5AC2'),
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Hexcolor('#FE6802'),
                  ),
                ),
              )
            : Container());
  }
}
