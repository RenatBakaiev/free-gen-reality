import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ProfileQrCode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: Hexcolor('#7D5AC2'),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 96),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          backgroundColor: Hexcolor('#F2F2F2'),
                          radius: 50,
                          backgroundImage: AssetImage(
                              'assets/fg_images/10_profile_edit_default_pic.png'),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: Text(
                            'ROLE_USER',
                            style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 22,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1.04),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Image.asset(
                    'assets/fg_images/10_profile_qr_qr_code.png',
                    width: 200,
                    height: 200,
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(14.0),
                      ),
                      child: Text("Готово",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Arial',
                              color: Colors.white,
                              letterSpacing: 1.09,
                              fontSize: 17)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ]),
          )),
    );
  }
}
