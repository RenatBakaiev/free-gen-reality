import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '6_home_missions_json.dart';
import '6_home_missions_favorite_json.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import '10_profile_currencies_json.dart';
import '10_profile_json.dart';
import '6_home_submissions_json.dart';
import '6_home_submissions_progress_json.dart';

class MissionDescriptionDatabase extends StatefulWidget {
  @override
  _MissionDescriptionDatabaseState createState() =>
      _MissionDescriptionDatabaseState();
}

class _MissionDescriptionDatabaseState
    extends State<MissionDescriptionDatabase> {
  final formKey = GlobalKey<FormState>(); // PROMOCODE
  TextEditingController promocodeTextEditingController =
      new TextEditingController();
  bool isPromoRight = false;
  bool missionUnlocked = false;

  bool isFavorite = false;
  bool isApiCallProcess = false;

  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

  bool isPlaying = false;
  AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();

  void _getAudio() async {
    var url = "http://generation-admin.ehub.com.ua/api/file/downloadFile/" +
        "$missionSound";
    var res = await audioPlayer.play(url, isLocal: true);
    if (res == 1) {
      setState(() {
        isPlaying = true;
      });
    }
  }

  void _stopFile() {
    audioPlayer?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer.pause();
    } else {
      audioPlayer.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  void _getVideo() {
    _controller = VideoPlayerController.network(
        'http://generation-admin.ehub.com.ua/api/file/downloadFile/$missionVideo');
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    _controller.play();
  }

  String userStatus;

  bool loading = false;
  String missionName;
  int missionTime;
  int missionSteps;
  int missionDoneSteps;
  String missionImage;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';
  String missionDescription;
  bool missionDone;
  String missionVideo = '';
  String missionSound = '';
  var missionLatitude;
  var missionLongitude;
  var missionAccessType;
  var missionAccessGranted;
  List<String> missionStatuses = new List<String>();
  String missionPromoCode = '';
  var missionPoints;
  var missionCurrency;

  String utf8convert(String text) {
    List<int> bytes = text.toString().codeUnits;
    return utf8.decode(bytes);
  }

  List<Publication> currencies;
  getCurrencyList() {
    ServicesCurrencies.getCurrencies().then((list) {
      setState(() {
        currencies = list.publication;
      });
    });
  }

  List<FavoriteMissions> favoriteMissions = List<FavoriteMissions>();

  Future<Mission> getMissionData() async {
    setState(() {
      loading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var missionId = sharedPreferences.get('missionId');
    String token = sharedPreferences.get("token");
    bool missionDoneMemory = sharedPreferences.get("missionCompleted");
    String userStatus = sharedPreferences.get("userStatus");
    String url =
        'http://generation-admin.ehub.com.ua/api/mission/' + '$missionId';
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          missionId = responseJson["id"];
          missionName = utf8.decode(responseJson["titleUa"].runes.toList());
          missionTime = responseJson["duration"];

          List<bool> activeSubmissions = new List<bool>();
          for (final submission in responseJson["subMissionList"]) {
            if (submission["active"] == true) {
              activeSubmissions.add(submission["active"]);
            }
          }
          missionSteps = activeSubmissions.length;
          print('ALL SUBMISSIONS');
          print(missionSteps);

          if (responseJson["imageUrl"] == null ||
              responseJson["imageUrl"] == '') {
            missionImage =
                'http://generation-admin.ehub.com.ua/api/file/downloadFile/default.png';
          } else {
            missionImage =
                'http://generation-admin.ehub.com.ua/api/file/downloadFile/' +
                    utf8.decode(responseJson["imageUrl"].runes.toList());
          }
          missionDescription = utf8convert(responseJson["descriptionUa"]);
          missionDone = missionDoneMemory;

          missionVideo = responseJson["videoUrl"];
          if (missionVideo != "" || missionVideo != null) {
            _getVideo();
          }

          missionSound = responseJson["audioUrl"];
          if ((missionSound != "" || missionSound != null) &&
              (missionVideo == "" || missionVideo == null)) {
            _getAudio();
          }

          missionLatitude = double.parse(responseJson["location"]["latitude"]);
          missionLongitude =
              double.parse(responseJson["location"]["longitude"]);
          getPosition();
          missionAccessType = responseJson["accessType"];

          if (missionAccessType == 'BYSTATUS') {
            for (final status in responseJson["statuses"]) {
              if (status["active"] == true) {
                missionStatuses
                    .add(utf8.decode(status["titleUa"].runes.toList()));
              }
            }
            print(missionStatuses);
            print(userStatus);
            if (missionStatuses.contains(userStatus)) {
              missionAccessGranted = true;
            } else {
              missionAccessGranted = false;
            }
            print(missionAccessGranted);
          }

          if (missionAccessType == 'PROMO') {
            missionPromoCode =
                utf8.decode(responseJson["promoCode"].runes.toList());
            print(missionPromoCode);
          }

          if (missionAccessType == 'PAID') {
            missionPoints = responseJson["price"];
            print(
                '**************************** MISSION PRICE ********************************************');

            for (int i = 0; i < currencies.length; i++) {
              if (responseJson["priceCurrency"] ==
                  currencies[i].id.toString()) {
                missionCurrency = currencies[i].contentShortUa.toUpperCase();
              }
            }
            print(missionPoints);
            print(missionCurrency);
            getUserData();
          }
          getUserData();
          ServicesFavoriteMissions.getFavoriteMissions().then((list) {
            setState(() {
              favoriteMissions = list;
              for (var favoriteMission in favoriteMissions) {
                if (favoriteMission.id == missionId) {
                  setState(() {
                    isFavorite = true;
                  });
                }
              }
            });
          });

          loading = false;
        });
      } else {
        print('error');
      }
    }

    getMission();
    return Mission();
  }

  bool loadingPosition = true;
  var currentLocation;
  var distanceInKilometers;

  getPosition() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        var distanceInMeters;
        distanceInMeters = Geolocator.distanceBetween(currentLocation.latitude,
            currentLocation.longitude, missionLatitude, missionLongitude);
        distanceInKilometers = (distanceInMeters * 0.001).toStringAsFixed(2);
        loadingPosition = false;
      });
    });
  }

  unlockMission() {
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        missionUnlocked = true;
      });
    });
  }

  showUnlockWindowPromocode() {
    showDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Container(
                      // width: MediaQuery.of(context).size.width - 100,
                      height: 470,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              top: 20,
                            ),
                            child: Text(
                              'Поздоровляємо!',
                              style: TextStyle(
                                color: Hexcolor('#1E2E45'),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Arial',
                              ),
                            ),
                          ),
                          Expanded(
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 15,
                                      bottom: 15,
                                    ),
                                    child: Text(
                                      'Ви розблокували місію',
                                      style: TextStyle(
                                        color: Hexcolor('#59B32D'),
                                        fontSize: 20,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    height: 165,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        image: DecorationImage(
                                          image: NetworkImage(missionImage),
                                          fit: BoxFit.cover,
                                        )),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 15,
                                      bottom: 15,
                                    ),
                                    child: Text(
                                      missionName,
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                        fontFamily: 'Arial',
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Text(
                                    'Вам доступна нова місія',
                                    style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                bottom: 20, right: 20, left: 20),
                            child: Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width - 40,
                              child: RaisedButton(
                                child: Text(
                                  "Закрити",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                color: Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                  unlockMission();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: -130,
                      right: -30,
                      child: Image.asset(
                        'assets/fg_images/error_mission_icon_unlock.png',
                        width: 200,
                        height: 200,
                      ),
                    )
                  ],
                ));
          });
        });
  }

  showLockedWindowWithPromocode() {
    showDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        barrierDismissible: false,
        context: context,
        builder: (context) {
          var _blankFocusNode2 = new FocusNode();
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(_blankFocusNode2);
                },
                child: Container(
                  // width: MediaQuery.of(context).size.width - 100,
                  height: 550,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Місія недоступна',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Arial',
                          ),
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                  top: 20,
                                  bottom: 15,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Місія доступна тільки',
                                      style: TextStyle(
                                        color: Hexcolor('#747474'),
                                        fontSize: 20,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          'по ',
                                          style: TextStyle(
                                            color: Hexcolor('#747474'),
                                            fontSize: 20,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        Text(
                                          'промокоду',
                                          style: TextStyle(
                                            color: Hexcolor('#FF1E1E'),
                                            fontSize: 20,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 20,
                                  right: 20,
                                ),
                                width: MediaQuery.of(context).size.width,
                                height: 165,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)),
                                    image: DecorationImage(
                                      image: NetworkImage(missionImage),
                                      fit: BoxFit.cover,
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: 15,
                                  bottom: 15,
                                ),
                                child: Text(
                                  missionName,
                                  style: TextStyle(
                                    color: Hexcolor('#1E2E45'),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Arial',
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      // height: 60,
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          45,
                                      child: Form(
                                        key: formKey,
                                        child: TextFormField(
                                          onChanged: (val) {},
                                          validator: (value) {
                                            return value == missionPromoCode
                                                ? null
                                                : 'Невірний промокод';
                                          },
                                          controller:
                                              promocodeTextEditingController,
                                          textAlign: TextAlign.left,
                                          decoration: InputDecoration(
                                            hintText: "Промокод",
                                            contentPadding: new EdgeInsets.only(
                                                left: 20,
                                                top: 20.5,
                                                bottom: 20.5),
                                            hintStyle: TextStyle(
                                              color: Hexcolor('#D3D3D3'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial',
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor(isPromoRight
                                                    ? '#59B32D'
                                                    : '#FE6802'),
                                                width: 1,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#E6E6E6'),
                                                width: 1,
                                              ),
                                            ),
                                            focusedErrorBorder:
                                                OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Colors.red,
                                                width: 1,
                                              ),
                                            ),
                                            errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Colors.red,
                                                width: 1,
                                              ),
                                            ),
                                            fillColor: Colors.white,
                                            filled: true,
                                          ),
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Arial',
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      height: 60,
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          45,
                                      child: RaisedButton(
                                        child: Text(
                                          "Застосувати",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 17,
                                            fontWeight: FontWeight.w700,
                                            fontFamily: 'Arial',
                                          ),
                                        ),
                                        color: Hexcolor('#FE6802'),
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    14.0)),
                                        onPressed: () {
                                          if (formKey.currentState.validate()) {
                                            setState(() {
                                              isPromoRight = true;
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              isPromoRight
                                  ? Container(
                                      margin: EdgeInsets.only(
                                        left: 40,
                                        top: 7,
                                        bottom: 15,
                                      ),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Правильний промокод',
                                        style: TextStyle(
                                          color: Hexcolor('#59B32D'),
                                          fontFamily: 'Arial',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ))
                                  : Container(
                                      height: 15,
                                    ),
                              !isPromoRight
                                  ? SizedBox(
                                      height: 30,
                                    )
                                  : SizedBox(
                                      height: 15,
                                    )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Arial',
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            if (isPromoRight) {
                              showUnlockWindowPromocode();
                              setState(() {
                                missionUnlocked = true;
                              });
                            } else {
                              Navigator.of(context).pop();
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
        });
  }

  var userPointsforMission;
  var userId;

  getUserData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");

    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);

        if (response.statusCode == 200) {
          setState(() {
            userId = responseJson["id"];
            print(userId);
          });
          for (int i = 0; i < responseJson["accounts"].length; i++) {
            if (missionCurrency == responseJson["accounts"][i]["currency"]) {
              userPointsforMission =
                  responseJson["accounts"][i]["accountBalance"];
            }
          }
          print(
              '**************************** USER POINTS ********************************************');
          print(userPointsforMission);

          final User user = userFromJson(response.body);
          loading = false;
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  checkAllSubmissions() {
    List<String> progressResult = new List<String>();
    for (final submission in subMissions) {
      for (final subMissionsProgress in subMissionsProgresses) {
        if (subMissionsProgress.submissionId == submission.id) {
          if (subMissionsProgress.trackStatus == 'FINISHED') {
            setState(() {
              progressResult.add('FINISHED');
            });
          }
        }
      }
    }
    print('DONE SUBMISSIONS');
    setState(() {
      missionDoneSteps = progressResult.length;
      loadingSteps = false;
    });
  }

  List<SubMissionProgresses> subMissionsProgresses;
  List<Submission> subMissions;
  bool loadingSteps = true;

  @override
  void initState() {
    ServicesSubMissions.getSubMissionData().then((list) {
      setState(() {
        subMissions = list.submissions;
        ServicesSubmissionProgresses.getProressSubmissions().then((list) {
          setState(() {
            subMissionsProgresses = list;
            checkAllSubmissions();
          });
        });
      });
    });
    getCurrencyList();
    getMissionData();

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    Future<void> share() async {
      await FlutterShare.share(
        title: 'Завантажуйте новий додаток і спробуйте виконати місію \n\n' +
            "'" +
            missionName +
            "'" +
            '\n',
        linkUrl:
            'https://play.google.com/store/apps/details?id=com.freegen.flutterfreegenapp',
      );
      // var request = await HttpClient().getUrl(Uri.parse(
      //     missionImage));
      // var response = await request.close();
      // Uint8List bytes = await consolidateHttpClientResponseBytes(response);
      // await Share.file('ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg', text: missionName,);
    }

    showWindowQuizDone() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Завдання пройдене',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Text(
                          'Ви не можете повторно пройти це завдання',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                            height: 1.4,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Закрити",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 1.05),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          });
    }

    showLockedWindow() {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            var _blankFocusNode2 = new FocusNode();
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    FocusScope.of(context).requestFocus(_blankFocusNode2);
                  },
                  child: Container(
                    height: 400,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                            top: 20,
                            bottom: 15,
                          ),
                          child: Text(
                            'Місія недоступна',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Arial',
                            ),
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  height: 165,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      image: DecorationImage(
                                        image: NetworkImage(missionImage),
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                  ),
                                  child: Text(
                                    missionName,
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Закрити",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Arial',
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
          });
    }

    showLockedStatusWindow() {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            var _blankFocusNode2 = new FocusNode();
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    FocusScope.of(context).requestFocus(_blankFocusNode2);
                  },
                  child: Container(
                    height: 520,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Місія недоступна',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Arial',
                            ),
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Місія доступна для статусу',
                                        style: TextStyle(
                                          color: Hexcolor('#747474'),
                                          fontSize: 20,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 5,
                                            left: 20,
                                            right: 20,
                                            bottom: 5,
                                          ),
                                          height: 45,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              40,
                                          child: GridView.builder(
                                              physics: BouncingScrollPhysics(),
                                              gridDelegate:
                                                  SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisCount:
                                                    missionStatuses.length == 1
                                                        ? 1
                                                        : missionStatuses
                                                                    .length ==
                                                                2
                                                            ? 2
                                                            : 3,
                                                childAspectRatio: (100 / 20),
                                              ),
                                              itemCount: missionStatuses.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                var status =
                                                    missionStatuses[index];
                                                return Center(
                                                  child: Text(
                                                    status,
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#FF1E1E'),
                                                      fontSize: 20,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                );
                                              })

                                          // ListView.builder(
                                          //     scrollDirection: Axis.horizontal,
                                          //     physics: BouncingScrollPhysics(),
                                          //     itemCount: missionStatuses.length,
                                          //     itemBuilder: (context, index) {
                                          //       var status =
                                          //           missionStatuses[index];

                                          //       return Text(
                                          //         status + ', ',
                                          //         style: TextStyle(
                                          //           color: Hexcolor('#FF1E1E'),
                                          //           fontSize: 20,
                                          //           fontFamily: 'Arial',
                                          //           fontWeight: FontWeight.w700,
                                          //         ),
                                          //       );
                                          //     }),

                                          )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  height: 165,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      image: DecorationImage(
                                        image: NetworkImage(missionImage),
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                  ),
                                  child: Text(
                                    missionName,
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Text(
                                    'Місія недоступна\nдля вашого статусу',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: 'Arial',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Закрити",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Arial',
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
          });
    }

    showLockWindow(balanceDifference) {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            var _blankFocusNode2 = new FocusNode();
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    FocusScope.of(context).requestFocus(_blankFocusNode2);
                  },
                  child: Container(
                    // width: MediaQuery.of(context).size.width - 100,
                    height: 510,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Місія недоступна',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Arial',
                            ),
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 20,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Text(
                                            'Вам не вистачає ',
                                            style: TextStyle(
                                              color: Hexcolor('#747474'),
                                              fontSize: 20,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                          Text(
                                            balanceDifference
                                                    .toInt()
                                                    .toString() +
                                                ' ' +
                                                missionCurrency,
                                            style: TextStyle(
                                              color: Hexcolor('#FF1E1E'),
                                              fontSize: 20,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        'для розблокування цієї місії',
                                        style: TextStyle(
                                          color: Hexcolor('#747474'),
                                          fontSize: 20,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  height: 165,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      image: DecorationImage(
                                        image: NetworkImage(missionImage),
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Text(
                                    missionName,
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Вартість',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Hexcolor('#B9BCC4'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      missionPoints.toInt().toString() +
                                          ' ' +
                                          missionCurrency,
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Hexcolor('#FF1E1E'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Закрити",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Arial',
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
          });
    }

    showUnlockWindow() {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                  insetPadding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Stack(
                    overflow: Overflow.visible,
                    children: [
                      Container(
                        // width: MediaQuery.of(context).size.width - 100,
                        height: 470,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(
                                top: 20,
                              ),
                              child: Text(
                                'Поздоровляємо!',
                                style: TextStyle(
                                  color: Hexcolor('#1E2E45'),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Arial',
                                ),
                              ),
                            ),
                            Expanded(
                              child: SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 15,
                                        bottom: 15,
                                      ),
                                      child: Text(
                                        'Ви розблокували місію',
                                        style: TextStyle(
                                          color: Hexcolor('#59B32D'),
                                          fontSize: 20,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 20,
                                        right: 20,
                                      ),
                                      width: MediaQuery.of(context).size.width,
                                      height: 165,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                          image: DecorationImage(
                                            image: NetworkImage(missionImage),
                                            fit: BoxFit.cover,
                                          )),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 15,
                                        bottom: 15,
                                      ),
                                      child: Text(
                                        missionName,
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 20,
                                          fontWeight: FontWeight.w700,
                                          fontFamily: 'Arial',
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Text(
                                      'Вам доступна нова місія',
                                      style: TextStyle(
                                        color: Hexcolor('#747474'),
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                        fontFamily: 'Arial',
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 20, right: 20, left: 20),
                              child: Container(
                                height: 60,
                                width: MediaQuery.of(context).size.width - 40,
                                child: RaisedButton(
                                  child: Text(
                                    "Закрити",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  color: Hexcolor('#FE6802'),
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0)),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    unlockMission();
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: -130,
                        right: -30,
                        child: Image.asset(
                          'assets/fg_images/error_mission_icon_unlock.png',
                          width: 200,
                          height: 200,
                        ),
                      )
                    ],
                  ));
            });
          });
    }

    Future<User> changeTus(
        double value, String operation, String typeCurrency) async {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      String token = sharedPreferences.get("token");
      int id = sharedPreferences.get("id");
      final http.Response response = await http.post(
        'http://generation-admin.ehub.com.ua/api/account/' +
            operation.toLowerCase() +
            '/' +
            '$id',
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(<String, dynamic>{
          "currency": typeCurrency,
          "operation": operation,
          "amount": value,
          "description": "quiz change"
        }),
      );
      print('http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id');
      if (response.statusCode == 200) {
        print(response.body);
        print('buying done!!!');
      } else {
        throw Exception('Failed to update User.');
      }
    }

    showConfirmWindow() {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            var _blankFocusNode2 = new FocusNode();
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    FocusScope.of(context).requestFocus(_blankFocusNode2);
                  },
                  child: Container(
                    // width: MediaQuery.of(context).size.width - 100,
                    height: 470,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Відкриття нової місії',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Arial',
                            ),
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 20,
                                  ),
                                  child: Text(
                                    'Ви точно хочете купити місію?',
                                    style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontSize: 20,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  height: 165,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      image: DecorationImage(
                                        image: NetworkImage(missionImage),
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Text(
                                    missionName,
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Вартість',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Hexcolor('#B9BCC4'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      missionPoints.toInt().toString() +
                                          ' ' +
                                          missionCurrency,
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Hexcolor('#59B32D'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(bottom: 20, right: 20, left: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 60,
                                width:
                                    MediaQuery.of(context).size.width / 2 - 45,
                                child: RaisedButton(
                                  child: Text(
                                    "Закрити",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  color: Hexcolor('#BEBEBE'),
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0)),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                height: 60,
                                width:
                                    MediaQuery.of(context).size.width / 2 - 45,
                                child: RaisedButton(
                                  child: Text(
                                    "Купити",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  color: Hexcolor('#FE6802'),
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0)),
                                  onPressed: () {
                                    if (missionPoints > userPointsforMission) {
                                      var balanceDifference = (missionPoints -
                                          userPointsforMission);
                                      showLockWindow(balanceDifference);
                                    }
                                    if (missionPoints < userPointsforMission ||
                                        missionPoints == userPointsforMission) {
                                      changeTus(missionPoints, "WITHDRAW",
                                          missionCurrency);
                                      showUnlockWindow();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
          });
    }

    addMissionToFavorites() async {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      String token = sharedPreferences.get("token");
      var missionId = sharedPreferences.get('missionId');
      String id = userId.toString();
      String url = 'http://generation-admin.ehub.com.ua/api/favorites/add';
      final http.Response response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(
            <String, dynamic>{"user_id": id, "mission_id": missionId}),
      );
      print(id);
      print(missionId);
      if (response.statusCode == 200) {
        setState(() {
          isFavorite = !isFavorite;
        });
        print(response.body);
        print('mission added to Favorites');
      } else {
        throw Exception('Failed to add mission.');
      }
    }

    deleteMissionFromFavorites() async {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      String token = sharedPreferences.get("token");
      var missionId = sharedPreferences.get('missionId');
      String id = userId.toString();
      final url =
          Uri.parse('http://generation-admin.ehub.com.ua/api/favorites/delete');
      final request = http.Request("DELETE", url);
      request.headers.addAll(<String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      request.body = jsonEncode({"user_id": id, "mission_id": missionId});
      final response = await request.send();
      if (response.statusCode == 200) {
        setState(() {
          isFavorite = !isFavorite;
        });
        print('mission deleted from Favorites');
      } else {
        throw Exception('Failed to delete mission.');
      }
    }

    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height - 85,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          missionVideo == "" || missionVideo == null
                              ? Stack(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(left: 20),
                                      margin: EdgeInsets.only(
                                          top: 250), // change height
                                      alignment: Alignment.centerLeft,
                                      width: MediaQuery.of(context).size.width,
                                      height: 70,
                                      decoration: BoxDecoration(
                                        color: Hexcolor('#7D5AC2'),
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          missionName,
                                          style: TextStyle(
                                            fontSize: 25,
                                            color: Colors.white,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w900,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Stack(
                                      children: [
                                        Container(
                                          height: 260,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                            image: NetworkImage(missionImage),
                                            fit: BoxFit.cover,
                                          )),
                                          child: Stack(
                                            fit: StackFit.expand,
                                            children: <Widget>[
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Column(
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              left: 10,
                                                              top: 35,
                                                            ),
                                                            child: IconButton(
                                                              icon: Image.asset(
                                                                'assets/fg_images/6_home_search_back.png',
                                                                width: 13.16,
                                                                height: 25,
                                                              ),
                                                              onPressed: () {
                                                                if (_controller
                                                                    .value
                                                                    .isPlaying) {
                                                                  _controller
                                                                      .pause();
                                                                }
                                                                _stopFile();
                                                                Navigator.pushNamed(
                                                                    context,
                                                                    '/5_myBottomBar.dart');
                                                              },
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 35),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .end,
                                                              children: [
                                                                isFavorite ==
                                                                        true
                                                                    ? GestureDetector(
                                                                        behavior:
                                                                            HitTestBehavior.opaque,
                                                                        onTap:
                                                                            () {
                                                                          deleteMissionFromFavorites();
                                                                        },
                                                                        child:
                                                                            Container(
                                                                          margin:
                                                                              EdgeInsets.only(right: 20),
                                                                          child:
                                                                              Image.asset(
                                                                            'assets/fg_images/9_favorites_icon.png',
                                                                            width:
                                                                                19.25,
                                                                            height:
                                                                                27.5,
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : GestureDetector(
                                                                        behavior:
                                                                            HitTestBehavior.opaque,
                                                                        onTap:
                                                                            () {
                                                                          addMissionToFavorites();
                                                                        },
                                                                        child:
                                                                            Container(
                                                                          margin:
                                                                              EdgeInsets.only(right: 20),
                                                                          child:
                                                                              Image.asset(
                                                                            'assets/fg_images/sportGames_icon_favorite.png',
                                                                            width:
                                                                                19.25,
                                                                            height:
                                                                                27.5,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                GestureDetector(
                                                                  behavior:
                                                                      HitTestBehavior
                                                                          .opaque,
                                                                  onTap: () {
                                                                    _stopFile();
                                                                    if (_controller
                                                                        .value
                                                                        .isPlaying) {
                                                                      _controller
                                                                          .pause();
                                                                    }
                                                                    Navigator.pushNamed(
                                                                        context,
                                                                        '/7_map.dart');
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    margin: EdgeInsets.only(
                                                                        right:
                                                                            20),
                                                                    child: Image
                                                                        .asset(
                                                                      'assets/fg_images/sportGames_icon_map.png',
                                                                      width:
                                                                          18.75,
                                                                      height:
                                                                          23.75,
                                                                    ),
                                                                  ),
                                                                ),
                                                                GestureDetector(
                                                                  behavior:
                                                                      HitTestBehavior
                                                                          .opaque,
                                                                  onTap: () {
                                                                    share();
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    margin: EdgeInsets.only(
                                                                        right:
                                                                            20),
                                                                    child: Image
                                                                        .asset(
                                                                      'assets/fg_images/6_home_logo_share.png',
                                                                      width:
                                                                          24.4,
                                                                      height:
                                                                          26.28,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          missionSound == "" ||
                                                                  missionSound ==
                                                                      null
                                                              ? Container()
                                                              : GestureDetector(
                                                                  behavior:
                                                                      HitTestBehavior
                                                                          .opaque,
                                                                  onTap: () {
                                                                    pausePlay();
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    margin:
                                                                        EdgeInsets
                                                                            .only(
                                                                      right: 15,
                                                                    ),
                                                                    child: Image
                                                                        .asset(
                                                                      !isPlaying
                                                                          ? 'assets/fg_images/audio_play.png'
                                                                          : 'assets/fg_images/audio_pause.png',
                                                                      height:
                                                                          50,
                                                                      width: 50,
                                                                    ),
                                                                  ),
                                                                ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                  Column(
                                                    children: [
                                                      Container(
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        margin: EdgeInsets.only(
                                                          left: 20,
                                                          bottom: 15,
                                                          right: 20,
                                                        ),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .end,
                                                          children: [
                                                            (missionAccessType == "CLOSED" ||
                                                                        missionAccessType ==
                                                                            "PROMO" ||
                                                                        missionAccessType ==
                                                                            "PAID" ||
                                                                        (missionAccessType ==
                                                                                "BYSTATUS" &&
                                                                            missionAccessGranted ==
                                                                                false)) &&
                                                                    missionUnlocked ==
                                                                        false
                                                                ? Image.asset(
                                                                    'assets/fg_images/6_home_logo_lock.png',
                                                                    width: 38,
                                                                    height: 38,
                                                                  )
                                                                : (missionAccessType ==
                                                                                "BYSTATUS" &&
                                                                            missionAccessGranted ==
                                                                                true) ||
                                                                        missionUnlocked ==
                                                                            true
                                                                    ? Image
                                                                        .asset(
                                                                        'assets/fg_images/6_home_logo_unlock.png',
                                                                        height:
                                                                            38,
                                                                        width:
                                                                            38,
                                                                      )
                                                                    : Container(),
                                                            loadingSteps
                                                                ? CircularProgressIndicator(
                                                                    backgroundColor:
                                                                        Hexcolor(
                                                                            '#7D5AC2'),
                                                                    valueColor:
                                                                        new AlwaysStoppedAnimation<
                                                                            Color>(
                                                                      Hexcolor(
                                                                          '#FE6802'),
                                                                    ),
                                                                  )
                                                                : missionName !=
                                                                        'Київ проти Корони'
                                                                    ? Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.end,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.end,
                                                                        children: [
                                                                          Text(
                                                                            missionDoneSteps.toString(),
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 36,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w700,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            ' ' +
                                                                                '/ ' +
                                                                                missionSteps.toString(),
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 24,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      )
                                                                    : Container(),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                          left: 20,
                                                          right: 20,
                                                        ),
                                                        height: 40,
                                                        child: Container(),
                                                      ),
                                                      // ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              Align(
                                                alignment:
                                                    Alignment.bottomCenter,
                                                child: Container(
                                                  child: ClipRect(
                                                    child: BackdropFilter(
                                                      filter:
                                                          ui.ImageFilter.blur(
                                                        sigmaX: 3.0,
                                                        sigmaY: 3.0,
                                                      ),
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                          right: 20,
                                                          left: 20,
                                                        ),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            bottomRight:
                                                                Radius.circular(
                                                                    10.0),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    10.0),
                                                          ),
                                                        ),
                                                        alignment:
                                                            Alignment.center,
                                                        height: 40.0,
                                                        child: missionName ==
                                                                'Київ проти Корони'
                                                            ? Container()
                                                            : loadingPosition
                                                                ? Center(
                                                                    child:
                                                                        CircularProgressIndicator(
                                                                      backgroundColor:
                                                                          Hexcolor(
                                                                              '#7D5AC2'),
                                                                      valueColor:
                                                                          new AlwaysStoppedAnimation<
                                                                              Color>(
                                                                        Hexcolor(
                                                                            '#FE6802'),
                                                                      ),
                                                                    ),
                                                                  )
                                                                : Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.end,
                                                                        children: [
                                                                          Image
                                                                              .asset(
                                                                            'assets/fg_images/6_home_logo_time.png',
                                                                            width:
                                                                                18,
                                                                            height:
                                                                                18,
                                                                            color:
                                                                                Hexcolor('#FE6802'),
                                                                          ),
                                                                          SizedBox(
                                                                            width:
                                                                                5,
                                                                          ),
                                                                          Text(
                                                                            missionTime.toString(),
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 13.5,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w600,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            ' хвилин',
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 13.5,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.end,
                                                                        children: [
                                                                          Image
                                                                              .asset(
                                                                            'assets/fg_images/sportGames_icon_favorite.png',
                                                                            width:
                                                                                15,
                                                                            height:
                                                                                18,
                                                                            color:
                                                                                Hexcolor('#FE6802'),
                                                                          ),
                                                                          SizedBox(
                                                                            width:
                                                                                5,
                                                                          ),
                                                                          Text(
                                                                            missionSteps.toString(),
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 13.5,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w600,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            ' об.',
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 13.5,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.end,
                                                                        children: [
                                                                          Image
                                                                              .asset(
                                                                            'assets/fg_images/6_home_icon_flag.png',
                                                                            width:
                                                                                11,
                                                                            height:
                                                                                18.5,
                                                                            color:
                                                                                Hexcolor('#FE6802'),
                                                                          ),
                                                                          SizedBox(
                                                                            width:
                                                                                5,
                                                                          ),
                                                                          Text(
                                                                            distanceInKilometers +
                                                                                ' км',
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 13.5,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w600,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            ' до старту',
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 13.5,
                                                                              color: Colors.white,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              : Container(
                                  // margin: EdgeInsets.only(top: 110),
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(10.0),
                                        bottomLeft: Radius.circular(10.0)),
                                  ),
                                  child: Stack(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          if (_controller.value.isPlaying) {
                                            _controller.pause();
                                          } else {
                                            _controller.play();
                                          }
                                        },
                                        child: FutureBuilder(
                                          future: _initializeVideoPlayerFuture,
                                          builder: (context, snapshot) {
                                            if (snapshot.connectionState ==
                                                ConnectionState.done) {
                                              return AspectRatio(
                                                aspectRatio: _controller
                                                    .value.aspectRatio,
                                                child: VideoPlayer(_controller),
                                              );
                                            } else {
                                              return Center(
                                                child:
                                                    CircularProgressIndicator(),
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                      Positioned(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                left: 10,
                                                top: 35,
                                              ),
                                              child: IconButton(
                                                icon: Image.asset(
                                                  'assets/fg_images/6_home_search_back.png',
                                                  width: 13.16,
                                                  height: 25,
                                                ),
                                                onPressed: () {
                                                  if (_controller
                                                      .value.isPlaying) {
                                                    _controller.pause();
                                                  }
                                                  _stopFile();
                                                  Navigator.pushNamed(context,
                                                      '/5_myBottomBar.dart');
                                                },
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 35),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  isFavorite == true
                                                      ? GestureDetector(
                                                          behavior:
                                                              HitTestBehavior
                                                                  .opaque,
                                                          onTap: () {
                                                            deleteMissionFromFavorites();
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 20),
                                                            child: Image.asset(
                                                              'assets/fg_images/9_favorites_icon.png',
                                                              width: 19.25,
                                                              height: 27.5,
                                                            ),
                                                          ),
                                                        )
                                                      : GestureDetector(
                                                          behavior:
                                                              HitTestBehavior
                                                                  .opaque,
                                                          onTap: () {
                                                            addMissionToFavorites();
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 20),
                                                            child: Image.asset(
                                                              'assets/fg_images/sportGames_icon_favorite.png',
                                                              width: 19.25,
                                                              height: 27.5,
                                                            ),
                                                          ),
                                                        ),
                                                  GestureDetector(
                                                    behavior:
                                                        HitTestBehavior.opaque,
                                                    onTap: () {
                                                      _stopFile();
                                                      if (_controller
                                                          .value.isPlaying) {
                                                        _controller.pause();
                                                      }
                                                      Navigator.pushNamed(
                                                          context,
                                                          '/7_map.dart');
                                                    },
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          right: 20),
                                                      child: Image.asset(
                                                        'assets/fg_images/sportGames_icon_map.png',
                                                        width: 18.75,
                                                        height: 23.75,
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    behavior:
                                                        HitTestBehavior.opaque,
                                                    onTap: () {
                                                      share();
                                                    },
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          right: 20),
                                                      child: Image.asset(
                                                        'assets/fg_images/6_home_logo_share.png',
                                                        width: 24.4,
                                                        height: 26.28,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 40,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              40,
                                          margin: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                            bottom: 15,
                                          ),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                (missionAccessType ==
                                                                "CLOSED" ||
                                                            missionAccessType ==
                                                                "PROMO" ||
                                                            missionAccessType ==
                                                                "PAID" ||
                                                            (missionAccessType ==
                                                                    "BYSTATUS" &&
                                                                missionAccessGranted ==
                                                                    false)) &&
                                                        missionUnlocked == false
                                                    ? Image.asset(
                                                        'assets/fg_images/6_home_logo_lock.png',
                                                        width: 38,
                                                        height: 38,
                                                      )
                                                    : (missionAccessType ==
                                                                    "BYSTATUS" &&
                                                                missionAccessGranted ==
                                                                    true) ||
                                                            missionUnlocked ==
                                                                true
                                                        ? Image.asset(
                                                            'assets/fg_images/6_home_logo_unlock.png',
                                                            height: 38,
                                                            width: 38,
                                                          )
                                                        : Container(),
                                                loadingSteps
                                                    ? CircularProgressIndicator(
                                                        backgroundColor:
                                                            Hexcolor('#7D5AC2'),
                                                        valueColor:
                                                            new AlwaysStoppedAnimation<
                                                                Color>(
                                                          Hexcolor('#FE6802'),
                                                        ),
                                                      )
                                                    : missionName !=
                                                            'Київ проти Корони'
                                                        ? Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                missionDoneSteps
                                                                    .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 36,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                              Text(
                                                                ' ' +
                                                                    '/ ' +
                                                                    missionSteps
                                                                        .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 24,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                        : Container(),
                                              ]),
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0,
                                        child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          margin: EdgeInsets.only(
                                              // bottom: 10,
                                              ),
                                          child: Align(
                                            alignment: Alignment.bottomCenter,
                                            child: Container(
                                              child: ClipRect(
                                                child: BackdropFilter(
                                                  filter: ui.ImageFilter.blur(
                                                    sigmaX: 3.0,
                                                    sigmaY: 3.0,
                                                  ),
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                      right: 20,
                                                      left: 20,
                                                    ),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        bottomRight:
                                                            Radius.circular(
                                                                10.0),
                                                        bottomLeft:
                                                            Radius.circular(
                                                                10.0),
                                                      ),
                                                    ),
                                                    alignment: Alignment.center,
                                                    height: 40.0,
                                                    child: missionName ==
                                                            'Київ проти Корони'
                                                        ? Container()
                                                        : loadingPosition
                                                            ? Center(
                                                                child:
                                                                    CircularProgressIndicator(
                                                                  backgroundColor:
                                                                      Hexcolor(
                                                                          '#7D5AC2'),
                                                                  valueColor:
                                                                      new AlwaysStoppedAnimation<
                                                                          Color>(
                                                                    Hexcolor(
                                                                        '#FE6802'),
                                                                  ),
                                                                ),
                                                              )
                                                            : Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .end,
                                                                    children: [
                                                                      Image
                                                                          .asset(
                                                                        'assets/fg_images/6_home_logo_time.png',
                                                                        width:
                                                                            18,
                                                                        height:
                                                                            18,
                                                                        color: Hexcolor(
                                                                            '#FE6802'),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            5,
                                                                      ),
                                                                      Text(
                                                                        missionTime
                                                                            .toString(),
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              13.5,
                                                                          color:
                                                                              Colors.white,
                                                                          fontFamily:
                                                                              'Arial',
                                                                          fontWeight:
                                                                              FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        ' хвилин',
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              13.5,
                                                                          color:
                                                                              Colors.white,
                                                                          fontFamily:
                                                                              'Arial',
                                                                          fontWeight:
                                                                              FontWeight.w400,
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .end,
                                                                    children: [
                                                                      Image
                                                                          .asset(
                                                                        'assets/fg_images/sportGames_icon_favorite.png',
                                                                        width:
                                                                            15,
                                                                        height:
                                                                            18,
                                                                        color: Hexcolor(
                                                                            '#FE6802'),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            5,
                                                                      ),
                                                                      Text(
                                                                        missionSteps
                                                                            .toString(),
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              13.5,
                                                                          color:
                                                                              Colors.white,
                                                                          fontFamily:
                                                                              'Arial',
                                                                          fontWeight:
                                                                              FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        ' об.',
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              13.5,
                                                                          color:
                                                                              Colors.white,
                                                                          fontFamily:
                                                                              'Arial',
                                                                          fontWeight:
                                                                              FontWeight.w400,
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .end,
                                                                    children: [
                                                                      Image
                                                                          .asset(
                                                                        'assets/fg_images/6_home_icon_flag.png',
                                                                        width:
                                                                            11,
                                                                        height:
                                                                            18.5,
                                                                        color: Hexcolor(
                                                                            '#FE6802'),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            5,
                                                                      ),
                                                                      Text(
                                                                        distanceInKilometers +
                                                                            ' км',
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              13.5,
                                                                          color:
                                                                              Colors.white,
                                                                          fontFamily:
                                                                              'Arial',
                                                                          fontWeight:
                                                                              FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        ' до старту',
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              13.5,
                                                                          color:
                                                                              Colors.white,
                                                                          fontFamily:
                                                                              'Arial',
                                                                          fontWeight:
                                                                              FontWeight.w400,
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                ],
                                                              ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 20,
                                      bottom: 20,
                                    ),
                                    child: Text(
                                      'Опис',
                                      style: TextStyle(
                                        color: Hexcolor('#545454'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                        bottom: 20,
                                      ),
                                      child: Html(
                                        data: missionDescription == null
                                            ? ''
                                            : missionDescription,
                                        onLinkTap: (i) {
                                          _launchUrl(i);
                                        },
                                      )
                                      // Text(
                                      //   missionDescription,
                                      //   style: TextStyle(
                                      //     color: Hexcolor('#545454'),
                                      //     fontSize: 18,
                                      //     fontFamily: 'Arial',
                                      //     letterSpacing: 0.75,
                                      //     height: 1.3,
                                      //     fontWeight: FontWeight.w400,
                                      //   ),
                                      // ),
                                      ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  (missionAccessType == "CLOSED" ||
                              missionAccessType == "PROMO" ||
                              missionAccessType == "PAID" ||
                              (missionAccessType == "BYSTATUS" &&
                                  missionAccessGranted == false)) &&
                          missionUnlocked == false
                      ? Container(
                          margin: EdgeInsets.only(bottom: 20),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 40,
                          child: RaisedButton(
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Text(
                                    "Почати Місію",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Image.asset(
                                    'assets/fg_images/error_mission_icon.png',
                                    width: 16,
                                    height: 20,
                                  ),
                                ]),
                            color: Hexcolor('#BEBEBE'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              print('CLOSED PROMO PAID STATUS');
                              if (missionAccessType == "CLOSED") {
                                showLockedWindow();
                              }
                              if (missionAccessType == "BYSTATUS" &&
                                  missionAccessGranted == false) {
                                showLockedStatusWindow();
                              }
                              if (missionAccessType == "PROMO") {
                                showLockedWindowWithPromocode();
                              }
                              if (missionAccessType == "PAID") {
                                showConfirmWindow();
                              }
                            },
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.only(bottom: 20),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 40,
                          child: RaisedButton(
                            child: Text(
                              "Почати Місію",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              if (_controller.value.isPlaying) {
                                _controller.pause();
                              }
                              _stopFile();
                              if (missionName == 'Київ проти Корони') {
                                _launchUrl('https://freegengame.web.app/');
                              } else {
                                if (missionDone == true) {
                                  showWindowQuizDone();
                                } else {
                                  Navigator.pushNamed(
                                      context, '/6_mission_map.dart');
                                }
                              }
                              // _stopFile();
                            },
                          ),
                        ),
                ],
              ),
            ),
    );
  }
}
