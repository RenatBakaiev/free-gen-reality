import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:hexcolor/hexcolor.dart';
import '6_home_filter_age_ages.dart';
import '6_home_filter_json.dart';

class HomeFilter extends StatefulWidget {
  @override
  _HomeFilterState createState() => _HomeFilterState();
}

class _HomeFilterState extends State<HomeFilter> {
  List<Publication> posts;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesFilters.getFilters().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  bool isPressedBikeMarathon = false;
  bool isPressedRun = false;
  bool isPressedWalk = false;
  bool isPressedCorporateQuest = false;
  bool isPressedSwim = false;
  bool isPressedQuest = false;
  bool isPressedMan = false;
  bool isPressedWoman = false;
  bool isSexManSelected = false;
  bool isSexWomanSelected = false;
  bool isSexSelected = false;

  // setSex() async {
  //   sex = await showDialog(
  //     context: context,
  //     child: Text(sex),
  //   );
  // }

  String sex = 'Не вибрана';

  setSex() {
    if (!isSexManSelected || !isSexWomanSelected) {
      setState(() {
        sex = 'Не вибрана';
      });
    }
    if (isSexManSelected) {
      setState(() {
        sex = 'Чоловіча';
      });
    }
    if (isSexWomanSelected) {
      setState(() {
        sex = 'Жіноча';
      });
    }
    if (isSexWomanSelected && isSexManSelected) {
      setState(() {
        sex = 'Чоловіча і Жіноча';
      });
    }
  }

  chooseAge() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 550,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          // height: 20,
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть вік",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Expanded(
                                    child: SingleChildScrollView(
                                      physics: BouncingScrollPhysics(),
                                      child: ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          shrinkWrap: true,
                                          itemCount: ages.length,
                                          itemBuilder: (context, index) {
                                            return GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                setState(() {
                                                  ages[index].isSelected =
                                                      !ages[index].isSelected;
                                                });
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                  top: 20,
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SizedBox(
                                                      width: 2,
                                                    ),
                                                    Image.asset(
                                                      ages[index].isSelected
                                                          ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                                          : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                                      width: 18,
                                                      height: 18,
                                                    ),
                                                    SizedBox(
                                                      width: 19,
                                                    ),
                                                    Text(
                                                      ages[index].age,
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontSize: 17,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(bottom: 20),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#EB5C18'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  chooseSex() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 235,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть стать",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isSexManSelected = !isSexManSelected;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isSexManSelected
                                            ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                            : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Чоловіча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isSexWomanSelected = !isSexWomanSelected;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isSexWomanSelected
                                            ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                            : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Жіноча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(bottom: 20),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      setSex();
                                      Navigator.of(context).pop();
                                      if ((isSexManSelected ||
                                              isSexWomanSelected) ||
                                          isSexManSelected &&
                                              isSexWomanSelected) {
                                        isSexSelected = true;
                                      }

                                      // if (isSexWomanSelected) {
                                      //   setState(() {
                                      //     womanSelected = true;
                                      //   });
                                      // }
                                      // if (isSexManSelected) {
                                      //   setState(() {
                                      //     manSelected = true;
                                      //   });
                                      // }
                                      // if (isSexWomanSelected &&
                                      //     isSexManSelected) {
                                      //   setState(() {
                                      //     allSexSelected = true;
                                      //   });
                                      // }
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#FFFFFF'),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 40,
                        left: 15,
                        right: 15,
                        bottom: 17,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Фільтр',
                            style: TextStyle(
                                fontSize: 37,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 38,
                                height: 38,
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_filter_close.png',
                                    width: 20,
                                    height: 20,
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, '/5_myBottomBar.dart');
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 200,
                    margin: EdgeInsets.only(right: 10),
                    child: GridView.builder(
                            scrollDirection: Axis.vertical,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: (150 / 50),
                            ),
                            physics: BouncingScrollPhysics(),
                            itemCount: posts.length,
                            itemBuilder: (BuildContext context, int index) {
                              Publication publication = posts[index];

                              var title =
                                  utf8.decode(publication.titleUa.runes.toList());
                              return Container(                                
                                margin: EdgeInsets.only(left: 10, bottom: 10,),
                                child: RaisedButton(
                                  child: Text(
                                    title == null ? '' : title,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  color: Hexcolor('#E6E6E6'),
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(22.5)),
                                  onPressed: () {},
                                ),
                              );
                            }),
                  ),



                  Container(
                    width: MediaQuery.of(context).size.width,
                    //height: MediaQuery.of(context).size.height - 435,
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [

                          


                          // Container(
                          //   margin: EdgeInsets.only(
                          //     left: 15,
                          //     right: 15,
                          //     top: 20,
                          //     bottom: 20,
                          //   ),
                          //   height: 65,
                          //   child: ListView.builder(
                          //     scrollDirection: Axis.horizontal,
                          //     physics: BouncingScrollPhysics(),
                          //     itemCount: posts.length,
                          //     itemBuilder: (context, index) {
                          //       Publication publication = posts[index];

                          //       var title = utf8
                          //           .decode(publication.titleUa.runes.toList());

                          //       return Container(
                          //           padding: EdgeInsets.only(bottom: 20),
                          //           margin: EdgeInsets.only(
                          //             left: 5,
                          //             right: 5,
                          //           ),
                          //           // width: 200,
                          //           height: 45,
                          //           child: RaisedButton(
                          //             child: Text(
                          //               title == null ? '' : title,
                          //               style: TextStyle(
                          //                 color: Colors.white,
                          //                 fontSize: 15,
                          //                 fontFamily: 'Arial',
                          //                 fontWeight: FontWeight.w600,
                          //               ),
                          //             ),
                          //             color:
                          //                 // isPressedBikeMarathon
                          //                 //     ? Hexcolor('#FE6802')
                          //                 //     :
                          //                 Hexcolor('#E6E6E6'),
                          //             shape: new RoundedRectangleBorder(
                          //                 borderRadius:
                          //                     new BorderRadius.circular(22.5)),
                          //             onPressed: () {
                          //               // setState(() {
                          //               //   isPressedBikeMarathon =
                          //               //       !isPressedBikeMarathon;
                          //               // });
                          //             },
                          //           ));
                          //     },
                          //   ),
                          // ),


                          // Container(
                          //   margin: EdgeInsets.only(
                          //     top: 20,
                          //   ),
                          //   child: Column(
                          //     children: [
                          //       Row(
                          //         children: [
                          //           Container(
                          //             width: MediaQuery.of(context).size.width,
                          //             child: Container(
                          //               margin: EdgeInsets.only(
                          //                 left: 20,
                          //               ),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                     MainAxisAlignment.start,
                          //                 children: [
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsets.only(bottom: 2.5),
                          //                     height: 45,
                          //                     child: RaisedButton(
                          //                       child: Text(
                          //                         "Веломарафон",
                          //                         style: TextStyle(
                          //                           color: Colors.white,
                          //                           fontSize: 15,
                          //                           fontFamily: 'Arial',
                          //                           fontWeight: FontWeight.w600,
                          //                         ),
                          //                       ),
                          //                       color: isPressedBikeMarathon
                          //                           ? Hexcolor('#FE6802')
                          //                           : Hexcolor('#E6E6E6'),
                          //                       shape:
                          //                           new RoundedRectangleBorder(
                          //                               borderRadius:
                          //                                   new BorderRadius
                          //                                           .circular(
                          //                                       22.5)),
                          //                       onPressed: () {
                          //                         setState(() {
                          //                           isPressedBikeMarathon =
                          //                               !isPressedBikeMarathon;
                          //                         });
                          //                       },
                          //                     ),
                          //                   ),
                          //                   SizedBox(
                          //                     width: 10,
                          //                   ),
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsets.only(bottom: 2.5),
                          //                     height: 45,
                          //                     child: RaisedButton(
                          //                       child: Text(
                          //                         "Бег",
                          //                         style: TextStyle(
                          //                           color: Colors.white,
                          //                           fontSize: 15,
                          //                           fontFamily: 'Arial',
                          //                           fontWeight: FontWeight.w600,
                          //                         ),
                          //                       ),
                          //                       color: isPressedRun
                          //                           ? Hexcolor('#FE6802')
                          //                           : Hexcolor('#E6E6E6'),
                          //                       shape:
                          //                           new RoundedRectangleBorder(
                          //                               borderRadius:
                          //                                   new BorderRadius
                          //                                           .circular(
                          //                                       22.5)),
                          //                       onPressed: () {
                          //                         setState(() {
                          //                           isPressedRun =
                          //                               !isPressedRun;
                          //                         });
                          //                       },
                          //                     ),
                          //                   ),
                          //                   SizedBox(
                          //                     width: 10,
                          //                   ),
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsets.only(bottom: 2.5),
                          //                     height: 45,
                          //                     child: RaisedButton(
                          //                       child: Text(
                          //                         "Ходьба",
                          //                         style: TextStyle(
                          //                           color: Colors.white,
                          //                           fontSize: 15,
                          //                           fontFamily: 'Arial',
                          //                           fontWeight: FontWeight.w600,
                          //                         ),
                          //                       ),
                          //                       color: isPressedWalk
                          //                           ? Hexcolor('#FE6802')
                          //                           : Hexcolor('#E6E6E6'),
                          //                       shape:
                          //                           new RoundedRectangleBorder(
                          //                               borderRadius:
                          //                                   new BorderRadius
                          //                                           .circular(
                          //                                       22.5)),
                          //                       onPressed: () {
                          //                         setState(() {
                          //                           isPressedWalk =
                          //                               !isPressedWalk;
                          //                         });
                          //                       },
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //           ),
                          //         ],
                          //       ),
                          //       SizedBox(
                          //         height: 10,
                          //       ),
                          //       Row(
                          //         children: [
                          //           Container(
                          //             width: MediaQuery.of(context).size.width,
                          //             child: Container(
                          //               margin: EdgeInsets.only(
                          //                   left: 20, right: 20),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                     MainAxisAlignment.start,
                          //                 children: [
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsets.only(bottom: 2.5),
                          //                     height: 45,
                          //                     child: RaisedButton(
                          //                       child: Text(
                          //                         "Корпоративный квест",
                          //                         style: TextStyle(
                          //                           color: Colors.white,
                          //                           fontSize: 15,
                          //                           fontFamily: 'Arial',
                          //                           fontWeight: FontWeight.w600,
                          //                         ),
                          //                       ),
                          //                       color: isPressedCorporateQuest
                          //                           ? Hexcolor('#FE6802')
                          //                           : Hexcolor('#E6E6E6'),
                          //                       shape:
                          //                           new RoundedRectangleBorder(
                          //                               borderRadius:
                          //                                   new BorderRadius
                          //                                           .circular(
                          //                                       22.5)),
                          //                       onPressed: () {
                          //                         setState(() {
                          //                           isPressedCorporateQuest =
                          //                               !isPressedCorporateQuest;
                          //                         });
                          //                       },
                          //                     ),
                          //                   ),
                          //                   SizedBox(
                          //                     width: 10,
                          //                   ),
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsets.only(bottom: 2.5),
                          //                     height: 45,
                          //                     child: RaisedButton(
                          //                       child: Text(
                          //                         "Плаванье",
                          //                         style: TextStyle(
                          //                           color: Colors.white,
                          //                           fontSize: 15,
                          //                           fontFamily: 'Arial',
                          //                           fontWeight: FontWeight.w600,
                          //                         ),
                          //                       ),
                          //                       color: isPressedSwim
                          //                           ? Hexcolor('#FE6802')
                          //                           : Hexcolor('#E6E6E6'),
                          //                       shape:
                          //                           new RoundedRectangleBorder(
                          //                               borderRadius:
                          //                                   new BorderRadius
                          //                                           .circular(
                          //                                       22.5)),
                          //                       onPressed: () {
                          //                         setState(() {
                          //                           isPressedSwim =
                          //                               !isPressedSwim;
                          //                         });
                          //                       },
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //           ),
                          //         ],
                          //       ),
                          //       SizedBox(
                          //         height: 10,
                          //       ),
                          //       Row(
                          //         children: [
                          //           Container(
                          //             width: MediaQuery.of(context).size.width,
                          //             child: Container(
                          //               margin: EdgeInsets.only(
                          //                   left: 20, right: 20),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                     MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsets.only(bottom: 2.5),
                          //                     height: 45,
                          //                     child: RaisedButton(
                          //                       child: Text(
                          //                         "Квест",
                          //                         style: TextStyle(
                          //                           color: Colors.white,
                          //                           fontSize: 15,
                          //                           fontFamily: 'Arial',
                          //                           fontWeight: FontWeight.w600,
                          //                         ),
                          //                       ),
                          //                       color: isPressedQuest
                          //                           ? Hexcolor('#FE6802')
                          //                           : Hexcolor('#E6E6E6'),
                          //                       shape:
                          //                           new RoundedRectangleBorder(
                          //                               borderRadius:
                          //                                   new BorderRadius
                          //                                           .circular(
                          //                                       22.5)),
                          //                       onPressed: () {
                          //                         setState(() {
                          //                           isPressedQuest =
                          //                               !isPressedQuest;
                          //                         });
                          //                       },
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //           ),
                          //         ],
                          //       ),
                          //       SizedBox(
                          //         height: 30,
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          Container(
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: Column(children: [
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, '/6_home_filter_location.dart');
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Місцезнаходження',
                                            style: TextStyle(
                                              color: Hexcolor('#919191'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 12),
                                            child: Row(
                                              children: [
                                                Image.asset(
                                                  'assets/fg_images/6_home_filter_pin.png',
                                                  width: 20,
                                                  height: 29,
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    'Київ',
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 16,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      letterSpacing: 1.025,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Container(
                                        child: Image.asset(
                                          'assets/fg_images/6_home_filter_arrow.png',
                                          width: 10,
                                          height: 18,
                                        ),
                                      ),
                                    ]),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Divider(
                                color: Hexcolor('#ECECEC'),
                                thickness: 1,
                                height: 12,
                              ),
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, '/6_home_filter_distance.dart');
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Відстань',
                                            style: TextStyle(
                                              color: Hexcolor('#919191'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 12),
                                            child: Row(
                                              children: [
                                                Image.asset(
                                                  'assets/fg_images/6_home_filter_man.png',
                                                  width: 18,
                                                  height: 30,
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    '5' + ' км',
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 16,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      letterSpacing: 1.025,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Container(
                                        child: Image.asset(
                                          'assets/fg_images/6_home_filter_arrow.png',
                                          width: 10,
                                          height: 18,
                                        ),
                                      ),
                                    ]),
                              ),
                              // SizedBox(
                              //   height: 5,
                              // ),
                              // Divider(
                              //   color: Hexcolor('#ECECEC'),
                              //   thickness: 1,
                              //   height: 12,
                              // ),
                              // GestureDetector(
                              //   behavior: HitTestBehavior.opaque,
                              //   onTap: () {
                              //     Navigator.pushNamed(
                              //         context, '/6_home_filter_category.dart');
                              //   },
                              //   child: Row(
                              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //       children: [
                              //         Column(
                              //           crossAxisAlignment: CrossAxisAlignment.start,
                              //           children: [
                              //             Text(
                              //               'Выбрать рубрику',
                              //               style: TextStyle(
                              //                 color: Hexcolor('#919191'),
                              //                 fontSize: 16,
                              //                 fontFamily: 'Arial',
                              //                 fontWeight: FontWeight.w600,
                              //                 letterSpacing: 1.025,
                              //               ),
                              //             ),
                              //             Container(
                              //               margin: EdgeInsets.only(top: 12),
                              //               child: Row(
                              //                 children: [
                              //                   Image.asset(
                              //                     'assets/fg_images/6_home_filter_category.png',
                              //                     width: 21,
                              //                     height: 21,
                              //                   ),
                              //                   Container(
                              //                     margin: EdgeInsets.only(left: 25),
                              //                     child: Text(
                              //                       'Любая',
                              //                       style: TextStyle(
                              //                         color: Hexcolor('#1E2E45'),
                              //                         fontSize: 16,
                              //                         fontFamily: 'Arial',
                              //                         fontWeight: FontWeight.w600,
                              //                         letterSpacing: 1.025,
                              //                       ),
                              //                     ),
                              //                   ),
                              //                 ],
                              //               ),
                              //             )
                              //           ],
                              //         ),
                              //         Container(
                              //           child: Image.asset(
                              //             'assets/fg_images/6_home_filter_arrow.png',
                              //             width: 10,
                              //             height: 18,
                              //           ),
                              //         ),
                              //       ]),
                              // ),
                              SizedBox(
                                height: 5,
                              ),
                              Divider(
                                color: Hexcolor('#ECECEC'),
                                thickness: 1,
                                height: 12,
                              ),
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  chooseSex();
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Стать',
                                            style: TextStyle(
                                              color: Hexcolor('#919191'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 12),
                                            child: Row(
                                              children: [
                                                Image.asset(
                                                  'assets/fg_images/6_home_filter_sex.png',
                                                  width: 21,
                                                  height: 21,
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    sex,
                                                    style: TextStyle(
                                                      color: (isSexManSelected ||
                                                                  isSexWomanSelected) ||
                                                              (isSexManSelected &&
                                                                  isSexWomanSelected)
                                                          ? Hexcolor('#1E2E45')
                                                          : Hexcolor('#A2A2A2'),
                                                      fontSize: 16,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      letterSpacing: 1.025,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Container(
                                        child: Image.asset(
                                          'assets/fg_images/6_home_filter_arrow.png',
                                          width: 10,
                                          height: 18,
                                        ),
                                      ),
                                    ]),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Divider(
                                color: Hexcolor('#ECECEC'),
                                thickness: 1,
                                height: 12,
                              ),
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  chooseAge();
                                  // Navigator.pushNamed(
                                  //     context, '/6_home_filter_age.dart');
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Виберіть вік',
                                            style: TextStyle(
                                              color: Hexcolor('#919191'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 12),
                                            child: Row(
                                              children: [
                                                Image.asset(
                                                  'assets/fg_images/6_home_filter_age.png',
                                                  width: 21,
                                                  height: 21,
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    'Будь-який',
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 16,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      letterSpacing: 1.025,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Container(
                                        child: Image.asset(
                                          'assets/fg_images/6_home_filter_arrow.png',
                                          width: 10,
                                          height: 18,
                                        ),
                                      ),
                                    ]),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Divider(
                                color: Hexcolor('#ECECEC'),
                                thickness: 1,
                                height: 12,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ]),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(14.0),
                      ),
                      child: Text("Результат поиска",
                          style: TextStyle(
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                              letterSpacing: 1.09,
                              fontSize: 17)),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
