import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import '10_profile_rewards.dart';
// import 'package:percent_indicator/percent_indicator.dart';

import 'package:assets_audio_player/assets_audio_player.dart';

class SplashScreen extends StatefulWidget {
  method() => createState()._stopFile();
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController progressController;
  Animation<double> animation;

  bool isLoading = true;
  bool hihihi = true;
  // AudioPlayer advancedPlayer;

  // @override
  // initState() {
  //   super.initState();
  //   // loadMusic();
  // }

  // Future loadMusic() async {
  //   advancedPlayer = await AudioCache().loop("music/main.mp3");
  // }

  // @override
  // void dispose() {
  //   advancedPlayer = null;
  //   super.dispose();
  // }

// --------------------------------------------------------------------------

  @override
  initState() {
    super.initState();
    // _playFile(); // Музыка закомичена !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //https://www.youtube.com/watch?v=3J5FR2H5FDk
    progressController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 10000));
    animation = Tween<double>(begin: 0, end: 100).animate(progressController)
      ..addListener(() {
        setState(() {});
      });

    String finalEmail;

    Future getValidationData() async {
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var obtainedEmail = sharedPreferences.getString('email');
      setState(() {
        finalEmail = obtainedEmail;
        // isApiCallProcess = false;
      });
      print('User email: $finalEmail');
    }

    Future.delayed(const Duration(seconds: 12), () {
      if (hihihi
          // allRewards[5].quantity < 10
          ) {
        // Navigator.pushNamed(context, '/0_splash_screen_language');
        getValidationData().whenComplete(() async {
          finalEmail == null
              ? Navigator.pushNamed(context, '/1_layout_1')
              : Navigator.pushNamed(context, '/5_myBottomBar.dart');
        });

        setState(() {
          isLoading = false;
        });
        // showYouReceiveRewardWindow();
      } else {
        // Navigator.pushNamed(context, '/1_layout_1'); /0_splash_screen_language
        Navigator.pushNamed(context, '/1_layout_1');
        setState(() {
          isLoading = false;
        });
      }

      // _stopFile();
    });
    _startLoading();
  }

  // showYouReceiveRewardWindow() {
  //   setState(() {
  //     allRewards[5].quantity = allRewards[5].quantity + 10;
  //   });
  //   showDialog(
  //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
  //       context: context,
  //       builder: (context) {
  //         return Dialog(
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           child: Container(
  //             width: MediaQuery.of(context).size.width - 80,
  //             height: 230,
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 Container(
  //                     alignment: Alignment.center,
  //                     margin: EdgeInsets.only(
  //                       top: 20,
  //                       left: 10,
  //                       right: 10,
  //                     ),
  //                     child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         crossAxisAlignment: CrossAxisAlignment.center,
  //                         children: [
  //                           Container(
  //                             margin: EdgeInsets.only(
  //                               bottom: 10,
  //                             ),
  //                             child: Text(
  //                               'Поздравляем!',
  //                               style: TextStyle(
  //                                 color: Hexcolor('#58B12D'),
  //                                 fontSize: 17,
  //                                 fontFamily: 'AvenirNextLTPro-Regular',
  //                                 fontWeight: FontWeight.w900,
  //                                 letterSpacing: 1.089,
  //                               ),
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           ),
  //                           Row(
  //                             mainAxisAlignment: MainAxisAlignment.center,
  //                             crossAxisAlignment: CrossAxisAlignment.end,
  //                             children: [
  //                               Text(
  //                                 'Вы получили ',
  //                                 style: TextStyle(
  //                                   color: Hexcolor('#000000'),
  //                                   fontSize: 17,
  //                                   fontFamily: 'AvenirNextLTPro-Regular',
  //                                   fontWeight: FontWeight.w900,
  //                                   letterSpacing: 1.089,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                               Text(
  //                                 '10 Foints',
  //                                 style: TextStyle(
  //                                   color: Hexcolor('#58B12D'),
  //                                   fontSize: 17,
  //                                   fontFamily: 'AvenirNextLTPro-Regular',
  //                                   fontWeight: FontWeight.w900,
  //                                   letterSpacing: 1.089,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                             ],
  //                           ),
  //                           Container(
  //                             margin: EdgeInsets.only(
  //                               top: 10,
  //                             ),
  //                             child: Image.asset(
  //                               'assets/fg_images/10_profile_user_param_icon_foint2.png',
  //                               height: 50,
  //                             ),
  //                           ),
  //                         ])),
  //                 Container(
  //                   margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
  //                   child: Column(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Container(
  //                         height: 60,
  //                         width: MediaQuery.of(context).size.width,
  //                         child: RaisedButton(
  //                           child: Text(
  //                             "Продолжить",
  //                             style: TextStyle(
  //                               color: Colors.white,
  //                               fontSize: 17,
  //                             ),
  //                           ),
  //                           color: Hexcolor('#EB5C18'),
  //                           shape: new RoundedRectangleBorder(
  //                               borderRadius: new BorderRadius.circular(14.0)),
  //                           onPressed: () {
  //                             Navigator.pushNamed(
  //                                 context, '/0_splash_screen_language');
  //                             _stopFile();
  //                           },
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  //   setState(() {
  //     isLoading = false;
  //   });
  // }

  _startLoading() {
    if (animation.value == 0) {
      progressController.forward();
    }
  }

  bool isPlaying = true;
  bool isPaused = false;

  AudioCache cache; // you have this
  AudioPlayer player; // create this

  // AssetsAudioPlayer.newPlayer().open(
  //   Audio("music/main.mp3"),
  //   autoPlay: true,
  //   showNotification: true,
  // );

  // AseettsAudioPlayer.newPlayer().stop();
  AssetsAudioPlayer assetsAudioPlayer = AssetsAudioPlayer();

  // void _playFile() async {
  //  await assetsAudioPlayer.open(
  //     Audio('assets/music/main.mp3')
  //   );
  // }

  // void _stopFile() async {
  //   await assetsAudioPlayer.playOrPause();
  //   print('stop music');
  // }

  // void _playFile() async {
  //   player = await AudioCache().loop("music/main.mp3"); // assign player here
  // }

  void _stopFile() {
    player?.stop(); // stop the file like this
    print('music stopped');
  }

  // void _pauseFile() {
  //   player?.pause();
  //   setState(() {
  //     isPlaying = false;
  //   });
  //   // pause the file like this
  // }

  // void _resumeFile() {
  //   player?.resume();
  //   setState(() {
  //     isPlaying = true;
  //   }); // resume the file like this
  // }
  void pausePlay() {
    if (isPaused && isPlaying) {
      player.resume();
    } else {
      player.pause();
    }
    setState(() {
      isPaused = !isPaused;
    });
  }

// --------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Stack(
          overflow: Overflow.visible,
          children: [
            GestureDetector(
              onTap: () {
                // Navigator.pushNamed(context, '/0_splash_screen_language');
                // Navigator.pushNamed(context, '/1_layout_1');
                _stopFile();
              },
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          // Navigator.pushNamed(context, '/testMap.dart');
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 50),
                          child: Image.asset(
                            'assets/fg_images/0_splash_screen_logo2.png',
                            width: 227,
                            height: 178,
                          ),
                        ),
                      ),

                      // Container(
                      //   margin: EdgeInsets.only(
                      //     top: 10,
                      //   ),
                      //   child: CircularProgressIndicator(
                      //     strokeWidth: 10,
                      //     backgroundColor: Hexcolor('#7D5AC2').withOpacity(0.5),
                      //     valueColor: AlwaysStoppedAnimation<Color>(
                      //       Hexcolor('#EB5C18'),
                      //     ),
                      //   ),
                      // ),

                      // Stack(
                      //   overflow: Overflow.visible,
                      //   children: [
                      //     SizedBox(
                      //   width: 300,
                      //   height: 300,
                      //   child: CircularProgressIndicator(
                      //     strokeWidth: 20,
                      //     backgroundColor: Hexcolor('#7D5AC2').withOpacity(0.5),
                      //     valueColor: AlwaysStoppedAnimation<Color>(
                      //       Hexcolor('#EB5C18'),
                      //     ),
                      //   ),
                      // ),
                      // Positioned(
                      //   top: 50,
                      //   left: 60,
                      //   child: Image.asset(
                      //     'assets/fg_images/0_splash_screen_lion.gif',
                      //     height: 200,))
                      //   ],
                      // ),

                      // Container(
                      //   //- I like this variant !!!
                      //   margin: EdgeInsets.only(
                      //     top: 20,
                      //     left: 10,
                      //     right: 10,
                      //   ),
                      //   child: LinearPercentIndicator(
                      //     backgroundColor: Hexcolor('#7D5AC2').withOpacity(0.5),
                      //     progressColor: Hexcolor('#EB5C18'),
                      //     lineHeight: 20,
                      //     width: MediaQuery.of(context).size.width - 20,
                      //     percent: 1.0,
                      //     center: Text(
                      //       "Загрузка... " + "${animation.value.toInt()}" + "%",
                      //       style: TextStyle(
                      //         color: Colors.white,
                      //         // fontFamily: 'AvenirNextLTPro-Regular',
                      //         fontSize: 15,
                      //         fontWeight: FontWeight.w300,
                      //       ),
                      //     ),
                      //     animation: true,
                      //     animationDuration: 10000,
                      //   ),
                      // ),
                      GestureDetector(
                          onTap: () {
                            // pausePlay();
                          },
                          child: Stack(
                            overflow: Overflow.visible,
                            children: [
                              
                              Image.asset(
                                'assets/fg_images/0_splash_screen_lion_and_penguin.gif',
                                // 'assets/fg_images/Logo.gif.mp4',
                                height: 370,
                              ),
                              
                              // Positioned(
                              //   right: 50,
                              //   bottom: 100,
                              //   left: 165,
                              //   child: isLoading
                              //       ? Container(
                              //           margin: EdgeInsets.only(
                              //             top: 10,
                              //           ),
                              //           child: CircularProgressIndicator(
                              //             strokeWidth: 10,
                              //             backgroundColor: Hexcolor('#7D5AC2')
                              //                 .withOpacity(0.5),
                              //             valueColor:
                              //                 AlwaysStoppedAnimation<Color>(
                              //               Hexcolor('#EB5C18'),
                              //             ),
                              //           ),
                              //         )
                              //       : Container(),
                              // )
                            ],
                          )),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Text("Бета-тестування додатку",
                                textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                        letterSpacing: 1.09,
                                        fontSize: 14)),
                          ),
                      isLoading
                          ? Container(
                              margin: EdgeInsets.only(
                                top: 10,
                              ),
                              child: CircularProgressIndicator(
                                strokeWidth: 10,
                                backgroundColor:
                                    Hexcolor('#7D5AC2').withOpacity(0.5),
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  Hexcolor('#FE6802'),
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image:
                            AssetImage('assets/fg_images/0_splash_screen.jpg'),
                        fit: BoxFit.cover)),
              ),
            ),
            // Positioned(
            //   top: MediaQuery.of(context).size.height / 2,

            //   child: LinearPercentIndicator(
            //     backgroundColor: Hexcolor('#7D5AC2').withOpacity(0.5),
            //     progressColor: Hexcolor('#EB5C18'),
            //     lineHeight: 15,
            //     width: MediaQuery.of(context).size.width,
            //     percent: 1.0,
            //     center: Text("Загрузка..." + "${animation.value.toInt()}" + "%"),
            //     animation: true,
            //     animationDuration: 10000,
            //   ),

            //   // LinearProgressIndicator(
            //   //   backgroundColor: Hexcolor('#7D5AC2').withOpacity(0.5),
            //   //   valueColor: AlwaysStoppedAnimation<Color>(Hexcolor('#EB5C18')),
            //   // ),
            // ),
            // Positioned(
            //   top: 54.67,
            //   right: 20,
            //   child: GestureDetector(
            //     behavior: HitTestBehavior.opaque,
            //     onTap: () {
            //       // pausePlay();
            //       setState(() {
            //         isPlaying = !isPlaying;
            //         isPaused = !isPaused;
            //       });
            //     },
            //     child: Image.asset(
            //       isPlaying && !isPaused
            //           ? 'assets/fg_images/0_splash_screen_icon_pause.png'
            //           : 'assets/fg_images/0_splash_screen_icon_play.png',
            //       width: 40,
            //       height: 34.67,
            //       color: Hexcolor('#FFFFFF'),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
