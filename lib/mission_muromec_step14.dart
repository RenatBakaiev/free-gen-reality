import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:vibration/vibration.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';

class MissionMuromecStep14 extends StatefulWidget {
  @override
  _MissionMuromecStep14State createState() => _MissionMuromecStep14State();
}

class _MissionMuromecStep14State extends State<MissionMuromecStep14> {
  AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();

  bool isPlaying = false;
  bool isApiCallProcess = false;

  void _getAudio() async {
    var url =
        "http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-02-05_muromec_info_14.mp3";
    var res = await audioPlayer.play(url, isLocal: true);
    if (res == 1) {
      setState(() {
        isPlaying = true;
      });
    }
  }

  void _stopFile() {
    audioPlayer?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer.pause();
    } else {
      audioPlayer.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  @override
  void initState() {
    super.initState();
    _getAudio();
    Vibration.vibrate(duration: 1000);
  }

  bool isFavorite = false;

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    setState(() {
      isApiCallProcess = true;
    });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      print(response.body);
      print('muromec step 14 done!!!');
      setState(() {
        isApiCallProcess = false;
      });
    } else {
      setState(() {
        isApiCallProcess = false;
      });
      throw Exception('Failed to update User.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 80
                    //  - 85
                    ,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 225,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                                  image: NetworkImage(
                                      'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_Muromec_pic14.gif'),
                                  fit: BoxFit.cover,
                                )),
                            child: Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                right: 20,
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(),
                                      GestureDetector(
                                        behavior: HitTestBehavior.opaque,
                                        onTap: () {
                                          pausePlay();
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            top: 30,
                                          ),
                                          child: Image.asset(
                                            !isPlaying
                                                ? 'assets/fg_images/audio_play.png'
                                                : 'assets/fg_images/audio_pause.png',
                                            height: 50,
                                            width: 50,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Опис',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    ' Вітаю!\n Ця прогулянка дійшла до кінця, проте, як бачиш, двері одних пригод зачиняються, а нові - відчиняються!)\n Навколо тебе розкинувся так званий екстрим парк. Кажуть, тут представлено понад 100 видів спорту!\n\n Дякую за подорож і до зустрічі!',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 16,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: SizedBox(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_search_back.png',
                                    width: 10,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    _stopFile();
                                    Navigator.pushNamed(
                                        context, '/mission_muromec_map.dart');
                                  },
                                ),
                              ),
                            ),
                            Text(
                              'Інформація',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/run_map_quest_pic_gostart.png',
                                width: 22,
                                height: 24,
                              ),
                              onPressed: () {
                                _stopFile();
                                Navigator.pushNamed(
                                  context,
                                  '/mission_muromec_desc.dart',
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Далі",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  _stopFile();
                  Navigator.pushNamed(context, '/mission_muromec_finish.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
