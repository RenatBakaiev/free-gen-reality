import '6_home_filter_category_model.dart';

final LastRequest lastRequest1 = LastRequest(
  request: 'Спортивные игры',
  isSelected: false,
);

final LastRequest lastRequest2 = LastRequest(
  request: 'Интелектуальная миссия',
  isSelected: false,
);

final LastRequest lastRequest3 = LastRequest(
  request: 'Миссия для компании',
  isSelected: false,
);

List<LastRequest> lastRequests = [
  lastRequest1,
  lastRequest2,
  lastRequest3,
];

final Quest quest1 = Quest(
  quest: 'Все миссиии',
  isSelected: false,
);

final Quest quest2 = Quest(
  quest: 'Рубрика миссий',
  isSelected: false,
);

final Quest quest3 = Quest(
  quest: 'Рубрика миссий 2',
  isSelected: false,
);

final Quest quest4 = Quest(
  quest: 'Рубрика миссий 3',
  isSelected: false,
);

final Quest quest5 = Quest(
  quest: 'Рубрика миссий 4',
  isSelected: false,
);

final Quest quest6 = Quest(
  quest: 'Рубрика миссий 5',
  isSelected: false,
);

final Quest quest7 = Quest(
  quest: 'Рубрика миссий 6',
  isSelected: false,
);

final Quest quest8 = Quest(
  quest: 'Рубрика миссий 7',
  isSelected: false,
);

List<Quest> quests = [
  quest1,
  quest2,
  quest3,
  quest4,
  quest5,
  quest6,
  quest7,
  quest8,
];
