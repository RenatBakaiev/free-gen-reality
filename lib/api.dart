import 'package:http/http.dart' as http;
import 'dart:convert';

// import 'package:shared_preferences/shared_preferences.dart';

// Future loginUser(String email, String password) async {
//   String url = "http://generation-admin.ehub.com.ua/api/user/login";
//   final response = await http.post(url,
//   headers: {"Accept": "Application/json"},
//   body: {"email": email,"password": password,}
//   );
//   var convertedDatatoJson = jsonDecode(response.body);
//   return convertedDatatoJson;
// }

Future<http.Response> loginUser(email, password) async {
  var url = 'http://generation-admin.ehub.com.ua/api/user/login';

  Map data = {
    "email": email,
    "password": password,
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);
  print("${response.statusCode}");
  print("${response.body}");
  return response;
}

Future<http.Response> registerUser(email, nickName, password, registrationType) async {
  var url = 'http://generation-admin.ehub.com.ua/api/user/register';

  Map data = {
    "email": email,
    "username": nickName,
    "password": password,
    "registrationType" : registrationType
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);
  print("${response.statusCode}");
  print("${response.body}");
  return response;
}