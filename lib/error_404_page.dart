import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import '10_profile_currencies_json.dart';

class Error404Page extends StatefulWidget {
  @override
  _Error404PageState createState() => _Error404PageState();
}

class _Error404PageState extends State<Error404Page> {
  List<Publication> currencies2;
  bool loading = true;
  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesCurrencies.getCurrencies().then((list) {
      setState(() {
        currencies2 = list.publication;
        loading = false;
        print(currencies2);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              color: Hexcolor('#7D5AC2'),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Stack(
                    alignment: Alignment.topCenter,
                    overflow: Overflow.visible,
                    children: [
                      Text(
                        '404',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Arial',
                          fontSize: 177,
                          fontWeight: FontWeight.w700,
                          color: Hexcolor('FFFFFF'),
                        ),
                      ),
                      // Container(
                      //   margin: EdgeInsets.only(top: 143),
                      //   child: Image.asset(
                      //     'assets/fg_images/error_404_page_penguin.png',
                      //     width: 313,
                      //   ),
                      // ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 20),
                        child: Text(
                          'Страница не найдена',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            color: Hexcolor('FFFFFF'),
                          ),
                        ),
                      ),
                      Text(
                        'Что-то пошло не так...\nВернитесь на главную и попробуйте снова.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Arial',
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Hexcolor('FFFFFF'),
                          height: 1.5,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        "На главную",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, '/5_myBottomBar.dart');
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
