// To parse this JSON data, do
//
//     final statuses = statusesFromJson(jsonString);

import 'dart:convert';

Statuses statusesFromJson(String str) => Statuses.fromJson(json.decode(str));

String statusesToJson(Statuses data) => json.encode(data.toJson());

class Statuses {
    Statuses({
        this.publication,
        this.page,
    });

    List<Publication> publication;
    Page page;

    factory Statuses.fromJson(Map<String, dynamic> json) => Statuses(
        publication: List<Publication>.from(json["publication"].map((x) => Publication.fromJson(x))),
        page: Page.fromJson(json["page"]),
    );

    Map<String, dynamic> toJson() => {
        "publication": List<dynamic>.from(publication.map((x) => x.toJson())),
        "page": page.toJson(),
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class Publication {
    Publication({
        this.id,
        this.active,
        this.deleted,
        this.tagsUa,
        this.position,
        this.color,
        this.cost,
        this.address,
        this.publicationType,
        this.author,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.pageType,
        this.titleShortUa,
        this.titleShortRu,
        this.titleShortEn,
        this.contentUa,
        this.contentRu,
        this.contentEn,
        this.contentShortUa,
        this.contentShortRu,
        this.contentShortEn,
        this.publicationDate,
        this.expireDate,
        this.img,
    });

    int id;
    bool active;
    bool deleted;
    dynamic tagsUa;
    int position;
    String color;
    double cost;
    String address;
    String publicationType;
    String author;
    String titleUa;
    String titleRu;
    String titleEn;
    dynamic pageType;
    dynamic titleShortUa;
    dynamic titleShortRu;
    dynamic titleShortEn;
    dynamic contentUa;
    dynamic contentRu;
    dynamic contentEn;
    dynamic contentShortUa;
    dynamic contentShortRu;
    dynamic contentShortEn;
    DateTime publicationDate;
    DateTime expireDate;
    String img;

    factory Publication.fromJson(Map<String, dynamic> json) => Publication(
        id: json["id"],
        active: json["active"],
        deleted: json["deleted"],
        tagsUa: json["tagsUa"],
        position: json["position"],
        color: json["color"],
        cost: json["cost"],
        address: json["address"],
        publicationType: json["publicationType"],
        author: json["author"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"] == null ? null : json["titleRu"],
        titleEn: json["titleEn"] == null ? null : json["titleEn"],
        pageType: json["page_type"],
        titleShortUa: json["titleShortUa"],
        titleShortRu: json["titleShortRu"],
        titleShortEn: json["titleShortEn"],
        contentUa: json["contentUa"],
        contentRu: json["contentRu"],
        contentEn: json["contentEn"],
        contentShortUa: json["contentShortUa"],
        contentShortRu: json["contentShortRU"],
        contentShortEn: json["contentShortEn"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        expireDate: json["expireDate"] == null ? null : DateTime.parse(json["expireDate"]),
        img: json["img"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "active": active,
        "deleted": deleted,
        "tagsUa": tagsUa,
        "position": position,
        "color": color,
        "cost": cost,
        "address": address,
        "publicationType": publicationType,
        "author": author,
        "titleUa": titleUa,
        "titleRu": titleRu == null ? null : titleRu,
        "titleEn": titleEn == null ? null : titleEn,
        "page_type": pageType,
        "titleShortUa": titleShortUa,
        "titleShortRu": titleShortRu,
        "titleShortEn": titleShortEn,
        "contentUa": contentUa,
        "contentRu": contentRu,
        "contentEn": contentEn,
        "contentShortUa": contentShortUa,
        "contentShortRU": contentShortRu,
        "contentShortEn": contentShortEn,
        "publicationDate": publicationDate.toIso8601String(),
        "expireDate": expireDate == null ? null : expireDate.toIso8601String(),
        "img": img,
    };
}
