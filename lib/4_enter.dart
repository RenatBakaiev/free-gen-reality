// import 'dart:html';

// import 'dart:convert';

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:flutter_free_gen_reality/api.dart';
// import 'package:flutter_free_gen_reality/api/api.dart';
import 'package:flutter_free_gen_reality/model/login_model.dart';
// import 'package:flutter_free_gen_reality/api.dart';
// import 'package:flutter_free_gen_reality/services/auth_node.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:http/http.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
// import '10_profile_rewards.dart';
// import 'package:http/http.dart' as http;
import '10_profile_json.dart';
import 'package:http/http.dart' as http;

class Enter extends StatefulWidget {
  @override
  _EnterState createState() => _EnterState();
}

bool userNotFound = false;
bool isVisible = false;

class _EnterState extends State<Enter> {
  var _blankFocusNode = new FocusNode();
  final formKey = GlobalKey<FormState>();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  String message = '';

  final scaffoldKey = GlobalKey<ScaffoldState>();

  // showYouReceiveRewardWindow() {
  //   setState(() {
  //     allRewards[5].quantity = allRewards[5].quantity + 100;
  //   });
  //   showDialog(
  //       barrierDismissible: false,
  //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
  //       context: context,
  //       builder: (context) {
  //         return Dialog(
  //           insetPadding: EdgeInsets.only(
  //             left: 20,
  //             right: 20,
  //           ),
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           child: Container(
  //             // width: MediaQuery.of(context).size.width - 40,
  //             height: 425,
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 Container(
  //                     alignment: Alignment.center,
  //                     margin: EdgeInsets.only(
  //                       top: 20,
  //                       left: 10,
  //                       right: 10,
  //                     ),
  //                     child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         crossAxisAlignment: CrossAxisAlignment.center,
  //                         children: [
  //                           Container(
  //                             margin: EdgeInsets.only(
  //                               bottom: 10,
  //                             ),
  //                             child: Text(
  //                               'Поздравляем!',
  //                               style: TextStyle(
  //                                 color: Hexcolor('#1E2E45'),
  //                                 fontSize: 24,
  //                                 fontFamily: 'Arial',
  //                                 fontWeight: FontWeight.w700,
  //                               ),
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           ),
  //                           Container(
  //                             margin: EdgeInsets.only(
  //                               bottom: 5,
  //                             ),
  //                             child: Text(
  //                               'Вы успешно зарегистрировались',
  //                               style: TextStyle(
  //                                 color: Hexcolor('#747474'),
  //                                 fontSize: 20,
  //                                 fontFamily: 'Arial',
  //                                 fontWeight: FontWeight.w400,
  //                               ),
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           ),
  //                           Row(
  //                             mainAxisAlignment: MainAxisAlignment.center,
  //                             crossAxisAlignment: CrossAxisAlignment.end,
  //                             children: [
  //                               Text(
  //                                 'и получили бонус ',
  //                                 style: TextStyle(
  //                                   color: Hexcolor('#747474'),
  //                                   fontSize: 20,
  //                                   fontFamily: 'Arial',
  //                                   fontWeight: FontWeight.w400,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                               Text(
  //                                 '100' + ' foint',
  //                                 style: TextStyle(
  //                                   color: Hexcolor('#519950'),
  //                                   fontSize: 20,
  //                                   fontFamily: 'Arial',
  //                                   fontWeight: FontWeight.w700,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                             ],
  //                           ),
  //                         ])),
  //                 Container(
  //                   margin: EdgeInsets.only(
  //                     top: 10,
  //                   ),
  //                   child: Image.asset(
  //                     'assets/fg_images/4_enter_register_rewards.jpg',
  //                     height: 200,
  //                   ),
  //                 ),
  //                 Container(
  //                   margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
  //                   child: Column(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Container(
  //                         height: 60,
  //                         width: MediaQuery.of(context).size.width,
  //                         child: RaisedButton(
  //                           child: Text(
  //                             "Продолжить",
  //                             style: TextStyle(
  //                               color: Colors.white,
  //                               fontSize: 17,
  //                               fontFamily: 'Arial',
  //                               fontWeight: FontWeight.w600,
  //                             ),
  //                           ),
  //                           color: Hexcolor('#FE6802'),
  //                           shape: new RoundedRectangleBorder(
  //                               borderRadius: new BorderRadius.circular(14.0)),
  //                           onPressed: () {
  //                             Navigator.pushNamed(
  //                                 context, '/5_myBottomBar.dart');
  //                           },
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }

  // login 3
  // bool _isLoading = false;

  // logIn(String email, String password) async {
  //   Map data = {
  //     "email": email,
  //     "password": password,
  //   };
  //   var jsonData = null;
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   var response = await http
  //       .post("http://generation-admin.ehub.com.ua/api/user/login", body: data);
  //   if (response.statusCode == 200) {
  //     jsonData = jsonDecode(response.body);
  //     setState(() {
  //       _isLoading = false;
  //       sharedPreferences.setString("token", jsonData['token']);
  //       Navigator.pushReplacementNamed(context, '/5_myBottomBar.dart');
  //     });
  //   } else {
  //     print(response.body);
  //   }
  // }

  // woman

  loginError() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 520,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Помилка авторизації',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Користувач не знайдений\nабо\nпомилка в e-mail чи паролі',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 20,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_wrong_answer.png',
                        height: 270,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Спробувати ще",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/4_enter');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  logIn() async {
    FocusScope.of(context).requestFocus(_blankFocusNode);
    if (formKey.currentState.validate()) {
      var email = emailTextEditingController.text;
      var password = passwordTextEditingController.text;
      setState(() {
        isApiCallProcess = true;
      });

      var response = await loginUser(email, password);
      print(response);
      if (response.statusCode == 200) {
        setState(() {
          isApiCallProcess = false;
        });
        Future.delayed(const Duration(seconds: 1), () {
          Navigator.pushNamed(context, '/5_myBottomBar.dart');
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);

        print(responseJson["access"]);
        print(responseJson["id"]);

        final SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setString('token', responseJson["access"]);
        sharedPreferences.setInt('id', responseJson["id"]);

        sharedPreferences.setString('photo', '');
      } else {
        setState(() {
          isApiCallProcess = false;
        });
        loginError();
      }
    }
  }

  // logIn() async {
  //   if (formKey.currentState.validate()) {
  //     var email = emailTextEditingController.text;
  //     var password = passwordTextEditingController.text;
  //     setState(() {
  //       message = 'Please wait...';
  //     });
  //     var response = await loginUser(email, password);
  //     print(response);
  // if (response.cotainsKey('status')) {
  //   setState(() {
  //     print(response['status_text']);
  //   });
  //   if (response['status'] == 1) {
  //     setState(() {
  //       print('Success');
  //       message = response['status_text'];
  //     });
  //     Navigator.pushNamed(context, '/5_myBottomBar.dart');
  //   }
  // }
  //     if (response['status'] == 1) {
  //       setState(() {
  //         print('Success');
  //         message = response['status_text'];
  //       });
  //       Navigator.pushNamed(context, '/5_myBottomBar.dart');
  //     } else {
  //       setState(() {
  //         print('Login Failed');
  //         message = 'Login Failed';
  //       });
  //     }
  //   }
  // }

  // logIn() {
  //   if (formKey.currentState.validate()) {
  //     // emailTextEditingController.clear();
  //     // passwordTextEditingController.clear();
  //     AuthService().login(name, password).then((val) {
  //       //https://www.youtube.com/watch?v=2D_76lkyF1c&t=132s
  //       if (val.data['success']) {
  //         emailTextEditingController.clear();
  //         passwordTextEditingController.clear();
  //         token = val.data['token'];
  //         print('Authenticated');
  //         Navigator.pushNamed(context, '/5_myBottomBar.dart');
  //         toast(
  //           "Вход выполнен!",
  //           Toast.LENGTH_LONG,
  //           ToastGravity.BOTTOM,
  //           Colors.green,
  //           // toastDuration: Duration(seconds: 100)
  //         );
  //       }
  //     });
  //   }
  // }

  toast(
    String msg,
    Toast toast,
    ToastGravity toastGravity,
    Color colors,
  ) {
    Fluttertoast.showToast(
        timeInSecForIosWeb: 5,
        msg: msg,
        toastLength: toast,
        gravity: toastGravity,
        backgroundColor: colors,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);

    loginRequestModel = new LoginRequestModel();
  }

  bool isApiCallProcess = false;

  LoginRequestModel loginRequestModel;
  LoginResponseModel loginResponseModel;
  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  register() {
    Navigator.pushNamed(context, '/4_enter_register');
  }

  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

// FACEBOOK LOGIN NEW

  // Future<User> loadUserImage() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   String token = sharedPreferences.get("token");
  //   int id = sharedPreferences.get("id");
  //   String url = 'http://generation-admin.ehub.com.ua/api/user/update';

  //   String photoLink = sharedPreferences.get("photo");

  //   final http.Response response = await http.put(
  //     url,
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json',
  //       'Authorization': 'Bearer $token',
  //     },
  //     body: jsonEncode(<String, dynamic>{
  //       "id": id,
  //       "profileImageUrl": photoLink,
  //     }),
  //   );
  //   print(response.statusCode);
  //   print(response.body);
  //   if (response.statusCode == 200) {
  //     print(photoLink);
  //     print('Image loaded');
  //     print(response.body);
  //   } else {
  //     throw Exception('Failed to load image.');
  //   }
  // }

  var email;
  var password;
  var nickName;
  var photo;

  logInWithFacebook() async {
    setState(() {
      isApiCallProcess = true;
    });

    var facebookLogin = new FacebookLogin();
    var result = await facebookLogin.logIn(['email']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      final AuthCredential credential = FacebookAuthProvider.getCredential(
          accessToken: result.accessToken.token);
      final FirebaseUser user =
          (await FirebaseAuth.instance.signInWithCredential(credential)).user;

      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString('facebookName', user.displayName);
      sharedPreferences.setString('uid', result.accessToken.userId);
      sharedPreferences.setString('token', result.accessToken.token);
      sharedPreferences.setString('email', user.email);
      //sharedPreferences.setString('photo', user.photoUrl);

      email = sharedPreferences.get("email");
      nickName = sharedPreferences.get("facebookName");
      password = sharedPreferences.get("uid");

      sharedPreferences.setString('photo', null);
      //photo = sharedPreferences.get("photo");

      var response = await loginUser(email, password);
      print(response);
      if (response.statusCode == 200) {
        setState(() {
          isApiCallProcess = false;
        });
        Future.delayed(const Duration(seconds: 1), () {
          Navigator.pushNamed(context, '/5_myBottomBar.dart');
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);

        final SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setString('token', responseJson["access"]);
        sharedPreferences.setInt('id', responseJson["id"]);
        // loadUserImage();
      } else {
        setState(() {
          isApiCallProcess = false;
        });
      }
    }
  }

  bool isSignIn = false;
  GoogleSignIn _googleSignIn = new GoogleSignIn();
  FirebaseUser _user;
  FirebaseAuth _auth = FirebaseAuth.instance;

  signUpWithGoogle() async {
    setState(() {
      isApiCallProcess = true;
    });

    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

    AuthResult result = (await _auth.signInWithCredential(credential));
    _user = result.user;
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();

    sharedPreferences.setString('email', _user.email);
    sharedPreferences.setString('password', _user.uid);
    sharedPreferences.setString('googleName', _user.displayName);
    //sharedPreferences.setString('photo', _user.photoUrl);

    sharedPreferences.setString('photo', null);

    email = sharedPreferences.get("email");
    password = sharedPreferences.get("password");
    //photo = sharedPreferences.get("photo");
    nickName = sharedPreferences.get("googleName");

    print('GOOGLE EMAIL: ' + email);
    print('GOOGLE UID: ' + password);

    if (email == "" || email == null) {
      message = "Користувач не знайдений! Зареєструйтесь";
    } else {
      var response = await loginUser(email, password);
      print(response);
      if (response.statusCode == 200) {
        setState(() {
          isApiCallProcess = false;
        });
        Future.delayed(const Duration(seconds: 1), () {
          Navigator.pushNamed(context, '/5_myBottomBar.dart');
        });
        Map<String, dynamic> responseJson = jsonDecode(response.body);

        print(responseJson["access"]);
        print(responseJson["id"]);

        final SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();

        sharedPreferences.setString('token', responseJson["access"]);
        sharedPreferences.setInt('id', responseJson["id"]);
        // loadUserImage();
      } else {
        setState(() {
          isApiCallProcess = false;
        });
        loginError();
      }
    }
  }

  var name, token;

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Hexcolor('#7D5AC2'),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blankFocusNode);
        },
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                      margin: EdgeInsets.only(top: 60),
                      child: Image.asset(
                        'assets/fg_images/4_enter_logo.png',
                        width: 140,
                        height: 126,
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              // bottom: 30,
                              top: 30),
                          child: Text(
                            'Вхід в додаток',
                            style: TextStyle(
                              color: Hexcolor('#FFFFFF'),
                              fontSize: 25,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Arial',
                              letterSpacing: 1.28,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            '',
                            // 'Пользователь не найден!',
                            style: TextStyle(
                              color: userNotFound
                                  ? Hexcolor('#FFFFFF')
                                  : Hexcolor('#7D5AC2'),
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Arial',
                              letterSpacing: 1.0,
                            ),
                          ),
                        ),
                        Form(
                          key: formKey,
                          child: Column(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 7,
                                    ),
                                    child: Text(
                                      "Email",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                  TextFormField(
                                    onSaved: (value) =>
                                        loginRequestModel.email = value,
                                    onChanged: (val) {
                                      name = val;
                                    },
                                    validator: (value) {
                                      return RegExp(
                                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                              .hasMatch(value)
                                          ? null
                                          : "Будь-ласка введіть коректний email";
                                    },
                                    controller: emailTextEditingController,
                                    textAlign: TextAlign.left,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: InputDecoration(
                                      hintText: "Введіть email",
                                      contentPadding: new EdgeInsets.only(
                                          left: 20, top: 21.5, bottom: 21.5),
                                      hintStyle: TextStyle(
                                          color: Hexcolor('#D3D3D3'),
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'Arial'),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Hexcolor('#FE6802'),
                                          width: 1,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Colors.white54,
                                          width: 1,
                                        ),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Hexcolor('#FF1E1E'),
                                          width: 1,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(14.0),
                                        borderSide: BorderSide(
                                          color: Hexcolor('#FF1E1E'),
                                          width: 1,
                                        ),
                                      ),
                                      fillColor: Colors.white,
                                      filled: true,
                                    ),
                                    style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial'),
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 15,
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 7,
                                    ),
                                    child: Text(
                                      "Пароль",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      TextFormField(
                                        onSaved: (value) =>
                                            loginRequestModel.password = value,
                                        onChanged: (val) {
                                          password = val;
                                        },
                                        obscuringCharacter: '*',
                                        obscureText: isVisible ? false : true,
                                        validator: (value) {
                                          return value.length > 5
                                              ? null
                                              : "Будь-ласка введіть пароль не менше 6 символів";
                                        },
                                        controller:
                                            passwordTextEditingController,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                          hintText: "Введіть пароль",
                                          contentPadding: new EdgeInsets.only(
                                              left: 20,
                                              top: 21.5,
                                              bottom: 21.5),
                                          hintStyle: TextStyle(
                                              color: Hexcolor('#D3D3D3'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial'),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FE6802'),
                                              width: 1,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Colors.white54,
                                              width: 1,
                                            ),
                                          ),
                                          focusedErrorBorder:
                                              OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                            borderSide: BorderSide(
                                              color: Hexcolor('#FF1E1E'),
                                              width: 1,
                                            ),
                                          ),
                                          fillColor: Colors.white,
                                          filled: true,
                                        ),
                                        style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Arial'),
                                      ),
                                      Positioned(
                                        right: 22.4,
                                        top: 18,
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isVisible = !isVisible;
                                            });
                                          },
                                          child: !isVisible
                                              ? Image.asset(
                                                  'assets/fg_images/4_enter_visible.png',
                                                  color: Hexcolor('#9B9B9B'),
                                                  width: 24,
                                                  height: 24,
                                                )
                                              : Image.asset(
                                                  'assets/fg_images/4_enter_hidden.png',
                                                  color: Hexcolor('#9B9B9B'),
                                                  width: 24,
                                                  height: 24,
                                                ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              // ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(),
                                  // GestureDetector(
                                  //   onTap: () {
                                  //     Navigator.pushNamed(
                                  //         context, '/5_myBottomBar.dart');
                                  //   },
                                  //   child: Container(
                                  //     alignment: Alignment.centerRight,
                                  //     child: Container(
                                  //       margin: EdgeInsets.only(top: 9),
                                  //       child: Text(
                                  //         'Пропустити',
                                  //         style: TextStyle(
                                  //           color: Hexcolor('#FFFFFF'),
                                  //           fontSize: 15,
                                  //           fontFamily: 'Arial',
                                  //           fontWeight: FontWeight.w700,
                                  //         ),
                                  //       ),
                                  //     ),
                                  //   ),
                                  // ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context, '/4_enter_forgot_password');
                                    },
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 9),
                                        child: Text(
                                          'Забули пароль?',
                                          style: TextStyle(
                                            color: Hexcolor('#FFFFFF'),
                                            fontSize: 15,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              Container(
                                margin: EdgeInsets.only(bottom: 15, top: 15),
                                height: 60,
                                width: MediaQuery.of(context).size.width - 40,
                                child: RaisedButton(
                                  color: Hexcolor('#FE6802'),
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0)),
                                  child: Text("Вхід",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontFamily: 'Arial',
                                          color: Colors.white,
                                          letterSpacing: 1.09,
                                          fontSize: 17)),
                                  onPressed: () async {
                                    // if (formKey.currentState.validate()) {
                                    //   setState(() {
                                    //     _isLoading = true;
                                    //   });
                                    //   logIn(
                                    //       emailTextEditingController.text,
                                    //       passwordTextEditingController
                                    //           .text);
                                    // }
                                    final SharedPreferences sharedPreferences =
                                        await SharedPreferences.getInstance();
                                    sharedPreferences.setString('email',
                                        emailTextEditingController.text);
                                    logIn();
                                  },
                                ),
                              ),
                              Text(
                                message,
                                style: TextStyle(color: Colors.white),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 15),
                                child: Text(
                                  'АБО',
                                  style: TextStyle(
                                      color: Hexcolor('#FFFFFF'),
                                      fontSize: 17,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 1.09,
                                      fontFamily: 'Arial'),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            25,
                                    child: RaisedButton(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/fg_images/4_enter_facebook.png',
                                            width: 14,
                                            height: 25,
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            'Facebook',
                                            style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              color: Hexcolor('#FFFFFF'),
                                            ),
                                          )
                                        ],
                                      ),
                                      color: Hexcolor('#3B5998'),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(14.0)),
                                      onPressed: () async {
                                        logInWithFacebook();
                                        SharedPreferences sharedPreferences =
                                            await SharedPreferences
                                                .getInstance();
                                        sharedPreferences.setString(
                                            'photo', "");
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            25,
                                    child: RaisedButton(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/fg_images/4_enter_google.png',
                                            width: 30,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Text(
                                            'Google',
                                            style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              color: Hexcolor('#1E2E45'),
                                            ),
                                          )
                                        ],
                                      ),
                                      color: Hexcolor('#FFFFFF'),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(14.0)),
                                      onPressed: () async {
                                        signUpWithGoogle();
                                        SharedPreferences sharedPreferences =
                                            await SharedPreferences
                                                .getInstance();
                                        sharedPreferences.setString(
                                            'photo', "");
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              // Container(
                              //   margin: EdgeInsets.only(top: 15),
                              //   height: 60,
                              //   width: MediaQuery.of(context).size.width - 40,
                              //   child: RaisedButton(
                              //     color: Hexcolor('#FFFFFF'),
                              //     shape: new RoundedRectangleBorder(
                              //         borderRadius:
                              //             new BorderRadius.circular(14.0)),
                              //     child: Row(
                              //       mainAxisAlignment: MainAxisAlignment.center,
                              //       crossAxisAlignment:
                              //           CrossAxisAlignment.center,
                              //       children: [
                              //         Image.asset(
                              //           'assets/fg_images/4_enter_apple.png',
                              //           width: 21.34,
                              //           height: 25.34,
                              //         ),
                              //         SizedBox(
                              //           width: 20,
                              //         ),
                              //         Text(
                              //           'Увійти з Apple',
                              //           style: TextStyle(
                              //             fontFamily: 'Arial',
                              //             fontSize: 17,
                              //             fontWeight: FontWeight.w700,
                              //             color: Hexcolor('#1E2E45'),
                              //           ),
                              //         )
                              //       ],
                              //     ),
                              //     onPressed: () {
                              //       print('Apple');
                              //     },
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                        top: 20,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Немає аккаунта? ',
                            style: TextStyle(
                              fontFamily: 'Arial',
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                              color: Hexcolor('#FFFFFF'),
                            ),
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              register();
                            },
                            child: Text(
                              'Зареєструватися',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontFamily: 'Arial',
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                color: Hexcolor('#FFFFFF'),
                              ),
                            ),
                          )
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 20,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Використовуючи цей додаток, ви погоджуєтесь з нашими',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Arial',
                            color: Colors.white,
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  _launchUrl('https://freegen.net/');
                                },
                                child: Text(
                                  'Умовами',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Arial',
                                    color: Hexcolor('#FE6802'),
                                    fontSize: 12,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                              Text(
                                ' і ',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'Arial',
                                  color: Colors.white,
                                  fontSize: 12,
                                ),
                              ),
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: () {
                                  _launchUrl('https://freegen.net/');
                                },
                                child: Text(
                                  'Політикою конфіденційності',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Arial',
                                    color: Hexcolor('#FE6802'),
                                    fontSize: 12,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                            ])
                      ],
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
