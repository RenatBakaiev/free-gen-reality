// import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';

// import '6_home.dart';
// import 'package:hexcolor/hexcolor.dart';

class SplashScreenLanguage extends StatefulWidget {
  @override
  _SplashScreenLanguageState createState() => _SplashScreenLanguageState();
}

class _SplashScreenLanguageState extends State<SplashScreenLanguage> {
  bool englishSelected = false;
  bool ukrainianSelected = false;
  bool russianSelected = false;

  bool signalTurnedOn = false;

  goNext() {
    Future.delayed(const Duration(milliseconds: 500), () {
      Navigator.pushNamed(context, '/1_layout_1');
    });
  }

  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

  // makeSignal() {
  //   setState(() {
  //     signalTurnedOn = true;
  //   });
  //   Future.delayed(const Duration(milliseconds: 500), () {
  //     setState(() {
  //       signalTurnedOn = false;
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Stack(
          overflow: Overflow.visible,
          children: [
            GestureDetector(
              onTap: () {
                // Navigator.pushNamed(context, '/1_layout_1');
              },
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        margin: EdgeInsets.only(top: 60),
                        child: Image.asset(
                          'assets/fg_images/0_splash_screen_logo2.png',
                          width: 227,
                          height: 178,
                        ),
                      ),
                    ),
                    Container(
                      child: Column(children: [
                        Container(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    bottom: 15,
                                    // top: 120,
                                  ),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width - 50,
                                  child: RaisedButton(
                                    color: ukrainianSelected
                                        ? Hexcolor('#75C433')
                                        : Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    child: Text("Українська",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Arial',
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      goNext();
                                      setState(() {
                                        // EasyLocalization.of(context).locale =
                                        //     Locale("uk","UA");
                                        ukrainianSelected = true;
                                        if (englishSelected ||
                                            russianSelected) {
                                          englishSelected = false;
                                          russianSelected = false;
                                        }
                                        // Home().method2();
                                      });
                                    },
                                  ),
                                ),
                              ]),
                        ),
                        Container(
                          child: Column(children: [
                            Container(
                              margin: EdgeInsets.only(
                                  // bottom: 30
                                  ),
                              height: 60,
                              width: MediaQuery.of(context).size.width - 50,
                              child: RaisedButton(
                                color: russianSelected
                                    ? Hexcolor('#75C433')
                                    : Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                child: Text("Русский",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontFamily: 'Arial',
                                        color: Colors.white,
                                        letterSpacing: 1.09,
                                        fontSize: 17)),
                                onPressed: () {                                 
                                  goNext();
                                  setState(() {
                                    // EasyLocalization.of(context).locale =
                                    //   Locale("ru","RU");
                                    russianSelected = true;
                                    if (englishSelected || ukrainianSelected) {
                                      englishSelected = false;
                                      ukrainianSelected = false;
                                    }
                                  });
                                },
                              ),
                            ),
                          ]),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            // top: 60,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 50,
                          child: RaisedButton(
                            color: Hexcolor("#BEBEBE"),
                            // englishSelected
                            // ? Hexcolor('#75C433')
                            // : Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            child: Text("English",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontFamily: 'Arial',
                                    color: Colors.white,
                                    letterSpacing: 1.09,
                                    fontSize: 17)),
                            onPressed: () {
                              // goNext();
                              // setState(() {
                              //   englishSelected = true;
                              //   if (ukrainianSelected || russianSelected) {
                              //     ukrainianSelected = false;
                              //     russianSelected = false;
                              //   }
                              //   // Home().method();
                              // });
                            },
                          ),
                        ),
                      ]),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        bottom: 20,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Используя это приложение, вы соглашаетесь с нашими',
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Arial',
                              color: Colors.white,
                              fontSize: 12,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    _launchUrl('https://freegen.net/');
                                  },
                                  child: Text(
                                    'Условиями',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                      color: Hexcolor('#FE6802'),
                                      fontSize: 12,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                                Text(
                                  ' и ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Arial',
                                    color: Colors.white,
                                    fontSize: 12,
                                  ),
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    _launchUrl('https://freegen.net/');
                                  },
                                  child: Text(
                                    'Политикой конфиденциальности',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                      color: Hexcolor('#FE6802'),
                                      fontSize: 12,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                              ])
                        ],
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image:
                            AssetImage('assets/fg_images/0_splash_screen.jpg'),
                        fit: BoxFit.cover)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
