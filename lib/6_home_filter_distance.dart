// ----------------------------------------------NEW MAP 25.09.20--------------------------------------------------------------------
// https://www.youtube.com/watch?v=Hg8CT3ysFjY&t=528s - GEOLOCATOR

import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';

class HomeFilterDistance extends StatefulWidget {
  // https://www.youtube.com/watch?v=2pFyy-ARuhw
  @override
  _HomeFilterDistanceState createState() => _HomeFilterDistanceState();
}

class _HomeFilterDistanceState extends State<HomeFilterDistance> {
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  var currentLocation;
  bool mapToggle = false;

  bool isPressed0 = true;
  bool isPressed5 = false;
  bool isPressed10 = false;
  bool isPressed15 = false;

  double zoomVal = 17.0;

  Set<Marker> _markers =
      HashSet<Marker>(); //  https://www.youtube.com/watch?v=N0NfbhF2A3g&t=123s
  @override
  void initState() {
    super.initState();
    Geolocator.getCurrentPosition().then((currloc) {
      setState(() {
        currentLocation = currloc;
        mapToggle = true;
      });
    });
  }

  Future<void> _set0km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _set5km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _set10km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _set15km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(overflow: Overflow.visible, children: [
          Container(
            margin: EdgeInsets.only(top: 115),
            child: mapToggle
                ? GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: LatLng(currentLocation.latitude,
                            currentLocation.longitude),
                        zoom: 18.0),
                    mapType: MapType.normal,
                    tiltGesturesEnabled: true,
                    compassEnabled: true,
                    scrollGesturesEnabled: true,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: false, // off default buttons
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false, // off default my location
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                    markers: _markers,
                  )
                : Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Hexcolor('#7D5AC2'),
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        Hexcolor('#FE6802'),
                      ),
                    ),
                  ),
          ),
          Positioned(
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                  left: 20,
                  right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Відстань',
                      style: TextStyle(
                          fontSize: 37,
                          color: Colors.white,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w900),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 38,
                          height: 38,
                          child: IconButton(
                            icon: Image.asset(
                              'assets/fg_images/6_home_filter_close.png',
                              width: 20,
                              height: 20,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/6_home_filter.dart');
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
              bottom: 115, // Chips
              child: Container(
                width: MediaQuery.of(context).copyWith().size.width,
                child: Container(
                  margin: EdgeInsets.only(right: 20, left: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 17.5,
                          child: RaisedButton(
                            child: Text(
                              "0 км",
                              style: TextStyle(
                                color: isPressed0
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed0
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = true;
                                isPressed5 = false;
                                isPressed10 = false;
                                isPressed15 = false;
                                _set0km(18.0);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 17.5,
                          child: RaisedButton(
                            child: Text(
                              "5 км",
                              style: TextStyle(
                                color: isPressed5
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed5
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = false;
                                isPressed5 = true;
                                isPressed10 = false;
                                isPressed15 = false;
                                _set5km(12);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 17.5,
                          child: RaisedButton(
                            child: Text(
                              "10 км",
                              style: TextStyle(
                                color: isPressed10
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed10
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = false;
                                isPressed5 = false;
                                isPressed10 = true;
                                isPressed15 = false;
                                _set10km(11.25);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 17.5,
                          child: RaisedButton(
                            child: Text(
                              "15 км",
                              style: TextStyle(
                                color: isPressed15
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed15
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = false;
                                isPressed5 = false;
                                isPressed10 = false;
                                isPressed15 = true;
                                _set15km(10.5);
                              });
                            },
                          ),
                        ),
                      ]),
                ),
              )),
          Positioned(
            bottom: 20,
            child: Container(
              margin: EdgeInsets.only(right: 20, left: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Готово",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Arial',
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_home_filter.dart');
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
