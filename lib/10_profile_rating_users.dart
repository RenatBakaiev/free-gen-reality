import '10_profile_rating_model.dart';

final User user1 = User(
  name: 'Алина Квиталина',
  image: 'assets/fg_images/10_profile_rating_user_1.jpg',
  rating: '1564',
);

final User user2 = User(
  name: 'Дмитрий Недельский',
  image: 'assets/fg_images/10_profile_rating_user_2.jpg',
  rating: '1347',
);

final User user3 = User(
  name: 'Коваленко Андрей',
  image: 'assets/fg_images/10_profile_rating_user_3.jpg',
  rating: '1259',
);

final User user4 = User(
  name: 'Валерия Дудикова',
  image: 'assets/fg_images/10_profile_rating_user_4.jpg',
  rating: '1025',
);

final User user5 = User(
  name: 'Кристина Галяндина',
  image: 'assets/fg_images/10_profile_rating_user_5.jpg',
  rating: '987',
);

final User user6 = User(
  name: 'Артур Пирожков',
  image: 'assets/fg_images/10_profile_rating_user_6.jpg',
  rating: '874',
);

final User user7 = User(
  name: 'Джек Петров',
  image: 'assets/fg_images/10_profile_rating_user_7.jpg',
  rating: '542',
);

final User user8 = User(
  name: 'Андрей Понамаренко',
  image: 'assets/fg_images/10_profile_rating_user_8.jpg',
  rating: '357',
);


List<User> users = [
  user1,
  user2,
  user3,
  user4,
  user5,
  user6,
  user7,
  user8,
];