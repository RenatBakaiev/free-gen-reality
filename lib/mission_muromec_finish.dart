import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';

class MissionMuromecFinish extends StatefulWidget {
  @override
  _MissionMuromecFinishState createState() => _MissionMuromecFinishState();
}

class _MissionMuromecFinishState extends State<MissionMuromecFinish> {
  AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlayer2 = new AudioPlayer();

  bool isPlaying = false;
  bool isApiCallProcess = false;

  void _getAudio2() async {
    var url =
        "http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-02-05_muromec_info_finish.mp3";
    var res = await audioPlayer2.play(url, isLocal: true);
    if (res == 1) {
      setState(() {
        isPlaying = true;
      });
    }
  }

  void _stopFile() {
    audioPlayer2?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer2.pause();
    } else {
      audioPlayer2.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  @override
  void initState() {
    _getAudio2();
    super.initState();
  }

  bool isFavorite = false;

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    setState(() {
      isApiCallProcess = true;
    });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      print(response.body);
      print('muromec step 14 done!!!');
      setState(() {
        isApiCallProcess = false;
      });
    } else {
      setState(() {
        isApiCallProcess = false;
      });
      throw Exception('Failed to update User.');
    }
  }

  checkAllPoints() {
    showDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width - 80,
              height: 230,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  // bottom: 10,
                                  ),
                              child: Text(
                                'Невдача!',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900,
                                  letterSpacing: 1.089,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ])),
                  Flexible(
                    child: Text(
                      'Спочатку необхідно пройти всі точки!',
                      style: TextStyle(
                        color: Hexcolor('#000000'),
                        fontSize: 17,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w900,
                        letterSpacing: 1.089,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Далі",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: Hexcolor('#EB5C18'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              Navigator.pushNamed(
                                  context, '/mission_muromec_map.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  showAllChepointsDone() {
    showDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width - 80,
              height: 230,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  // bottom: 10,
                                  ),
                              child: Text(
                                'Вітаємо!',
                                style: TextStyle(
                                  color: Hexcolor('#58B12D'),
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900,
                                  letterSpacing: 1.089,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ])),
                  Text(
                    'Всі точки вже пройдено!',
                    style: TextStyle(
                      color: Hexcolor('#000000'),
                      fontSize: 17,
                      fontFamily: 'Arial',
                      fontWeight: FontWeight.w900,
                      letterSpacing: 1.089,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Далі",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: Hexcolor('#EB5C18'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              Navigator.pushNamed(
                                  context, '/mission_muromec_map.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  showYouReceiveRewardWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 415,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдено!',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            'Вам нараховано ',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 20,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1.089,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            '100 TUS',
                            style: TextStyle(
                              color: Hexcolor('#58B12D'),
                              fontSize: 20,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w900,
                              letterSpacing: 1.089,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Image.asset(
                        'assets/fg_images/muromec_tus.png',
                        height: 185,
                        width: 185,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Далі",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () async {
                            Navigator.pushNamed(
                                context, '/mission_muromec_quest_done.dart');
                            // Navigator.pushNamed(
                            //     context, '/mission_muromec_map.dart');
                            final SharedPreferences sharedPreferences =
                                await SharedPreferences.getInstance();
                            var res =
                                sharedPreferences.get('muromecStep14Done');
                            if (res == false) {
                              changeTus(100, 'DEPOSIT');
                              sharedPreferences.setBool(
                                  'muromecStep14Done', true);
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -135,
                  right: -40,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 80
                    //  - 85
                    ,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 225,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                                  image: NetworkImage(
                                      'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_Muromec_pic_finish.gif'),
                                  fit: BoxFit.cover,
                                )),
                            child: Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                right: 20,
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(),
                                      GestureDetector(
                                        behavior: HitTestBehavior.opaque,
                                        onTap: () {
                                          pausePlay();
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            top: 30,
                                          ),
                                          child: Image.asset(
                                            !isPlaying
                                                ? 'assets/fg_images/audio_play.png'
                                                : 'assets/fg_images/audio_pause.png',
                                            height: 50,
                                            width: 50,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Опис',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    ' P.S.: Якщо ти потрапив у топ-10 рейтингу, то забирай свій приз у кафе Оло.',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 16,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: SizedBox(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_search_back.png',
                                    width: 10,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    _stopFile();
                                    Navigator.pushNamed(
                                        context, '/mission_muromec_map.dart');
                                  },
                                ),
                              ),
                            ),
                            Text(
                              'Інформація',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/run_map_quest_pic_gostart.png',
                                width: 22,
                                height: 24,
                              ),
                              onPressed: () {
                                _stopFile();
                                Navigator.pushNamed(
                                  context,
                                  '/mission_muromec_desc.dart',
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Далі",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () async {
                  _stopFile();
                  final SharedPreferences sharedPreferences =
                      await SharedPreferences.getInstance();
                  var res1 = sharedPreferences.get('muromecStep1Done');
                  var res2 = sharedPreferences.get('muromecStep2Done');
                  var res3 = sharedPreferences.get('muromecStep3Done');
                  var res4 = sharedPreferences.get('muromecStep4Done');
                  var res5 = sharedPreferences.get('muromecStep5Done');
                  var res6 = sharedPreferences.get('muromecStep6Done');
                  var res7 = sharedPreferences.get('muromecStep7Done');
                  var res8 = sharedPreferences.get('muromecStep8Done');
                  var res9 = sharedPreferences.get('muromecStep9Done');
                  var res10 = sharedPreferences.get('muromecStep10Done');
                  var res11 = sharedPreferences.get('muromecStep11Done');
                  var res12 = sharedPreferences.get('muromecStep12Done');
                  var res13 = sharedPreferences.get('muromecStep13Done');
                  var res14 = sharedPreferences.get('muromecStep14Done');
                  if (res1 == true &&
                      res2 == true &&
                      res3 == true &&
                      res4 == true &&
                      res5 == true &&
                      res6 == true &&
                      res7 == true &&
                      res8 == true &&
                      res9 == true &&
                      res10 == true &&
                      res11 == true &&
                      res12 == true &&
                      res13 == true &&
                      res14 == true) {
                    print('all done');
                    showAllChepointsDone();
                  } else {
                    if (res1 == true &&
                        res2 == true &&
                        res3 == true &&
                        res4 == true &&
                        res5 == true &&
                        res6 == true &&
                        res7 == true &&
                        res8 == true &&
                        res9 == true &&
                        res10 == true &&
                        res11 == true &&
                        res12 == true &&
                        res13 == true &&
                        res14 == false) {
                      Navigator.pushNamed(
                          context, '/mission_muromec_quest_done.dart');
                      // Navigator.pushNamed(
                      //     context, '/mission_muromec_map.dart');
                      changeTus(100, 'DEPOSIT');
                      sharedPreferences.setBool('muromecStep14Done', true);
                      // showYouReceiveRewardWindow();
                    } else {
                      checkAllPoints();
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
