import 'package:hexcolor/hexcolor.dart';

import '10_profile_model.dart';

final Reward reward1 = Reward(
  name: 'TUS',
  image: 'assets/fg_images/10_profile_user_param_icon_tus.png',
  quantity: 0,
  backColor: Hexcolor('#7238FC'),
);

final Reward reward2 = Reward(
  name: 'SKILL',
  image: 'assets/fg_images/10_profile_user_param_icon_skill.png',
  quantity: 0,
  backColor: Hexcolor('#7238FC'),
);

final Reward reward3 = Reward(
  name: 'TIKI',
  image: 'assets/fg_images/10_profile_user_param_icon_tiki.png',
  quantity: 0,
  backColor: Hexcolor('#7238FC'),
);

final Reward reward4 = Reward(
  name: 'KARMA',
  image: 'assets/fg_images/10_profile_user_param_icon_karma.png',
  quantity: 0,
  backColor: Hexcolor('#FFFFFF'),
);

final Reward reward5 = Reward(
  name: 'HIP',
  image: 'assets/fg_images/10_profile_user_param_icon_hip.png',
  quantity: 0,
  backColor: Hexcolor('#7238FC'),
);

final Reward reward6 = Reward(
  name: 'FOINT',
  image: 'assets/fg_images/10_profile_user_param_icon_foint.png',
  quantity: 0,
  backColor: Hexcolor('#FFFFFF'),
);

List<Reward> allRewards = [
  reward1,
  reward2,
  reward3,
  reward4,
  reward5,
  reward6,
];

final ProfileItem profileItem1 = ProfileItem(
  name: 'QR - Code',
  color: Hexcolor('#2084D9'),
  icon: 'assets/fg_images/10_profile_item_qr_code.png',
  link: '/10_profile_qr_code.dart',
);

final ProfileItem profileItem2 = ProfileItem(
  name: 'Рейтинг',
  color: Hexcolor('#8B59D0'),
  icon: 'assets/fg_images/10_profile_item_rating.png',
  link: '/10_profile_rating.dart',
);

final ProfileItem profileItem3 = ProfileItem(
  name: 'Магазин',
  color: Hexcolor('#FF587A'),
  icon: 'assets/fg_images/10_profile_item_store.png',
  link: '/10_profile_store.dart',
);

final ProfileItem profileItem4 = ProfileItem(
  name: 'Налаштування',
  color: Hexcolor('#EB5C18'),
  icon: 'assets/fg_images/10_profile_item_settings.png',
  link: '/10_profile_edit.dart',
);

final ProfileItem profileItem5 = ProfileItem(
  name: 'Вийти',
  color: Hexcolor('#D04437'),
  icon: 'assets/fg_images/10_profile_item_exit.png',
  link: '',
);

final ProfileItem profileItem6 = ProfileItem(
  name: 'Новини',
  color: Hexcolor('#2084D9'),
  icon: 'assets/fg_images/10_profile_item_news.png',
  link: '/10_profile_news_list.dart',
);

final ProfileItem profileItem7 = ProfileItem(
  name: 'ЧаПи',
  color: Hexcolor('#7C59C0'),
  icon: 'assets/fg_images/10_profile_item_faq.png',
  link: '/10_profile_faq.dart',
);

final ProfileItem profileItem8 = ProfileItem(
  name: 'Партнери',
  color: Hexcolor('#30B93A'),
  icon: 'assets/fg_images/10_profile_item_partners.png',
  link: '/10_profile_partners.dart',
);

final ProfileItem profileItem9 = ProfileItem(
  name: 'Співробітництво',
  color: Hexcolor('#37AACF'),
  icon: 'assets/fg_images/10_profile_item_cooperation.png',
  link: '/10_profile_cooperation.dart',
);

final ProfileItem profileItem10 = ProfileItem(
  name: 'Про нас',
  color: Hexcolor('#3892D0'),
  icon: 'assets/fg_images/10_profile_item_aboutus.png',
  link: '/10_profile_aboutus.dart',
);

final ProfileItem profileItem11 = ProfileItem(
  name: 'Контакти',
  color: Hexcolor('#CE7E37'),
  icon: 'assets/fg_images/10_profile_item_contacts.png',
  link: '/10_profile_contacts.dart',
);

List<ProfileItem> itemsForProfile = [
  // profileItem1,
  profileItem2,
  profileItem3,
  profileItem4,
  profileItem5, 
];

List<ProfileItem> itemsForMenu = [
  profileItem6,
  profileItem7,
  profileItem8,
  profileItem9,
  profileItem10,  
  profileItem11,  
];
