import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ProfileProduct3 extends StatefulWidget {
  @override
  _ProfileProduct3State createState() => _ProfileProduct3State();
}

class _ProfileProduct3State extends State<ProfileProduct3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: 265,
              decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0)),
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/fg_images/10_profile_store_product_3.jpg'),
                    fit: BoxFit.cover,
                  )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        margin: EdgeInsets.only(
                          left: 30,
                          top: 65,
                        ),
                        child: Image.asset(
                          'assets/fg_images/sportGames_icon_arrow_back.png',
                          width: 10,
                          height: 19,
                          color: Hexcolor('#FFFFFF'),
                        )),
                  ),
                ],
              ),
            ),
            Container(
              child: Expanded(
                  child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                          // margin: EdgeInsets.only(left: 5),
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30, left: 30),
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        child: Container(
                                      margin: EdgeInsets.only(bottom: 20),
                                      child: Text(
                                        'DJI Phantom3 Professional Electronic',
                                        style: TextStyle(
                                            fontSize: 25,
                                            color: Hexcolor('#1E2E45'),
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w600),
                                      ),
                                    )),
                                    // Image.asset(
                                    //   storeProducts[index].favoriteIcon,
                                    //   height: 23,
                                    //   width: 18,
                                    // ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Цена',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Hexcolor('#B9BCC4'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '120' + ' Foint',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Hexcolor('#298127'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Image.asset(
                                          'assets/fg_images/10_profile_store_icon_map.png',
                                          height: 26,
                                          width: 17,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 5),
                                          child: Container(
                                            child: Text(
                                              'Киев, Дарницкий',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Hexcolor('#1E2E45'),
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '29 января, 01:22',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Hexcolor('#B9BCC4'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 30,
                              right: 30,
                              top: 30,
                              bottom: 20,
                            ),
                            child: Column(children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 15),
                                child: Row(
                                  children: [
                                    Text(
                                      'Описание',
                                      style: TextStyle(
                                          color: Hexcolor('#484848'),
                                          fontSize: 18,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                                style: TextStyle(
                                  color: Hexcolor('#484848'),
                                  fontSize: 16,
                                  fontFamily: 'Arial',
                                ),
                              ),
                            ]),
                          ),
                        ],
                      )))),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 50,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Купить",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Arial',
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  // Navigator.pushNamed(context, '/5_myBottomBar.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
