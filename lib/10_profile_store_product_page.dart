import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProfileProductPage extends StatefulWidget {
  @override
  _ProfileProductPageState createState() => _ProfileProductPageState();
}

class _ProfileProductPageState extends State<ProfileProductPage> {
  bool loading = true;
  double foints;

  getUserData() async {
    setState(() {
      loading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);

        print(responseJson["accounts"]);

        if (response.statusCode == 200) {
          setState(() {
            for (int i = 0; i < responseJson["accounts"].length; i++) {
              if (responseJson["accounts"][i]["currency"] == "FOINT") {
                foints = responseJson["accounts"][i]["accountBalance"];
                print('You have $foints FOINT');
              }
            }
            loading = false;
          });
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  Future<User> changeTus(
      double value, String operation, String typeCurrency) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": typeCurrency,
        "operation": operation,
        "amount": value,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');

    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('quiz change done!!!');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    notEnoughFoints() {
      showGeneralDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          transitionBuilder: (context, a1, a2, widget) {
            final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
            double productPrice = arguments["price"];
            double quantity = productPrice - foints;
            return Transform(
              transform:
                  Matrix4.translationValues(0.0, curvedValue * -200, 0.0),
              child: Opacity(
                  opacity: a1.value,
                  child: Dialog(
                    insetPadding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      height: 490,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              top: 20,
                            ),
                            child: Text(
                              'Недостатньо коштів',
                              style: TextStyle(
                                color: Hexcolor('#1E2E45'),
                                fontSize: 24,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Вам не вистачає ',
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Text(
                                '$quantity' + ' Foint!',
                                style: TextStyle(
                                  color: Hexcolor('#FF1E1E'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 80,
                            height: 200,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                image: DecorationImage(
                                  image: NetworkImage(arguments["image"]),
                                  fit: BoxFit.cover,
                                )),
                          ),
                          Text(
                            arguments["name"],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                color: Hexcolor('1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Вартість',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Hexcolor('#B9BCC4'),
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                arguments["price"].toString() + ' Foint',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Hexcolor('#FF1E1E'),
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              right: 20,
                              bottom: 20,
                            ),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 80,
                            child: RaisedButton(
                              child: Text(
                                "Закрити",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                                // Navigator.pushNamed(
                                //     context, '/10_profile_store.dart');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
            );
          },
          transitionDuration: Duration(milliseconds: 500),
          barrierDismissible: false,
          barrierLabel: '',
          context: context,
          pageBuilder: (context, animation1, animation2) {
            return Container();
          });
    }

    youBoughtProduct() {
      showGeneralDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          transitionBuilder: (context, a1, a2, widget) {
            final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
            return Transform(
              transform:
                  Matrix4.translationValues(0.0, curvedValue * -200, 0.0),
              child: Opacity(
                  opacity: a1.value,
                  child: Dialog(
                    insetPadding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      height: 490,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              top: 20,
                            ),
                            child: Text(
                              'Вітаємо',
                              style: TextStyle(
                                color: Hexcolor('#1E2E45'),
                                fontSize: 24,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Text(
                            'Ви успішно купили товар!',
                            style: TextStyle(
                              color: Hexcolor('#59B32D'),
                              fontSize: 20,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 80,
                            height: 200,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                image: DecorationImage(
                                  image: NetworkImage(arguments["image"]),
                                  fit: BoxFit.cover,
                                )),
                          ),
                          Text(
                            arguments["name"],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                color: Hexcolor('1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Вартість',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Hexcolor('#B9BCC4'),
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                arguments["price"].toString() + ' Foint',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Hexcolor('#59B32D'),
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              right: 20,
                              bottom: 20,
                            ),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 80,
                            child: RaisedButton(
                              child: Text(
                                "Закрити",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
            );
          },
          transitionDuration: Duration(milliseconds: 500),
          barrierDismissible: false,
          barrierLabel: '',
          context: context,
          pageBuilder: (context, animation1, animation2) {
            return Container();
          });
    }

    confirmBuyProduct() {
      showGeneralDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          transitionBuilder: (context, a1, a2, widget) {
            final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
            double productPrice = arguments["price"];

            return Transform(
              transform:
                  Matrix4.translationValues(0.0, curvedValue * -200, 0.0),
              child: Opacity(
                  opacity: a1.value,
                  child: Dialog(
                    insetPadding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      height: 490,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              top: 20,
                            ),
                            child: Text(
                              'Купівля товару',
                              style: TextStyle(
                                color: Hexcolor('#1E2E45'),
                                fontSize: 24,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Text(
                            'Ви точно хочете купити?',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 20,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 80,
                            height: 200,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                image: DecorationImage(
                                  image: NetworkImage(arguments['image']),
                                  fit: BoxFit.cover,
                                )),
                          ),
                          Text(
                            arguments['name'],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                color: Hexcolor('1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Вартість',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Hexcolor('#B9BCC4'),
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                arguments['price'].toString() + ' Foint',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Hexcolor('#59B32D'),
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                bottom: 20, right: 20, left: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      45,
                                  child: RaisedButton(
                                    child: Text(
                                      "Скасування",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    color: Hexcolor('#BEBEBE'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      // Navigator.pushNamed(context,
                                      //     '/10_profile_store_product_page.dart');
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      45,
                                  child: RaisedButton(
                                    child: Text(
                                      "Купити",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      if (productPrice > foints) {
                                        notEnoughFoints();
                                        print('no money!');
                                      }
                                      if (productPrice < foints ||
                                          productPrice == foints) {                                        
                                        changeTus(productPrice, "WITHDRAW", "FOINT");
                                        youBoughtProduct();
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
            );
          },
          transitionDuration: Duration(milliseconds: 500),
          barrierDismissible: false,
          barrierLabel: '',
          context: context,
          pageBuilder: (context, animation1, animation2) {
            return Container();
          });
    }

    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 265,
                    decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0)),
                        image: DecorationImage(
                          image: NetworkImage(arguments['image']),
                          fit: BoxFit.cover,
                        )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // GestureDetector(
                        //   behavior: HitTestBehavior.opaque,
                        //   onTap: () {
                        //     Navigator.pushNamed(context, '/10_profile_store.dart');
                        //   },
                        //   child:
                        Container(
                          // width: 10,
                          // height: 19,
                          margin: EdgeInsets.only(
                            left: 20,
                            top: 45,
                          ),
                          child: IconButton(
                            icon: Image.asset(
                              'assets/fg_images/sportGames_icon_arrow_back.png',
                              width: 10,
                              height: 19,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/10_profile_store.dart');
                            },
                          ),
                          // Image.asset(
                          //   'assets/fg_images/sportGames_icon_arrow_back.png',
                          //   width: 10,
                          //   height: 19,
                          //   color: Hexcolor('#FFFFFF'),
                          // )
                        ),
                        // ),
                      ],
                    ),
                  ),
                  Container(
                    child: Expanded(
                        child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Container(
                                // margin: EdgeInsets.only(left: 5),
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 30, left: 20, right: 20),
                                  width: MediaQuery.of(context).size.width,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Container(
                                            margin: EdgeInsets.only(bottom: 20),
                                            child: Text(
                                              arguments['name'],
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  color: Hexcolor('#1E2E45'),
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          )),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Цена',
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Hexcolor('#B9BCC4'),
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            arguments['price']
                                                    .toInt()
                                                    .toString() +
                                                ' Foint',
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Hexcolor('#298127'),
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              Image.asset(
                                                'assets/fg_images/10_profile_store_icon_map.png',
                                                height: 26,
                                                width: 17,
                                              ),
                                              Flexible(
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Text(
                                                    arguments['address'],
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            arguments['time'],
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Hexcolor('#B9BCC4'),
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                    top: 30,
                                    bottom: 20,
                                  ),
                                  child: Column(children: [
                                    Container(
                                      margin: EdgeInsets.only(bottom: 15),
                                      child: Row(
                                        children: [
                                          Text(
                                            'Опис',
                                            style: TextStyle(
                                                color: Hexcolor('#484848'),
                                                fontSize: 18,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        arguments['desc'],
                                        style: TextStyle(
                                          color: Hexcolor('#484848'),
                                          fontSize: 16,
                                          fontFamily: 'Arial',
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                              ],
                            )))),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(14.0),
                      ),
                      child: Text("Купити",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Arial',
                              color: Colors.white,
                              letterSpacing: 1.09,
                              fontSize: 17)),
                      onPressed: () {
                        confirmBuyProduct();
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
