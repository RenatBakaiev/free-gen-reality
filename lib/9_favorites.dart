import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import '6_home_missions_favorite_json.dart';
import 'dart:convert' show utf8;
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './10_profile_statuses_json.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

// class Debouncer {
//   final int milliseconds;
//   VoidCallback action;
//   Timer _timer;
//   Debouncer({this.milliseconds});

//   run(VoidCallback action) {
//     if (null != _timer) {
//       _timer.cancel();
//     }
//     _timer = Timer(Duration(milliseconds: milliseconds), action);
//   }
// }

class Favorites extends StatefulWidget {
  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  var _blankFocusNode = new FocusNode();
  // final _debouncer = Debouncer(milliseconds: 1000);

  bool loadingPosition = true;
  Geolocator geolocator = Geolocator();
  var currentLocation;

  getPosition() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        loadingPosition = false;
      });
    });
  }

  String userStatus;

  getUserData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            for (int i = 0; i < userStatuses.length; i++) {
              if (responseJson["status"] == userStatuses[i].id.toString()) {
                userStatus =
                    utf8.decode(userStatuses[i].titleUa.runes.toList());
              }
            }
            print('User status: $userStatus');
            sharedPreferences.setString("userStatus", userStatus);
          });
        }

        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  List<Publication> userStatuses;
  getStatuses() {
    ServicesStatuses.getStatuses().then((list) {
      setState(() {
        userStatuses = list.publication;
      });
    });
  }

  List<FavoriteMissions> filteredMissions = List<FavoriteMissions>();
  List<FavoriteMissions> missions = List<FavoriteMissions>();
  bool loading;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  @override
  void initState() {
    super.initState();
    loading = true;
    getStatuses();
    getUserData();
    getPosition();
    ServicesFavoriteMissions.getFavoriteMissions().then((list) {
      setState(() {
        missions = list;
        filteredMissions = missions;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loadingPosition
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(_blankFocusNode);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#F2F2F2'),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Hexcolor('#7D5AC2'),
                        ),
                        alignment: Alignment.bottomLeft,
                        margin: EdgeInsets.only(
                          top: 40,
                          left: 20,
                          // right: 15,
                          // bottom: 33,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Обране',
                              style: TextStyle(
                                  fontSize: 37,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                        height: 100,
                        child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Hexcolor('#7D5AC2'),
                              // color: Colors.green,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                            ),
                            child: Container(
                              margin: EdgeInsets.only(
                                  right: 20, left: 20, bottom: 10),
                              child: Stack(
                                  overflow: Overflow.visible,
                                  alignment: Alignment.centerLeft,
                                  children: <Widget>[
                                    TextField(
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Hexcolor('#1E2E45'),
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w400,
                                      ),
                                      textAlign: TextAlign.left,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      decoration: new InputDecoration(
                                        hintText: 'Пошук',
                                        hintStyle: TextStyle(
                                          color: Hexcolor('#D3D3D3'),
                                          fontSize: 16,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                        contentPadding: new EdgeInsets.only(
                                            left: 42, top: 14, bottom: 14),
                                        fillColor: Colors.white,
                                        filled: true,
                                        border: new OutlineInputBorder(
                                          borderRadius:
                                              new BorderRadius.circular(24.0),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(24.0),
                                          borderSide: BorderSide(
                                            color: Hexcolor('#FE6802'),
                                            width: 1,
                                          ),
                                        ),
                                      ),
                                      onChanged: (string) {
                                        string = string.toLowerCase();
                                        // _debouncer.run(() {
                                        setState(() {
                                          filteredMissions =
                                              missions.where((post) {
                                            var postTitle = utf8
                                                .decode(
                                                    post.titleUa.runes.toList())
                                                .toLowerCase();
                                            return postTitle.contains(string);
                                          }).toList();
                                        });
                                        // });
                                      },
                                    ),
                                    Positioned(
                                        left: 17,
                                        top: 17,
                                        child: Image.asset(
                                          'assets/fg_images/6_home_search_search.png',
                                          width: 15,
                                          height: 15,
                                          color: Hexcolor('#717A87'),
                                        )),
                                  ]),
                            ))),

                    // from DATABASE
                    loadingPosition
                        ? Center(
                            child: Container(
                              margin: EdgeInsets.only(top: 100),
                              child: CircularProgressIndicator(
                                backgroundColor: Hexcolor('#7D5AC2'),
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                  Hexcolor('#FE6802'),
                                ),
                              ),
                            ),
                          )
                        : Expanded(
                            child: filteredMissions.length == 0
                                ? Center(
                                    child: Text(
                                      'Результатів не знайдено!',
                                      style: TextStyle(
                                        color: Hexcolor('#747474'),
                                        fontSize: 14,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w400,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                : ListView.builder(
                                    physics: BouncingScrollPhysics(),
                                    itemCount: filteredMissions.length,
                                    itemBuilder: (context, index) {
                                      FavoriteMissions mission = filteredMissions[index];

                                      var missionName = utf8.decode(
                                          mission.titleUa.runes.toList());
                                      var missionImage = utf8.decode(
                                          mission.imageUrl.runes.toList());
                                      var missionTime =
                                          mission.duration.toString();
                                      var missionProgress = mission.progress;
                                      print(missionProgress);
                                      int missionId = mission.id;
                                      var missionLatitude = double.parse(
                                          mission.location.latitude);
                                      var missionLongitude = double.parse(
                                          mission.location.longitude);

                                      var distanceInMeters;
                                      distanceInMeters =
                                          Geolocator.distanceBetween(
                                              currentLocation.latitude,
                                              currentLocation.longitude,
                                              missionLatitude,
                                              missionLongitude);
                                      var distanceInKilometers =
                                          (distanceInMeters * 0.001)
                                              .toStringAsFixed(2);

                                      var missionAccessType =
                                          mission.accessType;
                                      var missionAccessGranted;

                                      if (missionAccessType == 'BYSTATUS') {
                                        List<String> missionStatuses =
                                            new List<String>();
                                        for (final status in mission.statuses) {
                                          if (status.active == true) {
                                            missionStatuses.add(utf8.decode(
                                                status.titleUa.runes.toList()));
                                          }
                                        }
                                        print(missionStatuses);
                                        if (missionStatuses
                                            .contains(userStatus)) {
                                          missionAccessGranted = true;
                                        } else {
                                          missionAccessGranted = false;
                                        }
                                      }

                                      mission.distanceInMeters = double.parse(
                                          (distanceInMeters)
                                              .toStringAsFixed(0));
                                      // print(mission.distanceInMeters.toInt());

                                      // missions.sort((a,b)=>a.titleUa.compareTo(b.titleUa)); // sort by String

                                      // Comparator<Mission> id = (a, b) => a.id.compareTo(b.id); // sort by id
                                      // missions.sort(id);

                                      // missions[index].isSelected = false;

                                      Future<void> share() async {
                                        await FlutterShare.share(
                                          title:
                                              'Завантажуйте новий додаток і спробуйте виконати місію \n\n' +
                                                  "'" +
                                                  missionName +
                                                  "'" +
                                                  '\n',
                                          // text: 'Example share text',
                                          linkUrl:
                                              'https://play.google.com/store/apps/details?id=com.freegen.flutterfreegenapp',
                                          // chooserTitle: 'Example Chooser Title'
                                        );
                                      }

                                      showWindowQuizDone() {
                                        showDialog(
                                            barrierDismissible: false,
                                            barrierColor: Hexcolor('#341F5E')
                                                .withOpacity(0.8),
                                            context: context,
                                            builder: (context) {
                                              return Dialog(
                                                insetPadding: EdgeInsets.only(
                                                  left: 20,
                                                  right: 20,
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                child: Stack(
                                                  overflow: Overflow.visible,
                                                  children: [
                                                    Container(
                                                      height: 250,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 20,
                                                            ),
                                                            child: Text(
                                                              'Завдання пройдене',
                                                              style: TextStyle(
                                                                color: Hexcolor(
                                                                    '#1E2E45'),
                                                                fontSize: 24,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                          ),
                                                          Text(
                                                            'Ви не можете повторно пройти це завдання',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#747474'),
                                                              fontSize: 16,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              height: 1.4,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              left: 20,
                                                              right: 20,
                                                              bottom: 20,
                                                            ),
                                                            height: 60,
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width -
                                                                80,
                                                            child: RaisedButton(
                                                              child: Text(
                                                                "Закрити",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        17,
                                                                    fontFamily:
                                                                        'Arial',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700,
                                                                    letterSpacing:
                                                                        1.05),
                                                              ),
                                                              color: Hexcolor(
                                                                  '#FE6802'),
                                                              shape: new RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      new BorderRadius
                                                                              .circular(
                                                                          14.0)),
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            });
                                      }

                                      var color;

                                      setColor() {
                                        if (missionAccessType == "FREE") {
                                          color = Hexcolor('#1E2E45')
                                              .withOpacity(0.5);
                                        }
                                        if (missionAccessType == "CLOSED" ||
                                            missionAccessType == "PROMO" ||
                                            missionAccessType == "PAID" ||
                                            (mission.accessType == "BYSTATUS" &&
                                                missionAccessGranted ==
                                                    false)) {
                                          color = Hexcolor('#FE6802')
                                              .withOpacity(0.5);
                                        }
                                        if (mission.accessType == "BYSTATUS" &&
                                            missionAccessGranted == true) {
                                          color = Hexcolor('#219653')
                                              .withOpacity(0.5);
                                        }
                                      }

                                      setColor();

                                      return Stack(
                                          overflow: Overflow.visible,
                                          children: [
                                            GestureDetector(
                                              onTap: () async {
                                                if (missionProgress ==
                                                    'FINISHED') {
                                                  showWindowQuizDone();
                                                } else {
                                                  final SharedPreferences
                                                      sharedPreferences =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  sharedPreferences.setInt(
                                                      'missionId', missionId);
                                                  Navigator.pushNamed(context,
                                                      '/6_mission_desc.dart');
                                                }
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    bottom: 23,
                                                    left: 14,
                                                    right: 14),
                                                height: 190,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10)),
                                                    image: DecorationImage(
                                                      image: NetworkImage(mission
                                                                      .imageUrl ==
                                                                  "" ||
                                                              mission.imageUrl ==
                                                                  null
                                                          ? '$link' +
                                                              'default.png'
                                                          : '$link' +
                                                              missionImage),
                                                      fit: BoxFit.cover,
                                                    )),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    Container(
                                                      height: 55,
                                                      decoration: BoxDecoration(
                                                        color: color,
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        10.0),
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        10.0)),
                                                      ),
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            top: 5,
                                                            bottom: 5,
                                                            left: 10,
                                                            right: 10),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              child: Flexible(
                                                                child: Text(
                                                                  missionName,
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        16,
                                                                    fontFamily:
                                                                        'Arial',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700,
                                                                    letterSpacing:
                                                                        1.09,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            missionName ==
                                                                    "Київ проти Корони"
                                                                ? Container()
                                                                : Container(
                                                                    width: 130,
                                                                    child:
                                                                        Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .center,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: [
                                                                                Text(
                                                                                  distanceInKilometers + " км",
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                    fontSize: 13,
                                                                                    fontFamily: 'Arial',
                                                                                    fontWeight: FontWeight.w700,
                                                                                    letterSpacing: 1.09,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  " від вас",
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                    fontSize: 13,
                                                                                    fontFamily: 'Arial',
                                                                                    fontWeight: FontWeight.w400,
                                                                                    letterSpacing: 1.09,
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                            Container(
                                                                              margin: EdgeInsets.only(left: 10, right: 1.5),
                                                                              child: Image.asset(
                                                                                "assets/fg_images/6_home_quests_pin.png",
                                                                                width: 10.5,
                                                                                height: 15,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              5,
                                                                        ),
                                                                        Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: [
                                                                                Text(
                                                                                  missionTime,
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                    fontSize: 13,
                                                                                    fontFamily: 'Arial',
                                                                                    fontWeight: FontWeight.w700,
                                                                                    letterSpacing: 1.09,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  " хвилин",
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                    fontSize: 13,
                                                                                    fontFamily: 'Arial',
                                                                                    fontWeight: FontWeight.w400,
                                                                                    letterSpacing: 1.09,
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                            Container(
                                                                              margin: EdgeInsets.only(left: 10),
                                                                              child: Image.asset(
                                                                                "assets/fg_images/6_home_logo_time.png",
                                                                                width: 13.33,
                                                                                height: 13.33,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        )
                                                                      ],
                                                                    ),
                                                                  )
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                                right: 28,
                                                top: 14,
                                                child: GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {},
                                                  child: Image.asset(
                                                    'assets/fg_images/9_favorites_icon.png',
                                                    height: 23,
                                                    width: 18,
                                                  ),
                                                )),
                                            Positioned(
                                                right: 12,
                                                top: (190 / 2) - (50 / 2),
                                                child: SizedBox(
                                                  height: 50,
                                                  width: 50,
                                                  child: IconButton(
                                                    icon: Image.asset(
                                                      'assets/fg_images/6_home_logo_share.png',
                                                      width: 20,
                                                      height: 20,
                                                    ),
                                                    onPressed: () {
                                                      share();
                                                    },
                                                  ),
                                                )),
                                            missionProgress == 'FINISHED'
                                                ? Positioned(
                                                    top: 20,
                                                    left: 20,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        if (missionName ==
                                                            "Муромець race (тест)") {
                                                          Navigator.pushNamed(
                                                              context,
                                                              '/mission_muromec_rating.dart');
                                                        }
                                                      },
                                                      child: Image.asset(
                                                        'assets/fg_images/6_home_logo_mission_completed2.png',
                                                        height: 38,
                                                        width: 150,
                                                      ),
                                                    ))
                                                : Container(),
                                            mission.accessType == "CLOSED" ||
                                                    mission.accessType ==
                                                        "PROMO" ||
                                                    mission.accessType ==
                                                        "PAID" ||
                                                    (mission.accessType ==
                                                            "BYSTATUS" &&
                                                        missionAccessGranted ==
                                                            false)
                                                ? Positioned(
                                                    top: 12,
                                                    left: 26,
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_logo_lock.png',
                                                      height: 38,
                                                      width: 38,
                                                    ))
                                                : Container(),
                                            mission.accessType == "BYSTATUS" &&
                                                    missionAccessGranted == true
                                                ? Positioned(
                                                    top: 12,
                                                    left: 26,
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_logo_unlock.png',
                                                      height: 38,
                                                      width: 38,
                                                    ))
                                                : Container(),
                                          ]);
                                    }),
                          ),
                  ],
                ),
              ),
            ),
    );
  }
}
