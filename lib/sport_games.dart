import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
// import 'package:flutter_free_gen_reality/run_map_quest.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;

class SportGames extends StatefulWidget {
  @override
  _SportGamesState createState() => _SportGamesState();
}

class _SportGamesState extends State<SportGames> {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    Future<void> share() async {
      await FlutterShare.share(
        title: 'Загружай новое приложение и попробуйте выполнить миссию \n' +
            arguments['name'],
        // text: 'Example share text',
        linkUrl: 'https://google.com/',
        // chooserTitle: 'Example Chooser Title'
      );
    }

    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 390,
                    // width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Начало миссии',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          // start progress line
                          margin: EdgeInsets.only(top: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Stack(
                                overflow: Overflow.visible,
                                children: [
                                  Image.asset(
                                      'assets/fg_images/next_step_icon_start.png',
                                      width: 12,
                                      height: 12),
                                  Positioned(
                                    bottom: 9,
                                    left: 4.5,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                            'assets/fg_images/next_step_icon_start_flag.png',
                                            width: 18,
                                            height: 21),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 10, bottom: 8),
                                          child: Text(
                                            'ул. Владимирская, 24а',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 12,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                height: 1.5,
                                width: 25,
                                decoration:
                                    BoxDecoration(color: Hexcolor('#1E2E45')),
                              ),
                              Image.asset(
                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                  width: 12,
                                  height: 12),
                              Container(
                                height: 1.5,
                                width: 25,
                                decoration:
                                    BoxDecoration(color: Hexcolor('#1E2E45')),
                              ),
                              Image.asset(
                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                  width: 12,
                                  height: 12),
                              Container(
                                height: 1.5,
                                width: 25,
                                decoration:
                                    BoxDecoration(color: Hexcolor('#1E2E45')),
                              ),
                              Image.asset(
                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                  width: 12,
                                  height: 12),
                              Container(
                                height: 1.5,
                                width: 25,
                                decoration:
                                    BoxDecoration(color: Hexcolor('#1E2E45')),
                              ),
                              Stack(
                                overflow: Overflow.visible,
                                children: [
                                  Image.asset(
                                      'assets/fg_images/next_step_icon_notdone_step.png',
                                      width: 12,
                                      height: 12),
                                  Positioned(
                                    top: 9,
                                    right: 4.5,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              right: 10, top: 8),
                                          child: Text(
                                            'ул. Богдана Хмельницкого, 13/2',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 12,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        Image.asset(
                                            'assets/fg_images/next_step_icon_finish_flag.png',
                                            width: 18,
                                            height: 21),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Поздравляем с началом миссии,\nвам осталось пройти ' +
                                arguments['objects'].toString() +
                                ' шага!',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Начать миссию",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.pushNamed(context, '/questInfo.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  Positioned(
                    right: 40,
                    top: 50,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '0',
                            style: TextStyle(
                              fontSize: 36,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 2),
                            child: Text(
                              ' ' + '/ ' + arguments['objects'].toString(),
                              style: TextStyle(
                                fontSize: 24,
                                color: Hexcolor('#1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 85,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          margin: EdgeInsets.only(top: 250), // change height
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0)),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 5,
                            ),
                            child: Text(
                              arguments['name'],
                              // 'Все шаги',
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              height: 260,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  // borderRadius: BorderRadius.only(
                                  //     bottomRight: Radius.circular(10.0),
                                  //     bottomLeft: Radius.circular(10.0)),
                                  image: DecorationImage(
                                image: AssetImage(
                                    // 'assets/fg_images/sportGames_pic_moto.jpg'),
                                    arguments['image']),
                                fit: BoxFit.cover,
                              )),
                              child: Stack(
                                fit: StackFit.expand,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: 10,
                                              top: 35,
                                            ),
                                            child: IconButton(
                                              icon: Image.asset(
                                                'assets/fg_images/6_home_search_back.png',
                                                width: 13.16,
                                                height: 25,
                                              ),
                                              onPressed: () {
                                                Navigator.pushNamed(context,
                                                    '/5_myBottomBar.dart');
                                              },
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 35),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    setState(() {
                                                      isFavorite = !isFavorite;
                                                    });
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      isFavorite
                                                          ? 'assets/fg_images/9_favorites_icon.png'
                                                          : 'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 19.25,
                                                      height: 27.5,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    Navigator.pushNamed(
                                                        context, '/7_map.dart');
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/fg_images/sportGames_icon_map.png',
                                                      width: 18.75,
                                                      height: 23.75,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    share();
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_logo_share.png',
                                                      width: 24.4,
                                                      height: 26.28,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                              left: 20,
                                              bottom: 15,
                                              right: 20,
                                            ),
                                            child:
                                                // Text('')
                                                Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Image.asset(
                                                  'assets/fg_images/6_home_logo_unlock.png',
                                                  width: 38,
                                                  height: 38,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      '0',
                                                      style: TextStyle(
                                                        fontSize: 36,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w700,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' ' +
                                                          '/ ' +
                                                          arguments['objects']
                                                              .toString(),
                                                      style: TextStyle(
                                                        fontSize: 24,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          // BackdropFilter(
                                          //   filter: ui.ImageFilter.blur(
                                          //     sigmaX: 8.0,
                                          //     sigmaY: 8.0,
                                          //   ),
                                          //   child:
                                          Container(
                                            padding: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                            ),
                                            height: 40,
                                            child: Container(),
                                          ),
                                          // ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      child: ClipRect(
                                        child: BackdropFilter(
                                          filter: ui.ImageFilter.blur(
                                            sigmaX: 3.0,
                                            sigmaY: 3.0,
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              right: 20,
                                              left: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0),
                                              ),
                                            ),
                                            alignment: Alignment.center,
                                            height: 40.0,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/6_home_logo_time.png',
                                                      width: 18,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      arguments['time']
                                                          .toString(),
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      arguments['time'] != 1
                                                          ? ' минут'
                                                          : ' минута',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 7.5,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 15,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      arguments['objects']
                                                          .toString(),
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      arguments['objects'] != 1
                                                          ? ' объекта'
                                                          : ' объект',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 7.5,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/6_home_icon_flag.png',
                                                      width: 11,
                                                      height: 18.5,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      arguments['distance']
                                                              .toString() +
                                                          ' км',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' до старта',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            // Container(
                            //   height: 260, // change height
                            //   width: MediaQuery.of(context).size.width,
                            //   decoration: BoxDecoration(
                            //       borderRadius: BorderRadius.only(
                            //           bottomRight: Radius.circular(10.0),
                            //           bottomLeft: Radius.circular(10.0)),
                            //       image: DecorationImage(
                            //         image: AssetImage(
                            //             // 'assets/fg_images/sportGames_pic_moto.jpg'),
                            //             'assets/fg_images/6_home_quest_6.png'),
                            //         fit: BoxFit.cover,
                            //       )),
                            //   child: Column(
                            //     mainAxisAlignment:
                            //         MainAxisAlignment.spaceBetween,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Row(
                            //         mainAxisAlignment:
                            //             MainAxisAlignment.spaceBetween,
                            //         children: [
                            //           Container(
                            //             margin: EdgeInsets.only(
                            //               left: 10,
                            //               top: 35,
                            //             ),
                            //             child: IconButton(
                            //               icon: Image.asset(
                            //                 'assets/fg_images/6_home_search_back.png',
                            //                 width: 13.16,
                            //                 height: 25,
                            //               ),
                            //               onPressed: () {
                            //                 Navigator.pushNamed(
                            //                     context, '/5_myBottomBar.dart');
                            //               },
                            //             ),
                            //           ),
                            //           Container(
                            //             margin: EdgeInsets.only(top: 35),
                            //             child: Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.end,
                            //               children: [
                            //                 GestureDetector(
                            //                   behavior: HitTestBehavior.opaque,
                            //                   onTap: () {
                            //                     setState(() {
                            //                       isFavorite = !isFavorite;
                            //                     });
                            //                   },
                            //                   child: Container(
                            //                     margin:
                            //                         EdgeInsets.only(right: 20),
                            //                     child: Image.asset(
                            //                       isFavorite
                            //                           ? 'assets/fg_images/9_favorites_icon.png'
                            //                           : 'assets/fg_images/sportGames_icon_favorite.png',
                            //                       width: 19.25,
                            //                       height: 27.5,
                            //                     ),
                            //                   ),
                            //                 ),
                            //                 GestureDetector(
                            //                   behavior: HitTestBehavior.opaque,
                            //                   onTap: () {
                            //                     Navigator.pushNamed(
                            //                         context, '/7_map.dart');
                            //                   },
                            //                   child: Container(
                            //                     margin:
                            //                         EdgeInsets.only(right: 20),
                            //                     child: Image.asset(
                            //                       'assets/fg_images/sportGames_icon_map.png',
                            //                       width: 18.75,
                            //                       height: 23.75,
                            //                     ),
                            //                   ),
                            //                 ),
                            //                 GestureDetector(
                            //                   behavior: HitTestBehavior.opaque,
                            //                   onTap: () {
                            //                     share();
                            //                   },
                            //                   child: Container(
                            //                     margin:
                            //                         EdgeInsets.only(right: 20),
                            //                     child: Image.asset(
                            //                       'assets/fg_images/6_home_logo_share.png',
                            //                       width: 24.4,
                            //                       height: 26.28,
                            //                     ),
                            //                   ),
                            //                 )
                            //               ],
                            //             ),
                            //           )
                            //         ],
                            //       ),
                            //       Column(
                            //         children: [
                            //           Container(
                            //             width:
                            //                 MediaQuery.of(context).size.width,
                            //             margin: EdgeInsets.only(
                            //               left: 20,
                            //               bottom: 20,
                            //               right: 20,
                            //             ),
                            //             child:
                            //                 // Text('')
                            //                 Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.spaceBetween,
                            //               crossAxisAlignment:
                            //                   CrossAxisAlignment.end,
                            //               children: [
                            //                 Image.asset(
                            //                   'assets/fg_images/6_home_logo_unlock.png',
                            //                   width: 38,
                            //                   height: 38,
                            //                 ),
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.end,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Text(
                            //                       '0',
                            //                       style: TextStyle(
                            //                         fontSize: 36,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w700,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' ' + '/ ' + '4',
                            //                       style: TextStyle(
                            //                         fontSize: 24,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     ),
                            //                   ],
                            //                 ),
                            //               ],
                            //             ),
                            //           ),
                            //           // BackdropFilter(
                            //           //   filter: ui.ImageFilter.blur(
                            //           //     sigmaX: 8.0,
                            //           //     sigmaY: 8.0,
                            //           //   ),
                            //           //   child:
                            //           Container(
                            //             padding: EdgeInsets.only(
                            //               left: 20,
                            //               right: 20,
                            //             ),
                            //             decoration: BoxDecoration(
                            //               borderRadius: BorderRadius.only(
                            //                 bottomRight: Radius.circular(10.0),
                            //                 bottomLeft: Radius.circular(10.0),
                            //               ),
                            //               // color: Colors.orange.withOpacity(0.2),
                            //               color: Colors.orange.withOpacity(0.2),
                            //             ),
                            //             height: 65,
                            //             child: Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.spaceBetween,
                            //               crossAxisAlignment:
                            //                   CrossAxisAlignment.center,
                            //               children: [
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.start,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/6_home_logo_time.png',
                            //                       width: 18,
                            //                       height: 18,
                            //                       color: Hexcolor('#FE6802'),
                            //                     ),
                            //                     SizedBox(
                            //                       width: 5,
                            //                     ),
                            //                     Text(
                            //                       '200',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w600,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' минут',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //                 SizedBox(
                            //                   width: 7.5,
                            //                 ),
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.start,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/sportGames_icon_favorite.png',
                            //                       width: 15,
                            //                       height: 18,
                            //                       color: Hexcolor('#FE6802'),
                            //                     ),
                            //                     SizedBox(
                            //                       width: 5,
                            //                     ),
                            //                     Text(
                            //                       '4',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w600,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' объекта',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //                 SizedBox(
                            //                   width: 7.5,
                            //                 ),
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.start,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/6_home_icon_flag.png',
                            //                       width: 11,
                            //                       height: 18.5,
                            //                       color: Hexcolor('#FE6802'),
                            //                     ),
                            //                     SizedBox(
                            //                       width: 5,
                            //                     ),
                            //                     Text(
                            //                       '10' + ' км',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w600,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' до старта',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //               ],
                            //             ),
                            //           ),
                            //           // ),
                            //         ],
                            //       ),
                            //     ],
                            //   ),
                            // ),
                            // Positioned(
                            //   top: 10,
                            //   bottom: 10,
                            //   right: 10,
                            //   left: 10,
                            //   child: BackdropFilter(
                            //     filter: ui.ImageFilter.blur(
                            //       sigmaX: 5,
                            //       sigmaY: 5,
                            //     ),
                            //     child: Container(
                            //       color: Colors.white.withOpacity(0),
                            //     ),
                            //   ),
                            // ),
                          ],
                        )
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                top: 20,
                              ),
                              child: Text(
                                'Описание',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                              ),
                              child: Text(
                                arguments['desc'],
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 14,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                ),
                              ),
                            ),
                            // Container(
                            //   height: 260,
                            //   width: MediaQuery.of(context).size.width,
                            //   decoration: BoxDecoration(
                            //       // borderRadius: BorderRadius.only(
                            //       //     bottomRight: Radius.circular(10.0),
                            //       //     bottomLeft: Radius.circular(10.0)),
                            //       image: DecorationImage(
                            //         image: AssetImage(
                            //             // 'assets/fg_images/sportGames_pic_moto.jpg'),
                            //             'assets/fg_images/6_home_quest_6.png'),
                            //         fit: BoxFit.cover,
                            //       )),
                            //   child: Stack(
                            //     fit: StackFit.expand,
                            //     children: <Widget>[
                            //       Column(
                            //         mainAxisAlignment:
                            //             MainAxisAlignment.spaceBetween,
                            //         crossAxisAlignment:
                            //             CrossAxisAlignment.center,
                            //         children: [
                            //           Row(
                            //             mainAxisAlignment:
                            //                 MainAxisAlignment.spaceBetween,
                            //             children: [
                            //               Container(
                            //                 margin: EdgeInsets.only(
                            //                   left: 10,
                            //                   top: 35,
                            //                 ),
                            //                 child: IconButton(
                            //                   icon: Image.asset(
                            //                     'assets/fg_images/6_home_search_back.png',
                            //                     width: 13.16,
                            //                     height: 25,
                            //                   ),
                            //                   onPressed: () {
                            //                     Navigator.pushNamed(context,
                            //                         '/5_myBottomBar.dart');
                            //                   },
                            //                 ),
                            //               ),
                            //               Container(
                            //                 margin: EdgeInsets.only(top: 35),
                            //                 child: Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.end,
                            //                   children: [
                            //                     GestureDetector(
                            //                       behavior:
                            //                           HitTestBehavior.opaque,
                            //                       onTap: () {
                            //                         setState(() {
                            //                           isFavorite = !isFavorite;
                            //                         });
                            //                       },
                            //                       child: Container(
                            //                         margin: EdgeInsets.only(
                            //                             right: 20),
                            //                         child: Image.asset(
                            //                           isFavorite
                            //                               ? 'assets/fg_images/9_favorites_icon.png'
                            //                               : 'assets/fg_images/sportGames_icon_favorite.png',
                            //                           width: 19.25,
                            //                           height: 27.5,
                            //                         ),
                            //                       ),
                            //                     ),
                            //                     GestureDetector(
                            //                       behavior:
                            //                           HitTestBehavior.opaque,
                            //                       onTap: () {
                            //                         Navigator.pushNamed(
                            //                             context, '/7_map.dart');
                            //                       },
                            //                       child: Container(
                            //                         margin: EdgeInsets.only(
                            //                             right: 20),
                            //                         child: Image.asset(
                            //                           'assets/fg_images/sportGames_icon_map.png',
                            //                           width: 18.75,
                            //                           height: 23.75,
                            //                         ),
                            //                       ),
                            //                     ),
                            //                     GestureDetector(
                            //                       behavior:
                            //                           HitTestBehavior.opaque,
                            //                       onTap: () {
                            //                         share();
                            //                       },
                            //                       child: Container(
                            //                         margin: EdgeInsets.only(
                            //                             right: 20),
                            //                         child: Image.asset(
                            //                           'assets/fg_images/6_home_logo_share.png',
                            //                           width: 24.4,
                            //                           height: 26.28,
                            //                         ),
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //               )
                            //             ],
                            //           ),
                            //           Column(
                            //             children: [
                            //               Container(
                            //                 width: MediaQuery.of(context)
                            //                     .size
                            //                     .width,
                            //                 margin: EdgeInsets.only(
                            //                   left: 20,
                            //                   bottom: 20,
                            //                   right: 20,
                            //                 ),
                            //                 child:
                            //                     // Text('')
                            //                     Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment
                            //                           .spaceBetween,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/6_home_logo_unlock.png',
                            //                       width: 38,
                            //                       height: 38,
                            //                     ),
                            //                     Row(
                            //                       mainAxisAlignment:
                            //                           MainAxisAlignment.end,
                            //                       crossAxisAlignment:
                            //                           CrossAxisAlignment.end,
                            //                       children: [
                            //                         Text(
                            //                           '0',
                            //                           style: TextStyle(
                            //                             fontSize: 36,
                            //                             color: Colors.white,
                            //                             fontFamily: 'Arial',
                            //                             fontWeight:
                            //                                 FontWeight.w700,
                            //                           ),
                            //                         ),
                            //                         Text(
                            //                           ' ' + '/ ' + '4',
                            //                           style: TextStyle(
                            //                             fontSize: 24,
                            //                             color: Colors.white,
                            //                             fontFamily: 'Arial',
                            //                             fontWeight:
                            //                                 FontWeight.w400,
                            //                           ),
                            //                         ),
                            //                       ],
                            //                     ),
                            //                   ],
                            //                 ),
                            //               ),
                            //               // BackdropFilter(
                            //               //   filter: ui.ImageFilter.blur(
                            //               //     sigmaX: 8.0,
                            //               //     sigmaY: 8.0,
                            //               //   ),
                            //               //   child:
                            //               Container(
                            //                 padding: EdgeInsets.only(
                            //                   left: 20,
                            //                   right: 20,
                            //                 ),
                            //                 height: 65,
                            //                 child: Container(),
                            //               ),
                            //               // ),
                            //             ],
                            //           ),
                            //         ],
                            //       ),
                            //       Align(
                            //         alignment: Alignment.bottomCenter,
                            //           child: Container(
                            //             child: ClipRect(
                            //               child: BackdropFilter(
                            //                 filter: ui.ImageFilter.blur(
                            //                   sigmaX: 3.0,
                            //                   sigmaY: 3.0,
                            //                 ),
                            //                 child: Container(
                            //                   decoration: BoxDecoration(
                            //                     borderRadius: BorderRadius.only(
                            //                       bottomRight:
                            //                           Radius.circular(10.0),
                            //                       bottomLeft: Radius.circular(10.0),
                            //                     ),
                            //                   ),
                            //                   alignment: Alignment.center,
                            //                   height: 65.0,
                            //                   child: Row(
                            //                     mainAxisAlignment:
                            //                         MainAxisAlignment.spaceBetween,
                            //                     crossAxisAlignment:
                            //                         CrossAxisAlignment.center,
                            //                     children: [
                            //                       Row(
                            //                         mainAxisAlignment:
                            //                             MainAxisAlignment.start,
                            //                         crossAxisAlignment:
                            //                             CrossAxisAlignment.end,
                            //                         children: [
                            //                           Image.asset(
                            //                             'assets/fg_images/6_home_logo_time.png',
                            //                             width: 18,
                            //                             height: 18,
                            //                             color: Hexcolor('#FE6802'),
                            //                           ),
                            //                           SizedBox(
                            //                             width: 5,
                            //                           ),
                            //                           Text(
                            //                             '200',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w600,
                            //                             ),
                            //                           ),
                            //                           Text(
                            //                             ' минут',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w400,
                            //                             ),
                            //                           )
                            //                         ],
                            //                       ),
                            //                       SizedBox(
                            //                         width: 7.5,
                            //                       ),
                            //                       Row(
                            //                         mainAxisAlignment:
                            //                             MainAxisAlignment.start,
                            //                         crossAxisAlignment:
                            //                             CrossAxisAlignment.end,
                            //                         children: [
                            //                           Image.asset(
                            //                             'assets/fg_images/sportGames_icon_favorite.png',
                            //                             width: 15,
                            //                             height: 18,
                            //                             color: Hexcolor('#FE6802'),
                            //                           ),
                            //                           SizedBox(
                            //                             width: 5,
                            //                           ),
                            //                           Text(
                            //                             '4',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w600,
                            //                             ),
                            //                           ),
                            //                           Text(
                            //                             ' объекта',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w400,
                            //                             ),
                            //                           )
                            //                         ],
                            //                       ),
                            //                       SizedBox(
                            //                         width: 7.5,
                            //                       ),
                            //                       Row(
                            //                         mainAxisAlignment:
                            //                             MainAxisAlignment.start,
                            //                         crossAxisAlignment:
                            //                             CrossAxisAlignment.end,
                            //                         children: [
                            //                           Image.asset(
                            //                             'assets/fg_images/6_home_icon_flag.png',
                            //                             width: 11,
                            //                             height: 18.5,
                            //                             color: Hexcolor('#FE6802'),
                            //                           ),
                            //                           SizedBox(
                            //                             width: 5,
                            //                           ),
                            //                           Text(
                            //                             '10' + ' км',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w600,
                            //                             ),
                            //                           ),
                            //                           Text(
                            //                             ' до старта',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w400,
                            //                             ),
                            //                           )
                            //                         ],
                            //                       ),
                            //                     ],
                            //                   ),
                            //                 ),
                            //               ),
                            //             ),
                            //           ),

                            //       ),
                            //     ],
                            //   ),
                            // )

                            // Container(
                            //   margin: EdgeInsets.only(top: 15),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Container(
                            //           margin: EdgeInsets.only(
                            //             right: 15,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/fg_images/sportGames_icon_pin.png',
                            //             width: 20,
                            //             height: 29,
                            //           )),
                            //       Flexible(
                            //           child: Text(
                            //         'Пересечение улиц Владимирская и Рейтарская, Киев',
                            //         style: TextStyle(
                            //           color: Hexcolor('#545454'),
                            //           fontSize: 16,
                            //           fontFamily: 'Arial',
                            //           // letterSpacing: 1.025,
                            //         ),
                            //       )),
                            //     ],
                            //   ),
                            // ),

                            // Container(
                            //   margin: EdgeInsets.only(top: 15, bottom: 15),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Container(
                            //           margin: EdgeInsets.only(
                            //             right: 15,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/fg_images/sportGames_icon_clock.png',
                            //             width: 25,
                            //             height: 25,
                            //           )),
                            //       Flexible(
                            //           child: Text(
                            //         '12.03.2021 ( 16:30 )',
                            //         style: TextStyle(
                            //           color: Hexcolor('#545454'),
                            //           fontSize: 16,
                            //           fontFamily: 'Arial',
                            //           // letterSpacing: 1.025,
                            //         ),
                            //       )),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            //             Container(
            //   padding: EdgeInsets.only(left: 20),
            //   margin: EdgeInsets.only(top: 398),
            //   alignment: Alignment.centerLeft,
            //   width: MediaQuery.of(context).size.width,
            //   height: 70,
            //   decoration: BoxDecoration(
            //     color: Hexcolor('#7D5AC2'),
            //     borderRadius: BorderRadius.only(
            //         bottomLeft: Radius.circular(10.0),
            //         bottomRight: Radius.circular(10.0)),
            //   ),
            //   child: Text(
            //     'Спортивные игры',
            //     style: TextStyle(
            //       fontSize: 25,
            //       color: Colors.white,
            //       fontFamily: 'AvenirNextLTPro-Regular',
            //       fontWeight: FontWeight.w900,
            //     ),
            //   ),
            // ),

            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Начать Миссию",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  // Navigator.pushNamed(context, '/questInfo.dart');
                  showInfoWindow();

                  // Navigator.pushNamed(context, '/run_map_quest.dart');
                  // Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RunMapQuest()));
                },
              ),
            ),

            ///
          ],
        ),
      ),
    );
    // return Scaffold(
    //   body: Container(
    //     decoration: BoxDecoration(
    //       color: Hexcolor('#F2F2F2'),
    //     ),
    //     child:
    //     Column(
    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //       children: <Widget>[
    //         SingleChildScrollView(
    //           child: Stack(
    //             overflow: Overflow.visible,
    //             children: [
    //               Container(
    //                 padding: EdgeInsets.only(left: 20),
    //                 margin: EdgeInsets.only(top: 398),
    //                 alignment: Alignment.centerLeft,
    //                 width: MediaQuery.of(context).size.width,
    //                 height: 70,
    //                 decoration: BoxDecoration(
    //                   color: Hexcolor('#7D5AC2'),
    //                   borderRadius: BorderRadius.only(
    //                       bottomLeft: Radius.circular(10.0),
    //                       bottomRight: Radius.circular(10.0)),
    //                 ),
    //                 child: Text(
    //                   'Спортивные игры',
    //                   style: TextStyle(
    //                     fontSize: 25,
    //                     color: Colors.white,
    //                     fontFamily: 'AvenirNextLTPro-Regular',
    //                     fontWeight: FontWeight.w900,
    //                   ),
    //                 ),
    //               ),
    //               // Container(
    //               //   width: MediaQuery.of(context).size.width - 40,
    //               //   margin: EdgeInsets.only(top: 474, left: 20, right: 20),
    //               //   child: SingleChildScrollView(
    //               //     physics: BouncingScrollPhysics(),
    //               //     child: Column(
    //               //       mainAxisAlignment: MainAxisAlignment.start,
    //               //       crossAxisAlignment: CrossAxisAlignment.start,
    //               //       children: [
    //               //         Container(
    //               //           // margin:
    //               //           //     EdgeInsets.only(bottom: 20, top: 20),
    //               //           child: Text(
    //               //             'Вопрос',
    //               //             style: TextStyle(
    //               //               color: Colors.black,
    //               //               fontSize: 18,
    //               //               fontFamily: 'AvenirNextLTPro-Regular',
    //               //               letterSpacing: 1.025,
    //               //             ),
    //               //           ),
    //               //         ),
    //               //         Text(
    //               //           'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat?Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat?',
    //               //           style: TextStyle(
    //               //             color: Hexcolor('#545454'),
    //               //             fontSize: 14,
    //               //             fontFamily: 'AvenirNextLTPro-Regular',
    //               //             letterSpacing: 1.025,
    //               //           ),
    //               //         ),
    //               //       ],
    //               //     ),
    //               //   ),
    //               // ),
    //               Positioned(
    //                   child: SingleChildScrollView(
    //                 child: Column(
    //                   children: [
    //                     Container(
    //                       height: 408,
    //                       width: MediaQuery.of(context).size.width,
    //                       decoration: BoxDecoration(
    //                           borderRadius: BorderRadius.only(
    //                               bottomRight: Radius.circular(10.0),
    //                               bottomLeft: Radius.circular(10.0)),
    //                           image: DecorationImage(
    //                             image: AssetImage(
    //                                 'assets/fg_images/sportGames_pic_moto.jpg'),
    //                             fit: BoxFit.cover,
    //                           )),
    //                     ),
    //                   ],
    //                 ),
    //               )),
    //             ],
    //           ),
    //         ),
    //         Container(
    //           margin: EdgeInsets.only(bottom: 25),
    //           height: 60,
    //           width: MediaQuery.of(context).size.width - 40,
    //           child: RaisedButton(
    //             child: Text(
    //               "Начать Квест",
    //               style: TextStyle(
    //                 color: Colors.white,
    //                 fontSize: 17,
    //               ),
    //             ),
    //             color: Hexcolor('#EB5C18'),
    //             shape: new RoundedRectangleBorder(
    //                 borderRadius: new BorderRadius.circular(14.0)),
    //             onPressed: () {
    //               Navigator.pushNamed(context, '/questQuiz.dart');
    //             },
    //           ),
    //         ),///
    //       ],
    //     ),
    //   ),
    // );
  }
}
