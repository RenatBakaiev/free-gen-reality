import 'dart:convert';

List<SubMissionProgresses> subMissionProgressesFromJson(String str) => List<SubMissionProgresses>.from(json.decode(str).map((x) => SubMissionProgresses.fromJson(x)));

String subMissionProgressesToJson(List<SubMissionProgresses> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SubMissionProgresses {
    SubMissionProgresses({
        this.id,
        this.trackStatus,
        this.submissionId,
    });

    int id;
    String trackStatus;
    int submissionId;

    factory SubMissionProgresses.fromJson(Map<String, dynamic> json) => SubMissionProgresses(
        id: json["id"],
        trackStatus: json["trackStatus"],
        submissionId: json["submissionId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "trackStatus": trackStatus,
        "submissionId": submissionId,
    };
}
  
