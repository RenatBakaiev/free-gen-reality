import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '6_home_filter_age_ages.dart';

class HomeFilterAge extends StatefulWidget {
  @override
  _HomeFilterAgeState createState() => _HomeFilterAgeState();
}

class _HomeFilterAgeState extends State<HomeFilterAge> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(
                  top: 50,
                  left: 15,
                  right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Возраст',
                      style: TextStyle(
                          fontSize: 37,
                          color: Colors.white,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w900),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 38,
                          height: 38,
                          child: IconButton(
                            icon: Image.asset(
                              'assets/fg_images/6_home_filter_close.png',
                              width: 20,
                              height: 20,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/6_home_filter.dart');
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              child: Expanded(
                  child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  height: MediaQuery.of(context).size.height - 226,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(
                    // top: 20,
                    // right: 25,
                    left: 25,
                  ),
                  child: Column(
                    children: [
                      Container(
                        // height: MediaQuery.of(context).size.height-241,
                        margin: EdgeInsets.only(top: 15),
                        child: Column(children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 20),
                              child: Text(
                                'Возраст',
                                style: TextStyle(
                                  color: Hexcolor('#919191'),
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 1.089,
                                ),
                              ),
                            ),
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height - 286,
                              // child: Expanded(
                              child: ListView.builder(
                                  physics: BouncingScrollPhysics(),
                                  itemCount: ages.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        setState(() {
                                          ages[index].isSelected =
                                              !ages[index].isSelected;
                                        });
                                      },
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                              ),
                                              child: Text(
                                                ages[index].age,
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 17,
                                                  fontFamily:'Arial',
                                                  fontWeight: FontWeight.w600,    
                                                  // letterSpacing: 1.089,
                                                ),
                                              ),
                                            ),
                                            ages[index].isSelected
                                                ? Container(
                                                    margin: EdgeInsets.only(
                                                      right: 20,
                                                    ),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_filter_location_selected.png',
                                                      width: 30,
                                                      height: 30,
                                                    ),
                                                  )
                                                : Text('')
                                          ],
                                        ),
                                      ),
                                    );
                                  })),
                          // ),
                        ]),
                      )
                    ],
                  ),
                ),
              )),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 50,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Готово",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Arial',
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_home_filter.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
