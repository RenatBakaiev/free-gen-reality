import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';
// import 'package:flutter_free_gen_reality/6_home_quests.dart';
import '6_mission_step_selfie_preview.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:model_viewer/model_viewer.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/rendering.dart';

import '6_home_steps_json.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '10_profile_json.dart';

class MissionStepSelfie extends StatefulWidget {
  @override
  _MissionStepSelfieState createState() => _MissionStepSelfieState();
}

class _MissionStepSelfieState extends State {
  bool isApiCallProcess = false;
  CameraController controller;
  List cameras;
  int selectedCameraIndex;
  String imgPath;
  bool isListHidden = true;
  bool isImageHidden = false;

  Future<User> changeTus(
      double value, String operation, String typeCurrency) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": typeCurrency,
        "operation": operation,
        "amount": value,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');

    if (response.statusCode == 200) {
      print('quiz change done!!!');
    } else {
      throw Exception('Failed to update User.');
    }
  }

  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: this.context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  width: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      stepPoints != 0 && stepPoints != null
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Вам нараховано ',
                                  style: TextStyle(
                                    color: Hexcolor('#747474'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  stepPoints.toInt().toString() +
                                      ' ' +
                                      stepCurrency.toString(),
                                  style: TextStyle(
                                    color: Hexcolor('#58B12D'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            )
                          : Container(),
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            ),
                        child: Text(
                          'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на наступний крок",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, '/6_mission_step_main.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<Stup> setStepProgress() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    var usedId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");

    var stepDone = sharedPreferences.get('step$stepId' + 'Done' + '$usedId');

    String url =
        'http://generation-admin.ehub.com.ua/api/step/finish?user_id=' +
            '$usedId' +
            '&step_id=' +
            '$stepId';
    print(url);
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    print(response.statusCode);
    if (response.statusCode == 200) {
      print('Selfie done');
      setState(() {
        isLoading = false;
      });
      if (stepPoints != null) {
        if (stepDone == '' || stepDone == null) {
          changeTus(stepPoints, "DEPOSIT", stepCurrency);
          sharedPreferences.setBool('step$stepId' + 'Done' + '$usedId', true);
          print('deposit done');
        } else {
          print('you cant to reseive many times');
        }
      }
      showInfoWindow();
      // Navigator.pushNamed(context, '/6_mission_step_main.dart');
    } else {
      print('error to done info');
      setState(() {
        isLoading = false;
      });
    }
    return Stup();
  }

  Future<Stup> finishStep() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    var usedId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");

    String url =
        'http://generation-admin.ehub.com.ua/api/step/checkAnswer?user_id=' +
            '$usedId' +
            '&step_id=' +
            '$stepId';
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      print(responseJson);
      if (response.statusCode == 200) {
        print('Page done');
        setState(() {
          isLoading = false;
        });
        setStepProgress();
      } else {
        print('error');
        setState(() {
          isLoading = false;
        });
      }
    }

    getMission();
    return Stup();
  }

  bool isLoading = false;
  double stepPoints;
  var stepCurrency;
  List stepModelList;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';
  Future<Stup> getStepData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    String token = sharedPreferences.get("token");
    String url = 'http://generation-admin.ehub.com.ua/api/step/' + '$stepId';
    String urlCurrencies =
        'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies';
    print(url);
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });

      final responseCurrencies = await http.get(urlCurrencies, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(responseCurrencies.statusCode);
      Map<String, dynamic> responseJsonCurrencies =
          jsonDecode(responseCurrencies.body);

      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          stepPoints = responseJson["points"];
          stepModelList = responseJson["modelList"];
          print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
          print(stepModelList);
          print(stepModelList.length);

          for (int i = 0;
              i < responseJsonCurrencies["publication"].length;
              i++) {
            if (responseJson["currencyType"] ==
                responseJsonCurrencies["publication"][i]["id"].toString()) {
              stepCurrency =
                  responseJsonCurrencies["publication"][i]["titleUa"];
              print(stepCurrency);
            }
          }
          isLoading = false;
        });
      } else {
        print('error');
      }
    }

    getMission();
    return Stup();
  }

  @override
  void initState() {
    super.initState();

    getStepData();
    Vibration.vibrate(duration: 1000);

    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        _initCameraController(cameras[selectedCameraIndex]).then((void v) {});
      } else {
        print('No camera available');
      }
    }).catchError((err) {
      print('Error :${err.code}Error message : ${err.message}');
    });
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.max);
    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }
      if (controller.value.hasError) {
        print('Camera error ${controller.value.errorDescription}');
      }
    });
    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  child: SafeArea(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: _cameraPreviewWidget(),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 20,
                  left: 20,
                  child: Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        "Далі",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        finishStep();
                      },
                    ),
                  ),
                ),
                Positioned(
                  bottom: 80,
                  child: Container(
                    height: 90,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(20),
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isListHidden = !isListHidden;
                            });
                          },
                          child: SizedBox(
                            width: 34,
                            child: Image.asset(
                              'assets/fg_images/8_camera_icon_ar.png',
                              height: 34,
                              width: 34,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 40,
                        ),
                        _cameraControlWidget(context),
                        SizedBox(
                          width: 34,
                        ),
                        _cameraToggleRowWidget(),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 170,
                  child: isListHidden
                      ? stepModelList.length != 0
                          ? Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                  physics: BouncingScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: stepModelList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    var stepModelObject = stepModelList[index];
                                    var stepModel = link +
                                        utf8.decode(stepModelObject["fileName"]
                                            .runes
                                            .toList());

                                    return Container(
                                      margin: EdgeInsets.only(
                                        right: 20,
                                      ),
                                      height: 300,
                                      width: 100,
                                      child: ModelViewer(
                                          backgroundColor:
                                              Colors.white.withOpacity(0),
                                          src: stepModel,
                                          ar: true,
                                          autoRotate: false,
                                          cameraControls: true,
                                          autoPlay: true),
                                    );
                                  }),
                            )
                          : Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width,
                              child: Center(
                                  child: Text(
                                'Немає 3d об\'єктів',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              )))
                      : Container(),
                ),
                Positioned(
                  top: 65,
                  left: 15,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/fg_images/sportGames_icon_arrow_back.png',
                      width: 10,
                      height: 19,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
    );
  }

  /// Display Camera preview.
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return Center(
        child: const Text(
          'Loading',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.w900,
          ),
        ),
      );
    }

    return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: CameraPreview(controller),
    );
  }

  /// Display the control bar with buttons to take pictures
  Widget _cameraControlWidget(context) {
    return FloatingActionButton(
      child: Image.asset(
        'assets/fg_images/8_camera_icon_make_pic.png',
        width: 90,
        height: 90,
      ),
      backgroundColor: Colors.transparent,
      onPressed: () {
        _onCapturePressed(context);
      },
    );
  }

  File imageFile;
  final picker = ImagePicker();

  Widget _cameraToggleRowWidget() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }

    return GestureDetector(
      onTap: _onSwitchCamera,
      child: Container(
        // margin: EdgeInsets.only(left: 20),
        child: Image.asset(
          'assets/fg_images/8_camera_icon_toggle.png',
          width: 40,
          height: 35,
        ),
      ),
    );
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error:${e.code}\nError message : ${e.description}';
    print(errorText);
  }

  void _onCapturePressed(context) async {
    try {
      final path =
          join((await getTemporaryDirectory()).path, '${DateTime.now()}.png');
      await controller.takePicture(path);

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MissionStepSelfiePreview(
                  imgPath: path,
                )),
      );
    } catch (e) {
      _showCameraException(e);
    }
  }

  void _onSwitchCamera() {
    selectedCameraIndex =
        selectedCameraIndex < cameras.length - 1 ? selectedCameraIndex + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    _initCameraController(selectedCamera);
  }
}
