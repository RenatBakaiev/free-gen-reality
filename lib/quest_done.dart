// import 'dart:async';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';
import '10_profile_json.dart';
// import '10_profile_rewards.dart';
// import '6_home_quests.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

// import 'dart:io';

class QuestDone extends StatefulWidget {
  @override
  _QuestDoneState createState() => _QuestDoneState();
}

class _QuestDoneState extends State<QuestDone> {
  AudioCache cache; // you have this
  AudioPlayer player; // create this

  void _playFile() async {
    player = await AudioCache()
        .play("music/quest_done_coins.mp3"); // assign player here
  }

  

  // void _launchUrl(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not open Url';
  //   }
  // }

// Future<Size> _calculateImageDimension() {
//   Completer<Size> completer = Completer();
//   Image image = Image.asset("assets/fg_images/quest_done_pic.png");
//   image.image.resolve(ImageConfiguration()).addListener(
//     ImageStreamListener(
//       (ImageInfo image, bool synchronousCall) {
//         var myImage = image.image;
//         Size size = Size(myImage.width.toDouble(), myImage.height.toDouble());
//         completer.complete(size);
//       },
//     ),
//   );
//   return completer.future;
// }

  checkMission12NewYearTreesCompleted() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool mission12NewYearTreesCompleted = sharedPreferences.get("Mission12NewYearTreesCompleted");
    
    if (mission12NewYearTreesCompleted == false) {
      changeTus(100.0, "DEPOSIT");
    }
    else {
      print('This mission is complited!');
    }
  }

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "deposit 100 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      sharedPreferences.setBool('Mission12NewYearTreesCompleted', true);
      print(response.body);
      print('+100TUS done');

    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  @override
  void initState() {
    super.initState();
    // changeTus(100.0, "DEPOSIT");
    checkMission12NewYearTreesCompleted();
    _playFile();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Container(
            // margin: EdgeInsets.only(left: 25, right: 25, bottom: 25),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // RaisedButton(onPressed: () { _calculateImageDimension().then((size) => print("size = ${size}")); },

                // ),
                Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                    ),
                    child: Text(
                      'Вітаємо\nмісія виконана',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.w900,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Stack(
                          overflow: Overflow.visible,
                          children: [
                            Image.asset(
                              'assets/fg_images/quest_done_lightning2.png',
                            ),
                            Positioned(
                              right: 70,
                              top: 80,
                              child: RotationTransition(
                                turns: AlwaysStoppedAnimation(-21 / 360),
                                child: Text(
                                  "100",
                                  style: TextStyle(
                                    fontSize: 40,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              right: 20,
                              top: 110,
                              child: RotationTransition(
                                turns: AlwaysStoppedAnimation(-21 / 360),
                                child: Text(
                                  "TUS",
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                                bottom: -130,
                                left:
                                    MediaQuery.of(context).size.width / 2 - 125,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/fg_images/quest_done_pic.png',
                                      height: 250,
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ]),

                Column(
                  children: [
                    // Container(
                    //   margin: EdgeInsets.only(
                    //     left: 20,
                    //     right: 20,
                    //     bottom: 20,
                    //   ),
                    //   child: Text(
                    //     'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                    //     style: TextStyle(
                    //       fontFamily: 'Arial',
                    //       fontSize: 16,
                    //       fontWeight: FontWeight.w400,
                    //       color: Hexcolor('#FFFFFF'),
                    //     ),
                    //     textAlign: TextAlign.center,
                    //   ),
                    // ),
                    // Container(
                    //   height: 60,
                    //   margin: EdgeInsets.only(
                    //     left: 20,
                    //     right: 20,
                    //   ),
                    //   width: MediaQuery.of(context).size.width,
                    //   child: RaisedButton(
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.center,
                    //       crossAxisAlignment: CrossAxisAlignment.center,
                    //       children: [
                    //         Image.asset(
                    //           'assets/fg_images/4_enter_facebook.png',
                    //           width: 14,
                    //           height: 25,
                    //         ),
                    //         SizedBox(
                    //           width: 10,
                    //         ),
                    //         Text(
                    //           "Розповісти в Facebook",
                    //           style: TextStyle(
                    //             color: Colors.white,
                    //             fontSize: 16,
                    //           ),
                    //         ),
                    //         SizedBox(
                    //           width: 10,
                    //         ),
                    //         Text(
                    //           "+2 tus",
                    //           style: TextStyle(
                    //             color: Colors.white,
                    //             fontSize: 20,
                    //           ),
                    //         ),
                    //       ],
                    //     ),
                    //     color: Hexcolor('#3B5998'),
                    //     shape: new RoundedRectangleBorder(
                    //         borderRadius: new BorderRadius.circular(14.0)),
                    //     onPressed: () {
                    //       _launchUrl('https://www.facebook.com');
                    //       setState(() {
                    //         allRewards[0].quantity = allRewards[0].quantity + 2;
                    //       });
                    //     },
                    //   ),
                    // ),
                    Container(
                      margin: EdgeInsets.only(
                        bottom: 20,
                        right: 20,
                        left: 20,
                        top: 15,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 2 - 25,
                            child: RaisedButton(
                              child: Text(
                                "Головна",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                // setState(() {
                                //   questsDifferent[1].isCompleted = true;
                                // });
                                // changeTus(100.0, "DEPOSIT");
                                Navigator.pushNamed(
                                    context, '/5_myBottomBar.dart');
                              },
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 2 - 25,
                            child: RaisedButton(
                              child: Text(
                                "Місце в рейтингу",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, '/10_profile_rating.dart');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
            color: Hexcolor('#7D5AC2'),
          ),
        ),
      ),
    );
  }
}
