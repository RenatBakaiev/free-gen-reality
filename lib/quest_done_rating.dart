import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '10_profile_rating_users.dart';

class QuestDoneRating extends StatefulWidget {
  @override
  _QuestDoneRatingState createState() => _QuestDoneRatingState();
}

class _QuestDoneRatingState extends State<QuestDoneRating> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(
                  top: 50,
                  // left: 15,
                  // right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: SizedBox(
                        // height: 55,
                        // width: 40,
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/6_home_search_back.png',
                            width: 10,
                            height: 19,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                    Text(
                      'Рейтинг',
                      style: TextStyle(
                          fontSize: 37,
                          color: Colors.white,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w900),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: users.length,
                itemBuilder: (context, index) {
                  return Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    margin: EdgeInsets.only(bottom: 23, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            CircleAvatar(
                              radius: 30,
                              backgroundImage: AssetImage(users[index].image),
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 14),
                                child: Text(
                                  users[index].name,
                                  style: TextStyle(
                                    color: Hexcolor('#1E2E45'),
                                    fontSize: 16,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ],
                        ),
                        Container(
                          width: 55,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/fg_images/10_profile_rating_star.png',
                                height: 13.23,
                                width: 12.64,
                              ),
                              Text(
                                users[index].rating,
                                style: TextStyle(
                                  color: Hexcolor('#1E2E45'),
                                  fontSize: 16,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Главная",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Arial',
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  Navigator.pushNamed(context, '/5_myBottomBar.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
