import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

class ProfileAr extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ProfileAr> {
  ArCoreController arCoreController;

  void _onArCoreViewCreated(ArCoreController _arcoreController) {
    arCoreController = _arcoreController;
    _addSphere(arCoreController);
  }

  

    _addSphere(ArCoreController controller) async {
    final ByteData textureBytes = await rootBundle.load('assets/newyork360.png');

    final material = ArCoreMaterial(color: Color.fromARGB(120, 66, 134, 244), textureBytes: textureBytes.buffer.asUint8List(),); // ArCoreMaterial(color: Colors.deepPurple);
    // final material = ArCoreMaterial(color: Colors.deepPurple); // ArCoreMaterial(color: Colors.deepPurple);
    final sphere = ArCoreSphere(
      materials: [material],
      radius: 1,
    );
    final node = ArCoreNode(
      shape: sphere,
      position: vector.Vector3.zero(),
    );
    controller.addArCoreNode(node);
  }

  @override
  void dispose() {
    arCoreController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          child: ArCoreView(
            onArCoreViewCreated: _onArCoreViewCreated,
            enableTapRecognizer: true,
          ),
        ),
      ),
    );
  }
}
