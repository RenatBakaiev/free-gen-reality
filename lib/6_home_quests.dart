import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/6_home_quest_model.dart';
import 'package:hexcolor/hexcolor.dart';

// final int id;
// final String name;
// final String image;
// final Color color;

final Quest quest1 = Quest(
  id: 1,
  name: 'Спорт',
  image: 'assets/fg_images/6_home_quest_1.png',
);
final Quest quest2 = Quest(
  id: 2,
  name: 'Исторические',
  image: 'assets/fg_images/6_home_quest_2.png',
);
final Quest quest3 = Quest(
  id: 3,
  name: 'Культурные',
  image: 'assets/fg_images/6_home_quest_3.png',
);
final Quest quest4 = Quest(
  id: 4,
  name: 'Корпоративные',
  image: 'assets/fg_images/6_home_quest_4.png',
);

final Quest quest5 = Quest(
  id: 5,
  name: 'Недоступная миссия (нехватка валюты)',
  description: 'В этой миссии пробегите определенный маршрут за нужное время.',
  image: 'assets/fg_images/6_home_quest_5.png',
  distance: 5,
  color: Hexcolor('#EB5C18'),
  linkDesc: '/error_mission_cash.dart',
  linkMission: '/7_map.dart',
  isSelected: true,
  isLocked: true,
  isLockedCash: true,
  isLockedStatus: false,
  isLockedPromo: false,
  isFree: false,
  latitude: 50.448594,
  longitude: 30.505646,
  // isHidden: true,
  isCompleted: false,
  time: 10,
  objects: 1,
);

final Quest quest52 = Quest(
  id: 5,
  name: 'Недоступная миссия (нехватка статуса)',
  description:
      'В этой миссии нужно проехать на велосипеде определенный маршрут за нужное время.',
  image: 'assets/fg_images/6_home_quest_12.jpg',
  distance: 10,
  color: Hexcolor('#3B5998'),
  linkDesc: '/error_mission_status.dart',
  linkMission: '/7_map.dart',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: true,
  isLockedPromo: false,
  isFree: false,
  latitude: 50.449144,
  longitude: 30.505900,
  // isHidden: true,
  isCompleted: false,
  time: 10,
  objects: 1,
);

final Quest quest53 = Quest(
  id: 5,
  name: 'Недоступная миссия (промокод)',
  description:
      'В этой миссии нужно найти штрих-код.',
  image: 'assets/fg_images/6_home_quest_14.jpg',
  distance: 10,
  color: Hexcolor('#3B5998'),
  linkDesc: '/error_mission_promocode.dart',
  linkMission: '/7_map.dart',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.449346, 
  longitude: 30.509825, // менять
  // isHidden: true,
  isCompleted: false,
  time: 10,
  objects: 1,
);

final Quest quest6 = Quest(
  id: 6,
  name: 'Все шаги',
  description:
      'В этой миссии пройдите все шаги. Можно протестировать как работают все страницы миссий приложения. В остальных миссиях реализованы каждый отдельный шаг.',
  image: 'assets/fg_images/6_home_quest_6.png',
  distance: 9,
  color: Colors.orange,
  linkDesc: '/sport_games.dart',
  linkMission: '/sport_games.dart',
  isSelected: true,
  isLocked: false,
  isFree: false,
  latitude: 50.451421,
  longitude: 30.515380,
  // isHidden: true,
  isCompleted: false,
  time: 210,
  objects: 4,
);
final Quest quest7 = Quest(
  id: 7,
  name: 'Викторина',
  description: 'Ответьте на вопрос из Викторины.',
  image: 'assets/fg_images/6_home_quest_7.png',
  distance: 15,
  color: Colors.pink,
  linkDesc: '/desc_quiz.dart',
  linkMission: '/mission_quiz.dart',
  isSelected: true,
  isLocked: false,
  isFree: true,
  latitude: 50.450458,
  longitude: 30.510726,
  // isHidden: true,
  isCompleted: false,
  time: 5,
  objects: 1,
);

final Quest quest8 = Quest(
  id: 8,
  name: 'Метки на карте',
  description: 'Пройдите весь путь по карте.',
  image: 'assets/fg_images/6_home_quest_8.png',
  distance: 10,
  color: Colors.purpleAccent,
  linkDesc: '/desc_pinsOnMap.dart',
  linkMission: '/mission_pinsOnMap.dart',
  isSelected: true,
  isLocked: false,
  isFree: false,
  latitude: 50.448742,
  longitude: 30.513941,
  isCompleted: false,
  time: 100,
  objects: 2,
);

final Quest quest9 = Quest(
  id: 9,
  name: 'Київ проти Корони',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_6_home_quest_9.png',
  distance: 10,
  color: Colors.tealAccent,
  // link: '',
  isSelected: true,
  isLocked: false,
  isFree: true,
  isCompleted: false,
  time: 99999,
);

final Quest quest10 = Quest(
  id: 10,
  name: 'Описание',
  description: 'Прочитайте описание миссии.',
  image: 'assets/fg_images/6_home_quest_10.jpg',
  distance: 10,
  color: Colors.cyan,
  linkDesc: '/desc_description.dart',
  linkMission: '/mission_description.dart',
  isSelected: true,
  isLocked: false,
  isFree: false,
  latitude: 50.454741,
  longitude: 30.506251,
  isCompleted: false,
  time: 10,
  objects: 1,
);

final Quest quest11 = Quest(
  id: 11,
  name: 'Сканирование артефакта',
  description: 'Отсканируйте qr-код для получения дополнительной информации.',
  image: 'assets/fg_images/6_home_quest_11.jpg',
  distance: 12,
  color: Colors.indigo,
  linkDesc: '/desc_scanQR.dart',
  linkMission: '/mission_scanQR.dart',
  isSelected: true,
  isLocked: false,
  isFree: false,
  latitude: 50.453497,
  longitude: 30.516392,
  isCompleted: true,
  time: 1,
  objects: 1,
);

final Quest quest12 = Quest(
  id: 12,
  name: 'Прослушивание музыки',
  description: 'Прослушайте музыку и оставьте отзыв',
  image: 'assets/fg_images/6_home_quest_13.jpg',
  distance: 5,
  color: Colors.indigo,
  linkDesc: '/desc_music.dart',
  linkMission: '/mission_music.dart',
  isSelected: true,
  isLocked: false,
  isFree: false,
  latitude: 50.449945,
  longitude: 30.511056,
  isCompleted: false,
  time: 3,
  objects: 1,
);

final Quest quest13 = Quest(
  id: 13,
  name: 'Ялинковий квест',
  // description: 'Ялинка від ялинки різниться, як і райони в яких вони затишно вмостились. Хоча може це стереотип і його час зруйнувати?\n\nРозібратися у цьому непростому питанні, сидячи на дивані, не вийде 😜\n\nОдягайтеся тепліше, беріть друзів і приготуйтесь руйнувати стереотипи, шукати артефакти й танцювати з персонажами біля новорічних ялинок Києва!',
  description: 'Ялинка від ялинки різниться, як і райони в яких вони затишно вмостились. Хоча може це стереотип і його час зруйнувати?',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_z12_missions_desc2.jpg',
  distance: 5,
  color: Colors.indigo,
  linkDesc: '/missions_12_desc.dart',
  linkMission: '/missions_12_12_trees.dart',
  isSelected: true,
  isLocked: false,
  isFree: true,
  latitude: 50.453346, // Софиевская площадь
  longitude: 30.515856, //  
  isCompleted: false,
  time: 180,
  objects: 12,
);

final Quest quest133 = Quest(
  id: 133,
  name: 'Муромець race',
  description: 'Чули про 10 000 кроків? 👀\nУ цій місії їх буде більше, бо на вас чекає траса на 8-11 км по мальовничим місцям парку.',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_Muromec_main.jpeg',
  distance: 5,
  color: Colors.indigo,
  linkDesc: '/mission_muromec_desc.dart',
  linkMission: '/mission_muromec_map.dart',
  isSelected: true,
  isLocked: false,
  isFree: true,
  latitude: 50.496760, 
  longitude: 30.542460,  
  isCompleted: false,
  time: 180,
  objects: 14,
);



final Quest quest14 = Quest(
  id: 14,
  name: 'Пейзажний Київ',
  description: 'Хай! Ти вже перевірив байк та прогноз погоди, узяв водичку та павербенк і готовий вирушити? Чудово!\nПрогулянка мальовничим містом із невеличкими пригодами чекає на тебе!',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_quest_14.jpg',
  distance: 2,
  color: Hexcolor('#3B5998'),
  linkDesc: '',
  linkMission: '',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.455341, 
  longitude: 30.520690, 
  // isHidden: true,
  isCompleted: false,
  time: 120,
  objects: 1,
);

final Quest quest15 = Quest(
  id: 15,
  name: 'Zzzzombie race',
  description: 'Шукаєш мотивацію для ранкових пробіжок і який саундтрек включити?\nБери воду, навушники, смартфон, змахни пил і тінь сумніву - та стартуй в забігу "проти зомбі, за мізки!"',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_quest_15.jpg',
  distance: 5,
  color: Hexcolor('#3B5998'),
  linkDesc: '',
  linkMission: '',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.424457, 
  longitude: 30.611301, 
  // isHidden: true,
  isCompleted: false,
  time: 40,
  objects: 1,
);

final Quest quest16 = Quest(
  id: 16,
  name: 'Труханів - острів скарбів',
  description: 'Взяти лопату, карту і ліхтар, дійти до хрестика і копати поки не почуєш "дзень"?\nКількість призів: залежить від тебе 😏',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_quest_16.jpg',
  distance: 3,
  color: Hexcolor('#3B5998'),
  linkDesc: '',
  linkMission: '',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.455105, 
  longitude: 30.532545, 
  // isHidden: true,
  isCompleted: false,
  time: 108,
  objects: 1,
);

final Quest quest17 = Quest(
  id: 17,
  name: 'Київська Русь',
  description: 'В тебе 5 точок та 5 століть ⏱\nЗаряджай телефон, бери провіант та вирушай на точку старту ✊',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_quest_17.jpg',
  distance: 4,
  color: Hexcolor('#3B5998'),
  linkDesc: '',
  linkMission: '',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.443935, 
  longitude: 30.551324, 
  // isHidden: true,
  isCompleted: false,
  time: 55,
  objects: 1,
);

final Quest quest18 = Quest(
  id: 18,
  name: 'Пробіжка з фантомом',
  description: 'Хочеш пробіжку в компанії, але всі зайняті?\n👉 Вмикай Фантома.\nТисни👇',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_quest_18.jpg',
  distance: 1,
  color: Hexcolor('#3B5998'),
  linkDesc: '',
  linkMission: '',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.476046, // менять
  longitude: 30.428375, // менять
  // isHidden: true,
  isCompleted: false,
  time: 35,
  objects: 1,
);

final Quest quest19 = Quest(
  id: 19,
  name: 'Відьми Подолу',
  description: 'Зараз Поділ - це місце зустрічей в нішевих кафе, суботніх веломарафонів, прогулянок туристів і місцевих айтішників, тут театр для дітей та юнацтва, поруч Сковорода з Самсоном - чарівно, чи не так?',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_quest_19.jpg',
  distance: 1,
  color: Hexcolor('#3B5998'),
  linkDesc: '',
  linkMission: '',
  isSelected: true,
  isLocked: true,
  isLockedCash: false,
  isLockedStatus: false,
  isLockedPromo: true,
  isFree: false,
  latitude: 50.460703, // менять
  longitude: 30.514517, // менять
  // isHidden: true,
  isCompleted: false,
  time: 120,
  objects: 1,
);

List<Quest> questsMain = [
  quest1,
  quest2,
  quest3,
  quest4,
];

List<Quest> questsDifferent = [
  quest9,
  quest13,
  quest133,
  quest14,
  quest15,
  quest16,
  quest17,
  quest18,
  quest19,
  // quest5,
  // quest52,
  // quest53,
  // quest6,
  // quest7,
  // quest8,
  // quest10,
  // quest11,
  // quest12,
];

List<Quest> questsForMap = [
  quest9,
  quest13,
  quest133,
  quest14,
  quest15,
  quest16,
  quest17,
  quest18,
  quest19,
  // quest5,
  // quest52,
  // quest53,
  // quest6,
  // quest7,
  // quest8,
  // quest10,
  // quest11,
  // quest12,
];

List<Quest> questsForDisplay = questsDifferent;

List<Quest> questsFavorites = [];

final PicForCamera pic1 = PicForCamera(
  name: 'Lion',
  image:
      'assets/fg_images/0_splash_screen_lion_and_penguin.gif', // for emulator
  // image: 'https://modelviewer.dev/shared-assets/models/Astronaut.glb', // for device
);

final PicForCamera pic2 = PicForCamera(
  name: 'Pinguin',
  image: 'assets/fg_images/8_camera_pic_pinguin.png', // for emulator
  // image: 'https://modelviewer.dev/shared-assets/models/Astronaut.glb', // for device
);

final PicForCamera pic3 = PicForCamera(
  name: 'LionAndPinguin',
  image: 'assets/fg_images/error_mission_icon_unlock.png', // for emulator
  // image: 'https://modelviewer.dev/shared-assets/models/Astronaut.glb', // for device
);

List<PicForCamera> picsForCamera = [
  pic1,
  pic2,
  pic3,
  pic1,
  pic2,
  pic3,
];

// final ArForCamera ar1 = ArForCamera(
//   name: 'Shishkebab',
//   image: 'https://modelviewer.dev/shared-assets/models/shishkebab.glb',
// );

// final ArForCamera ar2 = ArForCamera(
//   name: 'Astronaut',
//   image: 'https://modelviewer.dev/shared-assets/models/Astronaut.glb',
// );

// final ArForCamera ar3 = ArForCamera(
//   name: 'Reflective-sphere',
//   image: 'https://modelviewer.dev/shared-assets/models/reflective-sphere.gltf',
// );

// final ArForCamera ar4 = ArForCamera(
//   name: 'DamagedHelmet',
//   image:
//       'https://modelviewer.dev/shared-assets/models/glTF-Sample-Models/2.0/DamagedHelmet/glTF/DamagedHelmet.gltf',
// );

// final ArForCamera ar5 = ArForCamera(
//   name: 'CylinderEngine',
//   image:
//       'https://modelviewer.dev/shared-assets/models/glTF-Sample-Models/2.0/2CylinderEngine/glTF-Draco/2CylinderEngine.gltf',
// );

// final ArForCamera ar6 = ArForCamera(
//   name: 'RobotExpressive',
//   image: 'https://modelviewer.dev/shared-assets/models/RobotExpressive.glb',
// );

// final ArForCamera ar7 = ArForCamera(
//   name: 'Horse',
//   image: 'https://modelviewer.dev/shared-assets/models/Horse.glb',
// );

// final ArForCamera ar8 = ArForCamera(
//   name: 'Pengy',
//   image: 'assets/3d/pengy.gltf',
// );

// final ArForCamera ar9 = ArForCamera(
//   name: 'Lion',
//   image: 'assets/3d/lion.gltf',
// );

// final ArForCamera ar10 = ArForCamera(
//   name: 'Dragon',
//   image: 'assets/3d/dragon.glb',
// );

// final ArForCamera ar8 = ArForCamera(
//   name: 'Horse',
//   image: 'assets/fg_images/6_home_quest_13.jpg',
// );

final ArForCamera arCamera1 = ArForCamera(
  name: 'arCamera1',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_LionStaticoptimized.glb',
  // image: 'https://modelviewer.dev/shared-assets/models/Astronaut.glb',
);

final ArForCamera arCamera2 = ArForCamera(
  name: 'arCamera2',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_PenguinStaticoptimized.glb', // assets/3d/PenguinStaticoptimized.glb
);

final ArForCamera arCamera3 = ArForCamera(
  name: 'arCamera3',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_PenguinDancezstepsantahatoptimized.glb',
);

final ArForCamera arCamera4 = ArForCamera(
  name: 'arCamera4',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Liondancedolphinopt.glb',
);

final ArForCamera arCamera5 = ArForCamera(
  name: 'arCamera5',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-05_LionDancecrisscrossoptiV5origintocenter.glb',
);

final ArForCamera arCamera6 = ArForCamera(
  name: 'arCamera6',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-05_LionDancecrisscrossoptiv6movedmodel.glb',
);

final ArForCamera arCamera7 = ArForCamera(
  name: 'arCamera7',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-05_LionDancecrisscrossoptiv7Scalednode0.glb',
);

final ArForCamera arCameraNew1 = ArForCamera(
  name: 'new1',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_1FixLionDance Zstep.glb',
);

final ArForCamera arCameraNew2 = ArForCamera(
  name: 'new2',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_2FixLionSkibidance.glb',
);

final ArForCamera arCameraNew3 = ArForCamera(
  name: 'new3',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_3FixPenguinDanceCrisscross.glb',
);

final ArForCamera arCameraNew4 = ArForCamera(
  name: 'new4',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_4FixPenguindancedolphin.glb',
);

final ArForCamera arCameraNew5 = ArForCamera(
  name: 'new5',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_5FixPenguinDanceRun.glb',
);

final ArForCamera arCameraNew6 = ArForCamera(
  name: 'new6',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_6FixLionCrisCrossFix.glb',
);

final ArForCamera arCameraNew7 = ArForCamera(
  name: 'new7',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_7FixLiondancealcatraz.glb',
);

final ArForCamera arCameraNew8 = ArForCamera(
  name: 'new8',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_8FixLionDanceCocos.glb',
);

final ArForCamera arCameraNew9 = ArForCamera(
  name: 'new9',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-11_9FixLionDanceRainbow.glb',
);

final ArForCamera ar1 = ArForCamera(
  name: 'ar1',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_LionSkibidanceOptimized.glb',
);

final ArForCamera ar2 = ArForCamera(
  name: 'ar2',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Liondancerainbowopt.glb',
);

final ArForCamera ar3 = ArForCamera(
  name: 'ar3',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_LionDancecrisscrossopti.glb',
);

final ArForCamera ar4 = ArForCamera(
  name: 'ar4',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Penguindancedolphinoptimized.glb',
);

final ArForCamera ar5 = ArForCamera(
  name: 'ar5',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_LionSkibidanceOptimized.glb',
);

final ArForCamera ar6 = ArForCamera(
  name: 'ar6',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Liondancezstepopti.glb',
);

final ArForCamera ar7 = ArForCamera(
  name: 'ar7',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_PenguinDanceRunoptimized.glb',
);

final ArForCamera ar8 = ArForCamera(
  name: 'ar8',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Liondancerainbowopt.glb',
);

final ArForCamera ar9 = ArForCamera(
  name: 'ar9',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Penguindancedolphinoptimized.glb',
);

final ArForCamera ar10 = ArForCamera(
  name: 'ar10',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_PenguinDanceCrisscrossoptimized.glb',
);

final ArForCamera ar11 = ArForCamera(
  name: 'ar11',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_Liondancecocosopt.glb',
);

final ArForCamera ar12 = ArForCamera(
  name: 'ar12',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_LiondanceAlcatrazopti.glb',
);
final ArForCamera ar13 = ArForCamera(
  name: 'ar13',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_horse.glb',
);

List<ArForCamera> arForCamera = [
  arCameraNew1,
  arCameraNew2,
  arCameraNew3,
  arCameraNew4,
  arCameraNew5,
  arCameraNew6,
  arCameraNew7,
  arCameraNew8,
  arCameraNew9
  // arCamera1,
  // arCamera2,
  // arCamera3,
  // arCamera4,
  // arCamera5,
  // arCamera6,
  // arCamera7,  
];

List<ArForCamera> ar_1 = [
  ar1
];

List<ArForCamera> ar_2 = [
  ar2
];

List<ArForCamera> ar_3 = [
  ar3
];

List<ArForCamera> ar_4 = [
  ar4
];

List<ArForCamera> ar_5 = [
  ar5
];

List<ArForCamera> ar_6 = [
  ar6
];

List<ArForCamera> ar_7 = [
  ar7
];

List<ArForCamera> ar_8 = [
  ar8
];

List<ArForCamera> ar_9 = [
  ar9
];

List<ArForCamera> ar_10 = [
  ar10
];

List<ArForCamera> ar_11 = [
  ar11
];

List<ArForCamera> ar_12 = [
  ar12,
];
