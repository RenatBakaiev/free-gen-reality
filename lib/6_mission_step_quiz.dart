import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vibration/vibration.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as http;
import '10_profile_json.dart';
import 'dart:convert';
import '6_home_steps_json.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class MissionStepQuiz extends StatefulWidget {
  @override
  _MissionStepQuizState createState() => _MissionStepQuizState();
}

class _MissionStepQuizState extends State<MissionStepQuiz> {
  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

  bool isLoading = false;
  var userEmail = '';

  AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();

  bool isPlaying = false;
  bool isApiCallProcess = false;

  void _getAudio() async {
    var url = "http://generation-admin.ehub.com.ua/api/file/downloadFile/" +
        "$stepSound";
    var res = await audioPlayer.play(url, isLocal: true);
    if (res == 1) {
      setState(() {
        isPlaying = true;
      });
    }
  }

  void _stopFile() {
    audioPlayer?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer.pause();
    } else {
      audioPlayer.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  void _getVideo() {
    _controller = VideoPlayerController.network(
        'http://generation-admin.ehub.com.ua/api/file/downloadFile/$stepVideo');
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    _controller.play();
  }

  Future<User> changeTus(
      double value, String operation, String typeCurrency) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": typeCurrency,
        "operation": operation,
        "amount": value,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');

    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('quiz change done!!!');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  String stepName = '';
  String stepDesc = '';
  String stepImage = '';
  String stepCorrectAnswer = '';
  var pointsRightAnswer;
  var pointsWrongAnswer;
  var rightAnswerCurrency;
  var wrongAnswerCurrency;
  String stepSound = '';
  String stepVideo = '';

  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  Future<Stup> getStepData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    String token = sharedPreferences.get("token");

    String url = 'http://generation-admin.ehub.com.ua/api/step/' + '$stepId';
    String urlCurrencies =
        'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies';

    getData() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);

      final responseCurrencies = await http.get(urlCurrencies, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(responseCurrencies.statusCode);
      Map<String, dynamic> responseJsonCurrencies =
          jsonDecode(responseCurrencies.body);
      print(responseJsonCurrencies["publication"].length);

      if (response.statusCode == 200) {
        setState(() {
          stepName = utf8.decode(responseJson["titleUa"].runes.toList());
          stepDesc = utf8.decode(responseJson["descriptionUa"].runes.toList());
          stepImage = utf8.decode(responseJson["imageUrl"].runes.toList());
          stepCorrectAnswer = responseJson["correctAnswer"];
          pointsRightAnswer = responseJson["points"];
          pointsWrongAnswer = responseJson["pointsWrongAnswer"];

          print(
              '################################## CURRENCIES TYPE ########################################');
          print(pointsRightAnswer);
          print(pointsWrongAnswer);

          stepVideo = responseJson["videoUrl"];
          if (stepVideo != "" || stepVideo != null) {
            _getVideo();
          }

          stepSound = responseJson["audioUrl"];
          if ((stepSound != "" || stepSound != null) &&
              (stepVideo == "" || stepVideo == null)) {
            _getAudio();
          }

          for (int i = 0;
              i < responseJsonCurrencies["publication"].length;
              i++) {
            if (responseJson["currencyType"] ==
                responseJsonCurrencies["publication"][i]["id"].toString()) {
              rightAnswerCurrency =
                  responseJsonCurrencies["publication"][i]["titleUa"];
              print(rightAnswerCurrency);
            }
            if (responseJson["currencyTypeWrongAnswer"] ==
                responseJsonCurrencies["publication"][i]["id"].toString()) {
              wrongAnswerCurrency =
                  responseJsonCurrencies["publication"][i]["titleUa"];
              print(wrongAnswerCurrency);
            }
          }
          isLoading = false;
        });
      } else {
        print('error');
      }
    }

    getData();
    return Stup();
  }

  int submissionSteps;
  int submissionDoneSteps;
  bool loadingSteps = true;

  Future<Stup> checkAllStepsDone() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var usedId = sharedPreferences.get('id');
    var subMissionProgressId = sharedPreferences.get('subMissionProgressId');
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/step/progress/list?user_id=$usedId&subMissionProgress_id=$subMissionProgressId';

    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    List<dynamic> responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      List<String> finishedSteps = new List<String>();
      for (int i = 0; i < responseJson.length; i++) {
        if (responseJson[i]["trackStatus"] == 'FINISHED') {
          finishedSteps.add('FINISHED');
        }
      }
      setState(() {
        submissionSteps = responseJson.length;
        submissionDoneSteps = finishedSteps.length;
        loadingSteps = false;
      });
    } else {
      print('error to get steps');
      setState(() {
        loadingSteps = false;
      });
    }
    return Stup();
  }

  @override
  void initState() {
    checkAllStepsDone();
    getStepData();
    Vibration.vibrate(duration: 1000);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool isPressedA = false;
  bool isPressedB = false;
  bool isPressedC = false;
  bool isPressedD = false;

  showInfoWindowWrongAnswer(answer) {
    Timer(
        Duration(milliseconds: 500),
        () => showDialog(
            barrierDismissible: false,
            barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
            context: context,
            builder: (context) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  height: 580,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Відповідь неправильна',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_wrong_answer.png',
                        height: 350,
                      ),
                      Text('Невдача',
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Hexcolor('#FF1E1E'),
                          ),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Вы вибрали неправильний варіант відповіді.',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            pointsWrongAnswer != 0 && pointsWrongAnswer != null
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'С вашого рахунку знято ',
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                          color: Hexcolor('#747474'),
                                        ),
                                      ),
                                      Text(
                                        pointsWrongAnswer.toInt().toString() +
                                            ' ' +
                                            wrongAnswerCurrency.toString(),
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                          fontSize: 16,
                                          color: Hexcolor('#FE6802'),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Спробувати ще раз",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () async {
                            if (_controller.value.isPlaying) {
                              _controller.pause();
                            }
                            _stopFile();
                            changeTus(pointsWrongAnswer, "WITHDRAW",
                                wrongAnswerCurrency);
                            Navigator.pushNamed(
                                context, '/6_mission_step_quiz.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  width: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      // pointsRightAnswer != 0 && pointsRightAnswer != null
                      //     ? Row(
                      //         mainAxisAlignment: MainAxisAlignment.center,
                      //         crossAxisAlignment: CrossAxisAlignment.end,
                      //         children: [
                      //           Text(
                      //             'Вам нараховано ',
                      //             style: TextStyle(
                      //               color: Hexcolor('#747474'),
                      //               fontSize: 20,
                      //               fontFamily: 'Arial',
                      //               fontWeight: FontWeight.w400,
                      //               letterSpacing: 1.089,
                      //             ),
                      //             textAlign: TextAlign.center,
                      //           ),
                      //           Text(
                      //             pointsRightAnswer.toInt().toString() +
                      //                 ' ' +
                      //                 rightAnswerCurrency.toString(),
                      //             style: TextStyle(
                      //               color: Hexcolor('#58B12D'),
                      //               fontSize: 20,
                      //               fontFamily: 'Arial',
                      //               fontWeight: FontWeight.w900,
                      //               letterSpacing: 1.089,
                      //             ),
                      //             textAlign: TextAlign.center,
                      //           ),
                      //         ],
                      //       )
                      //     : Container(),
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            ),
                        child: Text(
                          'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на наступний крок",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, '/6_mission_step_main.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<Stup> setStepProgress() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    var usedId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/step/finish?user_id=' +
            '$usedId' +
            '&step_id=' +
            '$stepId';
    print(url);
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      print('quiz done');
      setState(() {
        isLoading = false;
      });
      showInfoWindow();
      // Navigator.pushNamed(context, '/6_mission_step_main.dart');
    } else {
      print('error to done info');
      setState(() {
        isLoading = false;
      });
    }
    return Stup();
  }

  showInfoWindowRightAnswer(answer) {
    Timer(
        Duration(milliseconds: 500),
        () => showDialog(
            barrierDismissible: false,
            barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
            context: context,
            builder: (context) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  // width: MediaQuery.of(context).size.width - 80,
                  height: 580,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Відповідь правильна',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_right_answer.png',
                        height: 350,
                      ),
                      Text('Вітаємо!',
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Hexcolor('#59B32D'),
                          ),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Вы вибрали правильний варіант відповіді.',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            pointsRightAnswer != 0 && pointsRightAnswer != null
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Вам нараховано ',
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                          color: Hexcolor('#747474'),
                                        ),
                                      ),
                                      Text(
                                        pointsRightAnswer.toInt().toString() +
                                            ' ' +
                                            rightAnswerCurrency,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                          fontSize: 16,
                                          color: Hexcolor('#FE6802'),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Продовжити",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () async {
                            final SharedPreferences sharedPreferences =
                                await SharedPreferences.getInstance();
                            var stepId = sharedPreferences.get('stepId');
                            var usedId = sharedPreferences.get('id');

                            var stepDone = sharedPreferences
                                .get('step$stepId' + 'Done' + '$usedId');

                            if (_controller.value.isPlaying) {
                              _controller.pause();
                            }
                            _stopFile();
                            if (pointsRightAnswer != null) {
                              if (stepDone == '' || stepDone == null) {
                                changeTus(pointsRightAnswer, "DEPOSIT",
                                    rightAnswerCurrency);
                                sharedPreferences.setBool(
                                    'step$stepId' + 'Done' + '$usedId', true);
                                print('deposit done');
                              } else {
                                print('you cant to reseive many times');
                              }
                            }

                            setStepProgress();
                            // Navigator.pushNamed(
                            //     context, '/6_mission_step_main.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  showWindowQuizDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдене',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви не можете повторно пройти це завдання',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            if (_controller.value.isPlaying) {
                              _controller.pause();
                            }
                            // Navigator.pushNamed(
                            //     context, '/missions_12_scan_info1.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Stack(
                    overflow: Overflow.visible,
                    children: [
                      Positioned(
                        child: Container(
                          height: MediaQuery.of(context).size.height
                          //  - 85
                          ,
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                stepVideo == "" || stepVideo == null
                                    ? Container(
                                        margin: EdgeInsets.only(top: 90),
                                        height: 225,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0)),
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                  stepImage == "" ||
                                                          stepImage == null
                                                      ? '$link' + 'default.png'
                                                      : '$link' + stepImage),
                                              fit: BoxFit.cover,
                                            )),
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            bottom: 20,
                                            right: 20,
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(),
                                                  stepSound == "" ||
                                                          stepSound == null
                                                      ? Container()
                                                      : GestureDetector(
                                                          behavior:
                                                              HitTestBehavior
                                                                  .opaque,
                                                          onTap: () {
                                                            pausePlay();
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 30,
                                                            ),
                                                            child: Image.asset(
                                                              !isPlaying
                                                                  ? 'assets/fg_images/audio_play.png'
                                                                  : 'assets/fg_images/audio_pause.png',
                                                              height: 50,
                                                              width: 50,
                                                            ),
                                                          ),
                                                        ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(),
                                                  loadingSteps
                                                      ? CircularProgressIndicator(
                                                          backgroundColor:
                                                              Hexcolor(
                                                                  '#7D5AC2'),
                                                          valueColor:
                                                              new AlwaysStoppedAnimation<
                                                                  Color>(
                                                            Hexcolor('#FE6802'),
                                                          ),
                                                        )
                                                      : Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Text(
                                                              submissionDoneSteps
                                                                  .toString(),
                                                              style: TextStyle(
                                                                fontSize: 36,
                                                                color: Colors
                                                                    .white,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                            Text(
                                                              ' ' +
                                                                  '/ ' +
                                                                  submissionSteps
                                                                      .toString(),
                                                              style: TextStyle(
                                                                fontSize: 24,
                                                                color: Colors
                                                                    .white,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    : Container(
                                        margin: EdgeInsets.only(top: 90),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomRight:
                                                  Radius.circular(10.0),
                                              bottomLeft:
                                                  Radius.circular(10.0)),
                                        ),
                                        child: Stack(
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                if (_controller
                                                    .value.isPlaying) {
                                                  _controller.pause();
                                                } else {
                                                  _controller.play();
                                                }
                                              },
                                              child: FutureBuilder(
                                                future:
                                                    _initializeVideoPlayerFuture,
                                                builder: (context, snapshot) {
                                                  if (snapshot
                                                          .connectionState ==
                                                      ConnectionState.done) {
                                                    return AspectRatio(
                                                      aspectRatio: _controller
                                                          .value.aspectRatio,
                                                      child: VideoPlayer(
                                                          _controller),
                                                    );
                                                  } else {
                                                    return Center(
                                                      child:
                                                          CircularProgressIndicator(),
                                                    );
                                                  }
                                                },
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    40,
                                                margin: EdgeInsets.only(
                                                  left: 20,
                                                  right: 20,
                                                  bottom: 15,
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Container(),
                                                    loadingSteps
                                                        ? CircularProgressIndicator(
                                                            backgroundColor:
                                                                Hexcolor(
                                                                    '#7D5AC2'),
                                                            valueColor:
                                                                new AlwaysStoppedAnimation<
                                                                    Color>(
                                                              Hexcolor(
                                                                  '#FE6802'),
                                                            ),
                                                          )
                                                        : Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                submissionDoneSteps
                                                                    .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 36,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                              Text(
                                                                ' ' +
                                                                    '/ ' +
                                                                    submissionSteps
                                                                        .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 24,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                Container(
                                  width: MediaQuery.of(context).size.width - 40,
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  child: SingleChildScrollView(
                                    physics: BouncingScrollPhysics(),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              bottom: 20, top: 20),
                                          child: Text(
                                            'Питання',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontFamily: 'Arial',
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                        ),
                                        Html(
                                          data:
                                              stepDesc == null ? '' : stepDesc,
                                          onLinkTap: (i) {
                                            _launchUrl(i);
                                          },
                                        )
                                        // Text(
                                        //   stepDesc,
                                        //   style: TextStyle(
                                        //     color: Hexcolor('#545454'),
                                        //     fontSize: 16,
                                        //     fontFamily: 'Arial',
                                        //     letterSpacing: 1.025,
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 15,
                                    right: 15,
                                    top: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 1",
                                            style: TextStyle(
                                              color: isPressedA
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedA
                                              ? Colors.red
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            if (stepCorrectAnswer ==
                                                1.toString()) {
                                              showInfoWindowRightAnswer(1);
                                            } else {
                                              showInfoWindowWrongAnswer(1);
                                            }
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 2",
                                            style: TextStyle(
                                              color: isPressedB
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedB
                                              ? Hexcolor('#75C433')
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            if (stepCorrectAnswer ==
                                                2.toString()) {
                                              showInfoWindowRightAnswer(2);
                                            } else {
                                              showInfoWindowWrongAnswer(2);
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 15,
                                    right: 15,
                                    top: 10,
                                    bottom: 10,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 3",
                                            style: TextStyle(
                                              color: isPressedC
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedC
                                              ? Colors.red
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            if (stepCorrectAnswer ==
                                                3.toString()) {
                                              showInfoWindowRightAnswer(3);
                                            } else {
                                              showInfoWindowWrongAnswer(3);
                                            }
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 4",
                                            style: TextStyle(
                                              color: isPressedD
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedD
                                              ? Colors.red
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            if (stepCorrectAnswer ==
                                                4.toString()) {
                                              showInfoWindowRightAnswer(4);
                                            } else {
                                              showInfoWindowWrongAnswer(4);
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Hexcolor('#7D5AC2'),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                          ),
                          alignment: Alignment.bottomLeft,
                          margin: EdgeInsets.only(
                            top: 40,
                            bottom: 12,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: SizedBox(
                                      child: IconButton(
                                        icon: Image.asset(
                                          'assets/fg_images/6_home_search_back.png',
                                          width: 10,
                                          height: 19,
                                        ),
                                        onPressed: () {
                                          if (_controller.value.isPlaying) {
                                            _controller.pause();
                                          }
                                          _stopFile();
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ),
                                  ),
                                  Text(
                                    stepName,
                                    style: TextStyle(
                                        fontSize: 25,
                                        color: Colors.white,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                child: SizedBox(
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/run_map_quest_pic_gostart.png',
                                      width: 22,
                                      height: 24,
                                    ),
                                    onPressed: () {
                                      if (_controller.value.isPlaying) {
                                        _controller.pause();
                                      }
                                      _stopFile();
                                      Navigator.pushNamed(
                                        context,
                                        '/6_mission_desc.dart',
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}
