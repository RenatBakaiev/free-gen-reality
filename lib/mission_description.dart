import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class MissionDescription extends StatefulWidget {
  @override
  _MissionDescriptionState createState() => _MissionDescriptionState();
}

class _MissionDescriptionState extends State<MissionDescription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 85,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 340,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/fg_images/6_home_quest_10.jpg'),
                                  fit: BoxFit.cover,
                                )),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Описание',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Львовская площадь (укр. Львівська площа) — площадь в Шевченковском районе города Киева, местность Кудрявец. Расположена между улицами Большая Житомирская, Сретенская , Рейтарская, Ярославов Вал, Бульварно-Кудрявская и Сечевых Стрельцов. Ранее, в 1040-х годах, на месте расположения площади находились Западные ворота в каменной ограде Киева. С 1037 года здесь располагались Львовские ворота (первоначально Жидовские), открывавшие путь из Киева на Львов. Эта территория в то время представляла собой сложный узел фортификационных сооружений Западных и Жидовских ворот. Тем не менее, ни Западной, ни Жидовской площадь не называлась. В 1869 году площадь получила название Львовская — по названию Львовских ворот Старокиевской крепости, стоявших здесь в XVII-XVIII вв.. В 1835 году ворота получили название Житомирских. В середине XIX века ворота снесли и образовали площадь. Одновременно с официальным названием площадь имела и другое название — Сенная, поскольку здесь размещался Сенной базар (до 1958). С 1959 года носит современное название. Современный архитектурный ансамбль площади обозначен двумя доминантами, построенными в советское время — массивным Домом торговли и Домом художника, расположенными напротив друг друга. Дом торговли сооружался с 1968 по 1981 гг. Дом художника, образующий западную сторону площади, был построен в 1978 году. Фасад его второго этажа украшают 7 женских фигур — аллегории искусств. На Львовской площади была расположена Сретенская церковь — деревянная (XI век — середина XIX века), каменная (возведена по проекту архиектора В.Николаева в 1861 году, разрушена в 30-е годы XX века). Существуют планы по восстановлению церкви.',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                // Navigator.pushNamed(
                                //     context, '/desc_description.dart');
                                Navigator.of(context).pop();
                                // Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Описание',
                          style: TextStyle(
                              fontSize: 33,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15, bottom: 25),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width - 30,
                        child: RaisedButton(
                          child: Text(
                            "Далее",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/quest_done.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
