class Location {
  final String place;
  final String region;

  Location({
    this.place,
    this.region,
  });
}

class Region {
  final String region;
  final String city;
  bool isSelected;

  Region({
    this.region,
    this.city,
    this.isSelected,
  });
}

class District {
  final String district;
  final String districtPlace;
  bool isSelected;

  District({
    this.district,
    this.districtPlace,
    this.isSelected
  });
}
