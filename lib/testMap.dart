// // import 'dart:async';
// // import 'package:flutter/material.dart';
// // import 'package:flutter_free_gen_reality/geolocator_service.dart';
// // import 'package:geolocator/geolocator.dart';
// // import 'package:google_maps_flutter/google_maps_flutter.dart';
// // import 'package:provider/provider.dart';
// // import 'package:flutter_polyline_points/flutter_polyline_points.dart';

// // class MyAppRoutes extends StatelessWidget {
// //   final geoService = GeolocatorService();
// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       body: Stack(children: <Widget>[
// //         FutureProvider(
// //           create: (context) => geoService.getInitialLocation(),
// //           child: MaterialApp(
// //             debugShowCheckedModeBanner: false,
// //             title: 'Flutter Demo',
// //             theme: ThemeData(
// //               primarySwatch: Colors.blue,
// //             ),
// //             home: Consumer<Position>(
// //               builder: (context, position, widget) {
// //                 return (position != null)
// //                     ? GMap(position)
// //                     : Center(child: CircularProgressIndicator());
// //               },
// //             ),
// //           ),
// //         ),
// //         Positioned.fill(
// //           bottom: 10,
// //           child: Align(
// //             alignment: Alignment.bottomCenter,
// //             child: SizedBox(
// //               height: 30,
// //               width: 150,
// //               child: RaisedButton(
// //                   shape: new RoundedRectangleBorder(
// //                       borderRadius: new BorderRadius.circular(5.0)),
// //                   color: Colors.orange[900],
// //                   child: Text(
// //                     'Оновити маршрут',
// //                     style: TextStyle(
// //                         fontSize: 13,
// //                         color: Colors.white,
// //                         fontFamily: 'Roboto',
// //                         fontWeight: FontWeight.w900),
// //                   ),
// //                   onPressed: () {
// //                     Navigator.pushNamed(context, '/testMap.dart');
// //                   }),
// //             ),
// //           ),
// //         ),
// //         _buildContainer(),
// //       ]),
// //     );
// //   }
// // }

// // Widget _buildContainer() {
// //   return Align(
// //     alignment: Alignment.topLeft,
// //     child: Container(
// //       margin: EdgeInsets.only(left: 15, top: 15, right: 45),
// //       height: 150.0,
// //       child: ListView.builder(
// //           physics: BouncingScrollPhysics(),
// //           scrollDirection: Axis.horizontal,
// //           itemCount: 10,
// //           itemBuilder: (context, index) {
// //             return GestureDetector(
// //               onTap: () {
// //                 // Navigator.pushNamed(context, list[index]["quest"]);
// //               },
// //               child: ClipRRect(
// //                 // borderRadius: BorderRadius.all(Radius.circular(10)),
// //                 child: Container(
// //                     margin: EdgeInsets.only(right: 15),
// //                     width: 200,
// //                     decoration: BoxDecoration(
// //                       borderRadius: BorderRadius.all(Radius.circular(10)),
// //                     ),
// //                     // margin: const EdgeInsets.only(right: 15.0),
// //                     child: Column(
// //                       mainAxisAlignment: MainAxisAlignment.end,
// //                       children: <Widget>[
// //                         Container(
// //                           alignment: Alignment.center,
// //                           height: 40,
// //                           width: 200,
// //                           child: Text(
// //                             'h',
// //                             style: TextStyle(
// //                                 fontSize: 14,
// //                                 color: Colors.red,
// //                                 fontFamily: 'Comfortaa',
// //                                 fontWeight: FontWeight.w800),
// //                           ),
// //                           decoration: BoxDecoration(
// //                             color:
// //                                 Colors.transparent, //color: Colors.cyan[300],
// //                             borderRadius: BorderRadius.only(
// //                                 bottomRight: Radius.circular(10.0),
// //                                 bottomLeft: Radius.circular(10.0)),
// //                           ),
// //                         )
// //                       ],
// //                     )),
// //               ),
// //             );
// //           }),
// //     ),
// //   );
// // }

// // class GMap extends StatefulWidget {
// //   final Position initialPosition;

// //   GMap(this.initialPosition);

// //   @override
// //   State<StatefulWidget> createState() => _MapState();
// // }

// // class _MapState extends State<GMap> {
// //   final GeolocatorService geoService = GeolocatorService();
// //   Completer<GoogleMapController> _controller = Completer();
// //   BitmapDescriptor _markerIcon;
// //   BitmapDescriptor _me;

// //   //double _originLatitude = 50.450712, _originLongitude = 30.510529; // kiev //
// //   double _destLatitude = 50.448938, _destLongitude = 30.514434;

// //   // double _originLatitude2 = 50.448857, _originLongitude2 = 30.514577;
// //   double _destLatitude2 = 50.446069, _destLongitude2 = 30.513515;

// //   // double _originLatitude3 = 50.446021, _originLongitude3 = 30.513841;
// //   double _destLatitude3 = 50.445422, _destLongitude3 = 30.517384;

// //   // double _originLatitude4 = 50.445387, _originLongitude4 = 30.517775;
// //   double _destLatitude4 = 50.448126, _destLongitude4 = 30.519293;

// //   // double _originLatitude5 = 50.448181, _originLongitude5 = 30.519288;
// //   double _destLatitude5 = 50.450961, _destLongitude5 = 30.522501;

// //   Map<MarkerId, Marker> markers = {};
// //   Map<PolylineId, Polyline> polylines = {};
// //   // List<PatternItem> polylinesPatternItem = [];
// //   List<LatLng> polylineCoordinates = [];
// //   List<LatLng> polylineCoordinates2 = [];
// //   List<LatLng> polylineCoordinates3 = [];
// //   List<LatLng> polylineCoordinates4 = [];
// //   List<LatLng> polylineCoordinates5 = [];
// //   PolylinePoints polylinePoints = PolylinePoints();
// //   PolylinePoints polylinePoints2 = PolylinePoints();
// //   PolylinePoints polylinePoints3 = PolylinePoints();
// //   PolylinePoints polylinePoints4 = PolylinePoints();
// //   PolylinePoints polylinePoints5 = PolylinePoints();
// //   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

// //   @override
// //   void initState() {
// //     geoService.getCurrentLocation().listen((position) {
// //       centerScreen(position);
// //     });
// //     super.initState();

// //     _setMarkerIcon();
// //     _setMeIcon();

// //     /// first route
// //     // _addMarker(
// //     //     LatLng(widget.initialPosition.latitude, widget.initialPosition.longitude), // widget.initialPosition.latitude, widget.initialPosition.longitude _originLatitude, _originLongitude
// //     //     "origin",
// //     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
// //     //     InfoWindow(title: 'Start point'));
// //     _addMarker(
// //       LatLng(_destLatitude, _destLongitude),
// //       "destination",
// //       BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
// //       InfoWindow(title: 'End point'),
// //     );
// //     _getPolyline();

// //     /// second route
// //     // _addMarker(
// //     //     LatLng(_originLatitude2, _originLongitude2),
// //     //     "origin2",
// //     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
// //     //     InfoWindow(title: 'Start point 2'));
// //     _addMarker(
// //         LatLng(_destLatitude2, _destLongitude2),
// //         "destination2",
// //         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
// //         InfoWindow(title: 'End point 2'));

// //     _getPolyline2();

// //     /// third route
// //     // _addMarker(
// //     //     LatLng(_originLatitude3, _originLongitude3),
// //     //     "origin3",
// //     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
// //     //     InfoWindow(title: 'Start point 3'));
// //     _addMarker(
// //         LatLng(_destLatitude3, _destLongitude3),
// //         "destination3",
// //         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
// //         InfoWindow(title: 'End point 3'));
// //     _getPolyline3();

// //     /// fourth route
// //     // _addMarker(
// //     //     LatLng(_originLatitude4, _originLongitude4),
// //     //     "origin4",
// //     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow),
// //     //     InfoWindow(title: 'Start point 4'));
// //     _addMarker(
// //         LatLng(_destLatitude4, _destLongitude4),
// //         "destination4",
// //         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow),
// //         InfoWindow(title: 'End point 4'));
// //     _getPolyline4();

// //     /// fifth route
// //     // _addMarker(
// //     //     LatLng(_originLatitude5, _originLongitude5),
// //     //     "origin5",
// //     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
// //     //     InfoWindow(title: 'Start point 5'));
// //     _addMarker(
// //         LatLng(_destLatitude5, _destLongitude5),
// //         "destination5",
// //         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
// //         // BitmapDescriptor.fromAssetImage(ImageConfiguration(), "assets/game.png"),
// //         InfoWindow(title: 'End point 5'));
// //     _getPolyline5();
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       body: GoogleMap(
// //         initialCameraPosition: CameraPosition(
// //             target: LatLng(widget.initialPosition.latitude,
// //                 widget.initialPosition.longitude),
// //             zoom: 17.0),
// //         mapType: MapType.normal,
// //         tiltGesturesEnabled: true,
// //         compassEnabled: true,
// //         scrollGesturesEnabled: true,
// //         zoomGesturesEnabled: true,
// //         zoomControlsEnabled: false, // off default buttons
// //         myLocationEnabled: true,
// //         myLocationButtonEnabled: false, // off default my location
// //         onMapCreated: (GoogleMapController controller) {
// //           _controller.complete(controller);

// //           setState(() {
// //             _addCustomMarker(LatLng(_destLatitude2, _destLongitude2),
// //                 "destination2", _markerIcon, InfoWindow(title: 'End point 2'));
// //           });
// //         },
// //         markers: Set<Marker>.of(markers.values),
// //         polylines: Set<Polyline>.of(polylines.values),
// //       ),
// //     );
// //   }

// //   void _setMarkerIcon() async {
// //     _markerIcon = await BitmapDescriptor.fromAssetImage(
// //       ImageConfiguration(),
// //       "assets/profile_item_1.png",
// //     );
// //   }

// //   void _setMeIcon() async {
// //     _me = await BitmapDescriptor.fromAssetImage(
// //         ImageConfiguration(), "assets/profile_item_2.png");
// //   }

// //   _addMeMarker(
// //       LatLng position, String id, BitmapDescriptor _me, InfoWindow infowindow) {
// //     MarkerId markerId = MarkerId(id);
// //     Marker marker = Marker(
// //         markerId: markerId,
// //         icon: _me,
// //         position: position,
// //         infoWindow: infowindow);
// //     markers[markerId] = marker;
// //   }

// //   _addCustomMarker(LatLng position, String id, BitmapDescriptor _marketIcon,
// //       InfoWindow infowindow) {
// //     MarkerId markerId = MarkerId(id);
// //     Marker marker = Marker(
// //         markerId: markerId,
// //         icon: _marketIcon,
// //         position: position,
// //         infoWindow: infowindow);
// //     markers[markerId] = marker;
// //   }

// //   _addMarker(LatLng position, String id, BitmapDescriptor descriptor,
// //       InfoWindow infowindow) {
// //     MarkerId markerId = MarkerId(id);
// //     Marker marker = Marker(
// //         markerId: markerId,
// //         icon: descriptor,
// //         position: position,
// //         infoWindow: infowindow);
// //     markers[markerId] = marker;
// //   }

// //   // first polyline
// //   _addPolyLine() {
// //     PolylineId id = PolylineId("poly");
// //     Polyline polyline = Polyline(
// //         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
// //         polylineId: id,
// //         color: Colors.blue,
// //         points: polylineCoordinates,
// //         width: 10);
// //     polylines[id] = polyline;
// //     setState(() {});
// //   }

// //   _getPolyline() async {
// //     PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
// //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
// //       PointLatLng(
// //           widget.initialPosition.latitude, widget.initialPosition.longitude),

// //       /// currentLocation.latitude, currentLocation.longitude
// //       PointLatLng(_destLatitude, _destLongitude),
// //       travelMode: TravelMode.walking,
// //       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
// //     );
// //     if (result1.points.isNotEmpty) {
// //       result1.points.forEach((PointLatLng point) {
// //         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
// //       });
// //     }
// //     _addPolyLine();
// //   }

// // // second polyline
// //   _addPolyLine2() {
// //     PolylineId id = PolylineId("poly2");
// //     Polyline polyline2 = Polyline(
// //         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
// //         polylineId: id,
// //         color: Colors.red,
// //         points: polylineCoordinates2,
// //         width: 10);
// //     polylines[id] = polyline2;
// //     setState(() {});
// //   }

// //   _getPolyline2() async {
// //     PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
// //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
// //       PointLatLng(_destLatitude, _destLongitude),
// //       PointLatLng(_destLatitude2, _destLongitude2),
// //       travelMode: TravelMode.walking,
// //     );
// //     if (result2.points.isNotEmpty) {
// //       result2.points.forEach((PointLatLng point2) {
// //         polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
// //       });
// //     }
// //     _addPolyLine2();
// //   }

// // // third polyline
// //   _addPolyLine3() {
// //     PolylineId id = PolylineId("poly3");
// //     Polyline polyline3 = Polyline(
// //         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
// //         polylineId: id,
// //         color: Colors.indigo,
// //         points: polylineCoordinates3,
// //         width: 10);
// //     polylines[id] = polyline3;
// //     setState(() {});
// //   }

// //   _getPolyline3() async {
// //     PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
// //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
// //       PointLatLng(_destLatitude2, _destLongitude2),
// //       PointLatLng(_destLatitude3, _destLongitude3),
// //       travelMode: TravelMode.walking,
// //     );
// //     if (result3.points.isNotEmpty) {
// //       result3.points.forEach((PointLatLng point3) {
// //         polylineCoordinates3.add(LatLng(point3.latitude, point3.longitude));
// //       });
// //     }
// //     _addPolyLine3();
// //   }

// // // fourth polyline
// //   _addPolyLine4() {
// //     PolylineId id = PolylineId("poly4");
// //     Polyline polyline4 = Polyline(
// //         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
// //         polylineId: id,
// //         color: Colors.yellow,
// //         points: polylineCoordinates4,
// //         width: 10);
// //     polylines[id] = polyline4;
// //     setState(() {});
// //   }

// //   _getPolyline4() async {
// //     PolylineResult result4 = await polylinePoints4.getRouteBetweenCoordinates(
// //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
// //       PointLatLng(_destLatitude3, _destLongitude3),
// //       PointLatLng(_destLatitude4, _destLongitude4),
// //       travelMode: TravelMode.walking,
// //     );
// //     if (result4.points.isNotEmpty) {
// //       result4.points.forEach((PointLatLng point4) {
// //         polylineCoordinates4.add(LatLng(point4.latitude, point4.longitude));
// //       });
// //     }
// //     _addPolyLine4();
// //   }

// //   // fifth polyline
// //   _addPolyLine5() {
// //     PolylineId id = PolylineId("poly5");
// //     Polyline polyline5 = Polyline(
// //         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
// //         polylineId: id,
// //         color: Colors.orange,
// //         points: polylineCoordinates5,
// //         width: 10);
// //     polylines[id] = polyline5;
// //     setState(() {});
// //   }

// //   _getPolyline5() async {
// //     PolylineResult result5 = await polylinePoints5.getRouteBetweenCoordinates(
// //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
// //       PointLatLng(_destLatitude4, _destLongitude4),
// //       PointLatLng(_destLatitude5, _destLongitude5),
// //       travelMode: TravelMode.walking,
// //     );
// //     if (result5.points.isNotEmpty) {
// //       result5.points.forEach((PointLatLng point5) {
// //         polylineCoordinates5.add(LatLng(point5.latitude, point5.longitude));
// //       });
// //     }
// //     _addPolyLine5();
// //   }

// //   Future<void> centerScreen(Position position) async {
// //     setState(() {
// //       _addMeMarker(LatLng(position.latitude, position.longitude), "Me", _me,
// //           InfoWindow(title: 'Me'));
// //     });
// //     final GoogleMapController controller = await _controller.future;
// //     controller.animateCamera(CameraUpdate.newCameraPosition(
// //       CameraPosition(
// //           target: LatLng(position.latitude, position.longitude), zoom: 15.0),
// //     ));
// //   }
// //   //   @override
// //   // void dispose() {
// //   //   super.dispose();
// //   // }
// // }

// // --------------------------------------------------------------------------------

// import 'dart:async';
// import 'dart:collection';
// import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/geolocator_service.dart';
// import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:hexcolor/hexcolor.dart';
// import 'package:provider/provider.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';

// class MyAppRoutes extends StatelessWidget {
//   // final geoService = GeolocatorService();
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(children: <Widget>[
//         FutureProvider(
//           create: (context) => geoService.getInitialLocation(),
//           child: MaterialApp(
//             debugShowCheckedModeBanner: false,
//             title: 'Flutter Demo',
//             theme: ThemeData(
//               primarySwatch: Colors.blue,
//             ),
//             home: Consumer<Position>(
//               builder: (context, position, widget) {
//                 return (position != null)
//                     ? GMap(position)
//                     : Center(child: CircularProgressIndicator());
//               },
//             ),
//           ),
//         ),
//         // Positioned.fill(
//         //   bottom: 10,
//         //   child: Align(
//         //     alignment: Alignment.bottomCenter,
//         //     child: SizedBox(
//         //       height: 30,
//         //       width: 150,
//         //       child: RaisedButton(
//         //           shape: new RoundedRectangleBorder(
//         //               borderRadius: new BorderRadius.circular(5.0)),
//         //           color: Colors.orange[900],
//         //           child: Text(
//         //             'Оновити маршрут',
//         //             style: TextStyle(
//         //                 fontSize: 13,
//         //                 color: Colors.white,
//         //                 fontFamily: 'Roboto',
//         //                 fontWeight: FontWeight.w900),
//         //           ),
//         //           onPressed: () {
//         //             Navigator.pushNamed(context, '/testMap.dart');
//         //           }),
//         //     ),
//         //   ),
//         // ),
//       ]),
//     );
//   }
// }

// class GMap extends StatefulWidget {
//   final Position initialPosition;

//   GMap(this.initialPosition);

//   @override
//   State<StatefulWidget> createState() => _MapState();
// }

// class _MapState extends State<GMap> {
//   final GeolocatorService geoService = GeolocatorService();
//   Completer<GoogleMapController> _controller = Completer();

//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   double zoomVal = 17.0;

//   Future<void> _plus(double zoomVal) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//         zoom: zoomVal,
//         target: LatLng(widget.initialPosition.latitude,
//             widget.initialPosition.longitude))));
//   }

//   Future<void> _minus(double zoomVal) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//         target: LatLng(
//             widget.initialPosition.latitude, widget.initialPosition.longitude),
//         zoom: zoomVal)));
//   }

//   Future<void> _getMyLocation(double zoomVal) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//         target: LatLng(
//             widget.initialPosition.latitude, widget.initialPosition.longitude),
//         zoom: zoomVal)));
//   }

//   BitmapDescriptor _checkPoint1;
//   BitmapDescriptor _checkPoint2;
//   BitmapDescriptor _checkPoint3;
//   BitmapDescriptor _checkPoint4;
//   BitmapDescriptor _checkPoint5;
//   BitmapDescriptor _me;
//   Set<Marker> _markers = HashSet<Marker>();

//   double _destLatitude = 50.449284, _destLongitude = 30.512330;
//   double _destLatitude2 = 50.448845, _destLongitude2 = 30.514550;
//   double _destLatitude3 = 50.445969, _destLongitude3 = 30.513440;
//   double _destLatitude4 = 50.445539, _destLongitude4 = 30.515864;
//   double _destLatitude5 = 50.445271, _destLongitude5 = 30.518097;

//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   // List<PatternItem> polylinesPatternItem = [];
//   List<LatLng> polylineCoordinates0 = [];
//   List<LatLng> polylineCoordinates = [];
//   List<LatLng> polylineCoordinates2 = [];
//   List<LatLng> polylineCoordinates3 = [];
//   List<LatLng> polylineCoordinates4 = [];

//   PolylinePoints polylinePoints0 = PolylinePoints();
//   PolylinePoints polylinePoints = PolylinePoints();
//   PolylinePoints polylinePoints2 = PolylinePoints();
//   PolylinePoints polylinePoints3 = PolylinePoints();
//   PolylinePoints polylinePoints4 = PolylinePoints();

//   @override
//   void initState() {
//     geoService.getCurrentLocation().listen((position) {
//       centerScreen(position);
//     });
//     super.initState();

    

//     _getPolyline0();
//     _getPolyline();
//     _getPolyline2();
//     _getPolyline3();
//     _getPolyline4();

//     _setCheckPoint1();
//     _setCheckPoint2();
//     _setCheckPoint3();
//     _setCheckPoint4();
//     _setCheckPoint5();
//     _setMeIcon();
//   }

//   void _setCheckPoint1() async {
//     _checkPoint1 = await BitmapDescriptor.fromAssetImage(
//       ImageConfiguration(),
//       'assets/fg_images/run_map_quest_pic_start.png',
//     );
//   }

//   void _setCheckPoint2() async {
//     _checkPoint2 = await BitmapDescriptor.fromAssetImage(
//       ImageConfiguration(),
//       'assets/fg_images/run_map_quest_pic_check_done.png',
//     );
//   }

//   void _setCheckPoint3() async {
//     _checkPoint3 = await BitmapDescriptor.fromAssetImage(
//       ImageConfiguration(),
//       'assets/fg_images/run_map_quest_pic_check_2.png',
//     );
//   }

//   void _setCheckPoint4() async {
//     _checkPoint4 = await BitmapDescriptor.fromAssetImage(
//       ImageConfiguration(),
//       'assets/fg_images/run_map_quest_pic_check_3.png',
//     );
//   }

//   void _setCheckPoint5() async {
//     _checkPoint5 = await BitmapDescriptor.fromAssetImage(
//       ImageConfiguration(),
//       'assets/fg_images/run_map_quest_pic_finish.png',
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Stack(
//       overflow: Overflow.visible,
//       children: [
//         Container(
//           margin: EdgeInsets.only(top: 109),
//           child: GoogleMap(
//             initialCameraPosition: CameraPosition(
//                 target: LatLng(widget.initialPosition.latitude,
//                     widget.initialPosition.longitude),
//                 zoom: 17.0),
//             mapType: MapType.normal,
//             tiltGesturesEnabled: true,
//             compassEnabled: true,
//             scrollGesturesEnabled: true,
//             zoomGesturesEnabled: true,
//             zoomControlsEnabled: false, // off default buttons
//             myLocationEnabled: true,
//             myLocationButtonEnabled: false, // off default my location
//             onMapCreated: (GoogleMapController controller) {
//               _controller.complete(controller);

//               setState(() {
//                 _markers.add(Marker(
//                   markerId: MarkerId("0"),
//                   position: LatLng(_destLatitude, _destLongitude),
//                   icon: _checkPoint1,
//                 ));

//                 _markers.add(Marker(
//                   markerId: MarkerId("1"),
//                   position: LatLng(_destLatitude2, _destLongitude2),
//                   icon: _checkPoint2,
//                 ));

//                 _markers.add(Marker(
//                   markerId: MarkerId("2"),
//                   position: LatLng(_destLatitude3, _destLongitude3),
//                   icon: _checkPoint3,
//                 ));

//                 _markers.add(Marker(
//                   markerId: MarkerId("3"),
//                   position: LatLng(_destLatitude4, _destLongitude4),
//                   icon: _checkPoint4,
//                 ));

//                 _markers.add(Marker(
//                   markerId: MarkerId("4"),
//                   position: LatLng(_destLatitude5, _destLongitude5),
//                   icon: _checkPoint5,
//                   onTap: () {
//                     // Navigator.push(
//                     //             context,
//                     //             MaterialPageRoute(
//                     //                 builder: (context) => QuestDone()));
//                   },
//                 ));
//               });
//             },
//             markers: _markers,
//             polylines: Set<Polyline>.of(polylines.values),
//           ),
//         ),
//         Positioned(
//           top: 0,
//           child: Container(
//             width: MediaQuery.of(context).size.width,
//             decoration: BoxDecoration(
//               color: Hexcolor('#7D5AC2'),
//               borderRadius: BorderRadius.only(
//                   bottomLeft: Radius.circular(10.0),
//                   bottomRight: Radius.circular(10.0)),
//             ),
//             child: Container(
//               margin: EdgeInsets.only(
//                 top: 50,
//                 // left: 15,
//                 // right: 15,
//                 bottom: 22,
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.end,
//                     children: <Widget>[
//                       Container(
//                         margin: EdgeInsets.only(left: 10, right: 10),
//                         child: SizedBox(
//                           child: IconButton(
//                             icon: Image.asset(
//                               'assets/fg_images/6_home_search_back.png',
//                               width: 10,
//                               height: 19,
//                             ),
//                             onPressed: () {
//                               // Navigator.push(
//                               //   context,
//                               //   MaterialPageRoute(
//                               //       builder: (context) => SplashScreen()),
//                               // );
//                               Navigator.pop(context);
//                             },
//                           ),
//                         ),
//                       ),
//                       Text(
//                         'Test Map',
//                         style: TextStyle(
//                             fontSize: 33,
//                             color: Colors.white,
//                             fontFamily: 'Arial',
//                             fontWeight: FontWeight.w900),
//                       ),
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Positioned(
//           top: 290,
//           right: 20,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               GestureDetector(
//                   onTap: () {
//                     zoomVal++;
//                     _plus(zoomVal);
//                   },
//                   child: Image.asset(
//                     'assets/fg_images/7_map_icon_zoom_plus.png',
//                     width: 50,
//                     height: 50,
//                   )),
//               SizedBox(
//                 height: 15,
//               ),
//               GestureDetector(
//                 onTap: () {
//                   zoomVal--;
//                   _minus(zoomVal);
//                 },
//                 child: Image.asset(
//                   'assets/fg_images/7_map_icon_zoom_minus.png',
//                   width: 50,
//                   height: 50,
//                 ),
//               ),
//               SizedBox(
//                 height: 15,
//               ),
//               GestureDetector(
//                 onTap: () {
//                   _getMyLocation(zoomVal);
//                 },
//                 child: Image.asset(
//                   'assets/fg_images/7_map_icon_location.png',
//                   width: 50,
//                   height: 50,
//                 ),
//               ),
//             ],
//           ),
//         ),
//         Positioned(
//           bottom: 40,
//           child: Container(
//             margin: EdgeInsets.only(left: 5, right: 5),
//             height: 117,
//             width: MediaQuery.of(context).size.width,
//             child: ListView.builder(
//               physics: BouncingScrollPhysics(),
//               scrollDirection: Axis.horizontal,
//               itemCount: checkPoins.length,
//               itemBuilder: (context, index) {
//                 return GestureDetector(
//                   onTap: () {
//                     _goToCheckPoint(
//                         checkPoins[index].lat, checkPoins[index].long);
//                   },
//                   child: Container(
//                     margin: EdgeInsets.only(left: 5, right: 5),
//                     height: 117,
//                     width: 174,
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.all(Radius.circular(10)),
//                         image: DecorationImage(
//                           image: AssetImage(checkPoins[index].image),
//                           fit: BoxFit.cover,
//                         )),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.end,
//                       children: [
//                         Container(
//                           decoration: BoxDecoration(
//                             color: checkPoins[index].color.withOpacity(0.5),
//                             borderRadius: BorderRadius.only(
//                                 bottomRight: Radius.circular(10.0),
//                                 bottomLeft: Radius.circular(10.0)),
//                           ),
//                           height: 33,
//                           child: Container(
//                             margin: EdgeInsets.only(
//                               left: 5,
//                               bottom: 5,
//                             ),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: [
//                                 Container(
//                                   margin: EdgeInsets.only(left: 5),
//                                   child: Text(
//                                     checkPoins[index].name,
//                                     style: TextStyle(
//                                       color: Colors.white,
//                                       fontSize: 13,
//                                       fontFamily: 'Arial',
//                                       fontWeight: FontWeight.w600,
//                                       letterSpacing: 1.09,
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//                 );
//               },
//             ),
//           ),
//         ),
//       ],
//     ));
//   }

//   void _setMeIcon() async {
//     _me = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),
//         "assets/fg_images/run_map_quest_pic_meme.png");
//   }

//   _addMeMarker(
//       LatLng position, String id, BitmapDescriptor _me, InfoWindow infowindow) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker = Marker(
//         markerId: markerId,
//         icon: _me,
//         position: position,
//         infoWindow: infowindow);
//     markers[markerId] = marker;
//   }

//   Future<void> _goToCheckPoint(double lat, double long) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(
//         CameraPosition(target: LatLng(lat, long), zoom: 17)));
//   }

// // polyline from me to start

//   _addPolyLine0() {
//     PolylineId id = PolylineId("meToStart");
//     Polyline polyline0 = Polyline(
//         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
//         polylineId: id,
//         color: Hexcolor('#FE6802'),
//         points: polylineCoordinates0,
//         width: 10);
//     polylines[id] = polyline0;
//     setState(() {});
//   }

//   _getPolyline0() async {
//     PolylineResult result0 = await polylinePoints0.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(
//           widget.initialPosition.latitude, widget.initialPosition.longitude),
//       PointLatLng(_destLatitude, _destLongitude),
//       travelMode: TravelMode.walking,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result0.points.isNotEmpty) {
//       result0.points.forEach((PointLatLng point0) {
//         polylineCoordinates0.add(LatLng(point0.latitude, point0.longitude));
//       });
//     }
//     _addPolyLine0();
//   }

// // first polyline
//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
//         polylineId: id,
//         color: Colors.green,
//         points: polylineCoordinates,
//         width: 10);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_destLatitude, _destLongitude),
//       PointLatLng(_destLatitude2, _destLongitude2),
//       travelMode: TravelMode.walking,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result1.points.isNotEmpty) {
//       result1.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

// // second polyline
//   _addPolyLine2() {
//     PolylineId id = PolylineId("poly2");
//     Polyline polyline2 = Polyline(
//         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
//         polylineId: id,
//         color: Colors.blue,
//         points: polylineCoordinates2,
//         width: 10);
//     polylines[id] = polyline2;
//     setState(() {});
//   }

//   _getPolyline2() async {
//     PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_destLatitude2, _destLongitude2),
//       PointLatLng(_destLatitude3, _destLongitude3),
//       travelMode: TravelMode.walking,
//     );
//     if (result2.points.isNotEmpty) {
//       result2.points.forEach((PointLatLng point2) {
//         polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
//       });
//     }
//     _addPolyLine2();
//   }

// // third polyline
//   _addPolyLine3() {
//     PolylineId id = PolylineId("poly3");
//     Polyline polyline3 = Polyline(
//         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
//         polylineId: id,
//         color: Colors.blue,
//         points: polylineCoordinates3,
//         width: 10);
//     polylines[id] = polyline3;
//     setState(() {});
//   }

//   _getPolyline3() async {
//     PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_destLatitude3, _destLongitude3),
//       PointLatLng(_destLatitude4, _destLongitude4),
//       travelMode: TravelMode.walking,
//     );
//     if (result3.points.isNotEmpty) {
//       result3.points.forEach((PointLatLng point3) {
//         polylineCoordinates3.add(LatLng(point3.latitude, point3.longitude));
//       });
//     }
//     _addPolyLine3();
//   }

// // fourth polyline
//   _addPolyLine4() {
//     PolylineId id = PolylineId("poly4");
//     Polyline polyline4 = Polyline(
//         patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
//         polylineId: id,
//         color: Colors.blue,
//         points: polylineCoordinates4,
//         width: 10);
//     polylines[id] = polyline4;
//     setState(() {});
//   }

//   _getPolyline4() async {
//     PolylineResult result4 = await polylinePoints4.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_destLatitude4, _destLongitude4),
//       PointLatLng(_destLatitude5, _destLongitude5),
//       travelMode: TravelMode.walking,
//     );
//     if (result4.points.isNotEmpty) {
//       result4.points.forEach((PointLatLng point4) {
//         polylineCoordinates4.add(LatLng(point4.latitude, point4.longitude));
//       });
//     }
//     _addPolyLine4();
//   }

//   Future<void> centerScreen(Position position) async {
//     setState(() {
//       _addMeMarker(LatLng(position.latitude, position.longitude), "Me", _me,
//           InfoWindow(title: 'Me'));
//     });
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(
//       CameraPosition(
//           target: LatLng(position.latitude, position.longitude), zoom: 15.0),
//     ));

//     //   @override
//     // void dispose() {
//     //   super.dispose();
//     // }
//   }
// }
