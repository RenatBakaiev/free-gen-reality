import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';
import '10_profile_about_us_static_json.dart';

void _launchUrl(String i) async {
  if (await canLaunch(i)) {
    await launch(i);
  } else {
    throw 'Could not open Url';
  }
}

class ProfileAboutUs extends StatefulWidget {
  @override
  _ProfileAboutUsState createState() => _ProfileAboutUsState();
}

class _ProfileAboutUsState extends State<ProfileAboutUs> {
  List<Publication> posts;
  bool loading;

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesStaticAboutUs.getAboutUs().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Text(
                            'Про нас',
                            style: TextStyle(
                                fontSize: 33,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // Image.asset(
                  //   'assets/fg_images/0_splash_screen_lion_and_penguin.gif',
                  //   height: 350,
                  // ),
                  Expanded(
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: posts.length,
                        itemBuilder: (context, index) {
                          Publication publication = posts[index];

                          var title =
                              utf8.decode(publication.titleUa.runes.toList());
                          var content =
                              utf8.decode(publication.contentUa.runes.toList());

                          return Column(children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 15,
                                left: 20,
                                right: 20,
                              ),
                              child: Row(
                                children: [
                                  Text(
                                    // '',
                                    title == null ? '' : title,
                                    style: TextStyle(
                                        color: Hexcolor('#484848'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 20,
                                right: 20,
                              ),
                              child: Html(
                                data: content == null ? '' : content,
                                onLinkTap: (i) {
                                  _launchUrl(i);
                                },
                              ),
                            ),
                          ]);
                        }),
                  ),

                  // Container(
                  //   margin: EdgeInsets.only(left: 20, right: 20),
                  //   child: Column(children: [
                  //     Container(
                  //       margin: EdgeInsets.only(bottom: 15),
                  //       child: Row(
                  //         children: [
                  //           Text(
                  //             'Описание',
                  //             style: TextStyle(
                  //                 color: Hexcolor('#484848'),
                  //                 fontSize: 18,
                  //                 fontFamily: 'Arial',
                  //                 fontWeight: FontWeight.w600),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //     Text(
                  //       'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                  //       style: TextStyle(
                  //         color: Hexcolor('#484848'),
                  //         fontSize: 16,
                  //         fontFamily: 'Arial',
                  //       ),
                  //     ),
                  //   ]),
                  // ),
                  //   ],
                  // )
                ],
              ),
            ),
    );
  }
}

//-------------------------------------SPLASH SCREEN3-------------------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';

// class ProfileAboutUs extends StatefulWidget {
//   @override
//   _ProfileAboutUsState createState() => _ProfileAboutUsState();
// }

// class _ProfileAboutUsState extends State<ProfileAboutUs> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Stack(
//       overflow: Overflow.visible,
//       children: [
//         Container(
//           color: Hexcolor('#412A72'),
//           child: SingleChildScrollView(
//               physics: BouncingScrollPhysics(),
//               child: Container(
//                 child: Image.asset(
//                   'assets/fg_images/tets.jpg',
//                   width: MediaQuery.of(context).size.width,
//                 ),
//               )),
//         ),
//         Positioned(
//             bottom: 0,
//             left: 25,
//             child: Row(children: [
//               Container(
//                 margin: EdgeInsets.only(bottom: 25),
//                 height: 60,
//                 width: MediaQuery.of(context).size.width / 2 - 30,
//                 child: RaisedButton(
//                   child: Text(
//                     "Назад",
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 17,
//                       fontFamily: 'Arial',
//                       fontWeight: FontWeight.w600,
//                     ),
//                   ),
//                   color: Hexcolor('#EB5C18'),
//                   shape: new RoundedRectangleBorder(
//                       borderRadius: new BorderRadius.circular(14.0)),
//                   onPressed: () {
//                     print('its worked');
//                   },
//                 ),
//               ),
//               SizedBox(width: 10,),
//               Container(
//                 margin: EdgeInsets.only(bottom: 25),
//                 height: 60,
//                 width: MediaQuery.of(context).size.width / 2 - 30,
//                 child: RaisedButton(
//                   child: Text(
//                     "Продолжить",
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 17,
//                       fontFamily: 'Arial',
//                       fontWeight: FontWeight.w600,
//                     ),
//                   ),
//                   color: Hexcolor('#EB5C18'),
//                   shape: new RoundedRectangleBorder(
//                       borderRadius: new BorderRadius.circular(14.0)),
//                   onPressed: () {
//                     print('its worked');
//                   },
//                 ),
//               ),
//             ]))
//       ],
//     ));
//   }
// }
