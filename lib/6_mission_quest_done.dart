import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '6_home_missions_json.dart';

class MissionQuestDone extends StatefulWidget {
  @override
  _MissionQuestDoneState createState() => _MissionQuestDoneState();
}

class _MissionQuestDoneState extends State<MissionQuestDone> {
  AudioCache cache;
  AudioPlayer player;
  void _playFile() async {
    player = await AudioCache().play("music/quest_done_coins.mp3");
  }

  Future<User> changeTus(
      double value, String operation, String typeCurrency) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": typeCurrency,
        "operation": operation,
        "amount": value,
        "description": "mission reward"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');

    if (response.statusCode == 200) {
      print(response.body);
    } else {
      throw Exception('Failed to update User.');
    }
  }

  String missionName;
  bool loading = false;
  double missionPoints;
  var missionCurrency;
  Future<Mission> getMissionData() async {
    setState(() {
      loading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var missionId = sharedPreferences.get('missionId');
    String token = sharedPreferences.get("token");
    var userId = sharedPreferences.get("id");

    var missionDone =
        sharedPreferences.get('Mission$missionId' + 'Done' + '$userId');

    String url =
        'http://generation-admin.ehub.com.ua/api/mission/' + '$missionId';
    String urlCurrencies =
        'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies';
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      final responseCurrencies = await http.get(urlCurrencies, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      Map<String, dynamic> responseJsonCurrencies =
          jsonDecode(responseCurrencies.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          missionName = utf8.decode(responseJson["titleUa"].runes.toList());
          missionPoints = responseJson["points"];

          for (int i = 0;
              i < responseJsonCurrencies["publication"].length;
              i++) {
            if (responseJson["currencyType"] ==
                responseJsonCurrencies["publication"][i]["id"].toString()) {
              missionCurrency =
                  responseJsonCurrencies["publication"][i]["titleUa"];
              print(missionCurrency);
            }
          }
          if (missionDone == '' || missionDone == null) {
            changeTus(missionPoints, "DEPOSIT", missionCurrency);
            sharedPreferences.setBool(
                'Mission$missionId' + 'Done' + '$userId', true);
            print('deposit done');
          } else {
            print('you cant to reseive many times');
          }
          loading = false;
        });
      } else {
        print('error');
      }
    }

    getMission();
    return Mission();
  }

  bool isLoading = false;
  String elapsedTime = '';
  int elapsedtimeFromDarabase = 0;
  var userEmail;

  getUserData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    print(email);
    setState(() {
      userEmail = email;
    });
    print(token);
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';
    print(url);

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
        print(response.body);

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        print(responseJson);

        email = responseJson["email"];
        print(email);

        setState(() {
          if (responseJson["timer"] != null) {
            elapsedtimeFromDarabase = responseJson["timer"];

            transformMilliSeconds(int milliseconds) {
              int hundreds = (milliseconds / 10).truncate();
              int seconds = (hundreds / 100).truncate();
              int minutes = (seconds / 60).truncate();
              int hours = (minutes / 60).truncate();

              String hoursStr = (hours % 60).toString().padLeft(2, '0');
              String minutesStr = (minutes % 60).toString().padLeft(2, '0');
              String secondsStr = (seconds % 60).toString().padLeft(2, '0');

              return "$hoursStr:$minutesStr:$secondsStr";
            }

            elapsedTime = transformMilliSeconds(elapsedtimeFromDarabase);
          }
          isLoading = false;
        });
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  @override
  void initState() {
    super.initState();
    getMissionData();
    getUserData();
    _playFile();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: loading
            ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Hexcolor('#7D5AC2'),
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Hexcolor('#FE6802'),
                  ),
                ),
              )
            : Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 50,
                          ),
                          child: Text(
                            'Вітаємо\nмісія виконана',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 32,
                              fontWeight: FontWeight.w900,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Stack(
                                overflow: Overflow.visible,
                                children: [
                                  Image.asset(
                                    'assets/fg_images/quest_done_lightning2.png',
                                  ),
                                  Positioned(
                                    right: 55,
                                    top: 90,
                                    child: RotationTransition(
                                      turns: AlwaysStoppedAnimation(-21 / 360),
                                      child: Text(
                                        missionPoints.toInt().toString(),
                                        style: TextStyle(
                                          fontSize: 40,
                                          fontWeight: FontWeight.w900,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 20,
                                    top: 115,
                                    child: RotationTransition(
                                      turns: AlwaysStoppedAnimation(-21 / 360),
                                      child: Text(
                                        missionCurrency == null ||
                                                missionCurrency == ""
                                            ? ""
                                            : missionCurrency,
                                        style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.w900,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      bottom: -130,
                                      left: MediaQuery.of(context).size.width /
                                              2 -
                                          125,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/fg_images/quest_done_pic.png',
                                            height: 250,
                                          ),
                                        ],
                                      ))
                                ],
                              ),
                            ],
                          ),
                        ),
                      ]),
                      Column(
                        children: [
                          missionName == "Муромець race"
                              ? isLoading
                                  ? Center(
                                      child: CircularProgressIndicator(
                                        backgroundColor: Hexcolor('#7D5AC2'),
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                          Hexcolor('#FE6802'),
                                        ),
                                      ),
                                    )
                                  : Container(
                                      margin: EdgeInsets.only(),
                                      child: Text(elapsedTime,
                                          style: TextStyle(
                                            fontSize: 25.0,
                                            color: Hexcolor('#FE6802'),
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w600,
                                          )),
                                    )
                              : Container(),
                          Container(
                            margin: EdgeInsets.only(
                              bottom: 20,
                              right: 20,
                              left: 20,
                              top: 15,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      25,
                                  child: RaisedButton(
                                    child: Text(
                                      "Головна",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                    ),
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, '/5_myBottomBar.dart');
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      25,
                                  child: RaisedButton(
                                    child: Text(
                                      "Місце в рейтингу",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      if (missionName ==
                                          "Муромець race") {
                                        Navigator.pushNamed(context,
                                            '/mission_muromec_rating.dart');
                                      } else {
                                        Navigator.pushNamed(
                                            context, '/10_profile_rating.dart');
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
              ),
      ),
    );
  }
}
