import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;

class DescScanQR extends StatefulWidget {
  @override
  _DescScanQRState createState() => _DescScanQRState();
}

class _DescScanQRState extends State<DescScanQR> {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    Future<void> share() async {
      await FlutterShare.share(
        title: 'Загружай новое приложение и попробуйте выполнить миссию \n' +
            arguments['name'],
        // text: 'Example share text',
        linkUrl: 'https://google.com/',
        // chooserTitle: 'Example Chooser Title'
      );
    }

    showInfoWindow3() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              // insetPadding: EdgeInsets.only(
              //   left: 20,
              //   right: 20,
              // ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 390,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Начало миссии',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          // start progress line
                          margin: EdgeInsets.only(top: 30),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_start.png',
                                          width: 12,
                                          height: 12),
                                      Positioned(
                                        bottom: 9,
                                        left: 4.5,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                                'assets/fg_images/next_step_icon_start_flag.png',
                                                width: 18,
                                                height: 21),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 10, bottom: 8),
                                              child: Text(
                                                'ул. Владимирская, 24а',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 12,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  // Container(
                                  //   height: 1.5,
                                  //   width: 25,
                                  //   decoration:
                                  //       BoxDecoration(color: Hexcolor('#1E2E45')),
                                  // ),
                                  Container(
                                    height: 25,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: 6,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              Container(
                                                height: 1.5,
                                                width: 25,
                                                decoration: BoxDecoration(
                                                    color: Hexcolor('#1E2E45')),
                                              ),
                                              Image.asset(
                                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                                  width: 12,
                                                  height: 12),
                                            ],
                                          );
                                        }),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 11,
                                    ),
                                    child: Image.asset(
                                      'assets/fg_images/turn_icon.png',
                                      width: 25,
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  right: 31,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                        'assets/fg_images/next_step_icon_notdone_step.png',
                                        width: 12,
                                        height: 12),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 11,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 11,
                                    ),
                                    child: Image.asset(
                                      'assets/fg_images/turn_icon4.png',
                                      width: 25,
                                    ),
                                  ),
                                  Image.asset(
                                      'assets/fg_images/next_step_icon_notdone_step.png',
                                      width: 12,
                                      height: 12),
                                  Container(
                                    height: 25,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: 5,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              Container(
                                                height: 1.5,
                                                width: 25,
                                                decoration: BoxDecoration(
                                                    color: Hexcolor('#1E2E45')),
                                              ),
                                              Image.asset(
                                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                                  width: 12,
                                                  height: 12),
                                            ],
                                          );
                                        }),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 11,
                                    ),
                                    child: Image.asset(
                                      'assets/fg_images/turn_icon2.png',
                                      width: 25,
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 42,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                        'assets/fg_images/next_step_icon_notdone_step.png',
                                        width: 12,
                                        height: 12),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 20,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        bottom: 11,
                                      ),
                                      child: Image.asset(
                                        'assets/fg_images/turn_icon3.png',
                                        width: 25,
                                      ),
                                    ),
                                    Container(
                                      height: 25,
                                      child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: 6,
                                          itemBuilder: (context, index) {
                                            return Row(
                                              children: [
                                                Image.asset(
                                                    'assets/fg_images/next_step_icon_notdone_step.png',
                                                    width: 12,
                                                    height: 12),
                                                Container(
                                                  height: 1.5,
                                                  width: 25,
                                                  decoration: BoxDecoration(
                                                      color:
                                                          Hexcolor('#1E2E45')),
                                                ),
                                              ],
                                            );
                                          }),
                                    ),
                                    Stack(
                                      overflow: Overflow.visible,
                                      children: [
                                        Image.asset(
                                            'assets/fg_images/next_step_icon_notdone_step.png',
                                            width: 12,
                                            height: 12),
                                        Positioned(
                                          top: 9,
                                          right: 4.5,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(
                                                    right: 10, top: 8),
                                                child: Text(
                                                  'ул. Богдана Хмельницкого, 13/2',
                                                  style: TextStyle(
                                                    color: Hexcolor('#1E2E45'),
                                                    fontSize: 12,
                                                    fontFamily: 'Arial',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                              ),
                                              Image.asset(
                                                  'assets/fg_images/next_step_icon_finish_flag.png',
                                                  width: 18,
                                                  height: 21),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),

                              // Container(
                              //   margin: EdgeInsets.only(
                              //     top: 9,
                              //   ),
                              //   child: Image.asset(
                              //     'assets/fg_images/turn_icon.png',
                              //     width: 25,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Поздравляем с началом миссии,\nвам осталось пройти ' +
                                '21' +
                                ' шаг!',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Начать миссию",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/mission_scanQR.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  Positioned(
                    right: 40,
                    top: 50,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '0',
                            style: TextStyle(
                              fontSize: 36,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 2),
                            child: Text(
                              ' ' + '/ ' + '21',
                              style: TextStyle(
                                fontSize: 24,
                                color: Hexcolor('#1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          });
    }

    showInfoWindow2() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              // insetPadding: EdgeInsets.only(
              //   left: 20,
              //   right: 20,
              // ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 390,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Начало миссии',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          // start progress line
                          margin: EdgeInsets.only(top: 30),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_start.png',
                                          width: 12,
                                          height: 12),
                                      Positioned(
                                        bottom: 9,
                                        left: 4.5,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                                'assets/fg_images/next_step_icon_start_flag.png',
                                                width: 18,
                                                height: 21),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 10, bottom: 8),
                                              child: Text(
                                                'ул. Владимирская, 24а',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 12,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  // Container(
                                  //   height: 1.5,
                                  //   width: 25,
                                  //   decoration:
                                  //       BoxDecoration(color: Hexcolor('#1E2E45')),
                                  // ),
                                  Container(
                                    height: 25,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: 6,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              Container(
                                                height: 1.5,
                                                width: 25,
                                                decoration: BoxDecoration(
                                                    color: Hexcolor('#1E2E45')),
                                              ),
                                              Image.asset(
                                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                                  width: 12,
                                                  height: 12),
                                            ],
                                          );
                                        }),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 11,
                                    ),
                                    child: Image.asset(
                                      'assets/fg_images/turn_icon.png',
                                      width: 25,
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  right: 31,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                        'assets/fg_images/next_step_icon_notdone_step.png',
                                        width: 12,
                                        height: 12),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_notdone_step.png',
                                          width: 12,
                                          height: 12),
                                      Positioned(
                                        bottom: -19,
                                        left: 5.5,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                                'assets/fg_images/next_step_icon_finish_flag2.png',
                                                width: 18,
                                                height: 21),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 10, top: 8),
                                              child: Text(
                                                'ул. Богдана Хмельницкого, 13/2',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 12,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  // Container(
                                  //   height: 1.5,
                                  //   width: 25,
                                  //   decoration:
                                  //       BoxDecoration(color: Hexcolor('#1E2E45')),
                                  // ),
                                  Container(
                                    height: 25,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount: 6,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              Container(
                                                height: 1.5,
                                                width: 25,
                                                decoration: BoxDecoration(
                                                    color: Hexcolor('#1E2E45')),
                                              ),
                                              Image.asset(
                                                  'assets/fg_images/next_step_icon_notdone_step.png',
                                                  width: 12,
                                                  height: 12),
                                            ],
                                          );
                                        }),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      bottom: 11,
                                    ),
                                    child: Image.asset(
                                      'assets/fg_images/turn_icon2.png',
                                      width: 25,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Поздравляем с началом миссии,\nвам осталось пройти ' +
                                '14' +
                                ' шагов!',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Начать миссию",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              showInfoWindow3();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  Positioned(
                    right: 40,
                    top: 50,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '0',
                            style: TextStyle(
                              fontSize: 36,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 2),
                            child: Text(
                              ' ' + '/ ' + '14',
                              style: TextStyle(
                                fontSize: 24,
                                color: Hexcolor('#1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          });
    }

    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              // insetPadding: EdgeInsets.only(
              //   left: 20,
              //   right: 20,
              // ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 390,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Начало миссии',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          // start progress line
                          margin: EdgeInsets.only(top: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Stack(
                                overflow: Overflow.visible,
                                children: [
                                  Image.asset(
                                      'assets/fg_images/next_step_icon_start.png',
                                      width: 12,
                                      height: 12),
                                  Positioned(
                                    bottom: 9,
                                    left: 4.5,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                            'assets/fg_images/next_step_icon_start_flag.png',
                                            width: 18,
                                            height: 21),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 10, bottom: 8),
                                          child: Text(
                                            'ул. Владимирская, 24а',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 12,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                height: 1.5,
                                width: 25,
                                decoration:
                                    BoxDecoration(color: Hexcolor('#1E2E45')),
                              ),
                              Container(
                                height: 25,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 6,
                                    itemBuilder: (context, index) {
                                      return Row(
                                        children: [
                                          Image.asset(
                                              'assets/fg_images/next_step_icon_notdone_step.png',
                                              width: 12,
                                              height: 12),
                                          Container(
                                            height: 1.5,
                                            width: 25,
                                            decoration: BoxDecoration(
                                                color: Hexcolor('#1E2E45')),
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                              // Container(
                              //   margin: EdgeInsets.only(
                              //     top: 9,
                              //   ),
                              //   child: Image.asset(
                              //     'assets/fg_images/turn_icon.png',
                              //     width: 25,
                              //   ),
                              // ),
                              Stack(
                                overflow: Overflow.visible,
                                children: [
                                  Image.asset(
                                      'assets/fg_images/next_step_icon_notdone_step.png',
                                      width: 12,
                                      height: 12),
                                  Positioned(
                                    top: 9,
                                    right: 4.5,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              right: 10, top: 8),
                                          child: Text(
                                            'ул. Богдана Хмельницкого, 13/2',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 12,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        Image.asset(
                                            'assets/fg_images/next_step_icon_finish_flag.png',
                                            width: 18,
                                            height: 21),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Поздравляем с началом миссии,\nвам осталось пройти ' +
                                '7' +
                                ' шагов!',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Начать миссию",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              showInfoWindow2();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  Positioned(
                    right: 40,
                    top: 50,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '0',
                            style: TextStyle(
                              fontSize: 36,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 2),
                            child: Text(
                              ' ' + '/ ' + '7',
                              style: TextStyle(
                                fontSize: 24,
                                color: Hexcolor('#1E2E45'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 85,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          margin: EdgeInsets.only(top: 250),
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0)),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 5,
                            ),
                            child: Text(
                              arguments['name'],
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                        ),
                        Container(
                            height: 260,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                // borderRadius: BorderRadius.only(
                                //     bottomRight: Radius.circular(10.0),
                                //     bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                              image: AssetImage(arguments['image']),
                              fit: BoxFit.cover,
                            )),
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            left: 10,
                                            top: 35,
                                          ),
                                          child: IconButton(
                                            icon: Image.asset(
                                              'assets/fg_images/6_home_search_back.png',
                                              width: 13.16,
                                              height: 25,
                                            ),
                                            onPressed: () {
                                              Navigator.pushNamed(context,
                                                  '/5_myBottomBar.dart');
                                            },
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 35),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  setState(() {
                                                    isFavorite = !isFavorite;
                                                  });
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    isFavorite
                                                        ? 'assets/fg_images/9_favorites_icon.png'
                                                        : 'assets/fg_images/sportGames_icon_favorite.png',
                                                    width: 19.25,
                                                    height: 27.5,
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  Navigator.pushNamed(
                                                      context, '/7_map.dart');
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    'assets/fg_images/sportGames_icon_map.png',
                                                    width: 18.75,
                                                    height: 23.75,
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  share();
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    'assets/fg_images/6_home_logo_share.png',
                                                    width: 24.4,
                                                    height: 26.28,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          margin: EdgeInsets.only(
                                            left: 20,
                                            bottom: 15,
                                            right: 20,
                                          ),
                                          child:
                                              // Text('')
                                              Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              SizedBox(
                                                width: 38,
                                              ),
                                              // Image.asset(
                                              //   'assets/fg_images/6_home_logo_unlock.png',
                                              //   width: 38,
                                              //   height: 38,
                                              // ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    '0',
                                                    style: TextStyle(
                                                      fontSize: 36,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                  Text(
                                                    ' ' +
                                                        '/ ' +
                                                        arguments['objects']
                                                            .toString(),
                                                    style: TextStyle(
                                                      fontSize: 24,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                          ),
                                          height: 40,
                                          child: Container(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Container(
                                    child: ClipRect(
                                      child: BackdropFilter(
                                        filter: ui.ImageFilter.blur(
                                          sigmaX: 3.0,
                                          sigmaY: 3.0,
                                        ),
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: 20,
                                            left: 20,
                                          ),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              bottomRight:
                                                  Radius.circular(10.0),
                                              bottomLeft: Radius.circular(10.0),
                                            ),
                                          ),
                                          alignment: Alignment.center,
                                          height: 40.0,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                    'assets/fg_images/6_home_logo_time.png',
                                                    width: 18,
                                                    height: 18,
                                                    color: Hexcolor('#FE6802'),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    arguments['time']
                                                        .toString(),
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    arguments['time'] != 1
                                                        ? ' минут'
                                                        : ' минута',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                width: 7.5,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                    'assets/fg_images/sportGames_icon_favorite.png',
                                                    width: 15,
                                                    height: 18,
                                                    color: Hexcolor('#FE6802'),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    arguments['objects']
                                                        .toString(),
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    arguments['objects'] != 1
                                                        ? ' объекта'
                                                        : ' объект',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                width: 7.5,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                    'assets/fg_images/6_home_icon_flag.png',
                                                    width: 11,
                                                    height: 18.5,
                                                    color: Hexcolor('#FE6802'),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    arguments['distance']
                                                            .toString() +
                                                        ' км',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    ' до старта',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                top: 20,
                              ),
                              child: Text(
                                'Описание',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                              ),
                              child: Text(
                                'Отсканируйте qr-код для получения дополнительной информации. Дальше нужно действовать по инструкции.',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 14,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                ),
                              ),
                            ),
                            // Container(
                            //   margin: EdgeInsets.only(top: 15),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Container(
                            //           margin: EdgeInsets.only(
                            //             right: 15,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/fg_images/sportGames_icon_pin.png',
                            //             width: 20,
                            //             height: 29,
                            //           )),
                            //       Flexible(
                            //           child: Text(
                            //         'Софиевская площадь, Киев',
                            //         style: TextStyle(
                            //           color: Hexcolor('#545454'),
                            //           fontSize: 16,
                            //           fontFamily: 'Arial',
                            //           // letterSpacing: 1.025,
                            //         ),
                            //       )),
                            //     ],
                            //   ),
                            // ),
                            // Container(
                            //   margin: EdgeInsets.only(top: 15, bottom: 15),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Container(
                            //           margin: EdgeInsets.only(
                            //             right: 15,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/fg_images/sportGames_icon_clock.png',
                            //             width: 25,
                            //             height: 25,
                            //           )),
                            //       Flexible(
                            //           child: Text(
                            //         '12.03.2021 ( 16:30 )',
                            //         style: TextStyle(
                            //           color: Hexcolor('#545454'),
                            //           fontSize: 16,
                            //           fontFamily: 'Arial',
                            //         ),
                            //       )),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Начать Миссию",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  // Navigator.pushNamed(context, '/mission_scanQR.dart');
                  showInfoWindow();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
