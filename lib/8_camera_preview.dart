import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'dart:ui' as ui;
import 'package:permission_handler/permission_handler.dart';
import 'package:hexcolor/hexcolor.dart';

class PreviewScreen extends StatefulWidget {
  final String imgPath;

  PreviewScreen({this.imgPath});

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {
  GlobalKey globalKey = GlobalKey();
  GlobalKey _globalKey = GlobalKey();

  bool signalTurnedOn = false;

  makeSignal() {
    setState(() {
      signalTurnedOn = true;
    });
    Future.delayed(const Duration(milliseconds: 250), () {
      setState(() {
        signalTurnedOn = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _requestPermission();
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();
    final info = statuses[Permission.storage].toString();
    print(info);
  }

  _saveScreen() async {
    RenderRepaintBoundary boundary =
        _globalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    final result = await ImageGallerySaver.saveImage(
        byteData.buffer.asUint8List(),
        quality: 100);
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: RepaintBoundary(
                    key: _globalKey,
                    child: Image.file(
                      File(widget.imgPath),
                      filterQuality: FilterQuality.high,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          Positioned(
              bottom: 0,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 240.0,
                  color: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Зберегти в галерею",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            _saveScreen();
                          },
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Поділитися",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          color: signalTurnedOn
                              ? Hexcolor('#75C433')
                              : Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            makeSignal();
                            getBytesFromFile().then((bytes) {
                              Share.file('Share via', basename(widget.imgPath),
                                  bytes.buffer.asUint8List(), 'image/path',
                                  text: 'FreeGen');
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Future<ByteData> getBytesFromFile() async {
    Uint8List bytes = File(widget.imgPath).readAsBytesSync();
    return ByteData.view(bytes.buffer);
  }
}
