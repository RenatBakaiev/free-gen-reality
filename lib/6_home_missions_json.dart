// // To parse this JSON data, do
// //
// //     final missions = missionsFromJson(jsonString);

// import 'dart:convert';

// Missions missionsFromJson(String str) => Missions.fromJson(json.decode(str));

// String missionsToJson(Missions data) => json.encode(data.toJson());

// class Missions {
//     Missions({
//         this.missions,
//         this.page,
//     });

//     List<Mission> missions;
//     Page page;

//     factory Missions.fromJson(Map<String, dynamic> json) => Missions(
//         missions: List<Mission>.from(json["missions"].map((x) => Mission.fromJson(x))),
//         page: Page.fromJson(json["page"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "missions": List<dynamic>.from(missions.map((x) => x.toJson())),
//         "page": page.toJson(),
//     };
// }

// List<MissionProgress> missionProgressFromJson(String str) => List<MissionProgress>.from(json.decode(str).map((x) => MissionProgress.fromJson(x)));

// String missionProgressToJson(List<MissionProgress> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class MissionProgress {
//     MissionProgress({
//         this.trackStatus,
//         this.missionId,
//         this.subMissionProgressTracks,
//     });

//     String trackStatus;
//     int missionId;
//     List<dynamic> subMissionProgressTracks;

//     factory MissionProgress.fromJson(Map<String, dynamic> json) => MissionProgress(
//         trackStatus: json["trackStatus"],
//         missionId: json["missionId"],
//         subMissionProgressTracks: List<dynamic>.from(json["subMissionProgressTracks"].map((x) => x)),
//     );

//     Map<String, dynamic> toJson() => {
//         "trackStatus": trackStatus,
//         "missionId": missionId,
//         "subMissionProgressTracks": List<dynamic>.from(subMissionProgressTracks.map((x) => x)),
//     };
// }

// class Mission {
//     Mission({
//         this.id,
//         this.titleUa,
//         this.titleRu,
//         this.titleEn,
//         this.accessType,
//         this.subMissionList,
//         this.filters,
//         this.statuses,
//         this.duration,
//         this.descriptionUa,
//         this.descriptionRu,
//         this.descriptionEn,
//         this.location,
//         this.position,
//         this.createdDate,
//         this.lastModifiedDate,
//         this.currencyType,
//         this.points,
//         this.imageUrl,
//         this.audioUrl,
//         this.videoUrl,
//         this.promoCode,
//         this.price,
//         this.priceCurrency,
//         this.recommend,
//         this.active,
//         this.progress,
//     });

//     int id;
//     String titleUa;
//     String titleRu;
//     String titleEn;
//     String accessType;
//     List<SubMissionList> subMissionList;
//     List<Filter> filters;
//     List<Filter> statuses;
//     int duration;
//     String descriptionUa;
//     String descriptionRu;
//     String descriptionEn;
//     Location location;
//     int position;
//     DateTime createdDate;
//     DateTime lastModifiedDate;
//     String currencyType;
//     double points;
//     String imageUrl;
//     String audioUrl;
//     String videoUrl;
//     String promoCode;
//     double price;
//     String priceCurrency;
//     bool recommend;
//     bool active;
//     String progress; 

//     factory Mission.fromJson(Map<String, dynamic> json) => Mission(
//         id: json["id"],
//         titleUa: json["titleUa"],
//         titleRu: json["titleRu"],
//         titleEn: json["titleEn"] == null ? null : json["titleEn"],
//         accessType: json["accessType"],
//         subMissionList: List<SubMissionList>.from(json["subMissionList"].map((x) => SubMissionList.fromJson(x))),
//         filters: List<Filter>.from(json["filters"].map((x) => Filter.fromJson(x))),
//         statuses: List<Filter>.from(json["statuses"].map((x) => Filter.fromJson(x))),
//         duration: json["duration"],
//         descriptionUa: json["descriptionUa"],
//         descriptionRu: json["descriptionRu"],
//         descriptionEn: json["descriptionEn"] == null ? null : json["descriptionEn"],
//         location: json["location"] == null ? null : Location.fromJson(json["location"]),
//         position: json["position"],
//         createdDate: DateTime.parse(json["createdDate"]),
//         lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
//         currencyType: json["currencyType"],
//         points: json["points"],
//         imageUrl: json["imageUrl"],
//         audioUrl: json["audioUrl"],
//         videoUrl: json["videoUrl"],
//         promoCode: json["promoCode"],
//         price: json["price"] == null ? null : json["price"],
//         priceCurrency: json["priceCurrency"],
//         recommend: json["recommend"],
//         active: json["active"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "titleUa": titleUa,
//         "titleRu": titleRu,
//         "titleEn": titleEn == null ? null : titleEn,
//         "accessType": accessType,
//         "subMissionList": List<dynamic>.from(subMissionList.map((x) => x.toJson())),
//         "filters": List<dynamic>.from(filters.map((x) => x.toJson())),
//         "statuses": List<dynamic>.from(statuses.map((x) => x.toJson())),
//         "duration": duration,
//         "descriptionUa": descriptionUa,
//         "descriptionRu": descriptionRu,
//         "descriptionEn": descriptionEn == null ? null : descriptionEn,
//         "location": location == null ? null : location.toJson(),
//         "position": position,
//         "createdDate": createdDate.toIso8601String(),
//         "lastModifiedDate": lastModifiedDate.toIso8601String(),
//         "currencyType": currencyType,
//         "points": points,
//         "imageUrl": imageUrl,
//         "audioUrl": audioUrl,
//         "videoUrl": videoUrl,
//         "promoCode": promoCode,
//         "price": price == null ? null : price,
//         "priceCurrency": priceCurrency,
//         "recommend": recommend,
//         "active": active,
//     };
// }

// class Filter {
//     Filter({
//         this.active,
//         this.deleted,
//         this.id,
//         this.img,
//         this.tagsUa,
//         this.position,
//         this.color,
//         this.cost,
//         this.address,
//         this.publicationType,
//         this.author,
//         this.titleUa,
//         this.titleRu,
//         this.titleEn,
//         this.pageType,
//         this.titleShortUa,
//         this.titleShortRu,
//         this.titleShortEn,
//         this.contentUa,
//         this.contentRu,
//         this.contentEn,
//         this.contentShortUa,
//         this.contentShortRu,
//         this.contentShortEn,
//         this.publicationDate,
//         this.expireDate,
//     });

//     bool active;
//     bool deleted;
//     int id;
//     Img img;
//     dynamic tagsUa;
//     int position;
//     String color;
//     double cost;
//     String address;
//     PublicationType publicationType;
//     String author;
//     String titleUa;
//     String titleRu;
//     String titleEn;
//     dynamic pageType;
//     dynamic titleShortUa;
//     dynamic titleShortRu;
//     dynamic titleShortEn;
//     dynamic contentUa;
//     dynamic contentRu;
//     dynamic contentEn;
//     dynamic contentShortUa;
//     dynamic contentShortRu;
//     dynamic contentShortEn;
//     DateTime publicationDate;
//     DateTime expireDate;

//     factory Filter.fromJson(Map<String, dynamic> json) => Filter(
//         active: json["active"],
//         deleted: json["deleted"],
//         id: json["id"],
//         img: imgValues.map[json["img"]],
//         tagsUa: json["tagsUa"],
//         position: json["position"],
//         color: json["color"],
//         cost: json["cost"] == null ? null : json["cost"],
//         address: json["address"] == null ? null : json["address"],
//         publicationType: publicationTypeValues.map[json["publicationType"]],
//         author: json["author"],
//         titleUa: json["titleUa"],
//         titleRu: json["titleRu"] == null ? null : json["titleRu"],
//         titleEn: json["titleEn"] == null ? null : json["titleEn"],
//         pageType: json["page_type"],
//         titleShortUa: json["titleShortUa"],
//         titleShortRu: json["titleShortRu"],
//         titleShortEn: json["titleShortEn"],
//         contentUa: json["contentUa"],
//         contentRu: json["contentRu"],
//         contentEn: json["contentEn"],
//         contentShortUa: json["contentShortUa"],
//         contentShortRu: json["contentShortRU"],
//         contentShortEn: json["contentShortEn"],
//         publicationDate: DateTime.parse(json["publicationDate"]),
//         expireDate: DateTime.parse(json["expireDate"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "active": active,
//         "deleted": deleted,
//         "id": id,
//         "img": imgValues.reverse[img],
//         "tagsUa": tagsUa,
//         "position": position,
//         "color": color,
//         "cost": cost == null ? null : cost,
//         "address": address == null ? null : address,
//         "publicationType": publicationTypeValues.reverse[publicationType],
//         "author": author,
//         "titleUa": titleUa,
//         "titleRu": titleRu == null ? null : titleRu,
//         "titleEn": titleEn == null ? null : titleEn,
//         "page_type": pageType,
//         "titleShortUa": titleShortUa,
//         "titleShortRu": titleShortRu,
//         "titleShortEn": titleShortEn,
//         "contentUa": contentUa,
//         "contentRu": contentRu,
//         "contentEn": contentEn,
//         "contentShortUa": contentShortUa,
//         "contentShortRU": contentShortRu,
//         "contentShortEn": contentShortEn,
//         "publicationDate": publicationDate.toIso8601String(),
//         "expireDate": expireDate.toIso8601String(),
//     };
// }

// enum Img { DEFAULT_PNG }

// final imgValues = EnumValues({
//     "default.png": Img.DEFAULT_PNG
// });

// enum PublicationType { FILTERS, STATUSES }

// final publicationTypeValues = EnumValues({
//     "filters": PublicationType.FILTERS,
//     "statuses": PublicationType.STATUSES
// });

// class Location {
//     Location({
//         this.id,
//         this.latitude,
//         this.longitude,
//         this.active,
//     });

//     int id;
//     String latitude;
//     String longitude;
//     bool active;

//     factory Location.fromJson(Map<String, dynamic> json) => Location(
//         id: json["id"],
//         latitude: json["latitude"] == null ? null : json["latitude"],
//         longitude: json["longitude"] == null ? null : json["longitude"],
//         active: json["active"] == null ? null : json["active"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "latitude": latitude == null ? null : latitude,
//         "longitude": longitude == null ? null : longitude,
//         "active": active == null ? null : active,
//     };
// }

// class SubMissionList {
//     SubMissionList({
//         this.id,
//         this.titleUa,
//         this.titleRu,
//         this.titleEn,
//         this.createdDate,
//         this.lastModifiedDate,
//         this.imageUrl,
//         this.position,
//         this.stepList,
//         this.currencyType,
//         this.points,
//         this.active,
//     });

//     int id;
//     String titleUa;
//     String titleRu;
//     TitleEn titleEn;
//     DateTime createdDate;
//     DateTime lastModifiedDate;
//     String imageUrl;
//     int position;
//     List<StepList> stepList;
//     String currencyType;
//     double points;
//     bool active;

//     factory SubMissionList.fromJson(Map<String, dynamic> json) => SubMissionList(
//         id: json["id"],
//         titleUa: json["titleUa"],
//         titleRu: json["titleRu"],
//         titleEn: titleEnValues.map[json["titleEn"]],
//         createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
//         lastModifiedDate: json["lastModifiedDate"] == null ? null : DateTime.parse(json["lastModifiedDate"]),
//         imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
//         position: json["position"],
//         stepList: List<StepList>.from(json["stepList"].map((x) => StepList.fromJson(x))),
//         currencyType: json["currencyType"],
//         points: json["points"] == null ? null : json["points"],
//         active: json["active"] == null ? null : json["active"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "titleUa": titleUa,
//         "titleRu": titleRu,
//         "titleEn": titleEnValues.reverse[titleEn],
//         "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
//         "lastModifiedDate": lastModifiedDate == null ? null : lastModifiedDate.toIso8601String(),
//         "imageUrl": imageUrl == null ? null : imageUrl,
//         "position": position,
//         "stepList": List<dynamic>.from(stepList.map((x) => x.toJson())),
//         "currencyType": currencyType,
//         "points": points == null ? null : points,
//         "active": active == null ? null : active,
//     };
// }

// class StepList {
//     StepList({
//         this.id,
//         this.stepType,
//         this.titleUa,
//         this.titleRu,
//         this.titleEn,
//         this.createdDate,
//         this.lastModifiedDate,
//         this.location,
//         this.descriptionUa,
//         this.descriptionRu,
//         this.descriptionEn,
//         this.imageUrl,
//         this.audioUrl,
//         this.videoUrl,
//         this.position,
//         this.currencyType,
//         this.points,
//         this.activationRadius,
//         this.correctAnswer,
//         this.currencyTypeWrongAnswer,
//         this.pointsWrongAnswer,
//         this.imageScan,
//         this.scanType,
//         this.canBeSkipped,
//         this.active,
//         this.modelList,
//         this.geolocationList,
//     });

//     int id;
//     StepType stepType;
//     String titleUa;
//     String titleRu;
//     String titleEn;
//     DateTime createdDate;
//     DateTime lastModifiedDate;
//     dynamic location;
//     String descriptionUa;
//     String descriptionRu;
//     String descriptionEn;
//     String imageUrl;
//     AudioUrl audioUrl;
//     VideoUrl videoUrl;
//     int position;
//     String currencyType;
//     double points;
//     dynamic activationRadius;
//     String correctAnswer;
//     String currencyTypeWrongAnswer;
//     double pointsWrongAnswer;
//     ImageScan imageScan;
//     String scanType;
//     bool canBeSkipped;
//     bool active;
//     List<dynamic> modelList;
//     List<dynamic> geolocationList;

//     factory StepList.fromJson(Map<String, dynamic> json) => StepList(
//         id: json["id"],
//         stepType: stepTypeValues.map[json["stepType"]],
//         titleUa: json["titleUa"],
//         titleRu: json["titleRu"],
//         titleEn: json["titleEn"],
//         createdDate: DateTime.parse(json["createdDate"]),
//         lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
//         location: json["location"],
//         descriptionUa: json["descriptionUa"],
//         descriptionRu: json["descriptionRu"],
//         descriptionEn: json["descriptionEn"],
//         imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
//         audioUrl: json["audioUrl"] == null ? null : audioUrlValues.map[json["audioUrl"]],
//         videoUrl: json["videoUrl"] == null ? null : videoUrlValues.map[json["videoUrl"]],
//         position: json["position"],
//         currencyType: json["currencyType"],
//         points: json["points"] == null ? null : json["points"],
//         activationRadius: json["activationRadius"],
//         correctAnswer: json["correctAnswer"] == null ? null : json["correctAnswer"],
//         currencyTypeWrongAnswer: json["currencyTypeWrongAnswer"],
//         pointsWrongAnswer: json["pointsWrongAnswer"] == null ? null : json["pointsWrongAnswer"],
//         imageScan: imageScanValues.map[json["imageScan"]],
//         scanType: json["scanType"],
//         canBeSkipped: json["canBeSkipped"],
//         active: json["active"],
//         modelList: List<dynamic>.from(json["modelList"].map((x) => x)),
//         geolocationList: List<dynamic>.from(json["geolocationList"].map((x) => x)),
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "stepType": stepTypeValues.reverse[stepType],
//         "titleUa": titleUa,
//         "titleRu": titleRu,
//         "titleEn": titleEn,
//         "createdDate": createdDate.toIso8601String(),
//         "lastModifiedDate": lastModifiedDate.toIso8601String(),
//         "location": location,
//         "descriptionUa": descriptionUa,
//         "descriptionRu": descriptionRu,
//         "descriptionEn": descriptionEn,
//         "imageUrl": imageUrl == null ? null : imageUrl,
//         "audioUrl": audioUrl == null ? null : audioUrlValues.reverse[audioUrl],
//         "videoUrl": videoUrl == null ? null : videoUrlValues.reverse[videoUrl],
//         "position": position,
//         "currencyType": currencyType,
//         "points": points == null ? null : points,
//         "activationRadius": activationRadius,
//         "correctAnswer": correctAnswer == null ? null : correctAnswer,
//         "currencyTypeWrongAnswer": currencyTypeWrongAnswer,
//         "pointsWrongAnswer": pointsWrongAnswer == null ? null : pointsWrongAnswer,
//         "imageScan": imageScanValues.reverse[imageScan],
//         "scanType": scanType,
//         "canBeSkipped": canBeSkipped,
//         "active": active,
//         "modelList": List<dynamic>.from(modelList.map((x) => x)),
//         "geolocationList": List<dynamic>.from(geolocationList.map((x) => x)),
//     };
// }

// enum AudioUrl { EMPTY, THE_20210124_FRANK_SINATRA_L_O_V_E_LYRICS_MP3, THE_20210124_ICE_CRACKING_01_MP3, THE_2021012404476_MP3 }

// final audioUrlValues = EnumValues({
//     "": AudioUrl.EMPTY,
//     "2021-01-24_04476.mp3": AudioUrl.THE_2021012404476_MP3,
//     "2021-01-24_Frank_Sinatra_-_L.O.V.E._(lyrics).mp3": AudioUrl.THE_20210124_FRANK_SINATRA_L_O_V_E_LYRICS_MP3,
//     "2021-01-24_ice-cracking-01.mp3": AudioUrl.THE_20210124_ICE_CRACKING_01_MP3
// });

// enum ImageScan { EMPTY, THE_202101221_PNG, THE_20210121_TWO_COLUMN_JYY0_Z_JL4_JPG, THE_20210124_FREEGEN_PNG }

// final imageScanValues = EnumValues({
//     "": ImageScan.EMPTY,
//     "2021-01-21_two_column_JYY0zJl4.jpg": ImageScan.THE_20210121_TWO_COLUMN_JYY0_Z_JL4_JPG,
//     "2021-01-22_1.png": ImageScan.THE_202101221_PNG,
//     "2021-01-24_freegen.png": ImageScan.THE_20210124_FREEGEN_PNG
// });

// enum StepType { INFORMATIVE, QUIZ, SCANNING }

// final stepTypeValues = EnumValues({
//     "INFORMATIVE": StepType.INFORMATIVE,
//     "QUIZ": StepType.QUIZ,
//     "SCANNING": StepType.SCANNING
// });

// enum VideoUrl { EMPTY, THE_20210121_HEDGEHOG_MP4 }

// final videoUrlValues = EnumValues({
//     "": VideoUrl.EMPTY,
//     "2021-01-21_hedgehog.mp4": VideoUrl.THE_20210121_HEDGEHOG_MP4
// });

// enum TitleEn { EMPTY, TITLE_EN }

// final titleEnValues = EnumValues({
//     "": TitleEn.EMPTY,
//     "ваы": TitleEn.TITLE_EN
// });

// class Page {
//     Page({
//         this.size,
//         this.itemsOnCurrentPage,
//         this.currentPage,
//         this.totalItems,
//         this.totalPages,
//     });

//     int size;
//     int itemsOnCurrentPage;
//     int currentPage;
//     int totalItems;
//     int totalPages;

//     factory Page.fromJson(Map<String, dynamic> json) => Page(
//         size: json["size"],
//         itemsOnCurrentPage: json["itemsOnCurrentPage"],
//         currentPage: json["currentPage"],
//         totalItems: json["totalItems"],
//         totalPages: json["totalPages"],
//     );

//     Map<String, dynamic> toJson() => {
//         "size": size,
//         "itemsOnCurrentPage": itemsOnCurrentPage,
//         "currentPage": currentPage,
//         "totalItems": totalItems,
//         "totalPages": totalPages,
//     };
// }

// class EnumValues<T> {
//     Map<String, T> map;
//     Map<T, String> reverseMap;

//     EnumValues(this.map);

//     Map<T, String> get reverse {
//         if (reverseMap == null) {
//             reverseMap = map.map((k, v) => new MapEntry(v, k));
//         }
//         return reverseMap;
//     }
// }

// To parse this JSON data, do
//
//     final missions = missionsFromJson(jsonString);

import 'dart:convert';

Missions missionsFromJson(String str) => Missions.fromJson(json.decode(str));

String missionsToJson(Missions data) => json.encode(data.toJson());

class Missions {
    Missions({
        this.missions,
        this.page,
    });

    List<Mission> missions;
    Page page;

    factory Missions.fromJson(Map<String, dynamic> json) => Missions(
        missions: List<Mission>.from(json["missions"].map((x) => Mission.fromJson(x))),
        page: Page.fromJson(json["page"]),
    );

    Map<String, dynamic> toJson() => {
        "missions": List<dynamic>.from(missions.map((x) => x.toJson())),
        "page": page.toJson(),
    };
}

List<MissionProgress> missionProgressFromJson(String str) => List<MissionProgress>.from(json.decode(str).map((x) => MissionProgress.fromJson(x)));

String missionProgressToJson(List<MissionProgress> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MissionProgress {
    MissionProgress({
        this.trackStatus,
        this.missionId,
        this.subMissionProgressTracks,
    });

    String trackStatus;
    int missionId;
    List<dynamic> subMissionProgressTracks;

    factory MissionProgress.fromJson(Map<String, dynamic> json) => MissionProgress(
        trackStatus: json["trackStatus"],
        missionId: json["missionId"],
        subMissionProgressTracks: List<dynamic>.from(json["subMissionProgressTracks"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "trackStatus": trackStatus,
        "missionId": missionId,
        "subMissionProgressTracks": List<dynamic>.from(subMissionProgressTracks.map((x) => x)),
    };
}

class Mission {
    Mission({
        this.id,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.accessType,
        this.subMissionList,
        this.filters,
        this.statuses,
        this.duration,
        this.descriptionUa,
        this.descriptionRu,
        this.descriptionEn,
        this.location,
        this.position,
        this.createdDate,
        this.lastModifiedDate,
        this.currencyType,
        this.points,
        this.imageUrl,
        this.audioUrl,
        this.videoUrl,
        this.promoCode,
        this.price,
        this.priceCurrency,
        this.recommend,
        this.active,
        this.progress,
        this.distanceInMeters,
        this.isSelected,
    });

    int id;
    String titleUa;
    String titleRu;
    String titleEn;
    String accessType;
    List<SubMissionList> subMissionList;
    List<Filter> filters;
    List<Filter> statuses;
    int duration;
    String descriptionUa;
    String descriptionRu;
    String descriptionEn;
    Location location;
    int position;
    DateTime createdDate;
    DateTime lastModifiedDate;
    String currencyType;
    double points;
    String imageUrl;
    String audioUrl;
    String videoUrl;
    String promoCode;
    double price;
    String priceCurrency;
    bool recommend;
    bool active;
    String progress;
    double distanceInMeters;
    bool isSelected;

    factory Mission.fromJson(Map<String, dynamic> json) => Mission(
        id: json["id"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        accessType: json["accessType"],
        subMissionList: List<SubMissionList>.from(json["subMissionList"].map((x) => SubMissionList.fromJson(x))),
        filters: List<Filter>.from(json["filters"].map((x) => Filter.fromJson(x))),
        statuses: List<Filter>.from(json["statuses"].map((x) => Filter.fromJson(x))),
        duration: json["duration"],
        descriptionUa: json["descriptionUa"],
        descriptionRu: json["descriptionRu"],
        descriptionEn: json["descriptionEn"],
        location: Location.fromJson(json["location"]),
        position: json["position"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        currencyType: json["currencyType"],
        points: json["points"] == null ? null : json["points"],
        imageUrl: json["imageUrl"],
        audioUrl: json["audioUrl"],
        videoUrl: json["videoUrl"],
        promoCode: json["promoCode"],
        price: json["price"],
        priceCurrency: json["priceCurrency"],
        recommend: json["recommend"],
        active: json["active"],
        distanceInMeters: json["distanceInKilometers"], 
        isSelected: json["isSelected"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "accessType": accessType,
        "subMissionList": List<dynamic>.from(subMissionList.map((x) => x.toJson())),
        "filters": List<dynamic>.from(filters.map((x) => x.toJson())),
        "statuses": List<dynamic>.from(statuses.map((x) => x.toJson())),
        "duration": duration,
        "descriptionUa": descriptionUa,
        "descriptionRu": descriptionRu,
        "descriptionEn": descriptionEn,
        "location": location.toJson(),
        "position": position,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "currencyType": currencyType,
        "points": points == null ? null : points,
        "imageUrl": imageUrl,
        "audioUrl": audioUrl,
        "videoUrl": videoUrl,
        "promoCode": promoCode,
        "price": price,
        "priceCurrency": priceCurrency,
        "recommend": recommend,
        "active": active,
        "distanceInMeters": distanceInMeters, 
        "isSelected": isSelected,
    };
}

class Filter {
    Filter({
        this.id,
        this.active,
        this.deleted,
        this.tagsUa,
        this.position,
        this.color,
        this.cost,
        this.address,
        this.publicationType,
        this.author,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.pageType,
        this.titleShortUa,
        this.titleShortRu,
        this.titleShortEn,
        this.contentUa,
        this.contentRu,
        this.contentEn,
        this.contentShortUa,
        this.contentShortRu,
        this.contentShortEn,
        this.publicationDate,
        this.expireDate,
        this.img,
    });

    int id;
    bool active;
    bool deleted;
    dynamic tagsUa;
    int position;
    String color;
    double cost;
    String address;
    PublicationType publicationType;
    String author;
    String titleUa;
    String titleRu;
    String titleEn;
    dynamic pageType;
    dynamic titleShortUa;
    dynamic titleShortRu;
    dynamic titleShortEn;
    dynamic contentUa;
    dynamic contentRu;
    dynamic contentEn;
    dynamic contentShortUa;
    dynamic contentShortRu;
    dynamic contentShortEn;
    DateTime publicationDate;
    DateTime expireDate;
    Img img;

    factory Filter.fromJson(Map<String, dynamic> json) => Filter(
        id: json["id"],
        active: json["active"],
        deleted: json["deleted"],
        tagsUa: json["tagsUa"],
        position: json["position"],
        color: json["color"],
        cost: json["cost"] == null ? null : json["cost"],
        address: json["address"] == null ? null : json["address"],
        publicationType: publicationTypeValues.map[json["publicationType"]],
        author: json["author"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"] == null ? null : json["titleRu"],
        titleEn: json["titleEn"] == null ? null : json["titleEn"],
        pageType: json["page_type"],
        titleShortUa: json["titleShortUa"],
        titleShortRu: json["titleShortRu"],
        titleShortEn: json["titleShortEn"],
        contentUa: json["contentUa"],
        contentRu: json["contentRu"],
        contentEn: json["contentEn"],
        contentShortUa: json["contentShortUa"],
        contentShortRu: json["contentShortRU"],
        contentShortEn: json["contentShortEn"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        expireDate: json["expireDate"] == null ? null : DateTime.parse(json["expireDate"]),
        img: imgValues.map[json["img"]],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "active": active,
        "deleted": deleted,
        "tagsUa": tagsUa,
        "position": position,
        "color": color,
        "cost": cost == null ? null : cost,
        "address": address == null ? null : address,
        "publicationType": publicationTypeValues.reverse[publicationType],
        "author": author,
        "titleUa": titleUa,
        "titleRu": titleRu == null ? null : titleRu,
        "titleEn": titleEn == null ? null : titleEn,
        "page_type": pageType,
        "titleShortUa": titleShortUa,
        "titleShortRu": titleShortRu,
        "titleShortEn": titleShortEn,
        "contentUa": contentUa,
        "contentRu": contentRu,
        "contentEn": contentEn,
        "contentShortUa": contentShortUa,
        "contentShortRU": contentShortRu,
        "contentShortEn": contentShortEn,
        "publicationDate": publicationDate.toIso8601String(),
        "expireDate": expireDate == null ? null : expireDate.toIso8601String(),
        "img": imgValues.reverse[img],
    };
}

enum Img { DEFAULT_PNG }

final imgValues = EnumValues({
    "default.png": Img.DEFAULT_PNG
});

enum PublicationType { FILTERS, STATUSES }

final publicationTypeValues = EnumValues({
    "filters": PublicationType.FILTERS,
    "statuses": PublicationType.STATUSES
});

class Location {
    Location({
        this.id,
        this.latitude,
        this.longitude,
        this.active,
    });

    int id;
    String latitude;
    String longitude;
    bool active;

    factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "active": active,
    };
}

class SubMissionList {
    SubMissionList({
        this.autoCheckIn,
        this.activationRadius,
        this.id,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.createdDate,
        this.lastModifiedDate,
        this.location,
        this.imageUrl,
        this.position,
        this.stepList,
        this.currencyType,
        this.points,
        this.active,
    });

    bool autoCheckIn;
    int activationRadius;
    int id;
    String titleUa;
    String titleRu;
    String titleEn;
    DateTime createdDate;
    DateTime lastModifiedDate;
    Location location;
    String imageUrl;
    int position;
    List<StepList> stepList;
    String currencyType;
    double points;
    bool active;

    factory SubMissionList.fromJson(Map<String, dynamic> json) => SubMissionList(
        autoCheckIn: json["autoCheckIn"],
        activationRadius: json["activationRadius"],
        id: json["id"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        location: json["location"] == null ? null : Location.fromJson(json["location"]),
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
        position: json["position"],
        stepList: List<StepList>.from(json["stepList"].map((x) => StepList.fromJson(x))),
        currencyType: json["currencyType"],
        points: json["points"] == null ? null : json["points"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "autoCheckIn": autoCheckIn,
        "activationRadius": activationRadius,
        "id": id,
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "location": location == null ? null : location.toJson(),
        "imageUrl": imageUrl == null ? null : imageUrl,
        "position": position,
        "stepList": List<dynamic>.from(stepList.map((x) => x.toJson())),
        "currencyType": currencyType,
        "points": points == null ? null : points,
        "active": active,
    };
}

class StepList {
    StepList({
        this.id,
        this.stepType,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.createdDate,
        this.lastModifiedDate,
        this.location,
        this.descriptionUa,
        this.descriptionRu,
        this.descriptionEn,
        this.imageUrl,
        this.audioUrl,
        this.videoUrl,
        this.position,
        this.currencyType,
        this.points,
        this.activationRadius,
        this.correctAnswer,
        this.currencyTypeWrongAnswer,
        this.pointsWrongAnswer,
        this.imageScan,
        this.scanType,
        this.canBeSkipped,
        this.active,
        this.modelList,
        this.geolocationList,
    });

    int id;
    StepType stepType;
    String titleUa;
    String titleRu;
    String titleEn;
    DateTime createdDate;
    DateTime lastModifiedDate;
    dynamic location;
    String descriptionUa;
    String descriptionRu;
    String descriptionEn;
    String imageUrl;
    AudioUrl audioUrl;
    VideoUrl videoUrl;
    int position;
    String currencyType;
    double points;
    dynamic activationRadius;
    String correctAnswer;
    String currencyTypeWrongAnswer;
    double pointsWrongAnswer;
    ImageScan imageScan;
    String scanType;
    bool canBeSkipped;
    bool active;
    List<ModelList> modelList;
    List<dynamic> geolocationList;

    factory StepList.fromJson(Map<String, dynamic> json) => StepList(
        id: json["id"],
        stepType: stepTypeValues.map[json["stepType"]],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        location: json["location"],
        descriptionUa: json["descriptionUa"],
        descriptionRu: json["descriptionRu"],
        descriptionEn: json["descriptionEn"],
        imageUrl: json["imageUrl"],
        audioUrl: audioUrlValues.map[json["audioUrl"]],
        videoUrl: videoUrlValues.map[json["videoUrl"]],
        position: json["position"],
        currencyType: json["currencyType"],
        points: json["points"] == null ? null : json["points"],
        activationRadius: json["activationRadius"],
        correctAnswer: json["correctAnswer"] == null ? null : json["correctAnswer"],
        currencyTypeWrongAnswer: json["currencyTypeWrongAnswer"],
        pointsWrongAnswer: json["pointsWrongAnswer"] == null ? null : json["pointsWrongAnswer"],
        imageScan: imageScanValues.map[json["imageScan"]],
        scanType: json["scanType"],
        canBeSkipped: json["canBeSkipped"],
        active: json["active"],
        modelList: List<ModelList>.from(json["modelList"].map((x) => ModelList.fromJson(x))),
        geolocationList: List<dynamic>.from(json["geolocationList"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "stepType": stepTypeValues.reverse[stepType],
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "location": location,
        "descriptionUa": descriptionUa,
        "descriptionRu": descriptionRu,
        "descriptionEn": descriptionEn,
        "imageUrl": imageUrl,
        "audioUrl": audioUrlValues.reverse[audioUrl],
        "videoUrl": videoUrlValues.reverse[videoUrl],
        "position": position,
        "currencyType": currencyType,
        "points": points == null ? null : points,
        "activationRadius": activationRadius,
        "correctAnswer": correctAnswer == null ? null : correctAnswer,
        "currencyTypeWrongAnswer": currencyTypeWrongAnswer,
        "pointsWrongAnswer": pointsWrongAnswer == null ? null : pointsWrongAnswer,
        "imageScan": imageScanValues.reverse[imageScan],
        "scanType": scanType,
        "canBeSkipped": canBeSkipped,
        "active": active,
        "modelList": List<dynamic>.from(modelList.map((x) => x.toJson())),
        "geolocationList": List<dynamic>.from(geolocationList.map((x) => x)),
    };
}

enum AudioUrl { EMPTY, THE_20210212202101297134361460533538094_ZPDM9_P6_B_MP3, THE_20210124_ICE_CRACKING_01_MP3, THE_20210124_FRANK_SINATRA_L_O_V_E_LYRICS_MP3, THE_2021012404476_MP3, THE_2021021203_ROCK_HARD_RIDE_FREE_MP3, THE_20210212_QUIZ_AUDIO10_MP3, THE_1614184864379_MAID_WITH_THE_FLAXEN_HAIR_MP3 }

final audioUrlValues = EnumValues({
    "": AudioUrl.EMPTY,
    "1614184864379_Maid with the Flaxen Hair.mp3": AudioUrl.THE_1614184864379_MAID_WITH_THE_FLAXEN_HAIR_MP3,
    "2021-01-24_04476.mp3": AudioUrl.THE_2021012404476_MP3,
    "2021-01-24_Frank_Sinatra_-_L.O.V.E._(lyrics).mp3": AudioUrl.THE_20210124_FRANK_SINATRA_L_O_V_E_LYRICS_MP3,
    "2021-01-24_ice-cracking-01.mp3": AudioUrl.THE_20210124_ICE_CRACKING_01_MP3,
    "2021-02-12_03. Rock Hard Ride Free.mp3": AudioUrl.THE_2021021203_ROCK_HARD_RIDE_FREE_MP3,
    "2021-02-12_20210129-7134361460533538094_zpdm9P6b.mp3": AudioUrl.THE_20210212202101297134361460533538094_ZPDM9_P6_B_MP3,
    "2021-02-12_quiz_audio10.mp3": AudioUrl.THE_20210212_QUIZ_AUDIO10_MP3
});

enum ImageScan { EMPTY, THE_20210124_FREEGEN_PNG }

final imageScanValues = EnumValues({
    "": ImageScan.EMPTY,
    "2021-01-24_freegen.png": ImageScan.THE_20210124_FREEGEN_PNG
});

class ModelList {
    ModelList({
        this.id,
        this.modelUrl,
        this.fileName,
        this.createdDate,
        this.showInMainCamera,
        this.active,
    });

    int id;
    dynamic modelUrl;
    String fileName;
    DateTime createdDate;
    dynamic showInMainCamera;
    bool active;

    factory ModelList.fromJson(Map<String, dynamic> json) => ModelList(
        id: json["id"],
        modelUrl: json["modelUrl"],
        fileName: json["fileName"],
        createdDate: DateTime.parse(json["createdDate"]),
        showInMainCamera: json["showInMainCamera"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "modelUrl": modelUrl,
        "fileName": fileName,
        "createdDate": createdDate.toIso8601String(),
        "showInMainCamera": showInMainCamera,
        "active": active,
    };
}

enum StepType { INFORMATIVE, SCANNING, SELFIE, QUIZ }

final stepTypeValues = EnumValues({
    "INFORMATIVE": StepType.INFORMATIVE,
    "QUIZ": StepType.QUIZ,
    "SCANNING": StepType.SCANNING,
    "SELFIE": StepType.SELFIE
});

enum VideoUrl { EMPTY, THE_20210214360_MP4, THE_20210214180_MP4, THE_20210214_OLI_MP4, THE_20210214_CITY_SWELL_MP4, THE_20210214_VVIDEO_X_PARK_MP4, THE_20210214_VVIDEO_AUTO_RACE_MP4 }

final videoUrlValues = EnumValues({
    "": VideoUrl.EMPTY,
    "2021-02-14_180.mp4": VideoUrl.THE_20210214180_MP4,
    "2021-02-14_360.mp4": VideoUrl.THE_20210214360_MP4,
    "2021-02-14_City_Swell.mp4": VideoUrl.THE_20210214_CITY_SWELL_MP4,
    "2021-02-14_oli.mp4": VideoUrl.THE_20210214_OLI_MP4,
    "2021-02-14_vvideo_AutoRace.mp4": VideoUrl.THE_20210214_VVIDEO_AUTO_RACE_MP4,
    "2021-02-14_vvideo_XPark.mp4": VideoUrl.THE_20210214_VVIDEO_X_PARK_MP4
});

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
