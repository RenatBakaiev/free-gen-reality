// To parse this JSON data, do
//
//     final partners = partnersFromJson(jsonString);

import 'dart:convert';

Partners partnersFromJson(String str) => Partners.fromJson(json.decode(str));

String partnersToJson(Partners data) => json.encode(data.toJson());

class Partners {
    Partners({
        this.publication,
        this.page,
    });

    List<Publication> publication;
    Page page;

    factory Partners.fromJson(Map<String, dynamic> json) => Partners(
        publication: List<Publication>.from(json["publication"].map((x) => Publication.fromJson(x))),
        page: Page.fromJson(json["page"]),
    );

    Map<String, dynamic> toJson() => {
        "publication": List<dynamic>.from(publication.map((x) => x.toJson())),
        "page": page.toJson(),
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class Publication {
    Publication({
        this.id,
        this.tags,
        this.author,
        this.active,
        this.cost,
        this.address,
        this.img,
        this.position,
        this.color,
        this.publicationType,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.pageType,
        this.titleShortUa,
        this.titleShortRu,
        this.titleShortEn,
        this.contentUa,
        this.contentRu,
        this.contentEn,
        this.contentShortUa,
        this.contentShortRu,
        this.contentShortEn,
        this.publicationDate,
        this.expireDate,
    });

    int id;
    dynamic tags;
    String author;
    bool active;
    double cost;
    String address;
    String img;
    int position;
    String color;
    PublicationType publicationType;
    String titleUa;
    dynamic titleRu;
    dynamic titleEn;
    dynamic pageType;
    dynamic titleShortUa;
    dynamic titleShortRu;
    dynamic titleShortEn;
    String contentUa;
    dynamic contentRu;
    dynamic contentEn;
    dynamic contentShortUa;
    dynamic contentShortRu;
    dynamic contentShortEn;
    DateTime publicationDate;
    DateTime expireDate;

    factory Publication.fromJson(Map<String, dynamic> json) => Publication(
        id: json["id"],
        tags: json["tags"],
        author: json["author"],
        active: json["active"],
        cost: json["cost"] == null ? null : json["cost"],
        address: json["address"] == null ? null : json["address"],
        img: json["img"],
        position: json["position"],
        color: json["color"],
        publicationType: publicationTypeValues.map[json["publicationType"]],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        pageType: json["page_type"],
        titleShortUa: json["titleShortUa"],
        titleShortRu: json["titleShortRu"],
        titleShortEn: json["titleShortEn"],
        contentUa: json["contentUa"],
        contentRu: json["contentRu"],
        contentEn: json["contentEn"],
        contentShortUa: json["contentShortUa"],
        contentShortRu: json["contentShortRU"],
        contentShortEn: json["contentShortEn"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        expireDate: DateTime.parse(json["expireDate"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "tags": tags,
        "author": author,
        "active": active,
        "cost": cost == null ? null : cost,
        "address": address == null ? null : address,
        "img": img,
        "position": position,
        "color": color,
        "publicationType": publicationTypeValues.reverse[publicationType],
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "page_type": pageType,
        "titleShortUa": titleShortUa,
        "titleShortRu": titleShortRu,
        "titleShortEn": titleShortEn,
        "contentUa": contentUa,
        "contentRu": contentRu,
        "contentEn": contentEn,
        "contentShortUa": contentShortUa,
        "contentShortRU": contentShortRu,
        "contentShortEn": contentShortEn,
        "publicationDate": publicationDate.toIso8601String(),
        "expireDate": expireDate.toIso8601String(),
    };
}

enum PublicationType { PARTNERS }

final publicationTypeValues = EnumValues({
    "partners": PublicationType.PARTNERS
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
