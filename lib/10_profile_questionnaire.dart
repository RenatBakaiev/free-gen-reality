import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/10_profile_rewards.dart';
// import 'package:flutter_free_gen_reality/api.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import '10_profile_json.dart';
// import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class ProfileQuestionnaire extends StatefulWidget {
  @override
  _ProfileQuestionnaireState createState() => _ProfileQuestionnaireState();
}

class _ProfileQuestionnaireState extends State<ProfileQuestionnaire> {
  var _blankFocusNode = new FocusNode();

  // final format = DateFormat("yyyy-MM-dd HH:mm");
  final format = DateFormat("yyyy-MM-dd");

  bool isQuestionnaireFilled = true;
  bool isNotificationsSelected = false;
  bool isManSelected = false;
  bool isWomanSelected = false;
  bool isSexSelected = false;
  bool isLoading = false;

  String sex = 'Вкажіть вашу стать';

  changeSex() {
    if (isManSelected) {
      setState(() {
        sex = 'Чоловіча';
      });
    }
    if (isWomanSelected) {
      setState(() {
        sex = 'Жіноча';
      });
    }
  }

  final formKey = GlobalKey<FormState>();
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController secondNameTextEditingController =
      new TextEditingController();
  TextEditingController nickNameTextEditingController =
      new TextEditingController();
  TextEditingController birthdayTextEditingController =
      new TextEditingController();
  TextEditingController cityTextEditingController = new TextEditingController();
  TextEditingController countryTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  // TextEditingController phoneTextEditingController =
  //     new TextEditingController();

  // var controller = new MaskedTextController(mask: '+3(8063)499-31-20');
  var controller = new MaskedTextController(mask: '+380(00) 000-00-00');

  TextEditingController dateCtl = TextEditingController();

  bool makeErrorSex = false;
  // void _add50Tus() {
  //   // if (!isManSelected || !isWomanSelected) {
  //   //   setState(() {
  //   //     isSexSelected = false;
  //   //   });
  //   // }
  //   // else {
  //   //   setState(() {
  //   //     isSexSelected = true;
  //   //   });
  //   // }
  //   // if (!isSexSelected) {
  //   //   setState(() {
  //   //     makeErrorSex = true;
  //   //   });
  //   // }

  //   if (formKey.currentState.validate()) {
  //   // if (formKey.currentState.validate() && (isManSelected || isWomanSelected)) {
  //     if (!isSexSelected) {
  //       isSexSelected = true;
  //     }
  //     print('Form is filled');
  //     showYouReceiveRewardWindow();
  //     firstNameTextEditingController.clear();
  //     secondNameTextEditingController.clear();
  //     nickNameTextEditingController.clear();
  //     birthdayTextEditingController.clear();
  //     cityTextEditingController.clear();
  //     countryTextEditingController.clear();
  //     emailTextEditingController.clear();
  //     controller.clear();
  //   }
  // }
  String firstName = '';
  String lastName = '';
  String nickName = '';
  String userEmail = '';
  String birthday = '';
  String city = '';
  String country = '';
  String phone = '';
  String gender = '';
  int id;

  getUserData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    print(email);
    setState(() {
      userEmail = email;
    });
    print(token);
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';
    print(url);

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
        print(response.body);

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        print(responseJson);

        email = responseJson["email"];
        id = responseJson["id"];
        print(email);

        setState(() {
          if (responseJson["firstName"] != null &&
              responseJson["firstName"] != "") {
            var firstNameConverted =
                utf8.decode(responseJson["firstName"].runes.toList());
            firstName = firstNameConverted;
            firstNameTextEditingController.text = firstName;
          }
          if (responseJson["lastName"] != null &&
              responseJson["lastName"] != "") {
            var lastNameConverted =
                utf8.decode(responseJson["lastName"].runes.toList());
            lastName = lastNameConverted;
            secondNameTextEditingController.text = lastName;
          }
          if (responseJson["username"] != null &&
              responseJson["username"] != "") {
            var nickNameConverted =
                utf8.decode(responseJson["username"].runes.toList());
            nickName = nickNameConverted;
            nickNameTextEditingController.text = nickName;
          }
          if (responseJson["birthDate"] != null &&
              responseJson["birthDate"] != "") {
            birthday = responseJson["birthDate"].substring(0, 10);
            birthdayTextEditingController.text = birthday;
          }
          if (responseJson["phone"] != null && responseJson["phone"] != "") {
            phone = responseJson["phone"];
            controller.text = phone;
          }
          if (responseJson["country"] != null &&
              responseJson["country"] != "") {
            var countryConverted =
                utf8.decode(responseJson["country"].runes.toList());
            country = countryConverted;
            countryTextEditingController.text = country;
          }
          if (responseJson["city"] != null && responseJson["city"] != "") {
            var cityConverted =
                utf8.decode(responseJson["city"].runes.toList());
            city = cityConverted;
            cityTextEditingController.text = city;
          }
          if (responseJson["gender"] != null && responseJson["gender"] != "") {
            var genderConverted =
                utf8.decode(responseJson["gender"].runes.toList());
            gender = genderConverted;
            isSexSelected = true;
            if (gender == "Чоловіча") {
              sex = 'Чоловіча';
              isManSelected = true;
            } else {
              sex = 'Жіноча';
              isWomanSelected = true;
            }
          }
          if (responseJson["notifications"] == true) {
            isNotificationsSelected = true;
          } else {
            isNotificationsSelected = false;
          }
          isLoading = false;
        });
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  Future<User> add50Tus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    setState(() {
      isLoading = true;
    });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "deposit 15 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // showYouReceiveRewardWindow();
      setState(() {
        isLoading = false;
      });
      print(response.body);
    } else {
      setState(() {
        isLoading = false;
      });
      throw Exception('Failed to update User.');
    }
  }

  Future<User> editUser() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    String url = 'http://generation-admin.ehub.com.ua/api/user/update';
    print(id);
    print(url);
    print(token);

    var firstName = firstNameTextEditingController.text;
    var lastName = secondNameTextEditingController.text;
    var nickName = nickNameTextEditingController.text;
    var birthday = birthdayTextEditingController.text + "T00:00:00.000+00:00";
    var city = cityTextEditingController.text;
    var country = countryTextEditingController.text;
    var phone = controller.text;

    final http.Response response = await http.put(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "username": nickName,
        "birthDate": birthday,
        "city": city,
        "country": country,
        "phone": phone,
        "gender": isManSelected ? "Чоловіча" : "Жіноча",
        "notifications": isNotificationsSelected ? true : false,
      }),
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      setState(() {
        isLoading = false;
      });

      // Navigator.of(context).pop();
      showYouReceiveRewardWindow();
      print(response.body);
    } else {
      setState(() {
        isLoading = false;
      });
      throw Exception('Failed to edit User.');
    }
  }

  bool isApiCallProcess = false;

  void _saveData() {
    if (!isSexSelected) {
      setState(() {
        makeErrorSex = true;
      });
    }
    if (formKey.currentState.validate() && (isManSelected || isWomanSelected)) {
      if (!isSexSelected) {
        isSexSelected = true;
      }

      editUser();
    }
  }

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  showYouReceiveRewardWindow() {
    setState(() {
      allRewards[0].quantity = allRewards[0].quantity + 50;
    });
    showDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width - 80,
              height: 230,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 10,
                              ),
                              child: Text(
                                'Вітаємо!',
                                style: TextStyle(
                                  color: Hexcolor('#58B12D'),
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900,
                                  letterSpacing: 1.089,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Ви отримали  ',
                                  style: TextStyle(
                                    color: Hexcolor('#000000'),
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  '50 TUS',
                                  style: TextStyle(
                                    color: Hexcolor('#58B12D'),
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 10,
                              ),
                              child: Image.asset(
                                'assets/fg_images/10_profile_user_param_icon_foint2.png',
                                height: 50,
                              ),
                            ),
                          ])),
                  Container(
                    margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Далі",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: Hexcolor('#EB5C18'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              Navigator.pushNamed(
                                  context, '/5_myBottomBar.dart');
                              final SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();

                              var userId = sharedPreferences.get("id");
                              var res = sharedPreferences
                                  .get('isQuestionaireDone$userId');
                              if (res == '' || res == null) {
                                add50Tus(50, 'DEPOSIT');
                                sharedPreferences.setBool(
                                    'isQuestionaireDone$userId', true);
                              }
                              // Navigator.of(context).pop();
                              // Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  chooseSex() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 235,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть стать",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isWomanSelected = false;
                                      isManSelected = true;
                                      makeErrorSex = false;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isManSelected
                                            ? 'assets/fg_images/10_profile_questionaire_sex_selected.png'
                                            : 'assets/fg_images/10_profile_questionaire_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Чоловіча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isWomanSelected = true;
                                      isManSelected = false;
                                      makeErrorSex = false;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isWomanSelected
                                            ? 'assets/fg_images/10_profile_questionaire_sex_selected.png'
                                            : 'assets/fg_images/10_profile_questionaire_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Жіноча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#EB5C18'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      if ((isManSelected || isWomanSelected) ||
                                          (isManSelected && isWomanSelected)) {
                                        setState(() {
                                          isSexSelected = true;
                                        });
                                        print('sex is selected!');
                                        Navigator.of(context).pop();
                                      }
                                      changeSex();
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Hexcolor('#7D5AC2'),
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Hexcolor('#FE6802'),
                  ),
                ),
              )
            : GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(_blankFocusNode);
                },
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#FFFFFF'),
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Hexcolor('#7D5AC2'),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Hexcolor('#7D5AC2'),
                              ),
                              alignment: Alignment.bottomLeft,
                              margin: EdgeInsets.only(
                                top: 50,
                                // left: 15,
                                // right: 15,
                                bottom: 22,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: SizedBox(
                                      // height: 55,
                                      // width: 40,
                                      child: IconButton(
                                        icon: Image.asset(
                                          'assets/fg_images/6_home_search_back.png',
                                          width: 10,
                                          height: 19,
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Анкета',
                                    style: TextStyle(
                                        fontSize: 33,
                                        color: Colors.white,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w900),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: Expanded(
                                child: SingleChildScrollView(
                                    physics: BouncingScrollPhysics(),
                                    child: Container(
                                      margin: EdgeInsets.only(
                                        top: 15,
                                        left: 30,
                                        right: 30,
                                      ),
                                      child: Column(children: [
                                        // Row(
                                        //   children: [
                                        //     RaisedButton(
                                        //       onPressed: () {
                                        //         changeTus(15.0, "DEPOSIT");
                                        //       },
                                        //       child: Text('+'),
                                        //     ),
                                        //     SizedBox(width: 10),
                                        //     RaisedButton(
                                        //       onPressed: () {
                                        //         changeTus(5.0, "WITHDRAW");
                                        //       },
                                        //       child: Text('-'),
                                        //     ),
                                        //   ],
                                        // ),
                                        Container(
                                            alignment: Alignment.centerLeft,
                                            margin: EdgeInsets.only(bottom: 15),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'Заповни анкету, отримай',
                                                  style: TextStyle(
                                                      color:
                                                          Hexcolor('#484848'),
                                                      fontSize: 18,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      '50 TUS',
                                                      style: TextStyle(
                                                          color: Hexcolor(
                                                              '#FE6802'),
                                                          fontSize: 18,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                    Text(
                                                      ' і візьми участь у лотереї!',
                                                      style: TextStyle(
                                                          color: Hexcolor(
                                                              '#484848'),
                                                          fontSize: 18,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            )),
                                        Form(
                                          key: formKey,
                                          child: Column(
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Ім\'я",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть ім\'я";
                                                    },
                                                    controller:
                                                        firstNameTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText: "Введіть ім\'я",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Прізвище",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть прізвище";
                                                    },
                                                    controller:
                                                        secondNameTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText:
                                                          "Введіть прізвище",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Нікнейм",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть нікнейм";
                                                    },
                                                    controller:
                                                        nickNameTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText:
                                                          "Введіть нікнейм",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              // Row(
                                              //   mainAxisAlignment:
                                              //       MainAxisAlignment.spaceBetween,
                                              //   children: [
                                              //     Container(
                                              //       decoration: BoxDecoration(
                                              //         color: Hexcolor('#F2F2F2'),
                                              //         border: Border.all(
                                              //           color: isSexSelected ||
                                              //                   isWomanSelected ||
                                              //                   isManSelected
                                              //               ? Hexcolor('#666666')
                                              //               : Colors.red,
                                              //           width: 3,
                                              //         ),
                                              //         borderRadius: BorderRadius.only(
                                              //             topLeft: Radius.circular(14),
                                              //             topRight: Radius.circular(14),
                                              //             bottomLeft: Radius.circular(14),
                                              //             bottomRight:
                                              //                 Radius.circular(14)),
                                              //       ),
                                              //       alignment: Alignment.centerLeft,
                                              //       width: MediaQuery.of(context)
                                              //               .size
                                              //               .width -
                                              //           184,
                                              //       height: 47,
                                              //       child: Center(
                                              //         child: Text(
                                              //           'Пол',
                                              //           style: TextStyle(
                                              //               color: Hexcolor('#666666'),
                                              //               fontSize: 17,
                                              //               fontFamily: 'Arial',
                                              //               fontWeight: FontWeight.w600),
                                              //           textAlign: TextAlign.center,
                                              //         ),
                                              //       ),
                                              //     ),
                                              //     GestureDetector(
                                              //       behavior: HitTestBehavior.opaque,
                                              //       onTap: () {
                                              //         setState(() {
                                              //           isManSelected = true;
                                              //           if (isWomanSelected) {
                                              //             isWomanSelected = false;
                                              //           }
                                              //         });
                                              //       },
                                              //       child: Container(
                                              //         decoration: BoxDecoration(
                                              //           color: Hexcolor('#F2F2F2'),
                                              //           border: Border.all(
                                              //             color: isSexSelected ||
                                              //                     isWomanSelected ||
                                              //                     isManSelected
                                              //                 ? Hexcolor('#666666')
                                              //                 : Colors.red,
                                              //             width: 3,
                                              //           ),
                                              //           borderRadius: BorderRadius.only(
                                              //               topLeft: Radius.circular(14),
                                              //               topRight: Radius.circular(14),
                                              //               bottomLeft:
                                              //                   Radius.circular(14),
                                              //               bottomRight:
                                              //                   Radius.circular(14)),
                                              //         ),
                                              //         alignment: Alignment.centerLeft,
                                              //         width: 47,
                                              //         height: 47,
                                              //         child: isManSelected
                                              //             ? Center(
                                              //                 child: Image.asset(
                                              //                 'assets/fg_images/6_home_filter_location_selected.png',
                                              //                 width: 30,
                                              //                 height: 30,
                                              //               ))
                                              //             : Center(
                                              //                 child: Text(
                                              //                   'М',
                                              //                   style: TextStyle(
                                              //                       color: Hexcolor(
                                              //                           '#666666'),
                                              //                       fontSize: 17,
                                              //                       fontFamily: 'Arial',
                                              //                       fontWeight:
                                              //                           FontWeight.w600),
                                              //                   textAlign:
                                              //                       TextAlign.center,
                                              //                 ),
                                              //               ),
                                              //       ),
                                              //     ),
                                              //     GestureDetector(
                                              //       behavior: HitTestBehavior.opaque,
                                              //       onTap: () {
                                              //         setState(() {
                                              //           isWomanSelected = true;
                                              //           if (isManSelected) {
                                              //             isManSelected = false;
                                              //           }
                                              //         });
                                              //       },
                                              //       child: Container(
                                              //         decoration: BoxDecoration(
                                              //           color: Hexcolor('#F2F2F2'),
                                              //           border: Border.all(
                                              //             color: isSexSelected ||
                                              //                     isWomanSelected ||
                                              //                     isManSelected
                                              //                 ? Hexcolor('#666666')
                                              //                 : Colors.red,
                                              //             width: 3,
                                              //           ),
                                              //           borderRadius: BorderRadius.only(
                                              //               topLeft: Radius.circular(14),
                                              //               topRight: Radius.circular(14),
                                              //               bottomLeft:
                                              //                   Radius.circular(14),
                                              //               bottomRight:
                                              //                   Radius.circular(14)),
                                              //         ),
                                              //         alignment: Alignment.centerLeft,
                                              //         width: 47,
                                              //         height: 47,
                                              //         child: isWomanSelected
                                              //             ? Center(
                                              //                 child: Image.asset(
                                              //                 'assets/fg_images/6_home_filter_location_selected.png',
                                              //                 width: 30,
                                              //                 height: 30,
                                              //               ))
                                              //             : Center(
                                              //                 child: Text(
                                              //                   'Ж',
                                              //                   style: TextStyle(
                                              //                       color: Hexcolor(
                                              //                           '#666666'),
                                              //                       fontSize: 17,
                                              //                       fontFamily: 'Arial',
                                              //                       fontWeight:
                                              //                           FontWeight.w600),
                                              //                   textAlign:
                                              //                       TextAlign.center,
                                              //                 ),
                                              //               ),
                                              //       ),
                                              //     ),
                                              //   ],
                                              // ),
                                              // isSexSelected ||
                                              //         isManSelected ||
                                              //         isWomanSelected
                                              //     ? SizedBox(
                                              //         height: 15,
                                              //       )
                                              //     : Container(
                                              //         margin: EdgeInsets.only(
                                              //             bottom: 15, top: 7),
                                              //         alignment: Alignment.centerLeft,
                                              //         child: Text(
                                              //           'Пожалуйста введите пол',
                                              //           style: TextStyle(
                                              //             color: Colors.red[700],
                                              //             fontSize: 12,
                                              //           ),
                                              //         ),
                                              //       ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Дата народження",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  // DateTimeField(
                                                  //   format: format,
                                                  //   onShowPicker: (context,
                                                  //       currentValue) async {
                                                  //     final date =
                                                  //         await showDatePicker(
                                                  //       context: context,
                                                  //       firstDate:
                                                  //           DateTime(1900),
                                                  //       initialDate:
                                                  //           DateTime.now(),
                                                  //       lastDate:
                                                  //           DateTime(2100),
                                                  //       builder: (context,
                                                  //               child) =>
                                                  //           Localizations
                                                  //               .override(
                                                  //         context: context,
                                                  //         // locale: Locale('zh'),
                                                  //         child: child,
                                                  //       ),
                                                  //     );
                                                  //     return date;
                                                  // if (date != null) {
                                                  //   final time = await showTimePicker(
                                                  //     context: context,
                                                  //     initialTime:
                                                  //         TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                                                  //     builder: (context, child) => Localizations.override(
                                                  //       context: context,
                                                  //       // locale: Locale('zh'),
                                                  //       child: child,
                                                  //     ),
                                                  //   );
                                                  //   return DateTimeField.combine(date, time);
                                                  // } else {
                                                  //   return currentValue;
                                                  // }
                                                  // },
                                                  // ),
                                                  // TextFormField(
                                                  //   controller: dateCtl,
                                                  //   decoration: InputDecoration(
                                                  //     //  labelText: "Date of birth",
                                                  //     hintText:
                                                  //         "Ex. Insert your dob",
                                                  //   ),
                                                  //   onTap: () async {
                                                  //     DateTime date =
                                                  //         DateTime(1900);
                                                  //     FocusScope.of(context)
                                                  //         .requestFocus(
                                                  //             new FocusNode());

                                                  //     date =
                                                  //         await showDatePicker(
                                                  //             context: context,
                                                  //             initialDate:
                                                  //                 DateTime
                                                  //                     .now(),
                                                  //             firstDate:
                                                  //                 DateTime(
                                                  //                     1900),
                                                  //             lastDate:
                                                  //                 DateTime(
                                                  //                     2100));

                                                  //     dateCtl.text =
                                                  //         date.toString();
                                                  //   },
                                                  // ),
                                                  Stack(
                                                    overflow: Overflow.visible,
                                                    children: [
                                                      TextFormField(
                                                        onChanged: (val) {},
                                                        validator: (value) {
                                                          return value.length >
                                                                  0
                                                              ? null
                                                              : "Будь-ласка введіть дату народження";
                                                        },
                                                        onTap: () async {
                                                          DateTime date =
                                                              DateTime(1900);
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  new FocusNode());

                                                          date =
                                                              await showDatePicker(
                                                                  context:
                                                                      context,
                                                                  initialDate:
                                                                      DateTime
                                                                          .now(),
                                                                  firstDate:
                                                                      DateTime(
                                                                          1900),
                                                                  lastDate:
                                                                      DateTime(
                                                                          2100));

                                                          // birthdayTextEditingController
                                                          //         .text = date.toIso8601String();
                                                          birthdayTextEditingController
                                                                  .text =
                                                              "${date.year.toString()}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
                                                        },
                                                        controller:
                                                            birthdayTextEditingController,
                                                        textAlign:
                                                            TextAlign.left,
                                                        decoration:
                                                            InputDecoration(
                                                          hintText:
                                                              "Введіть дату народження",
                                                          contentPadding:
                                                              new EdgeInsets
                                                                  .only(
                                                            left: 20,
                                                            top: 21.5,
                                                            bottom: 21.5,
                                                          ),
                                                          hintStyle: TextStyle(
                                                            color: Hexcolor(
                                                                '#D3D3D3'),
                                                            fontSize: 17,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontFamily: 'Arial',
                                                          ),
                                                          focusedBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Hexcolor(
                                                                  '#EB5C18'),
                                                              width: 1,
                                                            ),
                                                          ),
                                                          enabledBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Hexcolor(
                                                                  '#E6E6E6'),
                                                              width: 1,
                                                            ),
                                                          ),
                                                          focusedErrorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Colors.red,
                                                              width: 1,
                                                            ),
                                                          ),
                                                          errorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Colors.red,
                                                              width: 1,
                                                            ),
                                                          ),
                                                          fillColor:
                                                              Colors.white,
                                                          filled: true,
                                                        ),
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 17,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily: 'Arial',
                                                        ),
                                                      ),
                                                      Positioned(
                                                          top: 20,
                                                          right: 17,
                                                          child: Image.asset(
                                                            'assets/fg_images/10_profile_questionnaire_icon_birthday.png',
                                                            height: 20,
                                                            width: 18,
                                                          ))
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),

                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  chooseSex();
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            bottom: 7,
                                                          ),
                                                          child: Text(
                                                            "Стать",
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontFamily:
                                                                  'Arial',
                                                              color: Hexcolor(
                                                                  '#9B9B9B'),
                                                              fontSize: 17,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          decoration:
                                                              BoxDecoration(
                                                                  color: Hexcolor(
                                                                      '#FFFFFF'),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            10),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10),
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            10),
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            10),
                                                                  ),
                                                                  border: Border
                                                                      .all(
                                                                    color: !makeErrorSex
                                                                        ? Hexcolor(
                                                                            '#E6E6E6')
                                                                        : Hexcolor(
                                                                            '#D12D2D'),
                                                                    width: 1,
                                                                  )),
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 20),
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width -
                                                              60,
                                                          height: 60,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              isSexSelected
                                                                  ? Text(
                                                                      sex,
                                                                      style:
                                                                          TextStyle(
                                                                        color: Hexcolor(
                                                                            '#1E2E45'),
                                                                        fontSize:
                                                                            17,
                                                                        fontFamily:
                                                                            'Arial',
                                                                        fontWeight:
                                                                            FontWeight.w400,
                                                                      ),
                                                                    )
                                                                  : Text(
                                                                      sex,
                                                                      style:
                                                                          TextStyle(
                                                                        color: sex !=
                                                                                "Вкажіть вашу стать"
                                                                            ? Hexcolor('#1E2E45')
                                                                            : Hexcolor('#D3D3D3'),
                                                                        fontSize:
                                                                            17,
                                                                        fontFamily:
                                                                            'Arial',
                                                                        fontWeight:
                                                                            FontWeight.w400,
                                                                      ),
                                                                    ),
                                                              Container(
                                                                margin:
                                                                    EdgeInsets
                                                                        .only(
                                                                  right: 20,
                                                                ),
                                                                child:
                                                                    Image.asset(
                                                                  'assets/fg_images/6_home_filter_arrow.png',
                                                                  width: 10,
                                                                  height: 18,
                                                                  color: Hexcolor(
                                                                      '#1E2E45'),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              !makeErrorSex
                                                  ? SizedBox(
                                                      height: 15,
                                                    )
                                                  : Container(
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          bottom: 15,
                                                          top: 7),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      child: Text(
                                                        'Будь-ласка введіть стать',
                                                        style: TextStyle(
                                                          color:
                                                              Colors.red[700],
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),

                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Email",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          color: Hexcolor(
                                                              '#FFFFFF'),
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    10),
                                                            topRight:
                                                                Radius.circular(
                                                                    10),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    10),
                                                            bottomRight:
                                                                Radius.circular(
                                                                    10),
                                                          ),
                                                          border: Border.all(
                                                            color: Hexcolor(
                                                                '#E6E6E6'),
                                                            width: 1,
                                                          )),
                                                      padding: EdgeInsets.only(
                                                          left: 20),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              60,
                                                      height: 60,
                                                      child: Text(
                                                        userEmail,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#D3D3D3'),
                                                          fontSize: 17,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      )),
                                                  // TextFormField(
                                                  //   onChanged: (val) {},
                                                  //   // validator: (value) {
                                                  //   //   return RegExp(
                                                  //   //               r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                  //   //           .hasMatch(value)
                                                  //   //       ? null
                                                  //   //       : "Будь-ласка введіть коректний email";
                                                  //   // },
                                                  //   controller:
                                                  //       emailTextEditingController,
                                                  //   textAlign: TextAlign.left,
                                                  //   textAlignVertical:
                                                  //       TextAlignVertical
                                                  //           .center,
                                                  //   decoration: InputDecoration(
                                                  //     hintText: userEmail,
                                                  //     contentPadding:
                                                  //         new EdgeInsets.only(
                                                  //       left: 20,
                                                  //       top: 21.5,
                                                  //       bottom: 21.5,
                                                  //     ),
                                                  //     hintStyle: TextStyle(
                                                  //       color:
                                                  //           Hexcolor('#D3D3D3'),
                                                  //       fontSize: 17,
                                                  //       fontWeight:
                                                  //           FontWeight.w400,
                                                  //       fontFamily: 'Arial',
                                                  //     ),
                                                  //     focusedBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Hexcolor(
                                                  //             '#EB5C18'),
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     enabledBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Hexcolor(
                                                  //             '#E6E6E6'),
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     focusedErrorBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Colors.red,
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     errorBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Colors.red,
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     fillColor: Colors.white,
                                                  //     filled: true,
                                                  //   ),
                                                  //   style: TextStyle(
                                                  //     color:
                                                  //         Hexcolor('#1E2E45'),
                                                  //     fontSize: 17,
                                                  //     fontWeight:
                                                  //         FontWeight.w400,
                                                  //     fontFamily: 'Arial',
                                                  //   ),
                                                  // ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Телефон",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 16
                                                          ? null
                                                          : "Будь-ласка введіть телефон";
                                                    },
                                                    controller: controller,
                                                    // controller: phoneTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      // hintText: "+3(8000)000-00-00",
                                                      hintText:
                                                          "+380(00) 000-00-00",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Країна",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть країну";
                                                    },
                                                    controller:
                                                        countryTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText: "Країна",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                              left: 20,
                                                              top: 21.5,
                                                              bottom: 21.5),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Місто",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть місто";
                                                    },
                                                    controller:
                                                        cityTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText: "Місто",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 35,
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  setState(() {
                                                    isNotificationsSelected =
                                                        !isNotificationsSelected;
                                                  });
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Image.asset(
                                                      isNotificationsSelected
                                                          ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                                          : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                                      width: 18,
                                                      height: 18,
                                                    ),
                                                    SizedBox(
                                                      width: 11,
                                                    ),
                                                    Text(
                                                      "Отримувати повідомлення",
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontSize: 17,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              // GestureDetector(
                                              //   behavior: HitTestBehavior.opaque,
                                              //   onTap: () {
                                              //     setState(() {
                                              //       isNotificationsSelected =
                                              //           !isNotificationsSelected;
                                              //     });
                                              //   },
                                              //   child: Row(
                                              //     mainAxisAlignment:
                                              //         MainAxisAlignment.spaceBetween,
                                              //     children: [
                                              //       Container(
                                              //         decoration: BoxDecoration(
                                              //           color: Hexcolor('#F2F2F2'),
                                              //           border: Border.all(
                                              //             color: Hexcolor('#666666'),
                                              //             width: 3,
                                              //           ),
                                              //           borderRadius: BorderRadius.only(
                                              //               topLeft: Radius.circular(14),
                                              //               topRight: Radius.circular(14),
                                              //               bottomLeft:
                                              //                   Radius.circular(14),
                                              //               bottomRight:
                                              //                   Radius.circular(14)),
                                              //         ),
                                              //         alignment: Alignment.centerLeft,
                                              //         width: MediaQuery.of(context)
                                              //                 .size
                                              //                 .width -
                                              //             122,
                                              //         height: 47,
                                              //         child: Center(
                                              //           child: Text(
                                              //             'Получать уведомления',
                                              //             style: TextStyle(
                                              //               color: Hexcolor('#666666'),
                                              //               fontSize: 17,
                                              //               fontWeight: FontWeight.w600,
                                              //               fontFamily: 'Arial',
                                              //             ),
                                              //             textAlign: TextAlign.center,
                                              //           ),
                                              //         ),
                                              //       ),
                                              //       Container(
                                              //         decoration: BoxDecoration(
                                              //           color: Hexcolor('#F2F2F2'),
                                              //           border: Border.all(
                                              //             color: Hexcolor('#666666'),
                                              //             width: 3,
                                              //           ),
                                              //           borderRadius: BorderRadius.only(
                                              //               topLeft: Radius.circular(14),
                                              //               topRight: Radius.circular(14),
                                              //               bottomLeft:
                                              //                   Radius.circular(14),
                                              //               bottomRight:
                                              //                   Radius.circular(14)),
                                              //         ),
                                              //         alignment: Alignment.centerLeft,
                                              //         width: 47,
                                              //         height: 47,
                                              //         child: isNotificationsSelected
                                              //             ? Center(
                                              //                 child: Image.asset(
                                              //                 'assets/fg_images/6_home_filter_location_selected.png',
                                              //                 width: 30,
                                              //                 height: 30,
                                              //               ))
                                              //             : Container(),
                                              //       ),
                                              //     ],
                                              //   ),
                                              // ),
                                              SizedBox(
                                                height: 35,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ]),
                                    ))),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 25),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 50,
                            child: RaisedButton(
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0),
                              ),
                              child: Text("Отримати 50 TUS",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Arial',
                                      color: Colors.white,
                                      letterSpacing: 1.09,
                                      fontSize: 17)),
                              onPressed: () {
                                _saveData();
                                // Navigator.pushNamed(context, '/5_myBottomBar.dart');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: -5,
                      right: -40,
                      child: Image.network(
                        'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-30_10_profile_questionnaire_lion_like.gif',
                        height: 230,
                      ),
                    )
                  ],
                ),
              ));
  }
}
