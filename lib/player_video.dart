// ----------------------------------------------------WORKING-NO-PROGRESS---------------------------------------------------------------
// import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';
// import 'package:video_player/video_player.dart';

// class PlayerVideo extends StatefulWidget {
//   @override
//   _PlayerVideoState createState() => _PlayerVideoState();
// }

// class _PlayerVideoState extends State<PlayerVideo> {
//   VideoPlayerController _controller;
//   Duration duration = new Duration();
//   Duration position = new Duration();

//   @override
//   void initState() {
//     super.initState();
//     _controller = VideoPlayerController.asset('assets/video/main.MP4')
//       ..initialize().then((_) {
//         // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
//         setState(() {});
//       });
//     _controller.setLooping(true);
//     _controller.setVolume(1);
//     // _controller.seekTo(position);
//     _controller.play();
//     duration = _controller.value.duration;
//     position = _controller.value.position;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(
//         decoration: BoxDecoration(
//           color: Hexcolor('#F2F2F2'),
//         ),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             Stack(
//               overflow: Overflow.visible,
//               children: [
//                 Positioned(
//                   child: Container(
//                     margin: EdgeInsets.only(
//                       left: 20,
//                       right: 20,
//                     ),
//                     height: MediaQuery.of(context).size.height - 85,
//                     child: SingleChildScrollView(
//                       physics: BouncingScrollPhysics(),
//                       child: Column(
//                         children: [
//                           SizedBox(
//                             height: 300,
//                           ),
//                           Center(
//                             child: _controller.value.initialized
//                                 ? AspectRatio(
//                                     aspectRatio: _controller.value.aspectRatio,
//                                     child: VideoPlayer(_controller),
//                                   )
//                                 : Container(),
//                           ),
//                           FloatingActionButton(
//                             onPressed: () {
//                               setState(() {
//                                 _controller.value.isPlaying
//                                     ? _controller.pause()
//                                     : _controller.play();
//                               });
//                             },
//                             child: Icon(
//                               _controller.value.isPlaying
//                                   ? Icons.pause
//                                   : Icons.play_arrow,
//                             ),
//                           ),
//                           ValueListenableBuilder(
//                             valueListenable: _controller,
//                             builder: (context, VideoPlayerValue value, child) {
//                               return Text('${value.position.inMinutes}:${value.position.inSeconds.remainder(60)}');
//                               // return Text(value.duration.inSeconds.toString());
//                             },
//                           ),
//                           // Text('${duration.inMinutes.toString()}:${duration.inSeconds.remainder(60).toString()}'),
//                           // Slider(
//                           //     activeColor: Colors.black,
//                           //     inactiveColor: Colors.pink,
//                           //     value: _controller.value.position.inSeconds.toDouble(),
//                           //     min: 0.0,
//                           //     max: _controller.value.duration.inSeconds.toDouble(),
//                           //     onChanged: (double value) {
//                           //       setState(() {
//                           //         //  _controller.seekTo(new Duration(seconds: value.toInt()));
//                           //       });
//                           //     })
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//                 Container(
//                   decoration: BoxDecoration(
//                     color: Hexcolor('#7D5AC2'),
//                     borderRadius: BorderRadius.only(
//                         bottomLeft: Radius.circular(10.0),
//                         bottomRight: Radius.circular(10.0)),
//                   ),
//                   child: Container(
//                     decoration: BoxDecoration(
//                       color: Hexcolor('#7D5AC2'),
//                     ),
//                     alignment: Alignment.bottomLeft,
//                     margin: EdgeInsets.only(
//                       top: 50,
//                       bottom: 22,
//                     ),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: <Widget>[
//                         Container(
//                           margin: EdgeInsets.only(left: 10, right: 10),
//                           child: SizedBox(
//                             child: IconButton(
//                               icon: Image.asset(
//                                 'assets/fg_images/6_home_search_back.png',
//                                 width: 10,
//                                 height: 19,
//                               ),
//                               onPressed: () {
//                                 Navigator.pushNamed(
//                                     context, '/5_myBottomBar.dart');
//                               },
//                             ),
//                           ),
//                         ),
//                         Text(
//                           'Видеоплеер',
//                           style: TextStyle(
//                               fontSize: 33,
//                               color: Colors.white,
//                               fontFamily: 'Arial',
//                               fontWeight: FontWeight.w900),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             Column(
//               children: [
//                 Container(
//                   margin: EdgeInsets.only(left: 15, right: 15, bottom: 25),
//                   width: MediaQuery.of(context).size.width,
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Container(
//                         height: 60,
//                         width: MediaQuery.of(context).size.width - 30,
//                         child: RaisedButton(
//                           child: Text(
//                             "Закрыть",
//                             style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 17,
//                               fontFamily: 'Arial',
//                               fontWeight: FontWeight.w600,
//                             ),
//                           ),
//                           color: Hexcolor('#FE6802'),
//                           shape: new RoundedRectangleBorder(
//                               borderRadius: new BorderRadius.circular(14.0)),
//                           onPressed: () {
//                             // Navigator.pushNamed(context, '/quest_done.dart');
//                           },
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _controller.dispose();
//   }
// }
// --------------------------------------------------CHEWIE------------------------------------------------------------------

import 'package:chewie/chewie.dart';
// import 'package:chewie/src/chewie_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:video_player/video_player.dart';

class PlayerVideo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PlayerVideoState();
  }
}

class _PlayerVideoState extends State<PlayerVideo> {
  // TargetPlatform _platform;
  // VideoPlayerController _videoPlayerController1;
  VideoPlayerController _videoPlayerController2;
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    this.initializePlayer();
  }

  @override
  void dispose() {
    // _videoPlayerController1.dispose();
    _videoPlayerController2.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  Future<void> initializePlayer() async {
    // _videoPlayerController1 = VideoPlayerController.asset(
    //     // 'assets/video/main.MP4');
    //     'assets/music/main.mp3');
    // await _videoPlayerController1.initialize();
    _videoPlayerController2 =
        VideoPlayerController.asset('assets/video/main.MP4');
    await _videoPlayerController2.initialize();
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController2,
      autoPlay: true,
      looping: true,
      // overlay: Container(
      //   height: 100,
      //   color: Colors.teal,
      // ),
      // Try playing around with some of these other options:
      // showControls: true,
      materialProgressColors: ChewieProgressColors(
        playedColor: Hexcolor('#FE6802'),
        handleColor: Hexcolor('#1B94ED'),
        backgroundColor: Hexcolor('#BEBEBE'),
        bufferedColor: Hexcolor('#BEBEBE'),
      ),
      placeholder: Container(
        color: Hexcolor('#F2F2F2'),
      ),
      autoInitialize: true,
    );
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(
                  top: 50,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Видео',
                          style: TextStyle(
                              fontSize: 32,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/run_map_quest_pic_gostart.png',
                            width: 22,
                            height: 24,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: _chewieController != null &&
                        _chewieController
                            .videoPlayerController.value.initialized
                    ? Container(
                        margin: EdgeInsets.only(
                          bottom: 20,
                          left: 20,
                          right: 20,
                        ),
                        child: Chewie(
                          controller: _chewieController,
                        ),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(
                            backgroundColor: Hexcolor('#7D5AC2'),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              Hexcolor('#FE6802'),
                            ),
                          ),
                          SizedBox(height: 20),
                          Text('Загрузка...',
                              style: TextStyle(
                                color: Hexcolor('#747474'),
                                fontSize: 14,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              )),
                        ],
                      ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        "Закрыть",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        // Navigator.pushNamed(context, '/quest_done.dart');
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
