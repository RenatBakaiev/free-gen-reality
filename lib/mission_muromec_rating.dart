import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/10_profile_rating_json.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:convert' show utf8;

class MissionMuromecRating extends StatefulWidget {
  @override
  _MissionMuromecRatingState createState() => _MissionMuromecRatingState();
}

class _MissionMuromecRatingState extends State<MissionMuromecRating> {
  List<User> users;
  bool loading;
  String timer = "";
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesUsersMuromec.getUsers().then((post) {
      setState(() {
        users = post.users;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#FFFFFF'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Text(
                              'Рейтинг',
                              style: TextStyle(
                                  fontSize: 37,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: users.length,
                      itemBuilder: (context, index) {
                        User user = users[index];
                        // var username = '';
                        var username = user.username;

                        transformMilliSeconds(int milliseconds) {
                          int hundreds = (milliseconds / 10).truncate();
                          int seconds = (hundreds / 100).truncate();
                          int minutes = (seconds / 60).truncate();
                          int hours = (minutes / 60).truncate();

                          String hoursStr =
                              (hours % 60).toString().padLeft(2, '0');
                          String minutesStr =
                              (minutes % 60).toString().padLeft(2, '0');
                          String secondsStr =
                              (seconds % 60).toString().padLeft(2, '0');
                          return "$hoursStr:$minutesStr:$secondsStr";
                        }

                        var timer = transformMilliSeconds(user.timer);

                        return user.timer != 0
                            ? Container(
                                height: 60,
                                width: MediaQuery.of(context).size.width - 40,
                                margin: EdgeInsets.only(
                                    bottom: 23, left: 20, right: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 30,
                                          backgroundImage: NetworkImage(
                                              // user.profileImageUrl == "" || user.profileImageUrl == null
                                              //     ? '$link' + 'default.png'
                                              //     : user.registrationType.toString() == 'EMAIL'
                                              //         ? user.profileImageUrl
                                              //         : '${'http://generation-admin.ehub.com.ua' + user.profileImageUrl.substring(21)}',

                                              user.profileImageUrl == "" ||
                                                      user.profileImageUrl ==
                                                          null
                                                  ? '$link' + 'default.png'
                                                  : user.profileImageUrl
                                                          .contains('localhost')
                                                      ? '${'http://generation-admin.ehub.com.ua' + user.profileImageUrl.substring(21)}'
                                                      : user.profileImageUrl),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(left: 14),
                                            child: Text(
                                              username == null
                                                  ? ''
                                                  : utf8.decode(
                                                      username.runes.toList()),
                                              // (firstName == null ? '' : firstName) + ' ' + (lastName == null ? '' : lastName),
                                              // firstName == null ? '' : firstName,
                                              style: TextStyle(
                                                color: Hexcolor('#1E2E45'),
                                                fontSize: 16,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600,
                                              ),
                                            )),
                                      ],
                                    ),
                                    Text(
                                      timer,
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 16,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container();
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
