import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';

import 'package:http/http.dart' as http;
import '10_profile_json.dart';
import 'dart:convert';

class ScanQrCode5 extends StatefulWidget {
  @override
  _ScanQrCode5State createState() => _ScanQrCode5State();
}

class _ScanQrCode5State extends State<ScanQrCode5> {
  bool isApiCallProcess = false;
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  QRViewController controller;

    Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "deposit 20 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('+20TUS done');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Урааа',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Перейти на наступний крок",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              setState(() {
                                isApiCallProcess = true;
                              });
                              final SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              var userEmail = sharedPreferences.get("email");
                              var res1 =
                                  sharedPreferences.getBool('isStep1Completed');
                              var res2 =
                                  sharedPreferences.getBool('isStep2Completed');
                              var res3 =
                                  sharedPreferences.getBool('isStep3Completed');
                              var res4 =
                                  sharedPreferences.getBool('isStep4Completed');
                              var res5 =
                                  sharedPreferences.getBool('isStep5Completed');
                              var res6 =
                                  sharedPreferences.getBool('isStep6Completed');
                              var res7 =
                                  sharedPreferences.getBool('isStep7Completed');
                              var res8 =
                                  sharedPreferences.getBool('isStep8Completed');
                              var res9 =
                                  sharedPreferences.getBool('isStep9Completed');
                              var res10 = sharedPreferences
                                  .getBool('isStep10Completed');
                              var res11 = sharedPreferences
                                  .getBool('isStep11Completed');
                              var res12 = sharedPreferences
                                  .getBool('isStep12Completed');
                              int tus = sharedPreferences.get("tus");
                              if (userEmail != '' && userEmail != null) {
                                sharedPreferences.setBool(
                                    'isStep5Completed', true);
                                sharedPreferences.setInt('tus', tus + 20);
                                setState(() {
                                  checkPoins10[4].isCompleted = true;
                                });
                                if (res1 == true &&
                                    res2 == true &&
                                    res3 == true &&
                                    res4 == true &&
                                    res5 == true &&
                                    res6 == true &&
                                    res7 == true &&
                                    res8 == true &&
                                    res9 == true &&
                                    res10 == true &&
                                    res11 == true &&
                                    res12 == true) {
                                  final SharedPreferences sharedPreferences =
                                      await SharedPreferences.getInstance();
                                  int tus = sharedPreferences.get("tus");
                                  sharedPreferences.setInt('tus', tus + 100);
                                  Navigator.pushNamed(
                                      context, '/quest_done.dart');
                                  setState(() {
                                    isApiCallProcess = false;
                                  });
                                } else {
                                 Navigator.pushNamed(
                                      context, '/missions_12_12_trees.dart');
                                  setState(() {
                                    isApiCallProcess = false;
                                  });
                                }
                              } else {
                                Navigator.pushNamed(
                                    context, '/missions_12_12_trees.dart');
                                setState(() {
                                  isApiCallProcess = false;
                                });
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Column(
            children: [
              Expanded(
                flex: 9,
                child: QRView(
                    key: qrKey,
                    overlay: QrScannerOverlayShape(
                      borderRadius: 10,
                      borderColor: Colors.red,
                      borderLength: 30,
                      borderWidth: 10,
                      cutOutSize: 300,
                    ),
                    onQRViewCreated: _onQRViewCreate),
              ),
            ],
          ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          qrText != 'http://freegen.net'
              ? Container()
              : Positioned(
                  bottom: 60,
                  left: 20,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#FE6802'),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                    ),
                    // color: Hexcolor('#FE6802'),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        'Продовжити',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () async {
                        // if (qrText == 'qr1') {print('код 1');}
                        // if (qrText == 'qr2') {print('код 2');}
                        // if (qrText != '') {
                        //   Navigator.of(context).push(MaterialPageRoute(
                        //       builder: (context) =>
                        //           ScanQRcodeResult(text: '$qrText')));
                        // }

                        // showInfoWindow();
                        changeTus(20.0, "DEPOSIT");
                        Navigator.pushNamed(context, '/missions_12_scan5.dart');
                        final SharedPreferences sharedPreferences =
                            await SharedPreferences.getInstance();
                        int tus = sharedPreferences.get("tus");
                        sharedPreferences.setInt('tus', tus + 20);
                      },
                    ),
                  ),
                ),
          Positioned(
            bottom: 20,
            left: MediaQuery.of(context).size.width / 2 - 40,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                // showInfoWindow();
                Navigator.pushNamed(context, '/missions_12_scan5.dart');
              },
              child: Container(
                // width: 110,
                child: Center(
                    child: Text(
                  'Пропустити',
                  style: TextStyle(
                    color: Hexcolor('#FFFFFF'),
                    fontFamily: 'Arial',
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                )),
              ),
            ),
          )
        ],
      ),
    );
  }

  //global variables:

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }
}
