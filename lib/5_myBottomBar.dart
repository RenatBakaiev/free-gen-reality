// import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_free_gen_reality/3_layout_3.dart';
// import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';

import '10_profile.dart';
import '6_home.dart';
import '7_map.dart';
import '8_camera.dart';
import '9_favorites.dart';

class MyBottomBar extends StatefulWidget {
  @override
  _MyBottomBarState createState() => _MyBottomBarState();
}

class _MyBottomBarState extends State<MyBottomBar> {
  int _currentIndex = 0;

  showLockProfileWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              // width: MediaQuery.of(context).size.width - 40,
              height: 600,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                  bottom: 10,
                                ),
                                child: Column(
                                  children: [
                                    Text(
                                      'Профіль користувача',
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 24,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      'недоступний',
                                      style: TextStyle(
                                        color: Hexcolor('#FF1E1E'),
                                        fontSize: 24,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                )),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 5,
                              ),
                              child: Text(
                                'Перейти в профіль можуть лише зареєстровані користувачі.',
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ])),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                    ),
                    child: Image.asset(
                      'assets/fg_images/3_layout_3_welcome.jpg',
                      height: 290,
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.only(bottom: 15, right: 20, left: 20),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: [
                            Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width / 2-45,
                              child: RaisedButton(
                                child: Text(
                                  "Реєстрація",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                color: Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, '/4_enter_register');
                                },
                              ),
                            ),
                            SizedBox(width: 10,),
                            Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width / 2-45,
                              child: RaisedButton(
                                child: Text(
                                  "Авторизація",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                color: Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, '/4_enter');
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(bottom: 20, right: 20, left: 20),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Відміна",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#BEBEBE'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }
  
  final List<Widget> _children = [
    Home(),
    Map(),
    Camera(),
    Favorites(),
    Profile(),
  ];

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.black));

    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        onTap: onTappedBar,
        unselectedItemColor: Hexcolor('#D2D2D2'),
        selectedItemColor: Hexcolor('#FE6802'),
        currentIndex: _currentIndex,
        backgroundColor: Colors.white,
        items: [
          BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 5),
                child: SizedBox(
                  width: 22,
                  height: 22,
                  child: ImageIcon(AssetImage(
                    'assets/fg_images/5_home_logo_home.png',
                  )),
                ),
              ),
              title: Container(
                margin: EdgeInsets.only(top: 9, bottom: 5),
                child: Text(
                  "Головна",
                  style: TextStyle(
                    color: Hexcolor('#D2D2D2'),
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              )),
          BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 5),
                child: SizedBox(
                  width: 17,
                  height: 23,
                  child: ImageIcon(AssetImage(
                    'assets/fg_images/5_home_logo_map.png',
                  )),
                ),
              ),
              title: Container(
                margin: EdgeInsets.only(top: 9, bottom: 5),
                child: Text(
                  "Мапа",
                  style: TextStyle(
                    color: Hexcolor('#D2D2D2'),
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              )),
          BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 5),
                child: SizedBox(
                    width: 30,
                    height: 20,
                    child: Stack(
                      overflow: Overflow.visible,
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                            bottom: -22.5,
                            child: CircleAvatar(
                              radius: 30,
                              backgroundColor: Colors.white,
                            )),
                        ImageIcon(AssetImage(
                          'assets/fg_images/5_home_logo_camera.png',
                        )),
                      ],
                    )),
              ),
              title: Container(
                margin: EdgeInsets.only(top: 10, bottom: 5),
                child: Text(
                  "Камера",
                  style: TextStyle(
                    color: Hexcolor('#D2D2D2'),
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              )),
          BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 5),
                child: SizedBox(
                  width: 18,
                  height: 23,
                  child: ImageIcon(AssetImage(
                    'assets/fg_images/5_home_logo_favorites.png',
                  )),
                ),
              ),
              title: Container(
                margin: EdgeInsets.only(top: 8, bottom: 5),
                child: Text(
                  "Обране",
                  style: TextStyle(
                    color: Hexcolor('#D2D2D2'),
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              )),
          BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 8, bottom: 0),
                child: Center(
                  child: SizedBox(
                  width: 18,
                  height: 23,
                  child: ImageIcon(AssetImage(
                    'assets/fg_images/5_home_logo_profile.png',
                  )),
                ),
                ),
              ),
              title: Container(
                margin: EdgeInsets.only(top: 8, bottom: 5),
                child: Text(
                  "Профіль",
                  style: TextStyle(
                    color: Hexcolor('#D2D2D2'),
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
