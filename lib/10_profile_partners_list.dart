import 'package:hexcolor/hexcolor.dart';
import '10_profile_partners_model.dart';

final Partner partner1 = Partner(
  name: 'Сеть АЗС Glusco',
  image: 'assets/fg_images/10_profile_partners_icon_1.jpg',
  backcolor: Hexcolor('#204586'),
  backColorImage: Hexcolor('#001C43'),
  link: '/10_profile_partners_partner_1_glusco.dart',
);

final Partner partner2 = Partner(
  name: 'Фокстрот',
  image: 'assets/fg_images/10_profile_partners_icon_2.jpg',
  backcolor: Hexcolor('#FE8A5C'),
  backColorImage: Hexcolor('#FFFFFF'),
  link: '/10_profile_partners_partner_2_foxtrot.dart',
);

final Partner partner3 = Partner(
  name: 'Новая почта',
  image: 'assets/fg_images/10_profile_partners_icon_3.jpg',
  backcolor: Hexcolor('#D6454A'),
  backColorImage: Hexcolor('#FFFFFF'),
  link: '/10_profile_partners_partner_3_nova_poshta.dart',
);

final Partner partner4 = Partner(
  name: 'Х Park',
  image: 'assets/fg_images/10_profile_partners_icon_4.jpg',
  backcolor: Hexcolor('#204586'),
  backColorImage: Hexcolor('#FFFFFF'),
);

final Partner partner5 = Partner(
  name: 'Укрпошта',
  image: 'assets/fg_images/10_profile_partners_icon_5.jpg',
  backcolor: Hexcolor('#F3C43C'),
  backColorImage: Hexcolor('#FFFFFF'),
);

final Partner partner6 = Partner(
  name: 'Сеть АЗС Glusco',
  image: 'assets/fg_images/10_profile_partners_icon_6.jpg',
  backcolor: Hexcolor('#20865B'),
  backColorImage: Hexcolor('#001C43'),
);

final Partner partner7 = Partner(
  name: 'Сеть АЗС Glusco',
  image: 'assets/fg_images/10_profile_partners_icon_7.jpg',
  backcolor: Hexcolor('#FE8A5C'),
  backColorImage: Hexcolor('#001C43'),
);

final Partner partner8 = Partner(
  name: 'Сеть АЗС Glusco',
  image: 'assets/fg_images/10_profile_partners_icon_8.jpg',
  backcolor: Hexcolor('#204586'),
  backColorImage: Hexcolor('#001C43'),
);

List<Partner> partners = [
  partner1,
  partner2,
  partner3,
  partner4,
  partner5,
  partner6,
  partner7,
  partner8,
];
