import 'dart:async';
// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '10_profile_rewards.dart';

class QuestQuiz extends StatefulWidget {
  @override
  _QuestQuizState createState() => _QuestQuizState();
}

class _QuestQuizState extends State<QuestQuiz> {
  bool isPressedA = false;
  bool isPressedB = false;
  bool isPressedC = false;
  bool isPressedD = false;

  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        // start progress line
                        margin: EdgeInsets.only(top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                Image.asset(
                                    'assets/fg_images/next_step_icon_start.png',
                                    width: 12,
                                    height: 12),
                                Positioned(
                                  bottom: 9,
                                  left: 4.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_start_flag.png',
                                          width: 18,
                                          height: 21),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 8),
                                        child: Text(
                                          'ул. Владимирская, 24а',
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 12,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#FE6802')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_done_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#FE6802')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_done_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#1E2E45')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_notdone_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#1E2E45')),
                            ),
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                Image.asset(
                                    'assets/fg_images/next_step_icon_notdone_step.png',
                                    width: 12,
                                    height: 12),
                                Positioned(
                                  top: 9,
                                  right: 4.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(right: 10, top: 8),
                                        child: Text(
                                          'ул. Богдана Хмельницкого, 13/2',
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 12,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_finish_flag.png',
                                          width: 18,
                                          height: 21),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Поздравляем!',
                                style: TextStyle(
                                  color: Hexcolor('#59B32D'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Вы заработали ',
                                    style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontSize: 16,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    '100' + ' foint',
                                    style: TextStyle(
                                      color: Hexcolor('#FE6802'),
                                      fontSize: 16,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'за прохождение шага!',
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 16,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          )),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на cледующий шаг",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/questScan.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
                Positioned(
                  right: 40,
                  top: 50,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '2',
                          style: TextStyle(
                            fontSize: 36,
                            color: Hexcolor('#1E2E45'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 2),
                          child: Text(
                            ' ' + '/ ' + '4',
                            style: TextStyle(
                              fontSize: 24,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  showInfoWindowWrongAnswer(answer) {
    setState(() {
      if (allRewards[0].quantity != 0) {
        allRewards[0].quantity = allRewards[0].quantity - 5;
      }
      if (allRewards[0].quantity <= 5) {
        allRewards[0].quantity = 0;
      }
    });
    Timer(
        Duration(milliseconds: 500),
        () => showDialog(
            barrierDismissible: false,
            barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
            context: context,
            builder: (context) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  height: 580,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Ответ неправильный',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_wrong_answer.png',
                        height: 350,
                      ),
                      Text('Неудача',
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Hexcolor('#FF1E1E'),
                          ),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Вы выбрали неправильный вариант ответа.',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'С вашего счета снято ',
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                    color: Hexcolor('#747474'),
                                  ),
                                ),
                                Text(
                                  '5' + ' TUS',
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                    color: Hexcolor('#FE6802'),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Попробовать еще раз",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/questQuiz.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  showInfoWindowRightAnswer(answer) {
    setState(() {
      allRewards[0].quantity = allRewards[0].quantity + 2;
    });
    Timer(
        Duration(milliseconds: 500),
        () => showDialog(
            barrierDismissible: false,
            barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
            context: context,
            builder: (context) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  // width: MediaQuery.of(context).size.width - 80,
                  height: 580,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Ответ правильный',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_right_answer.png',
                        height: 350,
                      ),
                      Text('Поздравляем!',
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Hexcolor('#59B32D'),
                          ),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Вы выбрали правильный вариант ответа.',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Вам начислено ',
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                    color: Hexcolor('#747474'),
                                  ),
                                ),
                                Text(
                                  '2' + ' TUS',
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                    color: Hexcolor('#FE6802'),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Продолжить",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            setState(() {
                              allRewards[5].quantity =
                                  allRewards[5].quantity + 100;
                            });
                            showInfoWindow();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height
                    //  - 85
                    ,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 340,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/fg_images/6_home_quest_6.png'),
                                  fit: BoxFit.cover,
                                )),
                            child: Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                right: 20,
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(),
                                      GestureDetector(
                                        behavior: HitTestBehavior.opaque,
                                        onTap: () {
                                           Navigator.pushNamed(context, '/player_video.dart');
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            top: 30,
                                          ),
                                          child: Image.asset(
                                            'assets/fg_images/player_video_start.png',
                                            height: 48,
                                            width: 48,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        '2',
                                        style: TextStyle(
                                          fontSize: 36,
                                          color: Colors.white,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Text(
                                        ' ' + '/ ' + '4',
                                        style: TextStyle(
                                          fontSize: 24,
                                          color: Colors.white,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Вопрос',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Как, в переводе на русский язык, звучало бы название фильма Мимино?',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 20,
                              // bottom: 20,
                              left: 40,
                            ),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '1) ' + 'Сокол',
                                  style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontFamily: 'Arial',
                                      fontSize: 16),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '2) ' + 'Медведь',
                                  style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontFamily: 'Arial',
                                      fontSize: 16),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '3) ' + 'Гора',
                                  style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontFamily: 'Arial',
                                      fontSize: 16),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '4) ' + 'Любовь',
                                  style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontFamily: 'Arial',
                                      fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 15,
                              right: 15,
                              top: 20,
                            ),
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      20,
                                  child: RaisedButton(
                                    child: Text(
                                      "Вариант 1",
                                      style: TextStyle(
                                        color: isPressedA
                                            ? Colors.white
                                            : Hexcolor('#545454'),
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    color: isPressedA
                                        ? Hexcolor('#75C433')
                                        : Hexcolor('#F4F4F4'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      setState(() {
                                        isPressedA = true;
                                      });

                                      showInfoWindowRightAnswer("Вариант 1");
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      20,
                                  child: RaisedButton(
                                    child: Text(
                                      "Вариант 2",
                                      style: TextStyle(
                                        color: isPressedB
                                            ? Colors.white
                                            : Hexcolor('#545454'),
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    color: isPressedB
                                        ? Colors.red
                                        : Hexcolor('#F4F4F4'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      setState(() {
                                        isPressedB = true;
                                      });
                                      showInfoWindowWrongAnswer('Вариант 2');
                                      // setState(() {
                                      //   if (!isPressedA &&
                                      //       !isPressedC &&
                                      //       !isPressedD) {
                                      //     isPressedB = !isPressedB;
                                      //   }
                                      // });
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 15,
                              right: 15,
                              top: 10,
                              bottom: 10,
                            ),
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      20,
                                  child: RaisedButton(
                                    child: Text(
                                      "Вариант 3",
                                      style: TextStyle(
                                        color: isPressedC
                                            ? Colors.white
                                            : Hexcolor('#545454'),
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    color: isPressedC
                                        ? Colors.red
                                        : Hexcolor('#F4F4F4'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      setState(() {
                                        isPressedC = true;
                                      });
                                      showInfoWindowWrongAnswer('Вариант 3');
                                      //setState(() {
                                      // if (!isPressedA &&
                                      //     !isPressedB &&
                                      //     !isPressedD) {
                                      //   isPressedC = !isPressedC;
                                      // }
                                      // });
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width / 2 -
                                      20,
                                  child: RaisedButton(
                                    child: Text(
                                      "Вариант 4",
                                      style: TextStyle(
                                        color: isPressedD
                                            ? Colors.white
                                            : Hexcolor('#545454'),
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    color: isPressedD
                                        ? Colors.red
                                        : Hexcolor('#F4F4F4'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    onPressed: () {
                                      setState(() {
                                        isPressedD = true;
                                      });

                                      showInfoWindowWrongAnswer('Вариант 4');
                                      // setState(() {
                                      //   if (!isPressedA &&
                                      //       !isPressedB &&
                                      //       !isPressedC) {
                                      //     isPressedD = !isPressedD;
                                      //   }
                                      // });
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: SizedBox(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_search_back.png',
                                    width: 10,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, '/questInfo.dart');
                                  },
                                ),
                              ),
                            ),
                            Text(
                              'Викторина',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/run_map_quest_pic_gostart.png',
                                width: 22,
                                height: 24,
                              ),
                              onPressed: () {
                                Navigator.pushNamed(
                                  context,
                                  '/5_myBottomBar.dart',
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
