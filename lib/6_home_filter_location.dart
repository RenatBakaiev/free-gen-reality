import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/6_home_filter_location_regions.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:translator/translator.dart';
import 'package:geocoder/geocoder.dart';
// import 'package:geocoding/geocoding.dart';

class HomeFilterLocation extends StatefulWidget {
  @override
  _HomeFilterLocationState createState() => _HomeFilterLocationState();
}

class _HomeFilterLocationState extends State<HomeFilterLocation> {
  bool checkBoxValue = false;

  Geolocator geolocator = Geolocator();
  Geocoder geocoder = Geocoder();
  final translator = GoogleTranslator();
  var currentLocation;
  bool loadingLocation = true;

  String _location = "";
  get _getPlace async {
    setState(() {
      loadingLocation = true;
    });
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    debugPrint('location: ${position.latitude}');

    final coordinates = new Coordinates(position.latitude, position.longitude);    
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    //print("${first.featureName} : ${first.addressLine}");
    print(
        "${first.addressLine}, ! ${first.adminArea}, ! ${first.coordinates}, ${first.countryCode}, ${first.countryName}, ${first.featureName}, ${first.locality}, ${first.postalCode}, ${first.subAdminArea}, ${first.subLocality}, ${first.subThoroughfare}, ${first.thoroughfare},");
    setState(() {
      //  _location = "${first.featureName} : ${first.addressLine}";
      _location =
          "${first.countryName}, ${first.subAdminArea},\n${first.subLocality},\n${first.thoroughfare}, ${first.featureName}";          
      loadingLocation = false;
    });
  }

  // String address = "";
  // getPlace() async {
  //   List<Placemark> newPlace = await placemarkFromCoordinates(
  //       currentLocation.latitude, currentLocation.longitude);

  //   // this is all you need
  //   Placemark placeMark = newPlace[0];
  //   String name = placeMark.name;
  //   String isoCountryCode = placeMark.isoCountryCode;
  //   String subAdministrativeArea = placeMark.subAdministrativeArea;
  //   String thoroughfare = placeMark.thoroughfare;
  //   String subThoroughfare = placeMark.subThoroughfare;
  //   //Position position = placeMark.;
  //   String subLocality = placeMark.subLocality;
  //   String locality = placeMark.locality;
  //   String administrativeArea = placeMark.administrativeArea;
  //   String postalCode = placeMark.postalCode;
  //   String country = placeMark.country;

  //   String address1 =
  //       "$name, $subLocality, $locality, $administrativeArea, $postalCode, $country, $isoCountryCode, $subAdministrativeArea, $thoroughfare, $subThoroughfare";

  //   // String location = "$locality, $subLocality";

  //   print(address1);
  //   // print(location);

  //   setState(() {
  //     // translator.translate(location, from: 'en', to: 'ru').then(print);
  //     // _location = location;
  //     address = address1;
  //     loadingLocation = false;
  //   });
  // }

  @override
  void initState() {
    super.initState();

    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        //getPlace();
        _getPlace;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: loadingLocation
            ? Center(
                child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(
                    backgroundColor: Hexcolor('#7D5AC2'),
                    valueColor: new AlwaysStoppedAnimation<Color>(
                      Hexcolor('#FE6802'),
                    ),
                  ),
                ),
              )
            : Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#FFFFFF'),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0)),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Hexcolor('#7D5AC2'),
                        ),
                        alignment: Alignment.bottomLeft,
                        margin: EdgeInsets.only(
                          top: 50,
                          left: 15,
                          right: 15,
                          bottom: 22,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                // _getPlace();
                              },
                              child: Text(
                                'Місцезнаходження',
                                style: TextStyle(
                                    fontSize: 34,
                                    color: Colors.white,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900),
                              ),
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: 38,
                                  height: 38,
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/6_home_filter_close.png',
                                      width: 20,
                                      height: 20,
                                    ),
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, '/6_home_filter.dart');
                                    },
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),



                    Container(
                      child: Expanded(
                          child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          // height: MediaQuery.of(context).size.height - 190,
                          alignment: Alignment.topCenter,
                          margin: EdgeInsets.only(
                            // top: 20,
                            // right: 25,
                            left: 25,
                          ),
                          child: Column(
                            children: [
                              Container(
                                height: MediaQuery.of(context).size.height - 15,
                                margin: EdgeInsets.only(top: 15),
                                child: Column(children: [
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Моє місцезнаходження',
                                              style: TextStyle(
                                                color: Hexcolor('#919191'),
                                                fontSize: 16,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w400,
                                                letterSpacing: 1.025,
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    height: 40,
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_filter_location_pin.png',
                                                      width: 20,
                                                      height: 29,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Text(
                                                      // 'Київ',
                                                      _location,
                                                      //address,
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontSize: 16,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        letterSpacing: 1.153,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            // Navigator.pushNamed(context,
                                            //     '/6_home_filter_location.dart');
                                          },
                                          child: Container(
                                            width: 62,
                                            height: 62,
                                            margin: EdgeInsets.only(right: 18),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/fg_images/6_home_filter_location_map.jpg'),
                                                  fit: BoxFit.cover,
                                                )),
                                          ),
                                        ),
                                      ]),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Divider(
                                    color: Hexcolor('#ECECEC'),
                                    // color: Colors.green,
                                    thickness: 1,
                                    height: 12,
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      // margin: EdgeInsets.only(top: 20),
                                      child: Text(
                                        'Вибрати район',
                                        style: TextStyle(
                                          color: Hexcolor('#919191'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                          letterSpacing: 1.089,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width,
                                      height:
                                          MediaQuery.of(context).size.height -
                                              125,
                                      // child: Expanded(
                                      child: ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          itemCount: districts.length,
                                          itemBuilder: (context, index) {
                                            return GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                setState(() {
                                                  districts[index].isSelected =
                                                      !districts[index]
                                                          .isSelected;
                                                });
                                              },
                                              child: Container(
                                                  // height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            districts[index]
                                                                .district,
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#1E2E45'),
                                                              fontSize: 17,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              letterSpacing:
                                                                  1.089,
                                                            ),
                                                          ),
                                                          Text(
                                                            districts[index]
                                                                .districtPlace,
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#919191'),
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              letterSpacing:
                                                                  1.089,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      districts[index]
                                                              .isSelected
                                                          ? Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                right: 20,
                                                              ),
                                                              child:
                                                                  Image.asset(
                                                                'assets/fg_images/6_home_filter_location_selected.png',
                                                                width: 30,
                                                                height: 30,
                                                              ),
                                                            )
                                                          : Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                right: 20,
                                                              ),
                                                              child:
                                                                  Image.asset(
                                                                'assets/fg_images/6_home_filter_location_unselected.png',
                                                                width: 30,
                                                                height: 30,
                                                              ),
                                                            ),
                                                    ],
                                                  )),
                                            );
                                          })),
                                  // ),
                                ]),
                              )
                            ],
                          ),
                        ),
                      )),
                    ),


                    Container(
                      margin: EdgeInsets.only(bottom: 20),
                      height: 60,
                      width: MediaQuery.of(context).size.width - 40,
                      child: RaisedButton(
                        color: Hexcolor('#FE6802'),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0),
                        ),
                        child: Text("Готово",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Arial',
                                color: Colors.white,
                                letterSpacing: 1.09,
                                fontSize: 17)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/6_home_filter.dart');
                        },
                      ),
                    ),
                  ],
                ),
              ));
  }
}
