import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/run_map_quest_model.dart';

final CheckPoint checkPoint1 = CheckPoint(
  name: '1й чекпоинт',
  image: 'assets/fg_images/run_map_quest_pic_checkpoint_1.jpg',
  color: Colors.black,
  lat: 50.448845,
  long: 30.514550,
);

final CheckPoint checkPoint2 = CheckPoint(
  name: '2й чекпоинт',
  image: 'assets/fg_images/run_map_quest_pic_checkpoint_2.jpg',
  color: Colors.black,
  lat: 50.445969,
  long: 30.513440,
);

final CheckPoint checkPoint3 = CheckPoint(
  name: '3й чекпоинт',
  image: 'assets/fg_images/run_map_quest_pic_checkpoint_3.jpg',
  color: Colors.black,
  lat: 50.445539,
  long: 30.515864,
);

List<CheckPoint> checkPoins = [
  checkPoint1,
  checkPoint2,
  checkPoint3,
];

final CheckPoint checkPointMap1 = CheckPoint(
  name: 'Деснянська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_1.jpg',
  color: Colors.black,
  lat: 50.505440, // elka
  long: 30.599712,
  isCompleted: false,
  // lat: 50.450631, // test
  // long: 30.510639,
);

final CheckPoint checkPointMap2 = CheckPoint(
  name: 'Дніпровська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_2.jpg',
  color: Colors.black,
  lat: 50.458019,
  long: 30.608130,
  isCompleted: false,
);

final CheckPoint checkPointMap3 = CheckPoint(
  name: 'Дарницька',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_3.jpg',
  color: Colors.black,
  lat: 50.409107,
  long: 30.657204,
  isCompleted: false,
);

final CheckPoint checkPointMap4 = CheckPoint(
  name: 'Оболонська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_4.jpg',
  color: Colors.black,
  lat: 50.511578,
  long: 30.501955,
  isCompleted: false,
);

final CheckPoint checkPointMap5 = CheckPoint(
  name: 'Голосіївська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_5.jpg',
  color: Colors.black,
  lat: 50.412766,
  long: 30.523388,
  isCompleted: false,
);

final CheckPoint checkPointMap6 = CheckPoint(
  name: 'Солом\'янська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_6.jpg',
  color: Colors.black,
  lat: 50.434011,
  long: 30.427453,
  isCompleted: false,
);

final CheckPoint checkPointMap7 = CheckPoint(
  name: 'Подільська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_7.jpg',
  color: Colors.black,
  lat: 50.464062,
  long: 30.519020,
  isCompleted: false,
);

final CheckPoint checkPointMap8 = CheckPoint(
  name: 'Печерська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_8.jpg',
  color: Colors.black,
  lat: 50.444367,
  long: 30.528763,
  isCompleted: false,
);

final CheckPoint checkPointMap9 = CheckPoint(
  name: 'Шевченківська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_9.jpg',
  color: Colors.black,
  lat: 50.442639,
  long: 30.513226,
  isCompleted: false,
);

final CheckPoint checkPointMap10 = CheckPoint(
  name: 'Святошинська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_10.jpg',
  color: Colors.black,
  lat: 50.456713,
  long: 30.380639,
  isCompleted: false,
);

final CheckPoint checkPointMap11 = CheckPoint(
  name: 'Дружби Народів',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_11.jpg',
  color: Colors.black,
  lat: 50.454765,
  long: 30.529825,
  isCompleted: false,
);

final CheckPoint checkPointMap12 = CheckPoint(
  name: 'Софіївська',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_xnew_year_tree_12.jpg',
  color: Colors.black,
  lat: 50.453530,
  long: 30.516144,
  isCompleted: false,
);

List<CheckPoint> checkPoins10 = [
  checkPointMap1,
  checkPointMap2,
  checkPointMap3,
  checkPointMap4,
  checkPointMap5,
  checkPointMap6,
  checkPointMap7,
  checkPointMap8,
  checkPointMap9,
  checkPointMap10,
  checkPointMap11,
  checkPointMap12,
];

final CheckPoint muromec1 = CheckPoint(
  name: 'Старт',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_1.jpg',
  color: Colors.black,
  lat: 50.49676,
  long: 30.54246,
  isCompleted: false,
);

final CheckPoint muromec2 = CheckPoint(
  name: 'Піфагор',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_2.jpeg',
  color: Colors.black,
  lat: 50.50081,
  long: 30.54259,
  isCompleted: false,
);

final CheckPoint muromec3 = CheckPoint(
  name: 'Лісочок',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_3.jpg',
  color: Colors.black,
  lat: 50.50303,
  long: 30.53895,
  isCompleted: false,
);

final CheckPoint muromec4 = CheckPoint(
  name: 'Яхт тайм',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_4.jpeg',
  color: Colors.black,
  lat: 50.50447,
  long: 30.53463,
  isCompleted: false,
);

final CheckPoint muromec5 = CheckPoint(
  name: 'Пісенне поле',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_5.jpeg',
  color: Colors.black,
  lat: 50.51068,
  long: 30.54137,
  isCompleted: false,
);

final CheckPoint muromec6 = CheckPoint(
  name: 'Озеро',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_6.jpeg',
  color: Colors.black,
  lat: 50.51506,
  long: 30.53985,
  isCompleted: false,
);

final CheckPoint muromec7 = CheckPoint(
  name: 'Реве та стогне',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_7.jpeg',
  color: Colors.black,
  lat: 50.52254,
  long: 30.54019,
  isCompleted: false,
);

final CheckPoint muromec8 = CheckPoint(
  name: 'Медитація',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_8.jpeg',
  color: Colors.black,
  lat: 50.5204,
  long: 30.55877,
  isCompleted: false,
);

final CheckPoint muromec9 = CheckPoint(
  name: 'Вигуровщина',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_9.jpg',
  color: Colors.black,
  lat: 50.51417,
  long: 30.56225,
  isCompleted: false,
);

final CheckPoint muromec10 = CheckPoint(
  name: 'Момент',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_10.jpeg',
  color: Colors.black,
  lat: 50.51214,
  long: 30.55427,
  isCompleted: false,
);

final CheckPoint muromec11 = CheckPoint(
  name: 'Пляж',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_11.jpeg',
  color: Colors.black,
  lat: 50.50963,
  long: 30.54964,
  isCompleted: false,
);

final CheckPoint muromec12 = CheckPoint(
  name: 'Бобрівня',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_12.jpeg',
  color: Colors.black,
  lat: 50.51098,
  long: 30.54363,
  isCompleted: false,
);

final CheckPoint muromec13 = CheckPoint(
  name: 'Півфінал',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_13.jpeg',
  color: Colors.black,
  lat: 50.49773,
  long: 30.54957,
  isCompleted: false,
);

final CheckPoint muromec14 = CheckPoint(
  name: 'Фінал',
  image: 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_mission_muromec_pic_map_14.jpg',
  color: Colors.black,
  lat: 50.49653,
  long: 30.5473,
  isCompleted: false,
);

List<CheckPoint> checkPoints14 = [
  muromec1,
  muromec2,
  muromec3,
  muromec4,
  muromec5,
  muromec6,
  muromec7,
  muromec8,
  muromec9,
  muromec10,
  muromec11,
  muromec12,
  muromec13,
  muromec14,
];
