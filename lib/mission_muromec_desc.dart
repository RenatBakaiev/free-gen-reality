import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

class MissionsMuromecDesc extends StatefulWidget {
  @override
  _MissionsMuromecDescState createState() => _MissionsMuromecDescState();
}

class _MissionsMuromecDescState extends State<MissionsMuromecDesc> {
  bool isFavorite = false;
  bool isApiCallProcess = false;

  AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();
  bool isPlaying = false;
  void _getAudio() async {
    var url =
        // "http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-19_startpage.mp3";
        "http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-21_startpage2.mp3";
    var res = await audioPlayer.play(url, isLocal: true);
    if (res == 1) {
      setState(() {
        isPlaying = true;
      });
    }
  }

  void _stopFile() {
    audioPlayer?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer.pause();
    } else {
      audioPlayer.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  @override
  void initState() {
    _getAudio();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    Future<void> share() async {
      await FlutterShare.share(
        title: 'Завантажуйте новий додаток і спробуйте виконати місію \n' +
            'Муромець race',
        linkUrl: 'https://google.com/',
      );
    }

    showWindowQuizDone() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Завдання пройдене',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Text(
                          'Ви не можете повторно пройти це завдання',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                            height: 1.4,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Закрити",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 1.05),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 85,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          margin: EdgeInsets.only(top: 250), // change height
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0)),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 5,
                            ),
                            child: Text(
                              'Муромець race',
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              height: 260,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  // borderRadius: BorderRadius.only(
                                  //     bottomRight: Radius.circular(10.0),
                                  //     bottomLeft: Radius.circular(10.0)),
                                  image: DecorationImage(
                                image: NetworkImage(
                                    'http://generation-admin.ehub.com.ua/api/file/downloadFile/2021-01-15_Muromec_desc.gif'),
                                // arguments['image']),
                                fit: BoxFit.cover,
                              )),
                              child: Stack(
                                fit: StackFit.expand,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(
                                                  left: 10,
                                                  top: 35,
                                                ),
                                                child: IconButton(
                                                  icon: Image.asset(
                                                    'assets/fg_images/6_home_search_back.png',
                                                    width: 13.16,
                                                    height: 25,
                                                  ),
                                                  onPressed: () {
                                                    _stopFile();
                                                    Navigator.pushNamed(context,
                                                        '/5_myBottomBar.dart');
                                                  },
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 35),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    GestureDetector(
                                                      behavior: HitTestBehavior
                                                          .opaque,
                                                      onTap: () {
                                                        setState(() {
                                                          isFavorite =
                                                              !isFavorite;
                                                        });
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            right: 20),
                                                        child: Image.asset(
                                                          isFavorite
                                                              ? 'assets/fg_images/9_favorites_icon.png'
                                                              : 'assets/fg_images/sportGames_icon_favorite.png',
                                                          width: 19.25,
                                                          height: 27.5,
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      behavior: HitTestBehavior
                                                          .opaque,
                                                      onTap: () {
                                                        Navigator.pushNamed(
                                                            context,
                                                            '/7_map.dart');
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            right: 20),
                                                        child: Image.asset(
                                                          'assets/fg_images/sportGames_icon_map.png',
                                                          width: 18.75,
                                                          height: 23.75,
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      behavior: HitTestBehavior
                                                          .opaque,
                                                      onTap: () {
                                                        share();
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            right: 20),
                                                        child: Image.asset(
                                                          'assets/fg_images/6_home_logo_share.png',
                                                          width: 24.4,
                                                          height: 26.28,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  pausePlay();
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                    right: 15,
                                                  ),
                                                  child: Image.asset(
                                                    !isPlaying
                                                        ? 'assets/fg_images/audio_play.png'
                                                        : 'assets/fg_images/audio_pause.png',
                                                    height: 50,
                                                    width: 50,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                              left: 20,
                                              bottom: 15,
                                              right: 20,
                                            ),
                                            child:
                                                // Text('')
                                                Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                SizedBox(width: 38),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                            ),
                                            height: 40,
                                            child: Container(),
                                          ),
                                          // ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      child: ClipRect(
                                        child: BackdropFilter(
                                          filter: ui.ImageFilter.blur(
                                            sigmaX: 3.0,
                                            sigmaY: 3.0,
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              right: 20,
                                              left: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0),
                                              ),
                                            ),
                                            alignment: Alignment.center,
                                            height: 40.0,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/6_home_logo_time.png',
                                                      width: 18,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      '180',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' хвилин',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 7.5,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 15,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      '14',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' об\'єктів',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                top: 20,
                                bottom: 20,
                              ),
                              child: Text(
                                'Опис',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                              ),
                              child: Text(
                                ' Чули про 10 000 кроків? 👀\n У цій місії їх буде більше, бо на вас чекає траса на 8-11 км по мальовничим місцям парку. Маршрут пролягає через піски, поля, річки - тож не завадить зручне спортивне взуття. Перед походом перевір наявність інтернету, заряд, погоду та воду.\n Ця подорож пішки може зайняти 3 години, а на тих, хто швидше пройде шлях, потрапить у топ-10 та на всіх точках зачекіниться - чекає подарунок у місцевому кафе 😜',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 0.75,
                                  height: 1.3,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Почати Місію",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () async {
                  _stopFile();
                  final SharedPreferences sharedPreferences =
                      await SharedPreferences.getInstance();
                  var res1 = sharedPreferences.get('muromecStep1Done');
                  var res2 = sharedPreferences.get('muromecStep2Done');
                  var res3 = sharedPreferences.get('muromecStep3Done');
                  var res4 = sharedPreferences.get('muromecStep4Done');
                  var res5 = sharedPreferences.get('muromecStep5Done');
                  var res6 = sharedPreferences.get('muromecStep6Done');
                  var res7 = sharedPreferences.get('muromecStep7Done');
                  var res8 = sharedPreferences.get('muromecStep8Done');
                  var res9 = sharedPreferences.get('muromecStep9Done');
                  var res10 = sharedPreferences.get('muromecStep10Done');
                  var res11 = sharedPreferences.get('muromecStep11Done');
                  var res12 = sharedPreferences.get('muromecStep12Done');
                  var res13 = sharedPreferences.get('muromecStep13Done');
                  var res14 = sharedPreferences.get('muromecStep14Done');
                  if (res1 == true &&
                      res2 == true &&
                      res3 == true &&
                      res4 == true &&
                      res5 == true &&
                      res6 == true &&
                      res7 == true &&
                      res8 == true &&
                      res9 == true &&
                      res10 == true &&
                      res11 == true &&
                      res12 == true &&
                      res13 == true &&
                      res14 == true) {
                    showWindowQuizDone();
                    setState(() {
                      isApiCallProcess = false;
                    });
                  } else {
                    setState(() {
                      isApiCallProcess = false;
                    });
                    Navigator.pushNamed(context, '/mission_muromec_map.dart');
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
