import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;

class ErrorMissionPromocode extends StatefulWidget {
  @override
  _ErrorMissionPromocodeState createState() => _ErrorMissionPromocodeState();
}

class _ErrorMissionPromocodeState extends State<ErrorMissionPromocode> {
  bool isFavorite = false;
  bool isPromoFieldVisible = false;
  final formKey = GlobalKey<FormState>();
  TextEditingController promocodeTextEditingController =
      new TextEditingController();
  bool isPromoRight = false;
  bool missionUnlocked = false;

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    Future<void> share() async {
      await FlutterShare.share(
        title: 'Загружай новое приложение и попробуйте выполнить миссию \n' +
            arguments['name'],
        // text: 'Example share text',
        linkUrl: 'https://google.com/',
        // chooserTitle: 'Example Chooser Title'
      );
    }

    showUnlockWindow() {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                  insetPadding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Stack(
                    overflow: Overflow.visible,
                    children: [
                      Container(
                        // width: MediaQuery.of(context).size.width - 100,
                        height: 470,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(
                                top: 20,
                              ),
                              child: Text(
                                'Поздравляем!',
                                style: TextStyle(
                                  color: Hexcolor('#1E2E45'),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Arial',
                                ),
                              ),
                            ),
                            Expanded(
                              child: SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 15,
                                        bottom: 15,
                                      ),
                                      child: Text(
                                        'Вы разблокировали миссию',
                                        style: TextStyle(
                                          color: Hexcolor('#59B32D'),
                                          fontSize: 20,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 20,
                                        right: 20,
                                      ),
                                      width: MediaQuery.of(context).size.width,
                                      height: 165,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                          image: DecorationImage(
                                            image:
                                                AssetImage(arguments['image']),
                                            fit: BoxFit.cover,
                                          )),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 15,
                                        bottom: 15,
                                      ),
                                      child: Text(
                                        arguments['name'],
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 20,
                                          fontWeight: FontWeight.w700,
                                          fontFamily: 'Arial',
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Text(
                                      'Вам доступна новая миссия',
                                      style: TextStyle(
                                        color: Hexcolor('#747474'),
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                        fontFamily: 'Arial',
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 20, right: 20, left: 20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            45,
                                    child: RaisedButton(
                                      child: Text(
                                        "Закрыть",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      color: Hexcolor('#BEBEBE'),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(14.0)),
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, '/5_myBottomBar.dart');
                                        setState(() {
                                          missionUnlocked = true;
                                        });
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            45,
                                    child: RaisedButton(
                                      child: Text(
                                        "Подробнее",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      color: Hexcolor('#FE6802'),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(14.0)),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                        setState(() {
                                          missionUnlocked = true;
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: -130,
                        right: -30,
                        child: Image.asset(
                          'assets/fg_images/error_mission_icon_unlock.png',
                          width: 200,
                          height: 200,
                        ),
                      )
                    ],
                  ));
            });
          });
    }

    showLockedWindowWithPromocode() {
      showDialog(
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          barrierDismissible: false,
          context: context,
          builder: (context) {
            var _blankFocusNode2 = new FocusNode();
            return StatefulBuilder(builder: (context, setState) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    FocusScope.of(context).requestFocus(_blankFocusNode2);
                  },
                  child: Container(
                    // width: MediaQuery.of(context).size.width - 100,
                    height: 590,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Миссия недоступна',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Arial',
                            ),
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 20,
                                    bottom: 15,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Миссия доступна только',
                                        style: TextStyle(
                                          color: Hexcolor('#747474'),
                                          fontSize: 20,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Text(
                                            'по ',
                                            style: TextStyle(
                                              color: Hexcolor('#747474'),
                                              fontSize: 20,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                          Text(
                                            'промокоду',
                                            style: TextStyle(
                                              color: Hexcolor('#FF1E1E'),
                                              fontSize: 20,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  height: 165,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      image: DecorationImage(
                                        image: AssetImage(arguments['image']),
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Text(
                                    arguments['name'],
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'Arial',
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        // height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                45,
                                        child: Form(
                                          key: formKey,
                                          child: TextFormField(
                                            onChanged: (val) {},
                                            validator: (value) {
                                              return value == 'FREEGEN'
                                                  ? null
                                                  : 'Неверный промокод';
                                            },
                                            controller:
                                                promocodeTextEditingController,
                                            textAlign: TextAlign.left,
                                            decoration: InputDecoration(
                                              hintText: "Промокод",
                                              contentPadding:
                                                  new EdgeInsets.only(
                                                      left: 20,
                                                      top: 20.5,
                                                      bottom: 20.5),
                                              hintStyle: TextStyle(
                                                color: Hexcolor('#D3D3D3'),
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: 'Arial',
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(14.0),
                                                borderSide: BorderSide(
                                                  color: Hexcolor(isPromoRight
                                                      ? '#59B32D'
                                                      : '#FE6802'),
                                                  width: 1,
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(14.0),
                                                borderSide: BorderSide(
                                                  color: Hexcolor('#E6E6E6'),
                                                  width: 1,
                                                ),
                                              ),
                                              focusedErrorBorder:
                                                  OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(14.0),
                                                borderSide: BorderSide(
                                                  color: Colors.red,
                                                  width: 1,
                                                ),
                                              ),
                                              errorBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(14.0),
                                                borderSide: BorderSide(
                                                  color: Colors.red,
                                                  width: 1,
                                                ),
                                              ),
                                              fillColor: Colors.white,
                                              filled: true,
                                            ),
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial',
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                45,
                                        child: RaisedButton(
                                          child: Text(
                                            "Применить",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w700,
                                              fontFamily: 'Arial',
                                            ),
                                          ),
                                          color: Hexcolor('#FE6802'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () {
                                            if (formKey.currentState
                                                .validate()) {
                                              setState(() {
                                                isPromoRight = true;
                                              });
                                              // setState(() {
                                              //   questsDifferent[index]
                                              //       .isLocked = false;
                                              // });
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                isPromoRight
                                    ? Container(
                                        margin: EdgeInsets.only(
                                          left: 40,
                                          top: 7,
                                          bottom: 15,
                                        ),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          'Правильный промокод',
                                          style: TextStyle(
                                            color: Hexcolor('#59B32D'),
                                            fontFamily: 'Arial',
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ))
                                    : Container(
                                        height: 15,
                                      ),
                                !isPromoRight
                                    ? SizedBox(
                                        height: 30,
                                      )
                                    : SizedBox(
                                        height: 15,
                                      )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            child: Text(
                              "Закрыть",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Arial',
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              if (isPromoRight) {
                                showUnlockWindow();
                                setState(() {
                                  missionUnlocked = true;
                                });
                              } else {
                                Navigator.pushNamed(
                                    context, '/5_myBottomBar.dart');
                              }
                              // Navigator.of(context).pop();
                              // Navigator.pushNamed(context, '/5_myBottomBar.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
          });
    }

    return Scaffold(
        body: Stack(
      overflow: Overflow.visible,
      children: [
        SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              color: Hexcolor('#FFFFFF'),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height - 0,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 20),
                              margin: EdgeInsets.only(top: 250),
                              alignment: Alignment.centerLeft,
                              width: MediaQuery.of(context).size.width,
                              height: 70,
                              decoration: BoxDecoration(
                                color: Hexcolor('#7D5AC2'),
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10.0),
                                    bottomRight: Radius.circular(10.0)),
                              ),
                              child: Container(
                                margin: EdgeInsets.only(
                                  top: 5,
                                ),
                                child: Text(
                                  arguments['name'],
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                                // margin: EdgeInsets.only(top: 110),
                                height: 260,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    // borderRadius: BorderRadius.only(
                                    //     bottomRight: Radius.circular(10.0),
                                    //     bottomLeft: Radius.circular(10.0)),
                                    image: DecorationImage(
                                  image: AssetImage(
                                      // 'assets/fg_images/sportGames_pic_moto.jpg'),
                                      arguments['image']),
                                  fit: BoxFit.cover,
                                )),
                                child: Stack(fit: StackFit.expand, children: [
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: 10,
                                              top: 35,
                                            ),
                                            child: IconButton(
                                              icon: Image.asset(
                                                'assets/fg_images/6_home_search_back.png',
                                                width: 13.16,
                                                height: 25,
                                              ),
                                              onPressed: () {
                                                Navigator.pushNamed(context,
                                                    '/5_myBottomBar.dart');
                                              },
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 35),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    setState(() {
                                                      isFavorite = !isFavorite;
                                                    });
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      isFavorite
                                                          ? 'assets/fg_images/9_favorites_icon.png'
                                                          : 'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 19.25,
                                                      height: 27.5,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    Navigator.pushNamed(
                                                        context, '/7_map.dart');
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/fg_images/sportGames_icon_map.png',
                                                      width: 18.75,
                                                      height: 23.75,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    share();
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_logo_share.png',
                                                      width: 24.4,
                                                      height: 26.28,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                              left: 20,
                                              bottom: 15,
                                              right: 20,
                                            ),
                                            child:
                                                // Text('')
                                                Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Image.asset(
                                                  !missionUnlocked
                                                      ? 'assets/fg_images/6_home_logo_lock.png'
                                                      : 'assets/fg_images/6_home_logo_unlock.png',
                                                  width: 38,
                                                  height: 38,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      '0',
                                                      style: TextStyle(
                                                        fontSize: 36,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w700,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' ' +
                                                          '/ ' +
                                                          arguments['objects']
                                                              .toString(),
                                                      style: TextStyle(
                                                        fontSize: 24,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                            ),
                                            height: 40,
                                            child: Container(),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      child: ClipRect(
                                        child: BackdropFilter(
                                          filter: ui.ImageFilter.blur(
                                            sigmaX: 3.0,
                                            sigmaY: 3.0,
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              right: 20,
                                              left: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0),
                                              ),
                                            ),
                                            alignment: Alignment.center,
                                            height: 40.0,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/6_home_logo_time.png',
                                                      width: 18,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      arguments['time']
                                                          .toString(),
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      arguments['time'] != 1
                                                          ? ' минут'
                                                          : ' минута',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 7.5,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 15,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      arguments['objects']
                                                          .toString(),
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      arguments['objects'] != 1
                                                          ? ' объекта'
                                                          : ' объект',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 7.5,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/6_home_icon_flag.png',
                                                      width: 11,
                                                      height: 18.5,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      arguments['distance']
                                                              .toString() +
                                                          ' км',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' до старта',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ])),
                          ],
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 40,
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                    bottom: 20,
                                    top: 20,
                                  ),
                                  child: Text(
                                    'Описание',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 18,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    bottom: 20,
                                  ),
                                  child: Text(
                                    arguments['desc'],
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                ///
              ],
            ),
          ),
        ),
        Positioned(
          top: MediaQuery.of(context).size.height - 80,
          left: 20,
          child: !missionUnlocked
              ? Container(
                  margin: EdgeInsets.only(bottom: 20),
                  height: 60,
                  width: MediaQuery.of(context).size.width - 40,
                  child: RaisedButton(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 16,
                          ),
                          Text(
                            "Начать Миссию",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Image.asset(
                            'assets/fg_images/error_mission_icon.png',
                            width: 16,
                            height: 20,
                          ),
                        ]),
                    color: Hexcolor('#BEBEBE'),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(14.0)),
                    onPressed: () {
                      showLockedWindowWithPromocode();
                      // Navigator.pushNamed(context, '/questInfo.dart');
                      // Navigator.pushNamed(context, '/run_map_quest.dart');
                      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RunMapQuest()));
                    },
                  ),
                )
              : Container(
                  margin: EdgeInsets.only(bottom: 25),
                  height: 60,
                  width: MediaQuery.of(context).size.width - 40,
                  child: RaisedButton(
                    child: Text(
                      "Начать Миссию",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    color: Hexcolor('#FE6802'),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(14.0)),
                    onPressed: () {
                      Navigator.pushNamed(context, '/7_map.dart');
                    },
                  ),
                ),
        )
      ],
    ));
  }
}
