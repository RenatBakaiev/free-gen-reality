import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '10_profile_json.dart';

class MissionMuromecQuestDone extends StatefulWidget {
  @override
  _MissionMuromecQuestDoneState createState() =>
      _MissionMuromecQuestDoneState();
}

class _MissionMuromecQuestDoneState extends State<MissionMuromecQuestDone> {
  AudioCache cache; // you have this
  AudioPlayer player; // create this

  void _playFile() async {
    player = await AudioCache()
        .play("music/quest_done_coins.mp3"); // assign player here
  }

  bool isLoading = false;
  int elapsedtimeFromDarabase = 0;
  var userEmail;
  String elapsedTime = '';

  getUserData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    print(email);
    setState(() {
      userEmail = email;
    });
    print(token);
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';
    print(url);

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
        print(response.body);

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        print(responseJson);

        email = responseJson["email"];
        print(email);

        setState(() {
          if (responseJson["timer"] != null) {
            elapsedtimeFromDarabase = responseJson["timer"];

            transformMilliSeconds(int milliseconds) {
              int hundreds = (milliseconds / 10).truncate();
              int seconds = (hundreds / 100).truncate();
              int minutes = (seconds / 60).truncate();
              int hours = (minutes / 60).truncate();

              String hoursStr = (hours % 60).toString().padLeft(2, '0');
              String minutesStr = (minutes % 60).toString().padLeft(2, '0');
              String secondsStr = (seconds % 60).toString().padLeft(2, '0');

              return "$hoursStr:$minutesStr:$secondsStr";
            }

            elapsedTime = transformMilliSeconds(elapsedtimeFromDarabase);
          }
          isLoading = false;
        });
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  @override
  void initState() {
    getUserData();
    super.initState();
    _playFile();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                    ),
                    child: Text(
                      'Вітаємо\nмісія виконана',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.w900,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Stack(
                          overflow: Overflow.visible,
                          children: [
                            Image.asset(
                              'assets/fg_images/quest_done_lightning2.png',
                            ),
                            Positioned(
                              right: 70,
                              top: 80,
                              child: RotationTransition(
                                turns: AlwaysStoppedAnimation(-21 / 360),
                                child: Text(
                                  "100",
                                  style: TextStyle(
                                    fontSize: 40,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              right: 20,
                              top: 110,
                              child: RotationTransition(
                                turns: AlwaysStoppedAnimation(-21 / 360),
                                child: Text(
                                  "TUS",
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                                bottom: -130,
                                left:
                                    MediaQuery.of(context).size.width / 2 - 125,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/fg_images/quest_done_pic.png',
                                      height: 250,
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ]),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          // top: 20,
                          ),
                      child: Text(elapsedTime,
                          style: TextStyle(
                            fontSize: 25.0,
                            color: Hexcolor('#FE6802'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w600,
                          )),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        bottom: 20,
                        right: 20,
                        left: 20,
                        top: 20,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 2 - 25,
                            child: RaisedButton(
                              child: Text(
                                "Головна",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, '/5_myBottomBar.dart');
                              },
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 2 - 25,
                            child: RaisedButton(
                              child: Text(
                                "Місце в рейтингу",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, '/mission_muromec_rating.dart');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
            color: Hexcolor('#7D5AC2'),
          ),
        ),
      ),
    );
  }
}
