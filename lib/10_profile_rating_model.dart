class User {
  final String name;
  final String image;
  final String rating;

  User ({
    this.name,
    this.image, 
    this.rating,
  });
}