import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
// import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
import 'package:http/http.dart' as http;
import '10_profile_json.dart';
import 'dart:convert';
import '6_home_steps_json.dart';

class MissionStepScan extends StatefulWidget {
  @override
  _MissionStepScanState createState() => _MissionStepScanState();
}

class _MissionStepScanState extends State<MissionStepScan> {
  bool isApiCallProcess = false;
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  QRViewController controller;

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "deposit 20 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('+20TUS done');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  bool isLoading = false;
  int stepId;
  String stepCorrectAnswer = '';
  bool stepCanBeSkipped;
  double stepPoints;
  var stepCurrency;

  Future<Stup> getStepData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    String token = sharedPreferences.get("token");

    String url = 'http://generation-admin.ehub.com.ua/api/step/' + '$stepId';
    String urlCurrencies =
        'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies';

    print(url);
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });

      final responseCurrencies = await http.get(urlCurrencies, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });

      print(responseCurrencies.statusCode);
      Map<String, dynamic> responseJsonCurrencies =
          jsonDecode(responseCurrencies.body);

      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          stepCorrectAnswer =
              utf8.decode(responseJson["correctAnswer"].runes.toList());
          stepCanBeSkipped = responseJson["canBeSkipped"];
          stepPoints = responseJson["points"];
          print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
          print(stepCorrectAnswer);
          isLoading = false;
          for (int i = 0;
              i < responseJsonCurrencies["publication"].length;
              i++) {
            if (responseJson["currencyType"] ==
                responseJsonCurrencies["publication"][i]["id"].toString()) {
              stepCurrency =
                  responseJsonCurrencies["publication"][i]["titleUa"];
              print(stepCurrency);
            }
          }
        });
      } else {
        print('error');
      }
    }

    getMission();
    return Stup();
  }

  @override
  void initState() {
    // _getAudio();
    getStepData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Урааа',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        stepPoints != 0 && stepPoints != null
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    'Вам нараховано ',
                                    style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontSize: 20,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: 1.089,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    stepPoints.toInt().toString() +
                                        ' ' +
                                        stepCurrency.toString(),
                                    style: TextStyle(
                                      color: Hexcolor('#58B12D'),
                                      fontSize: 20,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w900,
                                      letterSpacing: 1.089,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              )
                            : Container(),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Перейти на наступний крок",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/6_mission_step_main.dart');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                ],
              ),
            );
          });
    }

    Future<User> changeTus(
        double value, String operation, String typeCurrency) async {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      String token = sharedPreferences.get("token");
      int id = sharedPreferences.get("id");
      // setState(() {
      //   isApiCallProcess = true;
      // });
      final http.Response response = await http.post(
        'http://generation-admin.ehub.com.ua/api/account/' +
            operation.toLowerCase() +
            '/' +
            '$id',
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(<String, dynamic>{
          "currency": typeCurrency,
          "operation": operation,
          "amount": value,
          "description": "quiz change"
        }),
      );
      print('http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id');

      if (response.statusCode == 200) {
        // setState(() {
        //   isApiCallProcess = false;
        // });
        print(response.body);
        print('quiz change done!!!');
      } else {
        // setState(() {
        //   isApiCallProcess = false;
        // });
        throw Exception('Failed to update User.');
      }
    }

    Future<Stup> setStepProgress() async {
      setState(() {
        isLoading = true;
      });
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var stepId = sharedPreferences.get('stepId');
      var usedId = sharedPreferences.get('id');

      var stepDone = sharedPreferences.get('step$stepId' + 'Done' + '$usedId');

      String token = sharedPreferences.get("token");
      String url =
          'http://generation-admin.ehub.com.ua/api/step/finish?user_id=' +
              '$usedId' +
              '&step_id=' +
              '$stepId';
      print(url);
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {
        print('Scan done');
        setState(() {
          isLoading = false;
        });
        if (stepPoints != null) {
          if (stepDone == '' || stepDone == null) {
            changeTus(stepPoints, "DEPOSIT", stepCurrency);
            sharedPreferences.setBool('step$stepId' + 'Done' + '$usedId', true);
            print('deposit done');
          } else {
            print('you cant to reseive many times');
          }
        }
        showInfoWindow();
        // Navigator.pushNamed(context, '/6_mission_step_main.dart');
      } else {
        print('error to done info');
        setState(() {
          isLoading = false;
        });
      }
      return Stup();
    }

    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Stack(
              overflow: Overflow.visible,
              children: [
                Column(
                  children: [
                    Expanded(
                      flex: 9,
                      child: QRView(
                          key: qrKey,
                          overlay: QrScannerOverlayShape(
                            borderRadius: 10,
                            borderColor: Colors.red,
                            borderLength: 30,
                            borderWidth: 10,
                            cutOutSize: 300,
                          ),
                          onQRViewCreated: _onQRViewCreate),
                    ),
                  ],
                ),
                Positioned(
                  top: 65,
                  left: 15,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/fg_images/sportGames_icon_arrow_back.png',
                      width: 10,
                      height: 19,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                qrText != stepCorrectAnswer
                    ? Container()
                    : Positioned(
                        bottom: stepCanBeSkipped ? 60 : 20,
                        left: 20,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Hexcolor('#FE6802'),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                          ),
                          // color: Hexcolor('#FE6802'),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 40,
                          child: RaisedButton(
                            child: Text(
                              'Продовжити',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              // changeTus(20.0, "DEPOSIT");
                              // Navigator.pushNamed(
                              //     context, '/missions_12_scan1.dart'); // + 20 tus
                              // final SharedPreferences sharedPreferences =
                              //     await SharedPreferences.getInstance();
                              // int tus = sharedPreferences.get("tus");
                              // sharedPreferences.setInt('tus', tus + 20);
                              // showInfoWindow();

                              print('go next page');
                              setStepProgress();
                            },
                          ),
                        ),
                      ),
                stepCanBeSkipped
                    ? Positioned(
                        bottom: 20,
                        left: MediaQuery.of(context).size.width / 2 - 40,
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            // showInfoWindow();
                            // Navigator.pushNamed(context, '/missions_12_scan1.dart');
                            print('go next page');
                            setStepProgress();
                          },
                          child: Container(
                            // width: 110,
                            child: Center(
                                child: Text(
                              'Пропустити',
                              style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontFamily: 'Arial',
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                              ),
                            )),
                          ),
                        ),
                      )
                    : Container()
              ],
            ),
    );
  }

  //global variables:

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }
}
