import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileNews extends StatefulWidget {
  @override
  _ProfileNewsState createState() => _ProfileNewsState();
}

bool isLoading = true;

class _ProfileNewsState extends State<ProfileNews> {
  void _launchUrl(String i) async {
    if (await canLaunch(i)) {
      await launch(i);
    } else {
      throw 'Could not open Url';
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    return Scaffold(
      body: isLoading
          ? Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Text(
                            'Новини',
                            style: TextStyle(
                                fontSize: 37,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 20,
                      left: 20,
                      right: 20,
                      bottom: 17,
                    ),
                    width: MediaQuery.of(context).size.width,
                    height: 193,
                    child: Image.network(arguments['image'], fit: BoxFit.cover),
                    // child: Image.network('http://generation-admin.ehub.com.ua/api/file/downloadFile/cat.jpeg', fit: BoxFit.cover),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      // image: DecorationImage(
                      //   image: AssetImage(arguments['image']),
                      //   fit: BoxFit.cover,
                      // )
                    ),
                  ),
                  Container(
                    child: Expanded(
                        child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Container(
                              margin: EdgeInsets.only(
                                left: 30,
                                right: 30,
                              ),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            bottom: 15,
                                          ),
                                          child: Text(
                                            arguments['title'],
                                            style: TextStyle(
                                                color: Hexcolor('#484848'),
                                                fontSize: 18,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w600),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(
                                          bottom: 15,
                                        ),
                                        child: Text(
                                          arguments['time'],
                                          style: TextStyle(
                                            color: Hexcolor('#B9BCC4'),
                                            fontSize: 14,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      Html(
                                        data: arguments['text'] == null
                                            ? ''
                                            : arguments['text'],
                                        onLinkTap: (i) {
                                          _launchUrl(i);
                                        },
                                      )
                                      // Text(
                                      //   arguments['text'],
                                      //   style: TextStyle(
                                      //     color: Hexcolor('#484848'),
                                      //     fontSize: 16,
                                      //     fontFamily: 'AvenirNextLTPro-Regular',
                                      //   ),
                                      // )
                                    ]),
                              ),
                            ))),
                  ),
                ],
              ),
            )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            ),
    );
  }
}
