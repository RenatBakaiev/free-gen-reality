import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

class DescMusic extends StatefulWidget {
  @override
  _DescMusicState createState() => _DescMusicState();
}

class _DescMusicState extends State<DescMusic> {
  bool isFavorite = false;

  @override
  void initState() {
    super.initState();
    _playFile();
  }

  bool isPlaying = true;
  bool isPaused = false;
  AudioCache cache;
  AudioPlayer player;

  void _playFile() async {
    player = await AudioCache().play("music/main2.mp3");
  }

  void _stopFile() {
    player?.stop(); // stop the file like this
    print('music stopped');
  }

  void pausePlay() {
    if (isPaused && isPlaying) {
      player.resume();
    } else {
      player.pause();
    }
    setState(() {
      isPaused = !isPaused;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    Future<void> share() async {
      await FlutterShare.share(
        title: 'Загружай новое приложение и попробуйте выполнить миссию \n' +
            arguments['name'],
        // text: 'Example share text',
        linkUrl: 'https://google.com/',
        // chooserTitle: 'Example Chooser Title'
      );
    }

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#FFFFFF'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 95,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          margin: EdgeInsets.only(top: 250),
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0)),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 5,
                            ),
                            child: Text(
                              arguments['name'],
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                        ),
                        Container(
                            height: 260,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                // borderRadius: BorderRadius.only(
                                //     bottomRight: Radius.circular(10.0),
                                //     bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                              image: AssetImage(arguments['image']),
                              fit: BoxFit.cover,
                            )),
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            left: 10,
                                            top: 35,
                                          ),
                                          child: IconButton(
                                            icon: Image.asset(
                                              'assets/fg_images/6_home_search_back.png',
                                              width: 13.16,
                                              height: 25,
                                            ),
                                            onPressed: () {
                                              _stopFile();
                                              Navigator.pushNamed(context,
                                                  '/5_myBottomBar.dart');
                                            },
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 35),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  setState(() {
                                                    isFavorite = !isFavorite;
                                                  });
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    isFavorite
                                                        ? 'assets/fg_images/9_favorites_icon.png'
                                                        : 'assets/fg_images/sportGames_icon_favorite.png',
                                                    width: 19.25,
                                                    height: 27.5,
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  Navigator.pushNamed(
                                                      context, '/7_map.dart');
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    'assets/fg_images/sportGames_icon_map.png',
                                                    width: 18.75,
                                                    height: 23.75,
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  share();
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    'assets/fg_images/6_home_logo_share.png',
                                                    width: 24.4,
                                                    height: 26.28,
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  pausePlay();
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 20),
                                                  child: Image.asset(
                                                    isPlaying && !isPaused
                                                        ? 'assets/fg_images/0_splash_screen_icon_pause.png'
                                                        : 'assets/fg_images/0_splash_screen_icon_play.png',
                                                    width: 24,
                                                    height: 24,
                                                    color: Hexcolor('#FFFFFF'),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          margin: EdgeInsets.only(
                                            left: 20,
                                            bottom: 15,
                                            right: 20,
                                          ),
                                          child:
                                              // Text('')
                                              Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Image.asset(
                                                'assets/fg_images/6_home_logo_unlock.png',
                                                width: 38,
                                                height: 38,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    '0',
                                                    style: TextStyle(
                                                      fontSize: 36,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                  Text(
                                                    ' ' +
                                                        '/ ' +
                                                        arguments['objects']
                                                            .toString(),
                                                    style: TextStyle(
                                                      fontSize: 24,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                          ),
                                          height: 40,
                                          child: Container(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Container(
                                    child: ClipRect(
                                      child: BackdropFilter(
                                        filter: ui.ImageFilter.blur(
                                          sigmaX: 3.0,
                                          sigmaY: 3.0,
                                        ),
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: 20,
                                            left: 20,
                                          ),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              bottomRight:
                                                  Radius.circular(10.0),
                                              bottomLeft: Radius.circular(10.0),
                                            ),
                                          ),
                                          alignment: Alignment.center,
                                          height: 40.0,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                    'assets/fg_images/6_home_logo_time.png',
                                                    width: 18,
                                                    height: 18,
                                                    color: Hexcolor('#FE6802'),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    arguments['time']
                                                        .toString(),
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    arguments['time'] != 1
                                                        ? ' минут'
                                                        : ' минута',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                width: 7.5,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                    'assets/fg_images/sportGames_icon_favorite.png',
                                                    width: 15,
                                                    height: 18,
                                                    color: Hexcolor('#FE6802'),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    arguments['objects']
                                                        .toString(),
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    arguments['objects'] != 1
                                                        ? ' объекта'
                                                        : ' объект',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                width: 7.5,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                    'assets/fg_images/6_home_icon_flag.png',
                                                    width: 11,
                                                    height: 18.5,
                                                    color: Hexcolor('#FE6802'),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    arguments['distance']
                                                            .toString() +
                                                        ' км',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    ' до старта',
                                                    style: TextStyle(
                                                      fontSize: 13.5,
                                                      color: Colors.white,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                top: 20,
                              ),
                              child: Text(
                                'Описание',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                              ),
                              child: Text(
                                arguments['desc'],
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 14,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Начать Миссию",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  _stopFile();
                  Navigator.pushNamed(context, '/mission_music.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
