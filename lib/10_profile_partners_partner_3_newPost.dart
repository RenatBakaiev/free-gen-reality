import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';

void _launchUrl(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not open Url';
  }
}

void _launchCaller(int number) async {
  var url = 'tel:${number.toString()}';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not place Call';
  }
}

void _launchEmail(String emailId) async {
  var url = 'mailto:$emailId?subject=Hello';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not send email';
  }
}

class ProfilePartnersPartner3NewPost extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(
                  top: 50,
                  // left: 15,
                  // right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: SizedBox(
                        // height: 55,
                        // width: 40,
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/6_home_search_back.png',
                            width: 10,
                            height: 19,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                    Text(
                      'Новая почта',
                      style: TextStyle(
                          fontSize: 37,
                          color: Colors.white,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w900),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Expanded(
                  child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                        ),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 80,
                                // width: MediaQuery.of(context).size.width - 30,
                                // margin: EdgeInsets.only(left: 15, right: 15),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  // color: Hexcolor('#204586'),
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 15),
                                      child: CircleAvatar(
                                        radius: 23,
                                        backgroundColor: Colors.white,
                                        child: ClipOval(
                                          child: Image.asset(
                                            'assets/fg_images/10_profile_partners_icon_3.jpg',
                                            width: 36,
                                            height: 36,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text(
                                        'Новая почта',
                                        style: TextStyle(
                                          color: Hexcolor('#484848'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  // top: 20,
                                  // left: 15,
                                  // right: 15,
                                  bottom: 17,
                                ),
                                height: 193,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    image: DecorationImage(
                                      image: AssetImage(
                                          'assets/fg_images/10_profile_partners_partner_3.jpg'),
                                      fit: BoxFit.cover,
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                ),
                                child: Text(
                                  'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                                  style: TextStyle(
                                    color: Hexcolor('#484848'),
                                    fontSize: 16,
                                    fontFamily: 'Arial',
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 20,
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          // width: 120,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: 1.85,
                                              ),
                                              Image.asset(
                                                'assets/fg_images/10_profile_contacts_pic_phone.png',
                                                height: 17.80,
                                                width: 17.80,
                                              ),
                                              SizedBox(
                                                width: 10.0,
                                              ),
                                              Text(
                                                '',
                                                // 'Тел:',
                                                style: TextStyle(
                                                  color: Hexcolor('#747474'),
                                                  fontSize: 16,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        GestureDetector(
                                          behavior: HitTestBehavior.opaque,
                                          onTap: () {
                                            _launchCaller(
                                                0800500609);
                                          },
                                          child: Text(
                                            '0 800 500 609',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          // width: 120,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                'assets/fg_images/10_profile_contacts_pic_email.png',
                                                height: 15.0,
                                                width: 21.5,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                '',
                                                // 'Email:',
                                                style: TextStyle(
                                                  color: Hexcolor('#747474'),
                                                  fontSize: 16,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        GestureDetector(
                                          behavior: HitTestBehavior.opaque,
                                          onTap: () {
                                            _launchEmail(
                                                'info@novaposhta.com.ua');
                                          },
                                          child: Text(
                                            'info@novaposhta.com.ua',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                              // decoration: TextDecoration.underline,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          // width: 120,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: 1.75,
                                              ),
                                              Image.asset(
                                                'assets/fg_images/10_profile_contacts_pic_site.png',
                                                height: 18,
                                                width: 18,
                                              ),
                                              SizedBox(
                                                width: 11.75,
                                              ),
                                              Text(
                                                '',
                                                // 'Сайт:',
                                                style: TextStyle(
                                                  color: Hexcolor('#747474'),
                                                  fontSize: 16,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        GestureDetector(
                                          behavior: HitTestBehavior.opaque,
                                          onTap: () {
                                            _launchUrl(
                                                'https://novaposhta.ua/');
                                          },
                                          child: Text(
                                            'www.novaposhta.ua',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                              ),
                              // SizedBox(
                              //   height: 100,
                              // ),
                              // Container(
                              //   margin: EdgeInsets.only(
                              //     bottom: 15,
                              //     left: 15,
                              //     right: 15,
                              //   ),
                              //   child: Text(
                              //     'Тел: ' + '0 800 500 609',
                              //     style: TextStyle(
                              //         color: Hexcolor('#484848'),
                              //         fontSize: 16,
                              //         fontFamily: 'Arial',
                              //         fontWeight: FontWeight.w600),
                              //   ),
                              // ),
                              // Container(
                              //   margin: EdgeInsets.only(
                              //     bottom: 15,
                              //     left: 15,
                              //     right: 15,
                              //   ),
                              //   child: Text(
                              //     'Email: ' + 'info@novaposhta.com.ua',
                              //     style: TextStyle(
                              //         color: Hexcolor('#484848'),
                              //         fontSize: 16,
                              //         fontFamily: 'Arial',
                              //         fontWeight: FontWeight.w600),
                              //   ),
                              // ),
                            ]),
                      ))),
            ),
          ],
        ),
      ),
    );
  }
}
