import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '10_profile_json.dart';
import '10_profile_currencies_json.dart';
import '10_profile_rewards.dart';
import 'package:flutter_free_gen_reality/services/services.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool loading;
  String statusUser = '';
  String firstName = '';
  String lastName = '';
  String userEmail = '';
  String userName = '';
  double tusa = 0;
  String userGoogleName;
  String userFacebookName;
  double tus;
  var profileImageUrl;
  List currencies;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  getUserData() async {
    setState(() {
      loading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    String photolink = sharedPreferences.get("photo");
    String userStatus = sharedPreferences.get("userStatus");

    print(email);
    // print(token);
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';
    print(url);

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            //if (photolink != "" && photolink != null) {
            if (photolink != "") {
              profileImageUrl = responseJson["profileImageUrl"];
              // profileImageUrl = 'http://generation-admin.ehub.com.ua' +
              //       responseJson["profileImageUrl"].substring(21);
            } else {
              if (responseJson["profileImageUrl"] != "" &&
                  responseJson["profileImageUrl"] != null) {
                profileImageUrl = 'http://generation-admin.ehub.com.ua' +
                    responseJson["profileImageUrl"].substring(21);
              }
            }

            firstName = responseJson["firstName"];
            lastName = responseJson["lastName"];
            userEmail = responseJson["email"];
            userName = utf8.decode(responseJson["username"].runes.toList());
            currencies = responseJson["accounts"];
            statusUser = userStatus;
            // print(currencies);
            // print(
            //     '**************************************************************');
            // for (int i = 0; i < responseJson["accounts"].length; i++) {
            //   if (responseJson["accounts"][i]["currency"] == "TUS") {
            //     tus = responseJson["accounts"][i]["accountBalance"];
            //     print(tus);
            //   }
            // }
            // isLoading = false;
            loading = false;
          });
        }

        print(response.statusCode);
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  List<User> user;

  showLockProfileWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              // width: MediaQuery.of(context).size.width - 40,
              height: 630,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                  bottom: 10,
                                ),
                                child: Column(
                                  children: [
                                    Text(
                                      'Профіль користувача',
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 24,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      'недоступний',
                                      style: TextStyle(
                                        color: Hexcolor('#FF1E1E'),
                                        fontSize: 24,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                )),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 5,
                              ),
                              child: Text(
                                'Перейти в профіль можуть лише зареєстровані користувачі.',
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ])),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                    ),
                    child: Image.asset(
                      'assets/fg_images/3_layout_3_welcome.jpg',
                      height: 290,
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.only(bottom: 15, right: 20, left: 20),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: [
                            Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width / 2 - 45,
                              child: RaisedButton(
                                child: Text(
                                  "Реєстрація",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                color: Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, '/4_enter_register');
                                },
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width / 2 - 45,
                              child: RaisedButton(
                                child: Text(
                                  "Авторизація",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                color: Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                onPressed: () {
                                  Navigator.pushNamed(context, '/4_enter');
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(bottom: 20, right: 20, left: 20),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Відміна",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#BEBEBE'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  checkAuth() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    if (email == null) {
      print('Not authorized');
      showLockProfileWindow();
    }
  }

  checkFaceBookGoogle() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String googleName = sharedPreferences.get("googleName");
    String facebookName = sharedPreferences.get("facebookName");
    setState(() {
      userGoogleName = googleName;
      userFacebookName = facebookName;
    });
    getName();
  }

  String name;
  getName() {
    if (userGoogleName != '') {
      setState(() {
        name = userGoogleName;
      });
    }
    if (userFacebookName != '') {
      setState(() {
        name = userFacebookName;
      });
    }
  }

  bool isQuestionaireDone;

  checkQuestionaireDone() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    isQuestionaireDone = sharedPreferences.get("isQuestionaireDone");
  }

  List<Publication> currencies2;

  bool currencyImageLoading = true;
  getCurrenciesImage() {
    ServicesCurrencies.getCurrencies().then((list) {
      setState(() {
        currencies2 = list.publication;
        currencyImageLoading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    checkFaceBookGoogle();
    checkAuth();
    checkQuestionaireDone();

    loading = true;
    getUserData();
    getCurrenciesImage();

    // ServicesCurrencies.getCurrencies().then((list) {
    //   setState(() {
    //     currencies2 = list.publication;
    //     print(currencies2.length);
    //   });
    // });
  }

  final ScrollController _scrollController = ScrollController();

  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn _googleSignIn = new GoogleSignIn();
  // FacebookLogin _facebookLogin = new FacebookLogin();

  bool isSignIn = true;

  // Future<void> facebookLogOut() async {
  //   try {
  //     var facebookLogin = new FacebookLogin();
  //     await facebookLogin.logOut();
  //   } catch (e) {
  //     print(e.message);
  //   }
  // }

  static final FacebookLogin facebookSignIn = new FacebookLogin();

  String _message = 'Log in/out by pressing the buttons below.';

  num id = 535;

  Future<Null> _facebookLogOut() async {
    await facebookSignIn.logOut();
    _showMessage('Logged out.');
  }

  void _showMessage(String message) {
    setState(() {
      _message = message;
    });
  }

  Future<void> gooleSignout() async {
    await _auth.signOut().then((onValue) {
      _googleSignIn.signOut();
      setState(() {
        isSignIn = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              // alignment: Alignment.topCenter,
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 40,
                        left: 20,
                        right: 5,
                        // bottom: 33,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              // Navigator.pushNamed(
                              //     context, '/error_404_page.dart');
                              // Navigator.pushNamed(context, '/10_profile_ar.dart');
                            },
                            child: Text(
                              'Аккаунт',
                              style: TextStyle(
                                  fontSize: 37,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900),
                            ),
                          ),
                          Row(
                            children: [
                              // RaisedButton(
                              //   onPressed: () {  },
                              //   child: Text('+'),
                              // ),
                              // RaisedButton(
                              //   onPressed: () {  },
                              //   child: Text('-'),
                              // ),
                              // GestureDetector(
                              //     onTap: () {
                              //       _facebookLogOut().then((value) =>
                              //           Navigator.pushReplacementNamed(
                              //               context, '/4_enter'));
                              //       print('Facebook logged out!');
                              //       gooleSignout().then((value) =>
                              //           Navigator.pushReplacementNamed(
                              //               context, '/4_enter'));
                              //       print('Google logged out!');
                              //       Navigator.pushNamed(context, '/4_enter');
                              //     },
                              //     child: Icon(Icons.supervised_user_circle,
                              //         color: Colors.white)),
                              SizedBox(width: 45, height: 55, child: Container()
                                  // IconButton(
                                  //   icon: Image.asset(
                                  //     'assets/fg_images/10_profile_edit.png',
                                  //     width: 22,
                                  //     height: 22,
                                  //   ),
                                  //   onPressed: () {
                                  //     Navigator.pushNamed(
                                  //         context, '/10_profile_edit.dart');
                                  //     // _launchUrl('https://freegengame.web.app/');
                                  //     // const url = 'http://d66981q1.beget.tech/8bit/';
                                  //     // const url = 'https://freegengame.web.app/';
                                  //   },
                                  // ),
                                  ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 290,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 10,
                        left: 5,
                        right: 5,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: 70,
                              margin: EdgeInsets.only(right: 5, left: 5),
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    // onTap: () {
                                    //   Navigator.pushNamed(
                                    //       context, '/10_profile_edit.dart');
                                    // },
                                    child: profileImageUrl != null
                                        ? CircleAvatar(
                                            backgroundColor:
                                                Hexcolor('#F2F2F2'),
                                            radius: 35,
                                            backgroundImage:
                                                NetworkImage(profileImageUrl),
                                          )
                                        : CircleAvatar(
                                            backgroundColor:
                                                Hexcolor('#F2F2F2'),
                                            radius: 35,
                                            backgroundImage: AssetImage(
                                                'assets/fg_images/10_profile_edit_default_pic.png'),
                                          ),
                                  ),
                                  Container(
                                    height: 95,
                                    margin: EdgeInsets.only(left: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          userName == null
                                              ? name.toString()
                                              : userName,
                                          // (firstName == null ? '' : firstName)
                                          //  +
                                          //     ' ' +
                                          //     (lastName == null
                                          //         ? ''
                                          //         : lastName),
                                          style: TextStyle(
                                              color: Hexcolor('#FFFFFF'),
                                              fontSize: 21,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                              letterSpacing: 1.04),
                                        ),
                                        isQuestionaireDone == true
                                            ? Text(
                                                'Анкета заповнена',
                                                style: TextStyle(
                                                  color: Hexcolor('#FFFFFF'),
                                                  fontSize: 14,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              )
                                            : GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  Navigator.pushNamed(context,
                                                      '/10_profile_questionnaire.dart');
                                                },
                                                child: Container(
                                                    // width: 300,
                                                    child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      'Заповнити анкету ',
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#FFFFFF'),
                                                        fontSize: 14,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w700,
                                                      ),
                                                    ),
                                                    Text(
                                                      '(отримай 50 TUS)',
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#FE6802'),
                                                        fontSize: 14,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w700,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 15,
                                                    ),
                                                    Image.asset(
                                                      'assets/fg_images/6_home_filter_arrow.png',
                                                      width: 10,
                                                      height: 19,
                                                      color: Colors.white,
                                                    ),
                                                  ],
                                                )),
                                              ),
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.pushNamed(context,
                                                '/10_profile_faq.dart');
                                          },
                                          behavior: HitTestBehavior.opaque,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Text(
                                                'Ваш статус: ',
                                                style: TextStyle(
                                                  color: Hexcolor('#FFFFFF'),
                                                  fontSize: 14,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                              Text(
                                                statusUser,
                                                style: TextStyle(
                                                    color: Hexcolor('#FE6802'),
                                                    fontSize: 14,
                                                    fontFamily: 'Arial',
                                                    fontWeight: FontWeight.w700,
                                                    letterSpacing: 1),
                                              ),
                                            ],
                                          ),
                                        ),
                                        // GestureDetector(
                                        //   onTap: () {
                                        //     // Navigator.pushNamed(
                                        //     //     context, '/10_profile_faq.dart');
                                        //   },
                                        //   behavior: HitTestBehavior.opaque,
                                        //   child: Row(
                                        //     mainAxisAlignment:
                                        //         MainAxisAlignment.start,
                                        //     crossAxisAlignment:
                                        //         CrossAxisAlignment.end,
                                        //     children: [
                                        //       Text(
                                        //         'Ваш рівень: ',
                                        //         style: TextStyle(
                                        //             color: Hexcolor('#FFFFFF'),
                                        //             fontSize: 14,
                                        //             fontFamily: 'Arial',
                                        //             fontWeight: FontWeight.w400,
                                        //             letterSpacing: 1),
                                        //       ),
                                        //       Text(
                                        //         'Challenger',
                                        //         style: TextStyle(
                                        //             color: Hexcolor('#FE6802'),
                                        //             fontSize: 14,
                                        //             fontFamily: 'Arial',
                                        //             fontWeight: FontWeight.w700,
                                        //             letterSpacing: 1),
                                        //       ),
                                        //     ],
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            currencyImageLoading
                                ? Center(
                                    child: CircularProgressIndicator(
                                      backgroundColor: Hexcolor('#7D5AC2'),
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                        Hexcolor('#FE6802'),
                                      ),
                                    ),
                                  )
                                : Expanded(
                                    child: GridView.builder(
                                      scrollDirection: Axis.vertical,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        childAspectRatio: (126 / 80),
                                      ),
                                      physics: BouncingScrollPhysics(),
                                      itemCount: currencies.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        var currency = currencies[index];
                                        var currencyName = utf8.decode(
                                            currency["currency"]
                                                .runes
                                                .toList());
                                        var currencyBalance =
                                            currency["accountBalance"];
                                        var currencyImage;
                                        // print(currencyName);

                                        for (int i = 0;
                                            i < currencies2.length;
                                            i++) {
                                          if (currencyName ==
                                              currencies2[i]
                                                  .contentShortUa
                                                  .toUpperCase()) {
                                            currencyImage =
                                                link + currencies2[i].img;
                                          }
                                        }

                                        return Container(
                                          margin: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(10.0),
                                              topLeft: Radius.circular(10.0),
                                              topRight: Radius.circular(10.0),
                                              bottomRight:
                                                  Radius.circular(10.0),
                                            ),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 9),
                                                child: Image.network(
                                                  currencyImage == "" ||
                                                          currencyImage == null
                                                      ? '$link' + 'default.png'
                                                      : currencyImage,
                                                  width: 45,
                                                  height: 45,
                                                ),
                                                // child: CircleAvatar(
                                                //   radius: 22.5,
                                                //   backgroundColor: allRewards[index].backColor,
                                                //   child: ClipOval(
                                                //     child: Image.asset(allRewards[index].image, width: 45, height: 45, fit: BoxFit.cover,),

                                                //   )
                                                //   ),
                                                // backgroundImage:
                                                //     AssetImage(allRewards[index].image),
                                              ),
                                              SingleChildScrollView(
                                                physics:
                                                    BouncingScrollPhysics(),
                                                child: Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      8,
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(currencyName,
                                                          overflow:
                                                              TextOverflow.fade,
                                                          style: TextStyle(
                                                            color: Hexcolor(
                                                                '#696969'),
                                                            fontSize: 13,
                                                            fontFamily: 'Arial',
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          )),
                                                      SizedBox(
                                                        height: 6,
                                                      ),
                                                      Text(
                                                        currencyBalance
                                                            .toInt()
                                                            .toString(),
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 16,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                          ]),
                    ),
                  ),
                  Expanded(
                    // child: DraggableScrollbar.rrect(
                    //   controller: _scrollController,
                    //   backgroundColor: Hexcolor('#EB5C18'),
                    //   alwaysVisibleScrollThumb: true,
                    //   heightScrollThumb: 50,
                    //   padding: EdgeInsets.only(right: 10),
                    child: ListView.builder(
                      controller: _scrollController,
                      physics: BouncingScrollPhysics(),
                      itemCount: itemsForProfile.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () async {
                            if (itemsForProfile[index].link == '') {
                              // print('There will be function -logOut- in the future');
                              _facebookLogOut().then((value) =>
                                  Navigator.pushReplacementNamed(
                                      context, '/4_enter'));
                              print('Facebook logged out!');
                              gooleSignout().then((value) =>
                                  Navigator.pushReplacementNamed(
                                      context, '/4_enter'));
                              print('Google logged out!');
                              Navigator.pushNamed(context, '/4_enter');
                              final SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              sharedPreferences.remove('email');
                              print('email logged out!');
                            } else {
                              Navigator.pushNamed(
                                  context, itemsForProfile[index].link);
                            }
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 20, bottom: 20),
                            // height: MediaQuery.of(context).size.height,
                            child: Row(
                              children: [
                                CircleAvatar(
                                  radius: 22.5,
                                  backgroundColor: itemsForProfile[index].color,
                                  child: ClipRRect(
                                    child: Image.asset(
                                      itemsForProfile[index].icon,
                                      width: 22,
                                      height: 22,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20),
                                  child: Text(
                                    itemsForProfile[index].name,
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 17,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  // )
                ],
              ),
            ),
    );
  }
}
