import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
// import 'package:video_player/video_player.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

class Missions12Desc2 extends StatefulWidget {
  @override
  _Missions12Desc2State createState() => _Missions12Desc2State();
}

class _Missions12Desc2State extends State<Missions12Desc2> {
      AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();

  bool isPlaying = false;

  void _getAudio() async {
    var url =
        "http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_info_audio2.mp3";
     var res = await audioPlayer.play(url, isLocal: true);
      if (res == 1) {
        setState(() {
          isPlaying = true;
        });
      }
  }

  void _stopFile() {
    audioPlayer?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer.pause();
    } else {
      audioPlayer.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  bool isFavorite = false;
  // VideoPlayerController _controller;
  // Future<void> _initializeVideoPlayerFuture;

  void initState() {
    _getAudio();
    // _controller = VideoPlayerController.asset('assets/video/main.MP4');
    // _initializeVideoPlayerFuture = _controller.initialize();
    // _controller.setLooping(true);
    // _controller.setVolume(1.0);
    super.initState();
    // _controller.play();
  }

  // @override
  // void dispose() {
  //   _controller.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 80
                    //  - 85
                    ,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 260,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0)),
                              image: DecorationImage(
                                image: NetworkImage(
                                    'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_zz12_missions_desc2.gif'),
                                fit: BoxFit.cover,
                              )
                            ),
                            child: 
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                // right: 20,
                              ),
                              child: 
                              // GestureDetector(
                              //   onTap: () {
                              //     if (_controller.value.isPlaying) {
                              //       _controller.pause();
                              //     } else {
                              //       _controller.play();
                              //     }
                              //   },
                              //   child: FutureBuilder(
                              //     future: _initializeVideoPlayerFuture,
                              //     builder: (context, snapshot) {
                              //       if (snapshot.connectionState ==
                              //           ConnectionState.done) {
                              //         return AspectRatio(
                              //           aspectRatio:
                              //               _controller.value.aspectRatio,
                              //           child: VideoPlayer(_controller),
                              //         );
                              //       } else {
                              //         return Center(
                              //           child: CircularProgressIndicator(),
                              //         );
                              //       }
                              //     },
                              //   ),
                              // ),

                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(),
                                      GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                pausePlay();
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                  top: 30,
                                                  right: 15,
                                                ),
                                                child: Image.asset(
                                                  !isPlaying
                                                      ? 'assets/fg_images/audio_play.png'
                                                      : 'assets/fg_images/audio_pause.png',
                                                  height: 50,
                                                  width: 50,
                                                ),
                                              ),
                                            ),
                                    ],
                                  ),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.end,
                                  //   crossAxisAlignment: CrossAxisAlignment.end,
                                  //   children: [
                                  //     Text(
                                  //       '1',
                                  //       style: TextStyle(
                                  //         fontSize: 36,
                                  //         color: Colors.white,
                                  //         fontFamily: 'Arial',
                                  //         fontWeight: FontWeight.w700,
                                  //       ),
                                  //     ),
                                  //     Text(
                                  //       ' ' + '/ ' + '12',
                                  //       style: TextStyle(
                                  //         fontSize: 24,
                                  //         color: Colors.white,
                                  //         fontFamily: 'Arial',
                                  //         fontWeight: FontWeight.w400,
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Опис',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Gott sei Dank!\n(Слава Богу!)\nСловничок поповнено та підсумовано:\n\ndas Hüftgold - буквально перекладається як «золоті бочка». Ті щасливчики, у кого є трохи «зайвого» на боках, вважайте, що це золоті запаси!\nder Sarkasmus - сарказм\ndas Glück - щастя, везіння, удача\nder Kummerspeck - буквально «сумний бекон», та сама вага, яку можна набрати, коли заїдаєш свої печалі.\n\nMöge das neue Jahr die Freude, Glück und Gesundheit bringen!\n(Нехай Новий Рік принесе радість, щастя і' +
                                        "здоров'я" +
                                        '!',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: SizedBox(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_search_back.png',
                                    width: 10,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    _stopFile();
                                    Navigator.pushNamed(
                                        context, '/missions_12_quiz2.dart');
                                    // if (_controller.value.isPlaying) {
                                    //   _controller.pause();
                                    // }
                                  },
                                ),
                              ),
                            ),
                            Text(
                              'Інформація',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/run_map_quest_pic_gostart.png',
                                width: 22,
                                height: 24,
                              ),
                              onPressed: () {
                                _stopFile();
                                Navigator.pushNamed(
                                  context,
                                  '/missions_12_desc.dart',
                                );
                                // if (_controller.value.isPlaying) {
                                //   _controller.pause();
                                // }
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Далі",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  _stopFile();
                  Navigator.pushNamed(context, '/missions_12_scan_info2.dart');
                  // if (_controller.value.isPlaying) {
                  //   _controller.pause();
                  // }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
