//-----------------------------------------------SLIDER VARIANT-------------------------------------------------------
// import 'dart:io';
// import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/1_layout_slider_model.dart';
// import 'package:hexcolor/hexcolor.dart';
// import '10_profile_rewards.dart';

// class Layout1 extends StatefulWidget {
//   @override
//   _Layout1State createState() => _Layout1State();
// }

// class _Layout1State extends State<Layout1> {
//   List<SliderModel> slides = new List<SliderModel>();
//   int currentIndex = 0;

//   @override
//   void initState() {
//     super.initState();
//     slides = getSlides();
//   }

//   Widget pageIndexIndicator(bool isCurrentPage) {
//     return Container(
//       width: 13,
//       height: 13,
//       padding: EdgeInsets.symmetric(horizontal: 2),
//       margin: EdgeInsets.symmetric(horizontal: 17),
//       decoration: BoxDecoration(
//         color: isCurrentPage
//             ? Hexcolor('#FFFFFF')
//             : Hexcolor('#FFFFFF').withOpacity(0.3),
//         borderRadius: BorderRadius.circular(13),
//       ),
//     );
//   }

//   showYouReceiveRewardWindow() {
//     setState(() {
//       allRewards[5].quantity = allRewards[5].quantity + 10;
//     });
//     showDialog(
//         barrierDismissible: false,
//         barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
//         context: context,
//         builder: (context) {
//           return Dialog(
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(10),
//             ),
//             child: Container(
//               width: MediaQuery.of(context).size.width - 80,
//               height: 230,
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 children: [
//                   Container(
//                       alignment: Alignment.center,
//                       margin: EdgeInsets.only(
//                         top: 20,
//                         left: 10,
//                         right: 10,
//                       ),
//                       child: Column(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: [
//                             Container(
//                               margin: EdgeInsets.only(
//                                 bottom: 10,
//                               ),
//                               child: Text(
//                                 'Поздравляем!',
//                                 style: TextStyle(
//                                   color: Hexcolor('#58B12D'),
//                                   fontSize: 17,
//                                   fontFamily: 'Arial',
//                                   fontWeight: FontWeight.w900,
//                                   letterSpacing: 1.089,
//                                 ),
//                                 textAlign: TextAlign.center,
//                               ),
//                             ),
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.center,
//                               crossAxisAlignment: CrossAxisAlignment.end,
//                               children: [
//                                 Text(
//                                   'Вы получили ',
//                                   style: TextStyle(
//                                     color: Hexcolor('#000000'),
//                                     fontSize: 17,
//                                     fontFamily: 'Arial',
//                                     fontWeight: FontWeight.w900,
//                                     letterSpacing: 1.089,
//                                   ),
//                                   textAlign: TextAlign.center,
//                                 ),
//                                 Text(
//                                   '10 Foints',
//                                   style: TextStyle(
//                                     color: Hexcolor('#58B12D'),
//                                     fontSize: 17,
//                                     fontFamily: 'Arial',
//                                     fontWeight: FontWeight.w900,
//                                     letterSpacing: 1.089,
//                                   ),
//                                   textAlign: TextAlign.center,
//                                 ),
//                               ],
//                             ),
//                             Container(
//                               margin: EdgeInsets.only(
//                                 top: 10,
//                               ),
//                               child: Image.asset(
//                                 'assets/fg_images/10_profile_user_param_icon_foint2.png',
//                                 height: 50,
//                               ),
//                             ),
//                           ])),
//                   Container(
//                     margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Container(
//                           height: 60,
//                           width: MediaQuery.of(context).size.width,
//                           child: RaisedButton(
//                             child: Text(
//                               "Продолжить",
//                               style: TextStyle(
//                                 color: Colors.white,
//                                 fontSize: 17,
//                                 fontFamily: 'Arial',
//                                 fontWeight: FontWeight.w600,
//                               ),
//                             ),
//                             color: Hexcolor('#EB5C18'),
//                             shape: new RoundedRectangleBorder(
//                                 borderRadius: new BorderRadius.circular(14.0)),
//                             onPressed: () {
//                               Navigator.pushNamed(context, '/4_enter');
//                             },
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         backgroundColor: Hexcolor('#544474'),
//         body: PageView.builder(
//             itemCount: slides.length,
//             onPageChanged: (val) {
//               setState(() {
//                 currentIndex = val;
//               });
//             },
//             itemBuilder: (context, index) {
//               return SliderTile(
//                 image: slides[index].getImage(),
//                 imageBack: slides[index].getImageBack(),
//                 title: slides[index].getTitle(),
//                 text: slides[index].getText(),
//                 currentIndex: index,
//               );
//             }),
//         bottomSheet: currentIndex != slides.length - 1
//             ? Container(
//                 color: Hexcolor('#59477C'),
//                 height: Platform.isIOS ? 50 : 40,
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: [
//                     for (int i = 0; i < slides.length; i++)
//                       currentIndex == i
//                           ? pageIndexIndicator(true)
//                           : pageIndexIndicator(false),
//                   ],
//                 ),
//               )
//             : GestureDetector(
//                 onTap: () {
//                   showYouReceiveRewardWindow();
//                 },
//                 child: Container(
//                   alignment: Alignment.center,
//                   width: MediaQuery.of(context).size.width,
//                   color: Hexcolor('#EB5C18'),
//                   height: Platform.isIOS ? 50 : 40,
//                   child: Text(
//                     'ДАЛЕЕ',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontWeight: FontWeight.w600,
//                       fontFamily: 'AvenirNextLTPro-Regular',
//                     ),
//                   ),
//                 ),
//               ),
//       ),
//     );
//   }
// }

// class SliderTile extends StatefulWidget {
//   String image, imageBack, title, text;
//   int currentIndex;
//   SliderTile(
//       {this.image, this.imageBack, this.title, this.text, this.currentIndex});

//   @override
//   _SliderTileState createState() => _SliderTileState();
// }

// class _SliderTileState extends State<SliderTile> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: MediaQuery.of(context).size.height,
//       width: MediaQuery.of(context).size.width,
//       child: SingleChildScrollView(
//         physics: BouncingScrollPhysics(),
//         child: Container(
//           height: MediaQuery.of(context).size.height,
//           width: MediaQuery.of(context).size.width,
//           child: Image.asset(widget.imageBack,
//               height: MediaQuery.of(context).size.height,
//               width: MediaQuery.of(context).size.width,
//               fit: BoxFit.cover),
//         ),
//       ),
//     );
//   }
// }
//----------------------------------------------------------------------------------------------------------------------

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
// import 'package:easy_localization/easy_localization.dart';


class Layout1 extends StatefulWidget {
  @override
  _Layout1State createState() => _Layout1State();
}

class _Layout1State extends State<Layout1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      overflow: Overflow.visible,
      children: [
        Container(
          color: Hexcolor('#412A72'),
          child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                child: Image.asset(
                  'assets/fg_images/1_layout_1_back.jpg',
                  width: MediaQuery.of(context).size.width,
                ),
              )),
        ),
        Positioned(
            bottom: 0,
            
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot.png',
                    width: 10,
                    height: 10,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot2.png',
                    width: 10,
                    height: 10,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot2.png',
                    width: 10,
                    height: 10,
                  ),
                ],
              ),
              Row(children: [
                Container(
                  margin: EdgeInsets.only(bottom: 25, right: 25, top: 30, left: 25,),
                  height: 60,
                  width: MediaQuery.of(context).size.width - 50,
                  child: RaisedButton(
                    child: Text(
                      // "Далее",
                      "Далі",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    color: Hexcolor('#FE6802'),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(14.0)),
                    onPressed: () {
                      Navigator.pushNamed(context, '/2_layout_2');
                    },
                  ),
                ),
              ])
            ]))
      ],
    ));
  }
}
