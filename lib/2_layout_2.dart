import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Layout2 extends StatefulWidget {
  @override
  _Layout2State createState() => _Layout2State();
}

class _Layout2State extends State<Layout2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      overflow: Overflow.visible,
      children: [
        Container(
          color: Hexcolor('#412A72'),
          child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                child: Image.asset(
                  'assets/fg_images/2_layout_2_back.jpg',
                  width: MediaQuery.of(context).size.width,
                ),
              )),
        ),
        Positioned(
            bottom: 0,
            left: 25,
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot2.png',
                    width: 10,
                    height: 10,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot.png',
                    width: 10,
                    height: 10,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot2.png',
                    width: 10,
                    height: 10,
                  ),
                ],
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        bottom: 25,
                        top: 30,
                      ),
                      height: 60,
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      child: RaisedButton(
                        child: Text(
                          "Назад",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        color: Hexcolor('#BEBEBE'),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(14.0)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/1_layout_1');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 25),
                      height: 60,
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      child: RaisedButton(
                        child: Text(
                          // "Далее",
                          "Далі",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        color: Hexcolor('#FE6802'),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(14.0)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/3_layout_3');
                        },
                      ),
                    ),
                  ])
            ]))
      ],
    ));
  }
}
