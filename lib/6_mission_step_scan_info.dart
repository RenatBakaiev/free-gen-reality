import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:vibration/vibration.dart';
// import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'dart:async';
import '6_home_steps_json.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class MissionStepScanInfo extends StatefulWidget {
  @override
  _MissionStepScanInfoState createState() => _MissionStepScanInfoState();
}

class _MissionStepScanInfoState extends State<MissionStepScanInfo> {
  // Stopwatch watch = Stopwatch();
  // stopWatch() {
  //   setState(() {
  //     watch.stop();
  //     print(watch.elapsedMilliseconds); // its give to database
  //     watch.reset();
  //   });
  // }

  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

  AudioCache cache;
  AudioPlayer audioPlayer = new AudioPlayer();

  bool isPlaying = false;
  bool isApiCallProcess = false;

  void _getAudio() async {
    var url = "http://generation-admin.ehub.com.ua/api/file/downloadFile/" +
        "$stepSound";
    var res = await audioPlayer.play(url, isLocal: true);
    if (res == 1) {
      setState(() {
        isPlaying = true;
      });
    }
  }

  void _stopFile() {
    audioPlayer?.stop();
  }

  void pausePlay() {
    if (isPlaying) {
      audioPlayer.pause();
    } else {
      audioPlayer.resume();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  void _getVideo() {
    _controller = VideoPlayerController.network(
        'http://generation-admin.ehub.com.ua/api/file/downloadFile/$stepVideo');
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    _controller.play();
  }

  bool isLoading = false;
  String stepName = '';
  String stepDesc = '';
  String stepImage = '';
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';
  String stepSound = '';
  String stepVideo = '';

  Future<Stup> getStepData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    String token = sharedPreferences.get("token");
    String url = 'http://generation-admin.ehub.com.ua/api/step/' + '$stepId';
    print(url);
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          stepName = utf8.decode(responseJson["titleUa"].runes.toList());
          stepDesc = utf8.decode(responseJson["descriptionUa"].runes.toList());
          stepImage = responseJson["imageUrl"];

          stepVideo = responseJson["videoUrl"];
          if (stepVideo != "" || stepVideo != null) {
            _getVideo();
          }

          stepSound = responseJson["audioUrl"];
          if ((stepSound != "" || stepSound != null) &&
              (stepVideo == "" || stepVideo == null)) {
            _getAudio();
          }

          isLoading = false;
        });
      } else {
        print('error');
      }
    }

    getMission();
    return Stup();
  }

  int submissionSteps;
  int submissionDoneSteps;
  bool loadingSteps = true;

  Future<Stup> checkAllStepsDone() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var usedId = sharedPreferences.get('id');
    var subMissionProgressId = sharedPreferences.get('subMissionProgressId');
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/step/progress/list?user_id=$usedId&subMissionProgress_id=$subMissionProgressId';

    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    List<dynamic> responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      List<String> finishedSteps = new List<String>();
      for (int i = 0; i < responseJson.length; i++) {
        if (responseJson[i]["trackStatus"] == 'FINISHED') {
          finishedSteps.add('FINISHED');
        }
      }
      setState(() {
        submissionSteps = responseJson.length;
        submissionDoneSteps = finishedSteps.length;
        loadingSteps = false;
      });
    } else {
      print('error to get steps');
      setState(() {
        loadingSteps = false;
      });
    }
    return Stup();
  }

  @override
  void initState() {
    checkAllStepsDone();
    getStepData();
    Vibration.vibrate(duration: 1000);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Stack(
                    overflow: Overflow.visible,
                    children: [
                      Positioned(
                        child: Container(
                          height: MediaQuery.of(context).size.height - 80
                          //  - 85
                          ,
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                stepVideo == "" || stepVideo == null
                                    ? Container(
                                        margin: EdgeInsets.only(top: 90),
                                        height: 225,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0)),
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                  stepImage == "" ||
                                                          stepImage == null
                                                      ? '$link' + 'default.png'
                                                      : '$link' + stepImage),
                                              fit: BoxFit.cover,
                                            )),
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            bottom: 20,
                                            right: 20,
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(),
                                                  stepSound == "" ||
                                                          stepSound == null
                                                      ? Container()
                                                      : GestureDetector(
                                                          behavior:
                                                              HitTestBehavior
                                                                  .opaque,
                                                          onTap: () {
                                                            pausePlay();
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 30,
                                                            ),
                                                            child: Image.asset(
                                                              !isPlaying
                                                                  ? 'assets/fg_images/audio_play.png'
                                                                  : 'assets/fg_images/audio_pause.png',
                                                              height: 50,
                                                              width: 50,
                                                            ),
                                                          ),
                                                        ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(),
                                                  loadingSteps
                                                      ? CircularProgressIndicator(
                                                          backgroundColor:
                                                              Hexcolor(
                                                                  '#7D5AC2'),
                                                          valueColor:
                                                              new AlwaysStoppedAnimation<
                                                                  Color>(
                                                            Hexcolor('#FE6802'),
                                                          ),
                                                        )
                                                      : Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Text(
                                                              submissionDoneSteps
                                                                  .toString(),
                                                              style: TextStyle(
                                                                fontSize: 36,
                                                                color: Colors
                                                                    .white,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                            Text(
                                                              ' ' +
                                                                  '/ ' +
                                                                  submissionSteps
                                                                      .toString(),
                                                              style: TextStyle(
                                                                fontSize: 24,
                                                                color: Colors
                                                                    .white,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    : Container(
                                        margin: EdgeInsets.only(top: 90),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomRight:
                                                  Radius.circular(10.0),
                                              bottomLeft:
                                                  Radius.circular(10.0)),
                                        ),
                                        child: Stack(
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                if (_controller
                                                    .value.isPlaying) {
                                                  _controller.pause();
                                                } else {
                                                  _controller.play();
                                                }
                                              },
                                              child: FutureBuilder(
                                                future:
                                                    _initializeVideoPlayerFuture,
                                                builder: (context, snapshot) {
                                                  if (snapshot
                                                          .connectionState ==
                                                      ConnectionState.done) {
                                                    return AspectRatio(
                                                      aspectRatio: _controller
                                                          .value.aspectRatio,
                                                      child: VideoPlayer(
                                                          _controller),
                                                    );
                                                  } else {
                                                    return Center(
                                                      child:
                                                          CircularProgressIndicator(),
                                                    );
                                                  }
                                                },
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    40,
                                                margin: EdgeInsets.only(
                                                  left: 20,
                                                  right: 20,
                                                  bottom: 15,
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Container(),
                                                    loadingSteps
                                                        ? CircularProgressIndicator(
                                                            backgroundColor:
                                                                Hexcolor(
                                                                    '#7D5AC2'),
                                                            valueColor:
                                                                new AlwaysStoppedAnimation<
                                                                    Color>(
                                                              Hexcolor(
                                                                  '#FE6802'),
                                                            ),
                                                          )
                                                        : Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                submissionDoneSteps
                                                                    .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 36,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                              Text(
                                                                ' ' +
                                                                    '/ ' +
                                                                    submissionSteps
                                                                        .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 24,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      
                                Container(
                                  width: MediaQuery.of(context).size.width - 40,
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  child: SingleChildScrollView(
                                    physics: BouncingScrollPhysics(),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              bottom: 20, top: 20),
                                          child: Text(
                                            'Опис',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontFamily: 'Arial',
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                        ),
                                        Html(
                                          data:
                                              stepDesc == null ? '' : stepDesc,
                                          onLinkTap: (i) {
                                            _launchUrl(i);
                                          },
                                        )
                                        // Text(
                                        //   stepDesc,
                                        //   style: TextStyle(
                                        //     color: Hexcolor('#545454'),
                                        //     fontSize: 16,
                                        //     fontFamily: 'Arial',
                                        //     letterSpacing: 1.025,
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Hexcolor('#7D5AC2'),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                          ),
                          alignment: Alignment.bottomLeft,
                          margin: EdgeInsets.only(
                            top: 40,
                            bottom: 12,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: SizedBox(
                                      child: IconButton(
                                        icon: Image.asset(
                                          'assets/fg_images/6_home_search_back.png',
                                          width: 10,
                                          height: 19,
                                        ),
                                        onPressed: () {
                                          if (_controller.value.isPlaying) {
                                            _controller.pause();
                                          }
                                          _stopFile();
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ),
                                  ),
                                  Text(
                                    stepName,
                                    style: TextStyle(
                                        fontSize: 25,
                                        color: Colors.white,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                child: SizedBox(
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/run_map_quest_pic_gostart.png',
                                      width: 22,
                                      height: 24,
                                    ),
                                    onPressed: () {
                                      if (_controller.value.isPlaying) {
                                        _controller.pause();
                                      }
                                      _stopFile();
                                      Navigator.pushNamed(
                                        context,
                                        '/6_mission_desc.dart',
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        "Далі",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        if (_controller.value.isPlaying) {
                          _controller.pause();
                        }
                        _stopFile();
                        Navigator.pushNamed(
                            context, '/6_mission_step_scan.dart');
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
