import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/geolocator_service.dart';
import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Missions12Trees extends StatefulWidget {
  @override
  _Missions12TreesState createState() => _Missions12TreesState();
}

class _Missions12TreesState extends State<Missions12Trees> {
  // final GeolocatorService geoService = GeolocatorService();
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  int distanceInMeters1;
  int distanceInMeters2;
  int distanceInMeters3;
  int distanceInMeters4;
  int distanceInMeters5;
  int distanceInMeters6;
  int distanceInMeters7;
  int distanceInMeters8;
  int distanceInMeters9;
  int distanceInMeters10;
  int distanceInMeters11;
  int distanceInMeters12;

  var currentLocation;
  bool mapToggle = false;
  double zoomVal = 17.0;

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _getMyLocation(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  BitmapDescriptor _checkPoint1;
  BitmapDescriptor _checkPoint2;
  BitmapDescriptor _checkPoint3;
  BitmapDescriptor _checkPoint4;
  BitmapDescriptor _checkPoint5;
  BitmapDescriptor _checkPoint6;
  BitmapDescriptor _checkPoint7;
  BitmapDescriptor _checkPoint8;
  BitmapDescriptor _checkPoint9;
  BitmapDescriptor _checkPoint10;
  BitmapDescriptor _checkPoint11;
  BitmapDescriptor _checkPoint12;
  Set<Marker> _markers = HashSet<Marker>();

  // double _destLatitude = 50.450631, _destLongitude = 30.510639; // test
  double _destLatitude = 50.505440, _destLongitude = 30.599712; // elki
  double _destLatitude2 = 50.458019, _destLongitude2 = 30.608130;
  double _destLatitude3 = 50.409107, _destLongitude3 = 30.657204;
  double _destLatitude4 = 50.511578, _destLongitude4 = 30.501955;
  double _destLatitude5 = 50.412766, _destLongitude5 = 30.523388;
  double _destLatitude6 = 50.434011, _destLongitude6 = 30.427453;
  double _destLatitude7 = 50.464062, _destLongitude7 = 30.519020;
  double _destLatitude8 = 50.444367, _destLongitude8 = 30.528763;
  double _destLatitude9 = 50.442639, _destLongitude9 = 30.513226;
  double _destLatitude10 = 50.456713, _destLongitude10 = 30.380639;
  double _destLatitude11 = 50.454765, _destLongitude11 = 30.529825;
  double _destLatitude12 = 50.453530, _destLongitude12 = 30.516144;

  Map<PolylineId, Polyline> polylines = {};
  Geolocator geolocator = Geolocator();

  bool isLoading = false;

  bool isStep1Completed;
  bool isStep2Completed;
  bool isStep3Completed;
  bool isStep4Completed;
  bool isStep5Completed;
  bool isStep6Completed;
  bool isStep7Completed;
  bool isStep8Completed;
  bool isStep9Completed;
  bool isStep10Completed;
  bool isStep11Completed;
  bool isStep12Completed;

  checkTrees() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    isStep1Completed = sharedPreferences.get("isStep1Completed");
    isStep2Completed = sharedPreferences.get("isStep2Completed");
    isStep3Completed = sharedPreferences.get("isStep3Completed");
    isStep4Completed = sharedPreferences.get("isStep4Completed");
    isStep5Completed = sharedPreferences.get("isStep5Completed");
    isStep6Completed = sharedPreferences.get("isStep6Completed");
    isStep7Completed = sharedPreferences.get("isStep7Completed");
    isStep8Completed = sharedPreferences.get("isStep8Completed");
    isStep9Completed = sharedPreferences.get("isStep9Completed");
    isStep10Completed = sharedPreferences.get("isStep10Completed");
    isStep11Completed = sharedPreferences.get("isStep11Completed");
    isStep12Completed = sharedPreferences.get("isStep12Completed");
    if (isStep1Completed == false || isStep1Completed == null) {
      setState(() {
        isStep1Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep1Completed = true;
        checkPoins10[0].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep2Completed == false || isStep2Completed == null) {
      setState(() {
        isStep2Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep2Completed = true;
        checkPoins10[1].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep3Completed == false || isStep3Completed == null) {
      setState(() {
        isStep3Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep3Completed = true;
        checkPoins10[2].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep4Completed == false || isStep4Completed == null) {
      setState(() {
        isStep4Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep4Completed = true;
        checkPoins10[3].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep5Completed == false || isStep5Completed == null) {
      setState(() {
        isStep5Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep5Completed = true;
        checkPoins10[4].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep6Completed == false || isStep6Completed == null) {
      setState(() {
        isStep6Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep6Completed = true;
        checkPoins10[5].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep7Completed == false || isStep7Completed == null) {
      setState(() {
        isStep7Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep7Completed = true;
        checkPoins10[6].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep8Completed == false || isStep8Completed == null) {
      setState(() {
        isStep8Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep8Completed = true;
        checkPoins10[7].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep9Completed == false || isStep9Completed == null) {
      setState(() {
        isStep9Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep9Completed = true;
        checkPoins10[8].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep10Completed == false || isStep10Completed == null) {
      setState(() {
        isStep10Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep10Completed = true;
        checkPoins10[9].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep11Completed == false || isStep11Completed == null) {
      setState(() {
        isStep11Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep11Completed = true;
        checkPoins10[10].isCompleted = true;
        isLoading = false;
      });
    }
    if (isStep12Completed == false || isStep12Completed == null) {
      setState(() {
        isStep12Completed = false;
        isLoading = false;
      });
    } else {
      setState(() {
        isStep12Completed = true;
        checkPoins10[11].isCompleted = true;
        isLoading = false;
      });
    }
  }

  var userEmail;
  // checkAuth() async {
  //   setState(() {
  //     isLoading = true;
  //   });
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   String email = sharedPreferences.get("email");
  //   if (email == null || email == '') {
  //     setState(() {
  //       isLoading = false;
  //       isStep1Completed = false;
  //       isStep2Completed = false;
  //       isStep3Completed = false;
  //       isStep4Completed = false;
  //       isStep5Completed = false;
  //       isStep6Completed = false;
  //       isStep7Completed = false;
  //       isStep8Completed = false;
  //       isStep9Completed = false;
  //       isStep10Completed = false;
  //       isStep11Completed = false;
  //       isStep12Completed = false;
  //     });
  //   } else {
  //     print(email);
  //     setState(() {
  //       isLoading = false;
  //     });
  //   }
  // }
  showWindowQuizDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдене',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви не можете повторно пройти це завдання',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  checkDone() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var res1 = sharedPreferences.getBool('isStep1Completed');
    var res2 = sharedPreferences.getBool('isStep2Completed');
    var res3 = sharedPreferences.getBool('isStep3Completed');
    var res4 = sharedPreferences.getBool('isStep4Completed');
    var res5 = sharedPreferences.getBool('isStep5Completed');
    var res6 = sharedPreferences.getBool('isStep6Completed');
    var res7 = sharedPreferences.getBool('isStep7Completed');
    var res8 = sharedPreferences.getBool('isStep8Completed');
    var res9 = sharedPreferences.getBool('isStep9Completed');
    var res10 = sharedPreferences.getBool('isStep10Completed');
    var res11 = sharedPreferences.getBool('isStep11Completed');
    var res12 = sharedPreferences.getBool('isStep12Completed');
    if (res1 == true &&
        res2 == true &&
        res3 == true &&
        res4 == true &&
        res5 == true &&
        res6 == true &&
        res7 == true &&
        res8 == true &&
        res9 == true &&
        res10 == true &&
        res11 == true &&
        res12 == true) {
      showWindowQuizDone();
    }
  }

  @override
  void initState() {
    // checkAuth();

    checkTrees();
    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        mapToggle = true;
      });
    });

    // Geolocator
    //     .getPositionStream(LocationOptions(
    //         accuracy: LocationAccuracy.medium, timeInterval: 5000))
    //     .listen((position) {
    //   // Do something here
    //   if (currentLocation.latitude == // 50.450631, 30.510639
    //           _destLatitude &&
    //       currentLocation.longitude == _destLongitude) {
    //     // print('you are at start');
    //     showInfoWindowCheckTrue();
    //   } else {
    //     // print('you are not at start');
    //     // showInfoWindowCheckFalse();
    //   }
    // });

    super.initState();
    _setCheckPoint1();
    _setCheckPoint2();
    _setCheckPoint3();
    _setCheckPoint4();
    _setCheckPoint5();
    _setCheckPoint6();
    _setCheckPoint7();
    _setCheckPoint8();
    _setCheckPoint9();
    _setCheckPoint10();
    _setCheckPoint11();
    _setCheckPoint12();
    checkDone();
    // _setMe();
  }

  void _setCheckPoint1() async {
    isStep1Completed == false
        ? _checkPoint1 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint1 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint2() async {
    isStep2Completed == false
        ? _checkPoint2 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint2 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint3() async {
    isStep3Completed == false
        ? _checkPoint3 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint3 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint4() async {
    isStep4Completed == false
        ? _checkPoint4 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint4 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint5() async {
    isStep5Completed == false
        ? _checkPoint5 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint5 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint6() async {
    isStep6Completed == false
        ? _checkPoint6 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint6 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint7() async {
    isStep7Completed == false
        ? _checkPoint7 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint7 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint8() async {
    isStep8Completed == false
        ? _checkPoint8 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint8 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint9() async {
    isStep9Completed == false
        ? _checkPoint9 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint9 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint10() async {
    isStep10Completed == false
        ? _checkPoint10 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint10 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint11() async {
    isStep11Completed == false
        ? _checkPoint11 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint11 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  void _setCheckPoint12() async {
    isStep12Completed == false
        ? _checkPoint12 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/fg_images/z12_missions_12_trees.png')
        : _checkPoint12 = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(),
            'assets/fg_images/z12_missions_12_treesDone.png');
  }

  showInfoWindowCheckTrue() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я на місці',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showInfoWindowCheckFalse() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Ти не на місці',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Спробуй підійти до ялинки ближче\nі ще раз зачекінитись',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "withdraw 5 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('-5TUS done');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  showInfoWindowCheckinFalse(int i) {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 400,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Ти не на місці',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            'Спробуй підійти ближче до ялинки\nта ще раз зачекінитись\nабо',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                              height: 1.4,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'пропусти чек-ін за  ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 16,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                  height: 1.4,
                                ),
                              ),
                              Text(
                                '5 TUS',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Hexcolor('#FE6802'),
                                  fontSize: 16,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                  height: 1.4,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              right: 20,
                              bottom: 20,
                            ),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 80,
                            child: RaisedButton(
                              child: Text(
                                "Спробую ще",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 1.05),
                              ),
                              color: Hexcolor('#59B32D'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              right: 20,
                              bottom: 20,
                            ),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 80,
                            child: RaisedButton(
                              child: Text(
                                "Пропуск за 5 TUS",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 1.05),
                              ),
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(14.0)),
                              onPressed: () async {
                                changeTus(5.0, "WITHDRAW");

                                final SharedPreferences sharedPreferences =
                                    await SharedPreferences.getInstance();
                                int tus = sharedPreferences.get("tus");
                                sharedPreferences.setInt('tus', tus - 5);
                                if (i == 1) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz1.dart');
                                }
                                if (i == 2) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz2.dart');
                                }
                                if (i == 3) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz3.dart');
                                }
                                if (i == 4) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz4.dart');
                                }
                                if (i == 5) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz5.dart');
                                }
                                if (i == 6) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz6.dart');
                                }
                                if (i == 7) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz7.dart');
                                }
                                if (i == 8) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz8.dart');
                                }
                                if (i == 9) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz9.dart');
                                }
                                if (i == 10) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz10.dart');
                                }
                                if (i == 11) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz11.dart');
                                }
                                if (i == 12) {
                                  Navigator.pushNamed(
                                      context, '/missions_12_quiz12.dart');
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showWindowPointDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Точка вже пройдена',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви вже проходили цю точку раніше',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  checkComplited() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    isStep1Completed = sharedPreferences.get("isStep1Completed");
    isStep2Completed = sharedPreferences.get("isStep2Completed");
    isStep3Completed = sharedPreferences.get("isStep3Completed");
    isStep4Completed = sharedPreferences.get("isStep4Completed");
    isStep5Completed = sharedPreferences.get("isStep5Completed");
    isStep6Completed = sharedPreferences.get("isStep6Completed");
    isStep7Completed = sharedPreferences.get("isStep7Completed");
    isStep8Completed = sharedPreferences.get("isStep8Completed");
    isStep9Completed = sharedPreferences.get("isStep9Completed");
    isStep10Completed = sharedPreferences.get("isStep10Completed");
    isStep11Completed = sharedPreferences.get("isStep11Completed");
    isStep12Completed = sharedPreferences.get("isStep12Completed");
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              child: Stack(overflow: Overflow.visible, children: [
                Container(
                  margin: EdgeInsets.only(top: 109),
                  child: mapToggle
                      ? GoogleMap(
                          initialCameraPosition: CameraPosition(
                              target: LatLng(currentLocation.latitude,
                                  currentLocation.longitude),
                              zoom: 17.0),
                          mapType: MapType.normal,
                          tiltGesturesEnabled: true,
                          compassEnabled: true,
                          scrollGesturesEnabled: true,
                          zoomGesturesEnabled: true,
                          zoomControlsEnabled: false, // off default buttons
                          myLocationEnabled: true,
                          myLocationButtonEnabled:
                              false, // off default my location
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);

                            // _getLocation() {
                            //   print("locationLatitude: ${currentLocation.latitude}");
                            //   print(
                            //       "locationLongitude: ${currentLocation.longitude}");
                            // }

                            setState(() {
                              _markers.add(Marker(
                                markerId: MarkerId("1"),
                                position: LatLng(_destLatitude, _destLongitude),
                                icon: _checkPoint1,
                                onTap: () {
                                  // print(isStep1Completed);
                                  // print(isStep2Completed);
                                  // print(isStep3Completed);
                                  // print(isStep4Completed);
                                  // print(isStep5Completed);
                                  // print(isStep6Completed);
                                  // print(isStep7Completed);
                                  // print(isStep8Completed);
                                  // print(isStep9Completed);
                                  // print(isStep10Completed);
                                  // print(isStep11Completed);
                                  // print(isStep12Completed);
                                  showInfoWindowCheckinFalse(1);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("2"),
                                position:
                                    LatLng(_destLatitude2, _destLongitude2),
                                icon: _checkPoint2,
                                onTap: () {
                                  showInfoWindowCheckinFalse(2);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("3"),
                                position:
                                    LatLng(_destLatitude3, _destLongitude3),
                                icon: _checkPoint3,
                                onTap: () {
                                  showInfoWindowCheckinFalse(3);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("4"),
                                position:
                                    LatLng(_destLatitude4, _destLongitude4),
                                icon: _checkPoint4,
                                onTap: () {
                                  showInfoWindowCheckinFalse(4);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("5"),
                                position:
                                    LatLng(_destLatitude5, _destLongitude5),
                                icon: _checkPoint5,
                                onTap: () {
                                  showInfoWindowCheckinFalse(5);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("6"),
                                position:
                                    LatLng(_destLatitude6, _destLongitude6),
                                icon: _checkPoint6,
                                onTap: () {
                                  showInfoWindowCheckinFalse(6);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("7"),
                                position:
                                    LatLng(_destLatitude7, _destLongitude7),
                                icon: _checkPoint7,
                                onTap: () {
                                  showInfoWindowCheckinFalse(7);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("8"),
                                position:
                                    LatLng(_destLatitude8, _destLongitude8),
                                icon: _checkPoint8,
                                onTap: () {
                                  showInfoWindowCheckinFalse(8);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("9"),
                                position:
                                    LatLng(_destLatitude9, _destLongitude9),
                                icon: _checkPoint9,
                                onTap: () {
                                  showInfoWindowCheckinFalse(9);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("10"),
                                position:
                                    LatLng(_destLatitude10, _destLongitude10),
                                icon: _checkPoint10,
                                onTap: () {
                                  showInfoWindowCheckinFalse(10);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("11"),
                                position:
                                    LatLng(_destLatitude11, _destLongitude11),
                                icon: _checkPoint11,
                                onTap: () {
                                  showInfoWindowCheckinFalse(11);
                                },
                              ));

                              _markers.add(Marker(
                                markerId: MarkerId("12"),
                                position:
                                    LatLng(_destLatitude12, _destLongitude12),
                                icon: _checkPoint12,
                                onTap: () {
                                  showInfoWindowCheckinFalse(12);
                                },
                              ));
                            });
                          },
                          polylines: Set<Polyline>.of(polylines.values),
                          markers: _markers,
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Hexcolor('#7D5AC2'),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              Hexcolor('#FE6802'),
                            ),
                          ),
                        ),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                child: SizedBox(
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/6_home_search_back.png',
                                      width: 10,
                                      height: 19,
                                    ),
                                    onPressed: () {
                                      // Navigator.pushNamed(
                                      //     context, '/desc_pinsOnMap.dart');
                                      Navigator.of(context).pop();
                                      // Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ),
                              Text(
                                'Ялинковий квест',
                                style: TextStyle(
                                    fontSize: 28,
                                    color: Colors.white,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            child: SizedBox(
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/run_map_quest_pic_gostart.png',
                                  width: 22,
                                  height: 24,
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, '/missions_12_desc.dart');
                                  // Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 140,
                  right: 20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(
                                context, '/missions_12_12_trees.dart');
                          },
                          child: Image.asset(
                            'assets/fg_images/run_map_quest_pic_update.png',
                            width: 50,
                            height: 50,
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                          onTap: () {
                            zoomVal++;
                            _plus(zoomVal);
                          },
                          child: Image.asset(
                            'assets/fg_images/7_map_icon_zoom_plus.png',
                            width: 50,
                            height: 50,
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          zoomVal--;
                          _minus(zoomVal);
                        },
                        child: Image.asset(
                          'assets/fg_images/7_map_icon_zoom_minus.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          _getMyLocation(zoomVal);
                        },
                        child: Image.asset(
                          'assets/fg_images/7_map_icon_location.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 100,
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 117,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: checkPoins10.length,
                      itemBuilder: (context, index) {
                        return Stack(
                          overflow: Overflow.visible,
                          children: [
                            GestureDetector(
                              onTap: () {
                                _goToCheckPoint(checkPoins10[index].lat,
                                    checkPoins10[index].long);
                              },
                              child: Container(
                                margin: EdgeInsets.only(left: 5, right: 5),
                                height: 117,
                                width: 174,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          checkPoins10[index].image),
                                      fit: BoxFit.cover,
                                    )),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: checkPoins10[index]
                                            .color
                                            .withOpacity(0.5),
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(10.0),
                                            bottomLeft: Radius.circular(10.0)),
                                      ),
                                      height: 33,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: 5,
                                          bottom: 5,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Text(
                                                checkPoins10[index].name,
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1.09,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            checkPoins10[index].isCompleted == true
                                ? Positioned(
                                    top: 12.5,
                                    left: 15,
                                    child: Image.asset(
                                      'assets/fg_images/6_home_logo_mission_completed2.png',
                                      height: 30,
                                      width: 100,
                                    ))
                                : Container()
                          ],
                        );
                      },
                    ),
                  ),
                ),
                Positioned(
                  left: 20,
                  bottom: 0,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        "Перевірити ялинку",
                        style: TextStyle(
                          color: Hexcolor('#FFFFFF'),
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
// SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
// sharedPreferences.clear();
                        setState(() {
                          isLoading = true;
                        });
                        Geolocator.getCurrentPosition(
                                desiredAccuracy: LocationAccuracy.high)
                            .then((currloc) {
                          setState(() {
                            currentLocation = currloc;
                          });
                        });

                        print(
                            "locationLatitude: ${currentLocation.latitude}"); // its works
                        print(
                            "locationLongitude: ${currentLocation.longitude}");
                        distanceInMeters1 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude,
                                _destLongitude))
                            .toInt();

                        distanceInMeters2 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude2,
                                _destLongitude2))
                            .toInt();

                        distanceInMeters3 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude3,
                                _destLongitude3))
                            .toInt();

                        distanceInMeters4 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude4,
                                _destLongitude4))
                            .toInt();

                        distanceInMeters5 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude5,
                                _destLongitude5))
                            .toInt();

                        distanceInMeters6 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude6,
                                _destLongitude6))
                            .toInt();

                        distanceInMeters7 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude7,
                                _destLongitude7))
                            .toInt();

                        distanceInMeters8 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude8,
                                _destLongitude8))
                            .toInt();

                        distanceInMeters9 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude9,
                                _destLongitude9))
                            .toInt();

                        distanceInMeters10 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude10,
                                _destLongitude10))
                            .toInt();

                        distanceInMeters11 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude11,
                                _destLongitude11))
                            .toInt();

                        distanceInMeters12 = (Geolocator.distanceBetween(
                                currentLocation.latitude,
                                currentLocation.longitude,
                                _destLatitude12,
                                _destLongitude12))
                            .toInt();
                        print(distanceInMeters1);
                        print(distanceInMeters2);
                        print(distanceInMeters3);
                        print(distanceInMeters4);
                        print(distanceInMeters5);
                        print(distanceInMeters6);
                        print(distanceInMeters7);
                        print(distanceInMeters8);
                        print(distanceInMeters9);
                        print(distanceInMeters10);
                        print(distanceInMeters11);
                        print(distanceInMeters12);
                        checkComplited();
                        if (distanceInMeters1 < 50) {
                          if (isStep1Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz1.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters2 < 50) {
                          if (isStep2Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz2.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters3 < 50) {
                          if (isStep3Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz3.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters4 < 50) {
                          if (isStep4Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz4.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters5 < 50) {
                          if (isStep5Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz5.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters6 < 50) {
                          if (isStep6Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz6.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters7 < 50) {
                          if (isStep7Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz7.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters8 < 50) {
                          if (isStep8Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz8.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters9 < 50) {
                          if (isStep9Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz9.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters10 < 50) {
                          if (isStep10Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz10.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters11 < 50) {
                          if (isStep11Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz11.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters12 < 50) {
                          if (isStep12Completed == false) {
                            Navigator.pushNamed(
                                context, '/missions_12_quiz12.dart');
                          } else {
                            showWindowPointDone();
                          }
                        }
                        if (distanceInMeters1 > 50 &&
                            distanceInMeters2 > 50 &&
                            distanceInMeters3 > 50 &&
                            distanceInMeters4 > 50 &&
                            distanceInMeters5 > 50 &&
                            distanceInMeters6 > 50 &&
                            distanceInMeters7 > 50 &&
                            distanceInMeters8 > 50 &&
                            distanceInMeters9 > 50 &&
                            distanceInMeters10 > 50 &&
                            distanceInMeters11 > 50 &&
                            distanceInMeters12 > 50) {
                          showInfoWindowCheckFalse();
                        }

                        // if (currentLocation.latitude ==
                        //         _destLatitude && // 50.4492936, 30.5125368;
                        //     currentLocation.longitude == _destLongitude) {
                        //   print('you are at start');
                        //   showInfoWindowCheckTrue();
                        // } else {
                        //   print('you are not at start');
                        //   showInfoWindowCheckFalse();
                        // }
                      },
                    ),
                  ),
                )
              ]),
            ),
    );
  }

  Future<void> _goToCheckPoint(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 17)));
  }

  Future<void> centerScreen(Position currentLocation) async {
    setState(() {});
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 17.0),
    ));
  }
}
