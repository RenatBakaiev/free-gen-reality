import 'dart:ui';

class Reward {
  final String name;
  final String image;
  double quantity;
  final Color backColor;

  Reward ({
    this.name,
    this.image,
    this.quantity,
    this.backColor,
  });
}

class ProfileItem {
  final String name;
  final Color color;
  final String icon;
  final String link;

  ProfileItem ({
    this.name,
    this.color,
    this.icon,
    this.link,
  });
}