import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';

import '10_profile_contacts_static_json.dart';

class ProfileContacts extends StatefulWidget {
  @override
  _ProfileContactsState createState() => _ProfileContactsState();
}

void _launchUrl(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not open Url';
  }
}

// void _launchCaller(int number) async {
//   var url = 'tel:${number.toString()}';
//   if (await canLaunch(url)) {
//     await launch(url);
//   } else {
//     throw 'Could not place Call';
//   }
// }

// void _launchEmail(String emailId) async {
//   var url = 'mailto:$emailId?subject=Hello';
//   if (await canLaunch(url)) {
//     await launch(url);
//   } else {
//     throw 'Could not send email';
//   }
// }

class _ProfileContactsState extends State<ProfileContacts> {
  List<Publication> posts;
  bool loading;

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesStaticContacts.getContacts().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Text(
                            'Контакти',
                            style: TextStyle(
                                fontSize: 33,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // Image.asset(
                  //   'assets/fg_images/10_profile_contacts_pic.png',
                  //   height: 324,
                  // ),
                  Expanded(
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: posts.length,
                        itemBuilder: (context, index) {
                          Publication publication = posts[index];

                          var title =
                              utf8.decode(publication.titleUa.runes.toList());
                          var content =
                              utf8.decode(publication.contentUa.runes.toList());

                          return Column(children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 15,
                                left: 20,
                                right: 20,
                              ),
                              child: Row(
                                children: [
                                  Text(
                                    // '',
                                    title == null ? '' : title,
                                    style: TextStyle(
                                        color: Hexcolor('#484848'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 20,
                                right: 20,
                              ),
                              child: Html(
                                data: content == null ? '' : content,
                                onLinkTap: (i) {
                                  _launchUrl(i);
                                },
                              ),
                            ),
                          ]);
                        }),
                  ),

                  // Container(
                  //   child: Expanded(
                  //       child: SingleChildScrollView(
                  //           physics: BouncingScrollPhysics(),
                  //           child: Container(
                  //               // margin: EdgeInsets.only(left: 5),
                  //               child: Column(
                  //             mainAxisAlignment: MainAxisAlignment.end,
                  //             children: [
                  //               Image.asset(
                  //                 'assets/fg_images/10_profile_contacts_pic.png',
                  //                 height: 324,
                  //               ),
                  //               Container(
                  //                 margin: EdgeInsets.only(
                  //                   left: 20,
                  //                 ),
                  //                 child: Column(
                  //                   children: [
                  //                     Row(
                  //                       mainAxisAlignment: MainAxisAlignment.start,
                  //                       crossAxisAlignment: CrossAxisAlignment.center,
                  //                       children: [
                  //                         Container(
                  //                           // width: 120,
                  //                           child: Row(
                  //                             mainAxisAlignment:
                  //                                 MainAxisAlignment.start,
                  //                             crossAxisAlignment:
                  //                                 CrossAxisAlignment.center,
                  //                             children: [
                  //                               SizedBox(
                  //                                 width: 1.85,
                  //                               ),
                  //                               Image.asset(
                  //                                 'assets/fg_images/10_profile_contacts_pic_phone.png',
                  //                                 height: 17.80,
                  //                                 width: 17.80,
                  //                               ),
                  //                               SizedBox(
                  //                                 width: 10.0,
                  //                               ),
                  //                               Text('',
                  //                                 // 'Тел:',
                  //                                 style: TextStyle(
                  //                                   color: Hexcolor('#747474'),
                  //                                   fontSize: 16,
                  //                                   fontFamily: 'Arial',
                  //                                   fontWeight: FontWeight.w400,
                  //                                 ),
                  //                               )
                  //                             ],
                  //                           ),
                  //                         ),
                  //                         GestureDetector(
                  //                           behavior: HitTestBehavior.opaque,
                  //                           onTap: () {
                  //                             _launchCaller(380969112112);
                  //                           },
                  //                           child: Text(
                  //                             '096 911 21 12',
                  //                             style: TextStyle(
                  //                               color: Hexcolor('#1E2E45'),
                  //                               fontSize: 16,
                  //                               fontFamily: 'Arial',
                  //                               fontWeight: FontWeight.w600,
                  //                             ),
                  //                           ),
                  //                         )
                  //                       ],
                  //                     ),
                  //                     SizedBox(
                  //                       height: 15,
                  //                     ),
                  //                     Row(
                  //                       mainAxisAlignment: MainAxisAlignment.start,
                  //                       crossAxisAlignment: CrossAxisAlignment.center,
                  //                       children: [
                  //                         Container(
                  //                           // width: 120,
                  //                           child: Row(
                  //                             mainAxisAlignment:
                  //                                 MainAxisAlignment.start,
                  //                             crossAxisAlignment:
                  //                                 CrossAxisAlignment.center,
                  //                             children: [
                  //                               Image.asset(
                  //                                 'assets/fg_images/10_profile_contacts_pic_email.png',
                  //                                 height: 15.0,
                  //                                 width: 21.5,
                  //                               ),
                  //                               SizedBox(
                  //                                 width: 10,
                  //                               ),
                  //                               Text('',
                  //                                 // 'Email:',
                  //                                 style: TextStyle(
                  //                                   color: Hexcolor('#747474'),
                  //                                   fontSize: 16,
                  //                                   fontFamily: 'Arial',
                  //                                   fontWeight: FontWeight.w400,
                  //                                 ),
                  //                               )
                  //                             ],
                  //                           ),
                  //                         ),
                  //                         GestureDetector(
                  //                           behavior: HitTestBehavior.opaque,
                  //                           onTap: () {
                  //                             _launchEmail('freegenorg@gmail.com');
                  //                           },
                  //                           child: Text(
                  //                             'freegenorg@gmail.com',
                  //                             style: TextStyle(
                  //                               color: Hexcolor('#1E2E45'),
                  //                               fontSize: 16,
                  //                               fontFamily: 'Arial',
                  //                               fontWeight: FontWeight.w600,
                  //                               // decoration: TextDecoration.underline,
                  //                             ),
                  //                           ),
                  //                         )
                  //                       ],
                  //                     ),
                  //                     SizedBox(
                  //                       height: 15,
                  //                     ),
                  //                     Row(
                  //                       mainAxisAlignment: MainAxisAlignment.start,
                  //                       crossAxisAlignment: CrossAxisAlignment.center,
                  //                       children: [
                  //                         Container(
                  //                           // width: 120,
                  //                           child: Row(
                  //                             mainAxisAlignment:
                  //                                 MainAxisAlignment.start,
                  //                             crossAxisAlignment:
                  //                                 CrossAxisAlignment.center,
                  //                             children: [
                  //                               SizedBox(
                  //                                 width: 5.75,
                  //                               ),
                  //                               Image.asset(
                  //                                 'assets/fg_images/10_profile_contacts_pic_facebook.png',
                  //                                 height: 18.0,
                  //                                 width: 9.5,
                  //                               ),
                  //                               SizedBox(
                  //                                 width: 15.75,
                  //                               ),
                  //                               Text('',
                  //                                 // 'Facebook:',
                  //                                 style: TextStyle(
                  //                                   color: Hexcolor('#747474'),
                  //                                   fontSize: 16,
                  //                                   fontFamily: 'Arial',
                  //                                   fontWeight: FontWeight.w400,
                  //                                 ),
                  //                               )
                  //                             ],
                  //                           ),
                  //                         ),
                  //                         GestureDetector(
                  //                           behavior: HitTestBehavior.opaque,
                  //                           onTap: () {
                  //                             _launchUrl(
                  //                                 'https://www.facebook.com/FreeGen.Org');
                  //                           },
                  //                           child: Text(
                  //                             'www.facebook.com/FreeGen.org',
                  //                             style: TextStyle(
                  //                               color: Hexcolor('#1E2E45'),
                  //                               fontSize: 16,
                  //                               fontFamily: 'Arial',
                  //                               fontWeight: FontWeight.w600,
                  //                               decoration: TextDecoration.underline,
                  //                             ),
                  //                           ),
                  //                         )
                  //                       ],
                  //                     ),
                  //                     SizedBox(
                  //                       height: 15,
                  //                     ),
                  //                     Row(
                  //                       mainAxisAlignment: MainAxisAlignment.start,
                  //                       crossAxisAlignment: CrossAxisAlignment.center,
                  //                       children: [
                  //                         Container(
                  //                           // width: 120,
                  //                           child: Row(
                  //                             mainAxisAlignment:
                  //                                 MainAxisAlignment.start,
                  //                             crossAxisAlignment:
                  //                                 CrossAxisAlignment.center,
                  //                             children: [
                  //                               Image.asset(
                  //                                 'assets/fg_images/10_profile_contacts_pic_telegram.png',
                  //                                 height: 16.8,
                  //                                 width: 20,
                  //                               ),
                  //                               SizedBox(
                  //                                 width: 12.35,
                  //                               ),
                  //                               Text('',
                  //                                 // 'Telegram:',
                  //                                 style: TextStyle(
                  //                                   color: Hexcolor('#747474'),
                  //                                   fontSize: 16,
                  //                                   fontFamily: 'Arial',
                  //                                   fontWeight: FontWeight.w400,
                  //                                 ),
                  //                               )
                  //                             ],
                  //                           ),
                  //                         ),
                  //                         GestureDetector(
                  //                           behavior: HitTestBehavior.opaque,
                  //                           onTap: () {
                  //                             _launchUrl('https://t.me/poKLICH');
                  //                           },
                  //                           child: Text(
                  //                             'https://t.me/poKLICH',
                  //                             style: TextStyle(
                  //                               color: Hexcolor('#1E2E45'),
                  //                               fontSize: 16,
                  //                               fontFamily: 'Arial',
                  //                               fontWeight: FontWeight.w600,
                  //                             ),
                  //                           ),
                  //                         )
                  //                       ],
                  //                     ),
                  //                     SizedBox(
                  //                       height: 15,
                  //                     ),
                  //                     Row(
                  //                       mainAxisAlignment: MainAxisAlignment.start,
                  //                       crossAxisAlignment: CrossAxisAlignment.center,
                  //                       children: [
                  //                         Container(
                  //                           // width: 120,
                  //                           child: Row(
                  //                             mainAxisAlignment:
                  //                                 MainAxisAlignment.start,
                  //                             crossAxisAlignment:
                  //                                 CrossAxisAlignment.center,
                  //                             children: [
                  //                               SizedBox(
                  //                                 width: 1.75,
                  //                               ),
                  //                               Image.asset(
                  //                                 'assets/fg_images/10_profile_contacts_pic_site.png',
                  //                                 height: 18,
                  //                                 width: 18,
                  //                               ),
                  //                               SizedBox(
                  //                                 width: 11.75,
                  //                               ),
                  //                               Text('',
                  //                                 // 'Сайт:',
                  //                                 style: TextStyle(
                  //                                   color: Hexcolor('#747474'),
                  //                                   fontSize: 16,
                  //                                   fontFamily: 'Arial',
                  //                                   fontWeight: FontWeight.w400,
                  //                                 ),
                  //                               )
                  //                             ],
                  //                           ),
                  //                         ),
                  //                         GestureDetector(
                  //                           behavior: HitTestBehavior.opaque,
                  //                           onTap: () {
                  //                             _launchUrl('https://freegen.net/');
                  //                           },
                  //                           child: Text(
                  //                             'www.freegen.net',
                  //                             style: TextStyle(
                  //                               color: Hexcolor('#1E2E45'),
                  //                               fontSize: 16,
                  //                               fontFamily: 'Arial',
                  //                               fontWeight: FontWeight.w600,
                  //                               decoration: TextDecoration.underline,
                  //                             ),
                  //                           ),
                  //                         )
                  //                       ],
                  //                     ),
                  //                   ],
                  //                 ),
                  //               ),

                  //               // GestureDetector(
                  //               //   onTap: () {
                  //               //     _launchEmail('freegenorg@gmail.com'); // error emulator?
                  //               //     // customLaunch('malito:freegenorg@gmail.com?subject=hello');
                  //               //   },
                  //               //   child: Row(
                  //               //     mainAxisAlignment: MainAxisAlignment.start,
                  //               //     children: [
                  //               //       Column(
                  //               //         crossAxisAlignment: CrossAxisAlignment.start,
                  //               //         children: [
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               left: 30,
                  //               //               bottom: 10,
                  //               //               top: 30,
                  //               //             ),
                  //               //             child: Text(
                  //               //               'Email',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#9B9B9B'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               right: 20,
                  //               //               left: 20,
                  //               //             ),
                  //               //             decoration: BoxDecoration(
                  //               //               color: Hexcolor('#F2F2F2'),
                  //               //               borderRadius: BorderRadius.only(
                  //               //                   topLeft: Radius.circular(10),
                  //               //                   topRight: Radius.circular(10),
                  //               //                   bottomLeft: Radius.circular(10),
                  //               //                   bottomRight: Radius.circular(10)),
                  //               //               boxShadow: [
                  //               //                 BoxShadow(
                  //               //                   color: Colors.grey.withOpacity(0.5),
                  //               //                   spreadRadius: 5,
                  //               //                   blurRadius: 7,
                  //               //                   // offset: Offset(0,
                  //               //                   //     3), // changes position of shadow
                  //               //                 ),
                  //               //               ],
                  //               //             ),
                  //               //             padding: EdgeInsets.only(left: 25),
                  //               //             alignment: Alignment.centerLeft,
                  //               //             width: MediaQuery.of(context).size.width -
                  //               //                 40,
                  //               //             height: 60,
                  //               //             // color: Colors.red,
                  //               //             child: Text(
                  //               //               'freegenorg@gmail.com',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#000000'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //         ],
                  //               //       ),
                  //               //     ],
                  //               //   ),
                  //               // ),
                  //               //-------------------------------------------------------------------------------------------------------
                  //               // GestureDetector(
                  //               //   onTap: () {
                  //               //     _launchCaller(380969112112);
                  //               //   },
                  //               //   child: Row(
                  //               //     mainAxisAlignment: MainAxisAlignment.start,
                  //               //     children: [
                  //               //       Column(
                  //               //         crossAxisAlignment: CrossAxisAlignment.start,
                  //               //         children: [
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               left: 30,
                  //               //               bottom: 10,
                  //               //               top: 20,
                  //               //             ),
                  //               //             child: Text(
                  //               //               'Телефон',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#9B9B9B'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               right: 20,
                  //               //               left: 20,
                  //               //             ),
                  //               //             decoration: BoxDecoration(
                  //               //               color: Hexcolor('#F2F2F2'),
                  //               //               borderRadius: BorderRadius.only(
                  //               //                   topLeft: Radius.circular(10),
                  //               //                   topRight: Radius.circular(10),
                  //               //                   bottomLeft: Radius.circular(10),
                  //               //                   bottomRight: Radius.circular(10)),
                  //               //               boxShadow: [
                  //               //                 BoxShadow(
                  //               //                   color: Colors.grey.withOpacity(0.5),
                  //               //                   spreadRadius: 5,
                  //               //                   blurRadius: 7,
                  //               //                   // offset: Offset(0,
                  //               //                   //     3), // changes position of shadow
                  //               //                 ),
                  //               //               ],
                  //               //             ),
                  //               //             padding: EdgeInsets.only(left: 25),
                  //               //             alignment: Alignment.centerLeft,
                  //               //             width: MediaQuery.of(context).size.width -
                  //               //                 40,
                  //               //             height: 60,
                  //               //             // color: Colors.red,
                  //               //             child: Text(
                  //               //               '+38 096 911 21 12',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#000000'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //         ],
                  //               //       ),
                  //               //     ],
                  //               //   ),
                  //               // ),
                  //               //-------------------------------------------------------------------------------------------------------
                  //               // GestureDetector(
                  //               //   onTap: () {
                  //               //     _launchUrl(
                  //               //         'https://www.facebook.com/FreeGen.Org');
                  //               //   },
                  //               //   child: Row(
                  //               //     mainAxisAlignment: MainAxisAlignment.start,
                  //               //     children: [
                  //               //       Column(
                  //               //         crossAxisAlignment: CrossAxisAlignment.start,
                  //               //         children: [
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               left: 30,
                  //               //               bottom: 10,
                  //               //               top: 20,
                  //               //             ),
                  //               //             child: Text(
                  //               //               'Facebook',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#9B9B9B'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               right: 20,
                  //               //               left: 20,
                  //               //             ),
                  //               //             decoration: BoxDecoration(
                  //               //               color: Hexcolor('#F2F2F2'),
                  //               //               borderRadius: BorderRadius.only(
                  //               //                   topLeft: Radius.circular(10),
                  //               //                   topRight: Radius.circular(10),
                  //               //                   bottomLeft: Radius.circular(10),
                  //               //                   bottomRight: Radius.circular(10)),
                  //               //               boxShadow: [
                  //               //                 BoxShadow(
                  //               //                   color: Colors.grey.withOpacity(0.5),
                  //               //                   spreadRadius: 5,
                  //               //                   blurRadius: 7,
                  //               //                   // offset: Offset(0,
                  //               //                   //     3), // changes position of shadow
                  //               //                 ),
                  //               //               ],
                  //               //             ),
                  //               //             padding: EdgeInsets.only(left: 25),
                  //               //             alignment: Alignment.centerLeft,
                  //               //             width: MediaQuery.of(context).size.width -
                  //               //                 40,
                  //               //             height: 60,
                  //               //             // color: Colors.red,
                  //               //             child: Text(
                  //               //               'www.facebook.com/FreeGen.Org',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#000000'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //         ],
                  //               //       ),
                  //               //     ],
                  //               //   ),
                  //               // ),
                  //               //-------------------------------------------------------------------------------------------------------
                  //               // GestureDetector(
                  //               //   onTap: () {
                  //               //     _launchUrl('https://t.me/poKLICH');
                  //               //   },
                  //               //   child: Row(
                  //               //     mainAxisAlignment: MainAxisAlignment.start,
                  //               //     children: [
                  //               //       Column(
                  //               //         crossAxisAlignment: CrossAxisAlignment.start,
                  //               //         children: [
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               left: 30,
                  //               //               bottom: 10,
                  //               //               top: 20,
                  //               //             ),
                  //               //             child: Text(
                  //               //               'Telegram',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#9B9B9B'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               right: 20,
                  //               //               left: 20,
                  //               //             ),
                  //               //             decoration: BoxDecoration(
                  //               //               color: Hexcolor('#F2F2F2'),
                  //               //               borderRadius: BorderRadius.only(
                  //               //                   topLeft: Radius.circular(10),
                  //               //                   topRight: Radius.circular(10),
                  //               //                   bottomLeft: Radius.circular(10),
                  //               //                   bottomRight: Radius.circular(10)),
                  //               //               boxShadow: [
                  //               //                 BoxShadow(
                  //               //                   color: Colors.grey.withOpacity(0.5),
                  //               //                   spreadRadius: 5,
                  //               //                   blurRadius: 7,
                  //               //                   // offset: Offset(0,
                  //               //                   //     3), // changes position of shadow
                  //               //                 ),
                  //               //               ],
                  //               //             ),
                  //               //             padding: EdgeInsets.only(left: 25),
                  //               //             alignment: Alignment.centerLeft,
                  //               //             width: MediaQuery.of(context).size.width -
                  //               //                 40,
                  //               //             height: 60,
                  //               //             // color: Colors.red,
                  //               //             child: Text(
                  //               //               'https://t.me/poKLICH',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#000000'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //         ],
                  //               //       ),
                  //               //     ],
                  //               //   ),
                  //               // ),
                  //               //-------------------------------------------------------------------------------------------------------
                  //               // GestureDetector(
                  //               //   onTap: () {
                  //               //     _launchUrl('https://freegen.net/');
                  //               //   },
                  //               //   child: Row(
                  //               //     mainAxisAlignment: MainAxisAlignment.start,
                  //               //     children: [
                  //               //       Column(
                  //               //         crossAxisAlignment: CrossAxisAlignment.start,
                  //               //         children: [
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //               left: 30,
                  //               //               bottom: 10,
                  //               //               top: 20,
                  //               //             ),
                  //               //             child: Text(
                  //               //               'Сайт',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#9B9B9B'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //           Container(
                  //               //             margin: EdgeInsets.only(
                  //               //                 right: 20, left: 20, bottom: 20),
                  //               //             decoration: BoxDecoration(
                  //               //               color: Hexcolor('#F2F2F2'),
                  //               //               borderRadius: BorderRadius.only(
                  //               //                   topLeft: Radius.circular(10),
                  //               //                   topRight: Radius.circular(10),
                  //               //                   bottomLeft: Radius.circular(10),
                  //               //                   bottomRight: Radius.circular(10)),
                  //               //               boxShadow: [
                  //               //                 BoxShadow(
                  //               //                   color: Colors.grey.withOpacity(0.5),
                  //               //                   spreadRadius: 5,
                  //               //                   blurRadius: 7,
                  //               //                   // offset: Offset(0,
                  //               //                   //     3), // changes position of shadow
                  //               //                 ),
                  //               //               ],
                  //               //             ),
                  //               //             padding: EdgeInsets.only(left: 25),
                  //               //             alignment: Alignment.centerLeft,
                  //               //             width: MediaQuery.of(context).size.width -
                  //               //                 40,
                  //               //             height: 60,
                  //               //             // color: Colors.red,
                  //               //             child: Text(
                  //               //               'www.freegen.net',
                  //               //               style: TextStyle(
                  //               //                 color: Hexcolor('#000000'),
                  //               //                 fontSize: 16,
                  //               //                 fontFamily: 'Arial',
                  //               //                 fontWeight: FontWeight.w600,
                  //               //                 letterSpacing: 1.025,
                  //               //               ),
                  //               //             ),
                  //               //           ),
                  //               //         ],
                  //               //       ),
                  //               //     ],
                  //               //   ),
                  //               // ),
                  //             ],
                  //           )))),
                  // ),
                ],
              ),
            ),
    );
  }
}
