import '6_home_filter_age_model.dart';

final Age age1 = Age(
  age: '10-15',
  isSelected: false,
);

final Age age2 = Age(
  age: '15-20',
  isSelected: false,
);

final Age age3 = Age(
  age: '20-25',
  isSelected: false,
);

final Age age4 = Age(
  age: '25-30',
  isSelected: false,
);

final Age age5 = Age(
  age: '30-35',
  isSelected: false,
);

final Age age6 = Age(
  age: '35-40',
  isSelected: false,
);

final Age age7 = Age(
  age: '45-50',
  isSelected: false,
);

final Age age8 = Age(
  age: '50-55',
  isSelected: false,
);

final Age age9 = Age(
  age: '55-60',
  isSelected: false,
);

final Age age10 = Age(
  age: 'Будь-який',
  isSelected: false,
);

List<Age> ages = [
  age10,
  age1,
  age2,
  age3,
  age4,
  age5,
  age6,
  age7,
  age8,
  age9,
];