import 'dart:ui';

class Quest {
  final int id;
  final String name;
  final String imageUrl;
  final Color nameBackColor;
  final Color imageUrlBackColor;
  final String quest;

  Quest ({
    this.id,
    this.name,
    this.imageUrl, 
    this.nameBackColor,
    this.imageUrlBackColor,
    this.quest,
  });

  Map<String, dynamic> toMap(){
    return {
      "name": this.name,
      "imageUrl": this.imageUrl,
      "quest": this.quest
    };
  }

}