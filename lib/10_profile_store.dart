// import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:hexcolor/hexcolor.dart';
import '10_profile_store_products_json.dart';

// class Debouncer {
//   final int milliseconds;
//   VoidCallback action;
//   Timer _timer;

//   Debouncer({this.milliseconds});

//   run(VoidCallback action) {
//     if (null != _timer) {
//       _timer.cancel();
//     }
//     _timer = Timer(Duration(milliseconds: milliseconds), action);
//   }
// }

class ProfileStore extends StatefulWidget {
  @override
  _ProfileStoreState createState() => _ProfileStoreState();
}

class _ProfileStoreState extends State<ProfileStore> {
  var _blankFocusNode = new FocusNode();
  // final _debouncer = Debouncer(milliseconds: 500);
  bool isStoreOpen = true;

  List<Publication> filteredProducts = List<Publication>();
  List<Publication> posts = List<Publication>();
  bool loading;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  // List<Product> filteredProducts = List();

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesProducts.getProducts().then((post) {
      setState(() {
        posts = post.publication;
        filteredProducts = posts;
        loading = false;
      });
    });
    // setState(() {
    //   filteredProducts = posts;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Stack(
              overflow: Overflow.visible,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    FocusScope.of(context).requestFocus(_blankFocusNode);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#F2F2F2'),
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Hexcolor('#7D5AC2'),
                            ),
                            alignment: Alignment.bottomLeft,
                            margin: EdgeInsets.only(
                              top: 50,
                              // left: 15,
                              // right: 15,
                              // bottom: 33,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(left: 10, right: 10),
                                  child: SizedBox(
                                    // height: 55,
                                    // width: 40,
                                    child: IconButton(
                                      icon: Image.asset(
                                        'assets/fg_images/6_home_search_back.png',
                                        width: 10,
                                        height: 19,
                                      ),
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, '/5_myBottomBar.dart');
                                      },
                                    ),
                                  ),
                                ),
                                Text(
                                  'Магазин',
                                  style: TextStyle(
                                      fontSize: 37,
                                      color: Colors.white,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w900),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                            height: 100,
                            child: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Hexcolor('#7D5AC2'),
                                  // color: Colors.green,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10.0),
                                      bottomRight: Radius.circular(10.0)),
                                ),
                                child: Container(
                                  margin: EdgeInsets.only(
                                      right: 20, left: 20, bottom: 10),
                                  child: Stack(
                                      overflow: Overflow.visible,
                                      alignment: Alignment.centerLeft,
                                      children: <Widget>[
                                        TextField(
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Hexcolor('#7D5AC2'),
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w600,
                                          ),
                                          textAlign: TextAlign.left,
                                          textAlignVertical:
                                              TextAlignVertical.center,
                                          decoration: new InputDecoration(
                                            hintText: 'Пошук',
                                            hintStyle: TextStyle(
                                              color: Hexcolor('#717A87'),
                                              fontSize: 16,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w400,
                                            ),
                                            contentPadding: new EdgeInsets.only(
                                                left: 42, top: 14, bottom: 14),
                                            fillColor: Colors.white,
                                            filled: true,
                                            border: new OutlineInputBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      24.0),
                                            ),
                                          ),
                                          onChanged: (string) {
                                            // https://www.youtube.com/watch?v=_lbyRJoxSdg&t=593s
                                            string = string.toLowerCase();
                                            // _debouncer.run(() {
                                            setState(() {
                                              filteredProducts =
                                                  posts.where((post) {
                                                var postTitle = utf8
                                                    .decode(post.titleUa.runes
                                                        .toList())
                                                    .toLowerCase();
                                                return postTitle
                                                    .contains(string);
                                              }).toList();
                                            });
                                            // });
                                          },
                                        ),
                                        Positioned(
                                            left: 17,
                                            top: 17,
                                            child: Image.asset(
                                              'assets/fg_images/6_home_search_search.png',
                                              width: 15,
                                              height: 15,
                                              color: Hexcolor('#717A87'),
                                            )),
                                      ]),
                                ))),
                        Expanded(
                          child: filteredProducts.length == 0
                              ? Center(
                                  child: Text(
                                    'Результатів не знайдено!',
                                    style: TextStyle(
                                      color: Hexcolor('#747474'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                )
                              : ListView.builder(
                                  physics: BouncingScrollPhysics(),
                                  itemCount: filteredProducts.length,
                                  itemBuilder: (context, index) {
                                    Publication publication =
                                        filteredProducts[index];

                                    DateTime time = DateTime.parse(
                                        publication.publicationDate.toString());
                                    String publicationDate =
                                        // "${time.year.toString()}-${time.month.toString().padLeft(2, '0')}-${time.day.toString().padLeft(2, '0')} ${time.hour.toString()}:${time.minute.toString()}:${time.second.toString()}";
                                        "${time.day.toString().padLeft(2, '0')}-${time.month.toString().padLeft(2, '0')}-${time.year.toString()}";
                                    var title = utf8.decode(
                                        publication.titleUa.runes.toList());
                                    var content = utf8.decode(
                                        publication.contentUa.runes.toList());
                                    var address = utf8.decode(
                                        publication.address.runes.toList());
                                    // var currency = utf8.decode(
                                    //     publication.currency.runes.toList());    

                                    return GestureDetector(
                                      onTap: () {
                                        if (isStoreOpen) {
                                          Navigator.pushNamed(context,
                                              '/10_profile_store_product_page.dart',
                                              arguments: ({
                                                "image": publication.img ==
                                                            "" ||
                                                        publication.img == null
                                                    ? '$link' + 'default.png'
                                                    : '$link' +
                                                        '${publication.img}',
                                                "name": title,
                                                "price":
                                                    publication.cost == null
                                                        ? ''
                                                        : publication.cost,
                                                "address": address,
                                                "time": publicationDate == null
                                                    ? ''
                                                    : publicationDate,
                                                "desc": content,
                                              })); //
                                        }
                                      },
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                bottom: 20,
                                                left: 20,
                                                right: 20),
                                            height: 177,
                                            // decoration: BoxDecoration(color: Colors.indigoAccent),
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 177,
                                                  width: MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          2 -
                                                      20,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10)),
                                                      image: DecorationImage(
                                                        image: NetworkImage(
                                                          publication.img ==
                                                                      "" ||
                                                                  publication
                                                                          .img ==
                                                                      null
                                                              ? '$link' +
                                                                  'default.png'
                                                              : '$link' +
                                                                  '${publication.img}',
                                                        ),
                                                        fit: BoxFit.cover,
                                                      )),
                                                ),
                                                Container(
                                                  padding:
                                                      EdgeInsets.only(left: 18),
                                                  height: 177,
                                                  width: MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          2 -
                                                      20,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Flexible(
                                                              child: Container(
                                                            // margin: EdgeInsets.only(bottom: 20),
                                                            child: Text(
                                                              title == null
                                                                  ? ''
                                                                  : title,
                                                              maxLines: 3,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      17, // БЫЛО 20 - ПОМЕНЯЙ ОБРАТНО !!!!!!
                                                                  color: Hexcolor(
                                                                      '#1E2E45'),
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600),
                                                            ),
                                                          )),
                                                          // Image.asset(
                                                          //   storeProducts[index].favoriteIcon,
                                                          //   height: 23,
                                                          //   width: 18,
                                                          // ),
                                                        ],
                                                      ),
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            'Ціна',
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                color: Hexcolor(
                                                                    '#B9BCC4'),
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                          Text(
                                                            // '',
                                                            publication.cost
                                                                        .toInt()
                                                                        .toString() ==
                                                                    null
                                                                ? ''
                                                                : publication
                                                                        .cost
                                                                        .toInt()
                                                                        .toString() +
                                                                    ' Foint',
                                                            style: TextStyle(
                                                                fontSize: 16,
                                                                color: Hexcolor(
                                                                    '#298127'),
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                          Row(
                                                            children: [
                                                              Image.asset(
                                                                'assets/fg_images/10_profile_store_icon_map.png',
                                                                height: 26,
                                                                width: 17,
                                                              ),
                                                              Flexible(
                                                                child:
                                                                    Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              5),
                                                                  child: Text(
                                                                    address ==
                                                                            null
                                                                        ? ''
                                                                        : address,
                                                                    maxLines: 2,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                        color: Hexcolor(
                                                                            '#1E2E45'),
                                                                        fontFamily:
                                                                            'Arial',
                                                                        fontWeight:
                                                                            FontWeight.w500),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                          Text(
                                                            publicationDate ==
                                                                    null
                                                                ? ''
                                                                : publicationDate,
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                color: Hexcolor(
                                                                    '#B9BCC4'),
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 20),
                                            child: Divider(
                                              color: Hexcolor('#DBDBDB'),
                                              thickness: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 35,
                  right: 0,
                  child: Image.network(
                    isStoreOpen
                        ? 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-30_10_profile_store_access_granted.png'
                        : 'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-30_10_profile_store_access_denied.png',
                    height: 230,
                  ),
                ),
              ],
            ),
    );
  }
}
