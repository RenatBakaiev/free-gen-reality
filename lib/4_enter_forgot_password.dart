import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;
import 'ProgressHUD.dart';

class EnterForgotPassword extends StatefulWidget {
  @override
  _EnterForgotPasswordState createState() => _EnterForgotPasswordState();
}

class _EnterForgotPasswordState extends State<EnterForgotPassword> {
  var _blankFocusNode = new FocusNode();
  final formKey = GlobalKey<FormState>();
  TextEditingController emailTextEditingController =
      new TextEditingController();

  showWindowSendSuccess() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Пароль відправлено',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'На вказаний Вами e-mail був відправлений тимчасовий пароль для авторизації. Перевірте пошту та авторизуйтесь.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#1E2E45'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/4_enter');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showWindowSendError() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Пароль не відправлено!',
                          style: TextStyle(
                            color: Hexcolor('#FF1E1E'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Користувача з таким email не знайдено!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#1E2E45'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Спробувати ще",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  bool isApiCallProcess = false;
  sendEmail() async {
    setState(() {
      isApiCallProcess = true;
    });
    if (formKey.currentState.validate()) {
      var email = emailTextEditingController.text;
      String url =
          'http://generation-admin.ehub.com.ua/api/user/resetPassword/' +
              '$email';
      print(url);
      final response = await http.get(url);
      print(response.statusCode);
      if (response.statusCode == 200) {
        emailTextEditingController.clear();
        setState(() {
          isApiCallProcess = false;
        });
        showWindowSendSuccess();
      } else {
        setState(() {
          isApiCallProcess = false;
        });
        showWindowSendError();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Hexcolor('#7D5AC2'),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(_blankFocusNode);
          },
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              margin: EdgeInsets.only(right: 20, left: 20),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 80),
                      child: Image.asset(
                        'assets/fg_images/4_enter_logo.png',
                        width: 140,
                        height: 126,
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.only(
                                bottom: 30,
                                top: 30,
                                left: 10,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    behavior: HitTestBehavior.opaque,
                                    onTap: () {
                                      Navigator.pushNamed(context, '/4_enter');
                                    },
                                    child: Image.asset(
                                      'assets/fg_images/sportGames_icon_arrow_back.png',
                                      width: 10,
                                      height: 19,
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'Забули пароль?',
                                      style: TextStyle(
                                        color: Hexcolor('#FFFFFF'),
                                        fontSize: 25,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.28,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {},
                                      child: SizedBox(
                                        width: 10,
                                      )),
                                ],
                              )),
                          Form(
                            key: formKey,
                            child: Column(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        bottom: 7,
                                      ),
                                      child: Text(
                                        "Email",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'Arial',
                                          color: Colors.white,
                                          fontSize: 17,
                                        ),
                                      ),
                                    ),
                                    TextFormField(
                                      validator: (value) {
                                        return RegExp(
                                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                .hasMatch(value)
                                            ? null
                                            : "Будь-ласка введіть коректний email";
                                      },
                                      controller: emailTextEditingController,
                                      textAlign: TextAlign.left,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      decoration: InputDecoration(
                                        hintText: "Введіть email",
                                        contentPadding: new EdgeInsets.only(
                                            left: 20, top: 21.5, bottom: 21.5),
                                        hintStyle: TextStyle(
                                            color: Hexcolor('#D3D3D3'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Arial'),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(14.0),
                                          borderSide: BorderSide(
                                            color: Hexcolor('#FE6802'),
                                            width: 1,
                                          ),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(14.0),
                                          borderSide: BorderSide(
                                            color: Colors.white54,
                                            width: 1,
                                          ),
                                        ),
                                        focusedErrorBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(14.0),
                                          borderSide: BorderSide(
                                            color: Hexcolor('#FF1E1E'),
                                            width: 1,
                                          ),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(14.0),
                                          borderSide: BorderSide(
                                            color: Hexcolor('#FF1E1E'),
                                            width: 1,
                                          ),
                                        ),
                                        fillColor: Colors.white,
                                        filled: true,
                                      ),
                                      style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'Arial'),
                                    ),
                                  ],
                                ),

                                // ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Container(
                                    margin: EdgeInsets.only(top: 20),
                                    child: Text(
                                      // 'На вказаний Вами e-mail відправлений лист з посиланням для підтвердження реєстрації.\n\nПідтвердити реєстрацію ви можете на протязі 30 хвилин.',
                                      'На вказаний Вами e-mail буде відправлено тимчасовий пароль для авторизації.',
                                      style: TextStyle(
                                        color: Hexcolor('#FFFFFF'),
                                        fontSize: 15,
                                        fontFamily: 'Arial',
                                        height: 1.4,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),

                                Container(
                                  margin: EdgeInsets.only(top: 24),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width - 40,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    child: Text("Відправити",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Arial',
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      sendEmail();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
