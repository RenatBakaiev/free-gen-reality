import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '10_profile_rewards.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';

import 'package:http/http.dart' as http;
import '10_profile_json.dart';
import 'dart:convert';

class Missions12Quiz8 extends StatefulWidget {
  @override
  _Missions12Quiz8State createState() => _Missions12Quiz8State();
}

class _Missions12Quiz8State extends State<Missions12Quiz8> {
  bool isLoading = false;
  var userEmail = '';
  checkAuth() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    if (email == null) {
      print('Not authorized');
      setState(() {
        isLoading = false;
      });
    } else {
      print(email);
      setState(() {
        userEmail = email;
        isLoading = false;
      });
    }
  }

  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      print(response.body);
      print('quiz change done!!!');
    } else {
      // setState(() {
      //   isApiCallProcess = false;
      // });
      throw Exception('Failed to update User.');
    }
  }

  @override
  void initState() {
    checkAuth();
    super.initState();
    _controller = VideoPlayerController.network(
        'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_video_quiz8.mp4');
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    _controller.play();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool isPressedA = false;
  bool isPressedB = false;
  bool isPressedC = false;
  bool isPressedD = false;

  showInfoWindowWrongAnswer(answer) {
    setState(() {
      if (allRewards[0].quantity != 0) {
        allRewards[0].quantity = allRewards[0].quantity - 5;
      }
      if (allRewards[0].quantity <= 5) {
        allRewards[0].quantity = 0;
      }
    });
    Timer(
        Duration(milliseconds: 500),
        () => showDialog(
            barrierDismissible: false,
            barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
            context: context,
            builder: (context) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  height: 580,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Відповідь неправильна',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_wrong_answer.png',
                        height: 350,
                      ),
                      Text('Невдача',
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Hexcolor('#FF1E1E'),
                          ),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Вы вибрали неправильний варіант відповіді.',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            (userEmail == '' || userEmail == null)
                                ? Container()
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'С вашого рахунку знято ',
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                          color: Hexcolor('#747474'),
                                        ),
                                      ),
                                      Text(
                                        '5' + ' TUS',
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                          fontSize: 16,
                                          color: Hexcolor('#FE6802'),
                                        ),
                                      ),
                                    ],
                                  )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Спробувати ще раз",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            if (_controller.value.isPlaying) {
                              _controller.pause();
                            }
                            Navigator.pushNamed(
                                context, '/missions_12_quiz8.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  showInfoWindowRightAnswer(answer) {
    setState(() {
      allRewards[0].quantity = allRewards[0].quantity + 15;
    });
    Timer(
        Duration(milliseconds: 500),
        () => showDialog(
            barrierDismissible: false,
            barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
            context: context,
            builder: (context) {
              return Dialog(
                insetPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  // width: MediaQuery.of(context).size.width - 80,
                  height: 580,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Відповідь правильна',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/fg_images/questQuiz_pic_right_answer.png',
                        height: 350,
                      ),
                      Text('Вітаємо!',
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Hexcolor('#59B32D'),
                          ),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Вы вибрали правильний варіант відповіді.',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            (userEmail == '' || userEmail == null)
                                ? Container()
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Вам нараховано ',
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                          color: Hexcolor('#747474'),
                                        ),
                                      ),
                                      Text(
                                        '15' + ' TUS',
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w700,
                                          fontSize: 16,
                                          color: Hexcolor('#FE6802'),
                                        ),
                                      ),
                                    ],
                                  )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Продовжити",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            if (_controller.value.isPlaying) {
                              _controller.pause();
                            }
                            Navigator.pushNamed(
                                context, '/missions_12_desc8.dart');
                            setState(() {
                              allRewards[0].quantity =
                                  allRewards[0].quantity + 15;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  showWindowQuizDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдене',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви не можете повторно пройти це завдання',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            if (_controller.value.isPlaying) {
                              _controller.pause();
                            }
                            Navigator.pushNamed(
                                context, '/missions_12_desc8.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Stack(
                    overflow: Overflow.visible,
                    children: [
                      Positioned(
                        child: Container(
                          height: MediaQuery.of(context).size.height
                          //  - 85
                          ,
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 110),
                                  // height: 340,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(10.0),
                                        bottomLeft: Radius.circular(10.0)),
                                    // image: DecorationImage(
                                    //   image: AssetImage(
                                    //       'assets/fg_images/z12_missions_quiz8.gif'),
                                    //   fit: BoxFit.cover,
                                    // )
                                  ),
                                  child: GestureDetector(
                                    onTap: () {
                                      if (_controller.value.isPlaying) {
                                        _controller.pause();
                                      } else {
                                        _controller.play();
                                      }
                                    },
                                    child: FutureBuilder(
                                      future: _initializeVideoPlayerFuture,
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.done) {
                                          return AspectRatio(
                                            aspectRatio:
                                                _controller.value.aspectRatio,
                                            child: VideoPlayer(_controller),
                                          );
                                        } else {
                                          return Center(
                                            child: CircularProgressIndicator(),
                                          );
                                        }
                                      },
                                    ),
                                  ),
                                  // Container(
                                  //   margin: EdgeInsets.only(
                                  //     bottom: 20,
                                  //     right: 20,
                                  //   ),
                                  //   child: Column(
                                  //     mainAxisAlignment:
                                  //         MainAxisAlignment.spaceBetween,
                                  //     crossAxisAlignment:
                                  //         CrossAxisAlignment.center,
                                  //     children: [
                                  //       Row(
                                  //         mainAxisAlignment:
                                  //             MainAxisAlignment.spaceBetween,
                                  //         crossAxisAlignment:
                                  //             CrossAxisAlignment.center,
                                  //         children: [
                                  //           Container(),
                                  //           GestureDetector(
                                  //             behavior: HitTestBehavior.opaque,
                                  //             onTap: () {
                                  //               //  Navigator.pushNamed(context, '/player_video.dart');
                                  //             },
                                  //             child: Container(
                                  //               margin: EdgeInsets.only(
                                  //                 top: 30,
                                  //               ),
                                  //               // child: Image.asset(
                                  //               //   'assets/fg_images/player_video_start.png',
                                  //               //   height: 48,
                                  //               //   width: 48,
                                  //               // ),
                                  //             ),
                                  //           ),
                                  //         ],
                                  //       ),
                                  //       // Row(
                                  //       //   mainAxisAlignment: MainAxisAlignment.end,
                                  //       //   crossAxisAlignment: CrossAxisAlignment.end,
                                  //       //   children: [
                                  //       //     Text(
                                  //       //       '2',
                                  //       //       style: TextStyle(
                                  //       //         fontSize: 36,
                                  //       //         color: Colors.white,
                                  //       //         fontFamily: 'Arial',
                                  //       //         fontWeight: FontWeight.w700,
                                  //       //       ),
                                  //       //     ),
                                  //       //     Text(
                                  //       //       ' ' + '/ ' + '12',
                                  //       //       style: TextStyle(
                                  //       //         fontSize: 24,
                                  //       //         color: Colors.white,
                                  //       //         fontFamily: 'Arial',
                                  //       //         fontWeight: FontWeight.w400,
                                  //       //       ),
                                  //       //     ),
                                  //       //   ],
                                  //       // ),
                                  //     ],
                                  //   ),
                                  // ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width - 40,
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  child: SingleChildScrollView(
                                    physics: BouncingScrollPhysics(),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              bottom: 20, top: 20),
                                          child: Text(
                                            'Питання',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontFamily: 'Arial',
                                              letterSpacing: 1.025,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          '"Ранок починається з новорічного клопоту.." - пробурчав Мишко, встаючи з тепленького ліжечка о першій годині дня. Катя носилася по будинку на сегвеї, поспішаючи до майстра. І Михайлові навіть заздрісно стало, бо йому не світять 3 години спокою, щирі розмови, а в результаті - красиві нігтики 💅\n"Міша, тобі не важко буде допомогти мамі прикрасити ялинку? " - запитала Катя.\n"Сподіваюся, іграшки не з Дубаю !?" - подумав він.\n\nНайбільша ялинкова іграшка важить, як:',
                                          style: TextStyle(
                                            color: Hexcolor('#545454'),
                                            fontSize: 14,
                                            fontFamily: 'Arial',
                                            letterSpacing: 1.025,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 20,
                                    // bottom: 20,
                                    left: 20,
                                  ),
                                  alignment: Alignment.centerLeft,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '1) ' + 'Катя',
                                        style: TextStyle(
                                            color: Hexcolor('#747474'),
                                            fontFamily: 'Arial',
                                            fontSize: 16),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        '2) ' + 'літак - Боїнг 737',
                                        style: TextStyle(
                                            color: Hexcolor('#747474'),
                                            fontFamily: 'Arial',
                                            fontSize: 16),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        '3) ' + 'слон',
                                        style: TextStyle(
                                            color: Hexcolor('#747474'),
                                            fontFamily: 'Arial',
                                            fontSize: 16),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        '4) ' + 'машина - Ніссан Жук',
                                        style: TextStyle(
                                            color: Hexcolor('#747474'),
                                            fontFamily: 'Arial',
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 15,
                                    right: 15,
                                    top: 20,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 1",
                                            style: TextStyle(
                                              color: isPressedA
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedA
                                              ? Colors.red
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            setState(() {
                                              isLoading = true;
                                            });
                                            final SharedPreferences
                                                sharedPreferences =
                                                await SharedPreferences
                                                    .getInstance();
                                            var done = sharedPreferences
                                                .get("isQuiz8Done");
                                            if (done == true) {
                                              showWindowQuizDone();
                                              setState(() {
                                                isPressedA = true;
                                                isLoading = false;
                                              });
                                              return;
                                            } else {
                                              if ((userEmail != '' ||
                                                  userEmail != null)) {
                                                final SharedPreferences
                                                    sharedPreferences =
                                                    await SharedPreferences
                                                        .getInstance();

                                                int tus = sharedPreferences
                                                    .get("tus");
                                                sharedPreferences.setInt(
                                                    'tus', tus - 5);
                                                setState(() {
                                                  isPressedA = true;
                                                });
                                              }
                                            }

                                            changeTus(5.0, "WITHDRAW");
                                            showInfoWindowWrongAnswer(
                                                'Вариант 1');
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 2",
                                            style: TextStyle(
                                              color: isPressedB
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedB
                                              ? Colors.red
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            setState(() {
                                              isLoading = true;
                                            });
                                            final SharedPreferences
                                                sharedPreferences =
                                                await SharedPreferences
                                                    .getInstance();
                                            var done = sharedPreferences
                                                .get("isQuiz8Done");
                                            if (done == true) {
                                              showWindowQuizDone();
                                              setState(() {
                                                isPressedB = true;
                                                isLoading = false;
                                              });
                                              return;
                                            } else {
                                              if ((userEmail != '' ||
                                                  userEmail != null)) {
                                                final SharedPreferences
                                                    sharedPreferences =
                                                    await SharedPreferences
                                                        .getInstance();

                                                int tus = sharedPreferences
                                                    .get("tus");
                                                sharedPreferences.setInt(
                                                    'tus', tus - 5);
                                                setState(() {
                                                  isPressedB = true;
                                                });
                                              }
                                            }

                                            changeTus(5.0, "WITHDRAW");
                                            showInfoWindowWrongAnswer(
                                                'Вариант 2');
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    left: 15,
                                    right: 15,
                                    top: 10,
                                    bottom: 10,
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 3",
                                            style: TextStyle(
                                              color: isPressedC
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedC
                                              ? Colors.red
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            setState(() {
                                              isLoading = true;
                                            });
                                            final SharedPreferences
                                                sharedPreferences =
                                                await SharedPreferences
                                                    .getInstance();
                                            var done = sharedPreferences
                                                .get("isQuiz8Done");
                                            if (done == true) {
                                              showWindowQuizDone();
                                              setState(() {
                                                isPressedC = true;
                                                isLoading = false;
                                              });
                                              return;
                                            } else {
                                              if ((userEmail != '' ||
                                                  userEmail != null)) {
                                                final SharedPreferences
                                                    sharedPreferences =
                                                    await SharedPreferences
                                                        .getInstance();

                                                int tus = sharedPreferences
                                                    .get("tus");
                                                sharedPreferences.setInt(
                                                    'tus', tus - 5);
                                                setState(() {
                                                  isPressedC = true;
                                                });
                                              }
                                            }

                                            changeTus(5.0, "WITHDRAW");
                                            showInfoWindowWrongAnswer(
                                                "Вариант 3");
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        height: 60,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                20,
                                        child: RaisedButton(
                                          child: Text(
                                            "Вариант 4",
                                            style: TextStyle(
                                              color: isPressedD
                                                  ? Colors.white
                                                  : Hexcolor('#545454'),
                                              fontSize: 17,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          color: isPressedD
                                              ? Hexcolor('#75C433')
                                              : Hexcolor('#F4F4F4'),
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0)),
                                          onPressed: () async {
                                            setState(() {
                                              isLoading = true;
                                            });
                                            final SharedPreferences
                                                sharedPreferences =
                                                await SharedPreferences
                                                    .getInstance();
                                            var done = sharedPreferences
                                                .get("isQuiz8Done");
                                            if (done == true) {
                                              showWindowQuizDone();
                                              setState(() {
                                                isPressedD = true;
                                                isLoading = false;
                                              });
                                              return;
                                            } else {
                                              if (userEmail != '' ||
                                                  userEmail != null) {
                                                final SharedPreferences
                                                    sharedPreferences =
                                                    await SharedPreferences
                                                        .getInstance();

                                                int tus = sharedPreferences
                                                    .get("tus");
                                                print(tus);
                                                setState(() {
                                                  isLoading = true;
                                                  isPressedD = true;
                                                  sharedPreferences.setBool(
                                                      'isQuiz8Done', true);
                                                  sharedPreferences.setInt(
                                                      'tus', tus + 15);
                                                });
                                              }
                                            }
                                            changeTus(15.0, "DEPOSIT");
                                            showInfoWindowRightAnswer(
                                                'Вариант 4');
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Hexcolor('#7D5AC2'),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                          ),
                          alignment: Alignment.bottomLeft,
                          margin: EdgeInsets.only(
                            top: 50,
                            bottom: 22,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: SizedBox(
                                      child: IconButton(
                                        icon: Image.asset(
                                          'assets/fg_images/6_home_search_back.png',
                                          width: 10,
                                          height: 19,
                                        ),
                                        onPressed: () {
                                          if (_controller.value.isPlaying) {
                                            _controller.pause();
                                          }
                                          Navigator.pushNamed(context,
                                              '/missions_12_12_trees.dart');
                                        },
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Вікторина',
                                    style: TextStyle(
                                        fontSize: 32,
                                        color: Colors.white,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                child: SizedBox(
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/run_map_quest_pic_gostart.png',
                                      width: 22,
                                      height: 24,
                                    ),
                                    onPressed: () {
                                      if (_controller.value.isPlaying) {
                                        _controller.pause();
                                      }
                                      Navigator.pushNamed(
                                        context,
                                        '/missions_12_desc.dart',
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}
