import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class EnterRegisterClose extends StatefulWidget {
  @override
  _EnterRegisterCloseState createState() => _EnterRegisterCloseState();
}

class _EnterRegisterCloseState extends State<EnterRegisterClose> {
  // showSuccessRegisterWindow() {
  //   showDialog(
  //       barrierDismissible: false,
  //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
  //       context: context,
  //       builder: (context) {
  //         return Dialog(
  //           insetPadding: EdgeInsets.only(
  //             left: 20,
  //             right: 20,
  //           ),
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           child: Container(
  //             // width: MediaQuery.of(context).size.width - 40,
  //             height: 435,
  //             child: Container(
  //               margin: EdgeInsets.only(
  //                 left: 20,
  //                 right: 20,
  //               ),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //                   Container(
  //                     margin: EdgeInsets.only(
  //                       top: 20,
  //                     ),
  //                     child: Text(
  //                       'Регистрация',
  //                       style: TextStyle(
  //                         color: Hexcolor('#1E2E45'),
  //                         fontSize: 24,
  //                         fontFamily: 'Arial',
  //                         fontWeight: FontWeight.w700,
  //                       ),
  //                       textAlign: TextAlign.center,
  //                     ),
  //                   ),
  //                   Container(
  //                     child: Text(
  //                       'Вы завершили первый этап регистрации!',
  //                       style: TextStyle(
  //                         color: Hexcolor('#298127'),
  //                         fontSize: 20,
  //                         fontFamily: 'Arial',
  //                         fontWeight: FontWeight.w700,
  //                       ),
  //                       textAlign: TextAlign.center,
  //                     ),
  //                   ),
  //                   Text(
  //                     'Письмо для подтверждения регистрации было отправлено на Ваш e-mail.',
  //                     style: TextStyle(
  //                       color: Hexcolor('#747474'),
  //                       fontSize: 20,
  //                       fontFamily: 'Arial',
  //                       fontWeight: FontWeight.w400,
  //                     ),
  //                     textAlign: TextAlign.center,
  //                   ),
  //                   Text(
  //                     'Подтвердить регистрацию и получить бонус Вы можете в течение 30 минут.',
  //                     style: TextStyle(
  //                       color: Hexcolor('#747474'),
  //                       fontSize: 20,
  //                       fontFamily: 'Arial',
  //                       fontWeight: FontWeight.w400,
  //                     ),
  //                     textAlign: TextAlign.center,
  //                   ),
  //                   Container(
  //                     margin: EdgeInsets.only(
  //                       bottom: 20,
  //                     ),
  //                     child: Column(
  //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                       children: [
  //                         Container(
  //                           height: 60,
  //                           width: MediaQuery.of(context).size.width,
  //                           child: RaisedButton(
  //                             child: Text(
  //                               "Продолжить",
  //                               style: TextStyle(
  //                                 color: Colors.white,
  //                                 fontSize: 17,
  //                                 fontFamily: 'Arial',
  //                                 fontWeight: FontWeight.w600,
  //                               ),
  //                             ),
  //                             color: Hexcolor('#FE6802'),
  //                             shape: new RoundedRectangleBorder(
  //                                 borderRadius:
  //                                     new BorderRadius.circular(14.0)),
  //                             onPressed: () {
  //                               Navigator.pushNamed(
  //                                   context, '/5_myBottomBar.dart');
  //                             },
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         );
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Hexcolor('#7D5AC2'),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {},
          child: Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 80,
                          bottom: 30,
                        ),
                        child: Image.asset(
                          'assets/fg_images/4_enter_logo.png',
                          width: 140,
                          height: 126,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              Navigator.pushNamed(context, '/4_enter_register');
                            },
                            child: Image.asset(
                              'assets/fg_images/sportGames_icon_arrow_back.png',
                              width: 10,
                              height: 19,
                            ),
                          ),
                          Container(
                            child: Text(
                              'Регистрация',
                              style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 35,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Arial',
                                letterSpacing: 1.28,
                              ),
                            ),
                          ),
                          GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {},
                              child: SizedBox(
                                width: 10,
                              )),
                        ],
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          child: Text(
                            'Вы завершили первый этап регистрации!\n\nПисьмо для подтверждения регистрации было отправлено на Ваш e-mail.\n\nПодтвердить регистрацию и получить бонус Вы можете в течение 30 минут',
                            style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                height: 1.4),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      bottom: 20,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      child: Text("Закрыть",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Arial',
                              color: Colors.white,
                              letterSpacing: 1.09,
                              fontSize: 17)),
                      onPressed: () {
                        Navigator.pushNamed(context, '/4_enter');
                        // showSuccessRegisterWindow();
                      },
                    ),
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
