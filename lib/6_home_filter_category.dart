import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '6_home_filter_category_requests.dart';

class HomeFilterCategory extends StatefulWidget {
  @override
  _HomeFilterCategoryState createState() => _HomeFilterCategoryState();
}

class _HomeFilterCategoryState extends State<HomeFilterCategory> {
  // int _selectedIndex;
  // _onSelected(int index) {
  //   setState(() {
  //     _selectedIndex = index;
  //   });
  // }
  // bool checkBoxValue = false;
  // bool _isChecked = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#FFFFFF'),
          // color: Colors.blue,
        ),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(
                  top: 50,
                  left: 15,
                  right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Рубрика',
                      style: TextStyle(
                          fontSize: 37,
                          color: Colors.white,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w900),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 38,
                          height: 38,
                          child: IconButton(
                            icon: Image.asset(
                              'assets/fg_images/6_home_filter_close.png',
                              width: 20,
                              height: 20,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/6_home_filter.dart');
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              child: Expanded(
                  child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  // height: MediaQuery.of(context).size.height + 210,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(
                    // top: 20,
                    // right: 25,
                    left: 25,
                  ),
                  child: Column(
                    children: [
                      Container(
                        // height: MediaQuery.of(context).size.height-0,
                        margin: EdgeInsets.only(top: 15),
                        child: Column(children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              // margin: EdgeInsets.only(top: 20),
                              child: Text(
                                'Последние запросы',
                                style: TextStyle(
                                  color: Hexcolor('#919191'),
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 1.089,
                                ),
                              ),
                            ),
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height: 135,
                              // child: Expanded(
                              child: ListView.builder(
                                  physics: BouncingScrollPhysics(),
                                  itemCount: lastRequests.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        setState(() {
                                          lastRequests[index].isSelected =
                                              !lastRequests[index].isSelected;
                                        });
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 10),
                                        // height: 32,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              lastRequests[index].request,
                                              style: TextStyle(
                                                color: Hexcolor('#7D5AC2'),
                                                fontSize: 17,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w500,
                                                // letterSpacing: 1.089,
                                              ),
                                            ),
                                            lastRequests[index].isSelected
                                                ? Container(
                                                    margin: EdgeInsets.only(
                                                      right: 20,
                                                    ),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_filter_location_selected.png',
                                                      width: 30,
                                                      height: 30,
                                                    ),
                                                  )
                                                : Container(
                                                    margin: EdgeInsets.only(
                                                      right: 20,
                                                    ),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_filter_location_unselected.png',
                                                      width: 30,
                                                      height: 30,
                                                    ),
                                                  )
                                          ],
                                        ),
                                      ),
                                    );
                                  })
                              // ),
                              ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 20),
                              child: Text(
                                'Весь список миссий',
                                style: TextStyle(
                                  color: Hexcolor('#919191'),
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 1.089,
                                ),
                              ),
                            ),
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height - 400,
                              // child: Expanded(
                              child: ListView.builder(
                                  physics: BouncingScrollPhysics(),
                                  itemCount: quests.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        setState(() {
                                          quests[index].isSelected =
                                              !quests[index].isSelected;
                                        });
                                      },
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                              ),
                                              child: Text(
                                                quests[index].quest,
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 17,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w500,
                                                  // letterSpacing: 1.089,
                                                ),
                                              ),
                                            ),
                                            quests[index].isSelected
                                                ? Container(
                                                    margin: EdgeInsets.only(
                                                      right: 20,
                                                    ),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_filter_location_selected.png',
                                                      width: 30,
                                                      height: 30,
                                                    ),
                                                  )
                                                : Container(
                                                    margin: EdgeInsets.only(
                                                      right: 20,
                                                    ),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_filter_location_unselected.png',
                                                      width: 30,
                                                      height: 30,
                                                    ),
                                                  )
                                          ],
                                        ),
                                      ),
                                    );
                                  })),
                          // ),
                        ]),
                      )
                    ],
                  ),
                ),
              )),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Готово",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Arial',
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_home_filter.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
