import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/6_home_quests.dart';
import 'package:flutter_free_gen_reality/8_camera_preview.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
// import 'package:flutter_free_gen_reality/run_map_quest.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:model_viewer/model_viewer.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Missions12Scan10 extends StatefulWidget {
  @override
  _Missions12Scan10State createState() => _Missions12Scan10State();
}

class _Missions12Scan10State extends State {
  bool isApiCallProcess = false;
  CameraController controller;
  List cameras;
  int selectedCameraIndex;
  String imgPath;

  bool isListHidden = false;
  bool isImageHidden = false;

  // GlobalKey _globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      cameras = availableCameras;

      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        _initCameraController(cameras[selectedCameraIndex]).then((void v) {});
      } else {
        print('No camera available');
      }
    }).catchError((err) {
      print('Error :${err.code}Error message : ${err.message}');
    });
    // _requestPermission();
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.high);

    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (controller.value.hasError) {
        print('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Урааа',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Перейти на наступний крок",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              setState(() {
                                isApiCallProcess = true;
                              });
                              final SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              var res1 =
                                  sharedPreferences.getBool('isStep1Completed');
                              var res2 =
                                  sharedPreferences.getBool('isStep2Completed');
                              var res3 =
                                  sharedPreferences.getBool('isStep3Completed');
                              var res4 =
                                  sharedPreferences.getBool('isStep4Completed');
                              var res5 =
                                  sharedPreferences.getBool('isStep5Completed');
                              var res6 =
                                  sharedPreferences.getBool('isStep6Completed');
                              var res7 =
                                  sharedPreferences.getBool('isStep7Completed');
                              var res8 =
                                  sharedPreferences.getBool('isStep8Completed');
                              var res9 =
                                  sharedPreferences.getBool('isStep9Completed');
                              var res10 = sharedPreferences
                                  .getBool('isStep10Completed');
                              var res11 = sharedPreferences
                                  .getBool('isStep11Completed');
                              var res12 = sharedPreferences
                                  .getBool('isStep12Completed');
                              if (res1 == true &&
                                  res2 == true &&
                                  res3 == true &&
                                  res4 == true &&
                                  res5 == true &&
                                  res6 == true &&
                                  res7 == true &&
                                  res8 == true &&
                                  res9 == true &&
                                  res10 == true &&
                                  res11 == true &&
                                  res12 == true) {
                                final SharedPreferences sharedPreferences =
                                    await SharedPreferences.getInstance();
                                int tus = sharedPreferences.get("tus");
                                sharedPreferences.setInt('tus', tus + 100);
                                Navigator.pushNamed(
                                    context, '/quest_done.dart');
                                setState(() {
                                  isApiCallProcess = false;
                                });
                              } else {
                                Navigator.pushNamed(
                                    context, '/missions_12_12_trees.dart');
                                setState(() {
                                  isApiCallProcess = false;
                                });
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: _cameraPreviewWidget(),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            left: 20,
            child: Container(
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Продовжити",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () async {
                  showInfoWindow();
                  final SharedPreferences sharedPreferences =
                      await SharedPreferences.getInstance();
                  var userEmail = sharedPreferences.get("email");
                  int tus = sharedPreferences.get("tus");
                  if (userEmail != '' && userEmail != null) {
                    sharedPreferences.setBool('isStep10Completed', true);
                    sharedPreferences.setInt('tus', tus + 20);
                    setState(() {
                      checkPoins10[9].isCompleted = true;
                    });
                  }
                },
              ),
            ),
          ),
          Positioned(
            bottom: 80,
            child: Container(
              height: 90,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(20),
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  // _openGallery(context),
                  // SizedBox(
                  //   width: 20,
                  // ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isListHidden = !isListHidden;
                      });
                    },
                    child: SizedBox(
                      width: 34,
                      child: Image.asset(
                        'assets/fg_images/8_camera_icon_ar.png',
                        height: 34,
                        width: 34,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  // Spacer(),
                  _cameraControlWidget(context),
                  // SizedBox(
                  //   width: 33,
                  // ),
                  // Spacer(),
                  SizedBox(
                    width: 34,
                  ),
                  // _openQrCode(context),
                  _cameraToggleRowWidget(),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 170,
            child: isListHidden
                ? Container(
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: ar_10.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.only(
                              right: 20,
                            ),
                            height: 300,
                            width: 100,
                            child: ModelViewer(
                              backgroundColor: Colors.white.withOpacity(0),
                              src: ar_10[index].image,
                              ar: true,
                              autoRotate: false,
                              cameraControls: true,
                              autoPlay: true
                            ),
                          );
                        }),
                  )
                : Container(),
          ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                // Navigator.pushNamed(context, '/5_myBottomBar.dart');
              },
            ),
          ),
        ],
      ),
    );
  }

  /// Display Camera preview.
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Loading',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }

    return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: CameraPreview(controller),
    );
  }

  /// Display the control bar with buttons to take pictures
  Widget _cameraControlWidget(context) {
    return FloatingActionButton(
      child: Image.asset(
        'assets/fg_images/8_camera_icon_make_pic.png',
        width: 90,
        height: 90,
      ),
      backgroundColor: Colors.transparent,
      onPressed: () {
        _onCapturePressed(context);
        // _saveScreen();
        // loadPrefs();
      },
    );
  }

  // Widget _openQrCode(context) {
  //   return GestureDetector(
  //     onTap: () {
  //       Navigator.push(
  //         context,
  //         MaterialPageRoute(builder: (context) => ScanQrCode()),
  //       );
  //     },
  //     child: Image.asset(
  //       'assets/fg_images/8_camera_icon_qr_code.png',
  //       width: 34,
  //       height: 34,
  //       color: Colors.white,
  //     ),
  //   );
  // }

  // Widget _openGallery(context) {
  //   return GestureDetector(
  //     onTap: () {
  //       _openPhoneGallery(context);
  //     },
  //     child: Container(
  //       width: 45,
  //       height: 45,
  //       decoration: BoxDecoration(
  //           borderRadius: BorderRadius.all(Radius.circular(11.5)),
  //           image: DecorationImage(
  //             image: AssetImage('assets/fg_images/8_camera_icon_gallery.png'),
  //             fit: BoxFit.cover,
  //           )),
  //     ),
  //   );
  // }

  File imageFile;
  final picker = ImagePicker();
  // _openPhoneGallery(BuildContext context) async {
  //   var picture = await picker.getImage(source: ImageSource.gallery);
  //   this.setState(() {
  //     imageFile = File(picture.path);
  //   });
  //   // Navigator.of(context).pop();
  // }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraToggleRowWidget() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }
    // CameraDescription selectedCamera = cameras[selectedCameraIndex];
    // CameraLensDirection lensDirection = selectedCamera.lensDirection;

    return GestureDetector(
      onTap: _onSwitchCamera,
      child: Container(
        // margin: EdgeInsets.only(left: 20),
        child: Image.asset(
          'assets/fg_images/8_camera_icon_toggle.png',
          width: 40,
          height: 35,
        ),
      ),
    );
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error:${e.code}\nError message : ${e.description}';
    print(errorText);
  }

  void _onCapturePressed(context) async {
    try {
      final path =
          join((await getTemporaryDirectory()).path, '${DateTime.now()}.png');
      await controller.takePicture(path);

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PreviewScreen(
                  imgPath: path,
                )),
      );
    } catch (e) {
      _showCameraException(e);
    }
  }

  void _onSwitchCamera() {
    selectedCameraIndex =
        selectedCameraIndex < cameras.length - 1 ? selectedCameraIndex + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    _initCameraController(selectedCamera);
  }
}

//https://www.youtube.com/watch?v=kgqiMzjAyZo&t=306s - tutorial
//https://www.the-qrcode-generator.com/ - qr generator
class ScanQrCode extends StatefulWidget {
  @override
  _ScanQrCodeState createState() => _ScanQrCodeState();
}

class _ScanQrCodeState extends State<ScanQrCode> {
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  QRViewController controller;

  @override
  Widget build(BuildContext context) {
    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Урааа',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Перейти на наступний крок",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              Navigator.pushNamed(
                                  context, '/missions_12_12_trees.dart');
                              final SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              // sharedPreferences.clear();
                              var userEmail = sharedPreferences.get("email");
                              if (userEmail != '' && userEmail != null) {
                                sharedPreferences.setBool(
                                    'isStep1Completed', true);
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Column(
            children: [
              Expanded(
                flex: 9,
                child: QRView(
                    key: qrKey,
                    overlay: QrScannerOverlayShape(
                      borderRadius: 10,
                      borderColor: Colors.red,
                      borderLength: 30,
                      borderWidth: 10,
                      cutOutSize: 300,
                    ),
                    onQRViewCreated: _onQRViewCreate),
              ),
            ],
          ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          Positioned(
            bottom: 60,
            left: 20,
            child: Container(
              decoration: BoxDecoration(
                color: Hexcolor('#FE6802'),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
              ),
              // color: Hexcolor('#FE6802'),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  qrText == ''
                      ? 'Відскануйте qr код'
                      : 'Натисніть для відображення',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  // if (qrText == 'qr1') {print('код 1');}
                  // if (qrText == 'qr2') {print('код 2');}
                  if (qrText != '') {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            ScanQRcodeResult(text: '$qrText')));
                  }
                },
              ),
            ),
          ),
          Positioned(
            bottom: 21.5,
            left: MediaQuery.of(context).size.width / 2 - 50,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                showInfoWindow();
              },
              child: Container(
                // width: 110,
                child: Center(
                    child: Text(
                  'Пропустити',
                  style: TextStyle(
                    color: Hexcolor('#FFFFFF'),
                    fontFamily: 'Arial',
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                )),
              ),
            ),
          )
        ],
      ),
    );
  }

  //global variables:

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }
}

class ScanQRcodeResult extends StatefulWidget {
  final String text;
  ScanQRcodeResult({Key key, @required this.text}) : super(key: key);

  @override
  _ScanQRcodeResultState createState() => _ScanQRcodeResultState();
}

class _ScanQRcodeResultState extends State<ScanQRcodeResult> {
  // showInfoWindow() {
  //   showDialog(
  //       barrierDismissible: false,
  //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
  //       context: this.context,
  //       builder: (context) {
  //         return Dialog(
  //           insetPadding: EdgeInsets.only(
  //             left: 20,
  //             right: 20,
  //           ),
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(20),
  //           ),
  //           child: Stack(
  //             overflow: Overflow.visible,
  //             children: [
  //               Container(
  //                 height: 390,
  //                 child: Column(
  //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                   crossAxisAlignment: CrossAxisAlignment.center,
  //                   children: [
  //                     Container(
  //                       margin: EdgeInsets.only(
  //                         top: 20,
  //                       ),
  //                       child: Text(
  //                         'Урааа',
  //                         style: TextStyle(
  //                           color: Hexcolor('#1E2E45'),
  //                           fontSize: 24,
  //                           fontFamily: 'Arial',
  //                           fontWeight: FontWeight.w700,
  //                         ),
  //                       ),
  //                     ),
  //                     Container(
  //                       // start progress line
  //                       margin: EdgeInsets.only(top: 30),
  //                       child: Row(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         crossAxisAlignment: CrossAxisAlignment.center,
  //                         children: [
  //                           Stack(
  //                             overflow: Overflow.visible,
  //                             children: [
  //                               Image.asset(
  //                                   'assets/fg_images/next_step_icon_start.png',
  //                                   width: 12,
  //                                   height: 12),
  //                               Positioned(
  //                                 bottom: 9,
  //                                 left: 4.5,
  //                                 child: Row(
  //                                   mainAxisAlignment: MainAxisAlignment.center,
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.center,
  //                                   children: [
  //                                     Image.asset(
  //                                         'assets/fg_images/next_step_icon_start_flag.png',
  //                                         width: 18,
  //                                         height: 21),
  //                                     Container(
  //                                       margin: EdgeInsets.only(
  //                                           left: 10, bottom: 8),
  //                                       child: Text(
  //                                         'ул. Владимирская, 24а',
  //                                         style: TextStyle(
  //                                           color: Hexcolor('#1E2E45'),
  //                                           fontSize: 12,
  //                                           fontFamily: 'Arial',
  //                                           fontWeight: FontWeight.w400,
  //                                         ),
  //                                       ),
  //                                     )
  //                                   ],
  //                                 ),
  //                               )
  //                             ],
  //                           ),
  //                           Container(
  //                             height: 1.5,
  //                             width: 25,
  //                             decoration:
  //                                 BoxDecoration(color: Hexcolor('#FE6802')),
  //                           ),
  //                           Image.asset(
  //                               'assets/fg_images/next_step_icon_done_step.png',
  //                               width: 12,
  //                               height: 12),
  //                           Container(
  //                             height: 1.5,
  //                             width: 25,
  //                             decoration:
  //                                 BoxDecoration(color: Hexcolor('#FE6802')),
  //                           ),
  //                           Image.asset(
  //                               'assets/fg_images/next_step_icon_done_step.png',
  //                               width: 12,
  //                               height: 12),
  //                           Container(
  //                             height: 1.5,
  //                             width: 25,
  //                             decoration:
  //                                 BoxDecoration(color: Hexcolor('#FE6802')),
  //                           ),
  //                           Image.asset(
  //                               'assets/fg_images/next_step_icon_done_step.png',
  //                               width: 12,
  //                               height: 12),
  //                           Container(
  //                             height: 1.5,
  //                             width: 25,
  //                             decoration:
  //                                 BoxDecoration(color: Hexcolor('#1E2E45')),
  //                           ),
  //                           Stack(
  //                             overflow: Overflow.visible,
  //                             children: [
  //                               Image.asset(
  //                                   'assets/fg_images/next_step_icon_notdone_step.png',
  //                                   width: 12,
  //                                   height: 12),
  //                               Positioned(
  //                                 top: 9,
  //                                 right: 4.5,
  //                                 child: Row(
  //                                   mainAxisAlignment: MainAxisAlignment.center,
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.center,
  //                                   children: [
  //                                     Container(
  //                                       margin:
  //                                           EdgeInsets.only(right: 10, top: 8),
  //                                       child: Text(
  //                                         'ул. Богдана Хмельницкого, 13/2',
  //                                         style: TextStyle(
  //                                           color: Hexcolor('#1E2E45'),
  //                                           fontSize: 12,
  //                                           fontFamily: 'Arial',
  //                                           fontWeight: FontWeight.w400,
  //                                         ),
  //                                       ),
  //                                     ),
  //                                     Image.asset(
  //                                         'assets/fg_images/next_step_icon_finish_flag.png',
  //                                         width: 18,
  //                                         height: 21),
  //                                   ],
  //                                 ),
  //                               )
  //                             ],
  //                           ),
  //                         ],
  //                       ),
  //                     ),
  //                     Container(
  //                       margin: EdgeInsets.only(
  //                           // top: 10,
  //                           ),
  //                       child: Text(
  //                         'Вы молодец, прошли шаг миссии.\nНажмите кнопку для продолжения',
  //                         style: TextStyle(
  //                           color: Hexcolor('#747474'),
  //                           fontSize: 16,
  //                           fontFamily: 'Arial',
  //                           fontWeight: FontWeight.w400,
  //                         ),
  //                         textAlign: TextAlign.center,
  //                       ),
  //                     ),
  //                     Container(
  //                       margin: EdgeInsets.only(
  //                         left: 20,
  //                         right: 20,
  //                         bottom: 20,
  //                       ),
  //                       height: 60,
  //                       width: MediaQuery.of(context).size.width - 80,
  //                       child: RaisedButton(
  //                         child: Text(
  //                           "Перейти на cледующий шаг",
  //                           style: TextStyle(
  //                             color: Colors.white,
  //                             fontSize: 17,
  //                             fontFamily: 'Arial',
  //                             fontWeight: FontWeight.w700,
  //                           ),
  //                         ),
  //                         color: Hexcolor('#FE6802'),
  //                         shape: new RoundedRectangleBorder(
  //                             borderRadius: new BorderRadius.circular(14.0)),
  //                         onPressed: () {
  //                           Navigator.pushReplacement(
  //                               context,
  //                               MaterialPageRoute(
  //                                   builder: (BuildContext context) =>
  //                                       RunMapQuest()));
  //                         },
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //               Positioned(
  //                 top: -130,
  //                 right: -30,
  //                 child: Image.asset(
  //                   'assets/fg_images/error_mission_icon_unlock.png',
  //                   width: 200,
  //                   height: 200,
  //                 ),
  //               ),
  //               Positioned(
  //                 right: 40,
  //                 top: 50,
  //                 child: Container(
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.end,
  //                     crossAxisAlignment: CrossAxisAlignment.end,
  //                     children: [
  //                       Text(
  //                         '3',
  //                         style: TextStyle(
  //                           fontSize: 36,
  //                           color: Hexcolor('#1E2E45'),
  //                           fontFamily: 'Arial',
  //                           fontWeight: FontWeight.w700,
  //                         ),
  //                       ),
  //                       Container(
  //                         margin: EdgeInsets.only(bottom: 2),
  //                         child: Text(
  //                           ' ' + '/ ' + '3',
  //                           style: TextStyle(
  //                             fontSize: 24,
  //                             color: Hexcolor('#1E2E45'),
  //                             fontFamily: 'Arial',
  //                             fontWeight: FontWeight.w400,
  //                           ),
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               )
  //             ],
  //           ),
  //         );
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    showInfoWindow() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    width: 390,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Урааа',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        // Container(
                        //   // start progress line
                        //   margin: EdgeInsets.only(top: 30),
                        //   child: Column(
                        //     children: [
                        //       Row(
                        //         mainAxisAlignment: MainAxisAlignment.center,
                        //         crossAxisAlignment: CrossAxisAlignment.center,
                        //         children: [
                        //           Stack(
                        //             overflow: Overflow.visible,
                        //             children: [
                        //               Image.asset(
                        //                   'assets/fg_images/next_step_icon_start.png',
                        //                   width: 12,
                        //                   height: 12),
                        //               Positioned(
                        //                 bottom: 9,
                        //                 left: 4.5,
                        //                 child: Row(
                        //                   mainAxisAlignment:
                        //                       MainAxisAlignment.center,
                        //                   crossAxisAlignment:
                        //                       CrossAxisAlignment.center,
                        //                   children: [
                        //                     Image.asset(
                        //                         'assets/fg_images/next_step_icon_start_flag.png',
                        //                         width: 18,
                        //                         height: 21),
                        //                     Container(
                        //                       margin: EdgeInsets.only(
                        //                           left: 10, bottom: 8),
                        //                       child: Text(
                        //                         '',
                        //                         style: TextStyle(
                        //                           color: Hexcolor('#1E2E45'),
                        //                           fontSize: 12,
                        //                           fontFamily: 'Arial',
                        //                           fontWeight: FontWeight.w400,
                        //                         ),
                        //                       ),
                        //                     )
                        //                   ],
                        //                 ),
                        //               )
                        //             ],
                        //           ),
                        //           // Container(
                        //           //   height: 1.5,
                        //           //   width: 25,
                        //           //   decoration:
                        //           //       BoxDecoration(color: Hexcolor('#1E2E45')),
                        //           // ),
                        //           Row(
                        //             children: [
                        //               Container(
                        //                 height: 1.5,
                        //                 width: 25,
                        //                 decoration: BoxDecoration(
                        //                     color: Hexcolor('#FE6802')),
                        //               ),
                        //               Image.asset(
                        //                   'assets/fg_images/next_step_icon_done_step.png',
                        //                   width: 12,
                        //                   height: 12),
                        //             ],
                        //           ),
                        //           Container(
                        //             height: 25,
                        //             child: ListView.builder(
                        //                 shrinkWrap: true,
                        //                 scrollDirection: Axis.horizontal,
                        //                 itemCount: 5,
                        //                 itemBuilder: (context, index) {
                        //                   return Row(
                        //                     children: [
                        //                       Container(
                        //                         height: 1.5,
                        //                         width: 25,
                        //                         decoration: BoxDecoration(
                        //                             color: Hexcolor('#1E2E45')),
                        //                       ),
                        //                       Image.asset(
                        //                           'assets/fg_images/next_step_icon_notdone_step.png',
                        //                           width: 12,
                        //                           height: 12),
                        //                     ],
                        //                   );
                        //                 }),
                        //           ),
                        //           Container(
                        //             margin: EdgeInsets.only(
                        //               top: 11,
                        //             ),
                        //             child: Image.asset(
                        //               'assets/fg_images/turn_icon.png',
                        //               width: 25,
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //       Container(
                        //         margin: EdgeInsets.only(
                        //           right: 50,
                        //         ),
                        //         child: Row(
                        //           mainAxisAlignment: MainAxisAlignment.end,
                        //           crossAxisAlignment: CrossAxisAlignment.center,
                        //           children: [
                        //             Image.asset(
                        //                 'assets/fg_images/next_step_icon_notdone_step.png',
                        //                 width: 12,
                        //                 height: 12),
                        //           ],
                        //         ),
                        //       ),
                        //       Row(
                        //         mainAxisAlignment: MainAxisAlignment.end,
                        //         crossAxisAlignment: CrossAxisAlignment.center,
                        //         children: [
                        //           Stack(
                        //             overflow: Overflow.visible,
                        //             children: [
                        //               Image.asset(
                        //                   'assets/fg_images/next_step_icon_notdone_step.png',
                        //                   width: 12,
                        //                   height: 12),
                        //               Positioned(
                        //                 bottom: -20,
                        //                 left: 5.5,
                        //                 child: Row(
                        //                   mainAxisAlignment:
                        //                       MainAxisAlignment.center,
                        //                   crossAxisAlignment:
                        //                       CrossAxisAlignment.center,
                        //                   children: [
                        //                     Image.asset(
                        //                         'assets/fg_images/next_step_icon_finish_flag2.png',
                        //                         width: 18,
                        //                         height: 21),
                        //                     Container(
                        //                       margin: EdgeInsets.only(
                        //                           left: 10, top: 8),
                        //                       child: Text(
                        //                         '',
                        //                         style: TextStyle(
                        //                           color: Hexcolor('#1E2E45'),
                        //                           fontSize: 12,
                        //                           fontFamily: 'Arial',
                        //                           fontWeight: FontWeight.w400,
                        //                         ),
                        //                       ),
                        //                     )
                        //                   ],
                        //                 ),
                        //               )
                        //             ],
                        //           ),
                        //           // Container(
                        //           //   height: 1.5,
                        //           //   width: 25,
                        //           //   decoration:
                        //           //       BoxDecoration(color: Hexcolor('#1E2E45')),
                        //           // ),
                        //           Container(
                        //             height: 25,
                        //             margin: EdgeInsets.only(
                        //                 // right: 20
                        //                 ),
                        //             child: ListView.builder(
                        //                 shrinkWrap: true,
                        //                 scrollDirection: Axis.horizontal,
                        //                 itemCount: 5,
                        //                 itemBuilder: (context, index) {
                        //                   return Row(
                        //                     children: [
                        //                       Container(
                        //                         height: 1.5,
                        //                         width: 25,
                        //                         decoration: BoxDecoration(
                        //                             color: Hexcolor('#1E2E45')),
                        //                       ),
                        //                       Image.asset(
                        //                           'assets/fg_images/next_step_icon_notdone_step.png',
                        //                           width: 12,
                        //                           height: 12),
                        //                     ],
                        //                   );
                        //                 }),
                        //           ),
                        //           Container(
                        //             margin:
                        //                 EdgeInsets.only(bottom: 11, right: 20),
                        //             child: Image.asset(
                        //               'assets/fg_images/turn_icon2.png',
                        //               width: 25,
                        //             ),
                        //           ),
                        //           SizedBox(
                        //             width: 35,
                        //           )
                        //         ],
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        Container(
                          margin: EdgeInsets.only(
                              // top: 10,
                              ),
                          child: Text(
                            'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                            style: TextStyle(
                              color: Hexcolor('#747474'),
                              fontSize: 16,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Перейти на наступний крок",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () async {
                              Navigator.pushNamed(
                                  context, '/missions_12_12_trees.dart');
                              final SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              // sharedPreferences.clear();
                              var userEmail = sharedPreferences.get("email");
                              if (userEmail != '' && userEmail != null) {
                                sharedPreferences.setBool(
                                    'isStep1Completed', true);
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -130,
                    right: -30,
                    child: Image.asset(
                      'assets/fg_images/error_mission_icon_unlock.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  // Positioned(
                  //   right: 40,
                  //   top: 50,
                  //   child: Container(
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.end,
                  //       crossAxisAlignment: CrossAxisAlignment.end,
                  //       children: [
                  //         Text(
                  //           '1',
                  //           style: TextStyle(
                  //             fontSize: 36,
                  //             color: Hexcolor('#1E2E45'),
                  //             fontFamily: 'Arial',
                  //             fontWeight: FontWeight.w700,
                  //           ),
                  //         ),
                  //         Container(
                  //           margin: EdgeInsets.only(bottom: 2),
                  //           child: Text(
                  //             ' ' + '/ ' + '12',
                  //             style: TextStyle(
                  //               fontSize: 24,
                  //               color: Hexcolor('#1E2E45'),
                  //               fontFamily: 'Arial',
                  //               fontWeight: FontWeight.w400,
                  //             ),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // )
                ],
              ),
            );
          });
    }

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#FFFFFF'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                  left: 5,
                  right: 5,
                  bottom: 17,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          // margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Сканування',
                          style: TextStyle(
                              fontSize: 32,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/run_map_quest_pic_gostart.png',
                            width: 22,
                            height: 24,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              '/5_myBottomBar.dart',
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 195,
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(children: [
                    Container(
                      margin: EdgeInsets.only(top: 120, bottom: 80),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/fg_images/main_icon.png',
                            width: 40,
                            height: 34,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Image.asset(
                            'assets/fg_images/6_home_sidebar_title_image.png',
                            width: 160,
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Інформація про об\'єкт',
                            style: TextStyle(
                              fontSize: 18,
                              color: Hexcolor('#545454'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              // Navigator.pushReplacement(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (BuildContext context) =>
                              //             RunMapQuestDesc()));
                            },
                            child: Text(
                              widget.text,
                              style: TextStyle(
                                fontSize: 16,
                                color: Hexcolor('#747474'),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ])),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Продовжити",
                    style: TextStyle(
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  showInfoWindow();
                  // Navigator.pushNamed(context, '/missions_12_quiz2.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
