import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_free_gen_reality/services/services.dart';

import '6_home_missions_json.dart';
import '6_home_submissions_json.dart';
import '6_home_submissions_progress_json.dart';
import '6_home_steps_json.dart';

class MissionMapDatabase extends StatefulWidget {
  @override
  _MissionMapDatabaseState createState() => _MissionMapDatabaseState();
}

class _MissionMapDatabaseState extends State<MissionMapDatabase> {
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  bool checkingSubmission = false;
  checkPointsByButton() {
    startSubmission(
        subMissionId, subMissionPoints, subMissionCurrencyType) async {
      setState(() {
        checkingSubmission = false;
      });
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setInt('subMissionId', subMissionId);
      print(
          '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! SUBMISSION ID = $subMissionId !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');

      sharedPreferences.setDouble('subMissionPoints', subMissionPoints);

      sharedPreferences.setString(
          'subMissionCurrencyType', subMissionCurrencyType);

      if (missionName == "Муромець race") {
        stopWatch();
        updateTimer();
      }

      Navigator.pushNamed(context, '/6_mission_step_main.dart');
    }

    // for (final subMission in subMissions) {
    //   double distanceInMeters3;
    //   var subMissionLatitude = double.parse(subMission.location.latitude);
    //   var subMissionLongitude = double.parse(subMission.location.longitude);
    //   var subMissionAutoCheckIn = subMission.autoCheckIn;
    //   var subMissionRadius = subMission.activationRadius.toInt();
    //   var subMissionPoints = subMission.points;
    //   var subMissionCurrencyType = subMission.currencyType;
    //   var subMissionId = subMission.id;
    //   var subMissionComleted;

    //   distanceInMeters3 = Geolocator.distanceBetween(currentLocation.latitude,
    //       currentLocation.longitude, subMissionLatitude, subMissionLongitude);

    //   for (int i = 0; i < subMissionsProgresses.length; i++) {
    //     SubMissionProgresses subMission = subMissionsProgresses[i];
    //     if (subMissionId == subMission.submissionId &&
    //         subMission.trackStatus == 'FINISHED') {
    //       subMissionComleted = true;
    //     }
    //   }
    // v2
    // if (subMissionAutoCheckIn == false) {
    //   if (distanceInMeters3 < subMissionRadius) {
    //     if (subMissionComleted == false) {
    //       startSubmission(
    //           subMissionId, subMissionPoints, subMissionCurrencyType);
    //     } else {
    //       showWindowSubMissionDone();
    //     }
    //   } else {
    //     showInfoWindowCheckFalse();
    //   }
    // }

    // v3

    // if (subMissionAutoCheckIn == false &&
    //      &&
    //     subMissionComleted == false) {
    //   startSubmission(subMissionId, subMissionPoints, subMissionCurrencyType);

    // }
    // if(subMissionComleted == true){
    //   showWindowSubMissionDone();

    // }
    // if(distanceInMeters3 > subMissionRadius){
    //   showInfoWindowCheckFalse();

    // }
    // }
    // v1
    for (int i = 0; i < subMissions.length; i++) {
      Submission subMission = subMissions[i];
      double distanceInMeters3;
      var subMissionLatitude = double.parse(subMission.location.latitude);
      var subMissionLongitude = double.parse(subMission.location.longitude);
      var subMissionAutoCheckIn = subMission.autoCheckIn;
      var subMissionRadius = subMission.activationRadius.toInt();
      var subMissionPoints = subMission.points;
      var subMissionCurrencyType = subMission.currencyType;
      var subMissionId = subMission.id;
      var subMissionComleted;

      checkSubmissionProgress() async {
        for (int i = 0; i < subMissionsProgresses.length; i++) {
          SubMissionProgresses subMission = subMissionsProgresses[i];
          if (subMissionId == subMission.submissionId &&
              subMission.trackStatus == 'FINISHED') {
            subMissionComleted = true;
          }
        }
      }

      distanceInMeters3 = Geolocator.distanceBetween(currentLocation.latitude,
          currentLocation.longitude, subMissionLatitude, subMissionLongitude);

      print(distanceInMeters3);
      print(subMissionRadius);

      if (subMissionAutoCheckIn == false) {
        if (distanceInMeters3 < subMissionRadius) {
          print('CHECK DISTANCE');
          checkSubmissionProgress();

          if (subMissionComleted == true) {
            showWindowSubMissionDone();
          } else {
            startSubmission(
                subMissionId, subMissionPoints, subMissionCurrencyType);
          }
        } else {
          setState(() {
            checkingSubmission = false;
          });
          print('Chekin false');
        }
      }
    }
  }

  Stopwatch watch = Stopwatch();
  Timer timer;
  String elapsedTime = '';
  int elapsedtimeFromDarabase = 0;

  getUserData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    setState(() {
      userEmail = email;
    });
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
        Map<String, dynamic> responseJson = jsonDecode(response.body);

        email = responseJson["email"];
        setState(() {
          if (responseJson["timer"] != null) {
            elapsedtimeFromDarabase = responseJson["timer"];
          }
        });
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          setState(() {
            isLoading = false;
          });
          print('GET TIMER TIME FROM DATABASE');
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  Future<User> updateTimer() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    String url = 'http://generation-admin.ehub.com.ua/api/user/update';
    print(id);
    print(url);
    print(token);

    final http.Response response = await http.put(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "id": id,
        "timer": watch.elapsedMilliseconds + elapsedtimeFromDarabase,
      }),
    );
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      print('Timer updated');
      setState(() {
        isLoading = false;
      });
      // print(response.body);
    } else {
      setState(() {
        isLoading = false;
      });
      throw Exception('Failed to update timer.');
    }
  }

  updateTime(Timer timer) {
    if (watch.isRunning) {
      setState(() {
        elapsedTime = transformMilliSeconds(
            watch.elapsedMilliseconds + elapsedtimeFromDarabase);
      });
    }
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return "$hoursStr:$minutesStr:$secondsStr";
  }

  startWatch() {
    setState(() {
      watch.start();
      timer = Timer.periodic(Duration(milliseconds: 100), updateTime);
    });
  }

  stopWatch() {
    setState(() {
      watch.stop();
      print('TIREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEER');
      print(watch.elapsedMilliseconds);
    });
  }

  double distanceInMeters;

  int distanceInMeters1;
  int distanceInMeters2;
  int distanceInMeters3;
  int distanceInMeters4;
  int distanceInMeters5;
  int distanceInMeters6;
  int distanceInMeters7;
  int distanceInMeters8;
  int distanceInMeters9;
  int distanceInMeters10;
  int distanceInMeters11;
  int distanceInMeters12;
  int distanceInMeters13;
  int distanceInMeters14;

  var currentLocation;
  bool mapToggle = false;
  double zoomVal = 17.0;

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _getMyLocation(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  BitmapDescriptor checkPoint1;
  BitmapDescriptor checkPoint2;
  BitmapDescriptor checkPoint3;
  Set<Marker> markers = HashSet<Marker>();

  Map<PolylineId, Polyline> polylines = {};

  List<LatLng> polylineCoordinates = [];
  List<LatLng> polylineCoordinates2 = [];
  List<LatLng> polylineCoordinates3 = [];
  List<LatLng> polylineCoordinates4 = [];
  List<LatLng> polylineCoordinates5 = [];
  List<LatLng> polylineCoordinates6 = [];
  List<LatLng> polylineCoordinates7 = [];
  List<LatLng> polylineCoordinates8 = [];
  List<LatLng> polylineCoordinates9 = [];
  List<LatLng> polylineCoordinates10 = [];
  List<LatLng> polylineCoordinates11 = [];
  List<LatLng> polylineCoordinates12 = [];
  List<LatLng> polylineCoordinates13 = [];
  List<LatLng> polylineCoordinates14 = [];

  PolylinePoints polylinePoints = PolylinePoints();
  PolylinePoints polylinePoints2 = PolylinePoints();
  PolylinePoints polylinePoints3 = PolylinePoints();
  PolylinePoints polylinePoints4 = PolylinePoints();
  PolylinePoints polylinePoints5 = PolylinePoints();
  PolylinePoints polylinePoints6 = PolylinePoints();
  PolylinePoints polylinePoints7 = PolylinePoints();
  PolylinePoints polylinePoints8 = PolylinePoints();
  PolylinePoints polylinePoints9 = PolylinePoints();
  PolylinePoints polylinePoints10 = PolylinePoints();
  PolylinePoints polylinePoints11 = PolylinePoints();
  PolylinePoints polylinePoints12 = PolylinePoints();
  PolylinePoints polylinePoints13 = PolylinePoints();
  PolylinePoints polylinePoints14 = PolylinePoints();

  Geolocator geolocator = Geolocator();

  bool isLoading = false;
  bool deleteFirstPolyline = false;
  var userEmail;

  bool isStep1Completed = false;
  bool isStep2Completed = false;
  bool isStep3Completed = false;
  bool isStep4Completed = false;
  bool isStep5Completed = false;
  bool isStep6Completed = false;
  bool isStep7Completed = false;
  bool isStep8Completed = false;
  bool isStep9Completed = false;
  bool isStep10Completed = false;
  bool isStep11Completed = false;
  bool isStep12Completed = false;
  bool isStep13Completed = false;
  bool isStep14Completed = false;

  showWindowQuizDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдене',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви не можете повторно пройти це завдання',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showWindowSubMissionDone() {
    setState(() {
      checkingSubmission = false;
    });
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Завдання пройдене',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви не можете повторно пройти це завдання',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  StreamSubscription _getPositionSubscription;
  _getLocation() {
    startSubmission(
        subMissionId, subMissionPoints, subMissionCurrencyType) async {
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setInt('subMissionId', subMissionId);

      sharedPreferences.setDouble('subMissionPoints', subMissionPoints);

      sharedPreferences.setString(
          'subMissionCurrencyType', subMissionCurrencyType);

      if (missionName == "Муромець race") {
        stopWatch();
        updateTimer();
      }

      Navigator.pushNamed(context, '/6_mission_step_main.dart');
    }

    _getPositionSubscription = Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.bestForNavigation,
            timeInterval: 5000)
        .listen((currentLocation) {
      for (int i = 0; i < subMissions.length; i++) {
        Submission subMission = subMissions[i];
        double distanceInMeters2;
        var subMissionLatitude = double.parse(subMission.location.latitude);
        var subMissionLongitude = double.parse(subMission.location.longitude);
        var subMissionAutoCheckIn = subMission.autoCheckIn;
        var subMissionRadius = subMission.activationRadius.toInt();
        var subMissionPoints = subMission.points;
        var subMissionCurrencyType = subMission.currencyType;
        var subMissionId = subMission.id;
        var subMissionComleted;

        checkSubmissionProgress() async {
          for (int i = 0; i < subMissionsProgresses.length; i++) {
            SubMissionProgresses subMission = subMissionsProgresses[i];
            if (subMissionId == subMission.submissionId &&
                subMission.trackStatus == 'FINISHED') {
              subMissionComleted = true;
            }
          }
        }

        distanceInMeters2 = Geolocator.distanceBetween(currentLocation.latitude,
            currentLocation.longitude, subMissionLatitude, subMissionLongitude);

        // print(distanceInMeters2);

        if (subMissionAutoCheckIn == true) {
          if (distanceInMeters2 < subMissionRadius) {
            checkSubmissionProgress();
            if (subMissionComleted == true) {
              _getPositionSubscription.cancel();
            } else {
              _getPositionSubscription.cancel();
              startSubmission(
                  subMissionId, subMissionPoints, subMissionCurrencyType);
            }
          }
        }
      }
    });
  }

  var missionId;

  String missionName = '';
  double missionLatitude;
  double missionLongitude;
  double missionPoints;
  Future<Mission> getMissionData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var missionId = sharedPreferences.get('missionId');
    String token = sharedPreferences.get("token");

    String url =
        'http://generation-admin.ehub.com.ua/api/mission/' + '$missionId';
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          missionId = responseJson["id"];
          missionName = utf8.decode(responseJson["titleUa"].runes.toList());
          missionLatitude = double.parse(responseJson["location"]["latitude"]);
          missionLongitude =
              double.parse(responseJson["location"]["longitude"]);
          missionPoints = responseJson["points"];
          isLoading = false;
        });
        print('GET MISSION DATA');
      } else {
        print('error');
      }
    }

    getMission();
    return Mission();
  }

  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  List<SubMissionProgresses> subMissionsProgresses;
  List<Submission> subMissions;
  List<Stup> steps;
  bool loadingL = true;

  setMissionDone() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var usedId = sharedPreferences.get('id');
    var missionId = sharedPreferences.get('missionId');
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/mission/progress/isFinished?mission_id=$missionId&user_id=$usedId';
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      stopWatch();
      if (missionPoints != 0 && missionPoints != null) {
        Navigator.pushNamed(context, '/6_mission_quest_done.dart');
      } else {
        Navigator.pushNamed(context, '/5_myBottomBar.dart');
      }
    } else {
      print('error');
    }
  }

  bool missionContainsManualChekin = false;

  List<int> initializedSubmissions = new List<int>();

  checkAllSubmissions() {
    // chekin progress submissions
    List<String> progressResult = new List<String>();
    for (final submission in subMissions) {
      for (final subMissionsProgress in subMissionsProgresses) {
        if (subMissionsProgress.submissionId == submission.id) {
          if (subMissionsProgress.trackStatus == 'FINISHED') {
            setState(() {
              progressResult.add('FINISHED');
            });
          } else {
            setState(() {
              progressResult.add('INITIALIZED');
            });
          }
          if (subMissionsProgress.trackStatus == 'INITIALIZED') {
            initializedSubmissions.add(subMissionsProgress.submissionId);
          }
        }
      }
    }

    print(
        '************************************ ADD LIST OF INITIALIZED SUBMISSIONS *****************************************');

    print('GET OUR SUBMISSIONS PROGRESSES');
    print(progressResult);
    if (progressResult.contains('FINISHED')) {
      deleteFirstPolyline = true;
      if (missionName == "Муромець race") {
        startWatch();
      }
    }
    if (progressResult.contains('INITIALIZED')) {
      print('EXIST NOT DONE SUBMISSIONS - GO TO THE MAP');
    } else {
      print('NOT EXIST NOT DONE SUBMISSIONS - GO TO THE END');
      setMissionDone();
    }

    // chekin autoCheckIn submissions
    List<bool> chekinResult = new List<bool>();
    for (final submission in subMissions) {
      chekinResult.add(submission.autoCheckIn);
    }

    if (chekinResult.contains(false)) {
      setState(() {
        missionContainsManualChekin = true;
      });
    }
  }

  setCheckPoint1() async {
    checkPoint1 = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
    print('marker color1 is added');
  }

  setCheckPoint2() async {
    checkPoint2 = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
    print('marker color2 is added');
  }

  setCheckPoint3() async {
    checkPoint3 = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/fg_images/map_next_point.png');
    print('marker color3 is added');
  }

  @override
  void initState() {
    setCheckPoint1();
    setCheckPoint2();
    setCheckPoint3();
    getUserData();
    getMissionData();

    ServicesSubMissions.getSubMissionData().then((list) {
      setState(() {
        subMissions = list.submissions;
        print(
            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        print(subMissions.length);
        ServicesSubmissionProgresses.getProressSubmissions().then((list) {
          setState(() {
            subMissionsProgresses = list;
            print(
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            print(subMissionsProgresses.length);
            checkAllSubmissions();
            loadingL = false;
          });
          Geolocator.getCurrentPosition(
                  desiredAccuracy: LocationAccuracy.bestForNavigation)
              .then((currloc) {
            setState(() {
              currentLocation = currloc;
              mapToggle = true;
            });
          });
        });
      });
    });

    _getLocation(); // LOCATOR

    super.initState();
  }

  @override
  void dispose() {
    _getPositionSubscription.cancel();
    super.dispose();
  }

  showInfoWindowCheckTrue() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я на місці',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showInfoWindowCheckFalse() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Ти не на місці',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Спробуй підійти до точки ближче\nі ще раз зачекінитись',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/6_mission_map.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<User> changeTus(double tus, String operation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": "TUS",
        "operation": operation,
        "amount": tus,
        "description": "withdraw 5 tus"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');
    if (response.statusCode == 200) {
      print(response.body);
      print('-5TUS done');
    } else {
      throw Exception('Failed to update User.');
    }
  }

  showWindowPointDone() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Точка вже пройдена',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви вже проходили цю точку раніше',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, '/mission_muromec_map.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  // Set<Circle> circles = Set.from([
  //   Circle(
  //     circleId: CircleId("1"),
  //     center: LatLng(50.49676, 30.54246),
  //     radius: 15,
  //     strokeWidth: 2,
  //     strokeColor: Hexcolor('#FE6802'),
  //   ),
  // ]);

  Set<Circle> circles = HashSet<Circle>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              child: Stack(overflow: Overflow.visible, children: [
                Container(
                  margin: EdgeInsets.only(top: 89),
                  child: mapToggle
                      ? GoogleMap(
                          markers: markers,
                          circles: circles,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(currentLocation.latitude,
                                  currentLocation.longitude),
                              zoom: 17.0),
                          mapType: MapType.normal,
                          tiltGesturesEnabled: true,
                          compassEnabled: true,
                          scrollGesturesEnabled: true,
                          zoomGesturesEnabled: true,
                          zoomControlsEnabled: false, // off default buttons
                          myLocationEnabled: true,
                          myLocationButtonEnabled:
                              false, // off default my location
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);

                            _addPolyLine() {
                              PolylineId id = PolylineId("meToStart");
                              Polyline polyline = Polyline(
                                  patterns: <PatternItem>[
                                    PatternItem.dot,
                                    PatternItem.gap(50)
                                  ],
                                  polylineId: id,
                                  color: Hexcolor('#FE6802'),
                                  points: polylineCoordinates,
                                  width: 10);
                              polylines[id] = polyline;
                              setState(() {});
                            }

                            _getPolyline() async {
                              PolylineResult result = await polylinePoints
                                  .getRouteBetweenCoordinates(
                                "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
                                PointLatLng(currentLocation.latitude,
                                    currentLocation.longitude),
                                PointLatLng(
                                    double.parse(
                                        subMissions[0].location.latitude),
                                    double.parse(
                                        subMissions[0].location.longitude)),
                                travelMode: TravelMode.walking,
                                // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
                              );
                              if (result.points.isNotEmpty) {
                                result.points.forEach((PointLatLng point0) {
                                  polylineCoordinates.add(LatLng(
                                      point0.latitude, point0.longitude));
                                });
                              }
                              _addPolyLine();
                            }

                            if (deleteFirstPolyline == false) {
                              _getPolyline();
                            }

                            setState(() {
                              for (int i = 0; i < subMissions.length; i++) {
                                Submission subMission = subMissions[i];
                                var subMissionName = utf8
                                    .decode(subMission.titleUa.runes.toList());
                                var subMissionId = subMission.id;
                                var subMissionLatitude =
                                    double.parse(subMission.location.latitude);
                                var subMissionLongitude =
                                    double.parse(subMission.location.longitude);
                                var subMissionAutoCheckIn =
                                    subMission.autoCheckIn;
                                var subMissionRadius =
                                    subMission.activationRadius.toDouble();
                                var subMissionPoints = subMission.points;
                                var subMissionCurrencyType =
                                    subMission.currencyType;
                                var subMissionComleted;

                                getSubmissionProgressId() async {
                                  for (int i = 0;
                                      i < subMissionsProgresses.length;
                                      i++) {
                                    SubMissionProgresses subMission =
                                        subMissionsProgresses[i];
                                    if (subMissionId ==
                                            subMission.submissionId &&
                                        subMission.trackStatus == 'FINISHED') {
                                      subMissionComleted = true;
                                    }
                                  }
                                }

                                getSubmissionProgressId();

                                showWindowSubMissionDone() {
                                  showDialog(
                                      barrierDismissible: false,
                                      barrierColor:
                                          Hexcolor('#341F5E').withOpacity(0.8),
                                      context: context,
                                      builder: (context) {
                                        return Dialog(
                                          insetPadding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Stack(
                                            overflow: Overflow.visible,
                                            children: [
                                              Container(
                                                height: 250,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 20,
                                                      ),
                                                      child: Text(
                                                        'Завдання пройдене',
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 24,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      'Ви не можете повторно пройти це завдання',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#747474'),
                                                        fontSize: 16,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.4,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                        bottom: 20,
                                                      ),
                                                      height: 60,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              80,
                                                      child: RaisedButton(
                                                        child: Text(
                                                          "Закрити",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 17,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              letterSpacing:
                                                                  1.05),
                                                        ),
                                                        color:
                                                            Hexcolor('#FE6802'),
                                                        shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    14.0)),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      });
                                }

                                markers.add(Marker(
                                  infoWindow: InfoWindow(title: subMissionName),
                                  anchor: const Offset(0.5, 0.5),
                                  markerId: MarkerId("$subMissionId"),
                                  position: LatLng(
                                      subMissionLatitude, subMissionLongitude),
                                  icon:
                                      // subMissionComleted == true
                                      //      ? checkPoint2
                                      //      : checkPoint1,
                                      subMissionComleted == true
                                          ? checkPoint2
                                          : subMissionId ==
                                                  initializedSubmissions[0]
                                              ? checkPoint3
                                              : checkPoint1,
                                  onTap: () async {
                                    Geolocator.getCurrentPosition(
                                            desiredAccuracy: LocationAccuracy
                                                .bestForNavigation)
                                        .then((currloc) {
                                      setState(() {
                                        currentLocation = currloc;
                                      });
                                    });
                                    if (subMissionComleted == true) {
                                      showWindowSubMissionDone();
                                    } else if (subMissionAutoCheckIn == false) {
                                      distanceInMeters =
                                          (Geolocator.distanceBetween(
                                              currentLocation.latitude,
                                              currentLocation.longitude,
                                              subMissionLatitude,
                                              subMissionLongitude));
                                      if (distanceInMeters < subMissionRadius) {
                                        final SharedPreferences
                                            sharedPreferences =
                                            await SharedPreferences
                                                .getInstance();

                                        sharedPreferences.setInt(
                                            'subMissionId', subMissionId);

                                        sharedPreferences.setDouble(
                                            'subMissionPoints',
                                            subMissionPoints);

                                        sharedPreferences.setString(
                                            'subMissionCurrencyType',
                                            subMissionCurrencyType);

                                        if (missionName == "Муромець race") {
                                          stopWatch();
                                          updateTimer();
                                        }

                                        Navigator.pushNamed(context,
                                            '/6_mission_step_main.dart');
                                      } else {
                                        showInfoWindowCheckFalse();
                                      }
                                    }
                                  },
                                ));
                                circles.add(Circle(
                                  center: LatLng(
                                      subMissionLatitude, subMissionLongitude),
                                  circleId: CircleId("$subMissionId"),
                                  radius: subMissionRadius,
                                  strokeWidth: 1,
                                  strokeColor: Hexcolor('#028AFE'),
                                  fillColor:
                                      Hexcolor('#028AFE').withOpacity(0.25),
                                ));
                              }

                              print("markers are added");
                            });
                          },
                          polylines: Set<Polyline>.of(polylines.values),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Hexcolor('#7D5AC2'),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              Hexcolor('#FE6802'),
                            ),
                          ),
                        ),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 40,
                        // left: 15,
                        // right: 15,
                        bottom: 12,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 5, right: 5),
                                child: SizedBox(
                                  child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/6_home_search_back.png',
                                      width: 10,
                                      height: 19,
                                    ),
                                    onPressed: () {
                                      _getPositionSubscription.cancel();
                                      Navigator.pushNamed(
                                          context, '/6_mission_desc.dart');
                                    },
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  // startWatch();
                                },
                                child: Text(
                                  missionName,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w900),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 5),
                            child: SizedBox(
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/run_map_quest_pic_gostart.png',
                                  width: 22,
                                  height: 24,
                                ),
                                onPressed: () {
                                  // stopWatch();
                                  _getPositionSubscription.cancel();
                                  Navigator.pushNamed(
                                      context, '/5_myBottomBar.dart');
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 140,
                  right: 20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            if (missionName == "Муромець race") {
                              stopWatch();
                              updateTimer();
                            }
                            Navigator.pushNamedAndRemoveUntil(
                                context,
                                "/6_mission_map.dart",
                                (r) => false); // EXPERIMENT
                            // Navigator.pushNamed(context, '/6_mission_map.dart');
                          },
                          child: Image.asset(
                            'assets/fg_images/run_map_quest_pic_update.png',
                            width: 50,
                            height: 50,
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                          onTap: () {
                            zoomVal++;
                            _plus(zoomVal);
                          },
                          child: Image.asset(
                            'assets/fg_images/7_map_icon_zoom_plus.png',
                            width: 50,
                            height: 50,
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          zoomVal--;
                          _minus(zoomVal);
                        },
                        child: Image.asset(
                          'assets/fg_images/7_map_icon_zoom_minus.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          _getMyLocation(zoomVal);
                        },
                        child: Image.asset(
                          'assets/fg_images/7_map_icon_location.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                    ],
                  ),
                ),
                loadingL
                    ? Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Hexcolor('#7D5AC2'),
                          valueColor: new AlwaysStoppedAnimation<Color>(
                            Hexcolor('#FE6802'),
                          ),
                        ),
                      )
                    : Positioned(
                        // submissions from APP
                        bottom: missionContainsManualChekin ? 100 : 20,
                        child: Container(
                          margin: EdgeInsets.only(left: 5, right: 5),
                          height: 117,
                          width: MediaQuery.of(context).size.width,
                          child: ListView.builder(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemCount: subMissions.length,
                            itemBuilder: (context, index) {
                              Submission subMission = subMissions[index];

                              var subMissionComleted;
                              var subMissionId = subMission.id;

                              getSubmissionProgressId() async {
                                for (int i = 0;
                                    i < subMissionsProgresses.length;
                                    i++) {
                                  SubMissionProgresses subMission =
                                      subMissionsProgresses[i];
                                  if (subMissionId == subMission.submissionId &&
                                      subMission.trackStatus == 'FINISHED') {
                                    subMissionComleted = true;
                                  }
                                }
                              }

                              var subMissionName = utf8
                                  .decode(subMission.titleUa.runes.toList());
                              var subMissionLatitude =
                                  double.parse(subMission.location.latitude);
                              var subMissionLongitude =
                                  double.parse(subMission.location.longitude);
                              var subMissionImage = subMission.imageUrl;

                              getSubmissionProgressId();

                              return Stack(
                                overflow: Overflow.visible,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      _goToCheckPoint(subMissionLatitude,
                                          subMissionLongitude);
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(left: 5, right: 5),
                                      height: 117,
                                      width: 174,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                          image: DecorationImage(
                                            image: NetworkImage(subMission
                                                            .imageUrl ==
                                                        "" ||
                                                    subMission.imageUrl == null
                                                ? '$link' + 'default.png'
                                                : '$link' + subMissionImage),
                                            fit: BoxFit.cover,
                                          )),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.black.withOpacity(0.5),
                                              borderRadius: BorderRadius.only(
                                                  bottomRight:
                                                      Radius.circular(10.0),
                                                  bottomLeft:
                                                      Radius.circular(10.0)),
                                            ),
                                            height: 33,
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                left: 5,
                                                bottom: 5,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    width: 164,
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      subMissionName,
                                                      overflow:
                                                          TextOverflow.fade,
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 13,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        letterSpacing: 1.09,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  subMissionComleted == true
                                      ? Positioned(
                                          top: 12.5,
                                          left: 15,
                                          child: Image.asset(
                                            'assets/fg_images/6_home_logo_mission_completed2.png',
                                            height: 30,
                                            width: 100,
                                          ))
                                      : Container()
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                missionContainsManualChekin
                    ? checkingSubmission
                        ? Positioned(
                            bottom: 35,
                            left:
                                (MediaQuery.of(context).size.width - 17.5) / 2,
                            child: Center(
                              child: SizedBox(
                                width: 30,
                                height: 30,
                                child: CircularProgressIndicator(
                                  backgroundColor: Hexcolor('#7D5AC2'),
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                    Hexcolor('#FE6802'),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Positioned(
                            left: 20,
                            bottom: 0,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20),
                              height: 60,
                              width: MediaQuery.of(context).size.width - 40,
                              child: RaisedButton(
                                child: Text(
                                  "Перевірити точку",
                                  style: TextStyle(
                                    color: Hexcolor('#FFFFFF'),
                                    fontSize: 17,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                color: Hexcolor('#FE6802'),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(14.0)),
                                onPressed: () {
                                  setState(() {
                                    checkingSubmission = true;
                                  });
                                  Geolocator.getCurrentPosition(
                                          desiredAccuracy: LocationAccuracy
                                              .bestForNavigation)
                                      .then((currloc) {
                                    setState(() {
                                      currentLocation = currloc;
                                      print('LOCATION');
                                    });
                                    checkPointsByButton();
                                  });
                                },
                              ),
                            ),
                          )
                    : Container(),
                isLoading
                    ? Container()
                    : Positioned(
                        top: 140,
                        left: 20,
                        child: Text(elapsedTime,
                            style: TextStyle(
                              fontSize: 25.0,
                              color: Hexcolor('#FE6802'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            )),
                      ),
              ]),
            ),
    );
  }

  Future<void> _goToCheckPoint(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 19)));
  }

  Future<void> centerScreen(Position currentLocation) async {
    setState(() {});
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 17.0),
    ));
  }
}
