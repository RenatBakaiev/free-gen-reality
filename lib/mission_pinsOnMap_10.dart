// ----------------------Mission pinsOnMap 10 points 25.11.20------------------------------------
import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/geolocator_service.dart';
import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';

class MissionPinsOnMap10 extends StatefulWidget {
  @override
  _MissionPinsOnMap10State createState() => _MissionPinsOnMap10State();
}

class _MissionPinsOnMap10State extends State<MissionPinsOnMap10> {
  // final GeolocatorService geoService = GeolocatorService();
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  var currentLocation;
  bool mapToggle = false;
  double zoomVal = 17.0;

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _getMyLocation(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  BitmapDescriptor _checkPoint1;
  BitmapDescriptor _checkPoint2;
  BitmapDescriptor _checkPoint3;
  BitmapDescriptor _checkPoint4;
  BitmapDescriptor _checkPoint5;
  BitmapDescriptor _checkPoint6;
  BitmapDescriptor _checkPoint7;
  BitmapDescriptor _checkPoint8;
  BitmapDescriptor _checkPoint9;
  BitmapDescriptor _checkPoint10;
  Set<Marker> _markers = HashSet<Marker>();

  double _destLatitude = 50.448689, _destLongitude = 30.513945;
  double _destLatitude2 = 50.446598, _destLongitude2 = 30.513167;
  double _destLatitude3 = 50.450856, _destLongitude3 = 30.512524;
  double _destLatitude4 = 50.454661, _destLongitude4 = 30.506274;
  double _destLatitude5 = 50.447151, _destLongitude5 = 30.512488;
  double _destLatitude6 = 50.449857, _destLongitude6 = 30.506319;
  double _destLatitude7 = 50.451059, _destLongitude7 = 30.513963;
  double _destLatitude8 = 50.453115, _destLongitude8 = 30.514527;
  double _destLatitude9 = 50.447473, _destLongitude9 = 30.517918;
  double _destLatitude10 = 50.447486, _destLongitude10 = 30.507650;

  Map<PolylineId, Polyline> polylines = {};

  @override
  void initState() {
    Geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        mapToggle = true;
      });
    });

    super.initState();
    _setCheckPoint1();
    _setCheckPoint2();
    _setCheckPoint3();
    _setCheckPoint4();
    _setCheckPoint5();
    _setCheckPoint6();
    _setCheckPoint7();
    _setCheckPoint8();
    _setCheckPoint9();
    _setCheckPoint10();
    // _setMe();
  }

  void _setCheckPoint1() async {
    _checkPoint1 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_done.png',
    );
  }

  void _setCheckPoint2() async {
    _checkPoint2 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_2.png',
    );
  }

  void _setCheckPoint3() async {
    _checkPoint3 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_3.png',
    );
  }

  void _setCheckPoint4() async {
    _checkPoint4 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_4.png',
    );
  }

  void _setCheckPoint5() async {
    _checkPoint5 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_5.png',
    );
  }

    void _setCheckPoint6() async {
    _checkPoint6 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_6.png',
    );
  }

    void _setCheckPoint7() async {
    _checkPoint7 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_7.png',
    );
  }

    void _setCheckPoint8() async {
    _checkPoint8 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_8.png',
    );
  }

    void _setCheckPoint9() async {
    _checkPoint9 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_9.png',
    );
  }

    void _setCheckPoint10() async {
    _checkPoint10 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_10.png',
    );
  }

  showInfoWindowCheckTrue() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я на месте',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрыть",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showInfoWindowCheckFalse() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я не на месте',
                          style: TextStyle(
                            color: Hexcolor('#FF1E1E'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрыть",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(overflow: Overflow.visible, children: [
          Container(
            margin: EdgeInsets.only(top: 109),
            child: mapToggle
                ? GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: LatLng(currentLocation.latitude,
                            currentLocation.longitude),
                        zoom: 17.0),
                    mapType: MapType.normal,
                    tiltGesturesEnabled: true,
                    compassEnabled: true,
                    scrollGesturesEnabled: true,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: false, // off default buttons
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false, // off default my location
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);

                      // _getLocation() {
                      //   print("locationLatitude: ${currentLocation.latitude}");
                      //   print(
                      //       "locationLongitude: ${currentLocation.longitude}");
                      // }

                      setState(() {
                        _markers.add(Marker(
                          markerId: MarkerId("1"),
                          position: LatLng(_destLatitude, _destLongitude),
                          icon: _checkPoint1,
                          onTap: () {
                            Navigator.pushNamed(context, '/quest_done.dart');
                          },
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("2"),
                          position: LatLng(_destLatitude2, _destLongitude2),
                          icon: _checkPoint2,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("3"),
                          position: LatLng(_destLatitude3, _destLongitude3),
                          icon: _checkPoint3,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("4"),
                          position: LatLng(_destLatitude4, _destLongitude4),
                          icon: _checkPoint4,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("5"),
                          position: LatLng(_destLatitude5, _destLongitude5),
                          icon: _checkPoint5,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("6"),
                          position: LatLng(_destLatitude6, _destLongitude6),
                          icon: _checkPoint6,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("7"),
                          position: LatLng(_destLatitude7, _destLongitude7),
                          icon: _checkPoint7,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("8"),
                          position: LatLng(_destLatitude8, _destLongitude8),
                          icon: _checkPoint8,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("9"),
                          position: LatLng(_destLatitude9, _destLongitude9),
                          icon: _checkPoint9,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("10"),
                          position: LatLng(_destLatitude10, _destLongitude10),
                          icon: _checkPoint10,
                        ));

                      });
                    },
                    polylines: Set<Polyline>.of(polylines.values),
                    markers: _markers,
                  )
                : Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Hexcolor('#7D5AC2'),
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        Hexcolor('#FE6802'),
                      ),
                    ),
                  ),
          ),
          Positioned(
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                  // left: 15,
                  // right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                // Navigator.pushNamed(
                                //     context, '/desc_pinsOnMap.dart');
                                Navigator.of(context).pop();
                                // Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Метки на карте',
                          style: TextStyle(
                              fontSize: 33,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/run_map_quest_pic_gostart.png',
                            width: 22,
                            height: 24,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                            // Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 140,
            right: 20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/mission_pinsOnMap_10.dart');
                    },
                    child: Image.asset(
                      'assets/fg_images/run_map_quest_pic_update.png',
                      width: 50,
                      height: 50,
                    )),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                    onTap: () {
                      zoomVal++;
                      _plus(zoomVal);
                    },
                    child: Image.asset(
                      'assets/fg_images/7_map_icon_zoom_plus.png',
                      width: 50,
                      height: 50,
                    )),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    zoomVal--;
                    _minus(zoomVal);
                  },
                  child: Image.asset(
                    'assets/fg_images/7_map_icon_zoom_minus.png',
                    width: 50,
                    height: 50,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    _getMyLocation(zoomVal);
                  },
                  child: Image.asset(
                    'assets/fg_images/7_map_icon_location.png',
                    width: 50,
                    height: 50,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 100,
            child: Container(
              margin: EdgeInsets.only(left: 5, right: 5),
              height: 117,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: checkPoins10.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      _goToCheckPoint(
                          checkPoins10[index].lat, checkPoins10[index].long);
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 5, right: 5),
                      height: 117,
                      width: 174,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                            image: AssetImage(checkPoins10[index].image),
                            fit: BoxFit.cover,
                          )),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: checkPoins10[index].color.withOpacity(0.5),
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0)),
                            ),
                            height: 33,
                            child: Container(
                              margin: EdgeInsets.only(
                                left: 5,
                                bottom: 5,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(
                                      checkPoins10[index].name,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                        letterSpacing: 1.09,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Positioned(
            left: 20,
            bottom: 0,
            child: Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Проверить чекпоинт",
                  style: TextStyle(
                    color: Hexcolor('#FFFFFF'),
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#59B32D'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  // print(
                  //     "locationLatitude: ${currentLocation.latitude}"); // its works
                  // print("locationLongitude: ${currentLocation.longitude}");
                  // if (currentLocation.latitude ==
                  //         _destLatitude && // 50.4492936, 30.5125368;
                  //     currentLocation.longitude == _destLongitude) {
                  //   print('you are at start');
                  //   showInfoWindowCheckTrue();
                  // } else {
                  //   print('you are not at start');
                  //   showInfoWindowCheckFalse();
                  // }
                },
              ),
            ),
          )
        ]),
      ),
    );
  }

  Future<void> _goToCheckPoint(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 17)));
  }

  Future<void> centerScreen(Position currentLocation) async {
    setState(() {});
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 17.0),
    ));
  }
}
