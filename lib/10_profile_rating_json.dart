import 'dart:convert';

Users usersFromJson(String str) => Users.fromJson(json.decode(str));

String usersToJson(Users data) => json.encode(data.toJson());

class Users {
    Users({
        this.page,
        this.users,
    });

    Page page;
    List<User> users;

    factory Users.fromJson(Map<String, dynamic> json) => Users(
        page: Page.fromJson(json["page"]),
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "page": page.toJson(),
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class User {
    User({
        this.id,
        this.access,
        this.userId,
        this.firstName,
        this.lastName,
        this.fullName,
        this.username,
        this.email,
        this.phone,
        this.country,
        this.city,
        this.birthDate,
        this.gender,
        this.profileImageUrl,
        this.joinDate,
        this.lastLoginDate,
        this.lastLoginDateDisplay,
        this.role,
        this.status,
        this.registrationType,
        this.authorities,
        this.isActive,
        this.isNotLocked,
        this.accounts,
        this.timer,
    });

    int id;
    dynamic access;
    String userId;
    String firstName;
    String lastName;
    dynamic fullName;
    String username;
    String email;
    String phone;
    String country;
    String city;
    DateTime birthDate;
    String gender;
    String profileImageUrl;
    DateTime joinDate;
    DateTime lastLoginDate;
    DateTime lastLoginDateDisplay;
    Role role;
    String status;
    RegistrationType registrationType;
    List<Authority> authorities;
    bool isActive;
    bool isNotLocked;
    List<Account> accounts;
    int timer;

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        access: json["access"],
        userId: json["userId"],
        firstName: json["firstName"] == null ? null : json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        fullName: json["fullName"],
        username: json["username"],
        email: json["email"],
        phone: json["phone"] == null ? null : json["phone"],
        country: json["country"] == null ? null : json["country"],
        city: json["city"] == null ? null : json["city"],
        birthDate: json["birthDate"] == null ? null : DateTime.parse(json["birthDate"]),
        gender: json["gender"] == null ? null : json["gender"],
        profileImageUrl: json["profileImageUrl"] == null ? null : json["profileImageUrl"],
        joinDate: DateTime.parse(json["joinDate"]),
        lastLoginDate: json["lastLoginDate"] == null ? null : DateTime.parse(json["lastLoginDate"]),
        lastLoginDateDisplay: json["lastLoginDateDisplay"] == null ? null : DateTime.parse(json["lastLoginDateDisplay"]),
        role: roleValues.map[json["role"]],
        status: json["status"] == null ? null : json["status"],
        registrationType: registrationTypeValues.map[json["registrationType"]],
        authorities: List<Authority>.from(json["authorities"].map((x) => authorityValues.map[x])),
        isActive: json["isActive"],
        isNotLocked: json["isNotLocked"],
        timer: json["timer"],
        accounts: List<Account>.from(json["accounts"].map((x) => Account.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "access": access,
        "userId": userId,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "fullName": fullName,
        "username": username,
        "email": email,
        "phone": phone == null ? null : phone,
        "country": country == null ? null : country,
        "city": city == null ? null : city,
        "birthDate": birthDate == null ? null : birthDate.toIso8601String(),
        "gender": gender == null ? null : gender,
        "profileImageUrl": profileImageUrl == null ? null : profileImageUrl,
        "joinDate": joinDate.toIso8601String(),
        "lastLoginDate": lastLoginDate == null ? null : lastLoginDate.toIso8601String(),
        "lastLoginDateDisplay": lastLoginDateDisplay == null ? null : lastLoginDateDisplay.toIso8601String(),
        "role": roleValues.reverse[role],
        "status": status == null ? null : status,
        "registrationType": registrationTypeValues.reverse[registrationType],
        "authorities": List<dynamic>.from(authorities.map((x) => authorityValues.reverse[x])),
        "isActive": isActive,
        "isNotLocked": isNotLocked,
        "accounts": List<dynamic>.from(accounts.map((x) => x.toJson())),
        "timer": timer,
    };
}

class Account {
    Account({
        this.id,
        this.currency,
        this.accountBalance,
    });

    int id;
    Currency currency;
    double accountBalance;

    factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json["id"],
        currency: currencyValues.map[json["currency"]],
        accountBalance: json["accountBalance"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "currency": currencyValues.reverse[currency],
        "accountBalance": accountBalance,
    };
}

enum Currency { TUS, SKILL, TIKI, KARMA, HIP, FOINT }

final currencyValues = EnumValues({
    "FOINT": Currency.FOINT,
    "HIP": Currency.HIP,
    "KARMA": Currency.KARMA,
    "SKILL": Currency.SKILL,
    "TIKI": Currency.TIKI,
    "TUS": Currency.TUS
});

enum Authority { USER_READ, USER_UPDATE }

final authorityValues = EnumValues({
    "user:read": Authority.USER_READ,
    "user:update": Authority.USER_UPDATE
});

enum RegistrationType { FACEBOOK, EMAIL, GOOGLE }

final registrationTypeValues = EnumValues({
    "EMAIL": RegistrationType.EMAIL,
    "FACEBOOK": RegistrationType.FACEBOOK,
    "GOOGLE": RegistrationType.GOOGLE
});

enum Role { ROLE_USER }

final roleValues = EnumValues({
    "ROLE_USER": Role.ROLE_USER
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
