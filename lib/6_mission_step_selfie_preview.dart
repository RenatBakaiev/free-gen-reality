import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'dart:ui' as ui;
import 'package:permission_handler/permission_handler.dart';
import 'package:hexcolor/hexcolor.dart';

import '6_home_steps_json.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '10_profile_json.dart';

class MissionStepSelfiePreview extends StatefulWidget {
  final String imgPath;

  MissionStepSelfiePreview({this.imgPath});

  @override
  _MissionStepSelfiePreviewState createState() =>
      _MissionStepSelfiePreviewState();
}

class _MissionStepSelfiePreviewState extends State<MissionStepSelfiePreview> {
  GlobalKey globalKey = GlobalKey();
  GlobalKey _globalKey = GlobalKey();

  bool signalTurnedOn = false;

  makeSignal() {
    setState(() {
      signalTurnedOn = true;
    });
    Future.delayed(const Duration(milliseconds: 250), () {
      setState(() {
        signalTurnedOn = false;
      });
    });
  }

  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: this.context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  width: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      stepPoints != 0 && stepPoints != null
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Вам нараховано ',
                                  style: TextStyle(
                                    color: Hexcolor('#747474'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  stepPoints.toInt().toString() +
                                      ' ' +
                                      stepCurrency.toString(),
                                  style: TextStyle(
                                    color: Hexcolor('#58B12D'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w900,
                                    letterSpacing: 1.089,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            )
                          : Container(),
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            ),
                        child: Text(
                          'Вы молодець, пройшли крок міссії.\nНатисніть кнопку для продовження',
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на наступний крок",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, '/6_mission_step_main.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<User> changeTus(
      double value, String operation, String typeCurrency) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    // setState(() {
    //   isApiCallProcess = true;
    // });
    final http.Response response = await http.post(
      'http://generation-admin.ehub.com.ua/api/account/' +
          operation.toLowerCase() +
          '/' +
          '$id',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "currency": typeCurrency,
        "operation": operation,
        "amount": value,
        "description": "quiz change"
      }),
    );
    print('http://generation-admin.ehub.com.ua/api/account/' +
        operation.toLowerCase() +
        '/' +
        '$id');

    if (response.statusCode == 200) {
      print('quiz change done!!!');
    } else {
      throw Exception('Failed to update User.');
    }
  }

  Future<Stup> setStepProgress() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    var usedId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");

    var stepDone = sharedPreferences.get('step$stepId' + 'Done' + '$usedId');

    String url =
        'http://generation-admin.ehub.com.ua/api/step/finish?user_id=' +
            '$usedId' +
            '&step_id=' +
            '$stepId';
    print(url);
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    print(response.statusCode);
    if (response.statusCode == 200) {
      print('Selfie done');
      setState(() {
        isLoading = false;
      });
      if (stepPoints != null) {
        if (stepDone == '' || stepDone == null) {
          changeTus(stepPoints, "DEPOSIT", stepCurrency);
          sharedPreferences.setBool('step$stepId' + 'Done' + '$usedId', true);
          print('deposit done');
        } else {
          print('you cant to reseive many times');
        }
      }
      showInfoWindow();
      // Navigator.pushNamed(context, '/6_mission_step_main.dart');
    } else {
      print('error to done info');
      setState(() {
        isLoading = false;
      });
    }
    return Stup();
  }

  Future<Stup> finishStep() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    var usedId = sharedPreferences.get('id');
    String token = sharedPreferences.get("token");

    String url =
        'http://generation-admin.ehub.com.ua/api/step/checkAnswer?user_id=' +
            '$usedId' +
            '&step_id=' +
            '$stepId';
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      print(responseJson);
      if (response.statusCode == 200) {
        print('Page done');
        setState(() {
          isLoading = false;
        });
        setStepProgress();
      } else {
        print('error');
        setState(() {
          isLoading = false;
        });
      }
    }

    getMission();
    return Stup();
  }

  bool isLoading = false;
  double stepPoints;
  var stepCurrency;
  Future<Stup> getStepData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var stepId = sharedPreferences.get('stepId');
    String token = sharedPreferences.get("token");
    String url = 'http://generation-admin.ehub.com.ua/api/step/' + '$stepId';
    String urlCurrencies =
        'http://generation-admin.ehub.com.ua/api/publication/admin/all/currencies';
    print(url);
    getMission() async {
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });

      final responseCurrencies = await http.get(urlCurrencies, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print(responseCurrencies.statusCode);
      Map<String, dynamic> responseJsonCurrencies =
          jsonDecode(responseCurrencies.body);

      print(response.body);
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          stepPoints = responseJson["points"];

          for (int i = 0;
              i < responseJsonCurrencies["publication"].length;
              i++) {
            if (responseJson["currencyType"] ==
                responseJsonCurrencies["publication"][i]["id"].toString()) {
              stepCurrency =
                  responseJsonCurrencies["publication"][i]["titleUa"];
              print(stepCurrency);
            }
          }
          isLoading = false;
        });
      } else {
        print('error');
      }
    }

    getMission();
    return Stup();
  }

  @override
  void initState() {
    super.initState();
    getStepData();

    _requestPermission();
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
  }

  _saveScreen() async {
    RenderRepaintBoundary boundary =
        _globalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    final result = await ImageGallerySaver.saveImage(
        byteData.buffer.asUint8List(),
        quality: 100);
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: RepaintBoundary(
                    key: _globalKey,
                    child: Image.file(
                      File(widget.imgPath),
                      filterQuality: FilterQuality.high,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          Positioned(
              bottom: 0,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 240.0,
                  // height: 100.0,
                  color: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Далі",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            _saveScreen();
                            finishStep();
                          },
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Поділитися",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          color: signalTurnedOn
                              ? Hexcolor('#75C433')
                              : Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            makeSignal();
                            getBytesFromFile().then((bytes) {
                              Share.file('Share via', basename(widget.imgPath),
                                  bytes.buffer.asUint8List(), 'image/path',
                                  text: 'FreeGen');
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Future<ByteData> getBytesFromFile() async {
    Uint8List bytes = File(widget.imgPath).readAsBytesSync();
    return ByteData.view(bytes.buffer);
  }
}
