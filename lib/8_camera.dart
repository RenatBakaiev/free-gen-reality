import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/6_home_quests.dart';
import 'package:flutter_free_gen_reality/8_camera_preview.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:model_viewer/model_viewer.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import '8_camera_models_json.dart';
import 'dart:convert';

class Camera extends StatefulWidget {
  @override
  _CameraState createState() => _CameraState();
}

class _CameraState extends State {
  CameraController controller;
  List cameras;
  int selectedCameraIndex;
  String imgPath;

  bool isListHidden = true;

  List<Model3> models;
  bool loading = true;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  @override
  void initState() {
    super.initState();

    ServicesModels.getModelsForCamera().then((list) {
      setState(() {
        models = list.model3;
        loading = false;
      });
    });

    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        _initCameraController(cameras[selectedCameraIndex]).then((void v) {});
      } else {
        print('No camera available');
      }
    }).catchError((err) {
      print('Error :${err.code}Error message : ${err.message}');
    });
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.max);
    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }
      if (controller.value.hasError) {
        print('Camera error ${controller.value.errorDescription}');
      }
    });
    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: _cameraPreviewWidget(),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              height: 90,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(20),
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isListHidden = !isListHidden;
                      });
                    },
                    child: SizedBox(
                      width: 34,
                      child: Image.asset(
                        'assets/fg_images/8_camera_icon_ar.png',
                        height: 34,
                        width: 34,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  _cameraControlWidget(context),
                  SizedBox(
                    width: 34,
                  ),
                  _cameraToggleRowWidget(),
                ],
              ),
            ),
          ),
          loading
              ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
              : Positioned(
                  bottom: 80,
                  child: isListHidden
                      ? models.length != 0
                          ? Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                  physics: BouncingScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: models.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    Model3 model = models[index];
                                    var modelMain = link +
                                        utf8.decode(
                                            model.fileName.runes.toList());
                                            
                                    // print(modelMain);
                                    return Container(
                                      margin: EdgeInsets.only(
                                        right: 20,
                                      ),
                                      height: 300,
                                      width: 100,
                                      child: ModelViewer(
                                          backgroundColor:
                                              Colors.white.withOpacity(0),
                                          src: modelMain,
                                          ar: true,
                                          autoRotate: false,
                                          cameraControls: true,
                                          autoPlay: true),
                                    );
                                  }),
                            )
                          : Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width,
                              child: Center(
                                  child: Text(
                                'Немає 3d об\'єктів',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              )))
                      : Container(),
                ),
          Positioned(
            top: 65,
            left: 15,
            child: IconButton(
              icon: Image.asset(
                'assets/fg_images/sportGames_icon_arrow_back.png',
                width: 10,
                height: 19,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/5_myBottomBar.dart');
              },
            ),
          ),
        ],
      ),
    );
  }

  /// Display Camera preview.
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Loading',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }

    return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: CameraPreview(controller),
    );
  }

  /// Display the control bar with buttons to take pictures
  Widget _cameraControlWidget(context) {
    return FloatingActionButton(
      child: Image.asset(
        'assets/fg_images/8_camera_icon_make_pic.png',
        width: 90,
        height: 90,
      ),
      backgroundColor: Colors.transparent,
      onPressed: () {
        _onCapturePressed(context);
      },
    );
  }

  File imageFile;
  final picker = ImagePicker();

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraToggleRowWidget() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }

    return GestureDetector(
      onTap: _onSwitchCamera,
      child: Container(
        child: Image.asset(
          'assets/fg_images/8_camera_icon_toggle.png',
          width: 40,
          height: 35,
        ),
      ),
    );
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error:${e.code}\nError message : ${e.description}';
    print(errorText);
  }

  void _onCapturePressed(context) async {
    try {
      final path =
          join((await getTemporaryDirectory()).path, '${DateTime.now()}.png');
      await controller.takePicture(path);

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PreviewScreen(
                  imgPath: path,
                )),
      );
    } catch (e) {
      _showCameraException(e);
    }
  }

  void _onSwitchCamera() {
    selectedCameraIndex =
        selectedCameraIndex < cameras.length - 1 ? selectedCameraIndex + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    _initCameraController(selectedCamera);
  }
}
