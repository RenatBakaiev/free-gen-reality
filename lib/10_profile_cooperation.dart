import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/10_profile_cooperation_static_json.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';

void _launchUrl(String i) async {
  if (await canLaunch(i)) {
    await launch(i);
  } else {
    throw 'Could not open Url';
  }
}

class ProfileCooperation extends StatefulWidget {
  @override
  _ProfileCooperationState createState() => _ProfileCooperationState();
}

class _ProfileCooperationState extends State<ProfileCooperation> {
  List<Publication> posts;
  bool loading;

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesStaticCooperation.getCooperation().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Text(
                            'Cпівробітництво',
                            style: TextStyle(
                                fontSize: 33,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // Container(
                  //   child: Expanded(
                  //       child: SingleChildScrollView(
                  //           physics: BouncingScrollPhysics(),
                  //           child: Container(
                  //               margin: EdgeInsets.only(
                  //                 top: 15,
                  //                 left: 20,
                  //                 right: 20,
                  //               ),
                  //               child:

                  Expanded(
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: posts.length,
                        itemBuilder: (context, index) {
                          Publication publication = posts[index];

                          var title =
                              utf8.decode(publication.titleUa.runes.toList());
                          var content =
                              utf8.decode(publication.contentUa.runes.toList());

                          // if (publication.title == "Сотрудничество" ||
                          //     publication.title == "Співробітництво" ||
                          //     publication.title == "Cooperation") {
                          return Column(children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 15,
                                left: 20,
                                right: 20,
                              ),
                              child: Row(
                                children: [
                                  Text(
                                    // '',
                                    title == null ? '' : title,
                                    style: TextStyle(
                                        color: Hexcolor('#484848'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 20,
                                right: 20,
                              ),
                              child: Html(
                                    data: content == null ? '' : content,
                                    onLinkTap: (i) {
                                      _launchUrl(i);
                                    },
                                  ),
                              // child: Text(
                              //   // '',
                              //   content == null ? '' : content,
                              //   style: TextStyle(
                              //     color: Hexcolor('#484848'),
                              //     fontSize: 16,
                              //     fontFamily: 'Arial',
                              //   ),
                              // ),
                            ),
                          ]);
                          // } else {
                          //   return Container();
                          // }
                        }),
                  ),

                  // Column(children: [
                  //   Container(
                  //     margin: EdgeInsets.only(bottom: 15),
                  //     child: Row(
                  //       children: [
                  //         Text(
                  //           'Описание',
                  //           style: TextStyle(
                  //               color: Hexcolor('#484848'),
                  //               fontSize: 18,
                  //               fontFamily: 'Arial',
                  //               fontWeight: FontWeight.w600),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  //   Text(
                  //     'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                  //     style: TextStyle(
                  //       color: Hexcolor('#484848'),
                  //       fontSize: 16,
                  //       fontFamily: 'Arial',
                  //     ),
                  //   ),
                  // ]),
                  //               ))),
                  // ),
                  // Container(
                  //   margin: EdgeInsets.only(bottom: 20),
                  //   height: 60,
                  //   width: MediaQuery.of(context).size.width - 40,
                  //   child: RaisedButton(
                  //     color: Hexcolor('#FE6802'),
                  //     shape: new RoundedRectangleBorder(
                  //       borderRadius: new BorderRadius.circular(14.0),
                  //     ),
                  //     child: Text("Наши контакты",
                  //         style: TextStyle(
                  //             fontWeight: FontWeight.w600,
                  //             fontFamily: 'Arial',
                  //             color: Colors.white,
                  //             letterSpacing: 1.09,
                  //             fontSize: 17)),
                  //     onPressed: () {
                  //       Navigator.pushNamed(context, '/10_profile_aboutus.dart');
                  //     },
                  //   ),
                  // ),
                ],
              ),
            ),
    );
  }
}
