// import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class QuestInfo extends StatefulWidget {
  @override
  _QuestInfoState createState() => _QuestInfoState();
}

class _QuestInfoState extends State<QuestInfo> {
  showInfoWindow() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 390,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Урааа',
                          style: TextStyle(
                            color: Hexcolor('#1E2E45'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        // start progress line
                        margin: EdgeInsets.only(top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                Image.asset(
                                    'assets/fg_images/next_step_icon_start.png',
                                    width: 12,
                                    height: 12),
                                Positioned(
                                  bottom: 9,
                                  left: 4.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_start_flag.png',
                                          width: 18,
                                          height: 21),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 8),
                                        child: Text(
                                          'ул. Владимирская, 24а',
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 12,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#FE6802')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_done_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#1E2E45')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_notdone_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#1E2E45')),
                            ),
                            Image.asset(
                                'assets/fg_images/next_step_icon_notdone_step.png',
                                width: 12,
                                height: 12),
                            Container(
                              height: 1.5,
                              width: 25,
                              decoration:
                                  BoxDecoration(color: Hexcolor('#1E2E45')),
                            ),
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                Image.asset(
                                    'assets/fg_images/next_step_icon_notdone_step.png',
                                    width: 12,
                                    height: 12),
                                Positioned(
                                  top: 9,
                                  right: 4.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(right: 10, top: 8),
                                        child: Text(
                                          'ул. Богдана Хмельницкого, 13/2',
                                          style: TextStyle(
                                            color: Hexcolor('#1E2E45'),
                                            fontSize: 12,
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      Image.asset(
                                          'assets/fg_images/next_step_icon_finish_flag.png',
                                          width: 18,
                                          height: 21),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            ),
                        child: Text(
                          'Вы молодец, прошли шаг миссии.\nНажмите кнопку для продолжения',
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Перейти на cледующий шаг",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/questQuiz.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -130,
                  right: -30,
                  child: Image.asset(
                    'assets/fg_images/error_mission_icon_unlock.png',
                    width: 200,
                    height: 200,
                  ),
                ),
                Positioned(
                  right: 40,
                  top: 50,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '1',
                          style: TextStyle(
                            fontSize: 36,
                            color: Hexcolor('#1E2E45'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 2),
                          child: Text(
                            ' ' + '/ ' + '4',
                            style: TextStyle(
                              fontSize: 24,
                              color: Hexcolor('#1E2E45'),
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#FFFFFF'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 85,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 340,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/fg_images/6_home_quest_6.png'),
                                  fit: BoxFit.cover,
                                )),
                            child: Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                right: 20,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    '1',
                                    style: TextStyle(
                                      fontSize: 36,
                                      color: Colors.white,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Text(
                                    ' ' + '/ ' + '4',
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: Colors.white,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Все шаги',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'В этой миссии пройдите все шаги. Можно протестировать как работают все страницы миссий приложения. В остальных миссиях реализован каждый шаг отдельно.',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: SizedBox(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_search_back.png',
                                    width: 10,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    // Navigator.pushNamed(
                                    //     context, '/sport_games.dart');
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ),
                            Text(
                              'Описание',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/run_map_quest_pic_gostart.png',
                                width: 22,
                                height: 24,
                              ),
                              onPressed: () {
                                Navigator.pushNamed(
                                  context,
                                  '/5_myBottomBar.dart',
                                );
                                // Navigator.of(context).pop();
                                // Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width - 40,
                        child: RaisedButton(
                          child: Text(
                            "Далее",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            // Navigator.pushNamed(context, '/questQuiz.dart');
                            showInfoWindow();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
