import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/10_profile_rating_json.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:hexcolor/hexcolor.dart';
import '6_home_filter_age_ages.dart';
// import '10_profile_rating_users.dart';
import '6_home_filter_location_regions.dart';
import 'dart:convert' show utf8;

class ProfileRating extends StatefulWidget {
  @override
  _ProfileRatingState createState() => _ProfileRatingState();
}

class _ProfileRatingState extends State<ProfileRating> {
  bool isSexManSelected = false;
  bool isSexWomanSelected = false;
  chooseSex() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 235,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть стать",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isSexManSelected = !isSexManSelected;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isSexManSelected
                                            ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                            : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Чоловіча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isSexWomanSelected = !isSexWomanSelected;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isSexWomanSelected
                                            ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                            : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Жіноча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(bottom: 20),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      // setSex();
                                      Navigator.of(context).pop();
                                      // if ((isSexManSelected ||
                                      //         isSexWomanSelected) ||
                                      //     isSexManSelected &&
                                      //         isSexWomanSelected) {
                                      //   isSexSelected = true;
                                      // }
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  chooseAge() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 540,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          // height: 20,
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть вік",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Expanded(
                                    child: SingleChildScrollView(
                                      physics: BouncingScrollPhysics(),
                                      child: ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          shrinkWrap: true,
                                          itemCount: ages.length,
                                          itemBuilder: (context, index) {
                                            return GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                setState(() {
                                                  ages[index].isSelected =
                                                      !ages[index].isSelected;
                                                });
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                  top: 20,
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SizedBox(
                                                      width: 2,
                                                    ),
                                                    Image.asset(
                                                      ages[index].isSelected
                                                          ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                                          : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                                      width: 18,
                                                      height: 18,
                                                    ),
                                                    SizedBox(
                                                      width: 19,
                                                    ),
                                                    Text(
                                                      ages[index].age,
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontSize: 17,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(bottom: 20),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  chooseCity() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: MediaQuery.of(context).size.height - 20,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          // height: 20,
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть місто",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Expanded(
                                    child: SingleChildScrollView(
                                      physics: BouncingScrollPhysics(),
                                      child: ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          shrinkWrap: true,
                                          itemCount: regions.length,
                                          itemBuilder: (context, index) {
                                            return GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                setState(() {
                                                  regions[index].isSelected =
                                                      !regions[index]
                                                          .isSelected;
                                                });
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SizedBox(
                                                      width: 2,
                                                    ),
                                                    Image.asset(
                                                      regions[index].isSelected
                                                          ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                                          : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                                      width: 18,
                                                      height: 18,
                                                    ),
                                                    SizedBox(
                                                      width: 19,
                                                    ),
                                                    Text(
                                                      regions[index].city,
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontSize: 17,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                                Container(
                                  // margin: EdgeInsets.only(bottom: 20),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  List<User> users;
  bool loading;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesUsers.getUsers().then((post) {
      setState(() {
        users = post.users;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#FFFFFF'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.pushNamed(context, '/quest_done.dart'); // checking
                            },
                            child: Text(
                              'Рейтинг',
                              style: TextStyle(
                                  fontSize: 37,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // Container(
                  //   margin: EdgeInsets.only(
                  //     left: 20,
                  //     right: 20,
                  //     top: 15,
                  //   ),
                  //   child: Row(
                  //     children: [
                  //       Container(
                  //         height: 60,
                  //         width: MediaQuery.of(context).size.width / 3 - 20,
                  //         child: RaisedButton(
                  //           child: Row(
                  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //             crossAxisAlignment: CrossAxisAlignment.center,
                  //             children: [
                  //               SizedBox(
                  //                 width: 6,
                  //               ),
                  //               Text(
                  //                 "Місто",
                  //                 style: TextStyle(
                  //                   color: Colors.white,
                  //                   fontSize: 14,
                  //                   fontFamily: 'Arial',
                  //                   fontWeight: FontWeight.w800,
                  //                 ),
                  //               ),
                  //               Image.asset(
                  //                 'assets/fg_images/10_profile_rating_button_icon2.png',
                  //                 width: 6,
                  //                 height: 11,
                  //               )
                  //             ],
                  //           ),
                  //           color: Hexcolor('#FE6802'),
                  //           shape: new RoundedRectangleBorder(
                  //               borderRadius: new BorderRadius.circular(14.0)),
                  //           onPressed: () {
                  //             chooseCity();
                  //           },
                  //         ),
                  //       ),
                  //       SizedBox(
                  //         width: 10,
                  //       ),
                  //       Container(
                  //         height: 60,
                  //         width: MediaQuery.of(context).size.width / 3 - 20,
                  //         child: RaisedButton(
                  //           child: Row(
                  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //             crossAxisAlignment: CrossAxisAlignment.center,
                  //             children: [
                  //               SizedBox(
                  //                 width: 6,
                  //               ),
                  //               Text(
                  //                 "Вік",
                  //                 style: TextStyle(
                  //                   color: Colors.white,
                  //                   fontSize: 14,
                  //                   fontFamily: 'Arial',
                  //                   fontWeight: FontWeight.w800,
                  //                 ),
                  //               ),
                  //               Image.asset(
                  //                 'assets/fg_images/10_profile_rating_button_icon2.png',
                  //                 width: 6,
                  //                 height: 11,
                  //               )
                  //             ],
                  //           ),
                  //           color: Hexcolor('#FE6802'),
                  //           shape: new RoundedRectangleBorder(
                  //               borderRadius: new BorderRadius.circular(14.0)),
                  //           onPressed: () {
                  //             chooseAge();
                  //           },
                  //         ),
                  //       ),
                  //       SizedBox(
                  //         width: 10,
                  //       ),
                  //       Container(
                  //         height: 60,
                  //         width: MediaQuery.of(context).size.width / 3 - 20,
                  //         child: RaisedButton(
                  //           child: Row(
                  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //             crossAxisAlignment: CrossAxisAlignment.center,
                  //             children: [
                  //               SizedBox(
                  //                 width: 6,
                  //               ),
                  //               Text(
                  //                 "Стать",
                  //                 style: TextStyle(
                  //                   color: Colors.white,
                  //                   fontSize: 14,
                  //                   fontFamily: 'Arial',
                  //                   fontWeight: FontWeight.w800,
                  //                 ),
                  //               ),
                  //               Image.asset(
                  //                 'assets/fg_images/10_profile_rating_button_icon2.png',
                  //                 width: 6,
                  //                 height: 11,
                  //               )
                  //             ],
                  //           ),
                  //           color: Hexcolor('#FE6802'),
                  //           shape: new RoundedRectangleBorder(
                  //               borderRadius: new BorderRadius.circular(14.0)),
                  //           onPressed: () {
                  //             chooseSex();
                  //           },
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Expanded(
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: users.length,
                      itemBuilder: (context, index) {
                        User user = users[index];

                        // var firstName =
                        //     utf8.decode(user.firstName.runes.toList());
                        // var lastName =
                        //     utf8.decode(user.lastName.runes.toList());
                        var username = user.username;
                        var tusa;

                        for (int i = 0; i < user.accounts.length; i++) {
                          if (user.accounts[i].currency == Currency.TUS) {
                            var tus = user.accounts[i].accountBalance;
                            tusa = tus;
                          }
                        }
                        return Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width - 40,
                          margin:
                              EdgeInsets.only(bottom: 23, left: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    radius: 30,
                                    backgroundImage: NetworkImage(user
                                                    .profileImageUrl ==
                                                "" ||
                                            user.profileImageUrl == null
                                        ? '$link' + 'default.png'
                                        : user.profileImageUrl
                                                .contains('localhost')
                                            ? '${'http://generation-admin.ehub.com.ua' + user.profileImageUrl.substring(21)}'
                                            : user.profileImageUrl),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(left: 14),
                                      child: Text(
                                        username == null ? '' : utf8.decode(username.runes.toList()),
                                        // (firstName == null ? '' : firstName) + ' ' + (lastName == null ? '' : lastName),
                                        // firstName == null ? '' : firstName,
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 16,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )),
                                ],
                              ),
                              Container(
                                width: 55,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      // 'assets/fg_images/10_profile_rating_star.png', // Changed to TUS
                                      'assets/fg_images/10_profile_user_param_icon_tus.png',
                                      height: 20,
                                      // height: 13.23,
                                      // width: 12.64,
                                    ),
                                    Text(
                                      tusa.toInt().toString(),
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 16,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
