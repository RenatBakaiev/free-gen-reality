// ----------------------Update RunMapQuest 30.09.20------------------------------------
import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/geolocator_service.dart';
import 'package:flutter_free_gen_reality/run_map_quest_checkpoints.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';

class MissionPinsOnMap extends StatefulWidget {
  @override
  _MissionPinsOnMapState createState() => _MissionPinsOnMapState();
}

class _MissionPinsOnMapState extends State<MissionPinsOnMap> {
  // final GeolocatorService geoService = GeolocatorService();
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  Geolocator geolocator = Geolocator();
  Position userLocation;

  var currentLocation;
  bool mapToggle = false;

  double zoomVal = 17.0;

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _getMyLocation(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  // _checkMyLocation() async {
  //   if (await currentLocation.latitude == _destLatitude &&
  //       currentLocation.latitude == _destLongitude) {
  //     print('HELLO!');
  //   }
  // }

  BitmapDescriptor _checkPoint1;
  BitmapDescriptor _checkPoint2;
  BitmapDescriptor _checkPoint3;
  BitmapDescriptor _checkPoint4;
  BitmapDescriptor _checkPoint5;
  // BitmapDescriptor _me;
  Set<Marker> _markers = HashSet<Marker>();

  // double _destLatitude = 50.449284, _destLongitude = 30.512330;
  // double _destLatitude = 50.449293, _destLongitude = 30.512536; 50.450259, 30.511106
  double _destLatitude = 50.450259, _destLongitude =  30.511106;
  double _destLatitude2 = 50.448845, _destLongitude2 = 30.514550;
  double _destLatitude3 = 50.445969, _destLongitude3 = 30.513440;
  double _destLatitude4 = 50.445539, _destLongitude4 = 30.515864;
  double _destLatitude5 = 50.445271, _destLongitude5 = 30.518097;

  Map<PolylineId, Polyline> polylines = {};

  List<LatLng> polylineCoordinates0 = [];
  List<LatLng> polylineCoordinates = [];
  List<LatLng> polylineCoordinates2 = [];
  List<LatLng> polylineCoordinates3 = [];
  List<LatLng> polylineCoordinates4 = [];

  PolylinePoints polylinePoints0 = PolylinePoints();
  PolylinePoints polylinePoints = PolylinePoints();
  PolylinePoints polylinePoints2 = PolylinePoints();
  PolylinePoints polylinePoints3 = PolylinePoints();
  PolylinePoints polylinePoints4 = PolylinePoints();

  // var _locality = "";

  String _address = ""; // Display your position details.
  // void _getPlace() async {
  //   List<Placemark> newPlace = await geolocator.placemarkFromCoordinates(
  //       currentLocation.latitude, currentLocation.longitude);

  //   // this is all you need
  //   Placemark placeMark = newPlace[0];
  //   String name = placeMark.name;
  //   String isoCountryCode = placeMark.isoCountryCode;
  //   String subAdministrativeArea = placeMark.subAdministrativeArea;
  //   String thoroughfare = placeMark.thoroughfare;
  //   String subThoroughfare = placeMark.subThoroughfare;
  //   Position position = placeMark.position;
  //   String subLocality = placeMark.subLocality;
  //   String locality = placeMark.locality;
  //   String administrativeArea = placeMark.administrativeArea;
  //   String postalCode = placeMark.postalCode;
  //   String country = placeMark.country;
  //   String address =
  //       "$name, $subLocality, $locality, $administrativeArea, $postalCode, $country, $isoCountryCode, $subAdministrativeArea, $thoroughfare, $subThoroughfare, $position,";

  //   print(address);

  //   setState(() {
  //     _address = address;
  //     // _locality = country;  
  //   });
  // }

  @override
  void initState() {
    Geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        mapToggle = true;
      });
    });

    // Geolocator
    //     .getPositionStream(LocationOptions(
    //         accuracy: LocationAccuracy.bestForNavigation, timeInterval: 5000))
    //     .listen((position) {
    //   // Do something here
    //   if (currentLocation.latitude ==
    //           _destLatitude && // 50.4492936, 30.5125368;
    //       currentLocation.longitude == _destLongitude) {
    //     print('you are at start');
    //     showInfoWindowCheckTrue();
    //     _setCheckPoint10();
    //   } else {
    //     // print('you are not at start');
    //     // showInfoWindowCheckFalse();
    //   }
    // });

    super.initState();

    _getPolyline();
    _getPolyline2();
    _getPolyline3();
    _getPolyline4();

    _setCheckPoint1();
    _setCheckPoint2();
    _setCheckPoint3();
    _setCheckPoint4();
    _setCheckPoint5();
    // _setMe();
  }

  void _setCheckPoint10() async {
    _checkPoint1 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_done.png',
    );
  }

  void _setCheckPoint1() async {
    _checkPoint1 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_start.png',
    );
  }

  void _setCheckPoint2() async {
    _checkPoint2 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_done.png',
    );
  }

  void _setCheckPoint3() async {
    _checkPoint3 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_2.png',
    );
  }

  void _setCheckPoint4() async {
    _checkPoint4 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_check_3.png',
    );
  }

  void _setCheckPoint5() async {
    _checkPoint5 = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/fg_images/run_map_quest_pic_finish.png',
    );
  }

  // void _setMe() async {
  //   _me = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     'assets/fg_images/run_map_quest_pic_meme.png',
  //   );
  // }

  showInfoWindowCheckTrue() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я на месте',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрыть",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  showInfoWindowCheckFalse() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Я не на месте',
                          style: TextStyle(
                            color: Hexcolor('#FF1E1E'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрыть",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(overflow: Overflow.visible, children: [
          Container(
            margin: EdgeInsets.only(top: 109),
            child: mapToggle
                ? GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: LatLng(currentLocation.latitude,
                            currentLocation.longitude),
                        zoom: 17.0),
                    mapType: MapType.normal,
                    tiltGesturesEnabled: true,
                    compassEnabled: true,
                    scrollGesturesEnabled: true,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: false, // off default buttons
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false, // off default my location
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);

                      _getLocation() {
                        print("locationLatitude: ${currentLocation.latitude}");
                        print(
                            "locationLongitude: ${currentLocation.longitude}");
                        print("accuracy: ${currentLocation.accuracy}");
                        print("altitude: ${currentLocation.altitude}");
                        print("speed: ${currentLocation.speed}");
                      }

                      _addPolyLine0() {
                        PolylineId id = PolylineId("meToStart");
                        Polyline polyline0 = Polyline(
                            patterns: <PatternItem>[
                              PatternItem.dot,
                              PatternItem.gap(50)
                            ],
                            polylineId: id,
                            color: Hexcolor('#737373'),
                            points: polylineCoordinates0,
                            width: 10);
                        polylines[id] = polyline0;
                        setState(() {});
                      }

                      _getPolyline0() async {
                        PolylineResult result0 =
                            await polylinePoints0.getRouteBetweenCoordinates(
                          "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
                          PointLatLng(currentLocation.latitude,
                              currentLocation.longitude),
                          PointLatLng(_destLatitude, _destLongitude),
                          travelMode: TravelMode.walking,
                          // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
                        );
                        if (result0.points.isNotEmpty) {
                          result0.points.forEach((PointLatLng point0) {
                            polylineCoordinates0
                                .add(LatLng(point0.latitude, point0.longitude));
                          });
                        }
                        _addPolyLine0();
                      }

                      _getPolyline0();

                      setState(() {
                        _markers.add(Marker(
                          markerId: MarkerId("0"),
                          position: LatLng(_destLatitude, _destLongitude),
                          icon: _checkPoint1,
                          onTap: () {
                            _getLocation();
                            if (currentLocation.latitude == _destLatitude &&
                                currentLocation.longitude == _destLongitude) {
                              print('hi');
                            }
                            // showInfoWindow();
                            // Navigator.pushNamed(
                            //     context, '/mission_pinsOnMap.dart');
                            // _getPolyline0();
                          },
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("1"),
                          position: LatLng(_destLatitude2, _destLongitude2),
                          icon: _checkPoint2,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("2"),
                          position: LatLng(_destLatitude3, _destLongitude3),
                          icon: _checkPoint3,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("3"),
                          position: LatLng(_destLatitude4, _destLongitude4),
                          icon: _checkPoint4,
                        ));

                        _markers.add(Marker(
                          markerId: MarkerId("4"),
                          position: LatLng(_destLatitude5, _destLongitude5),
                          icon: _checkPoint5,
                          onTap: () {
                            // Navigator.pushNamed(context, '/quest_done.dart');
                            Navigator.pushNamed(
                                context, '/mission_pinsOnMap_10.dart');
                          },
                        ));

                        // _markers.add(Marker(
                        //   markerId: MarkerId("5"),
                        //   position: LatLng(currentLocation.latitude,
                        //       currentLocation.longitude),
                        //   icon: _me,
                        // ));
                      });
                    },
                    polylines: Set<Polyline>.of(polylines.values),
                    markers: _markers,
                  )
                : Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Hexcolor('#7D5AC2'),
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        Hexcolor('#FE6802'),
                      ),
                    ),
                  ),
          ),
          Positioned(
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                  // left: 15,
                  // right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                // Navigator.pushNamed(
                                //     context, '/desc_pinsOnMap.dart');
                                Navigator.of(context).pop();
                                // Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          // _locality,
                          "Метки на карте",
                          style: TextStyle(
                              fontSize: 33,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/run_map_quest_pic_gostart.png',
                            width: 22,
                            height: 24,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                            // Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Positioned(
          //   right: 20,
          //   top: 140,
          //   child: GestureDetector(
          //       onTap: () {
          //         Navigator.pushNamed(context, '/mission_pinsOnMap.dart');
          //       },
          //       child: Image.asset(
          //         'assets/fg_images/run_map_quest_pic_update.png',
          //         width: 50,
          //         height: 50,
          //       )),
          // ),
          Positioned(
            top: 140,
            right: 20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/mission_pinsOnMap.dart');
                    },
                    child: Image.asset(
                      'assets/fg_images/run_map_quest_pic_update.png',
                      width: 50,
                      height: 50,
                    )),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                    onTap: () {
                      zoomVal++;
                      _plus(zoomVal);
                    },
                    child: Image.asset(
                      'assets/fg_images/7_map_icon_zoom_plus.png',
                      width: 50,
                      height: 50,
                    )),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    zoomVal--;
                    _minus(zoomVal);
                  },
                  child: Image.asset(
                    'assets/fg_images/7_map_icon_zoom_minus.png',
                    width: 50,
                    height: 50,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    _getMyLocation(zoomVal);
                  },
                  child: Image.asset(
                    'assets/fg_images/7_map_icon_location.png',
                    width: 50,
                    height: 50,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 100,
            child: Container(
              margin: EdgeInsets.only(left: 5, right: 5),
              height: 117,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: checkPoins.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      _goToCheckPoint(
                          checkPoins[index].lat, checkPoins[index].long);
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 5, right: 5),
                      height: 117,
                      width: 174,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                            image: AssetImage(checkPoins[index].image),
                            fit: BoxFit.cover,
                          )),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: checkPoins[index].color.withOpacity(0.5),
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0)),
                            ),
                            height: 33,
                            child: Container(
                              margin: EdgeInsets.only(
                                left: 5,
                                bottom: 5,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(
                                      checkPoins[index].name,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                        letterSpacing: 1.09,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Positioned(
            left: 20,
            bottom: 0,
            child: Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Я на месте",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  // _getPlace();

                  print(
                      "locationLatitude: ${currentLocation.latitude}"); // its works
                  print("locationLongitude: ${currentLocation.longitude}");
                  if (currentLocation.latitude ==
                          _destLatitude && // 50.4492936, 30.5125368;
                      currentLocation.longitude == _destLongitude) {
                    print('you are at start');
                    showInfoWindowCheckTrue();
                  } else {
                    print('you are not at start');
                    showInfoWindowCheckFalse();
                  }
                },
              ),
            ),
          )
        ]),
      ),
    );
  }

  Future<void> _goToCheckPoint(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 17)));
  }

  // _addMeMarker(
  //     LatLng position, String id, BitmapDescriptor _me, InfoWindow infowindow) {
  //   MarkerId markerId = MarkerId(id);
  //   Marker marker = Marker(
  //       markerId: markerId,
  //       icon: _me,
  //       position: position,
  //       infoWindow: infowindow);
  //       marker =
  //   // _markers = marker;
  // }

// first polyline
  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
        patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
        polylineId: id,
        color: Colors.green,
        points: polylineCoordinates,
        width: 10);
    polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline() async {
    PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(_destLatitude, _destLongitude),
      PointLatLng(_destLatitude2, _destLongitude2),
      travelMode: TravelMode.walking,
      // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
    );
    if (result1.points.isNotEmpty) {
      result1.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }

// second polyline
  _addPolyLine2() {
    PolylineId id = PolylineId("poly2");
    Polyline polyline2 = Polyline(
        patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
        polylineId: id,
        color: Colors.blue,
        points: polylineCoordinates2,
        width: 10);
    polylines[id] = polyline2;
    setState(() {});
  }

  _getPolyline2() async {
    PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(_destLatitude2, _destLongitude2),
      PointLatLng(_destLatitude3, _destLongitude3),
      travelMode: TravelMode.walking,
    );
    if (result2.points.isNotEmpty) {
      result2.points.forEach((PointLatLng point2) {
        polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
      });
    }
    _addPolyLine2();
  }

// third polyline
  _addPolyLine3() {
    PolylineId id = PolylineId("poly3");
    Polyline polyline3 = Polyline(
        patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
        polylineId: id,
        color: Colors.blue,
        points: polylineCoordinates3,
        width: 10);
    polylines[id] = polyline3;
    setState(() {});
  }

  _getPolyline3() async {
    PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(_destLatitude3, _destLongitude3),
      PointLatLng(_destLatitude4, _destLongitude4),
      travelMode: TravelMode.walking,
    );
    if (result3.points.isNotEmpty) {
      result3.points.forEach((PointLatLng point3) {
        polylineCoordinates3.add(LatLng(point3.latitude, point3.longitude));
      });
    }
    _addPolyLine3();
  }

// fourth polyline
  _addPolyLine4() {
    PolylineId id = PolylineId("poly4");
    Polyline polyline4 = Polyline(
        patterns: <PatternItem>[PatternItem.dot, PatternItem.gap(50)],
        polylineId: id,
        color: Colors.blue,
        points: polylineCoordinates4,
        width: 10);
    polylines[id] = polyline4;
    setState(() {});
  }

  _getPolyline4() async {
    PolylineResult result4 = await polylinePoints4.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(_destLatitude4, _destLongitude4),
      PointLatLng(_destLatitude5, _destLongitude5),
      travelMode: TravelMode.walking,
    );
    if (result4.points.isNotEmpty) {
      result4.points.forEach((PointLatLng point4) {
        polylineCoordinates4.add(LatLng(point4.latitude, point4.longitude));
      });
    }
    _addPolyLine4();
  }

  Future<void> centerScreen(Position currentLocation) async {
    setState(() {
      // _markers.add(Marker(
      //   markerId: MarkerId("5"),
      //   position: LatLng(currentLocation.latitude, currentLocation.longitude),
      //   icon: _me,
      // ));
    });
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 17.0),
    ));
  }
}
