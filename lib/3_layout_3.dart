import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ProgressHUD.dart';
// import '10_profile_rewards.dart';

String finalEmail;

class Layout3 extends StatefulWidget {
  @override
  _Layout3State createState() => _Layout3State();
}

class _Layout3State extends State<Layout3> {
  // showYouReceiveRewardWindow() {
  showWelcomeWindow() {
    // setState(() {
    //   allRewards[5].quantity = allRewards[5].quantity + 10;
    // });
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              // width: MediaQuery.of(context).size.width - 40,
              height: 600,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: 20,
                        left: 10,
                        right: 10,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 10,
                              ),
                              child: Text(
                                'Ласкаво просимо!',
                                style: TextStyle(
                                  color: Hexcolor('#1E2E45'),
                                  fontSize: 24,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 5,
                              ),
                              child: Text(
                                'Зареєструйтесь в додатку',
                                style: TextStyle(
                                  color: Hexcolor('#747474'),
                                  fontSize: 20,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'і отримайте бонус  ',
                                  style: TextStyle(
                                    color: Hexcolor('#747474'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  '20' + ' TUS',
                                  style: TextStyle(
                                    color: Hexcolor('#FE6802'),
                                    fontSize: 20,
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ])),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                    ),
                    child: Image.asset(
                      'assets/fg_images/3_layout_3_welcome.jpg',
                      height: 290,
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.only(bottom: 20, right: 20, left: 20),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            "Реєстрація",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, '/4_enter_register');
                          },
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(bottom: 20, right: 20, left: 20),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          child: Text(
                            // "Продовжити",
                            "Вхід",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            // Navigator.pushNamed(context, '/5_myBottomBar.dart');
                            Navigator.pushNamed(context, '/4_enter');
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
  }

  Future getValidationData() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var obtainedEmail = sharedPreferences.getString('email');
    setState(() {
      finalEmail = obtainedEmail;
      isApiCallProcess = false;
    });
    print(finalEmail);
  }

  bool isApiCallProcess = false;
  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
        body: Stack(
      overflow: Overflow.visible,
      children: [
        Container(
          color: Hexcolor('#412A72'),
          child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                child: Image.asset(
                  'assets/fg_images/3_layout_3_back.jpg',
                  width: MediaQuery.of(context).size.width,
                ),
              )),
        ),
        Positioned(
            bottom: 0,
            left: 25,
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot2.png',
                    width: 10,
                    height: 10,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot2.png',
                    width: 10,
                    height: 10,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    'assets/fg_images/1_layout_1_icon_dot.png',
                    width: 10,
                    height: 10,
                  ),
                ],
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        bottom: 25,
                        top: 30,
                      ),
                      height: 60,
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      child: RaisedButton(
                        child: Text(
                          "Назад",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        color: Hexcolor('#BEBEBE'),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(14.0)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/2_layout_2');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 25),
                      height: 60,
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      child: RaisedButton(
                        child: Text(
                          "Почати",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        color: Hexcolor('#FE6802'),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(14.0)),
                        onPressed: () {
                          setState(() {
                            isApiCallProcess = true;
                          });
                          getValidationData().whenComplete(() async {
                            
                            finalEmail == null
                                ? showWelcomeWindow()
                                : Navigator.pushNamed(
                                    context, '/5_myBottomBar.dart');
                          });
                          // showWelcomeWindow();
                        },
                      ),
                    ),
                  ])
            ]))
      ],
    ));
  }
}
