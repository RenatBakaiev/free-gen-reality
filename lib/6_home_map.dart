// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:flutter_free_gen_reality/geolocator_service.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:provider/provider.dart';

// class HomeMap extends StatelessWidget {
//   final geoService = GeolocatorService();
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(children: <Widget>[
//         FutureProvider(
//           create: (context) => geoService.getInitialLocation(),
//           child: MaterialApp(
//             debugShowCheckedModeBanner: false,
//             title: 'Flutter Demo',
//             theme: ThemeData(
//               primarySwatch: Colors.blue,
//             ),
//             home: Consumer<Position>(
//               builder: (context, position, widget) {
//                 return (position != null)
//                     ? GMap(position)
//                     : Center(child: CircularProgressIndicator());
//               },
//             ),
//           ),
//         ),

//       ]),
//     );
//   }
// }

// class GMap extends StatefulWidget {
//   final Position initialPosition;

//   GMap(this.initialPosition);

//   @override
//   State<StatefulWidget> createState() => _MapState();
// }

// class _MapState extends State<GMap> {
//   final GeolocatorService geoService = GeolocatorService();
//   Completer<GoogleMapController> _controller = Completer();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";


//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: GoogleMap(
//         initialCameraPosition: CameraPosition(
//             target: LatLng(widget.initialPosition.latitude,
//                 widget.initialPosition.longitude),
//             zoom: 17.0),
//         mapType: MapType.normal,
//         tiltGesturesEnabled: true,
//         compassEnabled: true,
//         scrollGesturesEnabled: true,
//         zoomGesturesEnabled: true,
//         myLocationEnabled: true,
//         onMapCreated: (GoogleMapController controller) {
//           _controller.complete(controller);
          
//         },
//       ),
//     );
//   }

// }