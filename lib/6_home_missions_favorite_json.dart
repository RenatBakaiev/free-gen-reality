// To parse this JSON data, do
//
//     final favoriteMissions = favoriteMissionsFromJson(jsonString);

import 'dart:convert';

List<FavoriteMissions> favoriteMissionsFromJson(String str) => List<FavoriteMissions>.from(json.decode(str).map((x) => FavoriteMissions.fromJson(x)));

String favoriteMissionsToJson(List<FavoriteMissions> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FavoriteMissions {
    FavoriteMissions({
        this.id,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.accessType,
        this.subMissionList,
        this.filters,
        this.statuses,
        this.duration,
        this.descriptionUa,
        this.descriptionRu,
        this.descriptionEn,
        this.location,
        this.position,
        this.createdDate,
        this.lastModifiedDate,
        this.currencyType,
        this.points,
        this.imageUrl,
        this.audioUrl,
        this.videoUrl,
        this.promoCode,
        this.price,
        this.priceCurrency,
        this.recommend,
        this.active,
        this.progress,
        this.distanceInMeters,
    });

    int id;
    String titleUa;
    String titleRu;
    String titleEn;
    String accessType;
    List<dynamic> subMissionList;
    List<dynamic> filters;
    List<dynamic> statuses;
    int duration;
    String descriptionUa;
    String descriptionRu;
    String descriptionEn;
    Location location;
    int position;
    DateTime createdDate;
    DateTime lastModifiedDate;
    String currencyType;
    double points;
    String imageUrl;
    String audioUrl;
    String videoUrl;
    String promoCode;
    double price;
    String priceCurrency;
    bool recommend;
    bool active;
    String progress;
    double distanceInMeters;

    factory FavoriteMissions.fromJson(Map<String, dynamic> json) => FavoriteMissions(
        id: json["id"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        accessType: json["accessType"],
        subMissionList: List<dynamic>.from(json["subMissionList"].map((x) => x)),
        filters: List<dynamic>.from(json["filters"].map((x) => x)),
        statuses: List<dynamic>.from(json["statuses"].map((x) => x)),
        duration: json["duration"],
        descriptionUa: json["descriptionUa"],
        descriptionRu: json["descriptionRu"],
        descriptionEn: json["descriptionEn"],
        location: Location.fromJson(json["location"]),
        position: json["position"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        currencyType: json["currencyType"],
        points: json["points"],
        imageUrl: json["imageUrl"],
        audioUrl: json["audioUrl"],
        videoUrl: json["videoUrl"],
        promoCode: json["promoCode"],
        price: json["price"],
        priceCurrency: json["priceCurrency"],
        recommend: json["recommend"],
        active: json["active"],
        progress: json["progress"],
        distanceInMeters: json["distanceInKilometers"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "accessType": accessType,
        "subMissionList": List<dynamic>.from(subMissionList.map((x) => x)),
        "filters": List<dynamic>.from(filters.map((x) => x)),
        "statuses": List<dynamic>.from(statuses.map((x) => x)),
        "duration": duration,
        "descriptionUa": descriptionUa,
        "descriptionRu": descriptionRu,
        "descriptionEn": descriptionEn,
        "location": location.toJson(),
        "position": position,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "currencyType": currencyType,
        "points": points,
        "imageUrl": imageUrl,
        "audioUrl": audioUrl,
        "videoUrl": videoUrl,
        "promoCode": promoCode,
        "price": price,
        "priceCurrency": priceCurrency,
        "recommend": recommend,
        "active": active,
        "progress": progress,
        "distanceInMeters": distanceInMeters,
    };
}

class Location {
    Location({
        this.id,
        this.latitude,
        this.longitude,
        this.active,
    });

    int id;
    String latitude;
    String longitude;
    bool active;

    factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "active": active,
    };
}
