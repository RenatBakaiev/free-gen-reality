

// List<Post> postFromJson(String str) => List<Post>.from(json.decode(str).map((x) => Post.fromJson(x)));

// String postToJson(List<Post> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Post {
//     Post({
//         this.userId,
//         this.id,
//         this.title,
//         this.body,
//     });

//     int userId;
//     int id;
//     String title;
//     String body;

//     factory Post.fromJson(Map<String, dynamic> json) => Post(
//         userId: json["userId"],
//         id: json["id"],
//         title: json["title"],
//         body: json["body"],
//     );

//     Map<String, dynamic> toJson() => {
//         "userId": userId,
//         "id": id,
//         "title": title,
//         "body": body,
//     };
// }

// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);

// import 'dart:convert';

// List<Post> postFromJson(String str) => List<Post>.from(json.decode(str).map((x) => Post.fromJson(x)));

// String postToJson(List<Post> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Post {
//     Post({
//         this.id,
//         this.name,
//         this.username,
//         this.email,
//         this.address,
//         this.phone,
//         this.website,
//         this.company,
//     });

//     int id;
//     String name;
//     String username;
//     String email;
//     Address address;
//     String phone;
//     String website;
//     Company company;

//     factory Post.fromJson(Map<String, dynamic> json) => Post(
//         id: json["id"],
//         name: json["name"],
//         username: json["username"],
//         email: json["email"],
//         address: Address.fromJson(json["address"]),
//         phone: json["phone"],
//         website: json["website"],
//         company: Company.fromJson(json["company"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "username": username,
//         "email": email,
//         "address": address.toJson(),
//         "phone": phone,
//         "website": website,
//         "company": company.toJson(),
//     };
// }

// class Address {
//     Address({
//         this.street,
//         this.suite,
//         this.city,
//         this.zipcode,
//         this.geo,
//     });

//     String street;
//     String suite;
//     String city;
//     String zipcode;
//     Geo geo;

//     factory Address.fromJson(Map<String, dynamic> json) => Address(
//         street: json["street"],
//         suite: json["suite"],
//         city: json["city"],
//         zipcode: json["zipcode"],
//         geo: Geo.fromJson(json["geo"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "street": street,
//         "suite": suite,
//         "city": city,
//         "zipcode": zipcode,
//         "geo": geo.toJson(),
//     };
// }

// class Geo {
//     Geo({
//         this.lat,
//         this.lng,
//     });

//     String lat;
//     String lng;

//     factory Geo.fromJson(Map<String, dynamic> json) => Geo(
//         lat: json["lat"],
//         lng: json["lng"],
//     );

//     Map<String, dynamic> toJson() => {
//         "lat": lat,
//         "lng": lng,
//     };
// }

// class Company {
//     Company({
//         this.name,
//         this.catchPhrase,
//         this.bs,
//     });

//     String name;
//     String catchPhrase;
//     String bs;

//     factory Company.fromJson(Map<String, dynamic> json) => Company(
//         name: json["name"],
//         catchPhrase: json["catchPhrase"],
//         bs: json["bs"],
//     );

//     Map<String, dynamic> toJson() => {
//         "name": name,
//         "catchPhrase": catchPhrase,
//         "bs": bs,
//     };
// }

// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);

// import 'dart:convert';

//----------------------------------------------------------------------------------------------------------------

// List<Post> postFromJson(String str) =>
//     List<Post>.from(json.decode(str).map((x) => Post.fromJson(x)));

// String postToJson(List<Post> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Post {
//   Post({
//     this.id,
//     this.key,
//     this.tags,
//     this.locale,
//     this.author,
//     this.active,
//     this.title,
//     this.content,
//     this.img,
//     this.publicationType,
//     this.titleShort,
//     this.contentShort,
//     this.publicationDate,
//     this.expireDate,
    
//     // this.albumId,
//     // this.id,
//     // this.title,
//     // this.url,
//     // this.thumbnailUrl,
//   });

//   int id;
//   String key;
//   List tags;
//   String locale;
//   String author;
//   String active;
//   String title;
//   String content;
//   String img;
//   String publicationType;
//   String titleShort;
//   String contentShort;
//   String publicationDate;
//   String expireDate;

//   // int albumId;
//   // int id;
//   // String title;
//   // String url;
//   // String thumbnailUrl;

//   factory Post.fromJson(Map<String, dynamic> json) => Post(
//         id: json["id"],
//         key: json["key"],
//         tags: json["tags"],
//         locale: json["locale"],
//         author: json["author"],
//         active: json["active"],
//         title: json["title"],
//         content: json["content"],
//         img: json["img"],
//         publicationType: json["publicationType"],
//         titleShort: json["titleShort"],
//         contentShort: json["contentShort"],
//         publicationDate: json["publicationDate"],
//         expireDate :json["expireDate"],

//         // albumId: json["albumId"],
//         // id: json["id"],
//         // title: json["title"],
//         // url: json["url"],
//         // thumbnailUrl: json["thumbnailUrl"],
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "key": key,
//         "tags": tags,
//         "locale": locale,
//         "author": author,
//         "active": active,
//         "title": title,
//         "content": content,
//         "img": img,
//         "publicationType": publicationType,
//         "titleShort": titleShort,
//         "contentShort": contentShort,
//         "publicationDate": publicationDate,
//         "expireDate": expireDate,
        
//         // "albumId": albumId,
//         // "id": id,
//         // "title": title,
//         // "url": url,
//         // "thumbnailUrl": thumbnailUrl,
//       };
// }

// ------------------------------------------------------------------------------------------------------------------------------
// import '10_profile_news_list_model.dart';
// import 'dart:convert';

// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);


// Post postFromJson(String str) => Post.fromJson(json.decode(str));

// String postToJson(Post data) => json.encode(data.toJson());

// class Post {
//     Post({
//         this.publication,
//         this.page,
//     });

//     List<Publication> publication;
//     Page page;

//     factory Post.fromJson(Map<String, dynamic> json) => Post(
//         publication: List<Publication>.from(json["publication"].map((x) => Publication.fromJson(x))),
//         page: Page.fromJson(json["page"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "publication": List<dynamic>.from(publication.map((x) => x.toJson())),
//         "page": page.toJson(),
//     };
// }

// class Page {
//     Page({
//         this.size,
//         this.itemsOnCurrentPage,
//         this.currentPage,
//         this.totalItems,
//         this.totalPages,
//     });

//     int size;
//     int itemsOnCurrentPage;
//     int currentPage;
//     int totalItems;
//     int totalPages;

//     factory Page.fromJson(Map<String, dynamic> json) => Page(
//         size: json["size"],
//         itemsOnCurrentPage: json["itemsOnCurrentPage"],
//         currentPage: json["currentPage"],
//         totalItems: json["totalItems"],
//         totalPages: json["totalPages"],
//     );

//     Map<String, dynamic> toJson() => {
//         "size": size,
//         "itemsOnCurrentPage": itemsOnCurrentPage,
//         "currentPage": currentPage,
//         "totalItems": totalItems,
//         "totalPages": totalPages,
//     };
// }

// class Publication {
//     Publication({
//         this.id,
//         this.key,
//         this.tags,
//         this.locale,
//         this.author,
//         this.active,
//         this.title,
//         this.content,
//         this.titleShort,
//         this.img,
//         this.publicationType,
//         this.contentShort,
//         this.publicationDate,
//         this.expireDate,
//     });

//     int id;
//     String key;
//     List<Tag> tags;
//     String locale;
//     String author;
//     bool active;
//     String title;
//     String content;
//     String titleShort;
//     String img;
//     PublicationType publicationType;
//     String contentShort;
//     DateTime publicationDate;
//     DateTime expireDate;

//     factory Publication.fromJson(Map<String, dynamic> json) => Publication(
//         id: json["id"],
//         key: json["key"],
//         tags: List<Tag>.from(json["tags"].map((x) => tagValues.map[x])),
//         locale: json["locale"],
//         author: json["author"],
//         active: json["active"],
//         title: json["title"],
//         content: json["content"],
//         titleShort: json["titleShort"],
//         img: json["img"],
//         publicationType: publicationTypeValues.map[json["publicationType"]],
//         contentShort: json["contentShort"],
//         publicationDate: json["publicationDate"] == null ? null : DateTime.parse(json["publicationDate"]),
//         expireDate: DateTime.parse(json["expireDate"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "key": key,
//         "tags": List<dynamic>.from(tags.map((x) => tagValues.reverse[x])),
//         "locale": locale,
//         "author": author,
//         "active": active,
//         "title": title,
//         "content": content,
//         "titleShort": titleShort,
//         "img": img,
//         "publicationType": publicationTypeValues.reverse[publicationType],
//         "contentShort": contentShort,
//         "publicationDate": publicationDate == null ? null : publicationDate.toIso8601String(),
//         "expireDate": expireDate.toIso8601String(),
//     };
// }

// enum Img { THE_20201201_CHARTS_PNG, THE_20201202_DEVELOP_CAT_JPG, EMPTY }

// final imgValues = EnumValues({
//     "": Img.EMPTY,
//     "2020-12-01_charts.png": Img.THE_20201201_CHARTS_PNG,
//     "2020-12-02_develop_cat.jpg": Img.THE_20201202_DEVELOP_CAT_JPG
// });

// enum PublicationType { NEWS, FAQ }

// final publicationTypeValues = EnumValues({
//     "faq": PublicationType.FAQ,
//     "news": PublicationType.NEWS
// });

// enum Tag { EMPTY, CREATED, MODIFIED }

// final tagValues = EnumValues({
//     "created": Tag.CREATED,
//     "": Tag.EMPTY,
//     "modified": Tag.MODIFIED
// });

// class EnumValues<T> {
//     Map<String, T> map;
//     Map<T, String> reverseMap;

//     EnumValues(this.map);

//     Map<T, String> get reverse {
//         if (reverseMap == null) {
//             reverseMap = map.map((k, v) => new MapEntry(v, k));
//         }
//         return reverseMap;
//     }
// }


//------------------------------------------------------4.12.2020--------------------------------------------------------------

import '10_profile_news_list_model.dart';
// import 'dart:convert';

// Post postFromJson(String str) => Post.fromJson(json.decode(str));

// String postToJson(Post data) => json.encode(data.toJson());

// class Post {
//     Post({
//         this.publication,
//         this.page,
//     });

//     List<Publication> publication;
//     Page page;

//     factory Post.fromJson(Map<String, dynamic> json) => Post(
//         publication: List<Publication>.from(json["publication"].map((x) => Publication.fromJson(x))),
//         page: Page.fromJson(json["page"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "publication": List<dynamic>.from(publication.map((x) => x.toJson())),
//         "page": page.toJson(),
//     };
// }

// class Page {
//     Page({
//         this.size,
//         this.itemsOnCurrentPage,
//         this.currentPage,
//         this.totalItems,
//         this.totalPages,
//     });

//     int size;
//     int itemsOnCurrentPage;
//     int currentPage;
//     int totalItems;
//     int totalPages;

//     factory Page.fromJson(Map<String, dynamic> json) => Page(
//         size: json["size"],
//         itemsOnCurrentPage: json["itemsOnCurrentPage"],
//         currentPage: json["currentPage"],
//         totalItems: json["totalItems"],
//         totalPages: json["totalPages"],
//     );

//     Map<String, dynamic> toJson() => {
//         "size": size,
//         "itemsOnCurrentPage": itemsOnCurrentPage,
//         "currentPage": currentPage,
//         "totalItems": totalItems,
//         "totalPages": totalPages,
//     };
// }

// class Publication {
//     Publication({
//         this.id,
//         this.key,
//         this.tags,
//         this.locale,
//         this.author,
//         this.active,
//         this.title,
//         this.content,
//         this.titleShort,
//         this.img,
//         this.publicationType,
//         this.contentShort,
//         this.publicationDate,
//         this.expireDate,
//     });

//     int id;
//     String key;
//     List<Tag> tags;
//     Locale locale;
//     Author author;
//     bool active;
//     String title;
//     String content;
//     String titleShort;
//     String img;
//     PublicationType publicationType;
//     String contentShort;
//     DateTime publicationDate;
//     DateTime expireDate;

//     factory Publication.fromJson(Map<String, dynamic> json) => Publication(
//         id: json["id"],
//         key: json["key"],
//         tags: List<Tag>.from(json["tags"].map((x) => tagValues.map[x])),
//         locale: Locale.fromJson(json["locale"]),
//         author: authorValues.map[json["author"]],
//         active: json["active"],
//         title: json["title"],
//         content: json["content"],
//         titleShort: json["titleShort"],
//         img: json["img"],
//         publicationType: publicationTypeValues.map[json["publicationType"]],
//         contentShort: json["contentShort"],
//         publicationDate: DateTime.parse(json["publicationDate"]),
//         expireDate: DateTime.parse(json["expireDate"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "key": key,
//         "tags": List<dynamic>.from(tags.map((x) => tagValues.reverse[x])),
//         "locale": locale.toJson(),
//         "author": authorValues.reverse[author],
//         "active": active,
//         "title": title,
//         "content": content,
//         "titleShort": titleShort,
//         "img": img,
//         "publicationType": publicationTypeValues.reverse[publicationType],
//         "contentShort": contentShort,
//         "publicationDate": publicationDate.toIso8601String(),
//         "expireDate": expireDate.toIso8601String(),
//     };
// }

// enum Author { MAIN_AUTHOR, UPDATE_AUTHOR, CREATED_VITALII, EMPTY }

// final authorValues = EnumValues({
//     "CreatedVitalii": Author.CREATED_VITALII,
//     "": Author.EMPTY,
//     "Main Author": Author.MAIN_AUTHOR,
//     "UpdateAuthor": Author.UPDATE_AUTHOR
// });

// enum Img { EMPTY, DEFAULT_PNG, CAT_JPEG }

// final imgValues = EnumValues({
//     "cat.jpeg": Img.CAT_JPEG,
//     "default.png": Img.DEFAULT_PNG,
//     "": Img.EMPTY
// });

// class Locale {
//     Locale({
//         this.id,
//         this.name,
//         this.language,
//     });

//     Id id;
//     Name name;
//     Language language;

//     factory Locale.fromJson(Map<String, dynamic> json) => Locale(
//         id: idValues.map[json["id"]],
//         name: nameValues.map[json["name"]],
//         language: languageValues.map[json["language"]],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": idValues.reverse[id],
//         "name": nameValues.reverse[name],
//         "language": languageValues.reverse[language],
//     };
// }

// enum Id { EN_US }

// final idValues = EnumValues({
//     "en_US": Id.EN_US
// });

// enum Language { EN }

// final languageValues = EnumValues({
//     "en": Language.EN
// });

// enum Name { ENG }

// final nameValues = EnumValues({
//     "ENG": Name.ENG
// });

// enum PublicationType { NEWS, FAQ }

// final publicationTypeValues = EnumValues({
//     "faq": PublicationType.FAQ,
//     "news": PublicationType.NEWS
// });

// enum Tag { EMPTY, CREATED, MODIFIED }

// final tagValues = EnumValues({
//     "created": Tag.CREATED,
//     "": Tag.EMPTY,
//     "modified": Tag.MODIFIED
// });

// class EnumValues<T> {
//     Map<String, T> map;
//     Map<T, String> reverseMap;

//     EnumValues(this.map);

//     Map<T, String> get reverse {
//         if (reverseMap == null) {
//             reverseMap = map.map((k, v) => new MapEntry(v, k));
//         }
//         return reverseMap;
//     }
// }

// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);

// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);

import 'dart:convert';

Post postFromJson(String str) => Post.fromJson(json.decode(str));

String postToJson(Post data) => json.encode(data.toJson());

class Post {
    Post({
        this.publication,
        this.page,
    });

    List<Publication> publication;
    Page page;

    factory Post.fromJson(Map<String, dynamic> json) => Post(
        publication: List<Publication>.from(json["publication"].map((x) => Publication.fromJson(x))),
        page: Page.fromJson(json["page"]),
    );

    Map<String, dynamic> toJson() => {
        "publication": List<dynamic>.from(publication.map((x) => x.toJson())),
        "page": page.toJson(),
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class Publication {
    Publication({
        this.id,
        this.active,
        this.deleted,
        this.tagsUa,
        this.position,
        this.color,
        this.cost,
        this.address,
        this.publicationType,
        this.author,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.pageType,
        this.titleShortUa,
        this.titleShortRu,
        this.titleShortEn,
        this.contentUa,
        this.contentRu,
        this.contentEn,
        this.contentShortUa,
        this.contentShortRu,
        this.contentShortEn,
        this.publicationDate,
        this.expireDate,
        this.img,
    });

    int id;
    bool active;
    bool deleted;
    dynamic tagsUa;
    int position;
    String color;
    double cost;
    String address;
    String publicationType;
    String author;
    String titleUa;
    String titleRu;
    String titleEn;
    dynamic pageType;
    dynamic titleShortUa;
    dynamic titleShortRu;
    dynamic titleShortEn;
    String contentUa;
    String contentRu;
    String contentEn;
    String contentShortUa;
    String contentShortRu;
    String contentShortEn;
    DateTime publicationDate;
    DateTime expireDate;
    String img;

    factory Publication.fromJson(Map<String, dynamic> json) => Publication(
        id: json["id"],
        active: json["active"],
        deleted: json["deleted"],
        tagsUa: json["tagsUa"],
        position: json["position"],
        color: json["color"],
        cost: json["cost"],
        address: json["address"],
        publicationType: json["publicationType"],
        author: json["author"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"] == null ? null : json["titleRu"],
        titleEn: json["titleEn"] == null ? null : json["titleEn"],
        pageType: json["page_type"],
        titleShortUa: json["titleShortUa"],
        titleShortRu: json["titleShortRu"],
        titleShortEn: json["titleShortEn"],
        contentUa: json["contentUa"],
        contentRu: json["contentRu"] == null ? null : json["contentRu"],
        contentEn: json["contentEn"] == null ? null : json["contentEn"],
        contentShortUa: json["contentShortUa"],
        contentShortRu: json["contentShortRU"] == null ? null : json["contentShortRU"],
        contentShortEn: json["contentShortEn"] == null ? null : json["contentShortEn"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        expireDate: json["expireDate"] == null ? null : DateTime.parse(json["expireDate"]),
        img: json["img"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "active": active,
        "deleted": deleted,
        "tagsUa": tagsUa,
        "position": position,
        "color": color,
        "cost": cost,
        "address": address,
        "publicationType": publicationType,
        "author": author,
        "titleUa": titleUa,
        "titleRu": titleRu == null ? null : titleRu,
        "titleEn": titleEn == null ? null : titleEn,
        "page_type": pageType,
        "titleShortUa": titleShortUa,
        "titleShortRu": titleShortRu,
        "titleShortEn": titleShortEn,
        "contentUa": contentUa,
        "contentRu": contentRu == null ? null : contentRu,
        "contentEn": contentEn == null ? null : contentEn,
        "contentShortUa": contentShortUa,
        "contentShortRU": contentShortRu == null ? null : contentShortRu,
        "contentShortEn": contentShortEn == null ? null : contentShortEn,
        "publicationDate": publicationDate.toIso8601String(),
        "expireDate": expireDate == null ? null : expireDate.toIso8601String(),
        "img": img,
    };
}







//-----------------------------------------------------------------------------------------------------------------------------

final News news1 = News(
  title: 'Заголовок новость 1',
  title2: '31 декабря 2020',
  text:
      'Краткий текст tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
  image: 'assets/fg_images/10_profile_news_bunner.jpg',
  link: '/10_profile_news.dart',
);

final News news2 = News(
  title: 'Заголовок новость 2',
  title2: '31 декабря 2020',
  text:
      'Краткий текст tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
  image: 'assets/fg_images/10_profile_news_bunner.jpg',
  link: '/10_profile_news.dart',
);

final News news3 = News(
  title: 'Заголовок новость 3',
  title2: '31 декабря 2020',
  text:
      'Краткий текст tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
  image: 'assets/fg_images/10_profile_news_bunner.jpg',
  link: '/10_profile_news.dart',
);

List<News> listOfNews = [
  news1,
  news2,
  news3,
];


//----------------------------------------------------------------------------------------------------