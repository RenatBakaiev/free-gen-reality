import '10_profile_store_model.dart';

final Product product1 = Product(
    image: 'assets/fg_images/10_profile_store_product_1.jpg',
    name: 'DJI Phantom3 МОЖНО КУПИТЬ!',
    // name: 'DJI Phantom3 Professional',
    quantity: 120,
    place: 'Киев, Дарницкий',
    date: '29 января, 01:22',
    favoriteIcon: 'assets/fg_images/10_profile_store_icon_not_favorite.png',
    linkToItemFromStore: '/10_profile_store_product_1.dart');

final Product product2 = Product(
    image: 'assets/fg_images/10_profile_store_product_2.jpg',
    name: 'Long Bord New Professional НЕЛЬЗЯ КУПИТЬ!',
    quantity: 120,
    place: 'Киев, Дарницкий',
    date: '29 января, 01:22',
    favoriteIcon: 'assets/fg_images/10_profile_store_icon_not_favorite.png',
    linkToItemFromStore: '/10_profile_store_product_2.dart');

final Product product3 = Product(
    image: 'assets/fg_images/10_profile_store_product_3.jpg',
    name: 'DJI Phantom3 Professional Electronic',
    quantity: 120,
    place: 'Киев, Дарницкий',
    date: '29 января, 01:22',
    favoriteIcon: 'assets/fg_images/10_profile_store_icon_favorite.png',
    linkToItemFromStore: '/10_profile_store_product_3.dart');

final Product product4 = Product(
    image: 'assets/fg_images/10_profile_store_product_1.jpg',
    name: 'DJI Phantom3 Professional',
    quantity: 120,
    place: 'Киев, Дарницкий',
    date: '29 января, 01:22',
    favoriteIcon: 'assets/fg_images/10_profile_store_icon_not_favorite.png',
    linkToItemFromStore: '/10_profile_store_product_1.dart');

final Product product5 = Product(
    image: 'assets/fg_images/10_profile_store_product_2.jpg',
    name: 'My Product',
    quantity: 120,
    place: 'Киев, Дарницкий',
    date: '29 января, 01:22',
    favoriteIcon: 'assets/fg_images/10_profile_store_icon_not_favorite.png',
    linkToItemFromStore: '/10_profile_store_product_1.dart');

List<Product> storeProducts = [
  product1,
  product2,
  product3,
  product4,
  product5,
];
