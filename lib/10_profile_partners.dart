//---------------------------------------------Partners 4.12.2020----------------------------------------------------------
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import 'package:hexcolor/hexcolor.dart';
import '10_profile_partners_json.dart';

class ProfilePartners extends StatefulWidget {
  @override
  _ProfilePartnersState createState() => _ProfilePartnersState();
}

class _ProfilePartnersState extends State<ProfilePartners> {
  List<Publication> posts;
  bool loading;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  @override
  void initState() {
    super.initState();
    loading = true;
    ServicesPartners.getPartners().then((post) {
      setState(() {
        posts = post.publication;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: Hexcolor('#F2F2F2'),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0)),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#7D5AC2'),
                      ),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(
                        top: 50,
                        // left: 15,
                        // right: 15,
                        bottom: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: SizedBox(
                              // height: 55,
                              // width: 40,
                              child: IconButton(
                                icon: Image.asset(
                                  'assets/fg_images/6_home_search_back.png',
                                  width: 10,
                                  height: 19,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Text(
                            'Партнери',
                            style: TextStyle(
                                fontSize: 37,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: posts.length,
                      itemBuilder: (context, index) {
                        Publication publication = posts[index];

                        var title =
                            utf8.decode(publication.titleUa.runes.toList());
                        var content =
                            utf8.decode(publication.contentUa.runes.toList());

                        return GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(
                                context, '/10_profile_partner_page.dart',
                                arguments: ({
                                  'name': title,
                                  'image': publication.img == "" ||
                                          publication.img == null
                                      ? '$link' + 'default.png'
                                      : '$link' + '${publication.img}',
                                  // 'imageAd': partner.url,
                                  'desc': content,
                                  // 'phone': partner.id,
                                  // 'email': partner.id,
                                  // 'site': partner.id,
                                }));
                          },
                          child: Container(
                            height: 80,
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(
                                bottom: 20, left: 15, right: 15),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Hexcolor(publication.color.toString()),
                              // color: partners[index].backcolor,
                            ),
                            child: Row(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 15),
                                    child: CircleAvatar(
                                        backgroundColor: Colors.transparent,
                                        radius: 23.0,
                                        child: ClipOval(
                                          child: Image.network(
                                            publication.img == "" ||
                                                    publication.img == null
                                                ? '$link' + 'default.png'
                                                : '$link' +
                                                    '${publication.img}',
                                            width: 46,
                                            height: 46,
                                            fit: BoxFit.cover,
                                          ),
                                        ))
                                    //   backgroundImage: NetworkImage(
                                    //       publication.img == "" ||
                                    //               publication.img == null
                                    //           ? '$link' + 'default.png'
                                    //           : '$link' + '${publication.img}',
                                    //           ),
                                    //   backgroundColor: Colors.transparent,
                                    // )
                                    // CircleAvatar(
                                    //   radius: 23.0,
                                    //   backgroundImage: NetworkImage(
                                    //       publication.img == "" ||
                                    //               publication.img == null
                                    //           ? '$link' + 'default.png'
                                    //           : '$link' + '${publication.img}',
                                    //           ),
                                    //   backgroundColor: Colors.transparent,
                                    // )

                                    // CircleAvatar(
                                    //   radius: 23,

                                    //   backgroundColor: Colors.green,
                                    //   // partners[index].backColorImage,
                                    //   child: ClipOval(
                                    //     child: Image.network(
                                    //       // 'http://codeskulptor-demos.commondatastorage.googleapis.com/GalaxyInvaders/alien_1_1.png',
                                    //       publication.img == "" ||
                                    //               publication.img == null
                                    //           ? '$link' + 'default.png'
                                    //           : '$link' + '${publication.img}',
                                    //       // partners[index].image,
                                    //       width: 50,
                                    //       height: 50,
                                    //       fit: BoxFit.cover,
                                    //     ),
                                    //   ),
                                    // ),
                                    ),
                                // Container(
                                // width:
                                // MediaQuery.of(context).size.width - 111,
                                // margin: EdgeInsets.only(left: 20),
                                // child:
                                Flexible(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                      left: 20,
                                    ),
                                    child: Text(
                                      title == null ? '' : title,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      // partners[index].name,
                                      style: TextStyle(
                                        color: Hexcolor('#FFFFFF'),
                                        fontSize: 17,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
