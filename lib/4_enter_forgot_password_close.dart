import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class EnterForgotPasswordClose extends StatefulWidget {
  @override
  _EnterForgotPasswordCloseState createState() =>
      _EnterForgotPasswordCloseState();
}

class _EnterForgotPasswordCloseState extends State<EnterForgotPasswordClose> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Hexcolor('#7D5AC2'),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {},
          child: Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 80,
                          bottom: 30,
                        ),
                        child: Image.asset(
                          'assets/fg_images/4_enter_logo.png',
                          width: 140,
                          height: 126,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              Navigator.pushNamed(
                                  context, '/4_enter_forgot_password');
                            },
                            child: Image.asset(
                              'assets/fg_images/sportGames_icon_arrow_back.png',
                              width: 10,
                              height: 19,
                            ),
                          ),
                          Container(
                            child: Text(
                              'Забули пароль?',
                              style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 25,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Arial',
                                letterSpacing: 1.28,
                              ),
                            ),
                          ),
                          GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {},
                              child: SizedBox(
                                width: 10,
                              )),
                        ],
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          child: Text(
                            'На вказаний Вами e-mail був відправлений лист з посиланням для відновлення пароля.\n\nВи можете відновити пароль на протязі 30 хвилин після натискання кнопки "Відновити".',
                            style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                height: 1.4),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      bottom: 20,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      child: Text("Закрити",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Arial',
                              color: Colors.white,
                              letterSpacing: 1.09,
                              fontSize: 17)),
                      onPressed: () {
                        Navigator.pushNamed(
                            context, '/4_enter_restore_password');
                      },
                    ),
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
