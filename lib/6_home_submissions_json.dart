import 'dart:convert';

SubMissions subMissionsFromJson(String str) => SubMissions.fromJson(json.decode(str));

String subMissionsToJson(SubMissions data) => json.encode(data.toJson());

class SubMissions {
    SubMissions({
        this.submissions,
        this.page,
    });

    List<Submission> submissions;
    Page page;

    factory SubMissions.fromJson(Map<String, dynamic> json) => SubMissions(
        submissions: List<Submission>.from(json["submissions"].map((x) => Submission.fromJson(x))),
        page: Page.fromJson(json["page"]),
    );

    Map<String, dynamic> toJson() => {
        "submissions": List<dynamic>.from(submissions.map((x) => x.toJson())),
        "page": page.toJson(),
    };
}

class Page {
    Page({
        this.size,
        this.itemsOnCurrentPage,
        this.currentPage,
        this.totalItems,
        this.totalPages,
    });

    int size;
    int itemsOnCurrentPage;
    int currentPage;
    int totalItems;
    int totalPages;

    factory Page.fromJson(Map<String, dynamic> json) => Page(
        size: json["size"],
        itemsOnCurrentPage: json["itemsOnCurrentPage"],
        currentPage: json["currentPage"],
        totalItems: json["totalItems"],
        totalPages: json["totalPages"],
    );

    Map<String, dynamic> toJson() => {
        "size": size,
        "itemsOnCurrentPage": itemsOnCurrentPage,
        "currentPage": currentPage,
        "totalItems": totalItems,
        "totalPages": totalPages,
    };
}

class Submission {
    Submission({
        this.autoCheckIn,
        this.activationRadius,
        this.id,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.createdDate,
        this.lastModifiedDate,
        this.location,
        this.imageUrl,
        this.position,
        this.stepList,
        this.currencyType,
        this.points,
        this.active,
    });

    bool autoCheckIn;
    int activationRadius;
    int id;
    String titleUa;
    String titleRu;
    String titleEn;
    DateTime createdDate;
    DateTime lastModifiedDate;
    Location location;
    String imageUrl;
    int position;
    List<StepList> stepList;
    String currencyType;
    double points;
    bool active;

    factory Submission.fromJson(Map<String, dynamic> json) => Submission(
        autoCheckIn: json["autoCheckIn"],
        activationRadius: json["activationRadius"],
        id: json["id"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        location: json["location"] == null ? null : Location.fromJson(json["location"]),
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
        position: json["position"],
        stepList: List<StepList>.from(json["stepList"].map((x) => StepList.fromJson(x))),
        currencyType: json["currencyType"],
        points: json["points"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "autoCheckIn": autoCheckIn,
        "activationRadius": activationRadius,
        "id": id,
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "location": location == null ? null : location.toJson(),
        "imageUrl": imageUrl == null ? null : imageUrl,
        "position": position,
        "stepList": List<dynamic>.from(stepList.map((x) => x.toJson())),
        "currencyType": currencyType,
        "points": points,
        "active": active,
    };
}

class Location {
    Location({
        this.id,
        this.latitude,
        this.longitude,
        this.active,
    });

    int id;
    String latitude;
    String longitude;
    bool active;

    factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        active: json["active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "active": active,
    };
}

class StepList {
    StepList({
        this.id,
        this.stepType,
        this.titleUa,
        this.titleRu,
        this.titleEn,
        this.createdDate,
        this.lastModifiedDate,
        this.location,
        this.descriptionUa,
        this.descriptionRu,
        this.descriptionEn,
        this.imageUrl,
        this.audioUrl,
        this.videoUrl,
        this.position,
        this.currencyType,
        this.points,
        this.activationRadius,
        this.correctAnswer,
        this.currencyTypeWrongAnswer,
        this.pointsWrongAnswer,
        this.imageScan,
        this.scanType,
        this.canBeSkipped,
        this.active,
        this.modelList,
        this.geolocationList,
    });

    int id;
    String stepType;
    String titleUa;
    String titleRu;
    String titleEn;
    DateTime createdDate;
    DateTime lastModifiedDate;
    dynamic location;
    String descriptionUa;
    String descriptionRu;
    String descriptionEn;
    dynamic imageUrl;
    dynamic audioUrl;
    dynamic videoUrl;
    int position;
    String currencyType;
    double points;
    dynamic activationRadius;
    dynamic correctAnswer;
    String currencyTypeWrongAnswer;
    dynamic pointsWrongAnswer;
    String imageScan;
    String scanType;
    bool canBeSkipped;
    bool active;
    List<dynamic> modelList;
    List<dynamic> geolocationList;

    factory StepList.fromJson(Map<String, dynamic> json) => StepList(
        id: json["id"],
        stepType: json["stepType"],
        titleUa: json["titleUa"],
        titleRu: json["titleRu"],
        titleEn: json["titleEn"],
        createdDate: DateTime.parse(json["createdDate"]),
        lastModifiedDate: DateTime.parse(json["lastModifiedDate"]),
        location: json["location"],
        descriptionUa: json["descriptionUa"],
        descriptionRu: json["descriptionRu"],
        descriptionEn: json["descriptionEn"],
        imageUrl: json["imageUrl"],
        audioUrl: json["audioUrl"],
        videoUrl: json["videoUrl"],
        position: json["position"],
        currencyType: json["currencyType"],
        points: json["points"],
        activationRadius: json["activationRadius"],
        correctAnswer: json["correctAnswer"],
        currencyTypeWrongAnswer: json["currencyTypeWrongAnswer"],
        pointsWrongAnswer: json["pointsWrongAnswer"],
        imageScan: json["imageScan"],
        scanType: json["scanType"],
        canBeSkipped: json["canBeSkipped"],
        active: json["active"],
        modelList: List<dynamic>.from(json["modelList"].map((x) => x)),
        geolocationList: List<dynamic>.from(json["geolocationList"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "stepType": stepType,
        "titleUa": titleUa,
        "titleRu": titleRu,
        "titleEn": titleEn,
        "createdDate": createdDate.toIso8601String(),
        "lastModifiedDate": lastModifiedDate.toIso8601String(),
        "location": location,
        "descriptionUa": descriptionUa,
        "descriptionRu": descriptionRu,
        "descriptionEn": descriptionEn,
        "imageUrl": imageUrl,
        "audioUrl": audioUrl,
        "videoUrl": videoUrl,
        "position": position,
        "currencyType": currencyType,
        "points": points,
        "activationRadius": activationRadius,
        "correctAnswer": correctAnswer,
        "currencyTypeWrongAnswer": currencyTypeWrongAnswer,
        "pointsWrongAnswer": pointsWrongAnswer,
        "imageScan": imageScan,
        "scanType": scanType,
        "canBeSkipped": canBeSkipped,
        "active": active,
        "modelList": List<dynamic>.from(modelList.map((x) => x)),
        "geolocationList": List<dynamic>.from(geolocationList.map((x) => x)),
    };
}
