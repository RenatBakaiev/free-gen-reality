class LastRequest {
  final String request;
  bool isSelected;

  LastRequest({
    this.request,
    this.isSelected,
  });
}

class Quest {
  final String quest;
  bool isSelected;

  Quest({
    this.quest,
    this.isSelected,
  });
}
