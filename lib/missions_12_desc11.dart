import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:video_player/video_player.dart';

class Missions12Desc11 extends StatefulWidget {
  @override
  _Missions12Desc11State createState() => _Missions12Desc11State();
}

class _Missions12Desc11State extends State<Missions12Desc11> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
        'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_video_quiz11.mp4');
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    _controller.play();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 80
                    //  - 85
                    ,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            // height: 340,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0)),
                              // image: DecorationImage(
                              //   image: NetworkImage(
                              //       'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-23_zz12_missions_desc11.gif'),
                              //   fit: BoxFit.cover,
                              // )
                            ),
                            child: GestureDetector(
                              onTap: () {
                                if (_controller.value.isPlaying) {
                                  _controller.pause();
                                } else {
                                  _controller.play();
                                }
                              },
                              child: FutureBuilder(
                                future: _initializeVideoPlayerFuture,
                                builder: (context, snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    return AspectRatio(
                                      aspectRatio:
                                          _controller.value.aspectRatio,
                                      child: VideoPlayer(_controller),
                                    );
                                  } else {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                },
                              ),
                            ),
                            // Container(
                            //   margin: EdgeInsets.only(
                            //     bottom: 20,
                            //     right: 20,
                            //   ),
                            //   child: Column(
                            //     mainAxisAlignment:
                            //         MainAxisAlignment.spaceBetween,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Row(
                            //         mainAxisAlignment:
                            //             MainAxisAlignment.spaceBetween,
                            //         crossAxisAlignment:
                            //             CrossAxisAlignment.center,
                            //         children: [
                            //           Container(),
                            //           GestureDetector(
                            //             behavior: HitTestBehavior.opaque,
                            //             onTap: () {
                            //               //  Navigator.pushNamed(context, '/player_video.dart');
                            //             },
                            //             child: Container(
                            //               margin: EdgeInsets.only(
                            //                 top: 30,
                            //               ),
                            //               // child: Image.asset(
                            //               //   'assets/fg_images/player_video_start.png',
                            //               //   height: 48,
                            //               //   width: 48,
                            //               // ),
                            //             ),
                            //           ),
                            //         ],
                            //       ),
                            //       // Row(
                            //       //   mainAxisAlignment: MainAxisAlignment.end,
                            //       //   crossAxisAlignment: CrossAxisAlignment.end,
                            //       //   children: [
                            //       //     Text(
                            //       //       '1',
                            //       //       style: TextStyle(
                            //       //         fontSize: 36,
                            //       //         color: Colors.white,
                            //       //         fontFamily: 'Arial',
                            //       //         fontWeight: FontWeight.w700,
                            //       //       ),
                            //       //     ),
                            //       //     Text(
                            //       //       ' ' + '/ ' + '12',
                            //       //       style: TextStyle(
                            //       //         fontSize: 24,
                            //       //         color: Colors.white,
                            //       //         fontFamily: 'Arial',
                            //       //         fontWeight: FontWeight.w400,
                            //       //       ),
                            //       //     ),
                            //       //   ],
                            //       // ),
                            //     ],
                            //   ),
                            // ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Опис',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Хоч би що, головне - щоб було радісно в душі)\nПройдемося і розкриємо країни?\n\nВесело зі слонами обливаються - жителі ' +
                                        "М'янми" +
                                        '. Вважається, що чим більше води проллється на голову щасливчика, тим радісніше для нього буде прийдешній рік.\n\nА 21 березня, в день весняного рівнодення, башкири, татари, тюркомовні народи Казахстану, Середньої та Малої Азії, Ірану відзначають Новий рік за східним календарем. А ще колись, в стародавні часи, східні ' +
                                        "слов'яни" +
                                        ' вважали 1 березня початком Нового року. Саме на початку весни потихеньку прокидалась природа, а люди приступали до підготовки до польових робіт.\n\nКидатися речами з вікон - італійська традиція. І хоча ризик зустріти на бруківці піаніно мінімальний, все ж варто проявити деяку пильність. Втім, є і менш небезпечні традиції: італійці вірять, що якщо зустріти свято в червоному (нехай це будуть хоча б і шкарпетки), то наступні 365 днів вам по життю горить, навпаки, зелене світло.\n\nУ храмах Японії дзвони дзвонять рівно 108 разів, тому що японці вірять, що кожний удар дзвону позбавляє людину від вад: жадібності, заздрості, нерішучості, дурості, легковажності й жадібності. Але у кожного з вад є 18 підвидів, - множимо, - виходить 108 раз. Після того, як дзвін стихне, слово бере друга традиція - сміятися, веселитися і жартувати від душі аж до сходу.',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: SizedBox(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_search_back.png',
                                    width: 10,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    if (_controller.value.isPlaying) {
                                      _controller.pause();
                                    }
                                    Navigator.pushNamed(
                                        context, '/missions_12_quiz11.dart');
                                  },
                                ),
                              ),
                            ),
                            Text(
                              'Інформація',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/run_map_quest_pic_gostart.png',
                                width: 22,
                                height: 24,
                              ),
                              onPressed: () {
                                if (_controller.value.isPlaying) {
                                  _controller.pause();
                                }
                                Navigator.pushNamed(
                                  context,
                                  '/missions_12_desc.dart',
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Далі",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () {
                  if (_controller.value.isPlaying) {
                    _controller.pause();
                  }
                  Navigator.pushNamed(context, '/missions_12_scan_info11.dart');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
