// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
    User({
        this.id,
        this.access,
        this.userId,
        this.firstName,
        this.lastName,
        this.fullName,
        this.username,
        this.email,
        this.phone,
        this.country,
        this.city,
        this.birthDate,
        this.gender,
        this.profileImageUrl,
        this.joinDate,
        this.lastLoginDate,
        this.lastLoginDateDisplay,
        this.role,
        this.status,
        this.registrationType,
        this.authorities,
        this.isActive,
        this.isNotLocked,
        this.accounts,
    });

    int id;
    dynamic access;
    String userId;
    String firstName;
    String lastName;
    dynamic fullName;
    String username;
    String email;
    String phone;
    String country;
    String city;
    DateTime birthDate;
    String gender;
    String profileImageUrl;
    DateTime joinDate;
    DateTime lastLoginDate;
    DateTime lastLoginDateDisplay;
    String role;
    String status;
    dynamic registrationType;
    List<String> authorities;
    bool isActive;
    bool isNotLocked;
    List<dynamic> accounts;

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        access: json["access"],
        userId: json["userId"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        fullName: json["fullName"],
        username: json["username"],
        email: json["email"],
        phone: json["phone"],
        country: json["country"],
        city: json["city"],
        birthDate: DateTime.parse(json["birthDate"]),
        gender: json["gender"],
        profileImageUrl: json["profileImageUrl"],
        joinDate: DateTime.parse(json["joinDate"]),
        lastLoginDate: DateTime.parse(json["lastLoginDate"]),
        lastLoginDateDisplay: DateTime.parse(json["lastLoginDateDisplay"]),
        role: json["role"],
        status: json["status"],
        registrationType: json["registrationType"],
        authorities: List<String>.from(json["authorities"].map((x) => x)),
        isActive: json["isActive"],
        isNotLocked: json["isNotLocked"],
        accounts: List<dynamic>.from(json["accounts"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "access": access,
        "userId": userId,
        "firstName": firstName,
        "lastName": lastName,
        "fullName": fullName,
        "username": username,
        "email": email,
        "phone": phone,
        "country": country,
        "city": city,
        "birthDate": birthDate.toIso8601String(),
        "gender": gender,
        "profileImageUrl": profileImageUrl,
        "joinDate": joinDate.toIso8601String(),
        "lastLoginDate": lastLoginDate.toIso8601String(),
        "lastLoginDateDisplay": lastLoginDateDisplay.toIso8601String(),
        "role": role,
        "status": status,
        "registrationType": registrationType,
        "authorities": List<dynamic>.from(authorities.map((x) => x)),
        "isActive": isActive,
        "isNotLocked": isNotLocked,
        "accounts": List<dynamic>.from(accounts.map((x) => x)),
    };
}
