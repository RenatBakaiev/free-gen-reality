import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
// import '10_profile.dart';
import '10_profile_json.dart';
// import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'ProgressHUD.dart';

class ProfileEdit extends StatefulWidget {
  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController secondNameTextEditingController =
      new TextEditingController();
  TextEditingController nickNameTextEditingController =
      new TextEditingController();
  TextEditingController birthdayTextEditingController =
      new TextEditingController();
  TextEditingController cityTextEditingController = new TextEditingController();
  TextEditingController countryTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  // oldPasswordTextEditingController
  TextEditingController oldPasswordTextEditingController =
      new TextEditingController();
  TextEditingController newPasswordTextEditingController =
      new TextEditingController();
  TextEditingController repeatNewPasswordTextEditingController =
      new TextEditingController();

  var controller = new MaskedTextController(mask: '+380(00) 000-00-00');

  final formKey = GlobalKey<FormState>();
  final formKeyForPasswords = GlobalKey<FormState>();

  var _blankFocusNode = new FocusNode();

  bool isNotificationsSelected = false;
  bool isManSelected = false;
  bool isWomanSelected = false;
  bool isSexSelected = false;
  bool makeErrorSex = false;
  String sex = 'Вкажіть вашу стать';
  bool isVisibleOldPassword = true;
  bool isVisibleNewPassword = false;
  bool isVisibleConfirmNewPassword = false;
  bool deleteImageNow = false;

  changeSex() {
    if (isManSelected) {
      setState(() {
        sex = 'Чоловіча';
      });
    }
    if (isWomanSelected) {
      setState(() {
        sex = 'Жіноча';
      });
    }
  }

  showWindowPasswordChanged() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Пароль змінено!',
                          style: TextStyle(
                            color: Hexcolor('#59B32D'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви успішно поміняли пароль',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#59B32D'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

    showWindowPasswordError() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Пароль не змінено!',
                          style: TextStyle(
                            color: Hexcolor('#FF1E1E'),
                            fontSize: 24,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        'Ви ввели невірний старий пароль',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Hexcolor('#FF1E1E'),
                          fontSize: 16,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                          height: 1.4,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        height: 60,
                        width: MediaQuery.of(context).size.width - 80,
                        child: RaisedButton(
                          child: Text(
                            "Закрити",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.05),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                            // Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  bool isApiCallProcess = false;
  savePassword() async {
    if (formKeyForPasswords.currentState.validate()) {
      // oldPasswordTextEditingController.clear();
      // newPasswordTextEditingController.clear();
      // repeatNewPasswordTextEditingController.clear();
      //

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      String token = sharedPreferences.get("token");
      String email = sharedPreferences.get("email");
      var oldPassword = oldPasswordTextEditingController.text;
      var newPassword = newPasswordTextEditingController.text;
      setState(() {
        isLoading = true;
      });
      String url =
          'http://generation-admin.ehub.com.ua/api/user/changePassword';
          
      final http.Response response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(<String, dynamic>{
          "email": email,
          "old_password": oldPassword,
          "new_password": newPassword,
        }),
      );
      print(email);
      print(oldPassword);
      print(newPassword);
      print(token);
      print(response.statusCode);
      if (response.statusCode == 200) {
        setState(() {
          isLoading = false;
        });
        print('Password is changed');
        showWindowPasswordChanged();
        oldPasswordTextEditingController.clear();
        newPasswordTextEditingController.clear();
        repeatNewPasswordTextEditingController.clear();
      } else {
        setState(() {
          isLoading = false;
        });
        print('Error to change password');
        showWindowPasswordError();
      }
    }
  }

  changePassword() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            var _blankFocusNode2 = new FocusNode();

            return isLoading
                ? CircularProgressIndicator(
                    backgroundColor: Hexcolor('#7D5AC2'),
                    valueColor: new AlwaysStoppedAnimation<Color>(
                      Hexcolor('#FE6802'),
                    ),
                  )
                : Stack(
                    overflow: Overflow.visible,
                    children: [
                      Positioned(
                          bottom: 0,
                          child: Dialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            )),
                            insetPadding: EdgeInsets.only(
                              left: 0,
                              right: 0,
                            ),
                            child: GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(_blankFocusNode2);
                              },
                              child: Container(
                                width: width,
                                height: 465,
                                decoration: BoxDecoration(
                                  color: Hexcolor('#FFFFFF'),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20.0),
                                      topRight: Radius.circular(20.0)),
                                ),
                                child: Container(
                                  margin: EdgeInsets.only(
                                    right: 20,
                                    left: 20,
                                    bottom: 20,
                                    top: 20,
                                  ),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Image.asset(
                                                'assets/fg_images/6_home_filter_close.png',
                                                color: Hexcolor('#9B9B9B'),
                                                width: 22,
                                                height: 22,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 19,
                                            ),
                                            Text(
                                              "Змінити пароль",
                                              style: TextStyle(
                                                color: Hexcolor('#1E2E45'),
                                                fontSize: 18,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: SingleChildScrollView(
                                            physics: BouncingScrollPhysics(),
                                            child: Form(
                                              key: formKeyForPasswords,
                                              child: Column(children: [
                                                SizedBox(
                                                  height: 25,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        bottom: 7,
                                                      ),
                                                      child: Text(
                                                        "Старий пароль",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily: 'Arial',
                                                          color: Hexcolor(
                                                              '#9B9B9B'),
                                                          fontSize: 17,
                                                        ),
                                                      ),
                                                    ),
                                                    Stack(
                                                      overflow:
                                                          Overflow.visible,
                                                      children: [
                                                        TextFormField(
                                                          onChanged: (val) {},
                                                          obscuringCharacter:
                                                              '*',
                                                          obscureText:
                                                              isVisibleOldPassword
                                                                  ? false
                                                                  : true,
                                                          validator: (value) {
                                                            return value.length >
                                                                    0
                                                                ? null
                                                                : "Будь-ласка введіть старий пароль";
                                                          },
                                                          controller:
                                                              oldPasswordTextEditingController,
                                                          textAlign:
                                                              TextAlign.left,
                                                          decoration:
                                                              InputDecoration(
                                                            hintText:
                                                                "Введіть старий пароль",
                                                            contentPadding:
                                                                new EdgeInsets
                                                                    .only(
                                                              left: 20,
                                                              top: 21.5,
                                                              bottom: 21.5,
                                                            ),
                                                            hintStyle:
                                                                TextStyle(
                                                              color: Hexcolor(
                                                                  '#D3D3D3'),
                                                              fontSize: 17,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontFamily:
                                                                  'Arial',
                                                            ),
                                                            focusedBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color: Hexcolor(
                                                                    '#EB5C18'),
                                                                width: 1,
                                                              ),
                                                            ),
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color: Hexcolor(
                                                                    '#E6E6E6'),
                                                                width: 1,
                                                              ),
                                                            ),
                                                            focusedErrorBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color:
                                                                    Colors.red,
                                                                width: 1,
                                                              ),
                                                            ),
                                                            errorBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color:
                                                                    Colors.red,
                                                                width: 1,
                                                              ),
                                                            ),
                                                            fillColor:
                                                                Colors.white,
                                                            filled: true,
                                                          ),
                                                          style: TextStyle(
                                                            color: Hexcolor(
                                                                '#1E2E45'),
                                                            fontSize: 17,
                                                            fontFamily: 'Arial',
                                                            fontWeight:
                                                                FontWeight.w400,
                                                          ),
                                                        ),
                                                        Positioned(
                                                          right: 22.4,
                                                          top: 18,
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              setState(() {
                                                                isVisibleOldPassword =
                                                                    !isVisibleOldPassword;
                                                              });
                                                            },
                                                            child:
                                                                !isVisibleOldPassword
                                                                    ? Image
                                                                        .asset(
                                                                        'assets/fg_images/4_enter_visible.png',
                                                                        color: Hexcolor(
                                                                            '#9B9B9B'),
                                                                        width:
                                                                            24,
                                                                        height:
                                                                            24,
                                                                      )
                                                                    : Image
                                                                        .asset(
                                                                        'assets/fg_images/4_enter_hidden.png',
                                                                        color: Hexcolor(
                                                                            '#9B9B9B'),
                                                                        width:
                                                                            24,
                                                                        height:
                                                                            24,
                                                                      ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        bottom: 7,
                                                      ),
                                                      child: Text(
                                                        "Новий пароль",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily: 'Arial',
                                                          color: Hexcolor(
                                                              '#9B9B9B'),
                                                          fontSize: 17,
                                                        ),
                                                      ),
                                                    ),
                                                    Stack(
                                                      overflow:
                                                          Overflow.visible,
                                                      children: [
                                                        TextFormField(
                                                          onChanged: (val) {},
                                                          obscuringCharacter:
                                                              '*',
                                                          obscureText:
                                                              isVisibleNewPassword
                                                                  ? false
                                                                  : true,
                                                          validator: (value) {
                                                            if (value.length <
                                                                6) {
                                                              return "Будь-ласка введіть пароль не менше 6 символів";
                                                            }
                                                            if (newPasswordTextEditingController
                                                                    .text !=
                                                                repeatNewPasswordTextEditingController
                                                                    .text) {
                                                              return "Паролі не співпадають";
                                                            }
                                                            return null;
                                                          },
                                                          controller:
                                                              newPasswordTextEditingController,
                                                          textAlign:
                                                              TextAlign.left,
                                                          decoration:
                                                              InputDecoration(
                                                            hintText:
                                                                "Введіть новий пароль",
                                                            contentPadding:
                                                                new EdgeInsets
                                                                    .only(
                                                              left: 20,
                                                              top: 21.5,
                                                              bottom: 21.5,
                                                            ),
                                                            hintStyle:
                                                                TextStyle(
                                                              color: Hexcolor(
                                                                  '#D3D3D3'),
                                                              fontSize: 17,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontFamily:
                                                                  'Arial',
                                                            ),
                                                            focusedBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color: Hexcolor(
                                                                    '#EB5C18'),
                                                                width: 1,
                                                              ),
                                                            ),
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color: Hexcolor(
                                                                    '#E6E6E6'),
                                                                width: 1,
                                                              ),
                                                            ),
                                                            focusedErrorBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color:
                                                                    Colors.red,
                                                                width: 1,
                                                              ),
                                                            ),
                                                            errorBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color:
                                                                    Colors.red,
                                                                width: 1,
                                                              ),
                                                            ),
                                                            fillColor:
                                                                Colors.white,
                                                            filled: true,
                                                          ),
                                                          style: TextStyle(
                                                            color: Hexcolor(
                                                                '#1E2E45'),
                                                            fontSize: 17,
                                                            fontFamily: 'Arial',
                                                            fontWeight:
                                                                FontWeight.w400,
                                                          ),
                                                        ),
                                                        Positioned(
                                                          right: 22.4,
                                                          top: 18,
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              setState(() {
                                                                isVisibleNewPassword =
                                                                    !isVisibleNewPassword;
                                                              });
                                                            },
                                                            child:
                                                                !isVisibleNewPassword
                                                                    ? Image
                                                                        .asset(
                                                                        'assets/fg_images/4_enter_visible.png',
                                                                        color: Hexcolor(
                                                                            '#9B9B9B'),
                                                                        width:
                                                                            24,
                                                                        height:
                                                                            24,
                                                                      )
                                                                    : Image
                                                                        .asset(
                                                                        'assets/fg_images/4_enter_hidden.png',
                                                                        color: Hexcolor(
                                                                            '#9B9B9B'),
                                                                        width:
                                                                            24,
                                                                        height:
                                                                            24,
                                                                      ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        bottom: 7,
                                                      ),
                                                      child: Text(
                                                        "Підтвердіть пароль",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily: 'Arial',
                                                          color: Hexcolor(
                                                              '#9B9B9B'),
                                                          fontSize: 17,
                                                        ),
                                                      ),
                                                    ),
                                                    Stack(
                                                      overflow:
                                                          Overflow.visible,
                                                      children: [
                                                        TextFormField(
                                                          onChanged: (val) {},
                                                          obscuringCharacter:
                                                              '*',
                                                          obscureText:
                                                              isVisibleConfirmNewPassword
                                                                  ? false
                                                                  : true,
                                                          validator: (value) {
                                                            if (value.length <
                                                                6) {
                                                              return "Будь-ласка введіть пароль не менше 6 символів";
                                                            }
                                                            if (newPasswordTextEditingController
                                                                    .text !=
                                                                repeatNewPasswordTextEditingController
                                                                    .text) {
                                                              return "Паролі не співпадають";
                                                            }
                                                            return null;
                                                          },
                                                          controller:
                                                              repeatNewPasswordTextEditingController,
                                                          textAlign:
                                                              TextAlign.left,
                                                          decoration:
                                                              InputDecoration(
                                                            hintText:
                                                                "Підтвердіть новий пароль",
                                                            contentPadding:
                                                                new EdgeInsets
                                                                    .only(
                                                              left: 20,
                                                              top: 21.5,
                                                              bottom: 21.5,
                                                            ),
                                                            hintStyle:
                                                                TextStyle(
                                                              color: Hexcolor(
                                                                  '#D3D3D3'),
                                                              fontSize: 17,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontFamily:
                                                                  'Arial',
                                                            ),
                                                            focusedBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color: Hexcolor(
                                                                    '#EB5C18'),
                                                                width: 1,
                                                              ),
                                                            ),
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color: Hexcolor(
                                                                    '#E6E6E6'),
                                                                width: 1,
                                                              ),
                                                            ),
                                                            focusedErrorBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color:
                                                                    Colors.red,
                                                                width: 1,
                                                              ),
                                                            ),
                                                            errorBorder:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          14.0),
                                                              borderSide:
                                                                  BorderSide(
                                                                color:
                                                                    Colors.red,
                                                                width: 1,
                                                              ),
                                                            ),
                                                            fillColor:
                                                                Colors.white,
                                                            filled: true,
                                                          ),
                                                          style: TextStyle(
                                                            color: Hexcolor(
                                                                '#1E2E45'),
                                                            fontSize: 17,
                                                            fontFamily: 'Arial',
                                                            fontWeight:
                                                                FontWeight.w400,
                                                          ),
                                                        ),
                                                        Positioned(
                                                          right: 22.4,
                                                          top: 18,
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              setState(() {
                                                                isVisibleConfirmNewPassword =
                                                                    !isVisibleConfirmNewPassword;
                                                              });
                                                            },
                                                            child:
                                                                !isVisibleConfirmNewPassword
                                                                    ? Image
                                                                        .asset(
                                                                        'assets/fg_images/4_enter_visible.png',
                                                                        color: Hexcolor(
                                                                            '#9B9B9B'),
                                                                        width:
                                                                            24,
                                                                        height:
                                                                            24,
                                                                      )
                                                                    : Image
                                                                        .asset(
                                                                        'assets/fg_images/4_enter_hidden.png',
                                                                        color: Hexcolor(
                                                                            '#9B9B9B'),
                                                                        width:
                                                                            24,
                                                                        height:
                                                                            24,
                                                                      ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ]),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 60,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: RaisedButton(
                                            color: Hexcolor('#FE6802'),
                                            shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      14.0),
                                            ),
                                            child: Text("Зберегти",
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.white,
                                                    letterSpacing: 1.09,
                                                    fontSize: 17)),
                                            onPressed: () {
                                              savePassword();
                                            },
                                          ),
                                        ),
                                      ]),
                                ),
                              ),
                            ),
                          ))
                    ],
                  );
          });
        });
  }

  File _pickedImage; // https://www.youtube.com/watch?v=JvxelHxw8zw
  // String _pickedImage2;
  // PickedFile _imageFile;
  // final ImagePicker _picker = ImagePicker();

  // File imageFile;
  // final picker = ImagePicker();
  // _openPhoneGallery() async {
  //   var picture = await picker.getImage(source: ImageSource.gallery);
  //   this.setState(() {
  //     imageFile = File(picture.path);
  //   });
  // }

  Widget bottomSheet() {
    return Container(
      decoration: BoxDecoration(
        color: Hexcolor('#5A497B'),
      ),
      height: 100,
      width: MediaQuery.of(context).size.width,
      // margin: EdgeInsets.symmetric(
      //   horizontal: 20,
      //   vertical: 20,
      // ),
      child: Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Text('Виберіть фото профіля',
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Arial',
                  fontWeight: FontWeight.w600,
                  color: Hexcolor('#FE6802'))),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton.icon(
                  onPressed: () {
                    // takePhoto(ImageSource.camera);
                  },
                  icon: Icon(
                    Icons.camera,
                    color: Colors.white,
                  ),
                  label: Text(
                    'Камера',
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                        color: Hexcolor('#FE6802')),
                  )),
              FlatButton.icon(
                  onPressed: () {
                    // takePhoto(ImageSource.gallery);
                  },
                  icon: Icon(
                    Icons.image,
                    color: Colors.white,
                  ),
                  label: Text(
                    'Галерея',
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w600,
                        color: Hexcolor('#FE6802')),
                  ))
            ],
          ),
        ],
      ),
    );
  }

  _loadPicker(ImageSource source) async {
    File picked = await ImagePicker.pickImage(source: source);
    if (picked != null) {
      _cropImage(picked);
    }
    Navigator.pop(context);
  }

  //   Future<User> savePhoto(cropped) async {
  //   setState(() {
  //     isLoading = true;
  //   });
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   String token = sharedPreferences.get("token");
  //   int id = sharedPreferences.get("id");
  //   String url = 'http://generation-admin.ehub.com.ua/api/user/update';

  //   final http.Response response = await http.put(
  //     url,
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json',
  //       'Authorization': 'Bearer $token',
  //     },
  //     body: jsonEncode(<String, dynamic>{
  //       "id": id,
  //       "profileImageUrl": cropped,
  //     }),
  //   );
  //   print(response.statusCode);
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       isLoading = false;
  //     });
  //     print(response.body);
  //   } else {
  //     setState(() {
  //       isLoading = false;
  //     });
  //     throw Exception('Failed to save photo.');
  //   }
  // }

  _cropImage(File picked) async {
    File cropped = await ImageCropper.cropImage(
      androidUiSettings: AndroidUiSettings(
        statusBarColor: Colors.black,
        toolbarColor: Hexcolor('#FE6802'),
        toolbarTitle: 'Обрізка зображення',
        toolbarWidgetColor: Hexcolor('#FFFFFF'),
        lockAspectRatio: false,
        backgroundColor: Hexcolor('#7D5AC2'),
        activeControlsWidgetColor: Colors.cyanAccent,
        // dimmedLayerColor: Colors.yellow,
        cropFrameColor: Hexcolor('#FE6802'),
        // cropGridColor: Colors.pink,
        cropFrameStrokeWidth: 2,
        // cropGridStrokeWidth: 200,
        hideBottomControls: true,
        cropGridRowCount: 1,
        cropGridColumnCount: 1,
        showCropGrid: false,
      ),
      // compressQuality: 100,
      sourcePath: picked.path,
      // cropStyle: CropStyle.circle,
      cropStyle: CropStyle.circle,
      aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
      // aspectRatioPresets: [
      //   CropAspectRatioPreset.square,
      //   CropAspectRatioPreset.ratio3x2,
      //   CropAspectRatioPreset.original,
      //   CropAspectRatioPreset.ratio4x3,
      //   CropAspectRatioPreset.ratio16x9
      // ],
      // maxWidth: 300,
      // maxHeight: 300,
    );
    if (cropped != null) {
      setState(() {
        _pickedImage = cropped;
      });
      // savePhoto(cropped);
    }
  }

  Future<http.StreamedResponse> patchImage(String url, String filepath) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files
        .add(await http.MultipartFile.fromPath("profileImage", filepath));
    request.headers.addAll({
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    });
    var response = request.send();
    return response;
  }

  Future<http.StreamedResponse> deleteImage(String url, String file) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(http.MultipartFile.fromBytes(
        "profileImage", await File.fromUri(Uri.parse(file)).readAsBytes()));
    // request.files.add(new http.MultipartFile.fromBytes('file', await File.fromUri("<path/to/file>").readAsBytes(), contentType: new MediaType('image', 'jpeg')))
    request.headers.addAll({
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    });
    var response = request.send();
    return response;
  }

  // void takePhoto(ImageSource source) async {
  //   final pickedFile = await _picker.getImage(
  //     source: source,
  //   );
  //   setState(() {
  //     _imageFile = pickedFile;
  //   });
  // }

  // void deletePhoto() {
  //   setState(() {
  //     _imageFile = null;
  //   });
  // }

  void deletePhoto() {
    setState(() {
      _pickedImage = null;
    });
  }

  chooseSex() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 235,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Виберіть стать",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isWomanSelected = false;
                                      isManSelected = true;
                                      makeErrorSex = false;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isManSelected
                                            ? 'assets/fg_images/10_profile_questionaire_sex_selected.png'
                                            : 'assets/fg_images/10_profile_questionaire_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Чоловіча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    setState(() {
                                      isWomanSelected = true;
                                      isManSelected = false;
                                      makeErrorSex = false;
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Image.asset(
                                        isWomanSelected
                                            ? 'assets/fg_images/10_profile_questionaire_sex_selected.png'
                                            : 'assets/fg_images/10_profile_questionaire_sex_unselected.png',
                                        width: 18,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: 19,
                                      ),
                                      Text(
                                        "Жіноча",
                                        style: TextStyle(
                                          color: Hexcolor('#1E2E45'),
                                          fontSize: 17,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 60,
                                  width: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(14.0),
                                    ),
                                    child: Text("Зберегти",
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      if ((isManSelected || isWomanSelected) ||
                                          (isManSelected && isWomanSelected)) {
                                        setState(() {
                                          isSexSelected = true;
                                        });
                                        print('sex is selected!');
                                        Navigator.of(context).pop();
                                      }
                                      changeSex();
                                    },
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  changeProfilePhoto() {
    showDialog(
        barrierDismissible: false,
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            var width = MediaQuery.of(context).size.width;
            return Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                    bottom: 0,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                      insetPadding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                      ),
                      child: Container(
                        width: width,
                        height: 165,
                        decoration: BoxDecoration(
                          color: Hexcolor('#FFFFFF'),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 20,
                            left: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Image.asset(
                                        'assets/fg_images/6_home_filter_close.png',
                                        color: Hexcolor('#9B9B9B'),
                                        width: 22,
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 19,
                                    ),
                                    Text(
                                      "Змінити фото профіля",
                                      style: TextStyle(
                                        color: Hexcolor('#1E2E45'),
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        setState(() {
                                          profileImageUrl = null;
                                        });
                                        _loadPicker(ImageSource.gallery);
                                        // takePhoto(ImageSource.gallery);

                                        // showModalBottomSheet(
                                        //     context: context,
                                        //     builder: ((builder) =>
                                        //         bottomSheet()));
                                      },
                                      child: Text(
                                        'Нове фото профіля',
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Hexcolor('#1E2E45'),
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 25,
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () async {
                                        final SharedPreferences
                                            sharedPreferences =
                                            await SharedPreferences
                                                .getInstance();
                                        deletePhoto();
                                        Navigator.of(context).pop();
                                        setState(() {
                                          profileImageUrl = null;
                                          deleteImageNow = true;

                                          sharedPreferences.setString(
                                              'photo', '');
                                          // _pickedImage = null;
                                        });
                                      },
                                      child: Text(
                                        'Видалити фото профіля',
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Hexcolor('#FF1E1E'),
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ],
                                ),
                              ]),
                        ),
                      ),
                    ))
              ],
            );
          });
        });
  }

  // -------------------------GET USER DATA FROM BACKEND-------------------------
  bool isLoading = false;
  String firstName = '';
  String lastName = '';
  String nickName = '';
  String userEmail = '';
  String birthday = '';
  String city = '';
  String country = '';
  String phone = '';
  String gender = '';
  String registrationType;

  var profileImageUrl;
  getUserData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    String photolink = sharedPreferences.get("photo");
    // int id = sharedPreferences.get("id");
    print(email);
    setState(() {
      userEmail = email;
    });
    print(token);
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';
    print(url);

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
        print(response.body);

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        print(responseJson);

        email = responseJson["email"];
        print(email);

        setState(() {
          if (responseJson["firstName"] != null &&
              responseJson["firstName"] != "") {
            var firstNameConverted =
                utf8.decode(responseJson["firstName"].runes.toList());
            firstName = firstNameConverted;
            firstNameTextEditingController.text = firstName;
          }
          if (responseJson["lastName"] != null &&
              responseJson["lastName"] != "") {
            var lastNameConverted =
                utf8.decode(responseJson["lastName"].runes.toList());
            lastName = lastNameConverted;
            secondNameTextEditingController.text = lastName;
          }
          if (responseJson["username"] != null &&
              responseJson["username"] != "") {
            var nickNameConverted =
                utf8.decode(responseJson["username"].runes.toList());
            nickName = nickNameConverted;
            nickNameTextEditingController.text = nickName;
          }
          if (responseJson["birthDate"] != null &&
              responseJson["birthDate"] != "") {
            birthday = responseJson["birthDate"].substring(0, 10);
            birthdayTextEditingController.text = birthday;
          }
          if (responseJson["phone"] != null && responseJson["phone"] != "") {
            phone = responseJson["phone"];
            controller.text = phone;
          }
          if (responseJson["country"] != null &&
              responseJson["country"] != "") {
            var countryConverted =
                utf8.decode(responseJson["country"].runes.toList());
            country = countryConverted;
            countryTextEditingController.text = country;
          }
          if (responseJson["city"] != null && responseJson["city"] != "") {
            var cityConverted =
                utf8.decode(responseJson["city"].runes.toList());
            city = cityConverted;
            cityTextEditingController.text = city;
          }
          if (responseJson["gender"] != null && responseJson["gender"] != "") {
            var genderConverted =
                utf8.decode(responseJson["gender"].runes.toList());
            gender = genderConverted;
            isSexSelected = true;
            if (gender == "Чоловіча") {
              sex = 'Чоловіча';
              isManSelected = true;
            } else {
              sex = 'Жіноча';
              isWomanSelected = true;
            }
          }

          //if (photolink != "" && photolink != null) {
          if (photolink != "") {
            profileImageUrl = responseJson["profileImageUrl"];
          } else {
            if (responseJson["profileImageUrl"] != null) {
              profileImageUrl = 'http://generation-admin.ehub.com.ua' +
                  responseJson["profileImageUrl"].substring(21);
            }
          }
          if (responseJson["notifications"] == true) {
            isNotificationsSelected = true;
          } else {
            isNotificationsSelected = false;
          }
          registrationType = responseJson["registrationType"];

          isLoading = false;
        });
        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  // loadImage() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   int id = sharedPreferences.get("id");
  //   String token = sharedPreferences.get("token");

  //   final http.Response response = await http.post(
  //     'http://generation-admin.ehub.com.ua/api/user/update',
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer $token',
  //     },
  //   );
  //   print(response.statusCode);
  //   if (response.statusCode == 200) {
  //     if (_pickedImage != null) {
  //       var imageResponse = await patchImage(
  //           'http://generation-admin.ehub.com.ua/api/user/updateProfileImage/' +
  //               '$id',
  //           _pickedImage.path);
  //       print(_pickedImage.path);
  //       print(imageResponse.statusCode);
  //       if (imageResponse.statusCode == 200) {
  //         print('Image loaded');
  //       }
  //     } else {
  //       print("Error to load image");
  //     }
  //   }
  // }

  deleteImagefromDatabase() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    String url =
        'http://generation-admin.ehub.com.ua/api/user/deleteProfileImage/' +
            '$id';
    final http.Response response = await http.delete(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      },
      // body: jsonEncode(<String, dynamic>{
      //   "id": id
      // }),
    );
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      print("Image deleted");
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to delete image.');
    }
  }

  Future<User> editUser() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    int id = sharedPreferences.get("id");
    String url = 'http://generation-admin.ehub.com.ua/api/user/update';
    print(id);
    print(url);
    print(token);

    var firstName = firstNameTextEditingController.text;
    var lastName = secondNameTextEditingController.text;
    var nickName = nickNameTextEditingController.text;
    var birthday = birthdayTextEditingController.text + "T00:00:00.000+00:00";
    var city = cityTextEditingController.text;
    var country = countryTextEditingController.text;
    var phone = controller.text;
    // var profileImageUrl;

    // if (_pickedImage == null) {
    //   profileImageUrl = null;
    // }

    final http.Response response = await http.put(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(<String, dynamic>{
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "username": nickName,
        "birthDate": birthday,
        "city": city,
        "country": country,
        "phone": phone,
        "gender": isManSelected ? "Чоловіча" : "Жіноча",
        "notifications": isNotificationsSelected ? true : false,
      }),
    );
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      // loadImage();
      if (_pickedImage != null) {
        sharedPreferences.setString('photo', '');
        deleteImagefromDatabase();
        var imageResponse = await patchImage(
            'http://generation-admin.ehub.com.ua/api/user/updateProfileImage/' +
                '$id',
            _pickedImage.path);
        print(_pickedImage.path);
        print(imageResponse.statusCode);
        if (imageResponse.statusCode == 200) {
          print('Image loaded');
        }
      } else {
        print('Error to load image');
      }
      if (deleteImageNow == true) {
        deleteImagefromDatabase();
      }

      setState(() {
        isLoading = false;
      });
      Navigator.pushNamed(context, '/5_myBottomBar.dart');

      // Future.delayed(const Duration(seconds: 1), () {
      //   Navigator.pushNamed(context, '10_profile.dart');
      // });
      // Profile();
      // Navigator.pop(context);
      print(response.body);
    } else {
      setState(() {
        isLoading = false;
      });
      throw Exception('Failed to edit User.');
    }
  }

  void _saveData() {
    if (!isSexSelected) {
      setState(() {
        makeErrorSex = true;
      });
    }
    if (formKey.currentState.validate() && (isManSelected || isWomanSelected)) {
      if (!isSexSelected) {
        isSexSelected = true;
      }

      editUser();
    }
  }

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  // ----------------------------------------------------------------------------

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Hexcolor('#7D5AC2'),
                  valueColor: new AlwaysStoppedAnimation<Color>(
                    Hexcolor('#FE6802'),
                  ),
                ),
              )
            : GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(_blankFocusNode);
                },
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Hexcolor('#FFFFFF'),
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Hexcolor('#7D5AC2'),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    bottom: 100,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: SizedBox(
                                          // height: 55,
                                          // width: 40,
                                          child: IconButton(
                                            icon: Image.asset(
                                              'assets/fg_images/6_home_search_back.png',
                                              width: 10,
                                              height: 19,
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          // deletePhoto();
                                        },
                                        child: Text(
                                          'Профіль',
                                          style: TextStyle(
                                              fontSize: 37,
                                              color: Colors.white,
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 80,
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 80,
                          ),
                          Container(
                            child: Expanded(
                                child: SingleChildScrollView(
                                    physics: BouncingScrollPhysics(),
                                    child: Container(
                                      margin: EdgeInsets.only(
                                        top: 15,
                                        left: 25,
                                        right: 25,
                                      ),
                                      child: Column(children: [
                                        Form(
                                          key: formKey,
                                          child: Column(
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Ім\'я",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (value) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть ім\'я";
                                                    },
                                                    controller:
                                                        firstNameTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText: "Введіть ім\'я",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Прізвище",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть прізвище";
                                                    },
                                                    controller:
                                                        secondNameTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText:
                                                          "Введіть прізвище",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Нікнейм",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть нікнейм";
                                                    },
                                                    controller:
                                                        nickNameTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText:
                                                          "Введіть нікнейм",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Дата народження",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  Stack(
                                                    overflow: Overflow.visible,
                                                    children: [
                                                      TextFormField(
                                                        onChanged: (val) {},
                                                        validator: (value) {
                                                          return value.length >
                                                                  0
                                                              ? null
                                                              : "Будь-ласка введіть дату народження";
                                                        },
                                                        onTap: () async {
                                                          DateTime date =
                                                              DateTime(1900);
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  new FocusNode());

                                                          date =
                                                              await showDatePicker(
                                                                  context:
                                                                      context,
                                                                  initialDate:
                                                                      DateTime
                                                                          .now(),
                                                                  firstDate:
                                                                      DateTime(
                                                                          1900),
                                                                  lastDate:
                                                                      DateTime(
                                                                          2100));

                                                          // birthdayTextEditingController
                                                          //         .text = date.toIso8601String();
                                                          birthdayTextEditingController
                                                                  .text =
                                                              "${date.year.toString()}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
                                                        },
                                                        controller:
                                                            birthdayTextEditingController,
                                                        textAlign:
                                                            TextAlign.left,
                                                        decoration:
                                                            InputDecoration(
                                                          hintText:
                                                              "Введіть дату народження",
                                                          contentPadding:
                                                              new EdgeInsets
                                                                  .only(
                                                            left: 20,
                                                            top: 21.5,
                                                            bottom: 21.5,
                                                          ),
                                                          hintStyle: TextStyle(
                                                            color: Hexcolor(
                                                                '#D3D3D3'),
                                                            fontSize: 17,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontFamily: 'Arial',
                                                          ),
                                                          focusedBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Hexcolor(
                                                                  '#EB5C18'),
                                                              width: 1,
                                                            ),
                                                          ),
                                                          enabledBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Hexcolor(
                                                                  '#E6E6E6'),
                                                              width: 1,
                                                            ),
                                                          ),
                                                          focusedErrorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Colors.red,
                                                              width: 1,
                                                            ),
                                                          ),
                                                          errorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        14.0),
                                                            borderSide:
                                                                BorderSide(
                                                              color: Colors.red,
                                                              width: 1,
                                                            ),
                                                          ),
                                                          fillColor:
                                                              Colors.white,
                                                          filled: true,
                                                        ),
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 17,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily: 'Arial',
                                                        ),
                                                      ),
                                                      Positioned(
                                                          top: 20,
                                                          right: 17,
                                                          child: Image.asset(
                                                            'assets/fg_images/10_profile_questionnaire_icon_birthday.png',
                                                            height: 20,
                                                            width: 18,
                                                          ))
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  chooseSex();
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            bottom: 7,
                                                          ),
                                                          child: Text(
                                                            "Стать",
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontFamily:
                                                                  'Arial',
                                                              color: Hexcolor(
                                                                  '#9B9B9B'),
                                                              fontSize: 17,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          decoration:
                                                              BoxDecoration(
                                                                  color: Hexcolor(
                                                                      '#FFFFFF'),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            10),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10),
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            10),
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            10),
                                                                  ),
                                                                  border: Border
                                                                      .all(
                                                                    color: !makeErrorSex
                                                                        ? Hexcolor(
                                                                            '#E6E6E6')
                                                                        : Hexcolor(
                                                                            '#D12D2D'),
                                                                    width: 1,
                                                                  )),
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 20),
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width -
                                                              60,
                                                          height: 60,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              isSexSelected
                                                                  ? Text(
                                                                      sex,
                                                                      style:
                                                                          TextStyle(
                                                                        color: Hexcolor(
                                                                            '#1E2E45'),
                                                                        fontSize:
                                                                            17,
                                                                        fontFamily:
                                                                            'Arial',
                                                                        fontWeight:
                                                                            FontWeight.w400,
                                                                      ),
                                                                    )
                                                                  : Text(
                                                                      sex,
                                                                      style:
                                                                          TextStyle(
                                                                        color: sex ==
                                                                                "Вкажіть вашу стать"
                                                                            ? Hexcolor('#1E2E45')
                                                                            : Hexcolor('#D3D3D3'),
                                                                        fontSize:
                                                                            17,
                                                                        fontFamily:
                                                                            'Arial',
                                                                        fontWeight:
                                                                            FontWeight.w400,
                                                                      ),
                                                                    ),
                                                              Container(
                                                                margin:
                                                                    EdgeInsets
                                                                        .only(
                                                                  right: 20,
                                                                ),
                                                                child:
                                                                    Image.asset(
                                                                  'assets/fg_images/6_home_filter_arrow.png',
                                                                  width: 10,
                                                                  height: 18,
                                                                  color: Hexcolor(
                                                                      '#1E2E45'),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              !makeErrorSex
                                                  ? SizedBox(
                                                      height: 15,
                                                    )
                                                  : Container(
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          bottom: 15,
                                                          top: 7),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      child: Text(
                                                        'Будь-ласка введіть стать',
                                                        style: TextStyle(
                                                          color:
                                                              Colors.red[700],
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Email",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          color: Hexcolor(
                                                              '#FFFFFF'),
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    10),
                                                            topRight:
                                                                Radius.circular(
                                                                    10),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    10),
                                                            bottomRight:
                                                                Radius.circular(
                                                                    10),
                                                          ),
                                                          border: Border.all(
                                                            color: Hexcolor(
                                                                '#E6E6E6'),
                                                            width: 1,
                                                          )),
                                                      padding: EdgeInsets.only(
                                                          left: 20),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              60,
                                                      height: 60,
                                                      child: Text(
                                                        userEmail,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#D3D3D3'),
                                                          fontSize: 17,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      )),
                                                  // TextFormField(
                                                  //   // onChanged: (val) {},
                                                  //   // validator: (value) {
                                                  //   //   return RegExp(
                                                  //   //               r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                  //   //           .hasMatch(value)
                                                  //   //       ? null
                                                  //   //       : "Будь-ласка введіть коректний email";
                                                  //   // },
                                                  //   controller:
                                                  //       emailTextEditingController,
                                                  //   textAlign: TextAlign.left,
                                                  //   textAlignVertical:
                                                  //       TextAlignVertical
                                                  //           .center,
                                                  //   decoration: InputDecoration(
                                                  //     hintText: "Введіть email",
                                                  //     contentPadding:
                                                  //         new EdgeInsets.only(
                                                  //       left: 20,
                                                  //       top: 21.5,
                                                  //       bottom: 21.5,
                                                  //     ),
                                                  //     hintStyle: TextStyle(
                                                  //       color:
                                                  //           Hexcolor('#D3D3D3'),
                                                  //       fontSize: 17,
                                                  //       fontWeight:
                                                  //           FontWeight.w400,
                                                  //       fontFamily: 'Arial',
                                                  //     ),
                                                  //     focusedBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Hexcolor(
                                                  //             '#EB5C18'),
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     enabledBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Hexcolor(
                                                  //             '#E6E6E6'),
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     focusedErrorBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Colors.red,
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     errorBorder:
                                                  //         OutlineInputBorder(
                                                  //       borderRadius:
                                                  //           BorderRadius
                                                  //               .circular(14.0),
                                                  //       borderSide: BorderSide(
                                                  //         color: Colors.red,
                                                  //         width: 1,
                                                  //       ),
                                                  //     ),
                                                  //     fillColor: Colors.white,
                                                  //     filled: true,
                                                  //   ),
                                                  //   style: TextStyle(
                                                  //     color:
                                                  //         Hexcolor('#1E2E45'),
                                                  //     fontSize: 17,
                                                  //     fontWeight:
                                                  //         FontWeight.w400,
                                                  //     fontFamily: 'Arial',
                                                  //   ),
                                                  // ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Телефон",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 16
                                                          ? null
                                                          : "Будь-ласка введіть телефон";
                                                    },
                                                    controller: controller,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      // hintText: "+3(8000)000-00-00",
                                                      hintText:
                                                          "+380(00) 000-00-00",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Країна",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть країну";
                                                    },
                                                    controller:
                                                        countryTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText: "Країна",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                              left: 20,
                                                              top: 21.5,
                                                              bottom: 21.5),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      bottom: 7,
                                                    ),
                                                    child: Text(
                                                      "Місто",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                        color:
                                                            Hexcolor('#9B9B9B'),
                                                        fontSize: 17,
                                                      ),
                                                    ),
                                                  ),
                                                  TextFormField(
                                                    onChanged: (val) {},
                                                    validator: (value) {
                                                      return value.length > 0
                                                          ? null
                                                          : "Будь-ласка введіть місто";
                                                    },
                                                    controller:
                                                        cityTextEditingController,
                                                    textAlign: TextAlign.left,
                                                    decoration: InputDecoration(
                                                      hintText: "Місто",
                                                      contentPadding:
                                                          new EdgeInsets.only(
                                                        left: 20,
                                                        top: 21.5,
                                                        bottom: 21.5,
                                                      ),
                                                      hintStyle: TextStyle(
                                                        color:
                                                            Hexcolor('#D3D3D3'),
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Arial',
                                                      ),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#EB5C18'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Hexcolor(
                                                              '#E6E6E6'),
                                                          width: 1,
                                                        ),
                                                      ),
                                                      focusedErrorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      errorBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(14.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.red,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      fillColor: Colors.white,
                                                      filled: true,
                                                    ),
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              registrationType == "EMAIL"
                                                  ? Column(
                                                      // change password
                                                      children: [
                                                        SizedBox(
                                                          height: 35,
                                                        ),
                                                        GestureDetector(
                                                          behavior:
                                                              HitTestBehavior
                                                                  .opaque,
                                                          onTap: () {
                                                            changePassword();
                                                          },
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  SizedBox(
                                                                    width: 1,
                                                                  ),
                                                                  Image.asset(
                                                                    'assets/fg_images/10_profile_edit_password.png',
                                                                    width: 16,
                                                                    height: 20,
                                                                  ),
                                                                  SizedBox(
                                                                    width: 11,
                                                                  ),
                                                                  Text(
                                                                    "Змінити пароль",
                                                                    style:
                                                                        TextStyle(
                                                                      color: Hexcolor(
                                                                          '#1E2E45'),
                                                                      fontSize:
                                                                          17,
                                                                      fontFamily:
                                                                          'Arial',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              Image.asset(
                                                                'assets/fg_images/6_home_filter_arrow.png',
                                                                width: 10,
                                                                height: 18,
                                                                color: Hexcolor(
                                                                    '#1E2E45'),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 25,
                                                        ),
                                                        Divider(
                                                          color: Hexcolor(
                                                              '#ECECEC'),
                                                          thickness: 1,
                                                        ),
                                                      ],
                                                    )
                                                  : Container(),
                                              SizedBox(
                                                height: 25,
                                              ),
                                              GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  setState(() {
                                                    isNotificationsSelected =
                                                        !isNotificationsSelected;
                                                  });
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Image.asset(
                                                      isNotificationsSelected
                                                          ? 'assets/fg_images/6_home_filter_sex_selected.png'
                                                          : 'assets/fg_images/6_home_filter_sex_unselected.png',
                                                      width: 18,
                                                      height: 18,
                                                    ),
                                                    SizedBox(
                                                      width: 11,
                                                    ),
                                                    Text(
                                                      "Отримувати повідомлення",
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#1E2E45'),
                                                        fontSize: 17,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height: 35,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ]),
                                    ))),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 40,
                            child: RaisedButton(
                              color: Hexcolor('#FE6802'),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0),
                              ),
                              child: Text("Зберегти",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Arial',
                                      color: Colors.white,
                                      letterSpacing: 1.09,
                                      fontSize: 17)),
                              onPressed: () {
                                _saveData();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: (MediaQuery.of(context).size.width / 2) - 80,
                      top: 120,
                      child: Stack(
                        overflow: Overflow.visible,
                        children: [
                          profileImageUrl != null
                              ? GestureDetector(
                                  onTap: () {
                                    changeProfilePhoto();
                                    // showModalBottomSheet(
                                    //     context: context,
                                    //     builder: ((builder) => bottomSheet()));
                                  },
                                  child: CircleAvatar(
                                      backgroundColor: Hexcolor('#F2F2F2'),
                                      radius: 80,
                                      backgroundImage:
                                          // _pickedImage != null ? FileImage(_pickedImage) : null
                                          NetworkImage(profileImageUrl)

                                      // _imageFile == null
                                      //     ? AssetImage(
                                      //         'assets/fg_images/10_profile_edit_default_pic.png')
                                      //     : FileImage(File(_imageFile.path)),
                                      ),
                                )
                              : GestureDetector(
                                  onTap: () {
                                    changeProfilePhoto();
                                    // showModalBottomSheet(
                                    //     context: context,
                                    //     builder: ((builder) => bottomSheet()));
                                  },
                                  child: CircleAvatar(
                                      backgroundColor: Hexcolor('#F2F2F2'),
                                      radius: 80,
                                      backgroundImage:
                                          // _pickedImage != null ? FileImage(_pickedImage) : null
                                          _pickedImage == null
                                              ? AssetImage(
                                                  'assets/fg_images/10_profile_edit_default_pic.png')
                                              : FileImage(_pickedImage)

                                      // _imageFile == null
                                      //     ? AssetImage(
                                      //         'assets/fg_images/10_profile_edit_default_pic.png')
                                      //     : FileImage(File(_imageFile.path)),
                                      ),
                                ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: (MediaQuery.of(context).size.width / 2) + 40,
                      top: 240,
                      // top: 200,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          changeProfilePhoto();
                          // showModalBottomSheet(
                          //     context: context,
                          //     builder: ((builder) =>
                          //         bottomSheet())); // https://www.youtube.com/watch?v=6af6qgziYfo --- ITS FUCKING COOL VIDEO!!!
                        },
                        child: Image.asset(
                          'assets/fg_images/10_prodile_edit_icon_camera.png',
                          height: 40,
                          width: 40,
                        ),
                      ),
                    )
                  ],
                ),
              ));
  }
}
