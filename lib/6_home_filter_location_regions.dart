import '6_home_filter_location_model.dart';

final Location location1 = Location(
  place: 'Киев',
  region: 'Киевская область',
);

final Location location2 = Location(
  place: 'Ходосовка',
  region: 'Киевская область, Киево-Святошинский район',
);

final Location location3 = Location(
  place: 'Киевская область',
  region: 'Вся область',
);

List<Location> locations = [
  location1,
  location2,
  location3,
];

final Region region1 = Region(
  region: 'Винницкая область',
  city: 'Вінниця',
  isSelected: false,
);

final Region region2 = Region(
  region: 'Волынская область',
  city: 'Луцьк',
  isSelected: false,
);

final Region region3 = Region(
  region: 'Днепропетровская область',
  city: 'Дніпро',
  isSelected: false,
);

final Region region4 = Region(
  region: 'Донецкая область',
  city: 'Донецьк',
  isSelected: false,
);

final Region region5 = Region(
  region: 'Житомирская область',
  city: 'Житомир',
  isSelected: false,
);

final Region region6 = Region(
  region: 'Закарпатская область',
  city: 'Ужгород',
  isSelected: false,
);

final Region region7 = Region(
  region: 'Запорожская область',
  city: 'Запоріжжя',
  isSelected: false,
);

final Region region8 = Region(
  region: 'Ивано-Франковская область',
  city: 'Івано-Франківськ',
  isSelected: false,
);

final Region region9 = Region(
  region: 'Киевская область',
  city: 'Київ',
  isSelected: false,
);

final Region region10 = Region(
  region: 'Кировоградская область',
  city: 'Кропивницький',
  isSelected: false,
);

final Region region11 = Region(
  region: 'Луганская область',
  city: 'Луганськ',
  isSelected: false,
);

final Region region12 = Region(
  region: 'Львовская область',
  city: 'Львів',
  isSelected: false,
);

final Region region13 = Region(
  region: 'Николаевская область',
  city: 'Миколаїв',
  isSelected: false,
);

final Region region14 = Region(
  region: 'Одесская область',
  city: 'Одеса',
  isSelected: false,
);

final Region region15 = Region(
  region: 'Полтавская область',
  city: 'Полтава',
  isSelected: false,
);

final Region region16 = Region(
  region: 'Ровненская область',
  city: 'Рівне',
  isSelected: false,
);

final Region region17 = Region(
  region: 'Сумская область',
  city: 'Суми',
  isSelected: false,
);

final Region region18 = Region(
  region: 'Тернопольская область',
  city: 'Тернопіль',
  isSelected: false,
);

final Region region19 = Region(
  region: 'Харьковская область',
  city: 'Харьків',
  isSelected: false,
);

final Region region20 = Region(
  region: 'Херсонская область',
  city: 'Херсон',
  isSelected: false,
);

final Region region21 = Region(
  region: 'Хмельницкая область',
  city: 'Хмельницький',
  isSelected: false,
);

final Region region22 = Region(
  region: 'Черкасская область',
  city: 'Черкаси',
  isSelected: false,
);

final Region region23 = Region(
  region: 'Черниговская область',
  city: 'Чернігів',
  isSelected: false,
);

final Region region24 = Region(
  region: 'Черновицкая область',
  city: 'Чернівці',
  isSelected: false,
);

// final Region region25 = Region(
//   region: 'АР Крым',
//   city: 'Симферополь',
// );

List<Region> regions = [
  region1,
  region2,
  region3,
  region4,
  region5,
  region6,
  region7,
  region8,
  region9,
  region10,
  region11,
  region12,
  region13,
  region14,
  region15,
  region16,
  region17,
  region18,
  region19,
  region20,
  region21,
  region22,
  region23,
  region24,
];

final District district1 = District(
  district: 'Голосіївський',
  districtPlace: 'район',
  isSelected: false,
);

final District district2 = District(
  district: 'Дарницький',
  districtPlace: 'район',
  isSelected: false,
);

final District district3 = District(
  district: 'Дніпровський',
  districtPlace: 'район',
  isSelected: false,
);

final District district4 = District(
  district: 'Деснянський',
  districtPlace: 'район',
  isSelected: false,
);

final District district5 = District(
  district: 'Святошинський',
  districtPlace: 'район',
  isSelected: false,
);

final District district6 = District(
  district: 'Солом\'янський',
  districtPlace: 'район',
  isSelected: false,
);

final District district7 = District(
  district: 'Оболонський',
  districtPlace: 'район',
  isSelected: false,
);

final District district8 = District(
  district: 'Подольський',
  districtPlace: 'район',
  isSelected: false,
);

final District district9 = District(
  district: 'Печерський',
  districtPlace: 'район',
  isSelected: false,
);

final District district10 = District(
  district: 'Шевченківський',
  districtPlace: 'район',
  isSelected: false,
);

List<District> districts = [
  district1,
  district2,
  district3,
  district4,
  district5,
  district6,
  district7,
  district8,
  district9,
  district10,
];