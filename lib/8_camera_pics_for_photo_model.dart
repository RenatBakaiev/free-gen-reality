class PicForCamera {
  final String name;
  final String image;

  PicForCamera({
    this.name,
    this.image,
  });
}