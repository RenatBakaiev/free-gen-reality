import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ProfileProduct2 extends StatefulWidget {
  @override
  _ProfileProduct2State createState() => _ProfileProduct2State();
}

class _ProfileProduct2State extends State<ProfileProduct2> {
  int foints = 100;
  int productPrice = 120;

  notEnoughFoints() {
    showGeneralDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        transitionBuilder: (context, a1, a2, widget) {
          final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
          int productPrice = 120;
          int quantity = productPrice - foints;
          return Transform(
            transform: Matrix4.translationValues(0.0, curvedValue * -200, 0.0),
            child: Opacity(
                opacity: a1.value,
                child: Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              height: 490,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    child: Text(
                      'Недостаточно средств',
                      style: TextStyle(
                        color: Hexcolor('#1E2E45'),
                        fontSize: 24,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Вам не хватает ',
                        style: TextStyle(
                          color: Hexcolor('#747474'),
                          fontSize: 20,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        '$quantity' + ' Foint!',
                        style: TextStyle(
                          color: Hexcolor('#FF1E1E'),
                          fontSize: 20,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 80,
                    height: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/fg_images/10_profile_store_product_2.jpg'),
                          fit: BoxFit.cover,
                        )),
                  ),
                  Text(
                    'Long Bord New Professional',
                    style: TextStyle(
                        fontSize: 20,
                        color: Hexcolor('1E2E45'),
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w700),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Стоимость',
                        style: TextStyle(
                            fontSize: 14,
                            color: Hexcolor('#B9BCC4'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '$productPrice' + ' Foint',
                        style: TextStyle(
                            fontSize: 16,
                            color: Hexcolor('#FF1E1E'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      bottom: 20,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 80,
                    child: RaisedButton(
                      child: Text(
                        "Закрыть",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, '/10_profile_store.dart');
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
                ),
          );
        },
        transitionDuration: Duration(milliseconds: 500),
        barrierDismissible: false,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
    // showDialog(
    //     barrierDismissible: false,
    //     barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
    //     context: context,
    //     builder: (context) {
    //       int productPrice = 120;
    //       int quantity = productPrice - foints;
    //       return Dialog(
    //         insetPadding: EdgeInsets.only(
    //           left: 20,
    //           right: 20,
    //         ),
    //         shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(10),
    //         ),
    //         child: Container(
    //           height: 490,
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             crossAxisAlignment: CrossAxisAlignment.center,
    //             children: [
    //               Container(
    //                 margin: EdgeInsets.only(
    //                   top: 20,
    //                 ),
    //                 child: Text(
    //                   'Недостаточно средств',
    //                   style: TextStyle(
    //                     color: Hexcolor('#1E2E45'),
    //                     fontSize: 24,
    //                     fontFamily: 'Arial',
    //                     fontWeight: FontWeight.w700,
    //                   ),
    //                 ),
    //               ),
    //               Row(
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 crossAxisAlignment: CrossAxisAlignment.center,
    //                 children: [
    //                   Text(
    //                     'Вам не хватает ',
    //                     style: TextStyle(
    //                       color: Hexcolor('#747474'),
    //                       fontSize: 20,
    //                       fontFamily: 'Arial',
    //                       fontWeight: FontWeight.w400,
    //                     ),
    //                   ),
    //                   Text(
    //                     '$quantity' + ' Foint!',
    //                     style: TextStyle(
    //                       color: Hexcolor('#FF1E1E'),
    //                       fontSize: 20,
    //                       fontFamily: 'Arial',
    //                       fontWeight: FontWeight.w700,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //               Container(
    //                 width: MediaQuery.of(context).size.width - 80,
    //                 height: 200,
    //                 decoration: BoxDecoration(
    //                     borderRadius: BorderRadius.all(Radius.circular(14)),
    //                     image: DecorationImage(
    //                       image: AssetImage(
    //                           'assets/fg_images/10_profile_store_product_2.jpg'),
    //                       fit: BoxFit.cover,
    //                     )),
    //               ),
    //               Text(
    //                 'Long Bord New Professional',
    //                 style: TextStyle(
    //                     fontSize: 20,
    //                     color: Hexcolor('1E2E45'),
    //                     fontFamily: 'Arial',
    //                     fontWeight: FontWeight.w700),
    //               ),
    //               Row(
    //                 crossAxisAlignment: CrossAxisAlignment.center,
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 children: [
    //                   Text(
    //                     'Стоимость',
    //                     style: TextStyle(
    //                         fontSize: 14,
    //                         color: Hexcolor('#B9BCC4'),
    //                         fontFamily: 'Arial',
    //                         fontWeight: FontWeight.w400),
    //                   ),
    //                   SizedBox(
    //                     width: 10,
    //                   ),
    //                   Text(
    //                     '$productPrice' + ' Foint',
    //                     style: TextStyle(
    //                         fontSize: 16,
    //                         color: Hexcolor('#FF1E1E'),
    //                         fontFamily: 'Arial',
    //                         fontWeight: FontWeight.w700),
    //                   ),
    //                 ],
    //               ),
    //               Container(
    //                 margin: EdgeInsets.only(
    //                   left: 20,
    //                   right: 20,
    //                   bottom: 20,
    //                 ),
    //                 height: 60,
    //                 width: MediaQuery.of(context).size.width - 80,
    //                 child: RaisedButton(
    //                   child: Text(
    //                     "Закрыть",
    //                     style: TextStyle(
    //                       color: Colors.white,
    //                       fontSize: 17,
    //                       fontFamily: 'Arial',
    //                       fontWeight: FontWeight.w700,
    //                     ),
    //                   ),
    //                   color: Hexcolor('#FE6802'),
    //                   shape: new RoundedRectangleBorder(
    //                       borderRadius: new BorderRadius.circular(14.0)),
    //                   onPressed: () {
    //                     Navigator.pushNamed(context, '/10_profile_store.dart');
    //                   },
    //                 ),
    //               ),
    //             ],
    //           ),
    //         ),
    //       );
    //     });
  }

  youBoughtProduct() {
    showGeneralDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        transitionBuilder: (context, a1, a2, widget) {
          final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
          return Transform(
            transform: Matrix4.translationValues(0.0, curvedValue * -200, 0.0),
            child: Opacity(
                opacity: a1.value,
                child: Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              height: 490,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    child: Text(
                      'Поздравляем',
                      style: TextStyle(
                        color: Hexcolor('#1E2E45'),
                        fontSize: 24,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Text(
                    'Вы успешно купили товар!',
                    style: TextStyle(
                      color: Hexcolor('#59B32D'),
                      fontSize: 20,
                      fontFamily: 'Arial',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 80,
                    height: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/fg_images/10_profile_store_product_2.jpg'),
                          fit: BoxFit.cover,
                        )),
                  ),
                  Text(
                    'Long Bord New Professional',
                    style: TextStyle(
                        fontSize: 20,
                        color: Hexcolor('1E2E45'),
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w700),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Стоимость',
                        style: TextStyle(
                            fontSize: 14,
                            color: Hexcolor('#B9BCC4'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '$productPrice' + ' Foint',
                        style: TextStyle(
                            fontSize: 16,
                            color: Hexcolor('#59B32D'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      bottom: 20,
                    ),
                    height: 60,
                    width: MediaQuery.of(context).size.width - 80,
                    child: RaisedButton(
                      child: Text(
                        "Закрыть",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        Navigator.pushNamed(
                            context, '/10_profile_store_product_2.dart');
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
                ),
          );
        },
        transitionDuration: Duration(milliseconds: 500),
        barrierDismissible: false,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
    // showDialog(
    //     barrierDismissible: false,
    //     barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
    //     context: context,
    //     builder: (context) {
    //       int productPrice = 120;
    //       return Dialog(
    //         insetPadding: EdgeInsets.only(
    //           left: 20,
    //           right: 20,
    //         ),
    //         shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(10),
    //         ),
    //         child: Container(
    //           height: 490,
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             crossAxisAlignment: CrossAxisAlignment.center,
    //             children: [
    //               Container(
    //                 margin: EdgeInsets.only(
    //                   top: 20,
    //                 ),
    //                 child: Text(
    //                   'Поздравляем',
    //                   style: TextStyle(
    //                     color: Hexcolor('#1E2E45'),
    //                     fontSize: 24,
    //                     fontFamily: 'Arial',
    //                     fontWeight: FontWeight.w700,
    //                   ),
    //                 ),
    //               ),
    //               Text(
    //                 'Вы успешно купили товар!',
    //                 style: TextStyle(
    //                   color: Hexcolor('#59B32D'),
    //                   fontSize: 20,
    //                   fontFamily: 'Arial',
    //                   fontWeight: FontWeight.w700,
    //                 ),
    //               ),
    //               Container(
    //                 width: MediaQuery.of(context).size.width - 80,
    //                 height: 200,
    //                 decoration: BoxDecoration(
    //                     borderRadius: BorderRadius.all(Radius.circular(14)),
    //                     image: DecorationImage(
    //                       image: AssetImage(
    //                           'assets/fg_images/10_profile_store_product_2.jpg'),
    //                       fit: BoxFit.cover,
    //                     )),
    //               ),
    //               Text(
    //                 'Long Bord New Professional',
    //                 style: TextStyle(
    //                     fontSize: 20,
    //                     color: Hexcolor('1E2E45'),
    //                     fontFamily: 'Arial',
    //                     fontWeight: FontWeight.w700),
    //               ),
    //               Row(
    //                 crossAxisAlignment: CrossAxisAlignment.center,
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 children: [
    //                   Text(
    //                     'Стоимость',
    //                     style: TextStyle(
    //                         fontSize: 14,
    //                         color: Hexcolor('#B9BCC4'),
    //                         fontFamily: 'Arial',
    //                         fontWeight: FontWeight.w400),
    //                   ),
    //                   SizedBox(
    //                     width: 10,
    //                   ),
    //                   Text(
    //                     '$productPrice' + ' Foint',
    //                     style: TextStyle(
    //                         fontSize: 16,
    //                         color: Hexcolor('#59B32D'),
    //                         fontFamily: 'Arial',
    //                         fontWeight: FontWeight.w700),
    //                   ),
    //                 ],
    //               ),
    //               Container(
    //                 margin: EdgeInsets.only(
    //                   left: 20,
    //                   right: 20,
    //                   bottom: 20,
    //                 ),
    //                 height: 60,
    //                 width: MediaQuery.of(context).size.width - 80,
    //                 child: RaisedButton(
    //                   child: Text(
    //                     "Закрыть",
    //                     style: TextStyle(
    //                       color: Colors.white,
    //                       fontSize: 17,
    //                       fontFamily: 'Arial',
    //                       fontWeight: FontWeight.w700,
    //                     ),
    //                   ),
    //                   color: Hexcolor('#FE6802'),
    //                   shape: new RoundedRectangleBorder(
    //                       borderRadius: new BorderRadius.circular(14.0)),
    //                   onPressed: () {
    //                     Navigator.pushNamed(
    //                         context, '/10_profile_store_product_2.dart');
    //                   },
    //                 ),
    //               ),
    //             ],
    //           ),
    //         ),
    //       );
    //     });
  }

  confirmBuyProduct() {
    showGeneralDialog(
        barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
        transitionBuilder: (context, a1, a2, widget) {
          final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
          return Transform(
            transform: Matrix4.translationValues(0.0, curvedValue * -200, 0.0),
            child: Opacity(
                opacity: a1.value,
                child: Dialog(
            insetPadding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              height: 490,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    child: Text(
                      'Покупка товара',
                      style: TextStyle(
                        color: Hexcolor('#1E2E45'),
                        fontSize: 24,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Text(
                    'Вы точно хотите купить?',
                    style: TextStyle(
                      color: Hexcolor('#747474'),
                      fontSize: 20,
                      fontFamily: 'Arial',
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 80,
                    height: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/fg_images/10_profile_store_product_2.jpg'),
                          fit: BoxFit.cover,
                        )),
                  ),
                  Text(
                    'Long Bord New Professional',
                    style: TextStyle(
                        fontSize: 20,
                        color: Hexcolor('1E2E45'),
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w700),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Стоимость',
                        style: TextStyle(
                            fontSize: 14,
                            color: Hexcolor('#B9BCC4'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '$productPrice' + ' Foint',
                        style: TextStyle(
                            fontSize: 16,
                            color: Hexcolor('#59B32D'),
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width / 2 - 45,
                          child: RaisedButton(
                            child: Text(
                              "Отмена",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#BEBEBE'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/10_profile_store_product_2.dart');
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width / 2 - 45,
                          child: RaisedButton(
                            child: Text(
                              "Купить",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              if (productPrice > foints) {
                                notEnoughFoints();
                              } else {
                                youBoughtProduct();
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
                ),
          );
        },
        transitionDuration: Duration(milliseconds: 500),
        barrierDismissible: false,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
    // showDialog(
    //     barrierDismissible: false,
    //     barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
    //     context: context,
    //     builder: (context) {
    //       int productPrice = 120;
    //       return Dialog(
    //         insetPadding: EdgeInsets.only(
    //           left: 20,
    //           right: 20,
    //         ),
    //         shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(10),
    //         ),
    //         child: Container(
    //           height: 490,
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             crossAxisAlignment: CrossAxisAlignment.center,
    //             children: [
    //               Container(
    //                 margin: EdgeInsets.only(
    //                   top: 20,
    //                 ),
    //                 child: Text(
    //                   'Покупка товара',
    //                   style: TextStyle(
    //                     color: Hexcolor('#1E2E45'),
    //                     fontSize: 24,
    //                     fontFamily: 'Arial',
    //                     fontWeight: FontWeight.w700,
    //                   ),
    //                 ),
    //               ),
    //               Text(
    //                 'Вы точно хотите купить?',
    //                 style: TextStyle(
    //                   color: Hexcolor('#747474'),
    //                   fontSize: 20,
    //                   fontFamily: 'Arial',
    //                   fontWeight: FontWeight.w400,
    //                 ),
    //               ),
    //               Container(
    //                 width: MediaQuery.of(context).size.width - 80,
    //                 height: 200,
    //                 decoration: BoxDecoration(
    //                     borderRadius: BorderRadius.all(Radius.circular(14)),
    //                     image: DecorationImage(
    //                       image: AssetImage(
    //                           'assets/fg_images/10_profile_store_product_2.jpg'),
    //                       fit: BoxFit.cover,
    //                     )),
    //               ),
    //               Text(
    //                 'Long Bord New Professional',
    //                 style: TextStyle(
    //                     fontSize: 20,
    //                     color: Hexcolor('1E2E45'),
    //                     fontFamily: 'Arial',
    //                     fontWeight: FontWeight.w700),
    //               ),
    //               Row(
    //                 crossAxisAlignment: CrossAxisAlignment.center,
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 children: [
    //                   Text(
    //                     'Стоимость',
    //                     style: TextStyle(
    //                         fontSize: 14,
    //                         color: Hexcolor('#B9BCC4'),
    //                         fontFamily: 'Arial',
    //                         fontWeight: FontWeight.w400),
    //                   ),
    //                   SizedBox(
    //                     width: 10,
    //                   ),
    //                   Text(
    //                     '$productPrice' + ' Foint',
    //                     style: TextStyle(
    //                         fontSize: 16,
    //                         color: Hexcolor('#59B32D'),
    //                         fontFamily: 'Arial',
    //                         fontWeight: FontWeight.w700),
    //                   ),
    //                 ],
    //               ),
    //               Container(
    //                 margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
    //                 child: Row(
    //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                   children: [
    //                     Container(
    //                       height: 60,
    //                       width: MediaQuery.of(context).size.width / 2 - 45,
    //                       child: RaisedButton(
    //                         child: Text(
    //                           "Отмена",
    //                           style: TextStyle(
    //                             color: Colors.white,
    //                             fontSize: 17,
    //                             fontFamily: 'Arial',
    //                             fontWeight: FontWeight.w700,
    //                           ),
    //                         ),
    //                         color: Hexcolor('#BEBEBE'),
    //                         shape: new RoundedRectangleBorder(
    //                             borderRadius: new BorderRadius.circular(14.0)),
    //                         onPressed: () {
    //                           Navigator.pushNamed(
    //                               context, '/10_profile_store_product_2.dart');
    //                         },
    //                       ),
    //                     ),
    //                     SizedBox(
    //                       width: 10,
    //                     ),
    //                     Container(
    //                       height: 60,
    //                       width: MediaQuery.of(context).size.width / 2 - 45,
    //                       child: RaisedButton(
    //                         child: Text(
    //                           "Купить",
    //                           style: TextStyle(
    //                             color: Colors.white,
    //                             fontSize: 17,
    //                             fontFamily: 'Arial',
    //                             fontWeight: FontWeight.w700,
    //                           ),
    //                         ),
    //                         color: Hexcolor('#FE6802'),
    //                         shape: new RoundedRectangleBorder(
    //                             borderRadius: new BorderRadius.circular(14.0)),
    //                         onPressed: () {
    //                           if (productPrice > foints) {
    //                             notEnoughFoints();
    //                           } else {
    //                             youBoughtProduct();
    //                           }
    //                         },
    //                       ),
    //                     ),
    //                   ],
    //                 ),
    //               ),
    //             ],
    //           ),
    //         ),
    //       );
    //     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: 265,
              decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0)),
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/fg_images/10_profile_store_product_2.jpg'),
                    fit: BoxFit.cover,
                  )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/10_profile_store.dart');
                    },
                    child: Container(
                        margin: EdgeInsets.only(
                          left: 30,
                          top: 65,
                        ),
                        child: Image.asset(
                          'assets/fg_images/sportGames_icon_arrow_back.png',
                          width: 10,
                          height: 19,
                          color: Hexcolor('#FFFFFF'),
                        )),
                  ),
                ],
              ),
            ),
            Container(
              child: Expanded(
                  child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                          // margin: EdgeInsets.only(left: 5),
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30, left: 30),
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        child: Container(
                                      margin: EdgeInsets.only(bottom: 20),
                                      child: Text(
                                        'Long Bord New Professional',
                                        style: TextStyle(
                                            fontSize: 25,
                                            color: Hexcolor('#1E2E45'),
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w600),
                                      ),
                                    )),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Цена',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Hexcolor('#B9BCC4'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '120' + ' Foint',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Hexcolor('#298127'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Image.asset(
                                          'assets/fg_images/10_profile_store_icon_map.png',
                                          height: 26,
                                          width: 17,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 5),
                                          child: Container(
                                            child: Text(
                                              'Киев, Дарницкий',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Hexcolor('#1E2E45'),
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '29 января, 01:22',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Hexcolor('#B9BCC4'),
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 30,
                              right: 30,
                              top: 30,
                              bottom: 20,
                            ),
                            child: Column(children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 15),
                                child: Row(
                                  children: [
                                    Text(
                                      'Описание',
                                      style: TextStyle(
                                          color: Hexcolor('#484848'),
                                          fontSize: 18,
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\nSed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                                style: TextStyle(
                                  color: Hexcolor('#484848'),
                                  fontSize: 16,
                                  fontFamily: 'Arial',
                                ),
                              ),
                            ]),
                          ),
                        ],
                      )))),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 50,
              child: RaisedButton(
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(14.0),
                ),
                child: Text("Купить",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Arial',
                        color: Colors.white,
                        letterSpacing: 1.09,
                        fontSize: 17)),
                onPressed: () {
                  confirmBuyProduct();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
