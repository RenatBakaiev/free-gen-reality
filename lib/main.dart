// ------------------------------------MY NEW PROJECT--------------------------------------------------- 17.09.20
// import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
// import 'package:camera/camera.dart';
// import 'package:flutter/services.dart';
import 'package:flutter_free_gen_reality/0_splash_screen_language.dart';
import 'package:flutter_free_gen_reality/10_profile_aboutus.dart';
// import 'package:flutter_free_gen_reality/10_profile_product_page.dart';
import 'package:flutter_free_gen_reality/4_enter_forgot_password.dart';
import 'package:flutter_free_gen_reality/desc_description.dart';
import 'package:flutter_free_gen_reality/desc_pinsOnMap.dart';
import 'package:flutter_free_gen_reality/desc_quiz.dart';
import 'package:flutter_free_gen_reality/desc_scanQR.dart';
import 'package:flutter_free_gen_reality/missions_12_12_trees.dart';
import 'package:flutter_free_gen_reality/missions_12_desc11.dart';
import 'package:flutter_free_gen_reality/missions_12_desc4.dart';
import 'package:flutter_free_gen_reality/missions_12_desc9.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz1.dart';
import 'package:flutter_free_gen_reality/mission_description.dart';
import 'package:flutter_free_gen_reality/mission_pinsOnMap.dart';
import 'package:flutter_free_gen_reality/mission_quiz.dart';
import 'package:flutter_free_gen_reality/mission_scanQR.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz10.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz11.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz12.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz5.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz6.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz7.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz8.dart';
import 'package:flutter_free_gen_reality/missions_12_quiz9.dart';
import 'package:flutter_free_gen_reality/missions_12_scan1.dart';
import 'package:flutter_free_gen_reality/missions_12_scan10.dart';
import 'package:flutter_free_gen_reality/missions_12_scan11.dart';
import 'package:flutter_free_gen_reality/missions_12_scan12.dart';
import 'package:flutter_free_gen_reality/missions_12_scan2.dart';
import 'package:flutter_free_gen_reality/missions_12_scan3.dart';
import 'package:flutter_free_gen_reality/missions_12_scan4.dart';
import 'package:flutter_free_gen_reality/missions_12_scan5.dart';
import 'package:flutter_free_gen_reality/missions_12_scan6.dart';
import 'package:flutter_free_gen_reality/missions_12_scan7.dart';
import 'package:flutter_free_gen_reality/missions_12_scan8.dart';
import 'package:flutter_free_gen_reality/missions_12_scan9.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info1.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info10.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info11.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info12.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info2.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info3.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info4.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info5.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info6.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info7.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info8.dart';
import 'package:flutter_free_gen_reality/missions_12_scan_info9.dart';
import 'package:flutter_free_gen_reality/model_viewer.dart';
import 'package:flutter_free_gen_reality/player_audio.dart';
import 'package:flutter_free_gen_reality/player_video.dart';
import 'package:flutter_free_gen_reality/questInfo.dart';
import 'package:flutter_free_gen_reality/questQuiz.dart';
import 'package:flutter_free_gen_reality/questScan.dart';
import 'package:flutter_free_gen_reality/quest_done.dart';
import 'package:flutter_free_gen_reality/quest_done_rating.dart';
import 'package:flutter_free_gen_reality/run_map_quest.dart';
import 'package:flutter_free_gen_reality/run_map_quest_desc.dart';
import 'package:flutter_free_gen_reality/scanbot_sdk.dart';
import 'package:flutter_free_gen_reality/sport_games.dart';
// import 'package:flutter_free_gen_reality/testMap.dart';
// import './services/auth.dart';
import '0_splash_screen.dart';
import '10_profile_ar.dart';
import '10_profile_contacts.dart';
import '10_profile_cooperation.dart';
import '10_profile_edit.dart';
import '10_profile_faq.dart';
import '10_profile_news.dart';
import '10_profile_news_list.dart';
import '10_profile_partner_page.dart';
import '10_profile_partners.dart';
// import '10_profile_partners_partner_1_glusco.dart';
// import '10_profile_partners_partner_2_foxtrot.dart';
// import '10_profile_partners_partner_3_newPost.dart';
import '10_profile_questionnaire.dart';
import '10_profile_qr_code.dart';
import '10_profile_rating.dart';
import '10_profile_store.dart';
// import '10_profile_store_product_1.dart';
// import '10_profile_store_product_2.dart';
// import '10_profile_store_product_3.dart';
import '10_profile_store_product_page.dart';
import '1_layout_1.dart';
import '2_layout_2.dart';
import '3_layout_3.dart';
import '4_enter.dart';
import '4_enter_forgot_password_close.dart';
import '4_enter_register.dart';
import '4_enter_register_close.dart';
import '4_enter_restore_password.dart';
import '4_enter_restore_password_close.dart';
import '5_myBottomBar.dart';
import '6_home_filter.dart';
import '6_home_filter_age.dart';
import '6_home_filter_category.dart';
import '6_home_filter_distance.dart';
import '6_home_filter_location.dart';
// import '6_home_map.dart';
import '6_home_search.dart';

import '6_mission_map.dart'; // DATABASE
import '6_mission_step_info.dart'; // DATABASE
import '6_mission_step_scan_info.dart'; // DATABASE
import '6_mission_step_scan.dart'; // DATABASE
import '6_mission_step_quiz.dart'; // DATABASE
import '6_mission_step_selfie.dart'; // DATABASE
import '6_mission_step_main.dart'; // DATABASE
import '6_mission_quest_done.dart'; // DATABASE

import '7_map.dart';
import '8_camera.dart';
import 'desc_music.dart';
import 'error_404_page.dart';
import 'error_mission_cash.dart';
import 'error_mission_promocode.dart';
import 'error_mission_status.dart';

import 'mission_muromec_desc.dart';
import 'mission_muromec_map.dart';
import 'mission_muromec_step1.dart';
import 'mission_muromec_step2.dart';
import 'mission_muromec_step3.dart';
import 'mission_muromec_step4.dart';
import 'mission_muromec_step5.dart';
import 'mission_muromec_step6.dart';
import 'mission_muromec_step7.dart';
import 'mission_muromec_step8.dart';
import 'mission_muromec_step9.dart';
import 'mission_muromec_step10.dart';
import 'mission_muromec_step11.dart';
import 'mission_muromec_step12.dart';
import 'mission_muromec_step13.dart';
import 'mission_muromec_step14.dart';
import 'mission_muromec_finish.dart';
import 'mission_muromec_quest_done.dart';
import 'mission_muromec_rating.dart';

import 'missions_12_desc5.dart';
import 'missions_12_desc6.dart';
import 'missions_12_quiz2.dart';
import 'missions_12_quiz3.dart';
import 'mission_music.dart';
import 'mission_pinsOnMap_10.dart';
import 'missions_12_desc.dart';
import 'missions_12_desc2.dart';
import 'missions_12_desc7.dart';
import 'missions_12_desc8.dart';
import 'missions_12_quiz4.dart';

import 'missions_12_qr1.dart';
import 'missions_12_qr2.dart';
import 'missions_12_qr3.dart';
import 'missions_12_qr4.dart';
import 'missions_12_qr5.dart';
import 'missions_12_qr6.dart';
import 'missions_12_qr7.dart';
import 'missions_12_qr8.dart';
import 'missions_12_qr9.dart';
import 'missions_12_qr10.dart';
import 'missions_12_qr11.dart';
import 'missions_12_qr12.dart';
// import 'user.dart';
// import 'package:provider/provider.dart';
// import 'dart:async';

// List<CameraDescription> cameras;

import '6_mission_desc.dart';

main() => runApp(
      // EasyLocalization(
      //   path: 'assets/translations',
      //   fallbackLocale: Locale("uk", "UA"),
      //   saveLocale: true,
      //   supportedLocales: [
      //     Locale("uk", "UA"),
      //     Locale("ru", "RU"),
      //     Locale("en", "US")
      //   ],
      //   child:
      MyApp(),
      // ),
    );

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // localizationsDelegates: context.localizationDelegates,
        // supportedLocales: context.supportedLocales,
        // locale: context.locale,
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (BuildContext context) => SplashScreen(),
          '/0_splash_screen_language': (BuildContext context) =>
              SplashScreenLanguage(),
          '/1_layout_1': (BuildContext context) => Layout1(),
          '/2_layout_2': (BuildContext context) => Layout2(),
          '/3_layout_3': (BuildContext context) => Layout3(),
          '/4_enter': (BuildContext context) => Enter(),
          '/4_enter_forgot_password': (BuildContext context) =>
              EnterForgotPassword(),
          '/4_enter_forgot_password_close': (BuildContext context) =>
              EnterForgotPasswordClose(),
          '/4_enter_restore_password': (BuildContext context) =>
              EnterRestorePassword(),
          '/4_enter_restore_password_close': (BuildContext context) =>
              EnterRestorePasswordClose(),
          '/4_enter_register': (BuildContext context) => EnterRegister(),
          '/4_enter_register_close': (BuildContext context) =>
              EnterRegisterClose(),
          '/5_myBottomBar.dart': (BuildContext context) => MyBottomBar(),
          '/6_home_search.dart': (BuildContext context) => HomeSearch(),

          '/6_mission_desc.dart': (BuildContext context) =>
              MissionDescriptionDatabase(), // from DATABASE
          '/6_mission_map.dart': (BuildContext context) =>
              MissionMapDatabase(), // from DATABASE
          '/6_mission_step_info.dart': (BuildContext context) =>
              MissionStepInfo(), // from DATABASE
          '/6_mission_step_scan.dart': (BuildContext context) =>
              MissionStepScan(),
          '/6_mission_step_scan_info.dart': (BuildContext context) =>
              MissionStepScanInfo(), // from DATABASE // from DATABASE
          '/6_mission_step_quiz.dart': (BuildContext context) =>
              MissionStepQuiz(), // from DATABASE
          '/6_mission_step_selfie.dart': (BuildContext context) =>
              MissionStepSelfie(), // from DATABASE
          '/6_mission_step_main.dart': (BuildContext context) =>
              MissionStepMain(), // from DATABASE
          '/6_mission_quest_done.dart': (BuildContext context) =>
              MissionQuestDone(), // from DATABASE

          // '/6_home_map.dart': (BuildContext context) => HomeMap(),
          '/6_home_filter.dart': (BuildContext context) => HomeFilter(),
          '/6_home_filter_location.dart': (BuildContext context) =>
              HomeFilterLocation(),
          '/6_home_filter_distance.dart': (BuildContext context) =>
              HomeFilterDistance(),
          '/6_home_filter_category.dart': (BuildContext context) =>
              HomeFilterCategory(),
          '/6_home_filter_age.dart': (BuildContext context) => HomeFilterAge(),
          '/7_map.dart': (BuildContext context) => Map(),
          '/10_profile_qr_code.dart': (BuildContext context) => ProfileQrCode(),
          '/10_profile_news.dart': (BuildContext context) => ProfileNews(),
          '/10_profile_news_list.dart': (BuildContext context) =>
              ProfileNewsList(),
          '/10_profile_cooperation.dart': (BuildContext context) =>
              ProfileCooperation(),
          '/10_profile_questionnaire.dart': (BuildContext context) =>
              ProfileQuestionnaire(),
          '/10_profile_store.dart': (BuildContext context) => ProfileStore(),
          '/10_profile_store_product_page.dart': (BuildContext context) =>
              ProfileProductPage(),
          // '/10_profile_store_product_1.dart': (BuildContext context) =>
          //     ProfileProduct1(),
          // '/10_profile_store_product_2.dart': (BuildContext context) =>
          //     ProfileProduct2(),
          // '/10_profile_store_product_3.dart': (BuildContext context) =>
          //     ProfileProduct3(),
          '/10_profile_partners.dart': (BuildContext context) =>
              ProfilePartners(),
          '/10_profile_partner_page.dart': (BuildContext context) =>
              ProfilePartnerPage(),
          // '/10_profile_partners_partner_1_glusco.dart':
          //     (BuildContext context) => ProfilePartnersPartner1Glusco(),
          // '/10_profile_partners_partner_2_foxtrot.dart':
          //     (BuildContext context) => ProfilePartnersPartner2Foxtrot(),
          // '/10_profile_partners_partner_3_nova_poshta.dart':
          //     (BuildContext context) => ProfilePartnersPartner3NewPost(),
          '/10_profile_ar.dart': (BuildContext context) => ProfileAr(), // AR
          '/10_profile_rating.dart': (BuildContext context) => ProfileRating(),
          '/10_profile_faq.dart': (BuildContext context) => ProfileFaq(),
          '/10_profile_contacts.dart': (BuildContext context) =>
              ProfileContacts(),
          '/10_profile_aboutus.dart': (BuildContext context) =>
              ProfileAboutUs(),
          '/10_profile_edit.dart': (BuildContext context) => ProfileEdit(),
          '/quest_done.dart': (BuildContext context) => QuestDone(),
          '/quest_done_rating.dart': (BuildContext context) =>
              QuestDoneRating(),
          '/8_camera.dart': (BuildContext context) => Camera(),

          // Mission All steps

          '/sport_games.dart': (BuildContext context) => SportGames(),
          '/questInfo.dart': (BuildContext context) => QuestInfo(),
          '/questQuiz.dart': (BuildContext context) => QuestQuiz(),
          '/questScan.dart': (BuildContext context) => QuestScan(),
          '/run_map_quest_desc.dart': (BuildContext context) =>
              RunMapQuestDesc(),
          '/run_map_quest.dart': (BuildContext context) => RunMapQuest(),

          // 5 new missions
          //-----------------------------------------------------------------------------------

          '/desc_quiz.dart': (BuildContext context) => DescQuiz(),
          '/desc_pinsOnMap.dart': (BuildContext context) => DescPinsOnMap(),
          '/desc_description.dart': (BuildContext context) => DescDescription(),
          '/desc_scanQR.dart': (BuildContext context) => DescScanQR(),
          '/desc_music.dart': (BuildContext context) => DescMusic(),

          '/mission_quiz.dart': (BuildContext context) => MissionQuiz(),
          '/mission_pinsOnMap.dart': (BuildContext context) =>
              MissionPinsOnMap(),
          '/mission_pinsOnMap_10.dart': (BuildContext context) =>
              MissionPinsOnMap10(),
          '/mission_description.dart': (BuildContext context) =>
              MissionDescription(),
          '/mission_scanQR.dart': (BuildContext context) => MissionScanQR(),
          '/mission_music.dart': (BuildContext context) => MissionMusic(),

          //-----------------------------------------------------------------------------------

          // 2 error missions ErrorMissionCach && ErrorMissionStatus

          '/error_mission_cash.dart': (BuildContext context) =>
              ErrorMissionCach(),
          '/error_mission_status.dart': (BuildContext context) =>
              ErrorMissionStatus(),
          '/error_mission_promocode.dart': (BuildContext context) =>
              ErrorMissionPromocode(),

          //-----------------------------------------------------------------------------------

          // '/testMap.dart': (BuildContext context) => MyAppRoutes(), // test map

          //-----------------------------------------------------------------------------------

          '/error_404_page.dart': (BuildContext context) => Error404Page(),

          //-----------------------------------------------------------------------------------

          '/player_audio.dart': (BuildContext context) => PlayerAudio(),
          '/player_video.dart': (BuildContext context) => PlayerVideo(),

          //-----------------------------------------------------------------------------------

          '/scanbot_sdk.dart': (BuildContext context) => ScanbotSDK(),

          //-----------------------------------------------------------------------------------

          '/model_viewer.dart': (BuildContext context) => ModelViewer(),

          //----------------------------------------12missions------------------------------------------- //

          '/missions_12_12_trees.dart': (BuildContext context) =>
              Missions12Trees(),
          '/missions_12_scan1.dart': (BuildContext context) =>
              Missions12Scan1(),
          '/missions_12_scan2.dart': (BuildContext context) =>
              Missions12Scan2(),
          '/missions_12_scan3.dart': (BuildContext context) =>
              Missions12Scan3(),
          '/missions_12_scan4.dart': (BuildContext context) =>
              Missions12Scan4(),
          '/missions_12_scan5.dart': (BuildContext context) =>
              Missions12Scan5(),
          '/missions_12_scan6.dart': (BuildContext context) =>
              Missions12Scan6(),
          '/missions_12_scan7.dart': (BuildContext context) =>
              Missions12Scan7(),
          '/missions_12_scan8.dart': (BuildContext context) =>
              Missions12Scan8(),
          '/missions_12_scan9.dart': (BuildContext context) =>
              Missions12Scan9(),
          '/missions_12_scan10.dart': (BuildContext context) =>
              Missions12Scan10(),
          '/missions_12_scan11.dart': (BuildContext context) =>
              Missions12Scan11(),
          '/missions_12_scan12.dart': (BuildContext context) =>
              Missions12Scan12(),

          '/missions_12_desc.dart': (BuildContext context) => Missions12Desc(),
          '/missions_12_desc2.dart': (BuildContext context) =>
              Missions12Desc2(),
          '/missions_12_desc4.dart': (BuildContext context) =>
              Missions12Desc4(),
          '/missions_12_desc5.dart': (BuildContext context) =>
              Missions12Desc5(),
          '/missions_12_desc6.dart': (BuildContext context) =>
              Missions12Desc6(),
          '/missions_12_desc7.dart': (BuildContext context) =>
              Missions12Desc7(),
          '/missions_12_desc8.dart': (BuildContext context) =>
              Missions12Desc8(),
          '/missions_12_desc9.dart': (BuildContext context) =>
              Missions12Desc9(),
          '/missions_12_desc11.dart': (BuildContext context) =>
              Missions12Desc11(),

          '/missions_12_quiz1.dart': (BuildContext context) =>
              Missions12Quiz1(),
          '/missions_12_quiz2.dart': (BuildContext context) =>
              Missions12Quiz2(),
          '/missions_12_quiz3.dart': (BuildContext context) =>
              Missions12Quiz3(),
          '/missions_12_quiz4.dart': (BuildContext context) =>
              Missions12Quiz4(),
          '/missions_12_quiz5.dart': (BuildContext context) =>
              Missions12Quiz5(),
          '/missions_12_quiz6.dart': (BuildContext context) =>
              Missions12Quiz6(),
          '/missions_12_quiz7.dart': (BuildContext context) =>
              Missions12Quiz7(),
          '/missions_12_quiz8.dart': (BuildContext context) =>
              Missions12Quiz8(),
          '/missions_12_quiz9.dart': (BuildContext context) =>
              Missions12Quiz9(),
          '/missions_12_quiz10.dart': (BuildContext context) =>
              Missions12Quiz10(),
          '/missions_12_quiz11.dart': (BuildContext context) =>
              Missions12Quiz11(),
          '/missions_12_quiz12.dart': (BuildContext context) =>
              Missions12Quiz12(),

          '/missions_12_scan_info1.dart': (BuildContext context) =>
              Missions12ScanInfo1(),
          '/missions_12_scan_info2.dart': (BuildContext context) =>
              Missions12ScanInfo2(),
          '/missions_12_scan_info3.dart': (BuildContext context) =>
              Missions12ScanInfo3(),
          '/missions_12_scan_info4.dart': (BuildContext context) =>
              Missions12ScanInfo4(),
          '/missions_12_scan_info5.dart': (BuildContext context) =>
              Missions12ScanInfo5(),
          '/missions_12_scan_info6.dart': (BuildContext context) =>
              Missions12ScanInfo6(),
          '/missions_12_scan_info7.dart': (BuildContext context) =>
              Missions12ScanInfo7(),
          '/missions_12_scan_info8.dart': (BuildContext context) =>
              Missions12ScanInfo8(),
          '/missions_12_scan_info9.dart': (BuildContext context) =>
              Missions12ScanInfo9(),
          '/missions_12_scan_info10.dart': (BuildContext context) =>
              Missions12ScanInfo10(),
          '/missions_12_scan_info11.dart': (BuildContext context) =>
              Missions12ScanInfo11(),
          '/missions_12_scan_info12.dart': (BuildContext context) =>
              Missions12ScanInfo12(),

          '/missions_12_qr1.dart': (BuildContext context) => ScanQrCode1(),
          '/missions_12_qr2.dart': (BuildContext context) => ScanQrCode2(),
          '/missions_12_qr3.dart': (BuildContext context) => ScanQrCode3(),
          '/missions_12_qr4.dart': (BuildContext context) => ScanQrCode4(),
          '/missions_12_qr5.dart': (BuildContext context) => ScanQrCode5(),
          '/missions_12_qr6.dart': (BuildContext context) => ScanQrCode6(),
          '/missions_12_qr7.dart': (BuildContext context) => ScanQrCode7(),
          '/missions_12_qr8.dart': (BuildContext context) => ScanQrCode8(),
          '/missions_12_qr9.dart': (BuildContext context) => ScanQrCode9(),
          '/missions_12_qr10.dart': (BuildContext context) => ScanQrCode10(),
          '/missions_12_qr11.dart': (BuildContext context) => ScanQrCode11(),
          '/missions_12_qr12.dart': (BuildContext context) => ScanQrCode12(),

          // MUROMEC

          '/mission_muromec_desc.dart': (BuildContext context) =>
              MissionsMuromecDesc(),
          '/mission_muromec_map.dart': (BuildContext context) =>
              MissionMuromecMap(),

          '/mission_muromec_step1.dart': (BuildContext context) =>
              MissionMuromecStep1(),
          '/mission_muromec_step2.dart': (BuildContext context) =>
              MissionMuromecStep2(),
          '/mission_muromec_step3.dart': (BuildContext context) =>
              MissionMuromecStep3(),
          '/mission_muromec_step4.dart': (BuildContext context) =>
              MissionMuromecStep4(),
          '/mission_muromec_step5.dart': (BuildContext context) =>
              MissionMuromecStep5(),
          '/mission_muromec_step6.dart': (BuildContext context) =>
              MissionMuromecStep6(),
          '/mission_muromec_step7.dart': (BuildContext context) =>
              MissionMuromecStep7(),
          '/mission_muromec_step8.dart': (BuildContext context) =>
              MissionMuromecStep8(),
          '/mission_muromec_step9.dart': (BuildContext context) =>
              MissionMuromecStep9(),
          '/mission_muromec_step10.dart': (BuildContext context) =>
              MissionMuromecStep10(),
          '/mission_muromec_step11.dart': (BuildContext context) =>
              MissionMuromecStep11(),
          '/mission_muromec_step12.dart': (BuildContext context) =>
              MissionMuromecStep12(),
          '/mission_muromec_step13.dart': (BuildContext context) =>
              MissionMuromecStep13(),
          '/mission_muromec_step14.dart': (BuildContext context) =>
              MissionMuromecStep14(),

          '/mission_muromec_finish.dart': (BuildContext context) =>
              MissionMuromecFinish(),
          '/mission_muromec_quest_done.dart': (BuildContext context) =>
              MissionMuromecQuestDone(),
          '/mission_muromec_rating.dart': (BuildContext context) =>
              MissionMuromecRating(),
        });
  }
}

// -------------------------------------------------------------------------------------------
