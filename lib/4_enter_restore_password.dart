import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

class EnterRestorePassword extends StatefulWidget {
  @override
  _EnterRestorePasswordState createState() => _EnterRestorePasswordState();
}

class _EnterRestorePasswordState extends State<EnterRestorePassword> {
  var _blankFocusNode = new FocusNode();
  final formKey = GlobalKey<FormState>();

  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController2 =
      new TextEditingController();

  savePassword() {
    if (formKey.currentState.validate() &&
        passwordTextEditingController.text ==
            passwordTextEditingController2.text) {
      print("saved");
      passwordTextEditingController.clear();
      passwordTextEditingController2.clear();
      // Navigator.pushNamed(context, '/5_myBottomBar.dart');
      Navigator.pushNamed(context, '/4_enter_restore_password_close');
    }
    if (passwordTextEditingController.text !=
        passwordTextEditingController2.text) {
      print('Passwords must be the same');
    }
  }

  register() {
    Navigator.pushNamed(context, '/4_enter_register');
  }

  FacebookLogin fbLogin = FacebookLogin();

  Future<void> signUpWithFacebook() async {
    try {
      var facebookLogin = new FacebookLogin();
      var result = await facebookLogin.logIn(['email']);
      if (result.status == FacebookLoginStatus.loggedIn) {
        final AuthCredential credential = FacebookAuthProvider.getCredential(
            accessToken: result.accessToken.token);
        final FirebaseUser user =
            (await FirebaseAuth.instance.signInWithCredential(credential)).user;
        print('signed in ' + user.displayName);
        Navigator.of(context).pushReplacementNamed('/5_myBottomBar.dart');
        setState(() {
          isSignIn = true;
        });
        return user;
      }
    } catch (e) {
      print(e.message);
    }
  }

  bool isSignIn = false;
  bool isVisible = false;
  bool isVisible2 = false;

  GoogleSignIn _googleSignIn = new GoogleSignIn();
  FirebaseUser _user;
  FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> signUpWithGoogle() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

    AuthResult result = (await _auth.signInWithCredential(credential));

    _user = result.user;
    print('signed in ' + _user.displayName);
    setState(() {
      isSignIn = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Hexcolor('#7D5AC2'),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(_blankFocusNode);
          },
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              margin: EdgeInsets.only(right: 20, left: 20),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 60),
                      child: Image.asset(
                        'assets/fg_images/4_enter_logo.png',
                        width: 140,
                        height: 126,
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 30, top: 30),
                            child: Text(
                              'Відновлення пароля',
                              style: TextStyle(
                                color: Hexcolor('#FFFFFF'),
                                fontSize: 25,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Arial',
                                letterSpacing: 1.28,
                              ),
                            ),
                          ),
                          Form(
                            key: formKey,
                            child: Column(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        bottom: 7,
                                      ),
                                      child: Text(
                                        "Новий пароль",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'Arial',
                                          color: Colors.white,
                                          fontSize: 17,
                                        ),
                                      ),
                                    ),
                                    Stack(
                                      overflow: Overflow.visible,
                                      children: [
                                        TextFormField(
                                          obscuringCharacter: '*',
                                          obscureText: isVisible ? false : true,
                                          validator: (value) {
                                            // return value.length > 6
                                            //     ? null
                                            //     : "Пожалуйста введите пароль не меньше 6 символов";
                                            if (value.length < 6) {
                                              return "Будь-ласка введіть пароль не менше 6 символів";
                                            }
                                            if (passwordTextEditingController
                                                    .text !=
                                                passwordTextEditingController2
                                                    .text) {
                                              return "Паролі не співпадають";
                                            }
                                            if (passwordTextEditingController
                                                        .text !=
                                                    passwordTextEditingController2
                                                        .text &&
                                                value.length < 6) {
                                              return "Паролі не співпадають.\nБудь-ласка введіть пароль не менше 6 символів";
                                            }
                                            return null;
                                          },
                                          controller:
                                              passwordTextEditingController,
                                          textAlign: TextAlign.left,
                                          decoration: InputDecoration(
                                            hintText: "Введіть пароль",
                                            contentPadding: new EdgeInsets.only(
                                                left: 20,
                                                top: 21.5,
                                                bottom: 21.5),
                                            hintStyle: TextStyle(
                                                color: Hexcolor('#D3D3D3'),
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: 'Arial'),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#FE6802'),
                                                width: 1,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Colors.white54,
                                                width: 1,
                                              ),
                                            ),
                                            focusedErrorBorder:
                                                OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#FF1E1E'),
                                                width: 1,
                                              ),
                                            ),
                                            errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#FF1E1E'),
                                                width: 1,
                                              ),
                                            ),
                                            fillColor: Colors.white,
                                            filled: true,
                                          ),
                                          style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial'),
                                        ),
                                        Positioned(
                                          right: 22.4,
                                          top: 18,
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isVisible = !isVisible;
                                              });
                                            },
                                            child: !isVisible
                                                ? Image.asset(
                                                    'assets/fg_images/4_enter_visible.png',
                                                    color: Hexcolor('#9B9B9B'),
                                                    width: 24,
                                                    height: 24,
                                                  )
                                                : Image.asset(
                                                    'assets/fg_images/4_enter_hidden.png',
                                                    color: Hexcolor('#9B9B9B'),
                                                    width: 24,
                                                    height: 24,
                                                  ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),

                                SizedBox(
                                  height: 15,
                                ),

                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        bottom: 7,
                                      ),
                                      child: Text(
                                        "Підтвердіть пароль",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'Arial',
                                          color: Colors.white,
                                          fontSize: 17,
                                        ),
                                      ),
                                    ),
                                    Stack(
                                      overflow: Overflow.visible,
                                      children: [
                                        TextFormField(
                                          obscuringCharacter: '*',
                                          obscureText:
                                              isVisible2 ? false : true,
                                          validator: (value) {
                                            // return value.length > 6
                                            //     ? null
                                            //     : "Пожалуйста введите пароль не меньше 6 символов";
                                            if (value.length < 6) {
                                              return "Будь-ласка введіть пароль не менше 6 символів";
                                            }
                                            if (passwordTextEditingController
                                                    .text !=
                                                passwordTextEditingController2
                                                    .text) {
                                              return "Паролі не співпадають";
                                            }
                                            return null;
                                          },
                                          controller:
                                              passwordTextEditingController2,
                                          textAlign: TextAlign.left,
                                          decoration: InputDecoration(
                                            hintText: "Введіть пароль",
                                            contentPadding: new EdgeInsets.only(
                                                left: 20,
                                                top: 21.5,
                                                bottom: 21.5),
                                            hintStyle: TextStyle(
                                                color: Hexcolor('#D3D3D3'),
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: 'Arial'),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#FE6802'),
                                                width: 1,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Colors.white54,
                                                width: 1,
                                              ),
                                            ),
                                            focusedErrorBorder:
                                                OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#FF1E1E'),
                                                width: 1,
                                              ),
                                            ),
                                            errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(14.0),
                                              borderSide: BorderSide(
                                                color: Hexcolor('#FF1E1E'),
                                                width: 1,
                                              ),
                                            ),
                                            fillColor: Colors.white,
                                            filled: true,
                                          ),
                                          style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Arial'),
                                        ),
                                        Positioned(
                                          right: 22.4,
                                          top: 18,
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isVisible2 = !isVisible2;
                                              });
                                            },
                                            child: !isVisible2
                                                ? Image.asset(
                                                    'assets/fg_images/4_enter_visible.png',
                                                    color: Hexcolor('#9B9B9B'),
                                                    width: 24,
                                                    height: 24,
                                                  )
                                                : Image.asset(
                                                    'assets/fg_images/4_enter_hidden.png',
                                                    color: Hexcolor('#9B9B9B'),
                                                    width: 24,
                                                    height: 24,
                                                  ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),

                                // ),

                                Container(
                                  margin: EdgeInsets.only(top: 24),
                                  height: 60,
                                  width: MediaQuery.of(context).size.width - 40,
                                  child: RaisedButton(
                                    color: Hexcolor('#FE6802'),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(14.0)),
                                    child: Text("Відновити",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Arial',
                                            color: Colors.white,
                                            letterSpacing: 1.09,
                                            fontSize: 17)),
                                    onPressed: () {
                                      savePassword();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
