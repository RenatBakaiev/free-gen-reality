// ----------------------------------------------NEW MAP 25.09.20--------------------------------------------------------------------
// https://www.youtube.com/watch?v=Hg8CT3ysFjY&t=528s - GEOLOCATOR

import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';
import 'package:flutter_free_gen_reality/services/services.dart';
import '6_home_missions_json.dart';
import 'dart:convert' show utf8;
import '10_profile_currencies_json.dart';
import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'package:flutter_html/flutter_html.dart';

class Map extends StatefulWidget {
  // https://www.youtube.com/watch?v=2pFyy-ARuhw
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  Completer<GoogleMapController> _controller = Completer();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  bool isApiCallProcess = false;

  var currentLocation;
  bool mapToggle = false;
  bool isPromoFieldVisible = false;
  final formKey = GlobalKey<FormState>();
  TextEditingController promocodeTextEditingController =
      new TextEditingController();
  bool isPromoRight = false;

  bool isPressed0 = true;
  bool isPressed5 = false;
  bool isPressed10 = false;
  bool isPressed15 = false;

  bool isIconSelected = false;

  double zoomVal = 17.0;
  bool isHidden = false;

  BitmapDescriptor _missionIcon;
  BitmapDescriptor _missionIcon2;

  // BitmapDescriptor _markerIcon;
  // BitmapDescriptor _markerIcon2;
  // BitmapDescriptor _markerIcon3;
  // BitmapDescriptor _markerIcon4;
  // BitmapDescriptor _markerIcon5;
  // BitmapDescriptor _markerIcon6;
  // BitmapDescriptor _markerIcon7;
  // BitmapDescriptor _markerIcon8;

  // BitmapDescriptor _markerIcon8;
  // BitmapDescriptor _markerIcon9;
  Set<Marker> _markers =
      HashSet<Marker>(); //  https://www.youtube.com/watch?v=N0NfbhF2A3g&t=123s

  String userStatus;

  getUserStatus() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userStatus = sharedPreferences.get("userStatus");
  }

  List<Mission> missions;
  bool loading = true;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';

  List<Publication> currencies;
  getCurrencyList() {
    ServicesCurrencies.getCurrencies().then((list) {
      setState(() {
        currencies = list.publication;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getCurrencyList();

    ServicesMissions.getMissions().then((list) {
      setState(() {
        missions = list.missions;
        print('MISSIONS LENGTH');
        print(missions.length);
        getUserStatus();
        loading = false;
      });
      Geolocator.getCurrentPosition().then((currloc) {
        setState(() {
          currentLocation = currloc;
          mapToggle = true;
        });
      });
    });

    // _setMarkerIcon();
    // _setMarkerIcon2();
    // _setMarkerIcon3();
    // _setMarkerIcon4();
    // _setMarkerIcon5();
    // _setMarkerIcon6();
    // _setMarkerIcon7();
    // _setMarkerIcon8();
    // _setMarkerIcon9();
    _setMissionIcon();
    _setMissionIcon2();
  }

  void _setMissionIcon() async {
    _missionIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/fg_images/mission_muromec_start.png');
  }

  void _setMissionIcon2() async {
    _missionIcon2 = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/fg_images/mission_muromec_done.png');
  }

  // void _setMarkerIcon() async {
  //   _markerIcon = await BitmapDescriptor.fromAssetImage(
  //       ImageConfiguration(), 'assets/fg_images/z12_missions_12_treesMAP.png');
  //   // isIconSelected
  //   //     ? 'assets/fg_images/7_map_quest1.5_selected.png' // yaroslavov val 16
  //   //     : 'assets/fg_images/7_map_quest1.5.png');
  // }

  // void _setMarkerIcon2() async {
  //   _markerIcon2 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_kiev.png',
  //   );
  // }

  // void _setMarkerIcon3() async {
  //   _markerIcon3 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_zombie.png',
  //   );
  // }

  // void _setMarkerIcon4() async {
  //   _markerIcon4 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_trueh.png',
  //   );
  // }

  // void _setMarkerIcon5() async {
  //   _markerIcon5 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_rus.png',
  //   );
  // }

  // void _setMarkerIcon6() async {
  //   _markerIcon6 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_fantom.png',
  //   );
  // }

  // void _setMarkerIcon7() async {
  //   _markerIcon7 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_podol.png',
  //   );
  // }

  // void _setMarkerIcon8() async {
  //   _markerIcon8 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_mission_muromec.png',
  //   );
  // }

  // void _setMarkerIcon9() async {
  //   _markerIcon9 = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     // 'assets/fg_images/run_map_quest_pic_lion.png',
  //     'assets/fg_images/7_map_quest1.5.png',
  //   );
  // }

  Future<void> _set0km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _set5km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _set10km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _set15km(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: zoomVal,
        target: LatLng(currentLocation.latitude, currentLocation.longitude))));
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _getMyLocation(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: zoomVal)));
  }

  Future<void> _goToQuest(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 17)));
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(overflow: Overflow.visible, children: [
          Container(
            margin: EdgeInsets.only(top: 115),
            child: mapToggle
                ? GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: LatLng(currentLocation.latitude,
                            currentLocation.longitude),
                        zoom: 17.0),
                    mapType: MapType.normal,
                    tiltGesturesEnabled: true,
                    compassEnabled: true,
                    scrollGesturesEnabled: true,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: false, // off default buttons
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false, // off default my location
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);

                      setState(() {
                        // Markers from DATABASE
                        for (int i = 0; i < missions.length; i++) {
                          print("mission");
                          Mission mission = missions[i];
                          var missionName =
                              utf8.decode(mission.titleUa.runes.toList());
                          var missionId = mission.id;
                          var missionLatitude =
                              double.parse(mission.location.latitude);
                          var missionLongitude =
                              double.parse(mission.location.longitude);
                          var missionProgress = mission.progress;

                          // var missionImage =
                          //     utf8.decode(mission.imageUrl.runes.toList());
                          // BitmapDescriptor _missionImage;
                          // void _setIcon() async {
                          //   _missionImage =
                          //       await BitmapDescriptor.fromAssetImage(
                          //           ImageConfiguration(),
                          //           mission.imageUrl == "" ||
                          //                   mission.imageUrl == null
                          //               ? '$link' + 'default.png'
                          //               : '$link' + missionImage);
                          // }
                          // _setIcon();

                          _markers.add(Marker(
                            infoWindow: InfoWindow(title: missionName),
                            anchor: const Offset(0.5, 0.5),
                            markerId: MarkerId("$missionId"),
                            position: LatLng(missionLatitude, missionLongitude),
                            // icon: _missionImage,
                            icon: missionProgress == "FINISHED"
                                ? _missionIcon2
                                : _missionIcon,
                            onTap: () {
                              isHidden = !isHidden;
                            },
                          ));
                        }

                        // Markers from APP
                        // _markers.add(Marker(
                        //     markerId: MarkerId("0"),
                        //     position: LatLng(50.453346, 30.515856),
                        //     // infoWindow: InfoWindow(
                        //     // anchor: Offset(0.5, 3),
                        //     // anchor: Offset(3.2, 1.6),
                        //     // title: 'Sport Quest',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });

                        //       print('click');
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("1"),
                        //     position: LatLng(50.455341, 30.520690),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Corporative Quest',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon2,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("2"),
                        //     position: LatLng(50.424457, 30.611301),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Sport Games',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon3,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("3"),
                        //     position: LatLng(50.455105, 30.532545),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Sport Games',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon4,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("4"),
                        //     position: LatLng(50.443935, 30.551324),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Sport Games',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon5,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("5"),
                        //     position: LatLng(50.476046, 30.428375),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Sport Games',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon6,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("6"),
                        //     position: LatLng(50.460703, 30.514517),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Sport Games',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon7,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));

                        // _markers.add(Marker(
                        //     markerId: MarkerId("7"),
                        //     position: LatLng(50.49676, 30.54246),
                        //     // infoWindow: InfoWindow(
                        //     //   title: 'Sport Games',
                        //     // snippet: 'MyQuestMarker',
                        //     // ),
                        //     icon: _markerIcon8,
                        //     onTap: () {
                        //       setState(() {
                        //         isHidden = !isHidden;
                        //       });
                        //     }));
                      });
                    },
                    markers: _markers,
                  )
                : Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Hexcolor('#7D5AC2'),
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        Hexcolor('#FE6802'),
                      ),
                    ),
                  ),
          ),
          Positioned(
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: 50,
                  // left: 15,
                  right: 15,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        IconButton(
                          icon: Image.asset(
                            'assets/fg_images/6_home_search_back.png',
                            width: 10,
                            height: 19,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                        Text(
                          'Мапа',
                          style: TextStyle(
                              fontSize: 37,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        // SizedBox(
                        //   width: 40,
                        //   height: 55,
                        //   child: IconButton(
                        //     icon: Image.asset(
                        //       'assets/fg_images/6_home_logo_search.png',
                        //       width: 19,
                        //       height: 19,
                        //     ),
                        //     onPressed: () {
                        //       Navigator.pushNamed(
                        //           context, '/6_home_search.dart');
                        //     },
                        //   ),
                        // ),
                        // SizedBox(
                        //   width: 40,
                        //   height: 55,
                        //   child: IconButton(
                        //     icon: Image.asset(
                        //       'assets/fg_images/6_home_logo_filter.png',
                        //       width: 17,
                        //       height: 18,
                        //     ),
                        //     onPressed: () {
                        //       Navigator.pushNamed(
                        //           context, '/6_home_filter.dart');
                        //     },
                        //   ),
                        // ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 290,
            right: 20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      zoomVal++;
                      _plus(zoomVal);
                    },
                    child: Image.asset(
                      'assets/fg_images/7_map_icon_zoom_plus.png',
                      width: 50,
                      height: 50,
                    )),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    zoomVal--;
                    _minus(zoomVal);
                  },
                  child: Image.asset(
                    'assets/fg_images/7_map_icon_zoom_minus.png',
                    width: 50,
                    height: 50,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    _getMyLocation(zoomVal);
                  },
                  child: Image.asset(
                    'assets/fg_images/7_map_icon_location.png',
                    width: 50,
                    height: 50,
                  ),
                ),
              ],
            ),
          ),

          // from DATABASE
          loading
              ? Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Hexcolor('#7D5AC2'),
                    valueColor: new AlwaysStoppedAnimation<Color>(
                      Hexcolor('#FE6802'),
                    ),
                  ),
                )
              : Positioned(
                  top: 137,
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 117,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: missions.length,
                      itemBuilder: (BuildContext context, int index) {
                        Mission mission = missions[index];

                        var missionName =
                            utf8.decode(mission.titleUa.runes.toList());
                        var missionDesc =
                            utf8.decode(mission.descriptionUa.runes.toList());
                        var missionImage =
                            utf8.decode(mission.imageUrl.runes.toList());
                        var missionLatitude =
                            double.parse(mission.location.latitude);
                        var missionLongitude =
                            double.parse(mission.location.longitude);
                        int missionId = mission.id;
                        var missionProgress = mission.progress;
                        var missionAccessType = mission.accessType;

                        var missionAccessGranted; // ACCESS BY STATUS
                        List<String> missionStatuses = new List<String>();

                        if (missionAccessType == 'BYSTATUS') {
                          for (final status in mission.statuses) {
                            if (status.active == true) {
                              missionStatuses.add(
                                  utf8.decode(status.titleUa.runes.toList()));
                            }
                          }
                          print(
                              '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
                          print(missionStatuses);
                          if (missionStatuses.contains(userStatus)) {
                            missionAccessGranted = true;
                          } else {
                            missionAccessGranted = false;
                          }
                        }

                        var missionPromoCode; // ACCESS BY PROMO
                        var missionUnlocked = false;

                        if (missionAccessType == 'PROMO') {
                          missionPromoCode =
                              utf8.decode(mission.promoCode.runes.toList());
                          print(missionPromoCode);
                        }

                        var missionPoints; // ACCESS BY POINTS
                        var missionCurrency;

                        var userPointsforMission;
                        getUserData() async {
                          SharedPreferences sharedPreferences =
                              await SharedPreferences.getInstance();
                          String email = sharedPreferences.get("email");
                          String token = sharedPreferences.get("token");
                          String url =
                              'http://generation-admin.ehub.com.ua/api/user/find?email=' +
                                  '$email';
                          Future<User> getUser() async {
                            try {
                              final response = await http.get(url, headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'Authorization': 'Bearer $token',
                              });
                              var responseJson = jsonDecode(response.body);
                              if (response.statusCode == 200) {
                                for (int i = 0;
                                    i < responseJson["accounts"].length;
                                    i++) {
                                  if (missionCurrency ==
                                      responseJson["accounts"][i]["currency"]) {
                                    userPointsforMission =
                                        responseJson["accounts"][i]
                                            ["accountBalance"];
                                  }
                                }
                                print(
                                    '**************************** USER POINTS ********************************************');
                                print(userPointsforMission);
                                final User user = userFromJson(response.body);
                                loading = false;
                                return user;
                              } else {
                                return User();
                              }
                            } catch (e) {
                              print(e);
                              return User();
                            }
                          }

                          getUser();
                        }

                        if (missionAccessType == 'PAID') {
                          missionPoints = mission.price;
                          print(
                              '**************************** MISSION PRICE ********************************************');
                          for (int i = 0; i < currencies.length; i++) {
                            if (mission.priceCurrency ==
                                currencies[i].id.toString()) {
                              missionCurrency =
                                  currencies[i].contentShortUa.toUpperCase();
                            }
                          }
                          print(missionPoints);
                          print(missionCurrency);
                          getUserData();
                        }

                        void launchUrl(String url) async {
                          if (await canLaunch(url)) {
                            await launch(url);
                          } else {
                            throw 'Could not open Url';
                          }
                        }

                        showWindowQuizDone() {
                          showDialog(
                              barrierDismissible: false,
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              context: context,
                              builder: (context) {
                                return Dialog(
                                  insetPadding: EdgeInsets.only(
                                    left: 20,
                                    right: 20,
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      Container(
                                        height: 250,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                top: 20,
                                              ),
                                              child: Text(
                                                'Завдання пройдене',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 24,
                                                  fontFamily: 'Arial',
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            ),
                                            Text(
                                              'Ви не можете повторно пройти це завдання',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Hexcolor('#747474'),
                                                fontSize: 16,
                                                fontFamily: 'Arial',
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                left: 20,
                                                right: 20,
                                                bottom: 20,
                                              ),
                                              height: 60,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  80,
                                              child: RaisedButton(
                                                child: Text(
                                                  "Закрити",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 17,
                                                      fontFamily: 'Arial',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      letterSpacing: 1.05),
                                                ),
                                                color: Hexcolor('#FE6802'),
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                14.0)),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              });
                        }

                        showLockedWindow() {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                var _blankFocusNode2 = new FocusNode();
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                    insetPadding: EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(_blankFocusNode2);
                                      },
                                      child: Container(
                                        height: 400,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              margin: EdgeInsets.only(
                                                top: 20,
                                                bottom: 15,
                                              ),
                                              child: Text(
                                                'Місія недоступна',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Arial',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: SingleChildScrollView(
                                                physics:
                                                    BouncingScrollPhysics(),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                      ),
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 165,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          20)),
                                                          image:
                                                              DecorationImage(
                                                            image: NetworkImage(mission
                                                                            .imageUrl ==
                                                                        "" ||
                                                                    mission.imageUrl ==
                                                                        null
                                                                ? '$link' +
                                                                    'default.png'
                                                                : '$link' +
                                                                    missionImage),
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                      ),
                                                      child: Text(
                                                        missionName,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontFamily: 'Arial',
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                left: 20,
                                                right: 20,
                                                bottom: 20,
                                              ),
                                              height: 60,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: RaisedButton(
                                                child: Text(
                                                  "Закрити",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: 'Arial',
                                                  ),
                                                ),
                                                color: Hexcolor('#FE6802'),
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                14.0)),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                });
                              });
                        }

                        showLockedStatusWindow() {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                var _blankFocusNode2 = new FocusNode();
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                    insetPadding: EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(_blankFocusNode2);
                                      },
                                      child: Container(
                                        height: 500,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              margin: EdgeInsets.only(
                                                top: 20,
                                              ),
                                              child: Text(
                                                'Місія недоступна',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Arial',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: SingleChildScrollView(
                                                physics:
                                                    BouncingScrollPhysics(),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 15,
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            'Місія доступна для статусу',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#747474'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                          Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                top: 5,
                                                                left: 20,
                                                                right: 20,
                                                                bottom: 5,
                                                              ),
                                                              height: 45,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width -
                                                                  40,
                                                              child: GridView
                                                                  .builder(
                                                                      physics:
                                                                          BouncingScrollPhysics(),
                                                                      gridDelegate:
                                                                          SliverGridDelegateWithFixedCrossAxisCount(
                                                                        crossAxisCount:
                                                                            3,
                                                                        childAspectRatio:
                                                                            (100 /
                                                                                20),
                                                                      ),
                                                                      itemCount:
                                                                          missionStatuses
                                                                              .length,
                                                                      itemBuilder:
                                                                          (BuildContext context,
                                                                              int index) {
                                                                        var status =
                                                                            missionStatuses[index];
                                                                        return Center(
                                                                          child:
                                                                              Text(
                                                                            status,
                                                                            style:
                                                                                TextStyle(
                                                                              color: Hexcolor('#FF1E1E'),
                                                                              fontSize: 20,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w700,
                                                                            ),
                                                                          ),
                                                                        );
                                                                      }))
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                      ),
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 165,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          20)),
                                                          image:
                                                              DecorationImage(
                                                            image: NetworkImage(mission
                                                                            .imageUrl ==
                                                                        "" ||
                                                                    mission.imageUrl ==
                                                                        null
                                                                ? '$link' +
                                                                    'default.png'
                                                                : '$link' +
                                                                    missionImage),
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                      ),
                                                      child: Text(
                                                        missionName,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontFamily: 'Arial',
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 15,
                                                      ),
                                                      child: Text(
                                                        'Місія недоступна\nдля вашого статусу',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#747474'),
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily: 'Arial',
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                left: 20,
                                                right: 20,
                                                bottom: 20,
                                              ),
                                              height: 60,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: RaisedButton(
                                                child: Text(
                                                  "Закрити",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: 'Arial',
                                                  ),
                                                ),
                                                color: Hexcolor('#FE6802'),
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                14.0)),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                });
                              });
                        }

                        unlockMission() {
                          Future.delayed(Duration(seconds: 1), () {
                            setState(() {
                              missionUnlocked = true;
                            });
                          });
                        }

                        showUnlockWindowPromocode() {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                      insetPadding: EdgeInsets.only(
                                        left: 20,
                                        right: 20,
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Stack(
                                        overflow: Overflow.visible,
                                        children: [
                                          Container(
                                            // width: MediaQuery.of(context).size.width - 100,
                                            height: 470,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin: EdgeInsets.only(
                                                    top: 20,
                                                  ),
                                                  child: Text(
                                                    'Поздоровляємо!',
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 24,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: SingleChildScrollView(
                                                    physics:
                                                        BouncingScrollPhysics(),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            top: 15,
                                                            bottom: 15,
                                                          ),
                                                          child: Text(
                                                            'Ви розблокували місію',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#59B32D'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            left: 20,
                                                            right: 20,
                                                          ),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 165,
                                                          decoration:
                                                              BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius.all(
                                                                          Radius.circular(
                                                                              20)),
                                                                  image:
                                                                      DecorationImage(
                                                                    image: NetworkImage(mission.imageUrl ==
                                                                                "" ||
                                                                            mission.imageUrl ==
                                                                                null
                                                                        ? '$link' +
                                                                            'default.png'
                                                                        : '$link' +
                                                                            missionImage),
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  )),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            top: 15,
                                                            bottom: 15,
                                                          ),
                                                          child: Text(
                                                            missionName,
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#1E2E45'),
                                                              fontSize: 20,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              fontFamily:
                                                                  'Arial',
                                                            ),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Text(
                                                          'Вам доступна нова місія',
                                                          style: TextStyle(
                                                            color: Hexcolor(
                                                                '#747474'),
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontFamily: 'Arial',
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 20,
                                                      right: 20,
                                                      left: 20),
                                                  child: Container(
                                                    height: 60,
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width -
                                                            40,
                                                    child: RaisedButton(
                                                      child: Text(
                                                        "Почати місію",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 17,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                      shape: new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  14.0)),
                                                      onPressed: () async {
                                                        // Navigator.of(context)
                                                        //     .pop();
                                                        // Navigator.of(context)
                                                        //     .pop();
                                                        // unlockMission();
                                                        final SharedPreferences
                                                            sharedPreferences =
                                                            await SharedPreferences
                                                                .getInstance();
                                                        sharedPreferences
                                                            .setInt('missionId',
                                                                missionId);
                                                        Navigator.pushNamed(
                                                            context,
                                                            '/6_mission_map.dart');
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            top: -130,
                                            right: -30,
                                            child: Image.asset(
                                              'assets/fg_images/error_mission_icon_unlock.png',
                                              width: 200,
                                              height: 200,
                                            ),
                                          )
                                        ],
                                      ));
                                });
                              });
                        }

                        showLockedWindowWithPromocode() {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                var _blankFocusNode2 = new FocusNode();
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                    insetPadding: EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(_blankFocusNode2);
                                      },
                                      child: Container(
                                        // width: MediaQuery.of(context).size.width - 100,
                                        height: 550,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              margin: EdgeInsets.only(
                                                top: 20,
                                              ),
                                              child: Text(
                                                'Місія недоступна',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Arial',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: SingleChildScrollView(
                                                physics:
                                                    BouncingScrollPhysics(),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 20,
                                                        bottom: 15,
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            'Місія доступна тільки',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#747474'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                'по ',
                                                                style:
                                                                    TextStyle(
                                                                  color: Hexcolor(
                                                                      '#747474'),
                                                                  fontSize: 20,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                ),
                                                              ),
                                                              Text(
                                                                'промокоду',
                                                                style:
                                                                    TextStyle(
                                                                  color: Hexcolor(
                                                                      '#FF1E1E'),
                                                                  fontSize: 20,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                      ),
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 165,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          20)),
                                                          image:
                                                              DecorationImage(
                                                            image: NetworkImage(mission
                                                                            .imageUrl ==
                                                                        "" ||
                                                                    mission.imageUrl ==
                                                                        null
                                                                ? '$link' +
                                                                    'default.png'
                                                                : '$link' +
                                                                    missionImage),
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 15,
                                                      ),
                                                      child: Text(
                                                        missionName,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontFamily: 'Arial',
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            // height: 60,
                                                            width: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .width /
                                                                    2 -
                                                                45,
                                                            child: Form(
                                                              key: formKey,
                                                              child:
                                                                  TextFormField(
                                                                onChanged:
                                                                    (val) {},
                                                                validator:
                                                                    (value) {
                                                                  return value ==
                                                                          missionPromoCode
                                                                      ? null
                                                                      : 'Невірний промокод';
                                                                },
                                                                controller:
                                                                    promocodeTextEditingController,
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                decoration:
                                                                    InputDecoration(
                                                                  hintText:
                                                                      "Промокод",
                                                                  contentPadding: new EdgeInsets
                                                                          .only(
                                                                      left: 20,
                                                                      top: 20.5,
                                                                      bottom:
                                                                          20.5),
                                                                  hintStyle:
                                                                      TextStyle(
                                                                    color: Hexcolor(
                                                                        '#D3D3D3'),
                                                                    fontSize:
                                                                        17,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontFamily:
                                                                        'Arial',
                                                                  ),
                                                                  focusedBorder:
                                                                      OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            14.0),
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: Hexcolor(isPromoRight
                                                                          ? '#59B32D'
                                                                          : '#FE6802'),
                                                                      width: 1,
                                                                    ),
                                                                  ),
                                                                  enabledBorder:
                                                                      OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            14.0),
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: Hexcolor(
                                                                          '#E6E6E6'),
                                                                      width: 1,
                                                                    ),
                                                                  ),
                                                                  focusedErrorBorder:
                                                                      OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            14.0),
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: Colors
                                                                          .red,
                                                                      width: 1,
                                                                    ),
                                                                  ),
                                                                  errorBorder:
                                                                      OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            14.0),
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: Colors
                                                                          .red,
                                                                      width: 1,
                                                                    ),
                                                                  ),
                                                                  fillColor:
                                                                      Colors
                                                                          .white,
                                                                  filled: true,
                                                                ),
                                                                style:
                                                                    TextStyle(
                                                                  color: Hexcolor(
                                                                      '#1E2E45'),
                                                                  fontSize: 17,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontFamily:
                                                                      'Arial',
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Container(
                                                            height: 60,
                                                            width: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .width /
                                                                    2 -
                                                                45,
                                                            child: RaisedButton(
                                                              child: Text(
                                                                "Застосувати",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 17,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                  fontFamily:
                                                                      'Arial',
                                                                ),
                                                              ),
                                                              color: Hexcolor(
                                                                  '#FE6802'),
                                                              shape: new RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      new BorderRadius
                                                                              .circular(
                                                                          14.0)),
                                                              onPressed: () {
                                                                if (formKey
                                                                    .currentState
                                                                    .validate()) {
                                                                  setState(() {
                                                                    isPromoRight =
                                                                        true;
                                                                  });
                                                                }
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    isPromoRight
                                                        ? Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              left: 40,
                                                              top: 7,
                                                              bottom: 15,
                                                            ),
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: Text(
                                                              'Правильний промокод',
                                                              style: TextStyle(
                                                                color: Hexcolor(
                                                                    '#59B32D'),
                                                                fontFamily:
                                                                    'Arial',
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                              ),
                                                            ))
                                                        : Container(
                                                            height: 15,
                                                          ),
                                                    !isPromoRight
                                                        ? SizedBox(
                                                            height: 30,
                                                          )
                                                        : SizedBox(
                                                            height: 15,
                                                          )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                left: 20,
                                                right: 20,
                                                bottom: 20,
                                              ),
                                              height: 60,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: RaisedButton(
                                                child: Text(
                                                  "Закрити",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: 'Arial',
                                                  ),
                                                ),
                                                color: Hexcolor('#FE6802'),
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                14.0)),
                                                onPressed: () {
                                                  if (isPromoRight) {
                                                    showUnlockWindowPromocode();
                                                    setState(() {
                                                      missionUnlocked = true;
                                                    });
                                                  } else {
                                                    Navigator.of(context).pop();
                                                  }
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                });
                              });
                        }

                        showLockWindow(balanceDifference) {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                var _blankFocusNode2 = new FocusNode();
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                    insetPadding: EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(_blankFocusNode2);
                                      },
                                      child: Container(
                                        // width: MediaQuery.of(context).size.width - 100,
                                        height: 510,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              margin: EdgeInsets.only(
                                                top: 20,
                                              ),
                                              child: Text(
                                                'Місія недоступна',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Arial',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: SingleChildScrollView(
                                                physics:
                                                    BouncingScrollPhysics(),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 20,
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                'Вам не вистачає ',
                                                                style:
                                                                    TextStyle(
                                                                  color: Hexcolor(
                                                                      '#747474'),
                                                                  fontSize: 20,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                ),
                                                              ),
                                                              Text(
                                                                balanceDifference
                                                                        .toInt()
                                                                        .toString() +
                                                                    ' ' +
                                                                    missionCurrency,
                                                                style:
                                                                    TextStyle(
                                                                  color: Hexcolor(
                                                                      '#FF1E1E'),
                                                                  fontSize: 20,
                                                                  fontFamily:
                                                                      'Arial',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text(
                                                            'для розблокування цієї місії',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#747474'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                      ),
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 165,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          20)),
                                                          image:
                                                              DecorationImage(
                                                            image: NetworkImage(mission
                                                                            .imageUrl ==
                                                                        "" ||
                                                                    mission.imageUrl ==
                                                                        null
                                                                ? '$link' +
                                                                    'default.png'
                                                                : '$link' +
                                                                    missionImage),
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 15,
                                                      ),
                                                      child: Text(
                                                        missionName,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontFamily: 'Arial',
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                          'Вартість',
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Hexcolor(
                                                                  '#B9BCC4'),
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Text(
                                                          missionPoints
                                                                  .toInt()
                                                                  .toString() +
                                                              ' ' +
                                                              missionCurrency,
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color: Hexcolor(
                                                                  '#FF1E1E'),
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                left: 20,
                                                right: 20,
                                                bottom: 20,
                                              ),
                                              height: 60,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: RaisedButton(
                                                child: Text(
                                                  "Закрити",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: 'Arial',
                                                  ),
                                                ),
                                                color: Hexcolor('#FE6802'),
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                14.0)),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                });
                              });
                        }

                        Future<User> changeTus(double value, String operation,
                            String typeCurrency) async {
                          SharedPreferences sharedPreferences =
                              await SharedPreferences.getInstance();
                          String token = sharedPreferences.get("token");
                          int id = sharedPreferences.get("id");
                          final http.Response response = await http.post(
                            'http://generation-admin.ehub.com.ua/api/account/' +
                                operation.toLowerCase() +
                                '/' +
                                '$id',
                            headers: <String, String>{
                              'Content-Type': 'application/json',
                              'Accept': 'application/json',
                              'Authorization': 'Bearer $token',
                            },
                            body: jsonEncode(<String, dynamic>{
                              "currency": typeCurrency,
                              "operation": operation,
                              "amount": value,
                              "description": "quiz change"
                            }),
                          );
                          print(
                              'http://generation-admin.ehub.com.ua/api/account/' +
                                  operation.toLowerCase() +
                                  '/' +
                                  '$id');
                          if (response.statusCode == 200) {
                            print(response.body);
                            print('buying done!!!');
                          } else {
                            throw Exception('Failed to update User.');
                          }
                        }

                        showUnlockWindow() {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                      insetPadding: EdgeInsets.only(
                                        left: 20,
                                        right: 20,
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Stack(
                                        overflow: Overflow.visible,
                                        children: [
                                          Container(
                                            // width: MediaQuery.of(context).size.width - 100,
                                            height: 470,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin: EdgeInsets.only(
                                                    top: 20,
                                                  ),
                                                  child: Text(
                                                    'Поздоровляємо!',
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 24,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      fontFamily: 'Arial',
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: SingleChildScrollView(
                                                    physics:
                                                        BouncingScrollPhysics(),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            top: 15,
                                                            bottom: 15,
                                                          ),
                                                          child: Text(
                                                            'Ви розблокували місію',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#59B32D'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            left: 20,
                                                            right: 20,
                                                          ),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 165,
                                                          decoration:
                                                              BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius.all(
                                                                          Radius.circular(
                                                                              20)),
                                                                  image:
                                                                      DecorationImage(
                                                                    image: NetworkImage(mission.imageUrl ==
                                                                                "" ||
                                                                            mission.imageUrl ==
                                                                                null
                                                                        ? '$link' +
                                                                            'default.png'
                                                                        : '$link' +
                                                                            missionImage),
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  )),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                            top: 15,
                                                            bottom: 15,
                                                          ),
                                                          child: Text(
                                                            missionName,
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#1E2E45'),
                                                              fontSize: 20,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              fontFamily:
                                                                  'Arial',
                                                            ),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Text(
                                                          'Вам доступна нова місія',
                                                          style: TextStyle(
                                                            color: Hexcolor(
                                                                '#747474'),
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontFamily: 'Arial',
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 20,
                                                      right: 20,
                                                      left: 20),
                                                  child: Container(
                                                    height: 60,
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width -
                                                            40,
                                                    child: RaisedButton(
                                                      child: Text(
                                                        "Почати місію",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 17,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                      shape: new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  14.0)),
                                                      onPressed: () async {
                                                        final SharedPreferences
                                                            sharedPreferences =
                                                            await SharedPreferences
                                                                .getInstance();
                                                        sharedPreferences
                                                            .setInt('missionId',
                                                                missionId);
                                                        Navigator.pushNamed(
                                                            context,
                                                            '/6_mission_map.dart');
                                                        // Navigator.of(context).pop();
                                                        // Navigator.of(context).pop();
                                                        // unlockMission();
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            top: -130,
                                            right: -30,
                                            child: Image.asset(
                                              'assets/fg_images/error_mission_icon_unlock.png',
                                              width: 200,
                                              height: 200,
                                            ),
                                          )
                                        ],
                                      ));
                                });
                              });
                        }

                        showConfirmWindow() {
                          showDialog(
                              barrierColor:
                                  Hexcolor('#341F5E').withOpacity(0.8),
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                var _blankFocusNode2 = new FocusNode();
                                return StatefulBuilder(
                                    builder: (context, setState) {
                                  return Dialog(
                                    insetPadding: EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(_blankFocusNode2);
                                      },
                                      child: Container(
                                        // width: MediaQuery.of(context).size.width - 100,
                                        height: 470,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              margin: EdgeInsets.only(
                                                top: 20,
                                              ),
                                              child: Text(
                                                'Відкриття нової місії',
                                                style: TextStyle(
                                                  color: Hexcolor('#1E2E45'),
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Arial',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: SingleChildScrollView(
                                                physics:
                                                    BouncingScrollPhysics(),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 20,
                                                      ),
                                                      child: Text(
                                                        'Ви точно хочете купити місію?',
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#747474'),
                                                          fontSize: 20,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                      ),
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 165,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          20)),
                                                          image:
                                                              DecorationImage(
                                                            image: NetworkImage(mission
                                                                            .imageUrl ==
                                                                        "" ||
                                                                    mission.imageUrl ==
                                                                        null
                                                                ? '$link' +
                                                                    'default.png'
                                                                : '$link' +
                                                                    missionImage),
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 15,
                                                        bottom: 15,
                                                      ),
                                                      child: Text(
                                                        missionName,
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontFamily: 'Arial',
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                          'Вартість',
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Hexcolor(
                                                                  '#B9BCC4'),
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Text(
                                                          missionPoints
                                                                  .toInt()
                                                                  .toString() +
                                                              ' ' +
                                                              missionCurrency,
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color: Hexcolor(
                                                                  '#59B32D'),
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  bottom: 20,
                                                  right: 20,
                                                  left: 20),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    height: 60,
                                                    width:
                                                        MediaQuery.of(context)
                                                                    .size
                                                                    .width /
                                                                2 -
                                                            45,
                                                    child: RaisedButton(
                                                      child: Text(
                                                        "Закрити",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 17,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                      color:
                                                          Hexcolor('#BEBEBE'),
                                                      shape: new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  14.0)),
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Container(
                                                    height: 60,
                                                    width:
                                                        MediaQuery.of(context)
                                                                    .size
                                                                    .width /
                                                                2 -
                                                            45,
                                                    child: RaisedButton(
                                                      child: Text(
                                                        "Купити",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 17,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                      shape: new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  14.0)),
                                                      onPressed: () {
                                                        if (missionPoints >
                                                            userPointsforMission) {
                                                          var balanceDifference =
                                                              (missionPoints -
                                                                  userPointsforMission);
                                                          showLockWindow(
                                                              balanceDifference);
                                                        }
                                                        if (missionPoints <
                                                                userPointsforMission ||
                                                            missionPoints ==
                                                                userPointsforMission) {
                                                          changeTus(
                                                              missionPoints,
                                                              "WITHDRAW",
                                                              missionCurrency);
                                                          showUnlockWindow();
                                                        }
                                                      },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                });
                              });
                        }

                        void _launchUrl(String url) async {
                          if (await canLaunch(url)) {
                            await launch(url);
                          } else {
                            throw 'Could not open Url';
                          }
                        }

                        showDescription() {
                          showDialog(
                              barrierColor: Colors.white.withOpacity(0.0),
                              context: context,
                              builder: (context) {
                                var width = MediaQuery.of(context).size.width;

                                return Stack(
                                  overflow: Overflow.visible,
                                  children: [
                                    Positioned(
                                        bottom: 0,
                                        child: Dialog(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            topRight: Radius.circular(20),
                                          )),
                                          insetPadding: EdgeInsets.only(
                                            left: 0,
                                            right: 0,
                                          ),
                                          child: Container(
                                            width: width,
                                            height: 260,
                                            decoration: BoxDecoration(
                                              color: Hexcolor('#7D5AC2'),
                                              borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(20.0),
                                                  topRight:
                                                      Radius.circular(20.0)),
                                            ),
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 25,
                                                  left: 25,
                                                  bottom: 25),
                                              child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 16,
                                                        bottom: 16,
                                                      ),
                                                      width: 43,
                                                      height: 3,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            Hexcolor('A9B0BA'),
                                                      ),
                                                    ),
                                                    Container(
                                                      child: Expanded(
                                                        child:
                                                            SingleChildScrollView(
                                                          physics:
                                                              BouncingScrollPhysics(),
                                                          child: Container(
                                                            child: Column(
                                                                children: [
                                                                  Container(
                                                                    margin: EdgeInsets.only(
                                                                        bottom:
                                                                            15),
                                                                    child: Row(
                                                                      children: [
                                                                        Flexible(
                                                                          child:
                                                                              Text(
                                                                            missionName,
                                                                            style: TextStyle(
                                                                                color: Hexcolor('#FFFFFF'),
                                                                                fontSize: 18,
                                                                                fontFamily: 'Arial',
                                                                                fontWeight: FontWeight.w600),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Align(
                                                                      alignment:
                                                                          Alignment
                                                                              .centerLeft,
                                                                      child:
                                                                          Html(
                                                                        data: missionDesc ==
                                                                                null
                                                                            ? ''
                                                                            : missionDesc,
                                                                        defaultTextStyle:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              16,
                                                                          color:
                                                                              Hexcolor('#FFFFFF'),
                                                                          fontFamily:
                                                                              'Arial',
                                                                        ),
                                                                        onLinkTap:
                                                                            (i) {
                                                                          _launchUrl(
                                                                              i);
                                                                        },
                                                                      )
                                                                      // Text(
                                                                      //   missionDesc,
                                                                      //   maxLines:
                                                                      //       5,
                                                                      //   style:
                                                                      //       TextStyle(
                                                                      //     color: Hexcolor(
                                                                      //         '#FFFFFF'),
                                                                      //     fontSize:
                                                                      //         16,
                                                                      //     fontFamily:
                                                                      //         'Arial',
                                                                      //   ),
                                                                      // ),
                                                                      ),
                                                                ]),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          height: 60,
                                                          width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width /
                                                                  2 -
                                                              30,
                                                          child: RaisedButton(
                                                            child: Text(
                                                              "Детальніше",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 16,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                              ),
                                                            ),
                                                            color: Hexcolor(
                                                                '#FE6802'),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        14.0)),
                                                            onPressed:
                                                                () async {
                                                              if (missionProgress ==
                                                                  'FINISHED') {
                                                                showWindowQuizDone();
                                                              } else {
                                                                final SharedPreferences
                                                                    sharedPreferences =
                                                                    await SharedPreferences
                                                                        .getInstance();
                                                                sharedPreferences
                                                                    .setInt(
                                                                        'missionId',
                                                                        missionId);
                                                                if (missionProgress ==
                                                                    'FINISHED') {
                                                                  sharedPreferences
                                                                      .setBool(
                                                                          'missionCompleted',
                                                                          true);
                                                                } else {
                                                                  sharedPreferences
                                                                      .setBool(
                                                                          'missionCompleted',
                                                                          false);
                                                                }
                                                                Navigator.pushNamed(
                                                                    context,
                                                                    '/6_mission_desc.dart');
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Container(
                                                          height: 60,
                                                          width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width /
                                                                  2 -
                                                              30,
                                                          child: RaisedButton(
                                                            child: Text(
                                                              "Почати місію",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 16,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                              ),
                                                            ),
                                                            color: missionAccessType == "CLOSED" ||
                                                                    missionAccessType ==
                                                                        "PROMO" ||
                                                                    missionAccessType ==
                                                                        "PAID" ||
                                                                    (missionAccessType ==
                                                                            "BYSTATUS" &&
                                                                        missionAccessGranted ==
                                                                            false)
                                                                ? Hexcolor(
                                                                    '#BEBEBE')
                                                                : Hexcolor(
                                                                    '#FE6802'),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        14.0)),
                                                            onPressed:
                                                                () async {
                                                              if (missionName ==
                                                                  'Київ проти Корони') {
                                                                launchUrl(
                                                                    'https://freegengame.web.app/');
                                                              } else {
                                                                if (missionProgress ==
                                                                    'FINISHED') {
                                                                  showWindowQuizDone();
                                                                } else {
                                                                  if (missionAccessType ==
                                                                      "CLOSED") {
                                                                    showLockedWindow();
                                                                  } else {
                                                                    if (missionAccessType ==
                                                                            "BYSTATUS" &&
                                                                        missionAccessGranted ==
                                                                            false) {
                                                                      showLockedStatusWindow();
                                                                    } else if (missionAccessType ==
                                                                            "PROMO" &&
                                                                        missionUnlocked ==
                                                                            false) {
                                                                      showLockedWindowWithPromocode();
                                                                    } else if (missionAccessType ==
                                                                        "PAID") {
                                                                      showConfirmWindow();
                                                                    } else {
                                                                      final SharedPreferences
                                                                          sharedPreferences =
                                                                          await SharedPreferences
                                                                              .getInstance();
                                                                      sharedPreferences.setInt(
                                                                          'missionId',
                                                                          missionId);
                                                                      Navigator.pushNamed(
                                                                          context,
                                                                          '/6_mission_map.dart');
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ]),
                                            ),
                                          ),
                                        ))
                                  ],
                                );
                              });
                        }

                        hideDescription() {
                          return Container();
                        }

                        return Stack(
                          overflow: Overflow.visible,
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isHidden = !isHidden;
                                });
                                if (!isHidden) {
                                  showDescription();
                                } else {
                                  hideDescription();
                                }
                                _goToQuest(missionLatitude, missionLongitude);
                              },
                              child: Container(
                                margin: EdgeInsets.only(left: 5, right: 5),
                                height: 117,
                                width: 174,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          mission.imageUrl == "" ||
                                                  mission.imageUrl == null
                                              ? '$link' + 'default.png'
                                              : '$link' + missionImage),
                                      fit: BoxFit.cover,
                                    )),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(10.0),
                                            bottomLeft: Radius.circular(10.0)),
                                      ),
                                      // height: 33,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: 5,
                                          bottom: 5,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Container(
                                              child: Flexible(
                                                child: Text(
                                                  missionName,
                                                  style: TextStyle(
                                                    color: Hexcolor('FFFFFF'),
                                                    fontSize: 14,
                                                    fontFamily: 'Arial',
                                                    fontWeight: FontWeight.w600,
                                                    letterSpacing: 1.09,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            missionProgress == 'FINISHED'
                                ? Positioned(
                                    top: 12.5,
                                    left: 15,
                                    child: Image.asset(
                                      'assets/fg_images/6_home_logo_mission_completed2.png',
                                      height: 30,
                                      width: 100,
                                    ))
                                : Container(),
                            (missionAccessType == "CLOSED" ||
                                        missionAccessType == "PROMO" ||
                                        missionAccessType == "PAID" ||
                                        (missionAccessType == "BYSTATUS" &&
                                            missionAccessGranted == false)) &&
                                    missionProgress != 'FINISHED'
                                ? Positioned(
                                    top: 12.5,
                                    left: 15,
                                    child: Image.asset(
                                      'assets/fg_images/6_home_logo_lock.png',
                                      height: 38,
                                      width: 38,
                                    ))
                                : (mission.accessType == "BYSTATUS" &&
                                            missionAccessGranted == true) ||
                                        missionUnlocked == true
                                    ? Positioned(
                                        top: 12.5,
                                        left: 15,
                                        child: Image.asset(
                                          'assets/fg_images/6_home_logo_unlock.png',
                                          height: 38,
                                          width: 38,
                                        ))
                                    : Container()
                          ],
                        );
                      },
                    ),
                  ),
                ),

          // // from APP
          // Positioned(
          //   top: 137,
          //   child: Container(
          //     margin: EdgeInsets.only(left: 5, right: 5),
          //     height: 117,
          //     width: MediaQuery.of(context).size.width,
          //     child: ListView.builder(
          //       physics: BouncingScrollPhysics(),
          //       scrollDirection: Axis.horizontal,
          //       itemCount: questsForMap.length,
          //       itemBuilder: (context, index) {
          //         // showUnlockWindow() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //               insetPadding: EdgeInsets.only(
          //         //                 left: 20,
          //         //                 right: 20,
          //         //               ),
          //         //               shape: RoundedRectangleBorder(
          //         //                 borderRadius: BorderRadius.circular(10),
          //         //               ),
          //         //               child: Stack(
          //         //                 overflow: Overflow.visible,
          //         //                 children: [
          //         //                   Container(
          //         //                     // width: MediaQuery.of(context).size.width - 100,
          //         //                     height: 470,
          //         //                     child: Column(
          //         //                       mainAxisAlignment:
          //         //                           MainAxisAlignment.spaceBetween,
          //         //                       crossAxisAlignment:
          //         //                           CrossAxisAlignment.center,
          //         //                       children: [
          //         //                         Container(
          //         //                           alignment: Alignment.center,
          //         //                           margin: EdgeInsets.only(
          //         //                             top: 20,
          //         //                           ),
          //         //                           child: Text(
          //         //                             'Поздравляем!',
          //         //                             style: TextStyle(
          //         //                               color: Hexcolor('#1E2E45'),
          //         //                               fontSize: 24,
          //         //                               fontWeight: FontWeight.w700,
          //         //                               fontFamily: 'Arial',
          //         //                             ),
          //         //                           ),
          //         //                         ),
          //         //                         Expanded(
          //         //                           child: SingleChildScrollView(
          //         //                             physics: BouncingScrollPhysics(),
          //         //                             child: Column(
          //         //                               mainAxisAlignment:
          //         //                                   MainAxisAlignment
          //         //                                       .spaceBetween,
          //         //                               crossAxisAlignment:
          //         //                                   CrossAxisAlignment.center,
          //         //                               children: [
          //         //                                 Container(
          //         //                                   margin: EdgeInsets.only(
          //         //                                     top: 15,
          //         //                                     bottom: 15,
          //         //                                   ),
          //         //                                   child: Text(
          //         //                                     'Вы разблокировали миссию',
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#59B32D'),
          //         //                                       fontSize: 20,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                     ),
          //         //                                   ),
          //         //                                 ),
          //         //                                 Container(
          //         //                                   margin: EdgeInsets.only(
          //         //                                     left: 20,
          //         //                                     right: 20,
          //         //                                   ),
          //         //                                   width:
          //         //                                       MediaQuery.of(context)
          //         //                                           .size
          //         //                                           .width,
          //         //                                   height: 165,
          //         //                                   decoration: BoxDecoration(
          //         //                                       borderRadius:
          //         //                                           BorderRadius.all(
          //         //                                               Radius.circular(
          //         //                                                   20)),
          //         //                                       image: DecorationImage(
          //         //                                         image: AssetImage(
          //         //                                             questsForMap[
          //         //                                                     index]
          //         //                                                 .image),
          //         //                                         fit: BoxFit.cover,
          //         //                                       )),
          //         //                                 ),
          //         //                                 Container(
          //         //                                   margin: EdgeInsets.only(
          //         //                                     top: 15,
          //         //                                     bottom: 15,
          //         //                                   ),
          //         //                                   child: Text(
          //         //                                     questsForMap[index].name,
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#1E2E45'),
          //         //                                       fontSize: 20,
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                       fontFamily: 'Arial',
          //         //                                     ),
          //         //                                     textAlign:
          //         //                                         TextAlign.center,
          //         //                                   ),
          //         //                                 ),
          //         //                                 Text(
          //         //                                   'Вам доступна новая миссия',
          //         //                                   style: TextStyle(
          //         //                                     color:
          //         //                                         Hexcolor('#747474'),
          //         //                                     fontSize: 20,
          //         //                                     fontWeight:
          //         //                                         FontWeight.w700,
          //         //                                     fontFamily: 'Arial',
          //         //                                   ),
          //         //                                   textAlign: TextAlign.center,
          //         //                                 ),
          //         //                               ],
          //         //                             ),
          //         //                           ),
          //         //                         ),
          //         //                         Container(
          //         //                           margin: EdgeInsets.only(
          //         //                               bottom: 20,
          //         //                               right: 20,
          //         //                               left: 20),
          //         //                           child: Row(
          //         //                             mainAxisAlignment:
          //         //                                 MainAxisAlignment
          //         //                                     .spaceBetween,
          //         //                             children: [
          //         //                               Container(
          //         //                                 height: 60,
          //         //                                 width: MediaQuery.of(context)
          //         //                                             .size
          //         //                                             .width /
          //         //                                         2 -
          //         //                                     45,
          //         //                                 child: RaisedButton(
          //         //                                   child: Text(
          //         //                                     "Закрити",
          //         //                                     style: TextStyle(
          //         //                                       color: Colors.white,
          //         //                                       fontSize: 17,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                     ),
          //         //                                   ),
          //         //                                   color: Hexcolor('#BEBEBE'),
          //         //                                   shape:
          //         //                                       new RoundedRectangleBorder(
          //         //                                           borderRadius:
          //         //                                               new BorderRadius
          //         //                                                       .circular(
          //         //                                                   14.0)),
          //         //                                   onPressed: () {
          //         //                                     Navigator.pushNamed(
          //         //                                         context,
          //         //                                         '/5_myBottomBar.dart');
          //         //                                   },
          //         //                                 ),
          //         //                               ),
          //         //                               SizedBox(
          //         //                                 width: 10,
          //         //                               ),
          //         //                               Container(
          //         //                                 height: 60,
          //         //                                 width: MediaQuery.of(context)
          //         //                                             .size
          //         //                                             .width /
          //         //                                         2 -
          //         //                                     45,
          //         //                                 child: RaisedButton(
          //         //                                   child: Text(
          //         //                                     "Детальніше",
          //         //                                     style: TextStyle(
          //         //                                       color: Colors.white,
          //         //                                       fontSize: 17,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                     ),
          //         //                                   ),
          //         //                                   color: Hexcolor('#FE6802'),
          //         //                                   shape:
          //         //                                       new RoundedRectangleBorder(
          //         //                                           borderRadius:
          //         //                                               new BorderRadius
          //         //                                                       .circular(
          //         //                                                   14.0)),
          //         //                                   onPressed: () {
          //         //                                     Navigator.of(context)
          //         //                                         .pop();
          //         //                                     Navigator.of(context)
          //         //                                         .pop();
          //         //                                     Navigator.of(context)
          //         //                                         .pop(); // questsForMap[index].isLocked

          //         //                                     setState(() {
          //         //                                       questsForMap[index]
          //         //                                           .isLocked = false;
          //         //                                     });
          //         //                                   },
          //         //                                 ),
          //         //                               ),
          //         //                             ],
          //         //                           ),
          //         //                         ),
          //         //                       ],
          //         //                     ),
          //         //                   ),
          //         //                   Positioned(
          //         //                     top: -130,
          //         //                     right: -30,
          //         //                     child: Image.asset(
          //         //                       'assets/fg_images/error_mission_icon_unlock.png',
          //         //                       width: 200,
          //         //                       height: 200,
          //         //                     ),
          //         //                   )
          //         //                 ],
          //         //               ));
          //         //         });
          //         //       });
          //         // }

          //         // showUnlockWindow2() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //               insetPadding: EdgeInsets.only(
          //         //                 left: 20,
          //         //                 right: 20,
          //         //               ),
          //         //               shape: RoundedRectangleBorder(
          //         //                 borderRadius: BorderRadius.circular(10),
          //         //               ),
          //         //               child: Stack(
          //         //                 overflow: Overflow.visible,
          //         //                 children: [
          //         //                   Container(
          //         //                     // width: MediaQuery.of(context).size.width - 100,
          //         //                     height: 470,
          //         //                     child: Column(
          //         //                       mainAxisAlignment:
          //         //                           MainAxisAlignment.spaceBetween,
          //         //                       crossAxisAlignment:
          //         //                           CrossAxisAlignment.center,
          //         //                       children: [
          //         //                         Container(
          //         //                           alignment: Alignment.center,
          //         //                           margin: EdgeInsets.only(
          //         //                             top: 20,
          //         //                           ),
          //         //                           child: Text(
          //         //                             'Поздравляем!',
          //         //                             style: TextStyle(
          //         //                               color: Hexcolor('#1E2E45'),
          //         //                               fontSize: 24,
          //         //                               fontWeight: FontWeight.w700,
          //         //                               fontFamily: 'Arial',
          //         //                             ),
          //         //                           ),
          //         //                         ),
          //         //                         Expanded(
          //         //                           child: SingleChildScrollView(
          //         //                             physics: BouncingScrollPhysics(),
          //         //                             child: Column(
          //         //                               mainAxisAlignment:
          //         //                                   MainAxisAlignment
          //         //                                       .spaceBetween,
          //         //                               crossAxisAlignment:
          //         //                                   CrossAxisAlignment.center,
          //         //                               children: [
          //         //                                 Container(
          //         //                                   margin: EdgeInsets.only(
          //         //                                     top: 15,
          //         //                                     bottom: 15,
          //         //                                   ),
          //         //                                   child: Text(
          //         //                                     'Вы разблокировали миссию',
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#59B32D'),
          //         //                                       fontSize: 20,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                     ),
          //         //                                   ),
          //         //                                 ),
          //         //                                 Container(
          //         //                                   margin: EdgeInsets.only(
          //         //                                     left: 20,
          //         //                                     right: 20,
          //         //                                   ),
          //         //                                   width:
          //         //                                       MediaQuery.of(context)
          //         //                                           .size
          //         //                                           .width,
          //         //                                   height: 165,
          //         //                                   decoration: BoxDecoration(
          //         //                                       borderRadius:
          //         //                                           BorderRadius.all(
          //         //                                               Radius.circular(
          //         //                                                   20)),
          //         //                                       image: DecorationImage(
          //         //                                         image: AssetImage(
          //         //                                             questsForMap[
          //         //                                                     index]
          //         //                                                 .image),
          //         //                                         fit: BoxFit.cover,
          //         //                                       )),
          //         //                                 ),
          //         //                                 Container(
          //         //                                   margin: EdgeInsets.only(
          //         //                                     top: 15,
          //         //                                     bottom: 15,
          //         //                                   ),
          //         //                                   child: Text(
          //         //                                     questsForMap[index].name,
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#1E2E45'),
          //         //                                       fontSize: 20,
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                       fontFamily: 'Arial',
          //         //                                     ),
          //         //                                     textAlign:
          //         //                                         TextAlign.center,
          //         //                                   ),
          //         //                                 ),
          //         //                                 Text(
          //         //                                   'Вам доступна новая миссия',
          //         //                                   style: TextStyle(
          //         //                                     color:
          //         //                                         Hexcolor('#747474'),
          //         //                                     fontSize: 20,
          //         //                                     fontWeight:
          //         //                                         FontWeight.w700,
          //         //                                     fontFamily: 'Arial',
          //         //                                   ),
          //         //                                   textAlign: TextAlign.center,
          //         //                                 ),
          //         //                               ],
          //         //                             ),
          //         //                           ),
          //         //                         ),
          //         //                         Container(
          //         //                           margin: EdgeInsets.only(
          //         //                               bottom: 20,
          //         //                               right: 20,
          //         //                               left: 20),
          //         //                           child: Row(
          //         //                             mainAxisAlignment:
          //         //                                 MainAxisAlignment
          //         //                                     .spaceBetween,
          //         //                             children: [
          //         //                               Container(
          //         //                                 height: 60,
          //         //                                 width: MediaQuery.of(context)
          //         //                                             .size
          //         //                                             .width /
          //         //                                         2 -
          //         //                                     45,
          //         //                                 child: RaisedButton(
          //         //                                   child: Text(
          //         //                                     "Закрити",
          //         //                                     style: TextStyle(
          //         //                                       color: Colors.white,
          //         //                                       fontSize: 17,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                     ),
          //         //                                   ),
          //         //                                   color: Hexcolor('#BEBEBE'),
          //         //                                   shape:
          //         //                                       new RoundedRectangleBorder(
          //         //                                           borderRadius:
          //         //                                               new BorderRadius
          //         //                                                       .circular(
          //         //                                                   14.0)),
          //         //                                   onPressed: () {
          //         //                                     Navigator.pushNamed(
          //         //                                         context,
          //         //                                         '/5_myBottomBar.dart');
          //         //                                   },
          //         //                                 ),
          //         //                               ),
          //         //                               SizedBox(
          //         //                                 width: 10,
          //         //                               ),
          //         //                               Container(
          //         //                                 height: 60,
          //         //                                 width: MediaQuery.of(context)
          //         //                                             .size
          //         //                                             .width /
          //         //                                         2 -
          //         //                                     45,
          //         //                                 child: RaisedButton(
          //         //                                   child: Text(
          //         //                                     "Детальніше",
          //         //                                     style: TextStyle(
          //         //                                       color: Colors.white,
          //         //                                       fontSize: 17,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700,
          //         //                                     ),
          //         //                                   ),
          //         //                                   color: Hexcolor('#FE6802'),
          //         //                                   shape:
          //         //                                       new RoundedRectangleBorder(
          //         //                                           borderRadius:
          //         //                                               new BorderRadius
          //         //                                                       .circular(
          //         //                                                   14.0)),
          //         //                                   onPressed: () {
          //         //                                     Navigator.of(context)
          //         //                                         .pop();
          //         //                                     Navigator.of(context)
          //         //                                         .pop(); // questsForMap[index].isLocked

          //         //                                     setState(() {
          //         //                                       questsForMap[index]
          //         //                                           .isLocked = false;
          //         //                                     });
          //         //                                   },
          //         //                                 ),
          //         //                               ),
          //         //                             ],
          //         //                           ),
          //         //                         ),
          //         //                       ],
          //         //                     ),
          //         //                   ),
          //         //                   Positioned(
          //         //                     top: -130,
          //         //                     right: -30,
          //         //                     child: Image.asset(
          //         //                       'assets/fg_images/error_mission_icon_unlock.png',
          //         //                       width: 200,
          //         //                       height: 200,
          //         //                     ),
          //         //                   )
          //         //                 ],
          //         //               ));
          //         //         });
          //         //       });
          //         // }

          //         // showLockedWindowWithPromocode() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         var _blankFocusNode2 = new FocusNode();
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //             insetPadding: EdgeInsets.only(
          //         //               left: 20,
          //         //               right: 20,
          //         //             ),
          //         //             shape: RoundedRectangleBorder(
          //         //               borderRadius: BorderRadius.circular(10),
          //         //             ),
          //         //             child: GestureDetector(
          //         //               behavior: HitTestBehavior.opaque,
          //         //               onTap: () {
          //         //                 FocusScope.of(context)
          //         //                     .requestFocus(_blankFocusNode2);
          //         //               },
          //         //               child: Container(
          //         //                 // width: MediaQuery.of(context).size.width - 100,
          //         //                 height: 590,
          //         //                 child: Column(
          //         //                   mainAxisAlignment:
          //         //                       MainAxisAlignment.spaceBetween,
          //         //                   crossAxisAlignment:
          //         //                       CrossAxisAlignment.center,
          //         //                   children: [
          //         //                     Container(
          //         //                       alignment: Alignment.center,
          //         //                       margin: EdgeInsets.only(
          //         //                         top: 20,
          //         //                       ),
          //         //                       child: Text(
          //         //                         'Миссия недоступна',
          //         //                         style: TextStyle(
          //         //                           color: Hexcolor('#1E2E45'),
          //         //                           fontSize: 24,
          //         //                           fontWeight: FontWeight.w700,
          //         //                           fontFamily: 'Arial',
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Expanded(
          //         //                       child: SingleChildScrollView(
          //         //                         physics: BouncingScrollPhysics(),
          //         //                         child: Column(
          //         //                           mainAxisAlignment:
          //         //                               MainAxisAlignment.spaceBetween,
          //         //                           crossAxisAlignment:
          //         //                               CrossAxisAlignment.center,
          //         //                           children: [
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 20,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Column(
          //         //                                 mainAxisAlignment:
          //         //                                     MainAxisAlignment.center,
          //         //                                 crossAxisAlignment:
          //         //                                     CrossAxisAlignment.center,
          //         //                                 children: [
          //         //                                   Text(
          //         //                                     'Миссия доступна только',
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#747474'),
          //         //                                       fontSize: 20,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w400,
          //         //                                     ),
          //         //                                   ),
          //         //                                   Row(
          //         //                                     mainAxisAlignment:
          //         //                                         MainAxisAlignment
          //         //                                             .center,
          //         //                                     crossAxisAlignment:
          //         //                                         CrossAxisAlignment
          //         //                                             .end,
          //         //                                     children: [
          //         //                                       Text(
          //         //                                         'по ',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#747474'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w400,
          //         //                                         ),
          //         //                                       ),
          //         //                                       Text(
          //         //                                         'промокоду',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#FF1E1E'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                         ),
          //         //                                       ),
          //         //                                     ],
          //         //                                   ),
          //         //                                 ],
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 left: 20,
          //         //                                 right: 20,
          //         //                               ),
          //         //                               width: MediaQuery.of(context)
          //         //                                   .size
          //         //                                   .width,
          //         //                               height: 165,
          //         //                               decoration: BoxDecoration(
          //         //                                   borderRadius:
          //         //                                       BorderRadius.all(
          //         //                                           Radius.circular(
          //         //                                               20)),
          //         //                                   image: DecorationImage(
          //         //                                     image: AssetImage(
          //         //                                         questsForMap[index]
          //         //                                             .image),
          //         //                                     fit: BoxFit.cover,
          //         //                                   )),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 questsForMap[index].name,
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#1E2E45'),
          //         //                                   fontSize: 20,
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                   fontFamily: 'Arial',
          //         //                                 ),
          //         //                                 textAlign: TextAlign.center,
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               child: Row(
          //         //                                 mainAxisAlignment:
          //         //                                     MainAxisAlignment.center,
          //         //                                 crossAxisAlignment:
          //         //                                     CrossAxisAlignment.start,
          //         //                                 children: [
          //         //                                   Container(
          //         //                                     // height: 60,
          //         //                                     width:
          //         //                                         MediaQuery.of(context)
          //         //                                                     .size
          //         //                                                     .width /
          //         //                                                 2 -
          //         //                                             45,
          //         //                                     child: Form(
          //         //                                       key: formKey,
          //         //                                       child: TextFormField(
          //         //                                         onChanged: (val) {},
          //         //                                         validator: (value) {
          //         //                                           return value ==
          //         //                                                   'FREEGEN'
          //         //                                               ? null
          //         //                                               : 'Неверный промокод';
          //         //                                         },
          //         //                                         controller:
          //         //                                             promocodeTextEditingController,
          //         //                                         textAlign:
          //         //                                             TextAlign.left,
          //         //                                         decoration:
          //         //                                             InputDecoration(
          //         //                                           hintText:
          //         //                                               "Промокод",
          //         //                                           contentPadding:
          //         //                                               new EdgeInsets
          //         //                                                       .only(
          //         //                                                   left: 20,
          //         //                                                   top: 20.5,
          //         //                                                   bottom:
          //         //                                                       20.5),
          //         //                                           hintStyle:
          //         //                                               TextStyle(
          //         //                                             color: Hexcolor(
          //         //                                                 '#D3D3D3'),
          //         //                                             fontSize: 17,
          //         //                                             fontWeight:
          //         //                                                 FontWeight
          //         //                                                     .w400,
          //         //                                             fontFamily:
          //         //                                                 'Arial',
          //         //                                           ),
          //         //                                           focusedBorder:
          //         //                                               OutlineInputBorder(
          //         //                                             borderRadius:
          //         //                                                 BorderRadius
          //         //                                                     .circular(
          //         //                                                         14.0),
          //         //                                             borderSide:
          //         //                                                 BorderSide(
          //         //                                               color: Hexcolor(
          //         //                                                   isPromoRight
          //         //                                                       ? '#59B32D'
          //         //                                                       : '#FE6802'),
          //         //                                               width: 1,
          //         //                                             ),
          //         //                                           ),
          //         //                                           enabledBorder:
          //         //                                               OutlineInputBorder(
          //         //                                             borderRadius:
          //         //                                                 BorderRadius
          //         //                                                     .circular(
          //         //                                                         14.0),
          //         //                                             borderSide:
          //         //                                                 BorderSide(
          //         //                                               color: Hexcolor(
          //         //                                                   '#E6E6E6'),
          //         //                                               width: 1,
          //         //                                             ),
          //         //                                           ),
          //         //                                           focusedErrorBorder:
          //         //                                               OutlineInputBorder(
          //         //                                             borderRadius:
          //         //                                                 BorderRadius
          //         //                                                     .circular(
          //         //                                                         14.0),
          //         //                                             borderSide:
          //         //                                                 BorderSide(
          //         //                                               color:
          //         //                                                   Colors.red,
          //         //                                               width: 1,
          //         //                                             ),
          //         //                                           ),
          //         //                                           errorBorder:
          //         //                                               OutlineInputBorder(
          //         //                                             borderRadius:
          //         //                                                 BorderRadius
          //         //                                                     .circular(
          //         //                                                         14.0),
          //         //                                             borderSide:
          //         //                                                 BorderSide(
          //         //                                               color:
          //         //                                                   Colors.red,
          //         //                                               width: 1,
          //         //                                             ),
          //         //                                           ),
          //         //                                           fillColor:
          //         //                                               Colors.white,
          //         //                                           filled: true,
          //         //                                         ),
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#1E2E45'),
          //         //                                           fontSize: 17,
          //         //                                           fontWeight:
          //         //                                               FontWeight.w400,
          //         //                                           fontFamily: 'Arial',
          //         //                                         ),
          //         //                                       ),
          //         //                                     ),
          //         //                                   ),
          //         //                                   SizedBox(
          //         //                                     width: 10,
          //         //                                   ),
          //         //                                   Container(
          //         //                                     height: 60,
          //         //                                     width:
          //         //                                         MediaQuery.of(context)
          //         //                                                     .size
          //         //                                                     .width /
          //         //                                                 2 -
          //         //                                             45,
          //         //                                     child: RaisedButton(
          //         //                                       child: Text(
          //         //                                         "Применить",
          //         //                                         style: TextStyle(
          //         //                                           color: Colors.white,
          //         //                                           fontSize: 17,
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                           fontFamily: 'Arial',
          //         //                                         ),
          //         //                                       ),
          //         //                                       color:
          //         //                                           Hexcolor('#FE6802'),
          //         //                                       shape: new RoundedRectangleBorder(
          //         //                                           borderRadius:
          //         //                                               new BorderRadius
          //         //                                                       .circular(
          //         //                                                   14.0)),
          //         //                                       onPressed: () {
          //         //                                         if (formKey
          //         //                                             .currentState
          //         //                                             .validate()) {
          //         //                                           setState(() {
          //         //                                             isPromoRight =
          //         //                                                 true;
          //         //                                           });
          //         //                                           // setState(() {
          //         //                                           //   questsDifferent[index]
          //         //                                           //       .isLocked = false;
          //         //                                           // });
          //         //                                         }
          //         //                                       },
          //         //                                     ),
          //         //                                   ),
          //         //                                 ],
          //         //                               ),
          //         //                             ),
          //         //                             isPromoRight
          //         //                                 ? Container(
          //         //                                     margin: EdgeInsets.only(
          //         //                                       left: 40,
          //         //                                       top: 7,
          //         //                                       bottom: 15,
          //         //                                     ),
          //         //                                     alignment:
          //         //                                         Alignment.centerLeft,
          //         //                                     child: Text(
          //         //                                       'Правильный промокод',
          //         //                                       style: TextStyle(
          //         //                                         color: Hexcolor(
          //         //                                             '#59B32D'),
          //         //                                         fontFamily: 'Arial',
          //         //                                         fontSize: 12,
          //         //                                         fontWeight:
          //         //                                             FontWeight.w400,
          //         //                                       ),
          //         //                                     ))
          //         //                                 : Container(
          //         //                                     height: 15,
          //         //                                   ),
          //         //                             !isPromoRight
          //         //                                 ? SizedBox(
          //         //                                     height: 30,
          //         //                                   )
          //         //                                 : SizedBox(
          //         //                                     height: 15,
          //         //                                   )
          //         //                           ],
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                         left: 20,
          //         //                         right: 20,
          //         //                         bottom: 20,
          //         //                       ),
          //         //                       height: 60,
          //         //                       width:
          //         //                           MediaQuery.of(context).size.width,
          //         //                       child: RaisedButton(
          //         //                         child: Text(
          //         //                           "Закрыть",
          //         //                           style: TextStyle(
          //         //                             color: Colors.white,
          //         //                             fontSize: 17,
          //         //                             fontWeight: FontWeight.w700,
          //         //                             fontFamily: 'Arial',
          //         //                           ),
          //         //                         ),
          //         //                         color: Hexcolor('#FE6802'),
          //         //                         shape: new RoundedRectangleBorder(
          //         //                             borderRadius:
          //         //                                 new BorderRadius.circular(
          //         //                                     14.0)),
          //         //                         onPressed: () {
          //         //                           if (isPromoRight) {
          //         //                             showUnlockWindow2();
          //         //                             setState(() {
          //         //                               missionUnlocked = true;
          //         //                             });
          //         //                           } else {
          //         //                             Navigator.pushNamed(context,
          //         //                                 '/5_myBottomBar.dart');
          //         //                           }
          //         //                           // Navigator.of(context).pop();
          //         //                           // Navigator.pushNamed(context, '/5_myBottomBar.dart');
          //         //                         },
          //         //                       ),
          //         //                     ),
          //         //                   ],
          //         //                 ),
          //         //               ),
          //         //             ),
          //         //           );
          //         //         });
          //         //       });
          //         // }

          //         // showLockedWindowStatus() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         var _blankFocusNode2 = new FocusNode();
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //             insetPadding: EdgeInsets.only(
          //         //               left: 20,
          //         //               right: 20,
          //         //             ),
          //         //             shape: RoundedRectangleBorder(
          //         //               borderRadius: BorderRadius.circular(10),
          //         //             ),
          //         //             child: GestureDetector(
          //         //               behavior: HitTestBehavior.opaque,
          //         //               onTap: () {
          //         //                 FocusScope.of(context)
          //         //                     .requestFocus(_blankFocusNode2);
          //         //               },
          //         //               child: Container(
          //         //                 // width: MediaQuery.of(context).size.width - 100,
          //         //                 height: 520,
          //         //                 child: Column(
          //         //                   mainAxisAlignment:
          //         //                       MainAxisAlignment.spaceBetween,
          //         //                   crossAxisAlignment:
          //         //                       CrossAxisAlignment.center,
          //         //                   children: [
          //         //                     Container(
          //         //                       alignment: Alignment.center,
          //         //                       margin: EdgeInsets.only(
          //         //                         top: 20,
          //         //                       ),
          //         //                       child: Text(
          //         //                         'Миссия недоступна',
          //         //                         style: TextStyle(
          //         //                           color: Hexcolor('#1E2E45'),
          //         //                           fontSize: 24,
          //         //                           fontWeight: FontWeight.w700,
          //         //                           fontFamily: 'Arial',
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Expanded(
          //         //                       child: SingleChildScrollView(
          //         //                         physics: BouncingScrollPhysics(),
          //         //                         child: Column(
          //         //                           mainAxisAlignment:
          //         //                               MainAxisAlignment.spaceBetween,
          //         //                           crossAxisAlignment:
          //         //                               CrossAxisAlignment.center,
          //         //                           children: [
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Column(
          //         //                                 mainAxisAlignment:
          //         //                                     MainAxisAlignment.center,
          //         //                                 crossAxisAlignment:
          //         //                                     CrossAxisAlignment.center,
          //         //                                 children: [
          //         //                                   Text(
          //         //                                     'Миссия доступна для статуса',
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#747474'),
          //         //                                       fontSize: 20,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w400,
          //         //                                     ),
          //         //                                   ),
          //         //                                   Row(
          //         //                                     mainAxisAlignment:
          //         //                                         MainAxisAlignment
          //         //                                             .center,
          //         //                                     crossAxisAlignment:
          //         //                                         CrossAxisAlignment
          //         //                                             .end,
          //         //                                     children: [
          //         //                                       Text(
          //         //                                         'Senator',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#FF1E1E'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                         ),
          //         //                                       ),
          //         //                                       Text(
          //         //                                         ' и ',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#747474'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w400,
          //         //                                         ),
          //         //                                       ),
          //         //                                       Text(
          //         //                                         'выше',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#FF1E1E'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                         ),
          //         //                                       ),
          //         //                                     ],
          //         //                                   ),
          //         //                                 ],
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 left: 20,
          //         //                                 right: 20,
          //         //                               ),
          //         //                               width: MediaQuery.of(context)
          //         //                                   .size
          //         //                                   .width,
          //         //                               height: 165,
          //         //                               decoration: BoxDecoration(
          //         //                                   borderRadius:
          //         //                                       BorderRadius.all(
          //         //                                           Radius.circular(
          //         //                                               20)),
          //         //                                   image: DecorationImage(
          //         //                                     image: AssetImage(
          //         //                                         questsForMap[index]
          //         //                                             .image),
          //         //                                     fit: BoxFit.cover,
          //         //                                   )),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 questsForMap[index].name,
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#1E2E45'),
          //         //                                   fontSize: 20,
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                   fontFamily: 'Arial',
          //         //                                 ),
          //         //                                 textAlign: TextAlign.center,
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 'Миссия недоступна\nдля вашего статуса',
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#747474'),
          //         //                                   fontSize: 20,
          //         //                                   fontWeight: FontWeight.w400,
          //         //                                   fontFamily: 'Arial',
          //         //                                 ),
          //         //                               ),
          //         //                             ),
          //         //                           ],
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                         left: 20,
          //         //                         right: 20,
          //         //                         bottom: 20,
          //         //                       ),
          //         //                       height: 60,
          //         //                       width:
          //         //                           MediaQuery.of(context).size.width,
          //         //                       child: RaisedButton(
          //         //                         child: Text(
          //         //                           "Закрыть",
          //         //                           style: TextStyle(
          //         //                             color: Colors.white,
          //         //                             fontSize: 17,
          //         //                             fontWeight: FontWeight.w700,
          //         //                             fontFamily: 'Arial',
          //         //                           ),
          //         //                         ),
          //         //                         color: Hexcolor('#FE6802'),
          //         //                         shape: new RoundedRectangleBorder(
          //         //                             borderRadius:
          //         //                                 new BorderRadius.circular(
          //         //                                     14.0)),
          //         //                         onPressed: () {
          //         //                           showUnlockWindow();
          //         //                         },
          //         //                       ),
          //         //                     ),
          //         //                   ],
          //         //                 ),
          //         //               ),
          //         //             ),
          //         //           );
          //         //         });
          //         //       });
          //         // }

          //         // showLockedWindowCash() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         var _blankFocusNode2 = new FocusNode();
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //             insetPadding: EdgeInsets.only(
          //         //               left: 20,
          //         //               right: 20,
          //         //             ),
          //         //             shape: RoundedRectangleBorder(
          //         //               borderRadius: BorderRadius.circular(10),
          //         //             ),
          //         //             child: GestureDetector(
          //         //               behavior: HitTestBehavior.opaque,
          //         //               onTap: () {
          //         //                 FocusScope.of(context)
          //         //                     .requestFocus(_blankFocusNode2);
          //         //               },
          //         //               child: Container(
          //         //                 // width: MediaQuery.of(context).size.width - 100,
          //         //                 height: 510,
          //         //                 child: Column(
          //         //                   mainAxisAlignment:
          //         //                       MainAxisAlignment.spaceBetween,
          //         //                   crossAxisAlignment:
          //         //                       CrossAxisAlignment.center,
          //         //                   children: [
          //         //                     Container(
          //         //                       alignment: Alignment.center,
          //         //                       margin: EdgeInsets.only(
          //         //                         top: 20,
          //         //                       ),
          //         //                       child: Text(
          //         //                         'Миссия недоступна',
          //         //                         style: TextStyle(
          //         //                           color: Hexcolor('#1E2E45'),
          //         //                           fontSize: 24,
          //         //                           fontWeight: FontWeight.w700,
          //         //                           fontFamily: 'Arial',
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Expanded(
          //         //                       child: SingleChildScrollView(
          //         //                         physics: BouncingScrollPhysics(),
          //         //                         child: Column(
          //         //                           mainAxisAlignment:
          //         //                               MainAxisAlignment.spaceBetween,
          //         //                           crossAxisAlignment:
          //         //                               CrossAxisAlignment.center,
          //         //                           children: [
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 20,
          //         //                               ),
          //         //                               child: Column(
          //         //                                 mainAxisAlignment:
          //         //                                     MainAxisAlignment.center,
          //         //                                 crossAxisAlignment:
          //         //                                     CrossAxisAlignment.center,
          //         //                                 children: [
          //         //                                   Row(
          //         //                                     mainAxisAlignment:
          //         //                                         MainAxisAlignment
          //         //                                             .center,
          //         //                                     crossAxisAlignment:
          //         //                                         CrossAxisAlignment
          //         //                                             .end,
          //         //                                     children: [
          //         //                                       Text(
          //         //                                         'Вам не хватает ',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#747474'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w400,
          //         //                                         ),
          //         //                                       ),
          //         //                                       Text(
          //         //                                         '29' + ' Foint',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#FF1E1E'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                         ),
          //         //                                       ),
          //         //                                     ],
          //         //                                   ),
          //         //                                   Text(
          //         //                                     'для разблокировки этой миссии',
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#747474'),
          //         //                                       fontSize: 20,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w400,
          //         //                                     ),
          //         //                                   ),
          //         //                                 ],
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 left: 20,
          //         //                                 right: 20,
          //         //                               ),
          //         //                               width: MediaQuery.of(context)
          //         //                                   .size
          //         //                                   .width,
          //         //                               height: 165,
          //         //                               decoration: BoxDecoration(
          //         //                                   borderRadius:
          //         //                                       BorderRadius.all(
          //         //                                           Radius.circular(
          //         //                                               20)),
          //         //                                   image: DecorationImage(
          //         //                                     image: AssetImage(
          //         //                                         questsForMap[index]
          //         //                                             .image),
          //         //                                     fit: BoxFit.cover,
          //         //                                   )),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 questsForMap[index].name,
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#1E2E45'),
          //         //                                   fontSize: 20,
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                   fontFamily: 'Arial',
          //         //                                 ),
          //         //                                 textAlign: TextAlign.center,
          //         //                               ),
          //         //                             ),
          //         //                             Row(
          //         //                               crossAxisAlignment:
          //         //                                   CrossAxisAlignment.end,
          //         //                               mainAxisAlignment:
          //         //                                   MainAxisAlignment.center,
          //         //                               children: [
          //         //                                 Text(
          //         //                                   'Стоимость',
          //         //                                   style: TextStyle(
          //         //                                       fontSize: 14,
          //         //                                       color:
          //         //                                           Hexcolor('#B9BCC4'),
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w400),
          //         //                                 ),
          //         //                                 SizedBox(
          //         //                                   width: 10,
          //         //                                 ),
          //         //                                 Text(
          //         //                                   '120' + ' Foint',
          //         //                                   style: TextStyle(
          //         //                                       fontSize: 16,
          //         //                                       color:
          //         //                                           Hexcolor('#FF1E1E'),
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700),
          //         //                                 ),
          //         //                               ],
          //         //                             ),
          //         //                           ],
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                         left: 20,
          //         //                         right: 20,
          //         //                         bottom: 20,
          //         //                       ),
          //         //                       height: 60,
          //         //                       width:
          //         //                           MediaQuery.of(context).size.width,
          //         //                       child: RaisedButton(
          //         //                         child: Text(
          //         //                           "Закрыть",
          //         //                           style: TextStyle(
          //         //                             color: Colors.white,
          //         //                             fontSize: 17,
          //         //                             fontWeight: FontWeight.w700,
          //         //                             fontFamily: 'Arial',
          //         //                           ),
          //         //                         ),
          //         //                         color: Hexcolor('#FE6802'),
          //         //                         shape: new RoundedRectangleBorder(
          //         //                             borderRadius:
          //         //                                 new BorderRadius.circular(
          //         //                                     14.0)),
          //         //                         onPressed: () {
          //         //                           showUnlockWindow();
          //         //                         },
          //         //                       ),
          //         //                     ),
          //         //                   ],
          //         //                 ),
          //         //               ),
          //         //             ),
          //         //           );
          //         //         });
          //         //       });
          //         // }

          //         // showConfirmWindowCash() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         var _blankFocusNode2 = new FocusNode();
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //             insetPadding: EdgeInsets.only(
          //         //               left: 20,
          //         //               right: 20,
          //         //             ),
          //         //             shape: RoundedRectangleBorder(
          //         //               borderRadius: BorderRadius.circular(10),
          //         //             ),
          //         //             child: GestureDetector(
          //         //               behavior: HitTestBehavior.opaque,
          //         //               onTap: () {
          //         //                 FocusScope.of(context)
          //         //                     .requestFocus(_blankFocusNode2);
          //         //               },
          //         //               child: Container(
          //         //                 // width: MediaQuery.of(context).size.width - 100,
          //         //                 height: 470,
          //         //                 child: Column(
          //         //                   mainAxisAlignment:
          //         //                       MainAxisAlignment.spaceBetween,
          //         //                   crossAxisAlignment:
          //         //                       CrossAxisAlignment.center,
          //         //                   children: [
          //         //                     Container(
          //         //                       alignment: Alignment.center,
          //         //                       margin: EdgeInsets.only(
          //         //                         top: 20,
          //         //                       ),
          //         //                       child: Text(
          //         //                         'Открытие новой миссии',
          //         //                         style: TextStyle(
          //         //                           color: Hexcolor('#1E2E45'),
          //         //                           fontSize: 24,
          //         //                           fontWeight: FontWeight.w700,
          //         //                           fontFamily: 'Arial',
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Expanded(
          //         //                       child: SingleChildScrollView(
          //         //                         physics: BouncingScrollPhysics(),
          //         //                         child: Column(
          //         //                           mainAxisAlignment:
          //         //                               MainAxisAlignment.spaceBetween,
          //         //                           crossAxisAlignment:
          //         //                               CrossAxisAlignment.center,
          //         //                           children: [
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 20,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 'Вы точно хотите купить миссию?',
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#747474'),
          //         //                                   fontSize: 20,
          //         //                                   fontFamily: 'Arial',
          //         //                                   fontWeight: FontWeight.w400,
          //         //                                 ),
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 left: 20,
          //         //                                 right: 20,
          //         //                               ),
          //         //                               width: MediaQuery.of(context)
          //         //                                   .size
          //         //                                   .width,
          //         //                               height: 165,
          //         //                               decoration: BoxDecoration(
          //         //                                   borderRadius:
          //         //                                       BorderRadius.all(
          //         //                                           Radius.circular(
          //         //                                               20)),
          //         //                                   image: DecorationImage(
          //         //                                     image: AssetImage(
          //         //                                         questsForMap[index]
          //         //                                             .image),
          //         //                                     fit: BoxFit.cover,
          //         //                                   )),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 questsForMap[index].name,
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#1E2E45'),
          //         //                                   fontSize: 20,
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                   fontFamily: 'Arial',
          //         //                                 ),
          //         //                                 textAlign: TextAlign.center,
          //         //                               ),
          //         //                             ),
          //         //                             Row(
          //         //                               crossAxisAlignment:
          //         //                                   CrossAxisAlignment.end,
          //         //                               mainAxisAlignment:
          //         //                                   MainAxisAlignment.center,
          //         //                               children: [
          //         //                                 Text(
          //         //                                   'Стоимость',
          //         //                                   style: TextStyle(
          //         //                                       fontSize: 14,
          //         //                                       color:
          //         //                                           Hexcolor('#B9BCC4'),
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w400),
          //         //                                 ),
          //         //                                 SizedBox(
          //         //                                   width: 10,
          //         //                                 ),
          //         //                                 Text(
          //         //                                   '120' + ' Foint',
          //         //                                   style: TextStyle(
          //         //                                       fontSize: 16,
          //         //                                       color:
          //         //                                           Hexcolor('#59B32D'),
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w700),
          //         //                                 ),
          //         //                               ],
          //         //                             ),
          //         //                           ],
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                           bottom: 20, right: 20, left: 20),
          //         //                       child: Row(
          //         //                         mainAxisAlignment:
          //         //                             MainAxisAlignment.spaceBetween,
          //         //                         children: [
          //         //                           Container(
          //         //                             height: 60,
          //         //                             width: MediaQuery.of(context)
          //         //                                         .size
          //         //                                         .width /
          //         //                                     2 -
          //         //                                 45,
          //         //                             child: RaisedButton(
          //         //                               child: Text(
          //         //                                 "Закрыть",
          //         //                                 style: TextStyle(
          //         //                                   color: Colors.white,
          //         //                                   fontSize: 17,
          //         //                                   fontFamily: 'Arial',
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                 ),
          //         //                               ),
          //         //                               color: Hexcolor('#BEBEBE'),
          //         //                               shape:
          //         //                                   new RoundedRectangleBorder(
          //         //                                       borderRadius:
          //         //                                           new BorderRadius
          //         //                                                   .circular(
          //         //                                               14.0)),
          //         //                               onPressed: () {
          //         //                                 Navigator.pushNamed(context,
          //         //                                     '/5_myBottomBar.dart');
          //         //                               },
          //         //                             ),
          //         //                           ),
          //         //                           SizedBox(
          //         //                             width: 10,
          //         //                           ),
          //         //                           Container(
          //         //                             height: 60,
          //         //                             width: MediaQuery.of(context)
          //         //                                         .size
          //         //                                         .width /
          //         //                                     2 -
          //         //                                 45,
          //         //                             child: RaisedButton(
          //         //                               child: Text(
          //         //                                 "Купить",
          //         //                                 style: TextStyle(
          //         //                                   color: Colors.white,
          //         //                                   fontSize: 17,
          //         //                                   fontFamily: 'Arial',
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                 ),
          //         //                               ),
          //         //                               color: Hexcolor('#FE6802'),
          //         //                               shape:
          //         //                                   new RoundedRectangleBorder(
          //         //                                       borderRadius:
          //         //                                           new BorderRadius
          //         //                                                   .circular(
          //         //                                               14.0)),
          //         //                               onPressed: () {
          //         //                                 showLockedWindowCash();
          //         //                               },
          //         //                             ),
          //         //                           ),
          //         //                         ],
          //         //                       ),
          //         //                     ),
          //         //                   ],
          //         //                 ),
          //         //               ),
          //         //             ),
          //         //           );
          //         //         });
          //         //       });
          //         // }

          //         // showConfirmWindowStatus() {
          //         //   showDialog(
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       barrierDismissible: false,
          //         //       context: context,
          //         //       builder: (context) {
          //         //         var _blankFocusNode2 = new FocusNode();
          //         //         return StatefulBuilder(builder: (context, setState) {
          //         //           return Dialog(
          //         //             insetPadding: EdgeInsets.only(
          //         //               left: 20,
          //         //               right: 20,
          //         //             ),
          //         //             shape: RoundedRectangleBorder(
          //         //               borderRadius: BorderRadius.circular(10),
          //         //             ),
          //         //             child: GestureDetector(
          //         //               behavior: HitTestBehavior.opaque,
          //         //               onTap: () {
          //         //                 FocusScope.of(context)
          //         //                     .requestFocus(_blankFocusNode2);
          //         //               },
          //         //               child: Container(
          //         //                 // width: MediaQuery.of(context).size.width - 100,
          //         //                 height: 500,
          //         //                 child: Column(
          //         //                   mainAxisAlignment:
          //         //                       MainAxisAlignment.spaceBetween,
          //         //                   crossAxisAlignment:
          //         //                       CrossAxisAlignment.center,
          //         //                   children: [
          //         //                     Container(
          //         //                       alignment: Alignment.center,
          //         //                       margin: EdgeInsets.only(
          //         //                         top: 20,
          //         //                       ),
          //         //                       child: Text(
          //         //                         'Открытие новой миссии',
          //         //                         style: TextStyle(
          //         //                           color: Hexcolor('#1E2E45'),
          //         //                           fontSize: 24,
          //         //                           fontWeight: FontWeight.w700,
          //         //                           fontFamily: 'Arial',
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Expanded(
          //         //                       child: SingleChildScrollView(
          //         //                         physics: BouncingScrollPhysics(),
          //         //                         child: Column(
          //         //                           mainAxisAlignment:
          //         //                               MainAxisAlignment.spaceBetween,
          //         //                           crossAxisAlignment:
          //         //                               CrossAxisAlignment.center,
          //         //                           children: [
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Column(
          //         //                                 mainAxisAlignment:
          //         //                                     MainAxisAlignment.center,
          //         //                                 crossAxisAlignment:
          //         //                                     CrossAxisAlignment.center,
          //         //                                 children: [
          //         //                                   Text(
          //         //                                     'Миссия доступна для статуса',
          //         //                                     style: TextStyle(
          //         //                                       color:
          //         //                                           Hexcolor('#747474'),
          //         //                                       fontSize: 20,
          //         //                                       fontFamily: 'Arial',
          //         //                                       fontWeight:
          //         //                                           FontWeight.w400,
          //         //                                     ),
          //         //                                   ),
          //         //                                   Row(
          //         //                                     mainAxisAlignment:
          //         //                                         MainAxisAlignment
          //         //                                             .center,
          //         //                                     crossAxisAlignment:
          //         //                                         CrossAxisAlignment
          //         //                                             .end,
          //         //                                     children: [
          //         //                                       Text(
          //         //                                         'Senator',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#59B32D'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                         ),
          //         //                                       ),
          //         //                                       Text(
          //         //                                         ' и ',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#747474'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w400,
          //         //                                         ),
          //         //                                       ),
          //         //                                       Text(
          //         //                                         'выше',
          //         //                                         style: TextStyle(
          //         //                                           color: Hexcolor(
          //         //                                               '#59B32D'),
          //         //                                           fontSize: 20,
          //         //                                           fontFamily: 'Arial',
          //         //                                           fontWeight:
          //         //                                               FontWeight.w700,
          //         //                                         ),
          //         //                                       ),
          //         //                                     ],
          //         //                                   ),
          //         //                                 ],
          //         //                               ),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 left: 20,
          //         //                                 right: 20,
          //         //                               ),
          //         //                               width: MediaQuery.of(context)
          //         //                                   .size
          //         //                                   .width,
          //         //                               height: 165,
          //         //                               decoration: BoxDecoration(
          //         //                                   borderRadius:
          //         //                                       BorderRadius.all(
          //         //                                           Radius.circular(
          //         //                                               20)),
          //         //                                   image: DecorationImage(
          //         //                                     image: AssetImage(
          //         //                                         questsForMap[index]
          //         //                                             .image),
          //         //                                     fit: BoxFit.cover,
          //         //                                   )),
          //         //                             ),
          //         //                             Container(
          //         //                               margin: EdgeInsets.only(
          //         //                                 top: 15,
          //         //                                 bottom: 15,
          //         //                               ),
          //         //                               child: Text(
          //         //                                 questsForMap[index].name,
          //         //                                 style: TextStyle(
          //         //                                   color: Hexcolor('#1E2E45'),
          //         //                                   fontSize: 20,
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                   fontFamily: 'Arial',
          //         //                                 ),
          //         //                                 textAlign: TextAlign.center,
          //         //                               ),
          //         //                             ),
          //         //                             Text(
          //         //                               'Вы точно хотите открыть миссию?',
          //         //                               style: TextStyle(
          //         //                                   fontSize: 20,
          //         //                                   color: Hexcolor('#747474'),
          //         //                                   fontFamily: 'Arial',
          //         //                                   fontWeight:
          //         //                                       FontWeight.w400),
          //         //                             ),
          //         //                           ],
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                           bottom: 20, right: 20, left: 20),
          //         //                       child: Row(
          //         //                         mainAxisAlignment:
          //         //                             MainAxisAlignment.spaceBetween,
          //         //                         children: [
          //         //                           Container(
          //         //                             height: 60,
          //         //                             width: MediaQuery.of(context)
          //         //                                         .size
          //         //                                         .width /
          //         //                                     2 -
          //         //                                 45,
          //         //                             child: RaisedButton(
          //         //                               child: Text(
          //         //                                 "Закрыть",
          //         //                                 style: TextStyle(
          //         //                                   color: Colors.white,
          //         //                                   fontSize: 17,
          //         //                                   fontFamily: 'Arial',
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                 ),
          //         //                               ),
          //         //                               color: Hexcolor('#BEBEBE'),
          //         //                               shape:
          //         //                                   new RoundedRectangleBorder(
          //         //                                       borderRadius:
          //         //                                           new BorderRadius
          //         //                                                   .circular(
          //         //                                               14.0)),
          //         //                               onPressed: () {
          //         //                                 Navigator.pushNamed(context,
          //         //                                     '/5_myBottomBar.dart');
          //         //                               },
          //         //                             ),
          //         //                           ),
          //         //                           SizedBox(
          //         //                             width: 10,
          //         //                           ),
          //         //                           Container(
          //         //                             height: 60,
          //         //                             width: MediaQuery.of(context)
          //         //                                         .size
          //         //                                         .width /
          //         //                                     2 -
          //         //                                 45,
          //         //                             child: RaisedButton(
          //         //                               child: Text(
          //         //                                 "Открыть",
          //         //                                 style: TextStyle(
          //         //                                   color: Colors.white,
          //         //                                   fontSize: 17,
          //         //                                   fontFamily: 'Arial',
          //         //                                   fontWeight: FontWeight.w700,
          //         //                                 ),
          //         //                               ),
          //         //                               color: Hexcolor('#FE6802'),
          //         //                               shape:
          //         //                                   new RoundedRectangleBorder(
          //         //                                       borderRadius:
          //         //                                           new BorderRadius
          //         //                                                   .circular(
          //         //                                               14.0)),
          //         //                               onPressed: () {
          //         //                                 showLockedWindowStatus();
          //         //                               },
          //         //                             ),
          //         //                           ),
          //         //                         ],
          //         //                       ),
          //         //                     ),
          //         //                   ],
          //         //                 ),
          //         //               ),
          //         //             ),
          //         //           );
          //         //         });
          //         //       });
          //         // }

          //         // showWindowQuizDone() {
          //         //   showDialog(
          //         //       barrierDismissible: false,
          //         //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //         //       context: context,
          //         //       builder: (context) {
          //         //         return Dialog(
          //         //           insetPadding: EdgeInsets.only(
          //         //             left: 20,
          //         //             right: 20,
          //         //           ),
          //         //           shape: RoundedRectangleBorder(
          //         //             borderRadius: BorderRadius.circular(20),
          //         //           ),
          //         //           child: Stack(
          //         //             overflow: Overflow.visible,
          //         //             children: [
          //         //               Container(
          //         //                 height: 250,
          //         //                 child: Column(
          //         //                   mainAxisAlignment:
          //         //                       MainAxisAlignment.spaceBetween,
          //         //                   crossAxisAlignment:
          //         //                       CrossAxisAlignment.center,
          //         //                   children: [
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                         top: 20,
          //         //                       ),
          //         //                       child: Text(
          //         //                         'Завдання пройдене',
          //         //                         style: TextStyle(
          //         //                           color: Hexcolor('#1E2E45'),
          //         //                           fontSize: 24,
          //         //                           fontFamily: 'Arial',
          //         //                           fontWeight: FontWeight.w700,
          //         //                         ),
          //         //                       ),
          //         //                     ),
          //         //                     Text(
          //         //                       'Ви не можете повторно пройти це завдання',
          //         //                       textAlign: TextAlign.center,
          //         //                       style: TextStyle(
          //         //                         color: Hexcolor('#747474'),
          //         //                         fontSize: 16,
          //         //                         fontFamily: 'Arial',
          //         //                         fontWeight: FontWeight.w400,
          //         //                         height: 1.4,
          //         //                       ),
          //         //                     ),
          //         //                     Container(
          //         //                       margin: EdgeInsets.only(
          //         //                         left: 20,
          //         //                         right: 20,
          //         //                         bottom: 20,
          //         //                       ),
          //         //                       height: 60,
          //         //                       width:
          //         //                           MediaQuery.of(context).size.width -
          //         //                               80,
          //         //                       child: RaisedButton(
          //         //                         child: Text(
          //         //                           "Закрити",
          //         //                           style: TextStyle(
          //         //                               color: Colors.white,
          //         //                               fontSize: 17,
          //         //                               fontFamily: 'Arial',
          //         //                               fontWeight: FontWeight.w700,
          //         //                               letterSpacing: 1.05),
          //         //                         ),
          //         //                         color: Hexcolor('#FE6802'),
          //         //                         shape: new RoundedRectangleBorder(
          //         //                             borderRadius:
          //         //                                 new BorderRadius.circular(
          //         //                                     14.0)),
          //         //                         onPressed: () {
          //         //                           Navigator.of(context).pop();
          //         //                         },
          //         //                       ),
          //         //                     ),
          //         //                   ],
          //         //                 ),
          //         //               ),
          //         //             ],
          //         //           ),
          //         //         );
          //         //       });
          //         // }

          //         showLockedWindow() {
          //           showDialog(
          //               barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          //               barrierDismissible: false,
          //               context: context,
          //               builder: (context) {
          //                 var _blankFocusNode2 = new FocusNode();
          //                 return StatefulBuilder(builder: (context, setState) {
          //                   return Dialog(
          //                     insetPadding: EdgeInsets.only(
          //                       left: 20,
          //                       right: 20,
          //                     ),
          //                     shape: RoundedRectangleBorder(
          //                       borderRadius: BorderRadius.circular(10),
          //                     ),
          //                     child: GestureDetector(
          //                       behavior: HitTestBehavior.opaque,
          //                       onTap: () {
          //                         FocusScope.of(context)
          //                             .requestFocus(_blankFocusNode2);
          //                       },
          //                       child: Container(
          //                         // width: MediaQuery.of(context).size.width - 100,
          //                         height: 510,
          //                         child: Column(
          //                           mainAxisAlignment:
          //                               MainAxisAlignment.spaceBetween,
          //                           crossAxisAlignment:
          //                               CrossAxisAlignment.center,
          //                           children: [
          //                             Container(
          //                               alignment: Alignment.center,
          //                               margin: EdgeInsets.only(
          //                                 top: 20,
          //                               ),
          //                               child: Text(
          //                                 'Місія недоступна',
          //                                 style: TextStyle(
          //                                   color: Hexcolor('#1E2E45'),
          //                                   fontSize: 24,
          //                                   fontWeight: FontWeight.w700,
          //                                   fontFamily: 'Arial',
          //                                 ),
          //                               ),
          //                             ),
          //                             Expanded(
          //                               child: SingleChildScrollView(
          //                                 physics: BouncingScrollPhysics(),
          //                                 child: Column(
          //                                   mainAxisAlignment:
          //                                       MainAxisAlignment.spaceBetween,
          //                                   crossAxisAlignment:
          //                                       CrossAxisAlignment.center,
          //                                   children: [
          //                                     Container(
          //                                       margin: EdgeInsets.only(
          //                                         top: 15,
          //                                         bottom: 20,
          //                                       ),
          //                                       child: Column(
          //                                         mainAxisAlignment:
          //                                             MainAxisAlignment.center,
          //                                         crossAxisAlignment:
          //                                             CrossAxisAlignment.center,
          //                                         children: [
          //                                           Row(
          //                                             mainAxisAlignment:
          //                                                 MainAxisAlignment
          //                                                     .center,
          //                                             crossAxisAlignment:
          //                                                 CrossAxisAlignment
          //                                                     .end,
          //                                             children: [
          //                                               Text(
          //                                                 'Вам не вистачає ',
          //                                                 style: TextStyle(
          //                                                   color: Hexcolor(
          //                                                       '#747474'),
          //                                                   fontSize: 20,
          //                                                   fontFamily: 'Arial',
          //                                                   fontWeight:
          //                                                       FontWeight.w400,
          //                                                 ),
          //                                               ),
          //                                               Text(
          //                                                 '275' + ' Foint',
          //                                                 style: TextStyle(
          //                                                   color: Hexcolor(
          //                                                       '#FF1E1E'),
          //                                                   fontSize: 20,
          //                                                   fontFamily: 'Arial',
          //                                                   fontWeight:
          //                                                       FontWeight.w700,
          //                                                 ),
          //                                               ),
          //                                             ],
          //                                           ),
          //                                           Text(
          //                                             'для розблокування цієї місії',
          //                                             style: TextStyle(
          //                                               color:
          //                                                   Hexcolor('#747474'),
          //                                               fontSize: 20,
          //                                               fontFamily: 'Arial',
          //                                               fontWeight:
          //                                                   FontWeight.w400,
          //                                             ),
          //                                           ),
          //                                         ],
          //                                       ),
          //                                     ),
          //                                     Container(
          //                                       margin: EdgeInsets.only(
          //                                         left: 20,
          //                                         right: 20,
          //                                       ),
          //                                       width: MediaQuery.of(context)
          //                                           .size
          //                                           .width,
          //                                       height: 165,
          //                                       decoration: BoxDecoration(
          //                                           borderRadius:
          //                                               BorderRadius.all(
          //                                                   Radius.circular(
          //                                                       20)),
          //                                           image: DecorationImage(
          //                                             image: NetworkImage(
          //                                                 questsForMap[index]
          //                                                     .image),
          //                                             fit: BoxFit.cover,
          //                                           )),
          //                                     ),
          //                                     Container(
          //                                       margin: EdgeInsets.only(
          //                                         top: 15,
          //                                         bottom: 15,
          //                                       ),
          //                                       child: Text(
          //                                         questsForMap[index].name,
          //                                         style: TextStyle(
          //                                           color: Hexcolor('#1E2E45'),
          //                                           fontSize: 20,
          //                                           fontWeight: FontWeight.w700,
          //                                           fontFamily: 'Arial',
          //                                         ),
          //                                         textAlign: TextAlign.center,
          //                                       ),
          //                                     ),
          //                                     Row(
          //                                       crossAxisAlignment:
          //                                           CrossAxisAlignment.end,
          //                                       mainAxisAlignment:
          //                                           MainAxisAlignment.center,
          //                                       children: [
          //                                         Text(
          //                                           'Вартість',
          //                                           style: TextStyle(
          //                                               fontSize: 14,
          //                                               color:
          //                                                   Hexcolor('#B9BCC4'),
          //                                               fontFamily: 'Arial',
          //                                               fontWeight:
          //                                                   FontWeight.w400),
          //                                         ),
          //                                         SizedBox(
          //                                           width: 10,
          //                                         ),
          //                                         Text(
          //                                           '450' + ' Foint',
          //                                           style: TextStyle(
          //                                               fontSize: 16,
          //                                               color:
          //                                                   Hexcolor('#FF1E1E'),
          //                                               fontFamily: 'Arial',
          //                                               fontWeight:
          //                                                   FontWeight.w700),
          //                                         ),
          //                                       ],
          //                                     ),
          //                                   ],
          //                                 ),
          //                               ),
          //                             ),
          //                             Container(
          //                               margin: EdgeInsets.only(
          //                                 left: 20,
          //                                 right: 20,
          //                                 bottom: 20,
          //                               ),
          //                               height: 60,
          //                               width:
          //                                   MediaQuery.of(context).size.width,
          //                               child: RaisedButton(
          //                                 child: Text(
          //                                   "Закрити",
          //                                   style: TextStyle(
          //                                     color: Colors.white,
          //                                     fontSize: 17,
          //                                     fontWeight: FontWeight.w700,
          //                                     fontFamily: 'Arial',
          //                                   ),
          //                                 ),
          //                                 color: Hexcolor('#FE6802'),
          //                                 shape: new RoundedRectangleBorder(
          //                                     borderRadius:
          //                                         new BorderRadius.circular(
          //                                             14.0)),
          //                                 onPressed: () {
          //                                   Navigator.of(context).pop();
          //                                 },
          //                               ),
          //                             ),
          //                           ],
          //                         ),
          //                       ),
          //                     ),
          //                   );
          //                 });
          //               });
          //         }

          //         showDescription() {
          //           showDialog(
          //               barrierColor: Colors.white.withOpacity(0.0),
          //               context: context,
          //               builder: (context) {
          //                 var width = MediaQuery.of(context).size.width;

          //                 return Stack(
          //                   overflow: Overflow.visible,
          //                   children: [
          //                     Positioned(
          //                         bottom: 0,
          //                         child: Dialog(
          //                           shape: RoundedRectangleBorder(
          //                               borderRadius: BorderRadius.only(
          //                             topLeft: Radius.circular(20),
          //                             topRight: Radius.circular(20),
          //                           )),
          //                           insetPadding: EdgeInsets.only(
          //                             left: 0,
          //                             right: 0,
          //                           ),
          //                           child: Container(
          //                             width: width,
          //                             height: 260,
          //                             decoration: BoxDecoration(
          //                               color: Hexcolor('#7D5AC2'),
          //                               borderRadius: BorderRadius.only(
          //                                   topLeft: Radius.circular(20.0),
          //                                   topRight: Radius.circular(20.0)),
          //                             ),
          //                             child: Container(
          //                               margin: EdgeInsets.only(
          //                                   right: 25, left: 25, bottom: 25),
          //                               child: Column(
          //                                   mainAxisAlignment:
          //                                       MainAxisAlignment.spaceBetween,
          //                                   children: [
          //                                     Container(
          //                                       margin: EdgeInsets.only(
          //                                         top: 16,
          //                                         bottom: 16,
          //                                       ),
          //                                       width: 43,
          //                                       height: 3,
          //                                       decoration: BoxDecoration(
          //                                         color: Hexcolor('A9B0BA'),
          //                                       ),
          //                                     ),
          //                                     Container(
          //                                       child: Expanded(
          //                                         child: SingleChildScrollView(
          //                                           physics:
          //                                               BouncingScrollPhysics(),
          //                                           child: Container(
          //                                             child: Column(children: [
          //                                               Container(
          //                                                 margin:
          //                                                     EdgeInsets.only(
          //                                                         bottom: 15),
          //                                                 child: Row(
          //                                                   children: [
          //                                                     Flexible(
          //                                                       child: Text(
          //                                                         questsForMap[
          //                                                                 index]
          //                                                             .name,
          //                                                         style: TextStyle(
          //                                                             color: Hexcolor(
          //                                                                 '#FFFFFF'),
          //                                                             fontSize:
          //                                                                 18,
          //                                                             fontFamily:
          //                                                                 'Arial',
          //                                                             fontWeight:
          //                                                                 FontWeight
          //                                                                     .w600),
          //                                                       ),
          //                                                     ),
          //                                                   ],
          //                                                 ),
          //                                               ),
          //                                               Align(
          //                                                 alignment: Alignment
          //                                                     .centerLeft,
          //                                                 child: Text(
          //                                                   questsForMap[index]
          //                                                       .description,
          //                                                   style: TextStyle(
          //                                                     color: Hexcolor(
          //                                                         '#FFFFFF'),
          //                                                     fontSize: 16,
          //                                                     fontFamily:
          //                                                         'Arial',
          //                                                   ),
          //                                                 ),
          //                                               ),
          //                                             ]),
          //                                           ),
          //                                         ),
          //                                       ),
          //                                     ),
          //                                     Row(
          //                                       mainAxisAlignment:
          //                                           MainAxisAlignment
          //                                               .spaceBetween,
          //                                       children: [
          //                                         Container(
          //                                           height: 60,
          //                                           width:
          //                                               MediaQuery.of(context)
          //                                                           .size
          //                                                           .width /
          //                                                       2 -
          //                                                   30,
          //                                           child: RaisedButton(
          //                                             child: Text(
          //                                               "Детальніше",
          //                                               style: TextStyle(
          //                                                 color: Colors.white,
          //                                                 fontSize: 16,
          //                                                 fontFamily: 'Arial',
          //                                                 fontWeight:
          //                                                     FontWeight.w600,
          //                                               ),
          //                                             ),
          //                                             color:
          //                                                 Hexcolor('#FE6802'),
          //                                             shape: new RoundedRectangleBorder(
          //                                                 borderRadius:
          //                                                     new BorderRadius
          //                                                             .circular(
          //                                                         14.0)),
          //                                             onPressed: () {
          //                                               if (!questsForMap[index]
          //                                                   .isLocked) {
          //                                                 Navigator
          //                                                     .pushNamed(
          //                                                         context,
          //                                                         questsForMap[
          //                                                                 index]
          //                                                             .linkDesc,
          //                                                         arguments: ({
          //                                                           'name': questsDifferent[
          //                                                                   index]
          //                                                               .name,
          //                                                           'image': questsDifferent[
          //                                                                   index]
          //                                                               .image,
          //                                                           'distance':
          //                                                               questsDifferent[index]
          //                                                                   .distance,
          //                                                           'time': questsDifferent[
          //                                                                   index]
          //                                                               .time,
          //                                                           'objects': questsDifferent[
          //                                                                   index]
          //                                                               .objects,
          //                                                           'desc': questsDifferent[
          //                                                                   index]
          //                                                               .description
          //                                                         }));
          //                                               } else {
          //                                                 print('its locked');
          //                                                 showLockedWindow();
          //                                               }
          //                                             },
          //                                           ),
          //                                         ),
          //                                         SizedBox(
          //                                           width: 10,
          //                                         ),
          //                                         Container(
          //                                           height: 60,
          //                                           width:
          //                                               MediaQuery.of(context)
          //                                                           .size
          //                                                           .width /
          //                                                       2 -
          //                                                   30,
          //                                           child: RaisedButton(
          //                                             child: Text(
          //                                               "Почати місію",
          //                                               style: TextStyle(
          //                                                 color: Colors.white,
          //                                                 fontSize: 16,
          //                                                 fontFamily: 'Arial',
          //                                                 fontWeight:
          //                                                     FontWeight.w600,
          //                                               ),
          //                                             ),
          //                                             color: !questsForMap[
          //                                                         index]
          //                                                     .isLocked
          //                                                 ? Hexcolor('#FE6802')
          //                                                 : Hexcolor('#BEBEBE'),
          //                                             shape: new RoundedRectangleBorder(
          //                                                 borderRadius:
          //                                                     new BorderRadius
          //                                                             .circular(
          //                                                         14.0)),
          //                                             onPressed: () async {
          //                                               if (!questsForMap[index]
          //                                                   .isLocked) {
          //                                                 Navigator.pushNamed(
          //                                                     context,
          //                                                     questsForMap[
          //                                                             index]
          //                                                         .linkMission);
          //                                               } else {
          //                                                 print('its locked');
          //                                                 showLockedWindow();
          //                                               }

          //                                               // var value =
          //                                               //     'questsForMap[index].isLockedCash'; // DI4

          //                                               // switch (value) {
          //                                               //   case 'questsForMap[index].isLockedCash':
          //                                               //     {
          //                                               //       showConfirmWindowCash();
          //                                               //     }
          //                                               //     break;
          //                                               //   case 'questsForMap[index].isLockedStatus':
          //                                               //     {
          //                                               //       showConfirmWindowStatus();
          //                                               //     }
          //                                               //     break;
          //                                               //   default:
          //                                               //     {
          //                                               //       Navigator.pushNamed(
          //                                               //           context,
          //                                               //           questsForMap[
          //                                               //                   index]
          //                                               //               .linkMission,
          //                                               //           arguments: ({
          //                                               //             'name': questsDifferent[
          //                                               //                     index]
          //                                               //                 .name,
          //                                               //             'image': questsDifferent[
          //                                               //                     index]
          //                                               //                 .image,
          //                                               //             'distance':
          //                                               //                 questsDifferent[index]
          //                                               //                     .distance,
          //                                               //             'time': questsDifferent[
          //                                               //                     index]
          //                                               //                 .time,
          //                                               //             'objects': questsDifferent[
          //                                               //                     index]
          //                                               //                 .objects,
          //                                               //             'desc': questsDifferent[
          //                                               //                     index]
          //                                               //                 .description
          //                                               //           }));
          //                                               //     }
          //                                               //     break;
          //                                               // }
          //                                               // if (questsDifferent[1]
          //                                               //         .isCompleted =
          //                                               //     false) {
          //                                               //   if (questsForMap[
          //                                               //           index]
          //                                               //       .isLocked) {
          //                                               //     if (questsForMap[
          //                                               //             index]
          //                                               //         .isLockedCash) {
          //                                               //       showConfirmWindowCash();
          //                                               //     }
          //                                               //     if (questsForMap[
          //                                               //             index]
          //                                               //         .isLockedStatus) {
          //                                               //       showConfirmWindowStatus();
          //                                               //     }
          //                                               //     if (questsForMap[
          //                                               //             index]
          //                                               //         .isLockedPromo) {
          //                                               //       showLockedWindowWithPromocode();
          //                                               //     }
          //                                               // showConfirmWindowCash();
          //                                               // showConfirmWindowStatus();
          //                                               // showLockedWindowWithPromocode();
          //                                               // print('locked');
          //                                               //   } else {
          //                                               //     Navigator.pushNamed(
          //                                               //         context,
          //                                               //         questsForMap[
          //                                               //                 index]
          //                                               //             .linkMission,
          //                                               //         arguments: ({
          //                                               //           'name': questsDifferent[
          //                                               //                   index]
          //                                               //               .name,
          //                                               //           'image': questsDifferent[
          //                                               //                   index]
          //                                               //               .image,
          //                                               //           'distance': questsDifferent[
          //                                               //                   index]
          //                                               //               .distance,
          //                                               //           'time': questsDifferent[
          //                                               //                   index]
          //                                               //               .time,
          //                                               //           'objects': questsDifferent[
          //                                               //                   index]
          //                                               //               .objects,
          //                                               //           'desc': questsDifferent[
          //                                               //                   index]
          //                                               //               .description
          //                                               //         }));
          //                                               //   }
          //                                               // } else {
          //                                               //   showWindowQuizDone();
          //                                               // }
          //                                             },
          //                                           ),
          //                                         ),
          //                                       ],
          //                                     )
          //                                   ]),
          //                             ),
          //                           ),
          //                         ))
          //                   ],
          //                 );
          //               });
          //         }

          //         hideDescription() {
          //           return Container();
          //         }

          //         void launchUrl(String url) async {
          //           if (await canLaunch(url)) {
          //             await launch(url);
          //           } else {
          //             throw 'Could not open Url';
          //           }
          //         }

          //         return Stack(
          //           overflow: Overflow.visible,
          //           children: [
          //             GestureDetector(
          //               onTap: () {
          //                 if (questsForMap[index].id == 9) {
          //                   launchUrl('https://freegengame.web.app/');
          //                 } else {
          //                   setState(() {
          //                     isHidden = !isHidden;
          //                   });
          //                   if (!isHidden) {
          //                     showDescription();
          //                   } else {
          //                     hideDescription();
          //                   }
          //                   _goToQuest(questsForMap[index].latitude,
          //                       questsForMap[index].longitude);
          //                 }
          //               },
          //               child: Container(
          //                 margin: EdgeInsets.only(left: 5, right: 5),
          //                 height: 117,
          //                 width: 174,
          //                 decoration: BoxDecoration(
          //                     borderRadius:
          //                         BorderRadius.all(Radius.circular(10)),
          //                     image: DecorationImage(
          //                       image: NetworkImage(questsForMap[index].image),
          //                       fit: BoxFit.cover,
          //                     )),
          //                 child: Column(
          //                   mainAxisAlignment: MainAxisAlignment.end,
          //                   children: [
          //                     Container(
          //                       decoration: BoxDecoration(
          //                         color: Colors.transparent,
          //                         borderRadius: BorderRadius.only(
          //                             bottomRight: Radius.circular(10.0),
          //                             bottomLeft: Radius.circular(10.0)),
          //                       ),
          //                       // height: 33,
          //                       child: Container(
          //                         margin: EdgeInsets.only(
          //                           left: 5,
          //                           bottom: 5,
          //                         ),
          //                         child: Row(
          //                           mainAxisAlignment: MainAxisAlignment.start,
          //                           crossAxisAlignment: CrossAxisAlignment.end,
          //                           children: [
          //                             Container(
          //                               child: Flexible(
          //                                 child: Text(
          //                                   questsForMap[index].name,
          //                                   style: TextStyle(
          //                                     color: Hexcolor('FFFFFF'),
          //                                     fontSize: 14,
          //                                     fontFamily: 'Arial',
          //                                     fontWeight: FontWeight.w600,
          //                                     letterSpacing: 1.09,
          //                                   ),
          //                                 ),
          //                               ),
          //                             ),
          //                           ],
          //                         ),
          //                       ),
          //                     )
          //                   ],
          //                 ),
          //               ),
          //             ),
          //             Positioned(
          //               top: 10,
          //               left: 12.5,
          //               child: questsForMap[index].isLocked
          //                   ? Image.asset(
          //                       'assets/fg_images/6_home_logo_lock.png',
          //                       height: 38,
          //                       width: 38,
          //                     )
          //                   : Container(),
          //             ),
          //           ],
          //         );
          //       },
          //     ),
          //   ),
          // ),
          Positioned(
              bottom: 24, // Chips
              child: Container(
                width: MediaQuery.of(context).copyWith().size.width,
                child: Container(
                  margin: EdgeInsets.only(right: 14, left: 14),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 14.5,
                          child: RaisedButton(
                            child: Text(
                              "0 км",
                              style: TextStyle(
                                color: isPressed0
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed0
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = true;
                                isPressed5 = false;
                                isPressed10 = false;
                                isPressed15 = false;
                                _set0km(18.0);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 14.5,
                          child: RaisedButton(
                            child: Text(
                              "5 км",
                              style: TextStyle(
                                color: isPressed5
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed5
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = false;
                                isPressed5 = true;
                                isPressed10 = false;
                                isPressed15 = false;
                                _set5km(12);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 14.5,
                          child: RaisedButton(
                            child: Text(
                              "10 км",
                              style: TextStyle(
                                color: isPressed10
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed10
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = false;
                                isPressed5 = false;
                                isPressed10 = true;
                                isPressed15 = false;
                                _set10km(11.25);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 4 - 14.5,
                          child: RaisedButton(
                            child: Text(
                              "15 км",
                              style: TextStyle(
                                color: isPressed15
                                    ? Colors.white
                                    : Hexcolor('#8E8E8E'),
                                fontSize: 15,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            color: isPressed15
                                ? Hexcolor('#FE6802')
                                : Hexcolor('#F4F4F4'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              setState(() {
                                isPressed0 = false;
                                isPressed5 = false;
                                isPressed10 = false;
                                isPressed15 = true;
                                _set15km(10.5);
                              });
                            },
                          ),
                        ),
                      ]),
                ),
              )),
        ]),
      ),
    );
  }
}
