import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '10_profile_rewards.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import '6_home_quests.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:geolocator/geolocator.dart';

import 'package:flutter_free_gen_reality/services/services.dart';
import '6_home_missions_json.dart';
import '6_home_missions_favorite_json.dart';
import 'dart:convert' show utf8;

import '10_profile_json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import './10_profile_statuses_json.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Geolocator geolocator = Geolocator();
  var currentLocation;

  final CategoriesScroller categoriesScroller = CategoriesScroller();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool englishSelected = false;
  bool ukrainianSelected = false;
  bool russianSelected = false;
  bool menuIsVisible = false;
  bool isPromoFieldVisible = false;
  bool isPromoChecked = false;
  bool isPromoRight = false;
  bool isPromoWrong = false;

  // _setEng() {
  //   setState(() {
  //     if (ukrainianSelected || russianSelected) {
  //       ukrainianSelected = false;
  //       russianSelected = false;
  //     }
  //     englishSelected = true;
  //   });
  // }
  // _setUkr() {
  //   setState(() {
  //     if (englishSelected || russianSelected) {
  //       englishSelected = false;
  //       russianSelected = false;
  //     }
  //     ukrainianSelected = true;
  //   });
  // }

  // _setRus() {
  //   setState(() {
  //     if (englishSelected || ukrainianSelected) {
  //       englishSelected = false;
  //       ukrainianSelected = false;
  //     }
  //     russianSelected = true;
  //   });
  // }
  // bool mission12NewYearTreesCompleted;
  // checkTrees() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   setState(() {
  //     mission12NewYearTreesCompleted =
  //         sharedPreferences.get("Mission12NewYearTreesCompleted");
  //   });
  //   // print(mission12NewYearTreesCompleted);
  // }

  bool loadingPosition = true;

  getPosition() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation)
        .then((currloc) {
      setState(() {
        currentLocation = currloc;
        loadingPosition = false;
      });
    });
  }

  String userStatus;

  getUserData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    String email = sharedPreferences.get("email");
    String token = sharedPreferences.get("token");
    String url =
        'http://generation-admin.ehub.com.ua/api/user/find?email=' + '$email';

    Future<User> getUser() async {
      try {
        final response = await http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });

        Map<String, dynamic> responseJson = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            for (int i = 0; i < userStatuses.length; i++) {
              if (responseJson["status"] == userStatuses[i].id.toString()) {
                userStatus =
                    utf8.decode(userStatuses[i].titleUa.runes.toList());
              }
            }
            print('User status: $userStatus');
            sharedPreferences.setString("userStatus", userStatus);
          });
        }

        if (response.statusCode == 200) {
          final User user = userFromJson(response.body);
          return user;
        } else {
          return User();
        }
      } catch (e) {
        print(e);
        return User();
      }
    }

    getUser();
  }

  List<Publication> userStatuses;
  getStatuses() {
    ServicesStatuses.getStatuses().then((list) {
      setState(() {
        userStatuses = list.publication;
      });
    });
  }

  List<FavoriteMissions> favoriteMissions = List<FavoriteMissions>();
  List<Mission> missions;
  bool loading;
  String link = 'http://generation-admin.ehub.com.ua/api/file/downloadFile/';
  // String link = Link.urlDev;
  @override
  void initState() {
    loading = true;
    getStatuses();
    getUserData();
    getPosition();
    ServicesMissions.getMissions().then((list) {
      setState(() {
        missions = list.missions;
        // if (loadingPosition == false) {
        //   for (int i = 0; i < missions.length; i++) {
        //     Mission mission = missions[i];
        //     var missionLatitude = double.parse(mission.location.latitude);
        //     var missionLongitude = double.parse(mission.location.longitude);
        //     setState(() {
        //       mission.distanceInMeters = Geolocator.distanceBetween(
        //           currentLocation.latitude,
        //           currentLocation.longitude,
        //           missionLatitude,
        //           missionLongitude);
        //     });
        //   }
        // }
        ServicesFavoriteMissions.getFavoriteMissions().then((list) {
          setState(() {
            favoriteMissions = list;
          });
        });
        loading = false;
      });
    });

    super.initState();
  }

  bool isPlaying = false;
  bool isPaused = true;

  AudioCache cache; // you have this
  AudioPlayer player; // create this

  // void _playFile() async {
  //   if (!isPlaying && isPaused) {
  //     player = await AudioCache().play("music/main.mp3");
  //     setState(() {
  //       isPlaying = true;
  //       isPaused = false;
  //     }); // assign player here
  //   }
  // }

  // void _stopFile() {
  //   player?.stop(); // stop the file like this
  // }

  void pausePlay() {
    if (!isPaused && isPlaying) {
      player.pause();
      setState(() {
        isPlaying = false;
        isPaused = true;
      });
    } else {
      player.resume();

      setState(() {
        isPlaying = true;
        isPaused = false;
      });
    }
  }

  void _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open Url';
    }
  }

  Future<void> shareApp() async {
    await FlutterShare.share(
      title: 'Завантажуйте новий додаток і спробуйте виконати місії',
      // text: 'Example share text',
      linkUrl:
          'https://play.google.com/store/apps/details?id=com.freegen.flutterfreegenapp',
      // chooserTitle: 'Example Chooser Title'
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
          child: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        margin: EdgeInsets.only(top: 25),
        child: Column(children: [
          Container(
            margin: EdgeInsets.only(
              top: 20,
              bottom: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    // Navigator.pushNamed(context, '/player_audio.dart');
                  },
                  child: Image.asset(
                    'assets/fg_images/main_icon.png',
                    width: 40,
                    height: 34,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    // Navigator.pushNamed(context, '/player_video.dart');
                  },
                  child: Image.asset(
                    'assets/fg_images/6_home_sidebar_title_image.png',
                    width: 160,
                    height: 30,
                  ),
                ),
              ],
            ),
          ),
          Container(
              // child: Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   children: [
              //     Container(
              //       margin: EdgeInsets.only(
              //         left: 10,
              //         right: 5,
              //       ),
              //       height: 45,
              //       width: (MediaQuery.of(context).size.width - 108) / 3 - 13.5,
              //       child: RaisedButton(
              //         child: Text(
              //           "Eng",
              //           style: TextStyle(
              //             color: Colors.white,
              //             fontSize: 15,
              //             fontWeight: FontWeight.w600,
              //             fontFamily: 'Arial',
              //           ),
              //         ),
              //         color: englishSelected
              //             ? Hexcolor('#FE6802')
              //             : Hexcolor('#E6E6E6'),
              //         shape: new RoundedRectangleBorder(
              //             borderRadius: new BorderRadius.circular(22.5)),
              //         onPressed: () {
              //           if (!englishSelected) {
              //             _setEng();
              //           }
              //         },
              //       ),
              //     ),
              //     Container(
              //       margin: EdgeInsets.only(
              //         left: 5,
              //         right: 5,
              //       ),
              //       height: 45,
              //       width: (MediaQuery.of(context).size.width - 108) / 3 - 13.5,
              //       child: RaisedButton(
              //         child: Text(
              //           "Укр",
              //           style: TextStyle(
              //             color: Colors.white,
              //             fontSize: 15,
              //             fontWeight: FontWeight.w600,
              //             fontFamily: 'Arial',
              //           ),
              //         ),
              //         color: ukrainianSelected
              //             ? Hexcolor('#FE6802')
              //             : Hexcolor('#E6E6E6'),
              //         shape: new RoundedRectangleBorder(
              //             borderRadius: new BorderRadius.circular(22.5)),
              //         onPressed: () {
              //           if (!ukrainianSelected) {
              //             _setUkr();
              //           }
              //         },
              //       ),
              //     ),
              //     Container(
              //       margin: EdgeInsets.only(
              //         left: 5,
              //         right: 10,
              //       ),
              //       height: 45,
              //       width: (MediaQuery.of(context).size.width - 108) / 3 - 13.5,
              //       child: RaisedButton(
              //         child: Text(
              //           "Рус",
              //           style: TextStyle(
              //             color: Colors.white,
              //             fontSize: 15,
              //             fontWeight: FontWeight.w600,
              //             fontFamily: 'Arial',
              //           ),
              //         ),
              //         color: russianSelected
              //             ? Hexcolor('#FE6802')
              //             : Hexcolor('#E6E6E6'),
              //         shape: new RoundedRectangleBorder(
              //             borderRadius: new BorderRadius.circular(22.5)),
              //         onPressed: () {
              //           if (!russianSelected) {
              //             _setRus();
              //           }
              //         },
              //       ),
              //     ),
              //   ],
              // ),
              ),
          Container(
            // height: MediaQuery.of(context).size.height / 2,
            child: Expanded(
              flex: 1,
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: itemsForMenu.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        if (itemsForMenu[index].link == '') {
                          print(
                              'There will be function -logOut- in the future');
                        } else {
                          Navigator.pushNamed(
                              context, itemsForMenu[index].link);
                        }
                      },
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 20, bottom: 0),
                            child: Row(
                              children: [
                                CircleAvatar(
                                  radius: 22.5,
                                  backgroundColor: itemsForMenu[index].color,
                                  child: ClipRRect(
                                    child: Image.asset(
                                      itemsForMenu[index].icon,
                                      width: 22,
                                      height: 22,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20),
                                  child: Text(
                                    itemsForMenu[index].name,
                                    style: TextStyle(
                                      color: Hexcolor('#1E2E45'),
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Arial',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              top: 10,
                              bottom: 10,
                            ),
                            child: Divider(
                              color: Hexcolor('#E6E6E6'),
                              thickness: 1,
                            ),
                          ),
                        ],
                      ));
                },
              ),
            ),
          ),
          // GestureDetector(
          //   behavior: HitTestBehavior.opaque,
          //   onTap: () {
          //     pausePlay();
          //   },
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.start,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       Container(
          //         margin: EdgeInsets.only(
          //           left: 20,
          //           right: 16,
          //         ),
          //         child: Image.asset(
          //           isPlaying && !isPaused
          //               ? 'assets/fg_images/0_splash_screen_icon_pause.png'
          //               : 'assets/fg_images/0_splash_screen_icon_play.png',
          //           width: 40,
          //           height: 34.67,
          //           color: Hexcolor('#FE6802'),
          //         ),
          //       ),
          //       Text(
          //         isPlaying && !isPaused
          //             ? 'Вимкнути музику'
          //             : 'Увімкнути музыку',
          //         style: TextStyle(
          //           color: Hexcolor('#1E2E45'),
          //           fontSize: 17,
          //           fontWeight: FontWeight.w600,
          //           fontFamily: 'Arial',
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          Container(
            margin: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: 15,
              top: 15,
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      _launchUrl('https://www.facebook.com/FreeGen.Org');
                    },
                    child: Image.asset(
                      'assets/fg_images/6_home_sidebar_facebook.png',
                      width: 12.45,
                      height: 24,
                      color: Hexcolor('#FE6802'),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      _launchUrl('https://t.me/poKLICH');
                    },
                    child: Image.asset(
                      'assets/fg_images/6_home_sidebar_telegram.png',
                      width: 26.69,
                      height: 22.38,
                      color: Hexcolor('#FE6802'),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      _launchUrl('https://www.instagram.com/freegen_life/');
                    },
                    child: Image.asset(
                      'assets/fg_images/6_home_sidebar_instagram.png',
                      width: 26.67,
                      height: 26.67,
                      color: Hexcolor('#FE6802'),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      _launchUrl(
                          'https://www.youtube.com/playlist?list=PLh8Z__AgPfhNuqHCVEq8vJoUH-XpdD4ZC');
                    },
                    child: Image.asset(
                      'assets/fg_images/6_home_sidebar_youtube.png',
                      width: 28,
                      // height: 20,
                      color: Hexcolor('#FE6802'),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      shareApp();
                    },
                    child: Image.asset(
                      'assets/fg_images/6_home_sidebar_share.png',
                      width: 28.03,
                      height: 26.03,
                      color: Hexcolor('#FE6802'),
                    ),
                  ),
                ]),
          ),
          Container(
              margin: EdgeInsets.only(
                bottom: 10,
              ),
              child: Text(
                'Бета-версія. Етап відладки. Будемо вдячні за зауваження, надсилайте на FreeGenorg@gmail.com.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Arial',
                  fontSize: 12,
                  color: Hexcolor('#919191'),
                ),
              ))
        ]),
      )),
      body: loading
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Hexcolor('#7D5AC2'),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Hexcolor('#FE6802'),
                ),
              ),
            )
          : Container(
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 40,
                        left: 15,
                        right: 15,
                        bottom: 13,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          // GestureDetector(
                          //   onTap: () {
                          //     Navigator.push(
                          //       context,
                          //       MaterialPageRoute(builder: (context) => TestApp()),
                          //     );
                          //   },
                          //   child:
                          GestureDetector(
                            onTap: () async {
                              // Navigator.pushNamed(context, '/quest_done.dart');
                              // Navigator.pushNamed(context, '/mission_muromec_rating.dart'); //
                              // Navigator.pushNamed(context, '/run_map_quest.dart');
                              // Navigator.pushNamed(context, '/4_enter_register');
                              // Navigator.pushNamed(context, '/error_404_page.dart');

                              // Vibration.vibrate(duration: 1000);
                              // Vibration.vibrate(pattern: [500, 1000, 500, 2000]);
                              // Vibration.vibrate(pattern: [500, 1000, 500, 2000], intensities: [1, 255]);
                            },
                            child: Text(
                              'Головна',
                              style: TextStyle(
                                  fontSize: 37,
                                  color: Colors.white,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w900),
                            ),
                          ),
                          // ),
                          GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {},
                              child: SizedBox(
                                width: 50,
                                height: 35,
                              )),
                          Row(
                            children: [
                              SizedBox(
                                width: 40,
                                height: 55,
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/fg_images/6_home_logo_search.png',
                                    width: 19,
                                    height: 19,
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, '/6_home_search.dart');
                                  },
                                ),
                              ),
                              // SizedBox( // FILTER 2ND STEP
                              //   width: 40,
                              //   height: 55,
                              //   child: IconButton(
                              //     icon: Image.asset(
                              //       'assets/fg_images/6_home_logo_filter.png',
                              //       width: 17,
                              //       height: 18,
                              //     ),
                              //     onPressed: () {
                              //       Navigator.pushNamed(
                              //           context, '/6_home_filter.dart');
                              //     },
                              //   ),
                              // ),

                              SizedBox(
                                width: 40,
                                height: 55,
                                child: IconButton(
                                    icon: Image.asset(
                                      'assets/fg_images/5_home_logo_profile2.png',
                                      width: 20,
                                      height: 20,
                                      color: menuIsVisible
                                          ? Hexcolor('#E85B14')
                                          : Colors.white,
                                    ),
                                    onPressed: () {
                                      _scaffoldKey.currentState.openDrawer();
                                      // if (!isPlaying && isPaused) {
                                      //   _playFile();
                                      // }
                                    }),
                              ),

                              // menuIsVisible ? Drawer(
                              //   child: Container(
                              //     width: 200,
                              //     // height: 500,
                              //     color: Colors.white,
                              //   )
                              // ) : Container()
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                      height: 135,
                      child:

                          // from APP
                          // categoriesScroller),

                          // from DATABASE
                          Container(
                        decoration: BoxDecoration(
                          color: Hexcolor('#7D5AC2'),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    physics: BouncingScrollPhysics(),
                                    itemCount: missions.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      Mission mission = missions[index];

                                      var missionName = utf8.decode(
                                          mission.titleUa.runes.toList());
                                      var missionImage = utf8.decode(
                                          mission.imageUrl.runes.toList());
                                      var missionProgress = mission.progress;
                                      int missionId = mission.id;
                                      var missionRecommend = mission.recommend;

                                      showWindowQuizDone() {
                                        showDialog(
                                            barrierDismissible: false,
                                            barrierColor: Hexcolor('#341F5E')
                                                .withOpacity(0.8),
                                            context: context,
                                            builder: (context) {
                                              return Dialog(
                                                insetPadding: EdgeInsets.only(
                                                  left: 20,
                                                  right: 20,
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                child: Stack(
                                                  overflow: Overflow.visible,
                                                  children: [
                                                    Container(
                                                      height: 250,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 20,
                                                            ),
                                                            child: Text(
                                                              'Завдання пройдене',
                                                              style: TextStyle(
                                                                color: Hexcolor(
                                                                    '#1E2E45'),
                                                                fontSize: 24,
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                          ),
                                                          Text(
                                                            'Ви не можете повторно пройти це завдання',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#747474'),
                                                              fontSize: 16,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              height: 1.4,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              left: 20,
                                                              right: 20,
                                                              bottom: 20,
                                                            ),
                                                            height: 60,
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width -
                                                                80,
                                                            child: RaisedButton(
                                                              child: Text(
                                                                "Закрити",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        17,
                                                                    fontFamily:
                                                                        'Arial',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700,
                                                                    letterSpacing:
                                                                        1.05),
                                                              ),
                                                              color: Hexcolor(
                                                                  '#FE6802'),
                                                              shape: new RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      new BorderRadius
                                                                              .circular(
                                                                          14.0)),
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            });
                                      }

                                      return missionRecommend
                                          ? GestureDetector(
                                              onTap: () async {
                                                if (missionProgress ==
                                                    'FINISHED') {
                                                  showWindowQuizDone();
                                                } else {
                                                  final SharedPreferences
                                                      sharedPreferences =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  sharedPreferences.setInt(
                                                      'missionId', missionId);

                                                  Navigator.pushNamed(context,
                                                      '/6_mission_desc.dart');
                                                }
                                              },
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 15),
                                                child: Column(
                                                  children: <Widget>[
                                                    Stack(
                                                        overflow:
                                                            Overflow.visible,
                                                        children: [
                                                          Positioned(
                                                              right: 4,
                                                              child:
                                                                  CircleAvatar(
                                                                radius: 45,
                                                                backgroundColor:
                                                                    Hexcolor(
                                                                        '#EB5C18'),
                                                              )),
                                                          CircleAvatar(
                                                            radius: 45,
                                                            backgroundImage: NetworkImage(mission
                                                                            .imageUrl ==
                                                                        "" ||
                                                                    mission.imageUrl ==
                                                                        null
                                                                ? '$link' +
                                                                    'default.png'
                                                                : '$link' +
                                                                    missionImage),
                                                          ),
                                                        ]),
                                                    Container(
                                                      width: 90,
                                                      margin: EdgeInsets.only(
                                                          top: 10),
                                                      child: Text(
                                                        missionName,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 15,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          : Container();
                                    }),
                              ),
                            ),
                          ],
                        ),
                      )),

                  // FROM DATABASE

                  loadingPosition
                      ? Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 100),
                            child: CircularProgressIndicator(
                              backgroundColor: Hexcolor('#7D5AC2'),
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                Hexcolor('#FE6802'),
                              ),
                            ),
                          ),
                        )
                      : Expanded(
                          child: ListView.builder(
                              physics: BouncingScrollPhysics(),
                              itemCount: missions.length,
                              itemBuilder: (context, index) {
                                Mission mission = missions[index];

                                var missionName =
                                    utf8.decode(mission.titleUa.runes.toList());
                                var missionImage = utf8
                                    .decode(mission.imageUrl.runes.toList());
                                var missionTime = mission.duration.toString();
                                var missionProgress = mission.progress;
                                int missionId = mission.id;
                                var missionLatitude =
                                    double.parse(mission.location.latitude);
                                var missionLongitude =
                                    double.parse(mission.location.longitude);

                                var distanceInMeters;
                                distanceInMeters = Geolocator.distanceBetween(
                                    currentLocation.latitude,
                                    currentLocation.longitude,
                                    missionLatitude,
                                    missionLongitude);
                                var distanceInKilometers =
                                    (distanceInMeters * 0.001)
                                        .toStringAsFixed(2);

                                var missionAccessType = mission.accessType;
                                var missionAccessGranted;

                                if (missionAccessType == 'BYSTATUS') {
                                  List<String> missionStatuses =
                                      new List<String>();
                                  for (final status in mission.statuses) {
                                    if (status.active == true) {
                                      missionStatuses.add(utf8.decode(
                                          status.titleUa.runes.toList()));
                                    }
                                  }
                                  //print(missionStatuses);
                                  if (missionStatuses.contains(userStatus)) {
                                    missionAccessGranted = true;
                                  } else {
                                    missionAccessGranted = false;
                                  }
                                }

                                // mission.distanceInMeters = double.parse(
                                //     (distanceInMeters).toStringAsFixed(0));
                                // print(mission.distanceInMeters.toInt());

                                // missions.sort((a,b)=>a.titleUa.compareTo(b.titleUa)); // sort by String

                                // Comparator<Mission> distanceInMeters = (a, b) =>
                                //     a.distanceInMeters.toInt().compareTo(b
                                //         .distanceInMeters
                                //         .toInt()); // sort by id
                                // missions.sort(distanceInMeters);

                                missions[index].isSelected = false;
                                // print(missions[index].isSelected);

                                // toggleSelection() {
                                //   missions[index].isSelected =
                                //       !missions[index].isSelected;
                                // }

                                // Future<void> share() async {
                                //   await FlutterShare.share(
                                //     title: missionName == 'Київ проти Корони'
                                //         ? " "
                                //         : 'Завантажуйте новий додаток і спробуйте виконати місію \n\n' +
                                //             "'" +
                                //             missionName +
                                //             "'" +
                                //             '\n',
                                //     // text: 'Example share text',
                                //     linkUrl: missionName == 'Київ проти Корони'
                                //         ? 'https://freegengame.web.app/'
                                //         : 'https://play.google.com/store/apps/details?id=com.freegen.flutterfreegenapp',
                                //     // chooserTitle: 'Example Chooser Title'
                                //   );
                                // }

                                for (var favoriteMission in favoriteMissions) {
                                  if (favoriteMission.id == mission.id) {
                                    mission.isSelected = true;
                                  }
                                }

                                Future<void> share() async {
                                  await FlutterShare.share(
                                    title:
                                        'Завантажуйте новий додаток і спробуйте виконати місію \n\n' +
                                            "'" +
                                            missionName +
                                            "'" +
                                            '\n',
                                    // text: 'Example share text',
                                    linkUrl:
                                        'https://play.google.com/store/apps/details?id=com.freegen.flutterfreegenapp',
                                    // chooserTitle: 'Example Chooser Title'
                                  );
                                }

                                showWindowQuizDone() {
                                  showDialog(
                                      barrierDismissible: false,
                                      barrierColor:
                                          Hexcolor('#341F5E').withOpacity(0.8),
                                      context: context,
                                      builder: (context) {
                                        return Dialog(
                                          insetPadding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Stack(
                                            overflow: Overflow.visible,
                                            children: [
                                              Container(
                                                height: 250,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 20,
                                                      ),
                                                      child: Text(
                                                        'Завдання пройдене',
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#1E2E45'),
                                                          fontSize: 24,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      'Ви не можете повторно пройти це завдання',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color:
                                                            Hexcolor('#747474'),
                                                        fontSize: 16,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.4,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                        bottom: 20,
                                                      ),
                                                      height: 60,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              80,
                                                      child: RaisedButton(
                                                        child: Text(
                                                          "Закрити",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 17,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              letterSpacing:
                                                                  1.05),
                                                        ),
                                                        color:
                                                            Hexcolor('#FE6802'),
                                                        shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    14.0)),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      });
                                }

                                var color;

                                setColor() {
                                  if (missionAccessType == "FREE") {
                                    color =
                                        Hexcolor('#1E2E45').withOpacity(0.5);
                                  }
                                  if (missionAccessType == "CLOSED" ||
                                      missionAccessType == "PROMO" ||
                                      missionAccessType == "PAID" ||
                                      (mission.accessType == "BYSTATUS" &&
                                          missionAccessGranted == false)) {
                                    color =
                                        Hexcolor('#FE6802').withOpacity(0.5);
                                  }
                                  if (mission.accessType == "BYSTATUS" &&
                                      missionAccessGranted == true) {
                                    color =
                                        Hexcolor('#219653').withOpacity(0.5);
                                  }
                                }

                                setColor();

                                addMissionToFavorites() async {
                                  SharedPreferences sharedPreferences =
                                      await SharedPreferences.getInstance();
                                  String token = sharedPreferences.get("token");
                                  String id =
                                      sharedPreferences.get("id").toString();
                                  String url =
                                      'http://generation-admin.ehub.com.ua/api/favorites/add';
                                  final http.Response response =
                                      await http.post(
                                    url,
                                    headers: <String, String>{
                                      'Content-Type': 'application/json',
                                      'Accept': 'application/json',
                                      'Authorization': 'Bearer $token',
                                    },
                                    body: jsonEncode(<String, dynamic>{
                                      "user_id": id,
                                      "mission_id": missionId
                                    }),
                                  );
                                  if (response.statusCode == 200) {
                                    print(response.body);
                                    print('mission added to Favorites');
                                    Navigator.pushNamed(
                                        context, '/5_myBottomBar.dart');
                                  } else {
                                    throw Exception('Failed to add mission.');
                                  }
                                }

                                deleteMissionFromFavorites() async {
                                  SharedPreferences sharedPreferences =
                                      await SharedPreferences.getInstance();
                                  String token = sharedPreferences.get("token");
                                  String id =
                                      sharedPreferences.get("id").toString();
                                  final url = Uri.parse(
                                      'http://generation-admin.ehub.com.ua/api/favorites/delete');
                                  final request = http.Request("DELETE", url);
                                  request.headers.addAll(<String, String>{
                                    'Content-Type': 'application/json',
                                    'Accept': 'application/json',
                                    'Authorization': 'Bearer $token',
                                  });
                                  request.body = jsonEncode(
                                      {"user_id": id, "mission_id": missionId});
                                  final response = await request.send();
                                  if (response.statusCode == 200) {
                                    print('mission deleted from Favorites');
                                    Navigator.pushNamed(
                                        context, '/5_myBottomBar.dart');
                                  } else {
                                    throw Exception(
                                        'Failed to delete mission.');
                                  }
                                }

                                return Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      GestureDetector(
                                        onTap: () async {
                                          if (missionProgress == 'FINISHED') {
                                            showWindowQuizDone();
                                          } else {
                                            final SharedPreferences
                                                sharedPreferences =
                                                await SharedPreferences
                                                    .getInstance();
                                            sharedPreferences.setInt(
                                                'missionId', missionId);
                                            Navigator.pushNamed(context,
                                                '/6_mission_desc.dart');
                                          }
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 23, left: 14, right: 14),
                                          height: 190,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10)),
                                              image: DecorationImage(
                                                image: NetworkImage(mission
                                                                .imageUrl ==
                                                            "" ||
                                                        mission.imageUrl == null
                                                    ? '$link' + 'default.png'
                                                    : '$link' + missionImage),
                                                fit: BoxFit.cover,
                                              )),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                height: 55,
                                                decoration: BoxDecoration(
                                                  color: color,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          bottomRight:
                                                              Radius.circular(
                                                                  10.0),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10.0)),
                                                ),
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      top: 5,
                                                      bottom: 5,
                                                      left: 10,
                                                      right: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        child: Flexible(
                                                          child: Text(
                                                            missionName,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 16,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              letterSpacing:
                                                                  1.09,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      missionName ==
                                                              "Київ проти Корони"
                                                          ? Container()
                                                          : Container(
                                                              width: 130,
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: [
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.center,
                                                                        children: [
                                                                          Text(
                                                                            distanceInKilometers +
                                                                                " км",
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 13,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w700,
                                                                              letterSpacing: 1.09,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            " від вас",
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 13,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w400,
                                                                              letterSpacing: 1.09,
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                      Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                10,
                                                                            right:
                                                                                1.5),
                                                                        child: Image
                                                                            .asset(
                                                                          "assets/fg_images/6_home_quests_pin.png",
                                                                          width:
                                                                              10.5,
                                                                          height:
                                                                              15,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 5,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: [
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.center,
                                                                        children: [
                                                                          Text(
                                                                            missionTime,
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 13,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w700,
                                                                              letterSpacing: 1.09,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            " хвилин",
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 13,
                                                                              fontFamily: 'Arial',
                                                                              fontWeight: FontWeight.w400,
                                                                              letterSpacing: 1.09,
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                      Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                10),
                                                                        child: Image
                                                                            .asset(
                                                                          "assets/fg_images/6_home_logo_time.png",
                                                                          width:
                                                                              13.33,
                                                                          height:
                                                                              13.33,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  )
                                                                ],
                                                              ),
                                                            )
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      missions[index].isSelected == true
                                          ? Positioned(
                                              right: 28,
                                              top: 14,
                                              child: GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  deleteMissionFromFavorites();
                                                },
                                                child: Image.asset(
                                                  'assets/fg_images/9_favorites_icon.png',
                                                  height: 23,
                                                  width: 18,
                                                ),
                                              ))
                                          : Positioned(
                                              right: 28,
                                              top: 14,
                                              child: GestureDetector(
                                                behavior:
                                                    HitTestBehavior.opaque,
                                                onTap: () {
                                                  addMissionToFavorites();
                                                },
                                                child: Image.asset(
                                                  'assets/fg_images/sportGames_icon_favorite.png',
                                                  height: 23,
                                                  width: 18,
                                                ),
                                              )),
                                      Positioned(
                                          right: 12,
                                          top: (190 / 2) - (50 / 2),
                                          child: SizedBox(
                                            height: 50,
                                            width: 50,
                                            child: IconButton(
                                              icon: Image.asset(
                                                'assets/fg_images/6_home_logo_share.png',
                                                width: 20,
                                                height: 20,
                                              ),
                                              onPressed: () {
                                                share();
                                              },
                                            ),
                                          )),
                                      missionProgress == 'FINISHED'
                                          ? Positioned(
                                              top: 20,
                                              left: 20,
                                              child: GestureDetector(
                                                onTap: () {
                                                  if (missionName ==
                                                      "Муромець race (тест)") {
                                                    Navigator.pushNamed(context,
                                                        '/mission_muromec_rating.dart');
                                                  }
                                                },
                                                child: Image.asset(
                                                  'assets/fg_images/6_home_logo_mission_completed2.png',
                                                  height: 38,
                                                  width: 150,
                                                ),
                                              ))
                                          : Container(),
                                      (mission.accessType == "CLOSED" ||
                                                  mission.accessType ==
                                                      "PROMO" ||
                                                  mission.accessType ==
                                                      "PAID" ||
                                                  (mission.accessType ==
                                                          "BYSTATUS" &&
                                                      missionAccessGranted ==
                                                          false)) &&
                                              missionProgress != 'FINISHED'
                                          ? Positioned(
                                              top: 12,
                                              left: 26,
                                              child: Image.asset(
                                                'assets/fg_images/6_home_logo_lock.png',
                                                height: 38,
                                                width: 38,
                                              ))
                                          : Container(),
                                      mission.accessType == "BYSTATUS" &&
                                              missionAccessGranted == true
                                          ? Positioned(
                                              top: 12,
                                              left: 26,
                                              child: Image.asset(
                                                'assets/fg_images/6_home_logo_unlock.png',
                                                height: 38,
                                                width: 38,
                                              ))
                                          : Container(),
                                    ]);
                              }),
                        ),

                  // FROM APP

                  // Expanded(
                  //   child: ListView.builder(
                  //     physics: BouncingScrollPhysics(),
                  //     itemCount: questsDifferent.length,
                  //     itemBuilder: (context, index) {
                  //       showLockedWindow() {
                  //         showDialog(
                  //             barrierColor:
                  //                 Hexcolor('#341F5E').withOpacity(0.8),
                  //             barrierDismissible: false,
                  //             context: context,
                  //             builder: (context) {
                  //               var _blankFocusNode2 = new FocusNode();
                  //               return StatefulBuilder(
                  //                   builder: (context, setState) {
                  //                 return Dialog(
                  //                   insetPadding: EdgeInsets.only(
                  //                     left: 20,
                  //                     right: 20,
                  //                   ),
                  //                   shape: RoundedRectangleBorder(
                  //                     borderRadius: BorderRadius.circular(10),
                  //                   ),
                  //                   child: GestureDetector(
                  //                     behavior: HitTestBehavior.opaque,
                  //                     onTap: () {
                  //                       FocusScope.of(context)
                  //                           .requestFocus(_blankFocusNode2);
                  //                     },
                  //                     child: Container(
                  //                       // width: MediaQuery.of(context).size.width - 100,
                  //                       height: 510,
                  //                       child: Column(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.spaceBetween,
                  //                         crossAxisAlignment:
                  //                             CrossAxisAlignment.center,
                  //                         children: [
                  //                           Container(
                  //                             alignment: Alignment.center,
                  //                             margin: EdgeInsets.only(
                  //                               top: 20,
                  //                             ),
                  //                             child: Text(
                  //                               'Місія недоступна',
                  //                               style: TextStyle(
                  //                                 color: Hexcolor('#1E2E45'),
                  //                                 fontSize: 24,
                  //                                 fontWeight: FontWeight.w700,
                  //                                 fontFamily: 'Arial',
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           Expanded(
                  //                             child: SingleChildScrollView(
                  //                               physics:
                  //                                   BouncingScrollPhysics(),
                  //                               child: Column(
                  //                                 mainAxisAlignment:
                  //                                     MainAxisAlignment
                  //                                         .spaceBetween,
                  //                                 crossAxisAlignment:
                  //                                     CrossAxisAlignment.center,
                  //                                 children: [
                  //                                   Container(
                  //                                     margin: EdgeInsets.only(
                  //                                       top: 15,
                  //                                       bottom: 20,
                  //                                     ),
                  //                                     child: Column(
                  //                                       mainAxisAlignment:
                  //                                           MainAxisAlignment
                  //                                               .center,
                  //                                       crossAxisAlignment:
                  //                                           CrossAxisAlignment
                  //                                               .center,
                  //                                       children: [
                  //                                         Row(
                  //                                           mainAxisAlignment:
                  //                                               MainAxisAlignment
                  //                                                   .center,
                  //                                           crossAxisAlignment:
                  //                                               CrossAxisAlignment
                  //                                                   .end,
                  //                                           children: [
                  //                                             Text(
                  //                                               'Вам не вистачає ',
                  //                                               style:
                  //                                                   TextStyle(
                  //                                                 color: Hexcolor(
                  //                                                     '#747474'),
                  //                                                 fontSize: 20,
                  //                                                 fontFamily:
                  //                                                     'Arial',
                  //                                                 fontWeight:
                  //                                                     FontWeight
                  //                                                         .w400,
                  //                                               ),
                  //                                             ),
                  //                                             Text(
                  //                                               '275' +
                  //                                                   ' Foint',
                  //                                               style:
                  //                                                   TextStyle(
                  //                                                 color: Hexcolor(
                  //                                                     '#FF1E1E'),
                  //                                                 fontSize: 20,
                  //                                                 fontFamily:
                  //                                                     'Arial',
                  //                                                 fontWeight:
                  //                                                     FontWeight
                  //                                                         .w700,
                  //                                               ),
                  //                                             ),
                  //                                           ],
                  //                                         ),
                  //                                         Text(
                  //                                           'для розблокування цієї місії',
                  //                                           style: TextStyle(
                  //                                             color: Hexcolor(
                  //                                                 '#747474'),
                  //                                             fontSize: 20,
                  //                                             fontFamily:
                  //                                                 'Arial',
                  //                                             fontWeight:
                  //                                                 FontWeight
                  //                                                     .w400,
                  //                                           ),
                  //                                         ),
                  //                                       ],
                  //                                     ),
                  //                                   ),
                  //                                   Container(
                  //                                     margin: EdgeInsets.only(
                  //                                       left: 20,
                  //                                       right: 20,
                  //                                     ),
                  //                                     width:
                  //                                         MediaQuery.of(context)
                  //                                             .size
                  //                                             .width,
                  //                                     height: 165,
                  //                                     decoration: BoxDecoration(
                  //                                         borderRadius:
                  //                                             BorderRadius.all(
                  //                                                 Radius
                  //                                                     .circular(
                  //                                                         20)),
                  //                                         image:
                  //                                             DecorationImage(
                  //                                           image: NetworkImage(
                  //                                               questsDifferent[
                  //                                                       index]
                  //                                                   .image),
                  //                                           fit: BoxFit.cover,
                  //                                         )),
                  //                                   ),
                  //                                   Container(
                  //                                     margin: EdgeInsets.only(
                  //                                       top: 15,
                  //                                       bottom: 15,
                  //                                     ),
                  //                                     child: Text(
                  //                                       questsDifferent[index]
                  //                                           .name,
                  //                                       style: TextStyle(
                  //                                         color: Hexcolor(
                  //                                             '#1E2E45'),
                  //                                         fontSize: 20,
                  //                                         fontWeight:
                  //                                             FontWeight.w700,
                  //                                         fontFamily: 'Arial',
                  //                                       ),
                  //                                       textAlign:
                  //                                           TextAlign.center,
                  //                                     ),
                  //                                   ),
                  //                                   Row(
                  //                                     crossAxisAlignment:
                  //                                         CrossAxisAlignment
                  //                                             .end,
                  //                                     mainAxisAlignment:
                  //                                         MainAxisAlignment
                  //                                             .center,
                  //                                     children: [
                  //                                       Text(
                  //                                         'Вартість',
                  //                                         style: TextStyle(
                  //                                             fontSize: 14,
                  //                                             color: Hexcolor(
                  //                                                 '#B9BCC4'),
                  //                                             fontFamily:
                  //                                                 'Arial',
                  //                                             fontWeight:
                  //                                                 FontWeight
                  //                                                     .w400),
                  //                                       ),
                  //                                       SizedBox(
                  //                                         width: 10,
                  //                                       ),
                  //                                       Text(
                  //                                         '450' + ' Foint',
                  //                                         style: TextStyle(
                  //                                             fontSize: 16,
                  //                                             color: Hexcolor(
                  //                                                 '#FF1E1E'),
                  //                                             fontFamily:
                  //                                                 'Arial',
                  //                                             fontWeight:
                  //                                                 FontWeight
                  //                                                     .w700),
                  //                                       ),
                  //                                     ],
                  //                                   ),
                  //                                 ],
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           Container(
                  //                             margin: EdgeInsets.only(
                  //                               left: 20,
                  //                               right: 20,
                  //                               bottom: 20,
                  //                             ),
                  //                             height: 60,
                  //                             width: MediaQuery.of(context)
                  //                                 .size
                  //                                 .width,
                  //                             child: RaisedButton(
                  //                               child: Text(
                  //                                 "Закрити",
                  //                                 style: TextStyle(
                  //                                   color: Colors.white,
                  //                                   fontSize: 17,
                  //                                   fontWeight: FontWeight.w700,
                  //                                   fontFamily: 'Arial',
                  //                                 ),
                  //                               ),
                  //                               color: Hexcolor('#FE6802'),
                  //                               shape:
                  //                                   new RoundedRectangleBorder(
                  //                                       borderRadius:
                  //                                           new BorderRadius
                  //                                                   .circular(
                  //                                               14.0)),
                  //                               onPressed: () {
                  //                                 Navigator.pushNamed(context,
                  //                                     '/5_myBottomBar.dart');
                  //                               },
                  //                             ),
                  //                           ),
                  //                         ],
                  //                       ),
                  //                     ),
                  //                   ),
                  //                 );
                  //               });
                  //             });
                  //       }

                  //       // showLockProfileWindow() {
                  //       //   showDialog(
                  //       //       barrierDismissible: false,
                  //       //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
                  //       //       context: context,
                  //       //       builder: (context) {
                  //       //         return Dialog(
                  //       //           insetPadding: EdgeInsets.only(
                  //       //             left: 20,
                  //       //             right: 20,
                  //       //           ),
                  //       //           shape: RoundedRectangleBorder(
                  //       //             borderRadius: BorderRadius.circular(10),
                  //       //           ),
                  //       //           child: Container(
                  //       //             // width: MediaQuery.of(context).size.width - 40,
                  //       //             height: 630,
                  //       //             child: Column(
                  //       //               mainAxisAlignment:
                  //       //                   MainAxisAlignment.spaceBetween,
                  //       //               crossAxisAlignment: CrossAxisAlignment.center,
                  //       //               children: [
                  //       //                 Container(
                  //       //                     alignment: Alignment.center,
                  //       //                     margin: EdgeInsets.only(
                  //       //                       top: 20,
                  //       //                       left: 10,
                  //       //                       right: 10,
                  //       //                     ),
                  //       //                     child: Column(
                  //       //                         mainAxisAlignment:
                  //       //                             MainAxisAlignment.center,
                  //       //                         crossAxisAlignment:
                  //       //                             CrossAxisAlignment.center,
                  //       //                         children: [
                  //       //                           Container(
                  //       //                               margin: EdgeInsets.only(
                  //       //                                 bottom: 10,
                  //       //                               ),
                  //       //                               child: Column(
                  //       //                                 children: [
                  //       //                                   Text(
                  //       //                                     'Результат не буде',
                  //       //                                     style: TextStyle(
                  //       //                                       color:
                  //       //                                           Hexcolor('#1E2E45'),
                  //       //                                       fontSize: 24,
                  //       //                                       fontFamily: 'Arial',
                  //       //                                       fontWeight:
                  //       //                                           FontWeight.w700,
                  //       //                                     ),
                  //       //                                     textAlign:
                  //       //                                         TextAlign.center,
                  //       //                                   ),
                  //       //                                   Text(
                  //       //                                     'збережений',
                  //       //                                     style: TextStyle(
                  //       //                                       color:
                  //       //                                           Hexcolor('#1E2E45'),
                  //       //                                       fontSize: 24,
                  //       //                                       fontFamily: 'Arial',
                  //       //                                       fontWeight:
                  //       //                                           FontWeight.w700,
                  //       //                                     ),
                  //       //                                     textAlign:
                  //       //                                         TextAlign.center,
                  //       //                                   ),
                  //       //                                 ],
                  //       //                               )),
                  //       //                           Container(
                  //       //                             margin: EdgeInsets.only(
                  //       //                               bottom: 5,
                  //       //                             ),
                  //       //                             child: Text(
                  //       //                               'Досягнутий вами результат не буде збережений, бо ви не авторизовані.',
                  //       //                               style: TextStyle(
                  //       //                                 color: Hexcolor('#747474'),
                  //       //                                 fontSize: 20,
                  //       //                                 fontFamily: 'Arial',
                  //       //                                 fontWeight: FontWeight.w400,
                  //       //                               ),
                  //       //                               textAlign: TextAlign.center,
                  //       //                             ),
                  //       //                           ),
                  //       //                         ])),
                  //       //                 Container(
                  //       //                   margin: EdgeInsets.only(
                  //       //                     top: 10,
                  //       //                   ),
                  //       //                   child: Image.asset(
                  //       //                     'assets/fg_images/3_layout_3_welcome.jpg',
                  //       //                     height: 290,
                  //       //                   ),
                  //       //                 ),
                  //       //                 Column(
                  //       //                   children: [
                  //       //                     Container(
                  //       //                       margin: EdgeInsets.only(
                  //       //                           bottom: 15, right: 20, left: 20),
                  //       //                       height: 60,
                  //       //                       width:
                  //       //                           MediaQuery.of(context).size.width,
                  //       //                       child: Row(
                  //       //                         children: [
                  //       //                           Container(
                  //       //                             height: 60,
                  //       //                             width: MediaQuery.of(context)
                  //       //                                         .size
                  //       //                                         .width /
                  //       //                                     2 -
                  //       //                                 45,
                  //       //                             child: RaisedButton(
                  //       //                               child: Text(
                  //       //                                 "Реєстрація",
                  //       //                                 style: TextStyle(
                  //       //                                   color: Colors.white,
                  //       //                                   fontSize: 17,
                  //       //                                   fontFamily: 'Arial',
                  //       //                                   fontWeight: FontWeight.w600,
                  //       //                                 ),
                  //       //                               ),
                  //       //                               color: Hexcolor('#FE6802'),
                  //       //                               shape:
                  //       //                                   new RoundedRectangleBorder(
                  //       //                                       borderRadius:
                  //       //                                           new BorderRadius
                  //       //                                                   .circular(
                  //       //                                               14.0)),
                  //       //                               onPressed: () {
                  //       //                                 Navigator.pushNamed(context,
                  //       //                                     '/4_enter_register');
                  //       //                               },
                  //       //                             ),
                  //       //                           ),
                  //       //                           SizedBox(
                  //       //                             width: 10,
                  //       //                           ),
                  //       //                           Container(
                  //       //                             height: 60,
                  //       //                             width: MediaQuery.of(context)
                  //       //                                         .size
                  //       //                                         .width /
                  //       //                                     2 -
                  //       //                                 45,
                  //       //                             child: RaisedButton(
                  //       //                               child: Text(
                  //       //                                 "Авторизація",
                  //       //                                 style: TextStyle(
                  //       //                                   color: Colors.white,
                  //       //                                   fontSize: 17,
                  //       //                                   fontFamily: 'Arial',
                  //       //                                   fontWeight: FontWeight.w600,
                  //       //                                 ),
                  //       //                               ),
                  //       //                               color: Hexcolor('#FE6802'),
                  //       //                               shape:
                  //       //                                   new RoundedRectangleBorder(
                  //       //                                       borderRadius:
                  //       //                                           new BorderRadius
                  //       //                                                   .circular(
                  //       //                                               14.0)),
                  //       //                               onPressed: () {
                  //       //                                 Navigator.pushNamed(
                  //       //                                     context, '/4_enter');
                  //       //                               },
                  //       //                             ),
                  //       //                           )
                  //       //                         ],
                  //       //                       ),
                  //       //                     ),
                  //       //                     Container(
                  //       //                       margin: EdgeInsets.only(
                  //       //                           bottom: 20, right: 20, left: 20),
                  //       //                       height: 60,
                  //       //                       width:
                  //       //                           MediaQuery.of(context).size.width,
                  //       //                       child: RaisedButton(
                  //       //                         child: Text(
                  //       //                           "Продовжити",
                  //       //                           style: TextStyle(
                  //       //                             color: Colors.white,
                  //       //                             fontSize: 17,
                  //       //                             fontFamily: 'Arial',
                  //       //                             fontWeight: FontWeight.w600,
                  //       //                           ),
                  //       //                         ),
                  //       //                         color: Hexcolor('#BEBEBE'),
                  //       //                         shape: new RoundedRectangleBorder(
                  //       //                             borderRadius:
                  //       //                                 new BorderRadius.circular(
                  //       //                                     14.0)),
                  //       //                         onPressed: () {
                  //       //                           Navigator.pushNamed(context,
                  //       //                               questsDifferent[index].linkDesc,
                  //       //                               arguments: ({
                  //       //                                 'name': questsDifferent[index]
                  //       //                                     .name,
                  //       //                                 'image':
                  //       //                                     questsDifferent[index]
                  //       //                                         .image,
                  //       //                                 'distance':
                  //       //                                     questsDifferent[index]
                  //       //                                         .distance,
                  //       //                                 'time': questsDifferent[index]
                  //       //                                     .time,
                  //       //                                 'objects':
                  //       //                                     questsDifferent[index]
                  //       //                                         .objects,
                  //       //                                 'desc': questsDifferent[index]
                  //       //                                     .description
                  //       //                               }));
                  //       //                         },
                  //       //                       ),
                  //       //                     ),
                  //       //                   ],
                  //       //                 ),
                  //       //               ],
                  //       //             ),
                  //       //           ),
                  //       //         );
                  //       //       });
                  //       // }

                  //       // checkAuth() async {
                  //       //   SharedPreferences sharedPreferences =
                  //       //       await SharedPreferences.getInstance();
                  //       //   String email = sharedPreferences.get("email");
                  //       //   if (email == null) {
                  //       //     print('Not authorized');
                  //       //     showLockProfileWindow();
                  //       //   } else {
                  //       //     Navigator.pushNamed(
                  //       //         context, questsDifferent[index].linkDesc,
                  //       //         arguments: ({
                  //       //           'name': questsDifferent[index].name,
                  //       //           'image': questsDifferent[index].image,
                  //       //           'distance': questsDifferent[index].distance,
                  //       //           'time': questsDifferent[index].time,
                  //       //           'objects': questsDifferent[index].objects,
                  //       //           'desc': questsDifferent[index].description
                  //       //         }));
                  //       //   }
                  //       // }
                  //       // _getDistance() async {
                  //       //   double distanceInKilometers = await Geolocator().distanceBetween(currentLocation.latitude, currentLocation.longitude, questsDifferent[index].latitude, questsDifferent[index].longitude);
                  //       //   print((distanceInKilometers*0.001).toStringAsFixed(2));
                  //       // }

                  //       // double distanceInKilometers;
                  //       // bool loading = true;

                  //       // _getDistance() async {
                  //       //   double distanceInKilometers1 = await Geolocator()
                  //       //       .distanceBetween(
                  //       //           currentLocation.latitude,
                  //       //           currentLocation.longitude,
                  //       //           questsDifferent[index].latitude,
                  //       //           questsDifferent[index].longitude);

                  //       //   print((distanceInKilometers1 * 0.001).toStringAsFixed(2));
                  //       //   setState(() {
                  //       //     distanceInKilometers = distanceInKilometers1;
                  //       //     loading = false;
                  //       //   });
                  //       // }

                  //       void launchUrl(String url) async {
                  //         if (await canLaunch(url)) {
                  //           await launch(url);
                  //         } else {
                  //           throw 'Could not open Url';
                  //         }
                  //       }

                  //       Future<void> share() async {
                  //         await FlutterShare.share(
                  //           title:
                  //               'Загружай новое приложение и попробуйте выполнить миссию \n' +
                  //                   questsDifferent[index].name,
                  //           // text: 'Example share text',
                  //           linkUrl: 'https://google.com/',
                  //           // chooserTitle: 'Example Chooser Title'
                  //         );
                  //       }

                  //       return Stack(
                  //         overflow: Overflow.visible,
                  //         children: [
                  //           GestureDetector(
                  //             onTap: () {
                  //               if (questsDifferent[index].id == 9) {
                  //                 launchUrl(
                  //                     'https://freegengame.web.app/'); // showLockedWindow()
                  //               }
                  //               if (questsDifferent[index].isLocked) {
                  //                 showLockedWindow();
                  //               } else {
                  //                 Navigator.pushNamed(
                  //                     context, questsDifferent[index].linkDesc,
                  //                     arguments: ({
                  //                       'name': questsDifferent[index].name,
                  //                       'image': questsDifferent[index].image,
                  //                       'distance':
                  //                           questsDifferent[index].distance,
                  //                       'time': questsDifferent[index].time,
                  //                       'objects':
                  //                           questsDifferent[index].objects,
                  //                       'desc':
                  //                           questsDifferent[index].description
                  //                     }));
                  //               }
                  //               // SplashScreen().method();
                  //             },
                  //             child: Container(
                  //               // alignment: Alignment.topCenter,
                  //               margin: EdgeInsets.only(
                  //                   bottom: 23, left: 14, right: 14),
                  //               height: 190,
                  //               decoration: BoxDecoration(
                  //                   borderRadius:
                  //                       BorderRadius.all(Radius.circular(10)),
                  //                   image: DecorationImage(
                  //                     image: NetworkImage(
                  //                         questsDifferent[index].image),
                  //                     fit: BoxFit.cover,
                  //                   )),
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.end,
                  //                 children: [
                  //                   Container(
                  //                     height: 55,
                  //                     decoration: BoxDecoration(
                  //                       color: questsDifferent[index].isFree ||
                  //                               questsDifferent[index]
                  //                                   .isCompleted
                  //                           ? Hexcolor('1E2E45')
                  //                               .withOpacity(0.8)
                  //                           : questsDifferent[index].isLocked
                  //                               ? Hexcolor('FE6802')
                  //                                   .withOpacity(0.8)
                  //                               : Hexcolor('219653')
                  //                                   .withOpacity(0.8),

                  //                       // questsDifferent[index]
                  //                       //     .color
                  //                       //     .withOpacity(0.8),
                  //                       borderRadius: BorderRadius.only(
                  //                           bottomRight: Radius.circular(10.0),
                  //                           bottomLeft: Radius.circular(10.0)),
                  //                     ),
                  //                     child: Container(
                  //                       margin: EdgeInsets.only(
                  //                           top: 5,
                  //                           bottom: 5,
                  //                           left: 10,
                  //                           right: 10),
                  //                       child: Row(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.spaceBetween,
                  //                         crossAxisAlignment:
                  //                             CrossAxisAlignment.center,
                  //                         children: [
                  //                           Container(
                  //                             child: Flexible(
                  //                               child: Text(
                  //                                 questsDifferent[index].name,
                  //                                 style: TextStyle(
                  //                                   color: Colors.white,
                  //                                   fontSize: 16,
                  //                                   fontFamily: 'Arial',
                  //                                   fontWeight: FontWeight.w700,
                  //                                   letterSpacing: 1.09,
                  //                                 ),
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           Container(
                  //                             width: 125,
                  //                             child: Column(
                  //                               mainAxisAlignment:
                  //                                   MainAxisAlignment.center,
                  //                               crossAxisAlignment:
                  //                                   CrossAxisAlignment.center,
                  //                               children: [
                  //                                 // Row(
                  //                                 //   mainAxisAlignment:
                  //                                 //       MainAxisAlignment
                  //                                 //           .spaceBetween,
                  //                                 //   children: [
                  //                                 //     questsDifferent[index].id == 9
                  //                                 //         ? Container()
                  //                                 //         : Row(
                  //                                 //             mainAxisAlignment:
                  //                                 //                 MainAxisAlignment
                  //                                 //                     .center,
                  //                                 //             crossAxisAlignment:
                  //                                 //                 CrossAxisAlignment
                  //                                 //                     .center,
                  //                                 //             children: [
                  //                                 //               Text(
                  //                                 //                 questsDifferent[
                  //                                 //                             index]
                  //                                 //                         .distance
                  //                                 //                         .toString() +
                  //                                 //                     " км",
                  //                                 //                 style: TextStyle(
                  //                                 //                   color:
                  //                                 //                       Colors.white,
                  //                                 //                   fontSize: 13,
                  //                                 //                   fontFamily:
                  //                                 //                       'Arial',
                  //                                 //                   fontWeight:
                  //                                 //                       FontWeight
                  //                                 //                           .w700,
                  //                                 //                   letterSpacing:
                  //                                 //                       1.09,
                  //                                 //                 ),
                  //                                 //               ),
                  //                                 //               GestureDetector(
                  //                                 //                   behavior:
                  //                                 //                       HitTestBehavior
                  //                                 //                           .opaque,
                  //                                 //                   onTap: () {
                  //                                 //                     // _getDistance();
                  //                                 //                   },
                  //                                 //                   child: Text(
                  //                                 //                     " від вас",
                  //                                 //                     style:
                  //                                 //                         TextStyle(
                  //                                 //                       color: Colors
                  //                                 //                           .white,
                  //                                 //                       fontSize: 13,
                  //                                 //                       fontFamily:
                  //                                 //                           'Arial',
                  //                                 //                       fontWeight:
                  //                                 //                           FontWeight
                  //                                 //                               .w400,
                  //                                 //                       letterSpacing:
                  //                                 //                           1.09,
                  //                                 //                     ),
                  //                                 //                   ))
                  //                                 //             ],
                  //                                 //           ),
                  //                                 //     Container(
                  //                                 //       margin: EdgeInsets.only(
                  //                                 //           left: 10, right: 1.5),
                  //                                 //       child: questsDifferent[index]
                  //                                 //                   .id ==
                  //                                 //               9
                  //                                 //           ? Container()
                  //                                 //           : Image.asset(
                  //                                 //               "assets/fg_images/6_home_quests_pin.png",
                  //                                 //               width: 10.5,
                  //                                 //               height: 15,
                  //                                 //             ),
                  //                                 //     ),
                  //                                 //   ],
                  //                                 // ),
                  //                                 SizedBox(
                  //                                   height: 5,
                  //                                 ),
                  //                                 Row(
                  //                                   mainAxisAlignment:
                  //                                       MainAxisAlignment
                  //                                           .spaceBetween,
                  //                                   children: [
                  //                                     questsDifferent[index]
                  //                                                 .id ==
                  //                                             9
                  //                                         ? Container()
                  //                                         : Row(
                  //                                             mainAxisAlignment:
                  //                                                 MainAxisAlignment
                  //                                                     .center,
                  //                                             crossAxisAlignment:
                  //                                                 CrossAxisAlignment
                  //                                                     .center,
                  //                                             children: [
                  //                                               Text(
                  //                                                 questsDifferent[
                  //                                                         index]
                  //                                                     .time
                  //                                                     .toString(),
                  //                                                 style:
                  //                                                     TextStyle(
                  //                                                   color: Colors
                  //                                                       .white,
                  //                                                   fontSize:
                  //                                                       13,
                  //                                                   fontFamily:
                  //                                                       'Arial',
                  //                                                   fontWeight:
                  //                                                       FontWeight
                  //                                                           .w700,
                  //                                                   letterSpacing:
                  //                                                       1.09,
                  //                                                 ),
                  //                                               ),
                  //                                               Text(
                  //                                                 " хвилин",
                  //                                                 style:
                  //                                                     TextStyle(
                  //                                                   color: Colors
                  //                                                       .white,
                  //                                                   fontSize:
                  //                                                       13,
                  //                                                   fontFamily:
                  //                                                       'Arial',
                  //                                                   fontWeight:
                  //                                                       FontWeight
                  //                                                           .w400,
                  //                                                   letterSpacing:
                  //                                                       1.09,
                  //                                                 ),
                  //                                               )
                  //                                             ],
                  //                                           ),
                  //                                     Container(
                  //                                       margin: EdgeInsets.only(
                  //                                           left: 10),
                  //                                       child: questsDifferent[
                  //                                                       index]
                  //                                                   .id ==
                  //                                               9
                  //                                           ? Container()
                  //                                           : Image.asset(
                  //                                               "assets/fg_images/6_home_logo_time.png",
                  //                                               width: 13.33,
                  //                                               height: 13.33,
                  //                                             ),
                  //                                     ),
                  //                                   ],
                  //                                 )
                  //                               ],
                  //                             ),
                  //                           )
                  //                         ],
                  //                       ),
                  //                     ),
                  //                   )
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //           Positioned(
                  //               right: 28,
                  //               top: 14,
                  //               child: GestureDetector(
                  //                 onTap: () {
                  //                   setState(() {
                  //                     questsDifferent[index].isSelected =
                  //                         !questsDifferent[index].isSelected;
                  //                     if (!questsDifferent[index].isSelected) {
                  //                       print('This quest is selected!');
                  //                       // showSelected();
                  //                       questsFavorites
                  //                           .add(questsDifferent[index]);
                  //                     }
                  //                     if (questsDifferent[index].isSelected) {
                  //                       print('This quest is unselected!');
                  //                       // showUnSelected();
                  //                       questsFavorites
                  //                           .remove(questsDifferent[index]);
                  //                     }
                  //                   });
                  //                 },
                  //                 child: Image.asset(
                  //                   !questsDifferent[index].isSelected
                  //                       ? 'assets/fg_images/9_favorites_icon.png'
                  //                       : 'assets/fg_images/sportGames_icon_favorite.png',
                  //                   height: 23,
                  //                   width: 18,
                  //                 ),
                  //               )),
                  //           // mission12NewYearTreesCompleted == true
                  //           //    ? Positioned(
                  //           //        top: 20,
                  //           //        left: 20,
                  //           //        child: Image.asset(
                  //           //          'assets/fg_images/6_home_logo_mission_completed.png',
                  //           //          height: 38,
                  //           //          width: 150,
                  //           //        )) :
                  //                Positioned(
                  //                   top: 12,
                  //                   left: 26,
                  //                   child: questsDifferent[index].isFree
                  //                       ? Text('')
                  //                       : questsDifferent[index].isLocked
                  //                           ? Image.asset(
                  //                               'assets/fg_images/6_home_logo_lock.png',
                  //                               height: 38,
                  //                               width: 38,
                  //                             )
                  //                           : Image.asset(
                  //                               'assets/fg_images/6_home_logo_unlock.png',
                  //                               height: 38,
                  //                               width: 38,
                  //                             )),
                  //           Positioned(
                  //               right: 12,
                  //               top: (190 / 2) - (50 / 2),
                  //               child: SizedBox(
                  //                 height: 50,
                  //                 width: 50,
                  //                 child: IconButton(
                  //                   icon: Image.asset(
                  //                     'assets/fg_images/6_home_logo_share.png',
                  //                     width: 20,
                  //                     height: 20,
                  //                   ),
                  //                   onPressed: () {
                  //                     share();
                  //                   },
                  //                 ),
                  //               ))
                  //         ],
                  //       );
                  //     },
                  //   ),
                  // ),
                ],
              ),
            ),
    );
  }
}

class CategoriesScroller extends StatefulWidget {
  const CategoriesScroller();

  @override
  _CategoriesScrollerState createState() => _CategoriesScrollerState();
}

class _CategoriesScrollerState extends State<CategoriesScroller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Hexcolor('#7D5AC2'),
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0)),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              // from, APP
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: questsDifferent.length,
                  itemBuilder: (BuildContext context, int index) {
                    showLockedWindow() {
                      showDialog(
                          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
                          barrierDismissible: false,
                          context: context,
                          builder: (context) {
                            var _blankFocusNode2 = new FocusNode();
                            return StatefulBuilder(
                                builder: (context, setState) {
                              return Dialog(
                                insetPadding: EdgeInsets.only(
                                  left: 20,
                                  right: 20,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(_blankFocusNode2);
                                  },
                                  child: Container(
                                    // width: MediaQuery.of(context).size.width - 100,
                                    height: 510,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.only(
                                            top: 20,
                                          ),
                                          child: Text(
                                            'Місія недоступна',
                                            style: TextStyle(
                                              color: Hexcolor('#1E2E45'),
                                              fontSize: 24,
                                              fontWeight: FontWeight.w700,
                                              fontFamily: 'Arial',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: SingleChildScrollView(
                                            physics: BouncingScrollPhysics(),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                    top: 15,
                                                    bottom: 20,
                                                  ),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        children: [
                                                          Text(
                                                            'Вам не вистачає ',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#747474'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                          Text(
                                                            '275' + ' Foint',
                                                            style: TextStyle(
                                                              color: Hexcolor(
                                                                  '#FF1E1E'),
                                                              fontSize: 20,
                                                              fontFamily:
                                                                  'Arial',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                        'для розблокування цієї місії',
                                                        style: TextStyle(
                                                          color: Hexcolor(
                                                              '#747474'),
                                                          fontSize: 20,
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                  ),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 165,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  20)),
                                                      image: DecorationImage(
                                                        image: NetworkImage(
                                                            questsDifferent[
                                                                    index]
                                                                .image),
                                                        fit: BoxFit.cover,
                                                      )),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                    top: 15,
                                                    bottom: 15,
                                                  ),
                                                  child: Text(
                                                    questsDifferent[index].name,
                                                    style: TextStyle(
                                                      color:
                                                          Hexcolor('#1E2E45'),
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      fontFamily: 'Arial',
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      'Вартість',
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: Hexcolor(
                                                              '#B9BCC4'),
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                      '450' + ' Foint',
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          color: Hexcolor(
                                                              '#FF1E1E'),
                                                          fontFamily: 'Arial',
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                            bottom: 20,
                                          ),
                                          height: 60,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: RaisedButton(
                                            child: Text(
                                              "Закрити",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w700,
                                                fontFamily: 'Arial',
                                              ),
                                            ),
                                            color: Hexcolor('#FE6802'),
                                            shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        14.0)),
                                            onPressed: () {
                                              Navigator.pushNamed(context,
                                                  '/5_myBottomBar.dart');
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            });
                          });
                    }

                    // showLockProfileWindow() {
                    //   showDialog(
                    //       barrierDismissible: false,
                    //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
                    //       context: context,
                    //       builder: (context) {
                    //         return Dialog(
                    //           insetPadding: EdgeInsets.only(
                    //             left: 20,
                    //             right: 20,
                    //           ),
                    //           shape: RoundedRectangleBorder(
                    //             borderRadius: BorderRadius.circular(10),
                    //           ),
                    //           child: Container(
                    //             // width: MediaQuery.of(context).size.width - 40,
                    //             height: 600,
                    //             child: Column(
                    //               mainAxisAlignment:
                    //                   MainAxisAlignment.spaceBetween,
                    //               crossAxisAlignment: CrossAxisAlignment.center,
                    //               children: [
                    //                 Container(
                    //                     alignment: Alignment.center,
                    //                     margin: EdgeInsets.only(
                    //                       top: 20,
                    //                       left: 10,
                    //                       right: 10,
                    //                     ),
                    //                     child: Column(
                    //                         mainAxisAlignment:
                    //                             MainAxisAlignment.center,
                    //                         crossAxisAlignment:
                    //                             CrossAxisAlignment.center,
                    //                         children: [
                    //                           Container(
                    //                               margin: EdgeInsets.only(
                    //                                 bottom: 10,
                    //                               ),
                    //                               child: Column(
                    //                                 children: [
                    //                                   Text(
                    //                                     'Результат не буде',
                    //                                     style: TextStyle(
                    //                                       color: Hexcolor(
                    //                                           '#1E2E45'),
                    //                                       fontSize: 24,
                    //                                       fontFamily: 'Arial',
                    //                                       fontWeight:
                    //                                           FontWeight.w700,
                    //                                     ),
                    //                                     textAlign:
                    //                                         TextAlign.center,
                    //                                   ),
                    //                                   Text(
                    //                                     'збережений',
                    //                                     style: TextStyle(
                    //                                       color: Hexcolor(
                    //                                           '#1E2E45'),
                    //                                       fontSize: 24,
                    //                                       fontFamily: 'Arial',
                    //                                       fontWeight:
                    //                                           FontWeight.w700,
                    //                                     ),
                    //                                     textAlign:
                    //                                         TextAlign.center,
                    //                                   ),
                    //                                 ],
                    //                               )),
                    //                           Container(
                    //                             margin: EdgeInsets.only(
                    //                               bottom: 5,
                    //                             ),
                    //                             child: Text(
                    //                               'Досягнутий вами результат не буде збережений, бо ви не авторизовані.',
                    //                               style: TextStyle(
                    //                                 color: Hexcolor('#747474'),
                    //                                 fontSize: 20,
                    //                                 fontFamily: 'Arial',
                    //                                 fontWeight: FontWeight.w400,
                    //                               ),
                    //                               textAlign: TextAlign.center,
                    //                             ),
                    //                           ),
                    //                         ])),
                    //                 Container(
                    //                   margin: EdgeInsets.only(
                    //                     top: 10,
                    //                   ),
                    //                   child: Image.asset(
                    //                     'assets/fg_images/3_layout_3_welcome.jpg',
                    //                     height: 290,
                    //                   ),
                    //                 ),
                    //                 Column(
                    //                   children: [
                    //                     Container(
                    //                       margin: EdgeInsets.only(
                    //                           bottom: 15, right: 20, left: 20),
                    //                       height: 60,
                    //                       width:
                    //                           MediaQuery.of(context).size.width,
                    //                       child: Row(
                    //                         children: [
                    //                           Container(
                    //                             height: 60,
                    //                             width: MediaQuery.of(context)
                    //                                         .size
                    //                                         .width /
                    //                                     2 -
                    //                                 45,
                    //                             child: RaisedButton(
                    //                               child: Text(
                    //                                 "Реєстрація",
                    //                                 style: TextStyle(
                    //                                   color: Colors.white,
                    //                                   fontSize: 17,
                    //                                   fontFamily: 'Arial',
                    //                                   fontWeight:
                    //                                       FontWeight.w600,
                    //                                 ),
                    //                               ),
                    //                               color: Hexcolor('#FE6802'),
                    //                               shape:
                    //                                   new RoundedRectangleBorder(
                    //                                       borderRadius:
                    //                                           new BorderRadius
                    //                                                   .circular(
                    //                                               14.0)),
                    //                               onPressed: () {
                    //                                 Navigator.pushNamed(context,
                    //                                     '/4_enter_register');
                    //                               },
                    //                             ),
                    //                           ),
                    //                           SizedBox(
                    //                             width: 10,
                    //                           ),
                    //                           Container(
                    //                             height: 60,
                    //                             width: MediaQuery.of(context)
                    //                                         .size
                    //                                         .width /
                    //                                     2 -
                    //                                 45,
                    //                             child: RaisedButton(
                    //                               child: Text(
                    //                                 "Авторизація",
                    //                                 style: TextStyle(
                    //                                   color: Colors.white,
                    //                                   fontSize: 17,
                    //                                   fontFamily: 'Arial',
                    //                                   fontWeight:
                    //                                       FontWeight.w600,
                    //                                 ),
                    //                               ),
                    //                               color: Hexcolor('#FE6802'),
                    //                               shape:
                    //                                   new RoundedRectangleBorder(
                    //                                       borderRadius:
                    //                                           new BorderRadius
                    //                                                   .circular(
                    //                                               14.0)),
                    //                               onPressed: () {
                    //                                 Navigator.pushNamed(
                    //                                     context, '/4_enter');
                    //                               },
                    //                             ),
                    //                           )
                    //                         ],
                    //                       ),
                    //                     ),
                    //                     Container(
                    //                       margin: EdgeInsets.only(
                    //                           bottom: 20, right: 20, left: 20),
                    //                       height: 60,
                    //                       width:
                    //                           MediaQuery.of(context).size.width,
                    //                       child: RaisedButton(
                    //                         child: Text(
                    //                           "Продовжити",
                    //                           style: TextStyle(
                    //                             color: Colors.white,
                    //                             fontSize: 17,
                    //                             fontFamily: 'Arial',
                    //                             fontWeight: FontWeight.w600,
                    //                           ),
                    //                         ),
                    //                         color: Hexcolor('#BEBEBE'),
                    //                         shape: new RoundedRectangleBorder(
                    //                             borderRadius:
                    //                                 new BorderRadius.circular(
                    //                                     14.0)),
                    //                         onPressed: () {
                    //                           Navigator.pushNamed(
                    //                               context,
                    //                               questsDifferent[index]
                    //                                   .linkDesc,
                    //                               arguments: ({
                    //                                 'name':
                    //                                     questsDifferent[index]
                    //                                         .name,
                    //                                 'image':
                    //                                     questsDifferent[index]
                    //                                         .image,
                    //                                 'distance':
                    //                                     questsDifferent[index]
                    //                                         .distance,
                    //                                 'time':
                    //                                     questsDifferent[index]
                    //                                         .time,
                    //                                 'objects':
                    //                                     questsDifferent[index]
                    //                                         .objects,
                    //                                 'desc':
                    //                                     questsDifferent[index]
                    //                                         .description
                    //                               }));
                    //                         },
                    //                       ),
                    //                     ),
                    //                   ],
                    //                 ),
                    //               ],
                    //             ),
                    //           ),
                    //         );
                    //       });
                    // }

                    // checkAuth() async {
                    //   SharedPreferences sharedPreferences =
                    //       await SharedPreferences.getInstance();
                    //   String email = sharedPreferences.get("email");
                    //   if (email == null) {
                    //     print('Not authorized');
                    //     showLockProfileWindow();
                    //   } else {
                    //     Navigator.pushNamed(
                    //         context, questsDifferent[index].linkDesc,
                    //         arguments: ({
                    //           'name': questsDifferent[index].name,
                    //           'image': questsDifferent[index].image,
                    //           'distance': questsDifferent[index].distance,
                    //           'time': questsDifferent[index].time,
                    //           'objects': questsDifferent[index].objects,
                    //           'desc': questsDifferent[index].description
                    //         }));
                    //   }
                    // }

                    void launchUrl(String url) async {
                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not open Url';
                      }
                    }

                    return GestureDetector(
                      onTap: () {
                        if (questsDifferent[index].id == 9) {
                          launchUrl('https://freegengame.web.app/');
                        }
                        if (questsDifferent[index].isLocked) {
                          showLockedWindow();
                        } else {
                          Navigator.pushNamed(
                              context, questsDifferent[index].linkDesc,
                              arguments: ({
                                'name': questsDifferent[index].name,
                                'image': questsDifferent[index].image,
                                'distance': questsDifferent[index].distance,
                                'time': questsDifferent[index].time,
                                'objects': questsDifferent[index].objects,
                                'desc': questsDifferent[index].description
                              }));
                        }
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Column(
                          children: <Widget>[
                            Stack(overflow: Overflow.visible, children: [
                              Positioned(
                                  right: 4,
                                  child: CircleAvatar(
                                    radius: 45,
                                    backgroundColor: Hexcolor('#EB5C18'),
                                  )),
                              CircleAvatar(
                                radius: 45,
                                backgroundImage:
                                    NetworkImage(questsDifferent[index].image),
                              ),
                            ]),
                            Container(
                              width: 90,
                              margin: EdgeInsets.only(top: 10),
                              child: Text(
                                questsDifferent[index].name,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}

// class TestApp extends StatefulWidget {
//   @override
//   _TestState createState() => new _TestState();
// }

// class _TestState extends State<TestApp> {
//   String abc = "bb";

//   callback(newAbc) {
//     setState(() {
//       abc = newAbc;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     var column = new Column(
//       children: <Widget>[
//         new Text("This is text $abc"),
//         TestApp2(abc, callback)
//       ],
//     );
//     return new MaterialApp(
//       home: new Scaffold(
//         body: new Padding(padding: EdgeInsets.all(30.0), child: column),
//       ),
//     );
//   }
// }

// class TestApp2 extends StatefulWidget {
//   String abc;
//   Function(String) callback;

//   TestApp2(this.abc, this.callback);

//   @override
//   _TestState2 createState() => new _TestState2();
// }

// class _TestState2 extends State<TestApp2> {
//   @override
//   Widget build(BuildContext context) {
//     return new Container(
//       width: 150.0,
//       height: 30.0,
//       margin: EdgeInsets.only(top: 50.0),
//       child: new FlatButton(
//         onPressed: () {
//           widget.callback("RANDON TEXT"); //call to parent
//         },
//         child: new Text(widget.abc),
//         color: Colors.red,
//       ),
//     );
//   }
// }
