import 'dart:ui';

class CheckPoint {
  final String name;
  final String image;
  final Color color;
  final String link;
  final double lat;
  final double long;
  bool isCompleted;

  CheckPoint({
    this.name,
    this.image,
    this.color,
    this.link,
    this.lat,
    this.long,
    this.isCompleted,
  });
}
