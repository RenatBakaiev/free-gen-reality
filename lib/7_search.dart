// import 'package:flutter/material.dart';
// import '6_quests.dart';

// class MyForm extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => MyFormState();
// }

// class MyFormState extends State {
//   final _formKey = GlobalKey<FormState>();

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         child: new Form(
//       key: _formKey,
//       child: new Column(
//         children: <Widget>[
//           Container(
//             margin: const EdgeInsets.only(top: 18.0, bottom: 18.0),
//             alignment: Alignment.center,
//             child: TextFormField(
//               style: TextStyle(
//                   fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
//               textAlign: TextAlign.left,
//               textAlignVertical: TextAlignVertical.center,
//               decoration: new InputDecoration(
//                 hintText: 'Введіть назву квесту',
//                 contentPadding:
//                     new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
//                 fillColor: Colors.white,
//                 filled: true,
//                 border: new OutlineInputBorder(
//                   borderRadius: new BorderRadius.circular(25.0),
//                 ),
//               ),
//               // ignore: missing_return
//               validator: (String value) {
//                 if (value.isEmpty) return 'Будь-ласка введіть назву квесту!';
//               },
//             ),
//           ),
//         ],
//       ),
//     ));
//   }
// }

// class MyApp8 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.orange[900],
//       body: MyListPage(),
//     );
//   }
// }

// class MyListPage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyListPage> {
//   // final CategoriesScroller categoriesScroller = CategoriesScroller();

//   @override
//   Widget build(BuildContext context) {
//     var _blankFocusNode = new FocusNode();

//     final Size size = MediaQuery.of(context).size;
//     return SafeArea(
//       child: Scaffold(
//         appBar: AppBar(
//           elevation: 0.0,
//           automaticallyImplyLeading: false,
//           backgroundColor: Colors.orange[900],
//           title: Container(
//               child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               GestureDetector(
//                 child: Image.asset('assets/arrow2.png', width: 11, height: 11),
//                 onTap: () {
//                   Navigator.pushNamed(context, '/6_main_layout');
//                 },
//               ),
//               Image.asset('assets/freegen-logo-white.png',
//                   width: 39, height: 34, color: Colors.white),
//               Text(
//                 'FreeGen Reality',
//                 style: TextStyle(
//                     fontSize: 28,
//                     color: Colors.white,
//                     fontFamily: 'ArialRoundedMTBold'),
//               ),
//               SizedBox(
//                 width: 11,
//               )
//             ],
//           )),
//         ),
//         body: GestureDetector(
//           behavior: HitTestBehavior.opaque,
//           onTap: () {
//             FocusScope.of(context).requestFocus(_blankFocusNode);
//           },
//           child: Container(
//             padding: EdgeInsets.only(left: 10, right: 10),
//             decoration: BoxDecoration(
//                 gradient: LinearGradient(
//                     begin: Alignment.topCenter,
//                     end: Alignment.bottomCenter,
//                     colors: [Colors.orange[900], Colors.white])),
//             height: size.height,
//             child:
//              Column(
//               children: <Widget>[
//                 Container(
//                   margin: EdgeInsets.only(top: 40, bottom: 7),
//                   child: Row(
//                     children: <Widget>[
//                       Text(
//                         'Пошук',
//                         style: TextStyle(
//                             fontSize: 24,
//                             color: Colors.white,
//                             fontFamily: 'Comfortaa',
//                             fontWeight: FontWeight.w700),
//                       )
//                     ],
//                   ),
//                 ),
//                 MyForm(),
//                 Container(
//                   margin: EdgeInsets.only(top: 7, bottom: 7),
//                   child: Row(
//                     children: <Widget>[
//                       Text(
//                         'Результати пошуку',
//                         style: TextStyle(
//                             fontSize: 24,
//                             color: Colors.white,
//                             fontFamily: 'Comfortaa',
//                             fontWeight: FontWeight.w700),
//                       )
//                     ],
//                   ),
//                 ),
//                 Expanded(
//                   child: ListView.builder(
//                     physics: BouncingScrollPhysics(),
//                     itemCount: allQuests.length,
//                     itemBuilder: (BuildContext context, int index) {
//                       return Container(
//                           child: Column(
//                         children: <Widget>[
//                           Container(
//                             margin: const EdgeInsets.only(top: 10.0),
//                             alignment: Alignment.center,
//                             height: 137,
//                             width: double.infinity,
//                             child: Image.asset(allQuests[index].imageUrl,
//                                 width: 108, height: 94),
//                             decoration: BoxDecoration(
//                               color: allQuests[index]
//                                   .imageUrlBackColor, //color: Colors.pink[600],
//                               borderRadius: BorderRadius.only(
//                                   topRight: Radius.circular(10.0),
//                                   topLeft: Radius.circular(10.0)),
//                             ),
//                           ),
//                           Container(
//                             alignment: Alignment.center,
//                             height: 38,
//                             width: double.infinity,
//                             child: Text(
//                               allQuests[index].name,
//                               style: TextStyle(
//                                   fontSize: 14,
//                                   color: Colors.black,
//                                   fontFamily: 'Comfortaa',
//                                   fontWeight: FontWeight.w800),
//                             ),
//                             decoration: BoxDecoration(
//                               color: allQuests[index]
//                                   .nameBackColor, //color: Colors.cyan[300],
//                               borderRadius: BorderRadius.only(
//                                   bottomRight: Radius.circular(10.0),
//                                   bottomLeft: Radius.circular(10.0)),
//                             ),
//                           )
//                         ],
//                       ));
//                     },
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//         bottomNavigationBar: Container(
//           alignment: Alignment.topCenter,
//           height: 83,
//           decoration: BoxDecoration(color: Colors.white),
//           child: Container(
//             margin: const EdgeInsets.only(top: 10.0),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_1.png',
//                     height: 15.0,
//                     width: 14.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '/');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_2.png',
//                     height: 16.0,
//                     width: 16.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '/7_search');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_3.png',
//                     height: 42.0,
//                     width: 63.0,
//                   ),
//                   onTap: () {
//                     Navigator.of(context).pushNamed('/6_main_layout_camera');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_4.png',
//                     height: 16.0,
//                     width: 16.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '../screens/home_screen.dart');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_5.png',
//                     height: 15.0,
//                     width: 11.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '/9_profile');
//                   },
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// ------------------------------------------------------SEARCH-EXAMPLE-----------------------------------------------------------------

// import 'package:flutter/material.dart';

// class MyApp8 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Search App'), actions: <Widget>[
//         IconButton(
//           icon: Icon(Icons.search),
//           onPressed: () {
//             showSearch(context: context, delegate: DataSearch());
//           },
//         )
//       ]),
//       drawer: Drawer(),
//       // body: MySearchPage(),
//     );
//   }
// }

// class DataSearch extends SearchDelegate<String> {
//   final cities = [
//     "Agra",
//     "Allahabad",
//     "Bhandup",
//     "Mumbai",
//     "Delphi",
//     "Pune",
//     "Kolkata",
//     "Indore",
//     "Jaipur",
//     "Jalalpur",
//     "Varanasi",
//     "Bangalore",
//     "Nagpur"
//   ];

//   final recentCities = [
//     "Jaipur",
//     "Varanasi",
//     "Bangalore",
//     "Nagpur",
//   ];

//   @override
//   List<Widget> buildActions(BuildContext context) {
//     // actions for app bar
//     return [
//       IconButton(
//           icon: Icon(Icons.clear),
//           onPressed: () {
//             query = "";
//           })
//     ];
//   }

//   @override
//   Widget buildLeading(BuildContext context) {
//     // leading icon on the left of the app bar
//     return IconButton(
//         icon: AnimatedIcon(
//           icon: AnimatedIcons.menu_arrow,
//           progress: transitionAnimation,
//         ),
//         onPressed: () {
//           close(context, null);
//         });
//   }

//   @override
//   Widget buildResults(BuildContext context) {
//     // show some result based on selection
//     return Center(
//       child: Container(
//         height: 100,
//         width: 100,
//         child: Card(
//           color: Colors.red,
//           child: Center(
//             child: Text(query),
//           ),
//         ),
//       ),
//     );
//   }

//   @override
//   Widget buildSuggestions(BuildContext context) {
//     // show when someone searhces for something
//     final suggessionList = query.isEmpty
//         ? recentCities
//         : cities.where((p) => p.startsWith(query)).toList();

//     return ListView.builder(
//       itemBuilder: (context, index) => ListTile(
//         onTap: () {
//           showResults(context);
//         },
//         leading: Icon(Icons.location_city),
//         // title: Text(suggessionList[index]),
//         title: RichText(text: TextSpan(
//           text: suggessionList[index].substring(0,query.length),
//           style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
//           children: [TextSpan(
//             text: suggessionList[index].substring(query.length),
//             style: TextStyle(color: Colors.grey)
//           )]
//         ),

//         ),
//       ),
//       itemCount: suggessionList.length,
//     );
//   }
// }

// ------------------------------------------------------SEARCH-EXAMPLE-2----------------------------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:flutterfreegenapp/list_data.dart';

// class MyApp8 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Search Food Items"),
//         actions: <Widget>[
//           IconButton(onPressed: () {
//             showSearch(context: context, delegate: FoodItemsSearch());
//           }, icon: Icon(Icons.search),)
//         ],
//       ),
//     );
//   }
// }

// class FoodItemsSearch extends SearchDelegate<FoodItem> {
//   @override
//   List<Widget> buildActions(BuildContext context) {
//       return [IconButton(icon: Icon(Icons.clear), onPressed: (){
//         query = "";
//       })];
//     }

//     @override
//     Widget buildLeading(BuildContext context) {
//       return IconButton(icon: Icon(Icons.arrow_back), onPressed: () {
//         close(context, null);
//       });
//     }

//     @override
//     Widget buildResults(BuildContext context) {
//       return Center(child: Text(query, style: TextStyle(fontSize: 20)));
//     }

//     @override
//     Widget buildSuggestions(BuildContext context) {

//       final mylist = query.isEmpty?loadFoodItem() : loadFoodItem().where((p) => p.title.startsWith(query)).toList();

//       return mylist.isEmpty? Text('No Results Found ...', style: TextStyle(fontSize: 20),) : ListView.builder(
//         itemCount: mylist.length,
//         itemBuilder: (context, index) {
//           final FoodItem listitem = mylist[index];
//           return ListTile(
//             onTap: () {
//               showResults(context);
//             },
//             title:
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Text(listitem.title, style: TextStyle(fontSize: 20),),
//               Text(listitem.category, style: TextStyle(color: Colors.grey),),
//               Divider()
//             ],
//           ),);
//         });
//   }

// }
// ------------------------------------------------------SEARCH-VARIANT-2----------------------------------------------------------------
// import 'package:flutter/material.dart';
// import 'package:flutterfreegenapp/6_quest.model.dart';
// import '6_quests.dart';

// class MyForm extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => MyFormState();
// }

// class MyFormState extends State {
//   final _formKey = GlobalKey<FormState>();

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         child: new Form(
//       key: _formKey,
//       child: new Column(
//         children: <Widget>[
//           Container(
//             margin: const EdgeInsets.only(top: 18.0, bottom: 18.0),
//             alignment: Alignment.center,
//             child: GestureDetector(
//               onTap: () {
//                 showSearch(context: context, delegate: QuestSearch());
//               },
//                           child: TextFormField(
//                 style: TextStyle(
//                     fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
//                 textAlign: TextAlign.left,
//                 textAlignVertical: TextAlignVertical.center,
//                 decoration: new InputDecoration(
//                   hintText: 'Введіть назву квесту',
//                   contentPadding:
//                       new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
//                   fillColor: Colors.white,
//                   filled: true,
//                   border: new OutlineInputBorder(
//                     borderRadius: new BorderRadius.circular(25.0),
//                   ),
//                 ),
//                 // ignore: missing_return
//                 validator: (String value) {
//                   if (value.isEmpty) return 'Будь-ласка введіть назву квесту!';
//                 },
//               ),
//             ),
//           ),
//         ],
//       ),
//     ));
//   }
// }

// class MyApp8 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     var _blankFocusNode = new FocusNode();
//     return Scaffold(
//       appBar: AppBar(
//           elevation: 0.0,
//           automaticallyImplyLeading: false,
//           backgroundColor: Colors.orange[900],
//           title: Container(
//               child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               GestureDetector(
//                 child: Image.asset('assets/arrow2.png', width: 11, height: 11),
//                 onTap: () {
//                   Navigator.pushNamed(context, '/6_main_layout');
//                 },
//               ),
//               Image.asset('assets/freegen-logo-white.png',
//                   width: 39, height: 34, color: Colors.white),
//               Text(
//                 'FreeGen Reality',
//                 style: TextStyle(
//                     fontSize: 28,
//                     color: Colors.white,
//                     fontFamily: 'ArialRoundedMTBold'),
//               ),
//               // SizedBox(
//               //   width: 30,
//               //   child: IconButton(
//               //     onPressed: () {
//               //       // showSearch(context: context, delegate: FoodItemsSearch());
//               //     },
//               //     icon: Icon(Icons.search),
//               //   ),
//               // )
//             ],
//           )),
//           actions: <Widget>[
//             IconButton(
//                 icon: Icon(Icons.search),
//                 onPressed: () {
//                   showSearch(context: context, delegate: QuestSearch());
//                 })
//           ]
//           ),
//       body: GestureDetector(
//         behavior: HitTestBehavior.opaque,
//         onTap: () {
//           FocusScope.of(context).requestFocus(_blankFocusNode);
//         },
//         child: Container(
//           padding: EdgeInsets.only(left: 10, right: 10),
//           decoration: BoxDecoration(
//               gradient: LinearGradient(
//                   begin: Alignment.topCenter,
//                   end: Alignment.bottomCenter,
//                   colors: [Colors.orange[900], Colors.white])),
//           height: double.infinity,
//           width: double.infinity,

//             //            child: Column(
//             //   children: <Widget>[
//             //     Container(
//             //       margin: EdgeInsets.only(top: 40, bottom: 7),
//             //       child: Row(
//             //         children: <Widget>[
//             //           Text(
//             //             'Пошук',
//             //             style: TextStyle(
//             //                 fontSize: 24,
//             //                 color: Colors.white,
//             //                 fontFamily: 'Comfortaa',
//             //                 fontWeight: FontWeight.w700),
//             //           )
//             //         ],
//             //       ),
//             //     ),
//             //     MyForm(),
//             //     Container(
//             //       margin: EdgeInsets.only(top: 7, bottom: 7),
//             //       child: Row(
//             //         children: <Widget>[
//             //           Text(
//             //             'Результати пошуку',
//             //             style: TextStyle(
//             //                 fontSize: 24,
//             //                 color: Colors.white,
//             //                 fontFamily: 'Comfortaa',
//             //                 fontWeight: FontWeight.w700),
//             //           )
//             //         ],
//             //       ),
//             //     ),
//             //   ],
//             // ),

//         ),
//       ),
//       bottomNavigationBar: Container(
//         alignment: Alignment.topCenter,
//         height: 83,
//         decoration: BoxDecoration(color: Colors.white),
//         child: Container(
//           margin: const EdgeInsets.only(top: 10.0),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: <Widget>[
//               GestureDetector(
//                 child: Image.asset(
//                   'assets/bottom_pic_1.png',
//                   height: 15.0,
//                   width: 14.0,
//                 ),
//                 onTap: () {
//                   Navigator.pushNamed(context, '/');
//                 },
//               ),
//               GestureDetector(
//                 child: Image.asset(
//                   'assets/bottom_pic_2.png',
//                   height: 16.0,
//                   width: 16.0,
//                 ),
//                 onTap: () {
//                   Navigator.pushNamed(context, '/7_search');
//                 },
//               ),
//               GestureDetector(
//                 child: Image.asset(
//                   'assets/bottom_pic_3.png',
//                   height: 42.0,
//                   width: 63.0,
//                 ),
//                 onTap: () {
//                   Navigator.of(context).pushNamed('/6_main_layout_camera');
//                 },
//               ),
//               GestureDetector(
//                 child: Image.asset(
//                   'assets/bottom_pic_4.png',
//                   height: 16.0,
//                   width: 16.0,
//                 ),
//                 onTap: () {
//                   Navigator.pushNamed(context, '../screens/home_screen.dart');
//                 },
//               ),
//               GestureDetector(
//                 child: Image.asset(
//                   'assets/bottom_pic_5.png',
//                   height: 15.0,
//                   width: 11.0,
//                 ),
//                 onTap: () {
//                   Navigator.pushNamed(context, '/9_profile');
//                 },
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class QuestSearch extends SearchDelegate<String> {
//   @override
//   List<Widget> buildActions(BuildContext context) {
//     return [
//       IconButton(
//           icon: Icon(Icons.clear),
//           onPressed: () {
//             query = "";
//           })
//     ];
//   }

//   @override
//   Widget buildLeading(BuildContext context) {
//     return IconButton(
//         icon: Icon(Icons.arrow_back),
//         onPressed: () {
//           close(context, null);
//         });
//   }

//   @override
//   Widget buildResults(BuildContext context) {
//     return Center(child: Text(query, style: TextStyle(fontSize: 20)));

//   }

//   @override
//     Widget buildSuggestions(BuildContext context) {

//       final mylist = query.isEmpty?popularQuests : allQuests.where((p) => p.name.startsWith(query)).toList();

//       return mylist.isEmpty? Text('No Results Found ...', style: TextStyle(fontSize: 20),) : ListView.builder(
//         itemCount: mylist.length,
//         itemBuilder: (context, index) {
//           final Quest listitem = mylist[index];
//           return ListTile(
//             onTap: () {
//               showResults(context);
//             },
//             title:
//           // Column(
//           //   crossAxisAlignment: CrossAxisAlignment.start,
//           //   children: <Widget>[
//           //     Text(listitem.name, style: TextStyle(fontSize: 20),),

//           //   ],
//           // ),

//           GestureDetector(
//             onTap: () {
//               Navigator.of(context).pushNamed('/google_maps_routes');
//             },
//                       child: Container(
//                 child: Column(
//               children: <Widget>[
//                 Container(
//                   margin: const EdgeInsets.only(top: 10.0),
//                   alignment: Alignment.center,
//                   height: 137,
//                   width: double.infinity,
//                   child: Image.asset(listitem.imageUrl,
//                       width: 108, height: 94),
//                   decoration: BoxDecoration(
//                     color: listitem
//                         .imageUrlBackColor, //color: Colors.pink[600],
//                     borderRadius: BorderRadius.only(
//                         topRight: Radius.circular(10.0),
//                         topLeft: Radius.circular(10.0)),
//                   ),
//                 ),
//                 Container(
//                   alignment: Alignment.center,
//                   height: 38,
//                   width: double.infinity,
//                   child: Text(
//                     listitem.name,
//                     style: TextStyle(
//                         fontSize: 14,
//                         color: Colors.black,
//                         fontFamily: 'Comfortaa',
//                         fontWeight: FontWeight.w800),
//                   ),
//                   decoration: BoxDecoration(
//                     color:
//                         listitem.nameBackColor, //color: Colors.cyan[300],
//                     borderRadius: BorderRadius.only(
//                         bottomRight: Radius.circular(10.0),
//                         bottomLeft: Radius.circular(10.0)),
//                   ),
//                 )
//               ],
//             )

//             ),
//           ),

//           );
//         });
//     // return ListView.builder(
//     //   itemCount: allQuests.length,
//     //   itemBuilder: (context, index) {
//     //     return ListTile(
//     //       title: Container(
//     //           child: Column(
//     //         children: <Widget>[
//     //           Container(
//     //             margin: const EdgeInsets.only(top: 10.0),
//     //             alignment: Alignment.center,
//     //             height: 137,
//     //             width: double.infinity,
//     //             child: Image.asset(allQuests[index].imageUrl,
//     //                 width: 108, height: 94),
//     //             decoration: BoxDecoration(
//     //               color: allQuests[index]
//     //                   .imageUrlBackColor, //color: Colors.pink[600],
//     //               borderRadius: BorderRadius.only(
//     //                   topRight: Radius.circular(10.0),
//     //                   topLeft: Radius.circular(10.0)),
//     //             ),
//     //           ),
//     //           Container(
//     //             alignment: Alignment.center,
//     //             height: 38,
//     //             width: double.infinity,
//     //             child: Text(
//     //               allQuests[index].name,
//     //               style: TextStyle(
//     //                   fontSize: 14,
//     //                   color: Colors.black,
//     //                   fontFamily: 'Comfortaa',
//     //                   fontWeight: FontWeight.w800),
//     //             ),
//     //             decoration: BoxDecoration(
//     //               color:
//     //                   allQuests[index].nameBackColor, //color: Colors.cyan[300],
//     //               borderRadius: BorderRadius.only(
//     //                   bottomRight: Radius.circular(10.0),
//     //                   bottomLeft: Radius.circular(10.0)),
//     //             ),
//     //           )
//     //         ],
//     //       )),
//     //     );
//     //   },
//     // );

//   }
// }

// ------------------------------------------------------SEARCH-VARIANT-3----------------------------------------------------------------

import 'package:flutter/material.dart';
import '6_quests.dart';

class MyApp8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[900],
      body: MyListPage(),
    );
  }
}

class MyListPage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyListPage> {
  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              // GestureDetector(
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //     child: Image.asset('assets/arrow2.png', width: 11, height: 11)),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/6_main_layout');
              //   },
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              SizedBox(
                width: 30,
                height: 30,
              )
            ],
          )),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(_blankFocusNode);
          },
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.orange[900], Colors.white])),
            height: size.height,
            child: Column(
              children: <Widget>[
                _searchBar(),
                
                Container(
                  child: Expanded(
                    child: ListView.builder(
                      shrinkWrap: false,
                      physics: BouncingScrollPhysics(),
                      itemCount: questsForDisplay.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        return index == 0
                            ? SizedBox.shrink()
                            : _listItem(index - 1);
                      },
                    ),
                  ),
                ),
                
              ],
            ),
          ),
        ),
        bottomNavigationBar: Container(
          alignment: Alignment.topCenter,
          height: 83,
          decoration: BoxDecoration(color: Colors.white),
          child: Container(
            margin: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_1.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/6_main_layout');
                    },
                  ),
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_2.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/7_search');
                    },
                  ),
                ),
                GestureDetector(
                  child: Image.asset(
                    'assets/bottom_pic_3.png',
                    height: 42.0,
                    width: 63.0,
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/6_main_layout_camera');
                  },
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_4.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(
                          context, '../screens/home_screen.dart');
                    },
                  ),
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_5.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/9_profile');
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _searchBar() {
    return Column(children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 40, bottom: 7),
        child: Row(
          children: <Widget>[
            Text(
              'Пошук',
              style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontFamily: 'Comfortaa',
                  fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(top: 18.0, bottom: 18.0),
        alignment: Alignment.center,
        child: TextField(
          style: TextStyle(
              fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
          textAlign: TextAlign.left,
          textAlignVertical: TextAlignVertical.center,
          decoration: new InputDecoration(
            hintText: 'Введіть назву квесту',
            contentPadding:
                new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            fillColor: Colors.white,
            filled: true,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
            ),
          ),
          onChanged: (text) {
            text = text.toLowerCase();
            setState(() {
              questsForDisplay = allQuests.where((p) {
                var pName = p.name.toLowerCase();
                return pName.contains(text);
              }).toList();
            });
          },
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 7, bottom: 7),
        child: Row(
          children: <Widget>[
            Text(
              'Результати пошуку',
              style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontFamily: 'Comfortaa',
                  fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    ]);
  }

  _listItem(index) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, questsForDisplay[index].quest);
      },
      child: Container(
          child: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            alignment: Alignment.center,
            height: 137,
            width: double.infinity,
            child: Image.asset(questsForDisplay[index].imageUrl,
                width: 108, height: 94),
            decoration: BoxDecoration(
              color: questsForDisplay[index]
                  .imageUrlBackColor, //color: Colors.pink[600],
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0)),
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: 38,
            width: double.infinity,
            child: Text(
              questsForDisplay[index].name,
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontFamily: 'Comfortaa',
                  fontWeight: FontWeight.w800),
            ),
            decoration: BoxDecoration(
              color: questsForDisplay[index]
                  .nameBackColor, //color: Colors.cyan[300],
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0)),
            ),
          )
        ],
      )),
    );
  }
}

