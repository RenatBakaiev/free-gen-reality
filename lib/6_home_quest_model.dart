import 'dart:ui';

class Quest {
  final int id;
  final String name;
  final String description;
  final String image;
  final Color color;
  final int distance;
  final String linkDesc;
  final String linkMission;
  bool isSelected;
  bool isLocked;
  bool isLockedCash;
  bool isLockedStatus;
  bool isLockedPromo;
  final bool isFree;
  final double latitude;
  final double longitude;
  bool isHidden;
  bool isCompleted;
  final int time;
  final int objects;

  Quest({
    this.id,
    this.name,
    this.description,
    this.image,
    this.color,
    this.distance,
    this.linkDesc,
    this.linkMission,
    this.isSelected,
    this.isLocked,
    this.isLockedCash,
    this.isLockedStatus,
    this.isLockedPromo,
    this.isFree,
    this.latitude,
    this.longitude,
    this.isHidden,
    this.isCompleted,
    this.time,
    this.objects,
  });
}

class PicForCamera {
  final String name;
  final String image;

  PicForCamera({
    this.name,
    this.image,
  });
}

class ArForCamera {
  final String name;
  final String image;

  ArForCamera({
    this.name,
    this.image,
  });
}