import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

class MissionMusic extends StatefulWidget {
  @override
  _MissionMusicState createState() => _MissionMusicState();
}

class _MissionMusicState extends State<MissionMusic> {

    @override
  void initState() {
    super.initState();
    _playFile();
  }

  bool isPlaying = true;
  bool isPaused = false;
  AudioCache cache;
  AudioPlayer player;

  void _playFile() async {
    player = await AudioCache().play("music/main3.mp3");
  }

  void _stopFile() {
    player?.stop(); // stop the file like this
    print('music stopped');
  }

  void pausePlay() {
    if (isPaused && isPlaying) {
      player.resume();
    } else {
      player.pause();
    }
    setState(() {
      isPaused = !isPaused;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 85,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 110),
                            height: 340,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/fg_images/6_home_quest_13.jpg'),
                                  fit: BoxFit.cover,
                                )),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(),
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        pausePlay();
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          right: 20,
                                          top: 30,
                                        ),
                                        child: Image.asset(
                                          isPlaying && !isPaused
                                          ? 'assets/fg_images/0_splash_screen_icon_pause.png'
                                          : 'assets/fg_images/0_splash_screen_icon_play.png',
                                          width: 40,
                                          height: 40,
                                          color: Hexcolor('#FFFFFF'),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(),
                                    Container(),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 20, top: 20),
                                    child: Text(
                                      'Описание',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Arial',
                                        letterSpacing: 1.025,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Прослушайте эту удивительную композицию!',
                                    style: TextStyle(
                                      color: Hexcolor('#545454'),
                                      fontSize: 14,
                                      fontFamily: 'Arial',
                                      letterSpacing: 1.025,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Hexcolor('#7D5AC2'),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#7D5AC2'),
                    ),
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.only(
                      top: 50,
                      bottom: 22,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                // Navigator.pushNamed(
                                //     context, '/desc_description.dart');
                                _stopFile();
                                Navigator.of(context).pop();
                                // Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Описание',
                          style: TextStyle(
                              fontSize: 33,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15, bottom: 25),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width - 30,
                        child: RaisedButton(
                          child: Text(
                            "Далее",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          color: Hexcolor('#FE6802'),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(14.0)),
                          onPressed: () {
                            _stopFile();
                            Navigator.pushNamed(context, '/quest_done.dart');
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
