class SliderModel {
  String image;
  String imageBack;
  String title;
  String text;

  SliderModel({
    this.image,
    this.imageBack,
    this.title,
    this.text,
  });

  void setImage(String getImage) {
    image = getImage;
  }

  void setImageBack(String getImageBack) {
    imageBack = getImageBack;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setText(String getText) {
    text = getText;
  }

  String getImage() {
    return image;
  }

  String getImageBack() {
    return imageBack;
  }

  String getTitle() {
    return title;
  }

  String getText() {
    return text;
  }
}

List<SliderModel> getSlides() {

  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();

  //1
  sliderModel.setImage('assets/fg_images/1_layout_1_icon_arrow.png');
  sliderModel.setImageBack('assets/fg_images/1_layout_1_back2.jpg');
  sliderModel.setTitle('Самостiйнi та груповi квести або екскурсii простонеба!');
  sliderModel.setText('Перетвори свiй телефон в персонального гiда який проведе екскурсiю, або квест-гру по цiкавим мiсцям. Просто обери, або вподобай квест чи прогулянку та почни в будь-який час');
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  //2
  sliderModel.setImage('assets/fg_images/2_layout_2_icon_arrow.png');
  sliderModel.setImageBack('assets/fg_images/2_layout_2_back2.jpg');
  sliderModel.setTitle('Усi прогоди у твоiй власнiй \nкишенi');
  sliderModel.setText('Квести наповненi iсторiями i цiкавими фактами вiд мiсцевих, аудiогiдоми вiкторинами та загадками доповненою реальнiстю. Буде яскраво!');
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  //3
  sliderModel.setImage('assets/fg_images/0_splash_screen_lion.png');
  sliderModel.setImageBack('assets/fg_images/3_layout_3_back3.jpg');
  sliderModel.setTitle('Найкращий спосiб ознайомитися iз закладом, локацiею, або мiстом');
  sliderModel.setText('Вбудованi карти, контрольнi мiтки, считувач: QR-кодiв, забражень та обектiв, - не дозволять тобi нудьгувати. А цiкавi моменти, iсторii та загадки, допоможуть дiзнатися бiльше корисноi iнформацii про мiсцевiсть');
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  return slides;
}
