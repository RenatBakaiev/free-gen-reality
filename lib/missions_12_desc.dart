import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:ui' as ui;
import 'package:video_player/video_player.dart';
// import '6_home_quests.dart';
// import 'package:model_viewer/model_viewer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_free_gen_reality/ProgressHUD.dart';


// import 'package:shared_preferences/shared_preferences.dart';

class Missions12Desc extends StatefulWidget {
  @override
  _Missions12DescState createState() => _Missions12DescState();
}

class _Missions12DescState extends State<Missions12Desc> {
  bool isFavorite = false;
  bool isApiCallProcess = false;

  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
        'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_MAIN.mp4');
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    _controller.play();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    // final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    Future<void> share() async {
      await FlutterShare.share(
        title: 'Завантажуйте новий додаток і спробуйте виконати місію \n' +
            // title: 'Загружай новое приложение и попробуйте выполнить миссию \n' +
            // arguments['name'],
            'Ялинкоий квест',
        // text: 'Example share text',
        linkUrl: 'https://google.com/',
        // chooserTitle: 'Example Chooser Title'
      );
    }

    // showInfoWindow() {
    //   showDialog(
    //       barrierDismissible: false,
    //       barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
    //       context: context,
    //       builder: (context) {
    //         return Dialog(
    //           // insetPadding: EdgeInsets.only(
    //           //   left: 20,
    //           //   right: 20,
    //           // ),
    //           shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.circular(20),
    //           ),
    //           child: Stack(
    //             overflow: Overflow.visible,
    //             children: [
    //               Container(
    //                 height: 450,
    //                 width: 390,
    //                 child: Column(
    //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                   crossAxisAlignment: CrossAxisAlignment.center,
    //                   children: [
    //                     Container(
    //                       margin: EdgeInsets.only(
    //                         top: 20,
    //                       ),
    //                       child: Text(
    //                         'Початок місії',
    //                         style: TextStyle(
    //                           color: Hexcolor('#1E2E45'),
    //                           fontSize: 24,
    //                           fontFamily: 'Arial',
    //                           fontWeight: FontWeight.w700,
    //                         ),
    //                       ),
    //                     ),
    //                     Container(
    //                       // start progress line
    //                       margin: EdgeInsets.only(top: 30),
    //                       child: Column(
    //                         children: [
    //                           Row(
    //                             mainAxisAlignment: MainAxisAlignment.center,
    //                             crossAxisAlignment: CrossAxisAlignment.center,
    //                             children: [
    //                               Stack(
    //                                 overflow: Overflow.visible,
    //                                 children: [
    //                                   Image.asset(
    //                                       'assets/fg_images/next_step_icon_start.png',
    //                                       width: 12,
    //                                       height: 12),
    //                                   Positioned(
    //                                     bottom: 9,
    //                                     left: 4.5,
    //                                     child: Row(
    //                                       mainAxisAlignment:
    //                                           MainAxisAlignment.center,
    //                                       crossAxisAlignment:
    //                                           CrossAxisAlignment.center,
    //                                       children: [
    //                                         Image.asset(
    //                                             'assets/fg_images/next_step_icon_start_flag.png',
    //                                             width: 18,
    //                                             height: 21),
    //                                         Container(
    //                                           margin: EdgeInsets.only(
    //                                               left: 10, bottom: 8),
    //                                           child: Text(
    //                                             '',
    //                                             style: TextStyle(
    //                                               color: Hexcolor('#1E2E45'),
    //                                               fontSize: 12,
    //                                               fontFamily: 'Arial',
    //                                               fontWeight: FontWeight.w400,
    //                                             ),
    //                                           ),
    //                                         )
    //                                       ],
    //                                     ),
    //                                   )
    //                                 ],
    //                               ),
    //                               // Container(
    //                               //   height: 1.5,
    //                               //   width: 25,
    //                               //   decoration:
    //                               //       BoxDecoration(color: Hexcolor('#1E2E45')),
    //                               // ),
    //                               Container(
    //                                 height: 25,
    //                                 // margin: EdgeInsets.only(right: 25),
    //                                 child: ListView.builder(
    //                                     shrinkWrap: true,
    //                                     scrollDirection: Axis.horizontal,
    //                                     itemCount: 6,
    //                                     itemBuilder: (context, index) {
    //                                       return Row(
    //                                         children: [
    //                                           Container(
    //                                             height: 1.5,
    //                                             width: 15,
    //                                             decoration: BoxDecoration(
    //                                                 color: Hexcolor('#1E2E45')),
    //                                           ),
    //                                           Image.asset(
    //                                               'assets/fg_images/next_step_icon_notdone_step.png',
    //                                               width: 12,
    //                                               height: 12),
    //                                         ],
    //                                       );
    //                                     }),
    //                               ),
    //                               Container(
    //                                 margin: EdgeInsets.only(
    //                                   top: 11,
    //                                   // right: 35,
    //                                 ),
    //                                 child: Image.asset(
    //                                   'assets/fg_images/turn_icon.png',
    //                                   width: 25,
    //                                 ),
    //                               ),
    //                             ],
    //                           ),
    //                           Container(
    //                             margin: EdgeInsets.only(
    //                               left: 200,
    //                             ),
    //                             child: Row(
    //                               mainAxisAlignment: MainAxisAlignment.center,
    //                               crossAxisAlignment: CrossAxisAlignment.center,
    //                               children: [
    //                                 Image.asset(
    //                                     'assets/fg_images/next_step_icon_notdone_step.png',
    //                                     width: 12,
    //                                     height: 12),
    //                               ],
    //                             ),
    //                           ),
    //                           Row(
    //                             mainAxisAlignment: MainAxisAlignment.center,
    //                             crossAxisAlignment: CrossAxisAlignment.center,
    //                             children: [
    //                               SizedBox(
    //                                 width: 28,
    //                               ),
    //                               Stack(
    //                                 overflow: Overflow.visible,
    //                                 children: [
    //                                   Image.asset(
    //                                       'assets/fg_images/next_step_icon_notdone_step.png',
    //                                       width: 12,
    //                                       height: 12),
    //                                   Positioned(
    //                                     bottom: -20,
    //                                     left: 5.5,
    //                                     child: Row(
    //                                       mainAxisAlignment:
    //                                           MainAxisAlignment.center,
    //                                       crossAxisAlignment:
    //                                           CrossAxisAlignment.center,
    //                                       children: [
    //                                         Image.asset(
    //                                             'assets/fg_images/next_step_icon_finish_flag2.png',
    //                                             width: 18,
    //                                             height: 21),
    //                                         Container(
    //                                           margin: EdgeInsets.only(
    //                                               left: 10, top: 8),
    //                                           child: Text(
    //                                             '',
    //                                             style: TextStyle(
    //                                               color: Hexcolor('#1E2E45'),
    //                                               fontSize: 12,
    //                                               fontFamily: 'Arial',
    //                                               fontWeight: FontWeight.w400,
    //                                             ),
    //                                           ),
    //                                         )
    //                                       ],
    //                                     ),
    //                                   )
    //                                 ],
    //                               ),
    //                               // Container(
    //                               //   height: 1.5,
    //                               //   width: 25,
    //                               //   decoration:
    //                               //       BoxDecoration(color: Hexcolor('#1E2E45')),
    //                               // ),

    //                               Container(
    //                                 height: 25,
    //                                 child: ListView.builder(
    //                                     shrinkWrap: true,
    //                                     scrollDirection: Axis.horizontal,
    //                                     itemCount: 5,
    //                                     itemBuilder: (context, index) {
    //                                       return Row(
    //                                         children: [
    //                                           Container(
    //                                             height: 1.5,
    //                                             width: 15,
    //                                             decoration: BoxDecoration(
    //                                                 color: Hexcolor('#1E2E45')),
    //                                           ),
    //                                           Image.asset(
    //                                               'assets/fg_images/next_step_icon_notdone_step.png',
    //                                               width: 12,
    //                                               height: 12),
    //                                         ],
    //                                       );
    //                                     }),
    //                               ),
    //                               Container(
    //                                 margin: EdgeInsets.only(
    //                                   bottom: 11,
    //                                 ),
    //                                 child: Image.asset(
    //                                   'assets/fg_images/turn_icon2.png',
    //                                   width: 25,
    //                                 ),
    //                               ),
    //                               // SizedBox(width: 35,)
    //                             ],
    //                           ),
    //                         ],
    //                       ),
    //                     ),
    //                     Container(
    //                       margin: EdgeInsets.only(
    //                           // top: 10,
    //                           ),
    //                       child: Text(
    //                         'Поздоровляємо з початком місії,\nвам залишилось пройти ' +
    //                             '12' // arguments['objects'].toString() +
    //                                 ' кроків!',
    //                         style: TextStyle(
    //                           color: Hexcolor('#747474'),
    //                           fontSize: 16,
    //                           fontFamily: 'Arial',
    //                           fontWeight: FontWeight.w400,
    //                         ),
    //                         textAlign: TextAlign.center,
    //                       ),
    //                     ),
    //                     Container(
    //                       margin: EdgeInsets.only(
    //                         left: 20,
    //                         right: 20,
    //                         bottom: 20,
    //                       ),
    //                       height: 60,
    //                       width: MediaQuery.of(context).size.width - 80,
    //                       child: RaisedButton(
    //                         child: Text(
    //                           "Почати місію",
    //                           style: TextStyle(
    //                             color: Colors.white,
    //                             fontSize: 17,
    //                             fontFamily: 'Arial',
    //                             fontWeight: FontWeight.w700,
    //                           ),
    //                         ),
    //                         color: Hexcolor('#FE6802'),
    //                         shape: new RoundedRectangleBorder(
    //                             borderRadius: new BorderRadius.circular(14.0)),
    //                         onPressed: () {
    //                           Navigator.pushNamed(
    //                               context, '/missions_12_12_trees.dart');
    //                         },
    //                       ),
    //                     ),
    //                   ],
    //                 ),
    //               ),
    //               Positioned(
    //                 top: -130,
    //                 right: -50,
    //                 child: Image.asset(
    //                   'assets/fg_images/error_mission_icon_unlock.png',
    //                   width: 200,
    //                   height: 200,
    //                 ),
    //               ),
    //               Positioned(
    //                 right: 40,
    //                 top: 50,
    //                 child: Container(
    //                   child: Row(
    //                     mainAxisAlignment: MainAxisAlignment.end,
    //                     crossAxisAlignment: CrossAxisAlignment.end,
    //                     children: [
    //                       // Text(
    //                       //   '0',
    //                       //   style: TextStyle(
    //                       //     fontSize: 36,
    //                       //     color: Hexcolor('#1E2E45'),
    //                       //     fontFamily: 'Arial',
    //                       //     fontWeight: FontWeight.w700,
    //                       //   ),
    //                       // ),
    //                       // Container(
    //                       //   margin: EdgeInsets.only(bottom: 2),
    //                       //   child: Text(
    //                       //     ' ' + '/ ' + '12',
    //                       //     style: TextStyle(
    //                       //       fontSize: 24,
    //                       //       color: Hexcolor('#1E2E45'),
    //                       //       fontFamily: 'Arial',
    //                       //       fontWeight: FontWeight.w400,
    //                       //     ),
    //                       //   ),
    //                       // ),
    //                     ],
    //                   ),
    //                 ),
    //               )
    //             ],
    //           ),
    //         );
    //       });
    // }

    showWindowQuizDone() {
      showDialog(
          barrierDismissible: false,
          barrierColor: Hexcolor('#341F5E').withOpacity(0.8),
          context: context,
          builder: (context) {
            return Dialog(
              insetPadding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'Завдання пройдене',
                            style: TextStyle(
                              color: Hexcolor('#1E2E45'),
                              fontSize: 24,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Text(
                          'Ви не можете повторно пройти це завдання',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Hexcolor('#747474'),
                            fontSize: 16,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w400,
                            height: 1.4,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          height: 60,
                          width: MediaQuery.of(context).size.width - 80,
                          child: RaisedButton(
                            child: Text(
                              "Закрити",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 1.05),
                            ),
                            color: Hexcolor('#FE6802'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(14.0)),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          });
    }

    

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 85,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          margin: EdgeInsets.only(top: 250), // change height
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Hexcolor('#7D5AC2'),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0)),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 5,
                            ),
                            child: Text(
                              'Ялинковий квест',
                              // 'Все шаги',
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              height: 260,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  // borderRadius: BorderRadius.only(
                                  //     bottomRight: Radius.circular(10.0),
                                  //     bottomLeft: Radius.circular(10.0)),
                                  image: DecorationImage(
                                image: NetworkImage(
                                    'http://generation-admin.ehub.com.ua/api/file/downloadFile/2020-12-24_z12_missions_desc.gif'),
                                // arguments['image']),
                                fit: BoxFit.cover,
                              )),
                              child: Stack(
                                fit: StackFit.expand,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: 10,
                                              top: 35,
                                            ),
                                            child: IconButton(
                                              icon: Image.asset(
                                                'assets/fg_images/6_home_search_back.png',
                                                width: 13.16,
                                                height: 25,
                                              ),
                                              onPressed: () {
                                                if (_controller
                                                    .value.isPlaying) {
                                                  _controller.pause();
                                                }
                                                Navigator.pushNamed(context,
                                                    '/5_myBottomBar.dart');
                                              },
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 35),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    setState(() {
                                                      isFavorite = !isFavorite;
                                                    });
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      isFavorite
                                                          ? 'assets/fg_images/9_favorites_icon.png'
                                                          : 'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 19.25,
                                                      height: 27.5,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    if (_controller
                                                        .value.isPlaying) {
                                                      _controller.pause();
                                                    }
                                                    Navigator.pushNamed(
                                                        context, '/7_map.dart');
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/fg_images/sportGames_icon_map.png',
                                                      width: 18.75,
                                                      height: 23.75,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    share();
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Image.asset(
                                                      'assets/fg_images/6_home_logo_share.png',
                                                      width: 24.4,
                                                      height: 26.28,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                              left: 20,
                                              bottom: 15,
                                              right: 20,
                                            ),
                                            child:
                                                // Text('')
                                                Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                SizedBox(width: 38),
                                                // Image.asset(
                                                //   'assets/fg_images/6_home_logo_unlock.png',
                                                //   width: 38,
                                                //   height: 38,
                                                // ),
                                                // Row(
                                                //   mainAxisAlignment:
                                                //       MainAxisAlignment.end,
                                                //   crossAxisAlignment:
                                                //       CrossAxisAlignment.end,
                                                //   children: [
                                                //     Text(
                                                //       '0',
                                                //       style: TextStyle(
                                                //         fontSize: 36,
                                                //         color: Colors.white,
                                                //         fontFamily: 'Arial',
                                                //         fontWeight:
                                                //             FontWeight.w700,
                                                //       ),
                                                //     ),
                                                //     Text(
                                                //       ' ' +
                                                //           '/ ' + '12',
                                                //           // arguments['objects']
                                                //               // .toString(),
                                                //       style: TextStyle(
                                                //         fontSize: 24,
                                                //         color: Colors.white,
                                                //         fontFamily: 'Arial',
                                                //         fontWeight:
                                                //             FontWeight.w400,
                                                //       ),
                                                //     ),
                                                //   ],
                                                // ),
                                              ],
                                            ),
                                          ),
                                          // BackdropFilter(
                                          //   filter: ui.ImageFilter.blur(
                                          //     sigmaX: 8.0,
                                          //     sigmaY: 8.0,
                                          //   ),
                                          //   child:
                                          Container(
                                            padding: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                            ),
                                            height: 40,
                                            child: Container(),
                                          ),
                                          // ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      child: ClipRect(
                                        child: BackdropFilter(
                                          filter: ui.ImageFilter.blur(
                                            sigmaX: 3.0,
                                            sigmaY: 3.0,
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              right: 20,
                                              left: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0),
                                              ),
                                            ),
                                            alignment: Alignment.center,
                                            height: 40.0,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/6_home_logo_time.png',
                                                      width: 18,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      '180',
                                                      // arguments['time']
                                                      //     .toString(),
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' хвилин',
                                                      // arguments['time'] != 1
                                                      //     ? ' хвилин'
                                                      //     : ' хвилина',
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 7.5,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Image.asset(
                                                      'assets/fg_images/sportGames_icon_favorite.png',
                                                      width: 15,
                                                      height: 18,
                                                      color:
                                                          Hexcolor('#FE6802'),
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      '12',
                                                      // arguments['objects']
                                                      //     .toString(),
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' об\'єктів',
                                                      // arguments['objects'] != 1
                                                      //     ? " об'єктів"
                                                      //     : " об'єкт",
                                                      style: TextStyle(
                                                        fontSize: 13.5,
                                                        color: Colors.white,
                                                        fontFamily: 'Arial',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                // SizedBox(
                                                //   width: 7.5,
                                                // ),
                                                // Row(
                                                //   mainAxisAlignment:
                                                //       MainAxisAlignment.start,
                                                //   crossAxisAlignment:
                                                //       CrossAxisAlignment.end,
                                                //   children: [
                                                //     Image.asset(
                                                //       'assets/fg_images/6_home_icon_flag.png',
                                                //       width: 11,
                                                //       height: 18.5,
                                                //       color:
                                                //           Hexcolor('#FE6802'),
                                                //     ),
                                                //     SizedBox(
                                                //       width: 5,
                                                //     ),
                                                //     Text(
                                                //       '5'
                                                //       // arguments['distance']
                                                //       //         .toString() +
                                                //       ' км',
                                                //       style: TextStyle(
                                                //         fontSize: 13.5,
                                                //         color: Colors.white,
                                                //         fontFamily: 'Arial',
                                                //         fontWeight:
                                                //             FontWeight.w600,
                                                //       ),
                                                //     ),
                                                //     Text(
                                                //       ' до старту',
                                                //       style: TextStyle(
                                                //         fontSize: 13.5,
                                                //         color: Colors.white,
                                                //         fontFamily: 'Arial',
                                                //         fontWeight:
                                                //             FontWeight.w400,
                                                //       ),
                                                //     )
                                                //   ],
                                                // ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            // Container(
                            //   height: 260, // change height
                            //   width: MediaQuery.of(context).size.width,
                            //   decoration: BoxDecoration(
                            //       borderRadius: BorderRadius.only(
                            //           bottomRight: Radius.circular(10.0),
                            //           bottomLeft: Radius.circular(10.0)),
                            //       image: DecorationImage(
                            //         image: AssetImage(
                            //             // 'assets/fg_images/sportGames_pic_moto.jpg'),
                            //             'assets/fg_images/6_home_quest_6.png'),
                            //         fit: BoxFit.cover,
                            //       )),
                            //   child: Column(
                            //     mainAxisAlignment:
                            //         MainAxisAlignment.spaceBetween,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Row(
                            //         mainAxisAlignment:
                            //             MainAxisAlignment.spaceBetween,
                            //         children: [
                            //           Container(
                            //             margin: EdgeInsets.only(
                            //               left: 10,
                            //               top: 35,
                            //             ),
                            //             child: IconButton(
                            //               icon: Image.asset(
                            //                 'assets/fg_images/6_home_search_back.png',
                            //                 width: 13.16,
                            //                 height: 25,
                            //               ),
                            //               onPressed: () {
                            //                 Navigator.pushNamed(
                            //                     context, '/5_myBottomBar.dart');
                            //               },
                            //             ),
                            //           ),
                            //           Container(
                            //             margin: EdgeInsets.only(top: 35),
                            //             child: Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.end,
                            //               children: [
                            //                 GestureDetector(
                            //                   behavior: HitTestBehavior.opaque,
                            //                   onTap: () {
                            //                     setState(() {
                            //                       isFavorite = !isFavorite;
                            //                     });
                            //                   },
                            //                   child: Container(
                            //                     margin:
                            //                         EdgeInsets.only(right: 20),
                            //                     child: Image.asset(
                            //                       isFavorite
                            //                           ? 'assets/fg_images/9_favorites_icon.png'
                            //                           : 'assets/fg_images/sportGames_icon_favorite.png',
                            //                       width: 19.25,
                            //                       height: 27.5,
                            //                     ),
                            //                   ),
                            //                 ),
                            //                 GestureDetector(
                            //                   behavior: HitTestBehavior.opaque,
                            //                   onTap: () {
                            //                     Navigator.pushNamed(
                            //                         context, '/7_map.dart');
                            //                   },
                            //                   child: Container(
                            //                     margin:
                            //                         EdgeInsets.only(right: 20),
                            //                     child: Image.asset(
                            //                       'assets/fg_images/sportGames_icon_map.png',
                            //                       width: 18.75,
                            //                       height: 23.75,
                            //                     ),
                            //                   ),
                            //                 ),
                            //                 GestureDetector(
                            //                   behavior: HitTestBehavior.opaque,
                            //                   onTap: () {
                            //                     share();
                            //                   },
                            //                   child: Container(
                            //                     margin:
                            //                         EdgeInsets.only(right: 20),
                            //                     child: Image.asset(
                            //                       'assets/fg_images/6_home_logo_share.png',
                            //                       width: 24.4,
                            //                       height: 26.28,
                            //                     ),
                            //                   ),
                            //                 )
                            //               ],
                            //             ),
                            //           )
                            //         ],
                            //       ),
                            //       Column(
                            //         children: [
                            //           Container(
                            //             width:
                            //                 MediaQuery.of(context).size.width,
                            //             margin: EdgeInsets.only(
                            //               left: 20,
                            //               bottom: 20,
                            //               right: 20,
                            //             ),
                            //             child:
                            //                 // Text('')
                            //                 Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.spaceBetween,
                            //               crossAxisAlignment:
                            //                   CrossAxisAlignment.end,
                            //               children: [
                            //                 Image.asset(
                            //                   'assets/fg_images/6_home_logo_unlock.png',
                            //                   width: 38,
                            //                   height: 38,
                            //                 ),
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.end,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Text(
                            //                       '0',
                            //                       style: TextStyle(
                            //                         fontSize: 36,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w700,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' ' + '/ ' + '4',
                            //                       style: TextStyle(
                            //                         fontSize: 24,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     ),
                            //                   ],
                            //                 ),
                            //               ],
                            //             ),
                            //           ),
                            //           // BackdropFilter(
                            //           //   filter: ui.ImageFilter.blur(
                            //           //     sigmaX: 8.0,
                            //           //     sigmaY: 8.0,
                            //           //   ),
                            //           //   child:
                            //           Container(
                            //             padding: EdgeInsets.only(
                            //               left: 20,
                            //               right: 20,
                            //             ),
                            //             decoration: BoxDecoration(
                            //               borderRadius: BorderRadius.only(
                            //                 bottomRight: Radius.circular(10.0),
                            //                 bottomLeft: Radius.circular(10.0),
                            //               ),
                            //               // color: Colors.orange.withOpacity(0.2),
                            //               color: Colors.orange.withOpacity(0.2),
                            //             ),
                            //             height: 65,
                            //             child: Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.spaceBetween,
                            //               crossAxisAlignment:
                            //                   CrossAxisAlignment.center,
                            //               children: [
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.start,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/6_home_logo_time.png',
                            //                       width: 18,
                            //                       height: 18,
                            //                       color: Hexcolor('#FE6802'),
                            //                     ),
                            //                     SizedBox(
                            //                       width: 5,
                            //                     ),
                            //                     Text(
                            //                       '200',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w600,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' минут',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //                 SizedBox(
                            //                   width: 7.5,
                            //                 ),
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.start,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/sportGames_icon_favorite.png',
                            //                       width: 15,
                            //                       height: 18,
                            //                       color: Hexcolor('#FE6802'),
                            //                     ),
                            //                     SizedBox(
                            //                       width: 5,
                            //                     ),
                            //                     Text(
                            //                       '4',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w600,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' объекта',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //                 SizedBox(
                            //                   width: 7.5,
                            //                 ),
                            //                 Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.start,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/6_home_icon_flag.png',
                            //                       width: 11,
                            //                       height: 18.5,
                            //                       color: Hexcolor('#FE6802'),
                            //                     ),
                            //                     SizedBox(
                            //                       width: 5,
                            //                     ),
                            //                     Text(
                            //                       '10' + ' км',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w600,
                            //                       ),
                            //                     ),
                            //                     Text(
                            //                       ' до старта',
                            //                       style: TextStyle(
                            //                         fontSize: 13.5,
                            //                         color: Colors.white,
                            //                         fontFamily: 'Arial',
                            //                         fontWeight: FontWeight.w400,
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //               ],
                            //             ),
                            //           ),
                            //           // ),
                            //         ],
                            //       ),
                            //     ],
                            //   ),
                            // ),
                            // Positioned(
                            //   top: 10,
                            //   bottom: 10,
                            //   right: 10,
                            //   left: 10,
                            //   child: BackdropFilter(
                            //     filter: ui.ImageFilter.blur(
                            //       sigmaX: 5,
                            //       sigmaY: 5,
                            //     ),
                            //     child: Container(
                            //       color: Colors.white.withOpacity(0),
                            //     ),
                            //   ),
                            // ),
                          ],
                        )
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 20,
                                top: 20,
                              ),
                              child: Text(
                                'Опис',
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 1.025,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                if (_controller.value.isPlaying) {
                                  _controller.pause();
                                } else {
                                  _controller.play();
                                }
                              },
                              child: FutureBuilder(
                                future: _initializeVideoPlayerFuture,
                                builder: (context, snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    return AspectRatio(
                                      aspectRatio:
                                          _controller.value.aspectRatio,
                                      child: VideoPlayer(_controller),
                                    );
                                  } else {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                },
                              ),
                            ),
                            // Container(
                            //   height: 200,
                            //   child: ModelViewer(
                            //     src:
                            //         'https://modelviewer.dev/shared-assets/models/Astronaut.glb',
                            //     alt: "A 3D model of an astronaut",
                            //     ar: true,
                            //     autoRotate: true,
                            //     cameraControls: true,
                            //   ),
                            // ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 20,
                                bottom: 20,
                              ),
                              child: Text(
                                'Ялинка від ялинки різниться, як і райони в яких вони затишно вмостились. Хоча може це стереотип і його час зруйнувати? Розібратися у цьому непростому питанні, сидячи на дивані, не вийде 😜\nОдягайся тепліше, бери друзів і приготуйся руйнувати стереотипи, шукати артефакти й танцювати з персонажами біля новорічних ялинок Києва!\n\nА тепер детальніше:\n У цій місії буде зображена карта з 12-ма точками. Точки відповідають ялинкам. Тобі необхідно:\n1 - вибрати на карті точку\n2 - прийти туди та зачекінитись\n3 - пройти вікторину\n4 - виконати завдання.\nПотім можеш переходити до наступної ялинки 🎄За правильне виконання завдань нараховуються бали, а бали можна обміняти на корисності в розділі "магазин".\n\nP. S.: Виконуючи деякі завдання, орієнтуйся по лого додатка.\n\nТепер можеш вирушати в дорогу! Вдачі 😉',
                                // arguments['desc'],
                                style: TextStyle(
                                  color: Hexcolor('#545454'),
                                  fontSize: 18,
                                  fontFamily: 'Arial',
                                  letterSpacing: 0.75,
                                  height: 1.3,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            // Container(
                            //   height: 260,
                            //   width: MediaQuery.of(context).size.width,
                            //   decoration: BoxDecoration(
                            //       // borderRadius: BorderRadius.only(
                            //       //     bottomRight: Radius.circular(10.0),
                            //       //     bottomLeft: Radius.circular(10.0)),
                            //       image: DecorationImage(
                            //         image: AssetImage(
                            //             // 'assets/fg_images/sportGames_pic_moto.jpg'),
                            //             'assets/fg_images/6_home_quest_6.png'),
                            //         fit: BoxFit.cover,
                            //       )),
                            //   child: Stack(
                            //     fit: StackFit.expand,
                            //     children: <Widget>[
                            //       Column(
                            //         mainAxisAlignment:
                            //             MainAxisAlignment.spaceBetween,
                            //         crossAxisAlignment:
                            //             CrossAxisAlignment.center,
                            //         children: [
                            //           Row(
                            //             mainAxisAlignment:
                            //                 MainAxisAlignment.spaceBetween,
                            //             children: [
                            //               Container(
                            //                 margin: EdgeInsets.only(
                            //                   left: 10,
                            //                   top: 35,
                            //                 ),
                            //                 child: IconButton(
                            //                   icon: Image.asset(
                            //                     'assets/fg_images/6_home_search_back.png',
                            //                     width: 13.16,
                            //                     height: 25,
                            //                   ),
                            //                   onPressed: () {
                            //                     Navigator.pushNamed(context,
                            //                         '/5_myBottomBar.dart');
                            //                   },
                            //                 ),
                            //               ),
                            //               Container(
                            //                 margin: EdgeInsets.only(top: 35),
                            //                 child: Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.end,
                            //                   children: [
                            //                     GestureDetector(
                            //                       behavior:
                            //                           HitTestBehavior.opaque,
                            //                       onTap: () {
                            //                         setState(() {
                            //                           isFavorite = !isFavorite;
                            //                         });
                            //                       },
                            //                       child: Container(
                            //                         margin: EdgeInsets.only(
                            //                             right: 20),
                            //                         child: Image.asset(
                            //                           isFavorite
                            //                               ? 'assets/fg_images/9_favorites_icon.png'
                            //                               : 'assets/fg_images/sportGames_icon_favorite.png',
                            //                           width: 19.25,
                            //                           height: 27.5,
                            //                         ),
                            //                       ),
                            //                     ),
                            //                     GestureDetector(
                            //                       behavior:
                            //                           HitTestBehavior.opaque,
                            //                       onTap: () {
                            //                         Navigator.pushNamed(
                            //                             context, '/7_map.dart');
                            //                       },
                            //                       child: Container(
                            //                         margin: EdgeInsets.only(
                            //                             right: 20),
                            //                         child: Image.asset(
                            //                           'assets/fg_images/sportGames_icon_map.png',
                            //                           width: 18.75,
                            //                           height: 23.75,
                            //                         ),
                            //                       ),
                            //                     ),
                            //                     GestureDetector(
                            //                       behavior:
                            //                           HitTestBehavior.opaque,
                            //                       onTap: () {
                            //                         share();
                            //                       },
                            //                       child: Container(
                            //                         margin: EdgeInsets.only(
                            //                             right: 20),
                            //                         child: Image.asset(
                            //                           'assets/fg_images/6_home_logo_share.png',
                            //                           width: 24.4,
                            //                           height: 26.28,
                            //                         ),
                            //                       ),
                            //                     )
                            //                   ],
                            //                 ),
                            //               )
                            //             ],
                            //           ),
                            //           Column(
                            //             children: [
                            //               Container(
                            //                 width: MediaQuery.of(context)
                            //                     .size
                            //                     .width,
                            //                 margin: EdgeInsets.only(
                            //                   left: 20,
                            //                   bottom: 20,
                            //                   right: 20,
                            //                 ),
                            //                 child:
                            //                     // Text('')
                            //                     Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment
                            //                           .spaceBetween,
                            //                   crossAxisAlignment:
                            //                       CrossAxisAlignment.end,
                            //                   children: [
                            //                     Image.asset(
                            //                       'assets/fg_images/6_home_logo_unlock.png',
                            //                       width: 38,
                            //                       height: 38,
                            //                     ),
                            //                     Row(
                            //                       mainAxisAlignment:
                            //                           MainAxisAlignment.end,
                            //                       crossAxisAlignment:
                            //                           CrossAxisAlignment.end,
                            //                       children: [
                            //                         Text(
                            //                           '0',
                            //                           style: TextStyle(
                            //                             fontSize: 36,
                            //                             color: Colors.white,
                            //                             fontFamily: 'Arial',
                            //                             fontWeight:
                            //                                 FontWeight.w700,
                            //                           ),
                            //                         ),
                            //                         Text(
                            //                           ' ' + '/ ' + '4',
                            //                           style: TextStyle(
                            //                             fontSize: 24,
                            //                             color: Colors.white,
                            //                             fontFamily: 'Arial',
                            //                             fontWeight:
                            //                                 FontWeight.w400,
                            //                           ),
                            //                         ),
                            //                       ],
                            //                     ),
                            //                   ],
                            //                 ),
                            //               ),
                            //               // BackdropFilter(
                            //               //   filter: ui.ImageFilter.blur(
                            //               //     sigmaX: 8.0,
                            //               //     sigmaY: 8.0,
                            //               //   ),
                            //               //   child:
                            //               Container(
                            //                 padding: EdgeInsets.only(
                            //                   left: 20,
                            //                   right: 20,
                            //                 ),
                            //                 height: 65,
                            //                 child: Container(),
                            //               ),
                            //               // ),
                            //             ],
                            //           ),
                            //         ],
                            //       ),
                            //       Align(
                            //         alignment: Alignment.bottomCenter,
                            //           child: Container(
                            //             child: ClipRect(
                            //               child: BackdropFilter(
                            //                 filter: ui.ImageFilter.blur(
                            //                   sigmaX: 3.0,
                            //                   sigmaY: 3.0,
                            //                 ),
                            //                 child: Container(
                            //                   decoration: BoxDecoration(
                            //                     borderRadius: BorderRadius.only(
                            //                       bottomRight:
                            //                           Radius.circular(10.0),
                            //                       bottomLeft: Radius.circular(10.0),
                            //                     ),
                            //                   ),
                            //                   alignment: Alignment.center,
                            //                   height: 65.0,
                            //                   child: Row(
                            //                     mainAxisAlignment:
                            //                         MainAxisAlignment.spaceBetween,
                            //                     crossAxisAlignment:
                            //                         CrossAxisAlignment.center,
                            //                     children: [
                            //                       Row(
                            //                         mainAxisAlignment:
                            //                             MainAxisAlignment.start,
                            //                         crossAxisAlignment:
                            //                             CrossAxisAlignment.end,
                            //                         children: [
                            //                           Image.asset(
                            //                             'assets/fg_images/6_home_logo_time.png',
                            //                             width: 18,
                            //                             height: 18,
                            //                             color: Hexcolor('#FE6802'),
                            //                           ),
                            //                           SizedBox(
                            //                             width: 5,
                            //                           ),
                            //                           Text(
                            //                             '200',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w600,
                            //                             ),
                            //                           ),
                            //                           Text(
                            //                             ' минут',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w400,
                            //                             ),
                            //                           )
                            //                         ],
                            //                       ),
                            //                       SizedBox(
                            //                         width: 7.5,
                            //                       ),
                            //                       Row(
                            //                         mainAxisAlignment:
                            //                             MainAxisAlignment.start,
                            //                         crossAxisAlignment:
                            //                             CrossAxisAlignment.end,
                            //                         children: [
                            //                           Image.asset(
                            //                             'assets/fg_images/sportGames_icon_favorite.png',
                            //                             width: 15,
                            //                             height: 18,
                            //                             color: Hexcolor('#FE6802'),
                            //                           ),
                            //                           SizedBox(
                            //                             width: 5,
                            //                           ),
                            //                           Text(
                            //                             '4',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w600,
                            //                             ),
                            //                           ),
                            //                           Text(
                            //                             ' объекта',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w400,
                            //                             ),
                            //                           )
                            //                         ],
                            //                       ),
                            //                       SizedBox(
                            //                         width: 7.5,
                            //                       ),
                            //                       Row(
                            //                         mainAxisAlignment:
                            //                             MainAxisAlignment.start,
                            //                         crossAxisAlignment:
                            //                             CrossAxisAlignment.end,
                            //                         children: [
                            //                           Image.asset(
                            //                             'assets/fg_images/6_home_icon_flag.png',
                            //                             width: 11,
                            //                             height: 18.5,
                            //                             color: Hexcolor('#FE6802'),
                            //                           ),
                            //                           SizedBox(
                            //                             width: 5,
                            //                           ),
                            //                           Text(
                            //                             '10' + ' км',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w600,
                            //                             ),
                            //                           ),
                            //                           Text(
                            //                             ' до старта',
                            //                             style: TextStyle(
                            //                               fontSize: 13.5,
                            //                               color: Colors.white,
                            //                               fontFamily: 'Arial',
                            //                               fontWeight:
                            //                                   FontWeight.w400,
                            //                             ),
                            //                           )
                            //                         ],
                            //                       ),
                            //                     ],
                            //                   ),
                            //                 ),
                            //               ),
                            //             ),
                            //           ),

                            //       ),
                            //     ],
                            //   ),
                            // )

                            // Container(
                            //   margin: EdgeInsets.only(top: 15),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Container(
                            //           margin: EdgeInsets.only(
                            //             right: 15,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/fg_images/sportGames_icon_pin.png',
                            //             width: 20,
                            //             height: 29,
                            //           )),
                            //       Flexible(
                            //           child: Text(
                            //         'Пересечение улиц Владимирская и Рейтарская, Киев',
                            //         style: TextStyle(
                            //           color: Hexcolor('#545454'),
                            //           fontSize: 16,
                            //           fontFamily: 'Arial',
                            //           // letterSpacing: 1.025,
                            //         ),
                            //       )),
                            //     ],
                            //   ),
                            // ),

                            // Container(
                            //   margin: EdgeInsets.only(top: 15, bottom: 15),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.center,
                            //     children: [
                            //       Container(
                            //           margin: EdgeInsets.only(
                            //             right: 15,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/fg_images/sportGames_icon_clock.png',
                            //             width: 25,
                            //             height: 25,
                            //           )),
                            //       Flexible(
                            //           child: Text(
                            //         '12.03.2021 ( 16:30 )',
                            //         style: TextStyle(
                            //           color: Hexcolor('#545454'),
                            //           fontSize: 16,
                            //           fontFamily: 'Arial',
                            //           // letterSpacing: 1.025,
                            //         ),
                            //       )),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            //             Container(
            //   padding: EdgeInsets.only(left: 20),
            //   margin: EdgeInsets.only(top: 398),
            //   alignment: Alignment.centerLeft,
            //   width: MediaQuery.of(context).size.width,
            //   height: 70,
            //   decoration: BoxDecoration(
            //     color: Hexcolor('#7D5AC2'),
            //     borderRadius: BorderRadius.only(
            //         bottomLeft: Radius.circular(10.0),
            //         bottomRight: Radius.circular(10.0)),
            //   ),
            //   child: Text(
            //     'Спортивные игры',
            //     style: TextStyle(
            //       fontSize: 25,
            //       color: Colors.white,
            //       fontFamily: 'AvenirNextLTPro-Regular',
            //       fontWeight: FontWeight.w900,
            //     ),
            //   ),
            // ),

            Container(
              margin: EdgeInsets.only(bottom: 25),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              child: RaisedButton(
                child: Text(
                  "Почати Місію",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                color: Hexcolor('#FE6802'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(14.0)),
                onPressed: () async {
                  if (_controller.value.isPlaying) {
                    _controller.pause();
                  }
                  setState(() {
                    isApiCallProcess = true;
                  });
                  final SharedPreferences sharedPreferences =
                      await SharedPreferences.getInstance();
                  var res1 = sharedPreferences.getBool('isStep1Completed');
                  var res2 = sharedPreferences.getBool('isStep2Completed');
                  var res3 = sharedPreferences.getBool('isStep3Completed');
                  var res4 = sharedPreferences.getBool('isStep4Completed');
                  var res5 = sharedPreferences.getBool('isStep5Completed');
                  var res6 = sharedPreferences.getBool('isStep6Completed');
                  var res7 = sharedPreferences.getBool('isStep7Completed');
                  var res8 = sharedPreferences.getBool('isStep8Completed');
                  var res9 = sharedPreferences.getBool('isStep9Completed');
                  var res10 = sharedPreferences.getBool('isStep10Completed');
                  var res11 = sharedPreferences.getBool('isStep11Completed');
                  var res12 = sharedPreferences.getBool('isStep12Completed');
                  if (res1 == true &&
                      res2 == true &&
                      res3 == true &&
                      res4 == true &&
                      res5 == true &&
                      res6 == true &&
                      res7 == true &&
                      res8 == true &&
                      res9 == true &&
                      res10 == true &&
                      res11 == true &&
                      res12 == true) { 
                        showWindowQuizDone();                   
                    setState(() {
                      isApiCallProcess = false;
                    });
                  } else {
                    Navigator.pushNamed(context, '/missions_12_12_trees.dart');
                    setState(() {
                      isApiCallProcess = false;
                    });
                  }

                  
                },
              ),
            ),

            ///
          ],
        ),
      ),
    );
  }
}
