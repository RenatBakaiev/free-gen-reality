class Product {
  final String name;
  final String image;
  final int quantity;
  final String place;
  final String date;
  final String favoriteIcon;
  final String linkToItemFromStore;

  Product({
    this.name,
    this.image,
    this.quantity,
    this.place,
    this.date,
    this.favoriteIcon,
    this.linkToItemFromStore,
  });
}
