import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class PlayerAudio extends StatefulWidget {
  @override
  _PlayerAudioState createState() => _PlayerAudioState();
}

class _PlayerAudioState extends State<PlayerAudio> {
  Duration duration = new Duration();
  Duration position = new Duration();
  AudioPlayer advancedPlayer = new AudioPlayer();
  bool playing = false;
  bool volumeOff = false;

  void changeVolume(double value) {
    advancedPlayer.setVolume(value);
  }

  @override
  void initState() {
    super.initState();
    // initPlayer();
    _startPlay();
  }

  Widget slider() {
    return SliderTheme(
      data: SliderThemeData(
          trackHeight: 2,
          thumbColor: Hexcolor('#FE6802'),
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 6)),
      child: Slider(
          // mouseCursor: ,
          activeColor: Hexcolor('#FE6802'),
          inactiveColor: Hexcolor('#BEBEBE'),
          value: position.inSeconds.toDouble(),
          min: 0.0,
          max: duration.inSeconds.toDouble(),
          onChanged: (double value) {
            setState(() {
              advancedPlayer.seek(new Duration(seconds: value.toInt()));
            });
          }),
    );
  }

  _startPlay() async {
    if (!playing) {
      advancedPlayer = await AudioCache().play("music/main.mp3");
      setState(() {
        playing = true;
      });
    }

    advancedPlayer.onDurationChanged.listen((Duration d) {
      setState(() {
        duration = d;
      });
    });

    advancedPlayer.onAudioPositionChanged.listen((Duration d) {
      setState(() {
        position = d;
      });
    });
  }

  _getAudio() async {
    if (playing) {
      advancedPlayer.pause();
      setState(() {
        playing = false;
      });
    } else {
      advancedPlayer.resume();
      setState(() {
        playing = true;
      });
      advancedPlayer.onDurationChanged.listen((Duration d) {
        setState(() {
          duration = d;
        });
      });

      advancedPlayer.onAudioPositionChanged.listen((Duration d) {
        setState(() {
          position = d;
        });
      });
    }
  }

  // void _stopFile() {
  //   advancedPlayer.stop();
  //   setState(() {
  //     playing = false;
  //   }); // stop the file like this
  //   advancedPlayer.onDurationChanged.listen((Duration d) {
  //     setState(() {
  //       duration = d;
  //     });
  //   });

  //   advancedPlayer.onAudioPositionChanged.listen((Duration d) {
  //     setState(() {
  //       position = d;
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Hexcolor('#F2F2F2'),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Hexcolor('#7D5AC2'),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: Hexcolor('#7D5AC2'),
                ),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(
                  top: 50,
                  bottom: 22,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: SizedBox(
                            child: IconButton(
                              icon: Image.asset(
                                'assets/fg_images/6_home_search_back.png',
                                width: 10,
                                height: 19,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        Text(
                          'Аудио',
                          style: TextStyle(
                              fontSize: 32,
                              color: Colors.white,
                              fontFamily: 'Arial',
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: IconButton(
                          icon: Image.asset(
                            'assets/fg_images/run_map_quest_pic_gostart.png',
                            width: 22,
                            height: 24,
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/5_myBottomBar.dart');
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 0, left: 20, right: 20),
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        image:
                            AssetImage('assets/fg_images/player_audio_pic.jpg'),
                        fit: BoxFit.cover,
                      )),
                ),
                slider(),
                Container(
                  margin: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                          '${position.inMinutes}:${position.inSeconds.remainder(60)}'),
                      Text(
                          '${duration.inMinutes}:${duration.inSeconds.remainder(60)}'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    !volumeOff
                        ? GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              changeVolume(0);
                              setState(() {
                                volumeOff = true;
                              });
                            },
                            child: Image.asset(
                              'assets/fg_images/player_audio_sound_on.png',
                              height: 30,
                              width: 30,
                            ),
                          )
                        : GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              changeVolume(1);
                              setState(() {
                                volumeOff = false;
                              });
                            },
                            child: Image.asset(
                              'assets/fg_images/player_audio_sound_off.png',
                              height: 30,
                              width: 30,
                            ),
                          ),
                    SizedBox(
                      width: 50,
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        _getAudio();
                      },
                      child: Image.asset(
                        !playing
                            ? 'assets/fg_images/player_audio_play.png'
                            : 'assets/fg_images/player_audio_pause.png',
                        height: 70,
                        width: 70,
                      ),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    SizedBox(
                      width: 30,
                    ),
                  ],
                ),
                // Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     crossAxisAlignment: CrossAxisAlignment.center,
                //     children: [
                //       RaisedButton(
                //         child: Text('Ree/Pau'),
                //         onPressed: () {
                //           _getAudio();
                //         },
                //       ),
                //       RaisedButton(
                //         child: Text('Stop'),
                //         onPressed: () {
                //           _stopFile();
                //         },
                //       ), // changeVolume
                //       RaisedButton(
                //         child: Text('Set v 0'),
                //         onPressed: () {
                //           changeVolume(0);
                //         },
                //       ),
                //       RaisedButton(
                //         child: Text('Set v 1'),
                //         onPressed: () {
                //           changeVolume(1);
                //         },
                //       ),
                //     ]),
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width - 40,
                    child: RaisedButton(
                      child: Text(
                        "Закрыть",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Hexcolor('#FE6802'),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(14.0)),
                      onPressed: () {
                        // Navigator.pushNamed(context, '/quest_done.dart');
                        Navigator.pushNamed(context, '/5_myBottomBar.dart');
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
